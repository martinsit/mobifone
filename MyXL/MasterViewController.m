//
//  MasterViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/5/12.
//  Copyright (c) 2012 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "MasterViewController.h"
#import "AXISnetNavigationBar.h"

#import "CommonCell.h"
#import "MenuModel.h"
#import "BalanceViewController.h"
#import "PackageViewController.h"
#import "PromoViewController.h"
#import "DataManager.h"

#import "AXISnetCommon.h"
#import <QuartzCore/QuartzCore.h>

#import "Constant.h"
#import "NotificationViewController.h"

NSString * const MSMasterViewControllerCellReuseIdentifier = @"MSMasterViewControllerCellReuseIdentifier";

typedef NS_ENUM(NSUInteger, MSMasterViewControllerTableViewSectionType) {
    MSMasterViewControllerTableViewSectionTypeColors,
    MSMasterViewControllerTableViewSectionTypeAbout,
    MSMasterViewControllerTableViewSectionTypeCount,
};

@interface MasterViewController ()

@property (nonatomic, strong) NSDictionary *paneViewControllerTitles;
@property (nonatomic, strong) NSDictionary *paneViewControllerTintColor;
@property (nonatomic, strong) NSDictionary *paneViewControllerClasses;
@property (nonatomic, strong) NSArray *tableViewSectionBreaks;

@property (strong, nonatomic) UINavigationController* navController;
- (UINavigationController *)customizedNavigationController;
- (void)createCompleteButton:(UIViewController*)paneViewController;

@end

@implementation MasterViewController

@synthesize content;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.paneViewControllerType = NSUIntegerMax;
        self.paneViewControllerClasses = @{
        @(MSPaneViewControllerType1) : BalanceViewController.class,
        @(MSPaneViewControllerType2) : PackageViewController.class,
        @(MSPaneViewControllerType3) : UITableViewController.class,
        @(MSPaneViewControllerType4) : PromoViewController.class,
        @(MSPaneViewControllerType5) : UITableViewController.class,
        @(MSPaneViewControllerType6) : UITableViewController.class,
        };
        /*
        self.paneViewControllerType = NSUIntegerMax;
        self.paneViewControllerTitles = @{
        @(MSPaneViewControllerTypeRed) : @"Red",
        @(MSPaneViewControllerTypeGreen) : @"Green",
        @(MSPaneViewControllerTypeBlue) : @"Blue",
        @(MSPaneViewControllerTypeMonospace) : @"Monospace Ltd.",
        };
        self.paneViewControllerClasses = @{
        @(MSPaneViewControllerTypeRed) : UITableViewController.class,
        @(MSPaneViewControllerTypeGreen) : UITableViewController.class,
        @(MSPaneViewControllerTypeBlue) : UITableViewController.class,
        @(MSPaneViewControllerTypeMonospace) : DetailViewController.class,
        };
        self.paneViewControllerTintColor = @{
        @(MSPaneViewControllerTypeRed) : [UIColor colorWithRed:0.502 green:0.000 blue:0.000 alpha:1.000],
        @(MSPaneViewControllerTypeGreen) : [UIColor colorWithRed:0.251 green:0.502 blue:0.000 alpha:1.000],
        @(MSPaneViewControllerTypeBlue) : [UIColor colorWithRed:0.000 green:0.251 blue:0.502 alpha:1.000],
        @(MSPaneViewControllerTypeMonospace) : [UIColor blackColor],
        };
        self.tableViewSectionBreaks = @[
        @(MSPaneViewControllerTypeMonospace),
        @(MSPaneViewControllerTypeCount)
        ];*/
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //[self.tableView registerClass:UITableViewCell.class forCellReuseIdentifier:MSMasterViewControllerCellReuseIdentifier];
    
    self.tableView.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
    
    /*
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)] autorelease];
    self.navigationItem.rightBarButtonItem = addButton;*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MSMasterViewController

- (MSPaneViewControllerType)paneViewControllerTypeForIndexPath:(NSIndexPath *)indexPath
{
    MSPaneViewControllerType paneViewControllerType;
    if (indexPath.section == 0) {
        paneViewControllerType = indexPath.row;
    } else {
        paneViewControllerType = [self.tableViewSectionBreaks[indexPath.section - 1] integerValue] + indexPath.row;
    }
    NSAssert(paneViewControllerType < MSPaneViewControllerTypeCount, @"Invalid Index Path");
    return paneViewControllerType;
}

- (void)transitionToViewController:(MSPaneViewControllerType)paneViewControllerType
{
    if (paneViewControllerType == self.paneViewControllerType) {
        [self.navigationPaneViewController setPaneState:MSNavigationPaneStateClosed animated:YES];
        return;
    }
    
    BOOL animateTransition = self.navigationPaneViewController.paneViewController != nil;
    
    Class paneViewControllerClass = self.paneViewControllerClasses[@(paneViewControllerType)];
    NSParameterAssert([paneViewControllerClass isSubclassOfClass:UIViewController.class]);
    
    UIViewController *paneViewController = (UIViewController *)[[paneViewControllerClass alloc] init];
    
    //paneViewController.navigationItem.title = self.paneViewControllerTitles[@(paneViewControllerType)];
    //paneViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MSBarButtonIconNavigationPane.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(navigationPaneBarButtonItemTapped:)];
    
    [self createCompleteButton:paneViewController];
    
    //UINavigationController *paneNavigationViewController = [[UINavigationController alloc] initWithRootViewController:paneViewController];
    
    //
    UINavigationController *paneNavigationViewController = [self customizedNavigationController];
    [paneNavigationViewController setViewControllers:[NSArray arrayWithObject:paneViewController]];
    [self setNavController:paneNavigationViewController];
    //
    
    //paneNavigationViewController.navigationBar.tintColor = self.paneViewControllerTintColor[@(paneViewControllerType)];
    
    [self.navigationPaneViewController setPaneViewController:paneNavigationViewController animated:animateTransition completion:nil];
    
    self.paneViewControllerType = paneViewControllerType;
}

- (void)navigationPaneBarButtonItemTapped:(id)sender;
{
    [self.navigationPaneViewController setPaneState:MSNavigationPaneStateOpen animated:YES];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return MSMasterViewControllerTableViewSectionTypeCount;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    /*
    if (section == 0) {
        return [self.tableViewSectionBreaks[section] integerValue];
    } else {
        return ([self.tableViewSectionBreaks[section] integerValue] - [self.tableViewSectionBreaks[(section - 1)] integerValue]);
    }*/
    return [self.content count];
}

/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case MSMasterViewControllerTableViewSectionTypeColors:
            return @"Colors";
        case MSMasterViewControllerTableViewSectionTypeAbout:
            return @"About";
        default:
            return nil;
    }
}*/

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MSMasterViewControllerCellReuseIdentifier forIndexPath:indexPath];
    cell.textLabel.text = self.paneViewControllerTitles[@([self paneViewControllerTypeForIndexPath:indexPath])];
    return cell;*/
    
    static NSString *CellIdentifier = @"Cell";
	
	//UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		//cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}
    
    MenuModel *menuModel = [self.content objectAtIndex:indexPath.row];
    
    cell.iconLabel.text = @"A";
    cell.titleLabel.text = [menuModel.menuName uppercaseString];
    cell.arrowLabel.text = @"d";
    return cell;
}

/*
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    if (!self.detailViewController) {
        self.detailViewController = [[[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil] autorelease];
    }
    NSDate *object = _objects[indexPath.row];
    self.detailViewController.detailItem = object;
    [self.navigationController pushViewController:self.detailViewController animated:YES];*/
    
    /*
    MenuModel *menuModel = [self.content objectAtIndex:indexPath.row];
    DataManager *dataManager = [DataManager sharedInstance];
    dataManager.parentId = menuModel.menuId;
    
    [self transitionToViewController:[self paneViewControllerTypeForIndexPath:indexPath]];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];*/
}

/*
- (UITableViewCellAccessoryType)tableView:(UITableView *)tv accessoryTypeForRowWithIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellAccessoryDisclosureIndicator;
}*/

#pragma mark - Selector

- (UINavigationController *)customizedNavigationController
{
    UINavigationController *navController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
    
    // Ensure the UINavigationBar is created so that it can be archived. If we do not access the
    // navigation bar then it will not be allocated, and thus, it will not be archived by the
    // NSKeyedArchvier.
    [navController navigationBar];
    
    // Archive the navigation controller.
    NSMutableData *data = [NSMutableData data];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:navController forKey:@"root"];
    [archiver finishEncoding];
    [archiver release];
    [navController release];
    
    // Unarchive the navigation controller and ensure that our UINavigationBar subclass is used.
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [unarchiver setClass:[AXISnetNavigationBar class] forClassName:@"UINavigationBar"];
    UINavigationController *customizedNavController = [unarchiver decodeObjectForKey:@"root"];
    [unarchiver finishDecoding];
    [unarchiver release];
    
    // Modify the navigation bar to have a background image.
    AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[customizedNavController navigationBar];
    [navBar setTintColor:[UIColor whiteColor]];
    //[navBar setBackgroundImage:[UIImage imageNamed:@"NavBar-iPhone.png"] forBarMetrics:UIBarMetricsDefault];
    //[navBar setBackgroundImage:[UIImage imageNamed:@"NavBar-iPhone.png"] forBarMetrics:UIBarMetricsLandscapePhone];
    
    //--------------------//
    // The old background //
    //--------------------//
    //CGRect backgroundRect = CGRectMake(0.0, 0.0, self.view.bounds.size.width, 64.0);
    //UIImage *backgroundImage = createImageWithRect(backgroundRect, [UIColor whiteColor]);
    
    //----------------//
    // CR Change Logo //
    //----------------//
    UIImage *backgroundImage = radialGradientImage(CGSizeMake(self.view.bounds.size.width, 64.0), 25.0, 47.0, 124.0,
                                                   0.0, 91.0, 170.0, CGPointMake(1/4, 1/4), 2.0);
    
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    //[navBar setBackgroundImage:[UIImage imageNamed:@"NavBar-iPhone.png"] forBarMetrics:UIBarMetricsLandscapePhone];
    
    navBar.layer.shadowColor = [[UIColor blackColor] CGColor];
    navBar.layer.shadowOffset = CGSizeMake(1.0, 1.0);
    navBar.layer.shadowOpacity = 0.40;
    
    return customizedNavController;
}

- (void)createCompleteButton:(UIViewController*)paneViewController {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 44)]; //84
    view.backgroundColor = [UIColor clearColor];
    
    CGFloat padding = 5.0;
    
    UniChar menu = 0x005C;
    NSString *menuString = [NSString stringWithCharacters:&menu length:1];
    
    UIButton *menuButton = createButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonWidth, kDefaultButtonHeight),
                                                 menuString,
                                                 [UIFont fontWithName:kDefaultFontKSAN size:20.0],
                                                 kDefaultBaseColor,
                                                 kDefaultPinkColor,
                                                 kDefaultDarkPinkColor);
    [menuButton addTarget:self
                   action:@selector(performMenu:)
         forControlEvents:UIControlEventTouchDown];
    [view addSubview:menuButton];
    
    UIButton *accountButton = createButtonWithFrame(CGRectMake(menuButton.frame.origin.x + menuButton.frame.size.width + padding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonWidth, kDefaultButtonHeight),
                                                    @"A",
                                                    [UIFont fontWithName:kDefaultFontKSAN size:20.0],
                                                    kDefaultPurpleColor,
                                                    kDefaultButtonGrayColor,
                                                    kDefaultFontLightGrayColor);
    [accountButton addTarget:self
                      action:@selector(performAccount:)
            forControlEvents:UIControlEventTouchDown];
    [view addSubview:accountButton];
    
    accountFrame = accountButton.frame;
    
    UIButton *notifButton = createButtonWithFrame(CGRectMake(accountButton.frame.origin.x + accountButton.frame.size.width + padding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonWidth, kDefaultButtonHeight),
                                                  @":",
                                                  [UIFont fontWithName:kDefaultFontKSAN size:20.0],
                                                  kDefaultPurpleColor,
                                                  kDefaultButtonGrayColor,
                                                  kDefaultFontLightGrayColor);
    [notifButton addTarget:self
                    action:@selector(performNotification:)
          forControlEvents:UIControlEventTouchDown];
    [view addSubview:notifButton];
    
    UIButton *backButton = createButtonWithFrame(CGRectMake(notifButton.frame.origin.x + notifButton.frame.size.width + padding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonWidth, kDefaultButtonHeight),
                                                 @"f",
                                                 [UIFont fontWithName:kDefaultFontKSAN size:20.0],
                                                 kDefaultPurpleColor,
                                                 kDefaultButtonGrayColor,
                                                 kDefaultFontLightGrayColor);
    [backButton addTarget:self
                   action:@selector(performBack:)
         forControlEvents:UIControlEventTouchDown];
    [view addSubview:backButton];
    
    paneViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:view];
}

- (void)performMenu:(id)sender {
    [self.navigationPaneViewController setPaneState:MSNavigationPaneStateOpen animated:YES];
}

- (void)performAccount:(id)sender {
    
    if(!navPopover) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 220, 110)]; //84
        view.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        CGFloat padding = 10.0;
        CGFloat width = view.frame.size.width - (padding*2);
        
        UIButton *updateProfilebutton = createButtonWithIconAndArrow(CGRectMake(padding, padding, width, kDefaultButtonHeight),
                                                                     @"Q",
                                                                     [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                                                     kDefaultPurpleColor,
                                                                     @"UPDATE PROFILE",
                                                                     [UIFont fontWithName:kDefaultFont size:kDefaultFontSize],
                                                                     kDefaultPurpleColor,
                                                                     @"d",
                                                                     [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                                                     kDefaultPurpleColor,
                                                                     kDefaultButtonGrayColor,
                                                                     kDefaultFontLightGrayColor);
        
        [updateProfilebutton addTarget:self
                                action:@selector(performUpdateProfile:)
                      forControlEvents:UIControlEventTouchDown];
        
        [view addSubview:updateProfilebutton];
        
        UIButton *changePasswordButton = createButtonWithIconAndArrow(CGRectMake(padding, updateProfilebutton.frame.origin.y + updateProfilebutton.frame.size.height + padding, width, kDefaultButtonHeight),
                                                                      @"U",
                                                                      [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                                                      kDefaultPurpleColor,
                                                                      @"CHANGE PASSWORD",
                                                                      [UIFont fontWithName:kDefaultFont size:kDefaultFontSize],
                                                                      kDefaultPurpleColor,
                                                                      @"d",
                                                                      [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                                                      kDefaultPurpleColor,
                                                                      kDefaultButtonGrayColor,
                                                                      kDefaultFontLightGrayColor);
        [changePasswordButton addTarget:self
                                 action:@selector(performChangePassword:)
                       forControlEvents:UIControlEventTouchDown];
        
        [view addSubview:changePasswordButton];
        
        UIViewController *viewCon = [[UIViewController alloc] init];
        viewCon.view = view;
        viewCon.contentSizeForViewInPopover = view.frame.size;
        
        navPopover = [[WEPopoverController alloc] initWithContentViewController:viewCon];
        [navPopover setDelegate:self];
    }
    
    if([navPopover isPopoverVisible]) {
        [navPopover dismissPopoverAnimated:YES];
        [navPopover setDelegate:nil];
        navPopover = nil;
    }
    else {
        [navPopover presentPopoverFromRect:CGRectMake(accountFrame.origin.x + 7, 0, 50, 57)
                                    inView:self.navigationController.view
                  permittedArrowDirections:UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown
                                  animated:YES];
    }
}

- (void)performNotification:(id)sender {
    NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
    [self.navigationController pushViewController:notificationViewController animated:YES];
    notificationViewController = nil;
    [notificationViewController release];
}

- (void)performBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - PopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController {
    //NSLog(@"Did dismiss");
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    //NSLog(@"Should dismiss");
    return YES;
}

@end
