//
//  EncryptDecrypt.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/10/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Base64.h"
#include <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>


@interface EncryptDecrypt : NSObject {
    
}

+ (NSString*)removeNonASCIIChar:(NSString*)inputStr;
+ (NSData*)modifiedBase64Decode:(NSString*)inputText;
+ (NSString*)modifiedBase64Encode:(NSData*)inputData;
+ (NSString*)doCipher:(NSString*)plainText action:(CCOperation)encryptOrDecrypt withKey:(NSString*)saltKey;
+ (NSString*)doCipherForiOS7:(NSString*)plainText action:(CCOperation)encryptOrDecrypt withKey:(NSString*)saltKey;
+ (NSString*)doCipherAES:(NSString*)plainText action:(CCOperation)encryptOrDecrypt withKey:(NSString*)saltKey;
+ (NSString*)doCipherAESTest:(NSString*)plainText action:(CCOperation)encryptOrDecrypt withKey:(NSString*)saltKey;
+ (NSString*)returnMD5Hash:(NSString*)concat;

@end
