//
//  EncryptDecrypt.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/10/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "EncryptDecrypt.h"
//#import "Macros.h"


@implementation EncryptDecrypt

+ (NSData*)modifiedBase64Decode:(NSString*)inputText {
    //NSLog(@"input Text = %@",inputText);
    NSArray *search = [NSArray arrayWithObjects:@"-", @"_", nil];
    NSArray *replace = [NSArray arrayWithObjects:@"+", @"/", nil];
    for (int i=0; i<2; i++) {
        inputText = [inputText stringByReplacingOccurrencesOfString:[search objectAtIndex:i] withString:[replace objectAtIndex:i]];
    }
    //NSLog(@"replaced = %@",inputText);
    [Base64 initialize];
    NSData *b64DecData = [Base64 decode:inputText];
    return b64DecData;
}

+ (NSString*)modifiedBase64Encode:(NSData*)inputData {
    [Base64 initialize];
    NSString *b64EncStr = [Base64 encode:inputData];
    
    NSArray *search = [NSArray arrayWithObjects:@"+", @"/", nil];
    NSArray *replace = [NSArray arrayWithObjects:@"-", @"_", nil];
    NSString *result = b64EncStr;
    for (int i=0; i<2; i++) {
        result = [result stringByReplacingOccurrencesOfString:[search objectAtIndex:i] withString:[replace objectAtIndex:i]];
    }
    result = [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    return result;
}

+ (NSString*)removeNonASCIIChar:(NSString *)inputStr {
    NSString *output;
    
    NSMutableString *asciiCharacters = [NSMutableString string];
    for (NSInteger i = 32; i < 127; i++)  {
        [asciiCharacters appendFormat:@"%c", i];
    }
    
    NSCharacterSet *nonAsciiCharacterSet = [[NSCharacterSet characterSetWithCharactersInString:asciiCharacters] invertedSet];
    
    output = [[inputStr componentsSeparatedByCharactersInSet:nonAsciiCharacterSet] componentsJoinedByString:@""];
    
    return output;
}

+ (NSString*)doCipher:(NSString*)plainText action:(CCOperation)encryptOrDecrypt withKey:(NSString*)saltKey {
    const void *vplainText;
    size_t plainTextBufferSize;
    
    //[plainText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (encryptOrDecrypt == kCCDecrypt) {
        //NSData *EncryptData = [[NSData alloc] initWithBase64EncodedString:plainText];
        NSData *EncryptData = [self modifiedBase64Decode:plainText];
        plainTextBufferSize = [EncryptData length];
        vplainText = [EncryptData bytes];
    }
    else {
        
        //
        int plainTextLength = [plainText length];
        int divideResult = plainTextLength/8;
        //int modResult = plainTextLength % 8;
        //int addChar = 8 - modResult;
        
        char bytes[] = "";
        NSData * data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
        NSString *strFromData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        plainText = [plainText stringByPaddingToLength:(divideResult+1)*8 withString:strFromData startingAtIndex:0];
        //NSLog(@"plain = %@",plainText);
        //NSLog(@"plain length = %i",[plainText length]);
        //
        
        //plainTextBufferSize = [plainText length];
        //vplainText = (const void *)[plainText UTF8String];
        
        NSData *plainTextData = [plainText dataUsingEncoding: NSUTF8StringEncoding]; 
        plainTextBufferSize = [plainTextData length]; 
        vplainText = [plainTextData bytes];
        [strFromData release];
    }
    
    CCCryptorStatus ccStatus;
    uint8_t *bufferPtr = NULL;
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    // uint8_t ivkCCBlockSize3DES;
    
    bufferPtrSize = (plainTextBufferSize + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    bufferPtr = malloc( bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x0, bufferPtrSize);
    // memset((void *) iv, 0x0, (size_t) sizeof(iv));
    
    //NSString *key = @"123456789012345678901234";
    NSString *key = saltKey;
    //NSString *initVec = @"init Vec";
    const void *vkey = (const void *)[key UTF8String];
    //const void *vinitVec = (const void *)[initVec UTF8String];
    
    ccStatus = CCCrypt(encryptOrDecrypt,
                       kCCAlgorithm3DES,
                       kCCOptionECBMode,
                       vkey, //"123456789012345678901234", //key
                       kCCKeySize3DES,
                       nil, //"init Vec", //iv,
                       vplainText, //"Your Name", //plainText,
                       plainTextBufferSize,
                       (void *)bufferPtr,
                       bufferPtrSize,
                       &movedBytes);
    
    //if (ccStatus == kCCSuccess) //NSLog(@"SUCCESS");
    /*else*/ if (ccStatus == kCCParamError) return @"PARAM ERROR";
    else if (ccStatus == kCCBufferTooSmall) return @"BUFFER TOO SMALL";
    else if (ccStatus == kCCMemoryFailure) return @"MEMORY FAILURE";
    else if (ccStatus == kCCAlignmentError) return @"ALIGNMENT";
    else if (ccStatus == kCCDecodeError) return @"DECODE ERROR";
    else if (ccStatus == kCCUnimplemented) return @"UNIMPLEMENTED";
    
    NSString *result;
    
    if (encryptOrDecrypt == kCCDecrypt) {
        result = [[[NSString alloc] initWithData:[NSData dataWithBytes:(const void *)bufferPtr length:(NSUInteger)movedBytes] encoding:NSASCIIStringEncoding] autorelease];
        result = [self removeNonASCIIChar:result];
    }
    else {
        NSData *myData = [NSData dataWithBytes:(const void *)bufferPtr length:(NSUInteger)movedBytes];
        result = [self modifiedBase64Encode:myData];
        //result = [myData base64EncodingWithLineLength:movedBytes];
    }
    
    return result;
}

+ (NSString*)doCipherForiOS7:(NSString*)plainText action:(CCOperation)encryptOrDecrypt withKey:(NSString*)saltKey {
    const void *vplainText;
    size_t plainTextBufferSize;
    
    //[plainText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (encryptOrDecrypt == kCCDecrypt) {
        //NSData *EncryptData = [[NSData alloc] initWithBase64EncodedString:plainText];
        NSData *EncryptData = [self modifiedBase64Decode:plainText];
        plainTextBufferSize = [EncryptData length];
        vplainText = [EncryptData bytes];
    }
    else {
        
        //
        int plainTextLength = [plainText length];
        int divideResult = plainTextLength/8;
        //int modResult = plainTextLength % 8;
        //int addChar = 8 - modResult;
        
        char bytes[] = "";
        NSData * data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
        NSString *strFromData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        plainText = [plainText stringByPaddingToLength:(divideResult+1)*8 withString:strFromData startingAtIndex:0];
        //NSLog(@"plain = %@",plainText);
        //NSLog(@"plain length = %i",[plainText length]);
        //
        
        //plainTextBufferSize = [plainText length];
        //vplainText = (const void *)[plainText UTF8String];
        
        NSData *plainTextData = [plainText dataUsingEncoding: NSUTF8StringEncoding];
        plainTextBufferSize = [plainTextData length];
        vplainText = [plainTextData bytes];
        [strFromData release];
    }
    
    CCCryptorStatus ccStatus;
    uint8_t *bufferPtr = NULL;
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    // uint8_t ivkCCBlockSize3DES;
    
    bufferPtrSize = (plainTextBufferSize + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    bufferPtr = malloc( bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x0, bufferPtrSize);
    // memset((void *) iv, 0x0, (size_t) sizeof(iv));
    
    
    NSString *key = saltKey;
    
    
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    char keyPtr[kCCKeySize3DES+1]; // room for terminator (unused)
    bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
    
    BOOL patchNeeded = ([key length] > kCCKeySize3DES);
    if (patchNeeded) {
        //NSLog(@"--> NEED PATCH <--");
        key = [key substringToIndex:kCCKeySize3DES]; // Ensure that the key isn't longer than what's needed (kCCKeySizeAES256)
    }
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    if (patchNeeded) {
        //NSLog(@"--> NEED PATCH <--");
        keyPtr[0] = '\0';  // Previous iOS version than iOS7 set the first char to '\0' if the key was longer than kCCKeySizeAES256
    }
    
    //const void *vkey = (const void *)[key UTF8String];
    
    
    ccStatus = CCCrypt(encryptOrDecrypt,
                       kCCAlgorithm3DES,
                       kCCOptionECBMode,
                       keyPtr, //"123456789012345678901234", //key
                       kCCKeySize3DES,
                       NULL, //"init Vec", //iv,
                       vplainText, //"Your Name", //plainText,
                       plainTextBufferSize,
                       (void *)bufferPtr,
                       bufferPtrSize,
                       &movedBytes);
    
    //if (ccStatus == kCCSuccess) //NSLog(@"SUCCESS");
    /*else*/ if (ccStatus == kCCParamError) return @"PARAM ERROR";
    else if (ccStatus == kCCBufferTooSmall) return @"BUFFER TOO SMALL";
    else if (ccStatus == kCCMemoryFailure) return @"MEMORY FAILURE";
    else if (ccStatus == kCCAlignmentError) return @"ALIGNMENT";
    else if (ccStatus == kCCDecodeError) return @"DECODE ERROR";
    else if (ccStatus == kCCUnimplemented) return @"UNIMPLEMENTED";
    
    NSString *result;
    
    if (encryptOrDecrypt == kCCDecrypt) {
        result = [[[NSString alloc] initWithData:[NSData dataWithBytes:(const void *)bufferPtr length:(NSUInteger)movedBytes] encoding:NSASCIIStringEncoding] autorelease];
        result = [self removeNonASCIIChar:result];
    }
    else {
        NSData *myData = [NSData dataWithBytes:(const void *)bufferPtr length:(NSUInteger)movedBytes];
        result = [self modifiedBase64Encode:myData];
        //result = [myData base64EncodingWithLineLength:movedBytes];
    }
    
    return result;
}

+ (NSString*)doCipherAES:(NSString*)plainText action:(CCOperation)encryptOrDecrypt withKey:(NSString*)saltKey {
    const void *vplainText;
    NSUInteger dataLength;
    
    // for Decrypt
    if (encryptOrDecrypt == kCCDecrypt) {
        NSData *encryptData = [self modifiedBase64Decode:plainText];
        vplainText = [encryptData bytes];
        dataLength = [encryptData length];
    }
    // for Encrypt
    else {
        int plainTextLength = [plainText length];
        int divideResult = plainTextLength/8;
        
        char bytes[] = "";
        NSData * data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
        NSString *strFromData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        plainText = [plainText stringByPaddingToLength:(divideResult+1)*8 withString:strFromData startingAtIndex:0];
        
        NSData *plainTextData = [plainText dataUsingEncoding:NSUTF8StringEncoding];
        dataLength = [plainTextData length];
        vplainText = [plainTextData bytes];
        [strFromData release];
    }
    
    CCCryptorStatus ccStatus;
    
    NSString *key = saltKey;
    
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    char keyPtr[kCCKeySizeAES256+1]; // room for terminator (unused)
    bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
    
    BOOL patchNeeded = ([key length] > kCCKeySizeAES256);
    if (patchNeeded) {
        key = [key substringToIndex:kCCKeySizeAES256]; // Ensure that the key isn't longer than what's needed (kCCKeySizeAES256)
    }
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    if (patchNeeded) {
        keyPtr[0] = '\0';  // Previous iOS version than iOS7 set the first char to '\0' if the key was longer than kCCKeySizeAES256
    }
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    size_t numBytesEncrypted = 0;
    
    ccStatus = CCCrypt(encryptOrDecrypt,
                       kCCAlgorithmAES128,
                       kCCOptionPKCS7Padding,
                       keyPtr,
                       kCCKeySizeAES256,
                       NULL,
                       vplainText,
                       dataLength,
                       buffer,
                       bufferSize,
                       &numBytesEncrypted);
    
    if (ccStatus == kCCParamError) return @"PARAM ERROR";
    else if (ccStatus == kCCBufferTooSmall) return @"BUFFER TOO SMALL";
    else if (ccStatus == kCCMemoryFailure) return @"MEMORY FAILURE";
    else if (ccStatus == kCCAlignmentError) return @"ALIGNMENT";
    else if (ccStatus == kCCDecodeError) return @"DECODE ERROR";
    else if (ccStatus == kCCUnimplemented) return @"UNIMPLEMENTED";
    
    NSString *result;
    
    // for Decrypt
    if (encryptOrDecrypt == kCCDecrypt) {
        result = [[[NSString alloc] initWithData:[NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted] encoding:NSASCIIStringEncoding] autorelease];
        result = [self removeNonASCIIChar:result];
    }
    // for Encrypt
    else {
        NSData *myData = [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
        result = [self modifiedBase64Encode:myData];
    }
    
    return result;
}

+ (NSString*)doCipherAESTest:(NSString*)plainText action:(CCOperation)encryptOrDecrypt withKey:(NSString*)saltKey {
    
    const void *vplainText;
    NSUInteger dataLength;
    
    // for Decrypt
    if (encryptOrDecrypt == kCCDecrypt) {
        //NSData *encryptData = [self modifiedBase64Decode:plainText];
        NSData *encryptData = [plainText dataUsingEncoding:NSUTF8StringEncoding];
        vplainText = [encryptData bytes];
        dataLength = [encryptData length];
    }
    // for Encrypt
    else {
        /*
        int plainTextLength = [plainText length];
        int divideResult = plainTextLength/8;
        
        char bytes[] = "";
        NSData * data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
        NSString *strFromData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        plainText = [plainText stringByPaddingToLength:(divideResult+1)*8 withString:strFromData startingAtIndex:0];*/
        //NSLog(@"plainText = %@",plainText);
        NSData *plainTextData = [plainText dataUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"plainTextData = %@",plainTextData);
        vplainText = [plainTextData bytes];
        dataLength = [plainTextData length];
        //NSLog(@"dataLength = %i",dataLength);
        
        //[strFromData release];
    }
    
    CCCryptorStatus ccStatus;
    
    NSString *key = saltKey;
    
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    char keyPtr[kCCKeySizeAES256+1]; // room for terminator (unused)
    bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
    
    BOOL patchNeeded = ([key length] > kCCKeySizeAES256);
    if (patchNeeded) {
        //NSLog(@"Need Patch 1");
        key = [key substringToIndex:kCCKeySizeAES256]; // Ensure that the key isn't longer than what's needed (kCCKeySizeAES256)
    }
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    if (patchNeeded) {
        //NSLog(@"Need Patch 2");
        keyPtr[0] = '\0';  // Previous iOS version than iOS7 set the first char to '\0' if the key was longer than kCCKeySizeAES256
    }
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    size_t numBytesEncrypted = 0;
    
    ccStatus = CCCrypt(encryptOrDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding, keyPtr, kCCKeySizeAES256,
                       NULL,
                       vplainText, dataLength,
                       buffer, bufferSize,
                       &numBytesEncrypted);
    
    if (ccStatus == kCCParamError) return @"PARAM ERROR";
    else if (ccStatus == kCCBufferTooSmall) {
        //return @"BUFFER TOO SMALL";
        //NSLog(@"BUFFER TOO SMALL");
        
        size_t newsSize = numBytesEncrypted;
        void *buffer = malloc(newsSize);
        
//        void *dynOutBuffer = malloc(newsSize);
//        memset(dynOutBuffer, 0, newsSize);
        
        numBytesEncrypted = 0;
        
        ccStatus = CCCrypt(encryptOrDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding, key, kCCKeySizeAES256,
                           NULL,
                           vplainText, dataLength,
                           buffer, newsSize,
                           &numBytesEncrypted);
        
        if (ccStatus == kCCSuccess) {
            //NSLog(@"SUKSES");
        }
        else {
            //NSLog(@"GAK SUKSES");
        }
    }
    else if (ccStatus == kCCMemoryFailure) return @"MEMORY FAILURE";
    else if (ccStatus == kCCAlignmentError) return @"ALIGNMENT";
    else if (ccStatus == kCCDecodeError) return @"DECODE ERROR";
    else if (ccStatus == kCCUnimplemented) return @"UNIMPLEMENTED";
    
    NSString *result;
    
    // for Decrypt
    if (encryptOrDecrypt == kCCDecrypt) {
        result = [[[NSString alloc] initWithData:[NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted] encoding:NSASCIIStringEncoding] autorelease];
        //result = [self removeNonASCIIChar:result];
    }
    // for Encrypt
    else {
        NSData *myData = [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
        //result = [self modifiedBase64Encode:myData];
        result =  [[NSString alloc] initWithData:myData encoding:NSUTF8StringEncoding];
    }
    
    return result;
    free(buffer); //free the buffer;
}

+ (NSString *)returnMD5Hash:(NSString*)concat {
    const char *concat_str = [concat UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(concat_str, strlen(concat_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < 16; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
}

@end
