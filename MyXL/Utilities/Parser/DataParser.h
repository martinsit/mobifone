//
//  DataParser.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/6/12.
//  Copyright (c) 2012 Easy Touch Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DataParser;

@protocol DataParserDelegate <NSObject>
@optional
- (void)parseProfileDone:(BOOL)success withReason:(NSString*)reason;
- (void)parseMenuDone:(BOOL)success withReason:(NSString*)reason;
- (void)parsePromoDone:(BOOL)success withReason:(NSString*)reason;

- (void)parseDataDone:(BOOL)success withReason:(NSString*)reason;
- (void)parseDataDone:(BOOL)success withReason:(NSString*)reason withErrorCode:(NSString*)errorCode;

//- (void)parseAdsDone:(BOOL)success withReason:(NSString*)reason;
- (void)parseAdsDone:(NSDictionary*)result;

// TODO : Update Hygiene Defectc
-(void)parseRegisterDeviceTokenDone:(NSDictionary*)result;

// TODO : V1.9
-(void)parseGetUnreadNotificationDone:(NSDictionary *)result;
@end

@interface DataParser : NSObject {
    id <DataParserDelegate> _delegate;
}

@property (assign) id <DataParserDelegate> delegate;

- (void)parseProfileInfoData:(NSString*)data;

- (void)parseGetProfileData:(NSString*)data;

- (void)parseSignOutData:(NSString*)data;

- (void)parseRegisterData:(NSString*)data;

- (void)parseActivationData:(NSString*)data;

- (void)parseResetPasswordData:(NSString*)data;

- (void)parseResendPinData:(NSString*)data;

- (void)parseChangePasswordData:(NSString*)data;

- (void)parseRegisterDeviceTokenData:(NSString*)data;

- (void)parseGetMenuData:(NSString*)data;

- (void)parseUpdateProfileData:(NSString*)data;

- (void)parseCheckBalanceData:(NSString*)data;

- (void)parseTopUpVoucherData:(NSString*)data;

- (void)parseCheckUsageData:(NSString*)data;

- (void)parseCheckUsageXLData:(NSString*)data;

- (void)parseRecommendedPackageData:(NSString*)data;

- (void)parseUpgradePackageData:(NSString*)data;

- (void)parseSurveyData:(NSString*)data;

- (void)parseLastTransactionData:(NSString*)data;

- (void)parsePromoData:(NSString*)data;

// TODO : V1.9
- (void)parseGetUnreadNotificationData:(NSString*)data;

//- (void)parseNotificationData:(NSString*)data;
- (void)parseNotificationData:(NSString*)data isDetailNotif:(BOOL)isDetail;

- (void)parseNotificationDeleteData:(NSString*)data;

- (void)parseReplyNotificationData:(NSString*)data;

- (void)parseAdsData:(NSString*)data;

- (void)parseHotOfferBannerData:(NSString*)data;

- (void)parseBalanceItemsData:(NSString*)data;

- (void)parseBalanceTransferData:(NSString*)data;

- (void)parseExtendValidityData:(NSString*)data;

- (void)parsePaymentParamData:(NSString*)data;

- (void)parsePaymentHelpData:(NSString*)data;

- (void)parsePaymentDebitData:(NSString*)data;

- (void)parseGiftPackageData:(NSString*)data;

- (void)parseBuyPackageData:(NSString*)data;

- (void)parseContactUsData:(NSString*)data;

- (void)parseRoamingData:(NSString*)data;

- (void)parseXLStarData:(NSString*)data;

- (void)parsePUKData:(NSString*)data;

- (void)parseDeviceSettingData:(NSString*)data;

- (void)parseShopLocationData:(NSString*)data;

- (void)parseFAQData:(NSString*)data;

- (void)parseXLTunaiBalanceData:(NSString*)data;

- (void)parseXLTunaiReloadData:(NSString*)data;

- (void)parseChangeLanguageData:(NSString*)data;

- (void)parseFacebookData:(NSString*)data;

- (void)parseTermAndConditionData:(NSString*)data;

- (void)parseUserGuideData:(NSString*)data;

- (void)parseTrxHistoryData:(NSString*)data;

- (void)parseCategoryData:(NSString*)data;

- (void)parseProductData:(NSString*)data;

- (void)parseQuotaMeterData:(NSString*)data;

- (void)parseRecommendedPackageXLData:(NSString*)data;

- (void)parseShareQuotaData:(NSString*)data;

- (void)parseQuotaCalculatorData:(NSString*)data;

// TODO : NEPTUNE
-(void)parseMappingPaketSukaSukaData:(NSString *)data;
-(void)parseMatrixPaketSukaSukaData:(NSString *)data;
-(void)parsePageSukaSukaData:(NSString *)data;
-(void)parseCheckQuotaSukaSukaData:(NSString *)data;
-(void)parseRenameSukaSukaData:(NSString *)data;

// CR 4g USIM
- (void)parseInfo4GUsimData:(NSString*)data;

// Replace 4g USIM
- (void)parseReplace4gUsimData:(NSString*)data;
- (void)parseRateComment4gUsimData:(NSString*)data;

// TODO : V1.9
-(void)parseVoucherPromoData:(NSString *)data;

-(void)parseCheckPointPromo:(NSString *)data;

-(void)parseWinnerListXtraPromo:(NSString *)data;
-(void)parseCheckBonusXtraPromo:(NSString *)data;
-(void)parseGetBonusXtraPromo:(NSString *)data;

// 4G POWERPACK
-(void)parseFourGPowerPack:(NSString *)data;
-(void)parseFourGPackageStatus:(NSString *)data;

// Quota Improvement
- (void)parseQuotaImprovementData:(NSString*)data;

@end
