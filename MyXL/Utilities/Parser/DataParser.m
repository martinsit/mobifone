//
//  DataParser.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/6/12.
//  Copyright (c) 2012 Easy Touch Group. All rights reserved.
//

#import "DataParser.h"
#import "JSON.h"
#import "Constant.h"
#import "DataManager.h"
#import "EncryptDecrypt.h"
#import "Language.h"
#import "AXISnetCommon.h"

#import "ProfileModel.h"
#import "MenuModel.h"
#import "CheckBalanceModel.h"
#import "BonusInfoModel.h"
#import "TopUpVoucherModel.h"
#import "ExtendValidityModel.h"
#import "BuyPackageModel.h"
#import "CheckUsageModel.h"
#import "SurveyModel.h"
#import "LabelValueModel.h"
#import "TransactionHistoryModel.h"
#import "PromoModel.h"
#import "NotifModel.h"
#import "NotifParamModel.h"
#import "AdsModel.h"
#import "BalanceTransferModel.h"
#import "BankModel.h"
#import "PaymentDebitModel.h"
#import "ContactUsModel.h"
#import "PUKModel.h"
#import "DeviceSettingModel.h"
#import "ShopLocationModel.h"
#import "FAQResultModel.h"
#import "ProductModel.h"

#import "CheckUsageXLModel.h"
#import "PackageAllowanceModel.h"
#import "RoamingModel.h"
#import "QuotaMeterModel.h"
#import "QuotaCalculatorModel.h"
#import "InternetUsageModel.h"

#import "LogoModel.h"

#import "ThemesModel.h"
#import "MappingSukaSukaModel.h"
#import "PackageSukaMatrixModel.h"
#import "PageSukaModel.h"
#import "RenamePackageModel.h"
#import "RenameSukaSukaModel.h"

// TODO : HYGIENE
#import "LoginWordingModel.h"
#import "LoginWordingLangModel.h"

// TODO : V1.9
#import "UpdateVersionModel.h"

#import "CheckPointModel.h"
#import "WinnerListModel.h"
#import "XtraCheckBonusModel.h"

#import "FourGPackageModel.h"

// TODO : v1.9.5
#import "QuotaImprovementModel.h"

@implementation DataParser

@synthesize delegate = _delegate;

#pragma mark - SSO

- (NSDictionary *)removeNullResponse:(NSMutableDictionary *)dictionaryResponse
{
    NSMutableDictionary *dictionary = dictionaryResponse;
    NSString *nullString = @"";
    for (NSString *key in [dictionary allKeys]) {
        id value = dictionary[key];
        
        if ([value isKindOfClass:[NSDictionary class]]) {
            
            dictionary[key] = [self removeNullResponse:(NSMutableDictionary*)value];
            
        }else if([value isKindOfClass:[NSArray class]]){
            
            NSMutableArray *newArray = [value mutableCopy];
            for (int i = 0; i < [value count]; ++i) {
                
                id value2 = [value objectAtIndex:i];
                
                if ([value2 isKindOfClass:[NSDictionary class]]) {
                    newArray[i] = [self removeNullResponse:(NSMutableDictionary*)value2];
                }
                else if ([value2 isKindOfClass:[NSNull class]]){
                    newArray[i] = nullString;
                }
            }
            dictionary[key] = newArray;
        }else if ([value isKindOfClass:[NSNull class]]){
            dictionary[key] = nullString;
        }
    }
    
    return [dictionary copy];
}


- (void)parseFacebookData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *theDict = responseJSON;
    NSString *status = [theDict objectForKey:@"code"];
    BOOL isSuccess;
    NSString *reason = @"";
    NSString *errorCode = @"";
    if ([status isEqualToString:@"200"]) {
        isSuccess = YES;
    }
    else {
        reason = [theDict valueForKey:@"message"];
        errorCode = status;
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - PROFILE INFO

- (void)parseProfileInfoData:(NSString*)data {
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    id responseJSON = [data JSONValue];
    NSDictionary *profileDict = responseJSON;
    NSString *status = [profileDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    NSString *errorCode = @"";
    
    if ([status isEqualToString:@"1"]) {
        
        NSDictionary *data = [profileDict objectForKey:@"data"];
        //ProfileModel *profileModel  = [[ProfileModel alloc] init];
        ProfileModel *profileModel  = sharedData.profileData;
        profileModel.profileId      = [data objectForKey:@"id"];
        profileModel.name           = [data objectForKey:@"name"];
        NSString *fullName          = [data objectForKey:@"full_name"];
        if (!!fullName && ![fullName isEqual:[NSNull null]] && ![fullName isEqual:@"{}"]) {
            profileModel.fullName = fullName;
        }
        else {
            profileModel.fullName = @"";
        }
        
        NSString *msisdn = [data objectForKey:@"msisdn"];
        
        profileModel.msisdn     = msisdn;
        profileModel.email      = [data objectForKey:@"email"];
        profileModel.language   = [data objectForKey:@"language"];
        
        if ([profileModel.language length] <= 0) {
            profileModel.language = @"id";
        }
        
        [Language setLanguage:profileModel.language];
        
        profileModel.location   = [data objectForKey:@"location"];
        profileModel.regional   = [data objectForKey:@"regional"];
        profileModel.status     = [data objectForKey:@"status"];
        
        NSString *address = [data objectForKey:@"address"];
        if (!!address && ![address isEqual:[NSNull null]] && ![fullName isEqual:@"{}"]) {
            profileModel.address = address;
        }
        else {
            profileModel.address = @"";
        }
        
        NSString *address2 = [data objectForKey:@"address2"];
        if (!!address2 && ![address2 isEqual:[NSNull null]] && ![fullName isEqual:@"{}"]) {
            profileModel.address2 = address2;
        }
        else {
            profileModel.address2 = @"";
        }
        
        NSString *address3 = [data objectForKey:@"address3"];
        if (!!address2 && ![address2 isEqual:[NSNull null]] && ![fullName isEqual:@"{}"]) {
            profileModel.address3 = address3;
        }
        else {
            profileModel.address3 = @"";
        }
        
        profileModel.city           = [data objectForKey:@"city"];
        profileModel.postcode       = [data objectForKey:@"postcode"];
        profileModel.confirmEmail   = [data objectForKey:@"confirm_email"];
        profileModel.emailNotify    = [data objectForKey:@"email_notify"];
        profileModel.pushNotify     = [data objectForKey:@"push_notify"];
        profileModel.gender         = [data objectForKey:@"gender"];
        profileModel.birth          = [data objectForKey:@"birth"];
        profileModel.token          = [data objectForKey:@"token"];
        
        id autologin = [data objectForKey:@"autologin"];
        if (!!autologin && ![autologin isEqual:[NSNull null]]) {
            NSString *autologinStr = autologin;
            profileModel.autologin = autologinStr;
        }
        else {
            profileModel.autologin = @"";
        }
        
        profileModel.device         = [data objectForKey:@"device"];
        profileModel.imei           = [data objectForKey:@"imei"];
        profileModel.tac            = [data objectForKey:@"tac"];
        profileModel.subscriberType = [data objectForKey:@"subscriber_type"];
        profileModel.balanceValue   = [data objectForKey:@"balance_value"];
        profileModel.activeEndDate  = [data objectForKey:@"active_end_date"];
        profileModel.graceEndDate   = [data objectForKey:@"grace_end_date"];
        
        NSString *msisdnType        = [data objectForKey:@"msisdn_type"];
        profileModel.msisdnType     = [msisdnType intValue];
        
        NSString *idCardTypeTmp = [data objectForKey:@"idcard_type"];
        if (!!idCardTypeTmp && ![idCardTypeTmp isEqual:[NSNull null]]) {
            profileModel.idCardType = idCardTypeTmp;
        }
        else {
            profileModel.idCardType = @"";
        }
        
        NSString *idCardNumberTmp = [data objectForKey:@"idcard_number"];
        if (!!idCardNumberTmp && ![idCardNumberTmp isEqual:[NSNull null]] && ![fullName isEqual:@"{}"]) {
            profileModel.idCardNumber = idCardNumberTmp;
        }
        else {
            profileModel.idCardNumber = @"";
        }
        
        NSString *placeBirth = [data objectForKey:@"place_birth"];
        if (!!placeBirth && ![placeBirth isEqual:[NSNull null]] && ![fullName isEqual:@"{}"]) {
            profileModel.placeBirth = placeBirth;
        }
        else {
            profileModel.placeBirth = @"";
        }
        
        NSString *otherPhone = [data objectForKey:@"other_phone"];
        if (!!otherPhone && ![otherPhone isEqual:[NSNull null]]) {
            profileModel.otherPhone = otherPhone;
        }
        else {
            profileModel.otherPhone = @"";
        }
        
        //sharedData.profileData = profileModel;
        sharedData.currentVersion = [data objectForKey:@"version"];
        //[profileModel release];
        
        isSuccess = YES;
    }
    else {
        reason = [profileDict valueForKey:@"err_desc"];
        errorCode = status;
        isSuccess = NO;
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.currentVersion = [profileDict valueForKeyPath:@"data.version"];
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:withErrorCode:)])
        [_delegate parseDataDone:isSuccess withReason:reason withErrorCode:errorCode];
}

#pragma mark - GET PROFILE

- (void)parseGetProfileData:(NSString*)data
{
    @try
    {
        if([data length] > 0)
        {
            DataManager *sharedData = [DataManager sharedInstance];
            id responseJSON = [data JSONValue];
            NSDictionary *profileDict = responseJSON;
            NSString *status = [profileDict objectForKey:@"code"];
            //NSLog(@"status = %@",status);
            BOOL isSuccess;
            NSString *reason = @"";
            NSString *errorCode = status;
            if ([status isEqualToString:@"200"]) {
                
                NSDictionary *data = [profileDict objectForKey:@"data"];
                ProfileModel *profileModel = [[ProfileModel alloc] init];
                profileModel.profileId = [data objectForKey:@"id"];
                profileModel.name = [data objectForKey:@"name"];
                profileModel.fullName = [data objectForKey:@"full_name"];
                
                // 4G STATUS
                if([data objectForKey:@"fourg_powerpack"])
                {
                    NSDictionary *totalDict = [data objectForKey:@"fourg_powerpack"];
                    if ([totalDict count] > 0) {
                        sharedData.isFourGHandset = [[data valueForKeyPath:@"fourg_powerpack.isDevice4g"] boolValue];
                        sharedData.isFourGSim = [[data valueForKeyPath:@"fourg_powerpack.isSimcard4g"] boolValue];
                        profileModel.flag4Gpowermeter = YES;
                    }
                    else {
                        profileModel.flag4Gpowermeter = NO;
                    }
                }
                
                NSString *msisdn = [data objectForKey:@"msisdn"];
                NSString *sharedKeySSO = SHARED_KEY_SSO;
                //msisdn = [EncryptDecrypt doCipher:msisdn action:kCCDecrypt withKey:sharedKeySSO];
                msisdn = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCDecrypt withKey:sharedKeySSO];
                //NSLog(@"msisdn SSO = %@",msisdn);
                
                NSString *saltKeyAPI = SALT_KEY;
                //msisdn = [EncryptDecrypt doCipher:msisdn action:kCCEncrypt withKey:saltKeyAPI];
                msisdn = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCEncrypt withKey:saltKeyAPI];
                //NSLog(@"msisdn API encrypt = %@",msisdn);
                
                //        msisdn = [EncryptDecrypt doCipher:msisdn action:kCCDecrypt withKey:saltKeyAPI];
                //        NSLog(@"msisdn API decrypt -- ** = %@",msisdn);
                
                profileModel.msisdn = msisdn;
                profileModel.email = [data objectForKey:@"email"];
                profileModel.language = [data objectForKey:@"language"];
                
                if ([profileModel.language length] <= 0) {
                    profileModel.language = @"id";
                }
                
                /*
                 // save to preference
                 NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                 [prefs setObject:profileModel.language
                 forKey:@"language"];
                 [prefs synchronize];*/
                
                [Language setLanguage:profileModel.language];
                
                profileModel.location = [data objectForKey:@"location"];
                profileModel.regional = [data objectForKey:@"regional"];
                profileModel.status = [data objectForKey:@"status"];
                
                NSString *address = [data objectForKey:@"address"];
                if (!!address && ![address isEqual:[NSNull null]]) {
                    profileModel.address = address;
                }
                else {
                    profileModel.address = @"";
                    //NSLog(@"address NULL");
                }
                
                NSString *address2 = [data objectForKey:@"address2"];
                if (!!address2 && ![address2 isEqual:[NSNull null]]) {
                    profileModel.address2 = address2;
                }
                else {
                    profileModel.address2 = @"";
                    //NSLog(@"address2 NULL");
                }
                
                NSString *address3 = [data objectForKey:@"address3"];
                if (!!address2 && ![address2 isEqual:[NSNull null]]) {
                    profileModel.address3 = address3;
                }
                else {
                    profileModel.address3 = @"";
                    //NSLog(@"address3 NULL");
                }
                
                profileModel.city = [data objectForKey:@"city"];
                profileModel.postcode = [data objectForKey:@"postcode"];
                profileModel.confirmEmail = [data objectForKey:@"confirm_email"];
                profileModel.emailNotify = [data objectForKey:@"email_notify"];
                profileModel.pushNotify = [data objectForKey:@"push_notify"];
                profileModel.gender = [data objectForKey:@"gender"];
                profileModel.birth = [data objectForKey:@"birth"];
                profileModel.token = [data objectForKey:@"token"];
                
                // TODO : V1.9.5
                
                //NSMutableDictionary *profileDict = getDataFromPreferenceWithKey(KEEP_SIGNIN_CACHE_KEY);
                //[profileDict setObject:profileModel.token forKey:PROFILE_TOKEN_CACHE_KEY];
                saveDataToPreferenceWithKeyAndValue(PROFILE_TOKEN_CACHE_KEY,profileModel.token);
                
                //NSMutableDictionary *profileDict2 = getDataFromPreferenceWithKey(KEEP_SIGNIN_CACHE_KEY);
                NSString *token = getDataFromPreferenceWithKey(PROFILE_TOKEN_CACHE_KEY);
                NSLog(@"token cek bro = %@",token);
                
                //NSNumber *fbLink = [data objectForKey:@"fb_link"];
                //profileModel.fbLink = [fbLink intValue];
                
                id autologin = [data objectForKey:@"autologin"];
                if (!!autologin && ![autologin isEqual:[NSNull null]]) {
                    NSString *autologinStr = autologin;
                    profileModel.autologin = autologinStr;
                }
                else {
                    profileModel.autologin = @"";
                    //NSLog(@"autologin NULL");
                }
                
                profileModel.device         = [data objectForKey:@"device"];
                profileModel.imei           = [data objectForKey:@"imei"];
                profileModel.tac            = [data objectForKey:@"tac"];
                profileModel.subscriberType = [data objectForKey:@"subscriber_type"];
                profileModel.balanceValue   = [data objectForKey:@"balance_value"];
                profileModel.activeEndDate  = [data objectForKey:@"active_end_date"];
                profileModel.graceEndDate   = [data objectForKey:@"grace_end_date"];
                
                NSString *msisdnType        = [data objectForKey:@"msisdn_type"];
                profileModel.msisdnType     = [msisdnType intValue];
                
                NSString *idCardTypeTmp = [data objectForKey:@"idcard_type"];
                if (!!idCardTypeTmp && ![idCardTypeTmp isEqual:[NSNull null]]) {
                    profileModel.idCardType = idCardTypeTmp;
                }
                else {
                    profileModel.idCardType = @"";
                }
                
                NSString *idCardNumberTmp = [data objectForKey:@"idcard_number"];
                if (!!idCardNumberTmp && ![idCardNumberTmp isEqual:[NSNull null]]) {
                    profileModel.idCardNumber = idCardNumberTmp;
                }
                else {
                    profileModel.idCardNumber = @"";
                }
                
                NSString *placeBirth = [data objectForKey:@"place_birth"];
                if (!!placeBirth && ![placeBirth isEqual:[NSNull null]]) {
                    profileModel.placeBirth = placeBirth;
                }
                else {
                    profileModel.placeBirth = @"";
                }
                
                NSString *otherPhone = [data objectForKey:@"other_phone"];
                if (!!otherPhone && ![otherPhone isEqual:[NSNull null]]) {
                    profileModel.otherPhone = otherPhone;
                }
                else {
                    profileModel.otherPhone = @"";
                }
                
                // CR Change Logo
                profileModel.telcoOperator = [data objectForKey:@"operator"];
                NSArray *logoArray = [data objectForKey:@"logo"];
                if ([logoArray count] != 0) {
                    NSEnumerator *enumerator = [logoArray objectEnumerator];
                    NSDictionary *item;
                    
                    NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
                    
                    while (item = (NSDictionary*)[enumerator nextObject]) {
                        
                        LogoModel *logoModel = [[LogoModel alloc] init];
                        
                        NSArray *allKeys = [item allKeys];
                        NSString *key = [allKeys objectAtIndex:0];
                        if ([key isEqualToString:@"68x44"]) {
                            logoModel.logoURL = [item valueForKeyPath:@"68x44.url"];
                            logoModel.logoURL = [logoModel.logoURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                            logoModel.version = [item valueForKeyPath:@"68x44.version"];
                            logoModel.isRetina = NO;
                        }
                        else {
                            logoModel.logoURL = [item valueForKeyPath:@"135x88.url"];
                            logoModel.logoURL = [logoModel.logoURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                            logoModel.version = [item valueForKeyPath:@"135x88.version"];
                            logoModel.isRetina = YES;
                        }
                        
                        [contentTmp addObject:logoModel];
                        [logoModel release];
                        logoModel = nil;
                        
                    }
                    
                    profileModel.logo = [NSArray arrayWithArray:contentTmp];
                    [contentTmp release];
                    contentTmp = nil;
                }
                else
                {
                    NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
                    LogoModel *logoModel = [[LogoModel alloc] init];
                    logoModel.logoURL = @"";
                    logoModel.version = @"0";
                    logoModel.isRetina = NO;
                    [contentTmp addObject:logoModel];
                    [logoModel release];
                    logoModel = nil;
                    
                    LogoModel *logoModel1 = [[LogoModel alloc] init];
                    logoModel1.logoURL = @"";
                    logoModel1.version = @"0";
                    logoModel1.isRetina = YES;
                    [contentTmp addObject:logoModel1];
                    [logoModel1 release];
                    logoModel1 = nil;
                    
                    profileModel.logo = [NSArray arrayWithArray:contentTmp];
                    [contentTmp release];
                    contentTmp = nil;
                }
                //---------------
                
                // CR Reskinning
                ThemesModel *themesModel = [[ThemesModel alloc] init];
                themesModel.header = [data valueForKeyPath:@"theme.css_header"];
                themesModel.headerGradientStart = [data valueForKeyPath:@"theme.css_header_bg_start"];
                themesModel.headerGradientEnd = [data valueForKeyPath:@"theme.css_header_bg_end"];
                
                //themesModel.featureButton = [data valueForKeyPath:@"theme.css_menu_feature"];
                //themesModel.button = [data valueForKeyPath:@"theme.css_button_right"];
                
                themesModel.menuColor = [data valueForKeyPath:@"theme.css_menu_color"];
                themesModel.menuH3 = [data valueForKeyPath:@"theme.css_menu_h3"];
                
                themesModel.menuFeature     = [data valueForKeyPath:@"theme.css_menu_feature"];
                themesModel.menuFeatureTxt  = [data valueForKeyPath:@"theme.css_menu_feature_txt"];
                
                themesModel.accordionCurrentBg = [data valueForKeyPath:@"theme.css_accordion_current_bg"];
                themesModel.accordionBg        = [data valueForKeyPath:@"theme.css_accordion_bg"];
                
                themesModel.packageButtonBg  = [data valueForKeyPath:@"theme.css_package_button_bg"];
                themesModel.packageButtonTxt = [data valueForKeyPath:@"theme.css_package_button_txt"];
                
                profileModel.themesModel = themesModel;
                [themesModel release];
                //---------------
                
                // TODO : HYGIENE
                id wordingData = [profileDict valueForKeyPath:@"data.wording_page_login"];
                if ([wordingData isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *wordingPageLoginDict = (NSDictionary*)wordingData;
                    NSDictionary *langID = [wordingPageLoginDict valueForKey:@"language_id"];
                    NSDictionary *langEN = [wordingPageLoginDict valueForKey:@"language_en"];
                    LoginWordingModel *loginWordingObj = [[LoginWordingModel alloc] init];
                    LoginWordingLangModel *wordingENObj = [[LoginWordingLangModel alloc] init];
                    LoginWordingLangModel *wordingIDObj = [[LoginWordingLangModel alloc] init];
                    wordingENObj.btnLogin = [langEN valueForKey:kLoginWording_BtnLogin];
                    wordingENObj.btnRequest = [langEN valueForKey:kLoginWording_BtnRequest];
                    wordingENObj.guide = [langEN valueForKey:kLoginWording_ForgotPassword];
                    wordingENObj.info = [langEN valueForKey:kLoginWording_Info];
                    wordingENObj.password = [langEN valueForKey:kLoginWording_Password];
                    wordingENObj.remember = [langEN valueForKey:kLoginWording_RememberPassword];
                    wordingENObj.welcome = [langEN valueForKey:kLoginWording_Welcome];
                    wordingENObj.msisdn = [langEN valueForKey:kLoginWording_YourMsisdn];
                    
                    wordingIDObj.btnLogin = [langID valueForKey:kLoginWording_BtnLogin];
                    wordingIDObj.btnRequest = [langID valueForKey:kLoginWording_BtnRequest];
                    wordingIDObj.guide = [langID valueForKey:kLoginWording_ForgotPassword];
                    wordingIDObj.info = [langID valueForKey:kLoginWording_Info];
                    wordingIDObj.password = [langID valueForKey:kLoginWording_Password];
                    wordingIDObj.remember = [langID valueForKey:kLoginWording_RememberPassword];
                    wordingIDObj.welcome = [langID valueForKey:kLoginWording_Welcome];
                    wordingIDObj.msisdn = [langID valueForKey:kLoginWording_YourMsisdn];
                    
                    loginWordingObj.wordingEN = wordingENObj;
                    loginWordingObj.wordingID = wordingIDObj;
                    sharedData.loginWordingData = loginWordingObj;
                    [loginWordingObj release];
                    loginWordingObj = nil;
                    [wordingENObj release];
                    [wordingIDObj release];
                    wordingIDObj = nil;
                    wordingENObj = nil;
                }
                
                // XVAGANZA
                //profileModel.flagXvaganza = [[data objectForKey:@"flag_xvaganza"] boolValue];
                // TODO: V1.9.5
                profileModel.flagXvaganza = NO;
                sharedData.profileData = profileModel;
                sharedData.currentVersion = [data objectForKey:@"version"];
                [profileModel release];
                
                isSuccess = YES;
            }
            else {
                
                // TODO : HYGIENE
                NSDictionary *wordingPageLoginDict = [profileDict valueForKeyPath:@"data.wording_page_login"];
                if([wordingPageLoginDict isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *langID = [wordingPageLoginDict valueForKey:@"language_id"];
                    NSDictionary *langEN = [wordingPageLoginDict valueForKey:@"language_en"];
                    LoginWordingModel *loginWordingObj = [[LoginWordingModel alloc] init];
                    LoginWordingLangModel *wordingENObj = [[LoginWordingLangModel alloc] init];
                    LoginWordingLangModel *wordingIDObj = [[LoginWordingLangModel alloc] init];
                    wordingENObj.btnLogin = [langEN valueForKey:kLoginWording_BtnLogin];
                    wordingENObj.btnRequest = [langEN valueForKey:kLoginWording_BtnRequest];
                    wordingENObj.guide = [langEN valueForKey:kLoginWording_ForgotPassword];
                    wordingENObj.info = [langEN valueForKey:kLoginWording_Info];
                    wordingENObj.password = [langEN valueForKey:kLoginWording_Password];
                    wordingENObj.remember = [langEN valueForKey:kLoginWording_RememberPassword];
                    wordingENObj.welcome = [langEN valueForKey:kLoginWording_Welcome];
                    wordingENObj.msisdn = [langEN valueForKey:kLoginWording_YourMsisdn];
                    
                    wordingIDObj.btnLogin = [langID valueForKey:kLoginWording_BtnLogin];
                    wordingIDObj.btnRequest = [langID valueForKey:kLoginWording_BtnRequest];
                    wordingIDObj.guide = [langID valueForKey:kLoginWording_ForgotPassword];
                    wordingIDObj.info = [langID valueForKey:kLoginWording_Info];
                    wordingIDObj.password = [langID valueForKey:kLoginWording_Password];
                    wordingIDObj.remember = [langID valueForKey:kLoginWording_RememberPassword];
                    wordingIDObj.welcome = [langID valueForKey:kLoginWording_Welcome];
                    wordingIDObj.msisdn = [langID valueForKey:kLoginWording_YourMsisdn];
                    
                    loginWordingObj.wordingEN = wordingENObj;
                    loginWordingObj.wordingID = wordingIDObj;
                    sharedData.loginWordingData = loginWordingObj;
                    [loginWordingObj release];
                    loginWordingObj = nil;
                    [wordingENObj release];
                    [wordingIDObj release];
                    wordingIDObj = nil;
                    wordingENObj = nil;
                }
                
                reason = [profileDict valueForKey:@"message"];
                errorCode = status;
                isSuccess = NO;
                sharedData.currentVersion = [profileDict valueForKeyPath:@"data.version"];
            }
        
            // TODO : V1.9
            // CHECK NEW VERSION UPDATE
            NSDictionary *newVersion = [profileDict valueForKeyPath:@"data.new_version"];
            if([newVersion isKindOfClass:[NSDictionary class]])
            {
                newVersion = [self removeNullResponse:[newVersion mutableCopy]];
                NSLog(@"new version %@", newVersion);
                UpdateVersionModel *uvModel = [[UpdateVersionModel alloc] init];
                uvModel.version = [newVersion objectForKey:kUpdateVersion_version];
                uvModel.message = [newVersion objectForKey:kUpdateVersion_message];
                uvModel.code = [newVersion objectForKey:kUpdateVersion_code];
                uvModel.url = [newVersion objectForKey:kUpdateVersion_url];
                uvModel.posBtnTitle = [newVersion objectForKey:kUpdateVersion_posBtnTitle];
                uvModel.cancelBtnTitle = [newVersion objectForKey:kUpdateVersion_cancelBtnTitle];
                sharedData.updateVersionModel = uvModel;
                [uvModel release];
                uvModel = nil;
            }
            
            /*
             if ([_delegate respondsToSelector:@selector(parseProfileDone:withReason:)])
             [_delegate parseProfileDone:isSuccess withReason:reason];*/
            
            /*
             if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
             [_delegate parseDataDone:isSuccess withReason:reason];*/
            
            if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:withErrorCode:)])
                [_delegate parseDataDone:isSuccess withReason:reason withErrorCode:errorCode];
        }
        else
        {
            if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
                [_delegate parseDataDone:NO withReason:[Language get:@"general_error_login" alter:nil]];
        }
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_login" alter:nil]];
    }
}

#pragma mark - SIGN OUT
- (void)parseSignOutData:(NSString*)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *registerDict = responseJSON;
        NSString *status = [registerDict objectForKey:@"code"];
        //(@"status = %@",status);
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"200"]) {
            isSuccess = YES;
        }
        else {
            reason = [registerDict valueForKey:@"message"];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];

    }
    @catch (NSException *exception)
    {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

#pragma mark - REGISTRATION
- (void)parseRegisterData:(NSString*)data
{
    //NSString *dataTmp = @"{\"code\":\"200\", \"message\": \"success\"}";
    id responseJSON = [data JSONValue];
    NSDictionary *registerDict = responseJSON;
    NSString *status = [registerDict objectForKey:@"code"];
    //NSLog(@"status = %@",status);
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"200"]) {
        isSuccess = YES;
    }
    else {
        reason = [registerDict valueForKey:@"message"];
        if (!!reason && ![reason isEqual:[NSNull null]]) {
            reason = reason;
        }
        else {
            reason = [Language get:@"register_failed" alter:nil];
        }
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - ACTIVATION
- (void)parseActivationData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *activationDict = responseJSON;
    NSString *status = [activationDict objectForKey:@"code"];
    //NSLog(@"status = %@",status);
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"200"]) {
        isSuccess = YES;
    }
    else {
        reason = [activationDict valueForKey:@"message"];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - RESET PASSWORD
- (void)parseResetPasswordData:(NSString*)data
{
    @try
    {
        BOOL isSuccess;
        NSString *reason = @"";
        NSString *errorCode = @"";
        if(data != nil && ![data isEqualToString:@""])
        {
            id responseJSON = [data JSONValue];
            NSDictionary *resetPasswordDict = responseJSON;
            NSString *status = [resetPasswordDict objectForKey:@"code"];
            //NSLog(@"status = %@",status);
            errorCode = [resetPasswordDict valueForKey:@"code"];
            if ([status isEqualToString:@"200"])
            {
                NSDictionary *data = [resetPasswordDict valueForKey:@"data"];
                if(data)
                {
                    reason = [data valueForKey:@"message_splash"];
                    if([reason length] > 0)
                    {
                        errorCode = @"200";
                    }
                    else
                    {
                        reason = [resetPasswordDict valueForKey:@"message"];
                        errorCode = @"201";
                    }
                }
                else
                {
                    reason = [resetPasswordDict valueForKey:@"message"];
                    errorCode = @"201";
                }
                
                isSuccess = YES;
            }
            else
            {
                reason = [resetPasswordDict valueForKey:@"message"];
                isSuccess = NO;
            }
        }
        else
        {
            isSuccess = NO;
            errorCode = @"9999";
        }
        // TODO : Hygiene
        // Add error code to the result
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:withErrorCode:)])
            [_delegate parseDataDone:isSuccess withReason:reason withErrorCode:errorCode];
    }
    @catch (NSException *exception) {
        //NSLog(@"exception %@", [exception reason]);
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:withErrorCode:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil] withErrorCode:@"9999"];
    }
}


#pragma mark - RESEND PIN
- (void)parseResendPinData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *resendPinDict = responseJSON;
    NSString *status = [resendPinDict objectForKey:@"code"];
    //NSLog(@"status = %@",status);
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"200"]) {
        isSuccess = YES;
    }
    else {
        reason = [resendPinDict valueForKey:@"message"];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - CHANGE PASSWORD
- (void)parseChangePasswordData:(NSString*)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *changePasswordDict = responseJSON;
        NSString *status = [changePasswordDict objectForKey:@"code"];
        //NSLog(@"status = %@",status);
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"200"]) {
            reason = [changePasswordDict valueForKey:@"message"];
            isSuccess = YES;
        }
        else {
            reason = [changePasswordDict valueForKey:@"message"];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception)
    {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

#pragma mark - REGISTER DEVICE TOKEN
//- (void)parseRegisterDeviceTokenData:(NSString*)data
- (void)parseRegisterDeviceTokenData:(NSString*)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *regDevTokenDict = responseJSON;
        NSString *status = [regDevTokenDict objectForKey:@"code"];
        //NSLog(@"status = %@",status);
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"200"]) {
            isSuccess = YES;
        }
        else {
            reason = [regDevTokenDict valueForKey:@"message"];
            isSuccess = NO;
        }
        
        //        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        //            [_delegate parseDataDone:isSuccess withReason:reason];
        if ([_delegate respondsToSelector:@selector(parseRegisterDeviceTokenDone:)])
            [_delegate parseRegisterDeviceTokenDone:[NSDictionary dictionaryWithObject:@"1" forKey:@"success"]];
    }
    @catch (NSException *exception) {
        //        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        //            [_delegate parseDataDone:NO withReason:@""];
        if ([_delegate respondsToSelector:@selector(parseRegisterDeviceTokenDone:)])
            [_delegate parseRegisterDeviceTokenDone:[NSDictionary dictionaryWithObject:@"0" forKey:@"success"]];
    }
}

#pragma mark - API

#pragma mark - GET MENU
- (void)parseGetMenuData:(NSString*)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *menuDict = responseJSON;
        menuDict = [self removeNullResponse:[menuDict mutableCopy]];
        NSString *status = [menuDict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"]) {
            NSDictionary *data = [menuDict valueForKey:@"0"];
            NSArray *menuArray = [data objectForKey:@"result"];
            if ([menuArray count] != 0) {
                NSEnumerator *enumerator = [menuArray objectEnumerator];
                NSDictionary *item;
                
                NSMutableDictionary *menuDictTmp = [[NSMutableDictionary alloc] init];
                NSMutableArray *contentTmp;
                //NSMutableArray *paymentMethodTmp = [[NSMutableArray alloc] init];
                
                DataManager *dataManager = [DataManager sharedInstance];
                
                while (item = (NSDictionary*)[enumerator nextObject]) {
                    
                    MenuModel *menuModel = [[MenuModel alloc] init];
                    menuModel.menuName = [item valueForKeyPath:@"menu.0"];
                    
                    NSDictionary *attributes = [[item objectForKey:@"menu"] objectForKey:@"@attributes"];
                    menuModel.href         = [attributes valueForKey:@"href"];
                    menuModel.hrefweb      = [attributes valueForKey:@"hrefweb"];
                    menuModel.menuId       = [attributes valueForKey:@"id"];
                    menuModel.parentId     = [attributes valueForKey:@"parentid"];
                    menuModel.groupId      = [attributes valueForKey:@"gid"];
                    menuModel.groupName    = [attributes valueForKey:@"group"];
                    menuModel.desc         = [attributes valueForKey:@"desc"];
                    menuModel.duration     = [attributes valueForKey:@"duration"];
                    menuModel.price        = [attributes valueForKey:@"price"];
                    menuModel.volume       = [attributes valueForKey:@"volume"];
                    menuModel.packageId    = [attributes valueForKey:@"pkgid"];
                    menuModel.packageType  = [attributes valueForKey:@"pkgtype"];
                    menuModel.packageGroup = [attributes valueForKey:@"pkggroup"];
                    menuModel.tooltips     = [attributes valueForKey:@"tooltips"];
                    menuModel.topDesc      = [attributes valueForKey:@"topdesc"];
                    menuModel.icon         = [attributes valueForKey:@"icon"];
                    menuModel.hex          = [attributes valueForKey:@"hex"];
                    menuModel.confirmDesc  = [attributes valueForKey:@"confirmDesc"];
                    menuModel.open         = NO;
                    // TODO : V1.9
                    // Payment Method attribute
                    menuModel.paymentMethod = [attributes valueForKey:@"payment_method"];
                    menuModel.shortDesc = [attributes objectForKey:@"sort_desc"];
                    
                    // DROP 2
                    // position
                    if([attributes objectForKey:@"position"])
                    {
                        menuModel.position = [[attributes objectForKey:@"position"] integerValue];
                    }
                    
                    // Save mark for xlstar and package
                    if ([menuModel.hrefweb isEqualToString:@"#xlstar"]) {
                        dataManager.xlstarMenu = menuModel;
                    }
                    if ([menuModel.hrefweb isEqualToString:@"#package"] && ![menuModel.groupName isEqualToString:kFeatureMenu]) {
                        dataManager.packageMenu = menuModel;
                    }
                    if ([menuModel.href isEqualToString:@"#reload_pg"]) {
                        dataManager.paymentMethodMenu = menuModel;
                    }
                    if ([menuModel.menuId isEqualToString:@"6_0046"]) {
                        dataManager.package4gMenu = menuModel;
                    }
                    //
                    
                    //------------------------------------------------------------------//
                    // Set boolean value that apps have Quota Calculator Feature or not //
                    //------------------------------------------------------------------//
                    if ([menuModel.href isEqualToString:@"#quotacalculator"]) {
                        dataManager.haveQuotaCalculator = YES;
                    }
                    
                    //NSString *parentId = [attributes valueForKey:@"parentid"];
                    NSString *href     = [attributes valueForKey:@"href"];
                    if (![href isEqualToString:@"#notification"] &&
                        ![href isEqualToString:@"#hot_item"]) {
                        if ([menuDictTmp objectForKey:menuModel.parentId]) {
                            // key exists.
                            contentTmp = [menuDictTmp objectForKey:menuModel.parentId];
                            [contentTmp addObject:menuModel];
                        }
                        else {
                            contentTmp = [[[NSMutableArray alloc] init] autorelease];
                            [contentTmp addObject:menuModel];
                            [menuDictTmp setObject:contentTmp forKey:menuModel.parentId];
                        }
                    }
                    
                    [menuModel release];
                    menuModel = nil;
                }
                
                
                dataManager.menuData = [NSDictionary dictionaryWithDictionary:menuDictTmp];
                //NSLog(@"menuDictTmp = %@",dataManager.menuData);
                
                NSString *unread = [menuDict objectForKey:@"unread"];
                //NSLog(@"dataManager.unread Parser string = %@",unread);
                
                if ([unread isEqual:[NSNull null]]) {
                    dataManager.unread = 0;
                }
                else {
                    dataManager.unread = [unread intValue];
                }
                
                [menuDictTmp release];
            }
            
            //------------------//
            // Restructure Menu //
            //------------------//
            NSArray *homeMenuArray = [menuDict objectForKey:@"home"];
            if ([homeMenuArray count] != 0) {
                NSEnumerator *enumerator = [homeMenuArray objectEnumerator];
                NSDictionary *item;
                
                NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
                
                NSMutableArray *featureTmp = [[NSMutableArray alloc] init];
                
                while (item = (NSDictionary*)[enumerator nextObject]) {
                    
                    MenuModel *menuModel = [[MenuModel alloc] init];
                    menuModel.menuName = [item valueForKeyPath:@"menu.0"];
                    
                    NSDictionary *attributes = [[item objectForKey:@"menu"] objectForKey:@"@attributes"];
                    menuModel.href         = [attributes valueForKey:@"href"];
                    menuModel.hrefweb      = [attributes valueForKey:@"hrefweb"];
                    menuModel.menuId       = [attributes valueForKey:@"id"];
                    menuModel.parentId     = [attributes valueForKey:@"parentid"];
                    menuModel.groupId      = [attributes valueForKey:@"gid"];
                    menuModel.groupName    = [attributes valueForKey:@"group"];
                    menuModel.desc         = [attributes valueForKey:@"desc"];
                    menuModel.icon         = [attributes valueForKey:@"icon"];
                    menuModel.hex          = [attributes valueForKey:@"hex"];
                    menuModel.open         = NO;
                    
                    [contentTmp addObject:menuModel];
                    
                    if ([menuModel.groupName isEqualToString:kFeatureMenu]) {
                        [featureTmp addObject:menuModel];
                    }
                    
                    [menuModel release];
                    menuModel = nil;
                }
                
                DataManager *dataManager = [DataManager sharedInstance];
                dataManager.homeMenu = [NSArray arrayWithArray:contentTmp];
                dataManager.featureMenu = [NSArray arrayWithArray:featureTmp];
                [contentTmp release];
            }
            
            NSArray *sideMenuArray = [menuDict objectForKey:@"sidemenu"];
            if ([sideMenuArray count] != 0) {
                NSEnumerator *enumerator = [sideMenuArray objectEnumerator];
                NSDictionary *item;
                
                NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
                
                while (item = (NSDictionary*)[enumerator nextObject]) {
                    
                    MenuModel *menuModel = [[MenuModel alloc] init];
                    menuModel.menuName = [item valueForKeyPath:@"menu.0"];
//                    NSLog(@"--> Menu Name Log = %@",menuModel.menuName);
                    
                    NSDictionary *attributes = [[item objectForKey:@"menu"] objectForKey:@"@attributes"];
                    menuModel.href         = [attributes valueForKey:@"href"];
                    menuModel.hrefweb      = [attributes valueForKey:@"hrefweb"];
                    menuModel.menuId       = [attributes valueForKey:@"id"];
                    menuModel.parentId     = [attributes valueForKey:@"parentid"];
                    menuModel.groupId      = [attributes valueForKey:@"gid"];
                    menuModel.groupName    = [attributes valueForKey:@"group"];
                    menuModel.desc         = [attributes valueForKey:@"desc"];
                    menuModel.icon         = [attributes valueForKey:@"icon"];
                    menuModel.hex          = [attributes valueForKey:@"hex"];
                    menuModel.open         = NO;
                    
                    [contentTmp addObject:menuModel];
                    [menuModel release];
                    menuModel = nil;
                }
                
                DataManager *dataManager = [DataManager sharedInstance];
                dataManager.sideMenu = [NSArray arrayWithArray:contentTmp];
                [contentTmp release];
            }
        
            /*
             * Balance Sub Menu
             */
            //-DataManager *dataManager = [DataManager sharedInstance];
            // TODO : Bug fixing last 5 transactions
            // Get rid of the postpaid/prepaid condition check
            // FIXME : Uncomment this
//            if (dataManager.profileData.msisdnType == POSTPAID) {
//                NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
//                MenuModel *menuModel = [dataManager.homeMenu objectAtIndex:0];
//                [contentTmp addObject:menuModel];
//                dataManager.balanceMenu = [NSArray arrayWithArray:contentTmp];
//            }
//            else {
                NSArray *balanceMenuArray = [menuDict objectForKey:@"balancesubmenu"];
                if ([balanceMenuArray count] != 0) {
                    NSEnumerator *enumerator = [balanceMenuArray objectEnumerator];
                    NSDictionary *item;
                    
                    NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
                    
                    while (item = (NSDictionary*)[enumerator nextObject]) {
                        NSDictionary *attributes = [[item objectForKey:@"menu"] objectForKey:@"@attributes"];
                        // TODO : Only get the menu for balance with parentid = 4
                        if([[attributes objectForKey:@"parentid"] isEqualToString:@"4"] || [[attributes objectForKey:@"parentid"] isEqualToString:@"0"])
                        {
                            MenuModel *menuModel = [[MenuModel alloc] init];
                            menuModel.menuName     = [item valueForKeyPath:@"menu.0"];
                            menuModel.href         = [attributes valueForKey:@"href"];
                            menuModel.hrefweb      = [attributes valueForKey:@"hrefweb"];
                            menuModel.menuId       = [attributes valueForKey:@"id"];
                            menuModel.parentId     = [attributes valueForKey:@"parentid"];
                            menuModel.groupId      = [attributes valueForKey:@"gid"];
                            menuModel.groupName    = [attributes valueForKey:@"group"];
                            menuModel.desc         = [attributes valueForKey:@"desc"];
                            menuModel.icon         = [attributes valueForKey:@"icon"];
                            menuModel.hex          = [attributes valueForKey:@"hex"];
                            menuModel.open         = NO;
                            
                            [contentTmp addObject:menuModel];
                            [menuModel release];
                            menuModel = nil;
                        }
                    }
                    
                    DataManager *dataManager = [DataManager sharedInstance];
                    dataManager.balanceMenu = [NSArray arrayWithArray:contentTmp];
                    [contentTmp release];
//                }

                //-------------------------//
                // End of Restructure Menu //
                //-------------------------//
                
                isSuccess = YES;
            }
        }
        else {
            reason = [NSString stringWithFormat:@"%@",[menuDict valueForKey:@"err_desc"]];
            isSuccess = NO;
        }
//=======
//        }
        //-----------------
        
        //-------------------------//
        // End of Restructure Menu //
        //-------------------------//
//>>>>>>> 4gusim

        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception)
    {
        NSLog(@"get menu exception %@", [exception reason]);
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

#pragma mark - UPDATE PROFILE
- (void)parseUpdateProfileData:(NSString*)data
{
    @try {
        
        id responseJSON = [data JSONValue];
        NSDictionary *updateProfileDict = responseJSON;
        NSString *status = [updateProfileDict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"]) {
            
            DataManager *sharedData = [DataManager sharedInstance];
            
            sharedData.profileData.msisdn = [updateProfileDict objectForKey:@"msisdn"];
            //NSString *saltKeyAPI = SALT_KEY;
            
            //NSString *msisdnTest = [EncryptDecrypt doCipher:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
            //NSLog(@"msisdn API decrypt -- ** = %@",msisdnTest);
            
            sharedData.profileData.fullName     = [updateProfileDict objectForKey:@"fullname"];
            sharedData.profileData.name         = [updateProfileDict objectForKey:@"nickname"];
            sharedData.profileData.gender       = [updateProfileDict objectForKey:@"gender"];
            sharedData.profileData.birth        = [updateProfileDict objectForKey:@"birth"];
            sharedData.profileData.email        = [updateProfileDict objectForKey:@"email"];
            sharedData.profileData.language     = [updateProfileDict objectForKey:@"lang"];
            sharedData.profileData.address      = [updateProfileDict objectForKey:@"address"];
            sharedData.profileData.address2     = [updateProfileDict objectForKey:@"address2"];
            sharedData.profileData.address3     = [updateProfileDict objectForKey:@"address3"];
            sharedData.profileData.city         = [updateProfileDict objectForKey:@"city"];
            sharedData.profileData.postcode     = [updateProfileDict objectForKey:@"postcode"];
            sharedData.profileData.emailNotify  = [updateProfileDict objectForKey:@"email_notify"];
            sharedData.profileData.pushNotify   = [updateProfileDict objectForKey:@"push_notify"];
            sharedData.profileData.autologin    = [updateProfileDict objectForKey:@"autologin"];
            sharedData.profileData.placeBirth   = [updateProfileDict objectForKey:@"place_birth"];
            sharedData.profileData.idCardType   = [updateProfileDict objectForKey:@"idcard_type"];
            sharedData.profileData.idCardNumber = [updateProfileDict objectForKey:@"idcard_number"];
            
            sharedData.profileData.status       = [updateProfileDict objectForKey:@"profile_status"];
            
            NSString *otherPhone = [updateProfileDict objectForKey:@"other_phone"];
            if (!!otherPhone && ![otherPhone isEqual:[NSNull null]]) {
                sharedData.profileData.otherPhone = otherPhone;
            }
            else {
                sharedData.profileData.otherPhone = @"";
            }
            
            isSuccess = YES;
        }
        else {
            reason = [NSString stringWithFormat:@"%@",[updateProfileDict valueForKey:@"err_desc"]];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

#pragma mark - CHECK BALANCE
- (void)parseCheckBalanceData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *checkBalanceDict = responseJSON;
    NSString *status = [checkBalanceDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    NSString *errorCode = @"";
    if ([status isEqualToString:@"1"]) {
        NSDictionary *result = [checkBalanceDict objectForKey:@"result"];
        
        CheckBalanceModel *checkBalanceModel = [[CheckBalanceModel alloc] init];
        
        checkBalanceModel.msisdn = [checkBalanceDict valueForKey:@"msisdn"];
        
        checkBalanceModel.accountState = [result valueForKey:@"accountstate"];
        
        checkBalanceModel.accountStatus = [result valueForKey:@"accountstatus"];
        
        checkBalanceModel.accountType = [result valueForKey:@"accounttype"];
        
        checkBalanceModel.activeStopDate = [result valueForKey:@"activestopdate"];
        
        checkBalanceModel.balance = [result valueForKey:@"balance"];
        
        checkBalanceModel.disableStopDate = [result valueForKey:@"disablestopdate"];
        
        checkBalanceModel.suspendStopDate = [result valueForKeyPath:@"suspendstopdate"];
        
        // Bonus Info
        NSArray *bonusInfo = [result valueForKey:@"bonus"];
        if ([bonusInfo count] != 0) {
            NSEnumerator *enumerator = [bonusInfo objectEnumerator];
            NSDictionary *item;
            NSMutableArray *bonusTmp = [[NSMutableArray alloc] init];
            while (item = (NSDictionary*)[enumerator nextObject]) {
                BonusInfoModel *bonusInfoModel = [[BonusInfoModel alloc] init];
                bonusInfoModel.accId      = [item valueForKey:@"accid"];
                bonusInfoModel.accVal     = [item valueForKey:@"accval"];
                bonusInfoModel.accValType = [item valueForKey:@"accvaltype"];
                bonusInfoModel.accExpTime = [item valueForKey:@"accexptime"];
                bonusInfoModel.accBalDesc = [item valueForKey:@"accbaldesc"];
                bonusInfoModel.desc       = [item valueForKey:@"desc"];
                bonusInfoModel.bonusVal   = [item valueForKey:@"bonusval"];
                
                [bonusTmp addObject:bonusInfoModel];
                [bonusInfoModel release];
                bonusInfoModel = nil;
            }
            checkBalanceModel.bonusInfo = [NSArray arrayWithArray:bonusTmp];
            [bonusTmp release];
        }
        //-----------
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.checkBalanceData = checkBalanceModel;
        
        [checkBalanceModel release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[checkBalanceDict valueForKey:@"err_desc"]];
        errorCode = [NSString stringWithFormat:@"%@",[checkBalanceDict valueForKey:@"err_code"]];
        isSuccess = NO;
    }
    /*
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];*/
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:withErrorCode:)])
        [_delegate parseDataDone:isSuccess withReason:reason withErrorCode:errorCode];
}

#pragma mark - TOP UP VOUCHER
- (void)parseTopUpVoucherData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *topUpVoucherDict = responseJSON;
    NSString *status = [topUpVoucherDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSDictionary *result = [topUpVoucherDict objectForKey:@"result"];
        
        TopUpVoucherModel *topUpVoucherModel = [[TopUpVoucherModel alloc] init];
        
        topUpVoucherModel.msisdnTo           = [result valueForKey:@"msisdnto"];
        
        topUpVoucherModel.balanceNew         = [result valueForKey:@"newbalance"];
        
        topUpVoucherModel.activeStopNew      = [result valueForKey:@"newactivestop"];
        
        topUpVoucherModel.voucherValue       = [result valueForKey:@"vouchervalue"];
        
        topUpVoucherModel.addValidity        = [result valueForKey:@"addvalidity"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.topUpVoucherModel = topUpVoucherModel;
        
        [topUpVoucherModel release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[topUpVoucherDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - CHECK USAGE
- (void)parseCheckUsageData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *checkUsageDict = responseJSON;
    NSString *status = [checkUsageDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSArray *checkUsageArray = [checkUsageDict objectForKey:@"result"];
        if ([checkUsageArray count] != 0) {
            NSEnumerator *enumerator = [checkUsageArray objectEnumerator];
            NSDictionary *item;
            NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
            while (item = (NSDictionary*)[enumerator nextObject]) {
                CheckUsageModel *checkUsageModel = [[CheckUsageModel alloc] init];
                checkUsageModel.packageId       = [item valueForKey:@"packageid"];
                checkUsageModel.packageCode     = [item valueForKey:@"packagecode"];
                checkUsageModel.packageName     = [item valueForKey:@"packagename"];
                checkUsageModel.startDate       = [item valueForKey:@"startdate"];
                checkUsageModel.endDate         = [item valueForKey:@"enddate"];
                checkUsageModel.packageType     = [item valueForKey:@"package_type"];
                checkUsageModel.usage           = [item valueForKey:@"usage"];
                checkUsageModel.quota           = [item valueForKey:@"quota"];
                checkUsageModel.balance         = [item valueForKey:@"balance"];
                checkUsageModel.usagePercent    = [item valueForKey:@"usage_percent"];
                checkUsageModel.info            = [item valueForKey:@"info"];
                checkUsageModel.unregCmd        = [item valueForKey:@"unregcmd"];
                checkUsageModel.unsubCmd        = [item valueForKey:@"unsubcmd"];
                checkUsageModel.flagUpgrade     = [item valueForKey:@"flagupgrade"];
                checkUsageModel.s0              = [item valueForKey:@"s0"];
                checkUsageModel.t1              = [item valueForKey:@"t1"];
                checkUsageModel.s1              = [item valueForKey:@"s1"];
                checkUsageModel.t2              = [item valueForKey:@"t2"];
                checkUsageModel.s2              = [item valueForKey:@"s2"];
                checkUsageModel.t3              = [item valueForKey:@"t3"];
                checkUsageModel.s3              = [item valueForKey:@"s3"];
                checkUsageModel.t4              = [item valueForKey:@"t4"];
                checkUsageModel.s4              = [item valueForKey:@"s4"];
                checkUsageModel.t5              = [item valueForKey:@"t5"];
                checkUsageModel.s5              = [item valueForKey:@"s5"];
                
                NSNumber *percentage            = [item valueForKeyPath:@"remaining.percentage"];
                checkUsageModel.remainingPercentage = [percentage intValue];
                
                NSNumber *number                = [item valueForKeyPath:@"remaining.number"];
                checkUsageModel.remainingNumber = [number intValue];
                
                checkUsageModel.remainingType   = [item valueForKeyPath:@"remaining.type"];
                checkUsageModel.remainingLabel  = [item valueForKeyPath:@"remaining.label_remain"];
                
                [contentTmp addObject:checkUsageModel];
                [checkUsageModel release];
                checkUsageModel = nil;
            }
            
            DataManager *dataManager = [DataManager sharedInstance];
            dataManager.checkUsageData = [NSArray arrayWithArray:contentTmp];
            [contentTmp release];
        }
        else {
            NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
            
            CheckUsageModel *checkUsageModel = [[CheckUsageModel alloc] init];
            checkUsageModel.packageId       = @"";
            checkUsageModel.packageCode     = @"";
            checkUsageModel.packageName     = @"Regular Plan";
            checkUsageModel.startDate       = @"";
            checkUsageModel.endDate         = @"-";
            checkUsageModel.packageType     = @"0";
            checkUsageModel.usage           = @"-";
            checkUsageModel.quota           = @"";
            checkUsageModel.balance         = @"";
            checkUsageModel.usagePercent    = @"";
            checkUsageModel.info            = @"";
            checkUsageModel.unregCmd        = @"";
            checkUsageModel.unsubCmd        = @"";
            checkUsageModel.flagUpgrade     = @"";
            checkUsageModel.s0              = @"";
            checkUsageModel.t1              = @"";
            checkUsageModel.s1              = @"";
            checkUsageModel.t2              = @"";
            checkUsageModel.s2              = @"";
            checkUsageModel.t3              = @"";
            checkUsageModel.s3              = @"";
            checkUsageModel.t4              = @"";
            checkUsageModel.s4              = @"";
            checkUsageModel.t5              = @"";
            checkUsageModel.s5              = @"";
            
            [contentTmp addObject:checkUsageModel];
            [checkUsageModel release];
            checkUsageModel = nil;
            
            DataManager *dataManager = [DataManager sharedInstance];
            dataManager.checkUsageData = [NSArray arrayWithArray:contentTmp];
            [contentTmp release];
        }
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[checkUsageDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - CHECK USAGE XL
- (void)parseCheckUsageXLData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *checkUsageDict = responseJSON;
    // TODO :V1.9
    // Remove null response
    checkUsageDict = [self removeNullResponse:[checkUsageDict mutableCopy]];
    NSString *status = [checkUsageDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSArray *checkUsageArray = [checkUsageDict objectForKey:@"result"];
        if ([checkUsageArray count] != 0) {
            NSEnumerator *enumerator = [checkUsageArray objectEnumerator];
            NSDictionary *item;
            NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
            while (item = (NSDictionary*)[enumerator nextObject]) {
                CheckUsageXLModel *checkUsageModel = [[CheckUsageXLModel alloc] init];
                checkUsageModel.soccd           = [item valueForKey:@"soccd"];
                checkUsageModel.idService       = [item valueForKey:@"IdService"];
                checkUsageModel.name            = [item valueForKey:@"name"];
                checkUsageModel.nameEnc         = [item valueForKey:@"name_enc"];
                checkUsageModel.desc            = [item valueForKey:@"desc"];
                checkUsageModel.regDate         = [item valueForKey:@"regDate"];
                checkUsageModel.recDate         = [item valueForKey:@"recDate"];
                checkUsageModel.expDate         = [item valueForKey:@"expDate"];
                checkUsageModel.maxQuota        = [item valueForKey:@"maxquota"];
                checkUsageModel.totalRemaining  = [item valueForKey:@"totalRemaining"];
                checkUsageModel.totalRemainingPercent = [item valueForKey:@"totalRemainingPercent"];
                checkUsageModel.shareQuota      = [item valueForKey:@"share_quota"];
                checkUsageModel.quotaMeter      = [item valueForKey:@"quota_meter"];
                // TODO : V1.9
                // Quota Bar Unlimited
                // FIXME: TESTING DUMMY
//                checkUsageModel.isUnlimited = YES;
                checkUsageModel.isUnlimited     = [[item objectForKey:@"unlimited"] boolValue];
                
                NSString *showbar               = [item valueForKey:@"showbar"];
                if ([showbar isEqualToString:@"0"]) {
                    checkUsageModel.showbar = NO;
                }
                else {
                    checkUsageModel.showbar = YES;
                }
                
                NSArray *allowanceTmp = [item valueForKey:@"package_allowance"];
                NSEnumerator *enumerator2 = [allowanceTmp objectEnumerator];
                NSDictionary *item2;
                NSMutableArray *packageAllowanceTmp = [[NSMutableArray alloc] init];
                while (item2 = (NSDictionary*)[enumerator2 nextObject]) {
                    PackageAllowanceModel *model = [[PackageAllowanceModel alloc] init];
                    model.type              = [item2 valueForKey:@"type"];
                    model.name              = [item2 valueForKey:@"bname"];
                    model.quota             = [item2 valueForKey:@"quota"];
                    model.remaining         = [item2 valueForKey:@"remaining"];
                    model.remainingPercent  = [item2 valueForKey:@"remainingPercent"];
                    // TODO : Bug fix Drop 1
                    // Quota bar unlimited
                    model.usage             = [item2 objectForKey:@"usage"];
                    [packageAllowanceTmp addObject:model];
                    [model release];
                    model = nil;
                }
                
                checkUsageModel.packageAllowance = [NSArray arrayWithArray:packageAllowanceTmp];
                
                //
                NSArray *shareQuotaTmp = [item valueForKey:@"share_list"];
                NSEnumerator *enumerator3 = [shareQuotaTmp objectEnumerator];
                NSDictionary *item3;
                NSMutableArray *shareQuotaListTmp = [[NSMutableArray alloc] init];
                while (item3 = (NSDictionary*)[enumerator3 nextObject]) {
                    LabelValueModel *model = [[LabelValueModel alloc] init];
                    model.value              = [item3 valueForKey:@"shareQuotaId"];
                    model.label              = [item3 valueForKey:@"shareQuotaName"];
                    
                    [shareQuotaListTmp addObject:model];
                    [model release];
                    model = nil;
                }
                
                checkUsageModel.shareQuotaList = [NSArray arrayWithArray:shareQuotaListTmp];
                
                // TODO : V1.9
                // Quotabar unlimited
                checkUsageModel.fup               = [item objectForKey:@"fup"];
                //
                
                [contentTmp addObject:checkUsageModel];
                
                [packageAllowanceTmp release];
                [checkUsageModel release];
                checkUsageModel = nil;
            }
            
            DataManager *dataManager = [DataManager sharedInstance];
            dataManager.checkUsageXLData = [NSArray arrayWithArray:contentTmp];
            [contentTmp release];
        }
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[checkUsageDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - QUOTA IMPROVEMENT

- (void)parseQuotaImprovementData:(NSString*)data {
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *dict = responseJSON;
        dict = [self removeNullResponse:[dict mutableCopy]];
        NSLog(@"clean dict = %@",dict);
        BOOL isSuccess;
        NSString *reason = @"";
        if([dict isKindOfClass:[NSDictionary class]])
        {
            NSString *status = [dict objectForKey:@"status"];
            if ([status isEqualToString:@"1"])
            {
                DataManager *sharedData = [DataManager sharedInstance];
                NSDictionary *resultDict = [dict objectForKey:@"result"];
                if([resultDict isKindOfClass:[NSDictionary class]])
                {
                    QuotaImprovementModel *quotaModel = [[QuotaImprovementModel alloc] initWithDictionary:resultDict];
                    sharedData.quotaImprovementData = quotaModel;
                    [quotaModel release];
                    quotaModel = nil;
                }
                
                isSuccess = YES;
            }
            else {
                isSuccess = NO;
                reason = [dict objectForKey:@"err_desc"];
            }
        }
        else
        {
            isSuccess = NO;
        }
        
        if([reason isEqualToString:@""])
        {
            reason = [Language get:@"general_error_message" alter:nil];
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

#pragma mark - RECOMMENDED PACKAGE
- (void)parseRecommendedPackageData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *recommendedPackageDict = responseJSON;
    NSString *status = [recommendedPackageDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSArray *items = [recommendedPackageDict valueForKeyPath:@"result"];
        if ([items count] != 0) {
            NSEnumerator *enumerator = [items objectEnumerator];
            NSDictionary* item;
            NSMutableArray *recommendedDataTmp = [[NSMutableArray alloc] init];
            while ((item = (NSDictionary*)[enumerator nextObject])) {
                
                MenuModel *recommendedModel     = [[MenuModel alloc] init];
                recommendedModel.menuName       = [item objectForKey:@"0"];
                NSDictionary *attr = [item objectForKey:@"@attributes"];
                recommendedModel.href           = [attr objectForKey:@"href"];
                recommendedModel.menuId         = [attr objectForKey:@"id"];
                recommendedModel.packageId      = [attr objectForKey:@"pkgid"];
                recommendedModel.packageGroup   = [attr objectForKey:@"pkggroup"];
                recommendedModel.volume         = [attr objectForKey:@"volume"];
                recommendedModel.duration       = [attr objectForKey:@"duration"];
                recommendedModel.packageType    = [attr objectForKey:@"pkgtype"];
                recommendedModel.price          = [attr objectForKey:@"price"];
                recommendedModel.desc           = [attr objectForKey:@"desc"];
                //recommendedModel.groupId        = self.selectedDataModel.groupId;
                //recommendedModel.groupName      = self.selectedDataModel.menuName;
                recommendedModel.open           = NO;
                
                [recommendedDataTmp addObject:recommendedModel];
                
                [recommendedModel release];
                recommendedModel = nil;
            }
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.recommendedData = [NSArray arrayWithArray:recommendedDataTmp];
            [recommendedDataTmp release];
        }
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[recommendedPackageDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - UPGRADE PACKAGE
- (void)parseUpgradePackageData:(NSString*)data
{
    id responseJSON = [data JSONValue];
    NSDictionary *upgradePackageDict = responseJSON;
    NSString *status = [upgradePackageDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSArray *items = [upgradePackageDict valueForKeyPath:@"result"];
        if ([items count] != 0) {
            NSEnumerator *enumerator = [items objectEnumerator];
            NSDictionary* item;
            NSMutableArray *upgradeDataTmp = [[NSMutableArray alloc] init];
            while ((item = (NSDictionary*)[enumerator nextObject])) {
                
                MenuModel *upgradeModel     = [[MenuModel alloc] init];
                upgradeModel.menuName       = [item objectForKey:@"0"];
                NSDictionary *attr = [item objectForKey:@"@attributes"];
                upgradeModel.href           = [attr objectForKey:@"href"];
                upgradeModel.oriPackage     = [attr objectForKey:@"oripkg"];
                upgradeModel.menuId         = [attr objectForKey:@"id"];
                upgradeModel.packageId      = [attr objectForKey:@"pkgid"];
                upgradeModel.packageGroup   = [attr objectForKey:@"pkggroup"];
                upgradeModel.volume         = [attr objectForKey:@"volume"];
                upgradeModel.duration       = [attr objectForKey:@"duration"];
                upgradeModel.packageType    = [attr objectForKey:@"pkgtype"];
                upgradeModel.price          = [attr objectForKey:@"price"];
                upgradeModel.desc           = [attr objectForKey:@"desc"];
                //recommendedModel.groupId        = self.selectedDataModel.groupId;
                //recommendedModel.groupName      = self.selectedDataModel.menuName;
                upgradeModel.open           = NO;
                
                [upgradeDataTmp addObject:upgradeModel];
                
                [upgradeModel release];
                upgradeModel = nil;
            }
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.upgradePackageData = [NSArray arrayWithArray:upgradeDataTmp];
            [upgradeDataTmp release];
        }
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[upgradePackageDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - SURVEY
- (void)parseSurveyData:(NSString*)data
{
    id responseJSON = [data JSONValue];
    NSDictionary *surveyDict = responseJSON;
    NSString *status = [surveyDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        
        NSArray *surveyArray = [surveyDict objectForKey:@"result"];
        if ([surveyArray count] != 0) {
            NSEnumerator *enumerator = [surveyArray objectEnumerator];
            NSDictionary *item;
            
            NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
            
            while (item = (NSDictionary*)[enumerator nextObject]) {
                
                SurveyModel *surveyModel    = [[SurveyModel alloc] init];
                surveyModel.surveyId        = [item valueForKey:@"id"];
                surveyModel.typeOption      = [item valueForKey:@"type_option"];
                surveyModel.question        = [item valueForKey:@"question"];
                surveyModel.minAnswer       = [item valueForKey:@"min_answer"];
                surveyModel.maxAnswer       = [item valueForKey:@"max_answer"];
                surveyModel.validationMsg   = [item valueForKey:@"validation_msg"];
                
                NSArray *option = [item valueForKey:@"option"];
                if ([option count] > 0) {
                    NSEnumerator *enumerator2 = [option objectEnumerator];
                    NSDictionary *item2;
                    
                    NSMutableArray *contentTmp2 = [[NSMutableArray alloc] init];
                    
                    while (item2 = (NSDictionary*)[enumerator2 nextObject]) {
                        LabelValueModel *labelValueModel = [[LabelValueModel alloc] init];
                        labelValueModel.label = [item2 valueForKey:@"label"];
                        labelValueModel.value = [item2 valueForKey:@"value"];
                        
                        [contentTmp2 addObject:labelValueModel];
                        [labelValueModel release];
                        labelValueModel = nil;
                    }
                    
                    surveyModel.option = [NSArray arrayWithArray:contentTmp2];
                    [contentTmp2 release];
                }
                
                [contentTmp addObject:surveyModel];
                [surveyModel release];
                surveyModel = nil;
            }
            
            DataManager *dataManager = [DataManager sharedInstance];
            dataManager.surveyData = [NSArray arrayWithArray:contentTmp];
            [contentTmp release];
        }
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[surveyDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - LAST TRANSACTION
- (void)parseLastTransactionData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *lastTransactionDict = responseJSON;
    NSString *status = [lastTransactionDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSArray *lastTransactionArray = [lastTransactionDict objectForKey:@"result"];
        if ([lastTransactionArray count] != 0) {
            NSEnumerator *enumerator = [lastTransactionArray objectEnumerator];
            NSDictionary *item;
            
            NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
            
            while (item = (NSDictionary*)[enumerator nextObject]) {
                TransactionHistoryModel *historyModel = [[TransactionHistoryModel alloc] init];
                
                NSString *destination = [item valueForKey:@"history_destination"];
                if (!!destination && ![destination isEqual:[NSNull null]]) {
                    historyModel.destination = destination;
                }
                else {
                    historyModel.destination = @"";
                }
                
                NSString *type = [item valueForKey:@"history_type"];
                if (!!type && ![type isEqual:[NSNull null]]) {
                    historyModel.type  = type;
                }
                else {
                    historyModel.type = @"";
                }
                
                NSString *typeLabel = [item valueForKey:@"history_typeLabel"];
                if (!!typeLabel && ![typeLabel isEqual:[NSNull null]]) {
                    historyModel.typeLabel = typeLabel;
                }
                else {
                    historyModel.typeLabel = @"";
                }
                
                NSString *duration = [item valueForKey:@"history_duration"];
                if (!!duration && ![duration isEqual:[NSNull null]]) {
                    historyModel.duration  = duration;
                }
                else {
                    historyModel.duration = @"";
                }
                
                NSString *durationType = [item valueForKey:@"history_duration_type"];
                if (!!durationType && ![durationType isEqual:[NSNull null]]) {
                    historyModel.durationType  = durationType;
                }
                else {
                    historyModel.durationType = @"";
                }
                
                NSString *callCharge = [item valueForKey:@"history_call_charge"];
                if (!!callCharge && ![callCharge isEqual:[NSNull null]]) {
                    historyModel.callCharge  = callCharge;
                }
                else {
                    historyModel.callCharge = @"";
                }
                
                NSString *time = [item valueForKey:@"history_time"];
                if (!!time && ![time isEqual:[NSNull null]]) {
                    historyModel.time  = time;
                }
                else {
                    historyModel.time = @"";
                }
                
                NSString *charging = [item valueForKey:@"history_charging"];
                if (!!charging && ![charging isEqual:[NSNull null]]) {
                    historyModel.charging = charging;
                }
                else {
                    historyModel.charging = @"";
                }
                
                [contentTmp addObject:historyModel];
                [historyModel release];
                historyModel = nil;
            }
            
            DataManager *dataManager = [DataManager sharedInstance];
            dataManager.lastTransactionData = [NSArray arrayWithArray:contentTmp];
            [contentTmp release];
        }
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[lastTransactionDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - PROMO
- (void)parsePromoData:(NSString*)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *promoDict = responseJSON;
        promoDict = [self removeNullResponse:[promoDict mutableCopy]];
        NSString *status = [promoDict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"]) {
            NSArray *promoArray = [promoDict objectForKey:@"result"];
            if ([promoArray count] != 0) {
                NSEnumerator *enumerator = [promoArray objectEnumerator];
                NSDictionary *item;
                
                NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
                
                while (item = (NSDictionary*)[enumerator nextObject]) {
                    PromoModel *promoModel = [[PromoModel alloc] init];
                    
                    promoModel.promoId       = [item valueForKey:kPromo_id];
                    promoModel.promoTitle    = [item valueForKey:kPromo_title];
                    promoModel.promoBody     = [item valueForKey:kPromo_body];
                    promoModel.promoDateTime = [item valueForKey:kPromo_dateTime];
                    promoModel.promoUrl      = [item valueForKey:kPromo_URL];
                    promoModel.promoStatus   = [item valueForKey:kPromo_status];
                    promoModel.promoShortDesc = [item valueForKey:kPromo_shortDesc];
                    promoModel.promoMenuId   = [item valueForKey:kPromo_menuID];
                    
                    // TODO : V1.9
                    // PROMO MANAGEMENT
                    promoModel.promoShortDesc = [item objectForKey:kPromo_shortDesc];
                    promoModel.promoBtnAction = [item objectForKey:kPromo_buttonAction];
                    promoModel.promoBtnGenerate = [item objectForKey:kPromo_buttonGenerate];
                    promoModel.promoChild = [item objectForKey:kPromo_child];
                    promoModel.promoStart = [item objectForKey:kPromo_start];
                    promoModel.promoStatus = [item objectForKey:kPromo_status];
                    promoModel.promoTotalChild = [[item objectForKey:kPromo_totChild] integerValue];
                    
                    if(promoModel.promoTotalChild > 0)
                    {
                        NSArray *array = [item objectForKey:[item valueForKey:kPromo_id]];
                        if([array isKindOfClass:[NSArray class]])
                        {
                            promoModel.promoChild = [self generateChildPromoWithChildArray:array];
                        }
                    }
                    
                    promoModel.promoType = [item objectForKey:kPromo_type];
                    promoModel.promoTypeLink = [item objectForKey:kPromo_typeLink];
                    promoModel.promoShow = [item objectForKey:kPromo_show];
                    promoModel.promoParentID = [item objectForKey:kPromo_parentID];
                    promoModel.promoSequence = [item objectForKey:kPromo_sequence];
                    promoModel.promoOff = [item objectForKey:kPromo_off];
                    promoModel.promoMsisdnType = [item objectForKey:kPromo_msisdnType];
                    promoModel.promoImage = [item objectForKey:kPromo_image];
                    promoModel.promoEnd = [item objectForKey:kPromo_end];
                    promoModel.promoVoucherDesc = [item objectForKey:kPromo_voucherDesc];
                    promoModel.promoVoucher = [item objectForKey:kPromo_voucher];
                    promoModel.promoBtnVoucher = [item objectForKey:kPromo_btnVoucher];
                    promoModel.promoBtnTerm = [item objectForKey:kPromo_btnTerm];
                    promoModel.promoTermDesc = [item objectForKey:kPromo_termDesc];
                    promoModel.promoRelatedInfo = [item objectForKey:kPromo_relatedInfo];
                    promoModel.promoRelatedInfoURL = [item objectForKey:kPromo_relatedInfoURL];
                    
                    [contentTmp addObject:promoModel];
                    [promoModel release];
                    promoModel = nil;
                }
                
                DataManager *dataManager = [DataManager sharedInstance];
                dataManager.promoData = [NSArray arrayWithArray:contentTmp];
                [contentTmp release];
            }
            
            isSuccess = YES;
        }
        else {
            reason = [NSString stringWithFormat:@"%@",[promoDict valueForKey:@"err_desc"]];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];

    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception %@", [exception reason]);
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:@""];
    }
}

-(NSArray *)generateChildPromoWithChildArray:(NSArray *)childArray
{
    @autoreleasepool
    {
        @try {
            NSMutableArray *childs = [[NSMutableArray alloc] init];
            for (NSDictionary *dict in childArray)
            {
                PromoModel *childModel = [[PromoModel alloc] init];
                childModel.promoId       = [dict valueForKey:kPromo_id];
                childModel.promoTitle    = [dict valueForKey:kPromo_title];
                childModel.promoBody     = [dict valueForKey:kPromo_body];
                childModel.promoDateTime = [dict valueForKey:kPromo_dateTime];
                childModel.promoUrl      = [dict valueForKey:kPromo_URL];
                childModel.promoStatus   = [dict valueForKey:kPromo_status];
                childModel.promoShortDesc = [dict valueForKey:kPromo_shortDesc];
                childModel.promoMenuId   = [dict valueForKey:kPromo_menuID];
                childModel.promoBtnAction = [dict objectForKey:kPromo_buttonAction];
                childModel.promoBtnGenerate = [dict objectForKey:kPromo_buttonGenerate];
                childModel.promoTotalChild = [[dict objectForKey:kPromo_totChild] integerValue];
                if(childModel.promoTotalChild > 0)
                {
                    NSArray *array = [dict objectForKey:[dict valueForKey:kPromo_id]];
                    if([array isKindOfClass:[NSArray class]])
                    {
                        childModel.promoChild = [self generateChildPromoWithChildArray:array];
                    }
                }
                childModel.promoStart = [dict objectForKey:kPromo_start];
                childModel.promoStatus = [dict objectForKey:kPromo_status];
                childModel.promoType = [dict objectForKey:kPromo_type];
                childModel.promoTypeLink = [dict objectForKey:kPromo_typeLink];
                childModel.promoShow = [dict objectForKey:kPromo_show];
                childModel.promoParentID = [dict objectForKey:kPromo_parentID];
                childModel.promoSequence = [dict objectForKey:kPromo_sequence];
                childModel.promoOff = [dict objectForKey:kPromo_off];
                childModel.promoMsisdnType = [dict objectForKey:kPromo_msisdnType];
                childModel.promoImage = [dict objectForKey:kPromo_image];
                childModel.promoEnd = [dict objectForKey:kPromo_end];
                childModel.promoVoucherDesc = [dict objectForKey:kPromo_voucherDesc];
                childModel.promoVoucher = [dict objectForKey:kPromo_voucher];
                childModel.promoBtnVoucher = [dict objectForKey:kPromo_btnVoucher];
                childModel.promoBtnTerm = [dict objectForKey:kPromo_btnTerm];
                childModel.promoTermDesc = [dict objectForKey:kPromo_termDesc];
                childModel.promoRelatedInfo = [dict objectForKey:kPromo_relatedInfo];
                childModel.promoRelatedInfoURL = [dict objectForKey:kPromo_relatedInfoURL];
                
                
                [childs addObject:childModel];
            }
            return [childs copy];
        }
        @catch (NSException *exception) {
            NSLog(@"exception %@", [exception reason]);
        }
    }
    return nil;
}

#pragma mark - NOTIFICATION
// TODO : V1.9
- (void)parseGetUnreadNotificationData:(NSString*)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *notifDict = responseJSON;
        notifDict = [self removeNullResponse:[notifDict mutableCopy]];
        NSString *status = [notifDict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"]) {
            
            DataManager *sharedData = [DataManager sharedInstance];
            // Unread Message
            NSString *unreadString = [notifDict objectForKey:@"unread"];
            sharedData.unreadMsg = [unreadString intValue];
            isSuccess = YES;
        }
        else {
            reason = [NSString stringWithFormat:@"%@",[notifDict valueForKey:@"err_desc"]];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseGetUnreadNotificationDone:)])
            [_delegate parseGetUnreadNotificationDone:[NSDictionary dictionaryWithObject:@"1" forKey:@"success"]];

    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception %@", [exception reason]);
        if ([_delegate respondsToSelector:@selector(parseGetUnreadNotificationDone:)])
            [_delegate parseGetUnreadNotificationDone:[NSDictionary dictionaryWithObject:@"0" forKey:@"success"]];
    }
}

- (void)parseNotificationData:(NSString*)data isDetailNotif:(BOOL)isDetail {
    id responseJSON = [data JSONValue];
    NSDictionary *notifDict = responseJSON;
    NSString *status = [notifDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    
    if ([status isEqualToString:@"1"]) {
        
        DataManager *sharedData = [DataManager sharedInstance];
        
        if (isDetail) {
            NSDictionary *item = [notifDict valueForKeyPath:@"result"];
            NotifModel *notifModel  = [[NotifModel alloc] init];
            
            notifModel.idNotif      = [item objectForKey:@"mailboxID"];
            notifModel.message      = [item objectForKey:@"briefMessage"];
            notifModel.dateTime     = [item objectForKey:@"timestamp"];
            notifModel.readFlag     = [item objectForKey:@"readFlag"];
            notifModel.notifType    = [item objectForKey:@"notificationType"];
            notifModel.status       = [item objectForKey:@"notificationCondition"];
            notifModel.source       = [item objectForKey:@"contactSource"];
            notifModel.serviceId    = [item objectForKey:@"serviceID"];
            notifModel.from         = [item objectForKey:@"briefTitle"];
            notifModel.detailMessage = [item objectForKey:@"message"];
            notifModel.month        = [item objectForKey:@"formated_month"];
            
            sharedData.detailNotificationData = notifModel;
            [notifModel release];
            
            // Update Unread Message
            if ([notifModel.readFlag isEqualToString:@"UNREAD"]) {
                sharedData.unreadMsg--;
            }
        }
        else {
            NSArray *items = [notifDict valueForKeyPath:@"result"];
            
            NSEnumerator *enumerator = [items objectEnumerator];
            NSDictionary* item;
            NSMutableArray *notifDataTmp = [[NSMutableArray alloc] init];
            while (item = (NSDictionary*)[enumerator nextObject]) {
                NotifModel *notifModel  = [[NotifModel alloc] init];
                
                notifModel.idNotif      = [item objectForKey:@"mailboxID"];
                notifModel.message      = [item objectForKey:@"briefMessage"];
                notifModel.dateTime     = [item objectForKey:@"timestamp"];
                notifModel.readFlag     = [item objectForKey:@"readFlag"];
                notifModel.notifType    = [item objectForKey:@"notificationType"];
                notifModel.status       = [item objectForKey:@"notificationCondition"];
                notifModel.source       = [item objectForKey:@"contactSource"];
                notifModel.serviceId    = [item objectForKey:@"serviceID"];
                notifModel.from         = [item objectForKey:@"briefTitle"];
                notifModel.month        = [item objectForKey:@"formated_month"];
                
                [notifDataTmp addObject:notifModel];
                
                [notifModel release];
                notifModel = nil;
            }
            
            sharedData.notificationData = [NSArray arrayWithArray:notifDataTmp];
            [notifDataTmp release];
            
            // Unread Message
            NSString *unreadString = [notifDict objectForKey:@"unread"];
            sharedData.unreadMsg = [unreadString intValue];
        }
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[notifDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - NOTIFICATION DELETE
- (void)parseNotificationDeleteData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *notifParamDict = responseJSON;
    NSString *status = [notifParamDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[notifParamDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - REPLY NOTIFICATION
- (void)parseReplyNotificationData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *notifParamDict = responseJSON;
    NSString *status = [notifParamDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSString *desc = [notifParamDict valueForKey:@"statusdesc"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.replyNotifDesc = desc;
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[notifParamDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - ADS
- (void)parseAdsData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *adsDict = responseJSON;
    NSString *status = [adsDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    NSArray *adsData = nil;
    if ([status isEqualToString:@"1"]) {
        NSArray *items = [adsDict valueForKeyPath:@"result"];
        
        if (!!items && ![items isEqual:[NSNull null]]) {
            NSEnumerator *enumerator = [items objectEnumerator];
            NSDictionary* item;
            NSMutableArray *adsDataTmp = [[NSMutableArray alloc] init];
            while (item = (NSDictionary*)[enumerator nextObject]) {
                AdsModel *adsModel  = [[AdsModel alloc] init];
                adsModel.type       = [item objectForKey:@"type"];
                adsModel.imageURL   = [item objectForKey:@"img"];
                adsModel.text       = [item objectForKey:@"txt"];
                adsModel.actionURL  = [item objectForKey:@"url"];
                
                [adsDataTmp addObject:adsModel];
                
                [adsModel release];
                adsModel = nil;
            }
            
            //DataManager *sharedData = [DataManager sharedInstance];
            //sharedData.adsData = [NSArray arrayWithArray:adsDataTmp];
            
            adsData = [NSArray arrayWithArray:adsDataTmp];
            [adsDataTmp release];
        }
        
        isSuccess = YES;
    }
    else {
        //reason = [NSString stringWithFormat:@"%@",[adsDict valueForKey:@"err_desc"]];
        reason = [NSString stringWithFormat:@"%@",[adsDict valueForKey:@"message"]];
        isSuccess = NO;
    }
    
    /*
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];*/
    
    // change by iNot 17 May 2013
    
    NSString *resultKey = RESULT_KEY;
    NSNumber *resultValue = [NSNumber numberWithBool:isSuccess];
    
    NSString *reasonKey = REASON_KEY;
    NSString *reasonValue = reason;
    
    NSString *dataKey = DATA_KEY;
    NSArray *dataValue = adsData;
    
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                          resultValue, resultKey,
                          reasonValue, reasonKey,
                          dataValue, dataKey,
                          nil];
    
    if ([_delegate respondsToSelector:@selector(parseAdsDone:)])
        [_delegate parseAdsDone:dict];
    //
    
    /*
    if ([_delegate respondsToSelector:@selector(parseAdsDone:withReason:)])
        [_delegate parseAdsDone:isSuccess withReason:reason];*/
}

#pragma mark - HOT OFFER BANNER
- (void)parseHotOfferBannerData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *adsDict = responseJSON;
    NSString *status = [adsDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    NSArray *adsData = nil;
    if ([status isEqualToString:@"1"]) {
        NSArray *items = [adsDict valueForKeyPath:@"result"];
        
        if (!!items && ![items isEqual:[NSNull null]]) {
            NSEnumerator *enumerator = [items objectEnumerator];
            NSDictionary* item;
            NSMutableArray *adsDataTmp = [[NSMutableArray alloc] init];
            while (item = (NSDictionary*)[enumerator nextObject]) {
                
                MenuModel *hotOfferBannerModel  = [[MenuModel alloc] init];
                hotOfferBannerModel.href         = [item valueForKey:@"href"];
                hotOfferBannerModel.hrefweb      = [item valueForKey:@"hrefweb"];
                hotOfferBannerModel.menuId       = [item valueForKey:@"id"];
                hotOfferBannerModel.parentId     = [item valueForKey:@"parentid"];
                hotOfferBannerModel.groupId      = [item valueForKey:@"gid"];
                hotOfferBannerModel.groupName    = [item valueForKey:@"group"];
                if ([hotOfferBannerModel.groupName isEqual:[NSNull null]]) {
                    hotOfferBannerModel.groupName = @"";
                }
                hotOfferBannerModel.desc         = [item valueForKey:@"desc"];
                if ([hotOfferBannerModel.desc isEqual:[NSNull null]]) {
                    hotOfferBannerModel.desc = @"";
                }
                hotOfferBannerModel.duration     = [item valueForKey:@"duration"];
                hotOfferBannerModel.price        = [item valueForKey:@"price"];
                hotOfferBannerModel.volume       = [item valueForKey:@"volume"];
                hotOfferBannerModel.packageId    = [item valueForKey:@"pkgid"];
                hotOfferBannerModel.packageType  = [item valueForKey:@"pkgtype"];
                hotOfferBannerModel.packageGroup = [item valueForKey:@"pkggroup"];
                hotOfferBannerModel.tooltips     = [item valueForKey:@"tooltips"];
                hotOfferBannerModel.topDesc      = [item valueForKey:@"topdesc"];
                hotOfferBannerModel.icon         = [item valueForKey:@"icon"];
                if ([hotOfferBannerModel.icon isEqual:[NSNull null]]) {
                    hotOfferBannerModel.icon = @"";
                }
                hotOfferBannerModel.hex          = [item valueForKey:@"hex"];
                hotOfferBannerModel.confirmDesc  = [item valueForKey:@"confirmDesc"];
                hotOfferBannerModel.banner       = [item valueForKey:@"banner"];
                hotOfferBannerModel.menuName     = [item valueForKey:@"name"];
                hotOfferBannerModel.open         = NO;
                
                [adsDataTmp addObject:hotOfferBannerModel];
                
                [hotOfferBannerModel release];
                hotOfferBannerModel = nil;
            }
            
            adsData = [NSArray arrayWithArray:adsDataTmp];
            [adsDataTmp release];
        }
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[adsDict valueForKey:@"message"]];
        isSuccess = NO;
    }
    
    // change by iNot 17 May 2013
    NSString *resultKey = RESULT_KEY;
    NSNumber *resultValue = [NSNumber numberWithBool:isSuccess];
    
    NSString *reasonKey = REASON_KEY;
    NSString *reasonValue = reason;
    
    NSString *dataKey = DATA_KEY;
    NSArray *dataValue = adsData;
    
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.hotOfferBannerData = [NSArray arrayWithArray:adsData];
    
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                          resultValue, resultKey,
                          reasonValue, reasonKey,
                          dataValue, dataKey,
                          nil];
    
    if ([_delegate respondsToSelector:@selector(parseAdsDone:)])
        [_delegate parseAdsDone:dict];
}

#pragma mark - BALANCE ITEMS
- (void)parseBalanceItemsData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *balanceItemsDict = responseJSON;
    NSString *status = [balanceItemsDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSArray *items = [balanceItemsDict valueForKeyPath:@"result"];
        
        NSEnumerator *enumerator = [items objectEnumerator];
        NSDictionary* item;
        NSMutableArray *balanceItemsDataTmp = [[NSMutableArray alloc] init];
        while (item = (NSDictionary*)[enumerator nextObject]) {
            LabelValueModel *labelValueModel = [[LabelValueModel alloc] init];
            labelValueModel.label = [item objectForKey:@"label"];
            labelValueModel.value = [item objectForKey:@"value"];
            
            [balanceItemsDataTmp addObject:labelValueModel];
            
            [labelValueModel release];
            labelValueModel = nil;
        }
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.balanceItemsData = [NSArray arrayWithArray:balanceItemsDataTmp];
        [balanceItemsDataTmp release];
        
        isSuccess = YES;
        
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[balanceItemsDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - BALANCE TRANSFER
- (void)parseBalanceTransferData:(NSString*)data {
    //NSLog(@"data = %@",data);
    id responseJSON = [data JSONValue];
    NSDictionary *balanceTransferDict = responseJSON;
    NSString *status = [balanceTransferDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        
        NSDictionary *balanceTransferResult = [balanceTransferDict valueForKeyPath:@"result"];
        
        BalanceTransferModel *balanceTransferModel = [[BalanceTransferModel alloc] init];
        balanceTransferModel.msisdnTo = [balanceTransferResult objectForKey:@"msisdnto"];
        balanceTransferModel.amount   = [balanceTransferResult objectForKey:@"amount"];
        balanceTransferModel.charge   = [balanceTransferResult objectForKey:@"charge"];
        balanceTransferModel.balanceNew          = [balanceTransferResult objectForKey:@"newbalance"];
        balanceTransferModel.targetOldActiveStop = [balanceTransferResult objectForKey:@"targetoldactivestop"];
        balanceTransferModel.targetNewActiveStop = [balanceTransferResult objectForKey:@"targetnewactivestop"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.balanceTransferData = balanceTransferModel;
        
        [balanceTransferModel release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[balanceTransferDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - EXTEND VALIDITY
- (void)parseExtendValidityData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *extendValidityDict = responseJSON;
    NSString *status = [extendValidityDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        
        ExtendValidityModel *extendValidityModel = [[ExtendValidityModel alloc] init];
        extendValidityModel.statusText = [extendValidityDict valueForKey:@"statustext"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.extendValidityData = extendValidityModel;
        
        [extendValidityModel release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[extendValidityDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - PAYMENT PARAM
- (void)parsePaymentParamData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *paymentParamDict = responseJSON;
    NSString *status = [paymentParamDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        
        NSMutableDictionary *paramDict = [[NSMutableDictionary alloc] init];
        
        NSDictionary *items = [paymentParamDict valueForKey:@"result"];
        // Amount
        NSArray *amount = [items valueForKey:@"amount"];
        if ([amount count] > 0) {
            NSEnumerator *enumerator = [amount objectEnumerator];
            NSDictionary *item;
            NSMutableArray *amountDataTmp = [[NSMutableArray alloc] init];
            while (item = (NSDictionary*)[enumerator nextObject]) {
                LabelValueModel *labelValueModel = [[LabelValueModel alloc] init];
                labelValueModel.label = [item objectForKey:@"label"];
                labelValueModel.value = [item objectForKey:@"value"];
                
                [amountDataTmp addObject:labelValueModel];
                
                [labelValueModel release];
                labelValueModel = nil;
            }
            
            [paramDict setObject:amountDataTmp forKey:AMOUNT_KEY];
            [amountDataTmp release];
        }
        
        // Bank
        NSArray *bank = [items valueForKey:@"bank"];
        if ([bank count] > 0) {
            NSEnumerator *enumerator = [bank objectEnumerator];
            NSDictionary *item;
            NSMutableArray *bankDataTmp = [[NSMutableArray alloc] init];
            while (item = (NSDictionary*)[enumerator nextObject]) {
                BankModel *bankModel = [[BankModel alloc] init];
                bankModel.bankId     = [item objectForKey:@"id"];
                bankModel.value      = [item objectForKey:@"value"];
                bankModel.label      = [item objectForKey:@"label"];
                bankModel.methodId   = [item objectForKey:@"method_id"];
                bankModel.urlRef     = [item objectForKey:@"url_ref"];
                bankModel.labelRefId = [item objectForKey:@"label_ref_id"];
                bankModel.labelRefEn = [item objectForKey:@"label_ref_en"];
                bankModel.methodName = [item objectForKey:@"method_name"];
                
                [bankDataTmp addObject:bankModel];
                
                [bankModel release];
                bankModel = nil;
            }
            
            [paramDict setObject:bankDataTmp forKey:BANK_KEY];
            [bankDataTmp release];
        }
        
        // Method
        NSArray *method = [items valueForKey:@"method"];
        if ([method count] > 0) {
            NSEnumerator *enumerator = [method objectEnumerator];
            NSDictionary *item;
            NSMutableArray *methodDataTmp = [[NSMutableArray alloc] init];
            while (item = (NSDictionary*)[enumerator nextObject]) {
                LabelValueModel *labelValueModel = [[LabelValueModel alloc] init];
                labelValueModel.itemId = [item objectForKey:@"id"];
                labelValueModel.label  = [item objectForKey:@"label"];
                labelValueModel.value  = [item objectForKey:@"value"];
                
                [methodDataTmp addObject:labelValueModel];
                
                [labelValueModel release];
                labelValueModel = nil;
            }
            
            [paramDict setObject:methodDataTmp forKey:METHOD_KEY];
            [methodDataTmp release];
        }
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.paymentParamData = [NSDictionary dictionaryWithDictionary:paramDict];
        [paramDict release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[paymentParamDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - PAYMENT HELP
- (void)parsePaymentHelpData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *paymentHelpDict = responseJSON;
    NSString *status = [paymentHelpDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.paymentHelpData = [paymentHelpDict objectForKey:@"result"];
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[paymentHelpDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - PAYMENT DEBIT
- (void)parsePaymentDebitData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *paymentDebitDict = responseJSON;
    NSNumber *statusTmp = [paymentDebitDict objectForKey:@"status"];
    int status = [statusTmp intValue];
    //NSLog(@"status = %i",status);
    BOOL isSuccess;
    NSString *reason = @"";
    if (status == 1) {
        
        NSDictionary *paymentDebitResult = [paymentDebitDict valueForKeyPath:@"result"];
        
        PaymentDebitModel *paymentDebitModel = [[PaymentDebitModel alloc] init];
        paymentDebitModel.msisdnTo   = [paymentDebitResult objectForKey:@"msisdnto"];
        paymentDebitModel.amount     = [paymentDebitResult objectForKey:@"amount"];
        paymentDebitModel.transType  = [paymentDebitResult objectForKey:@"transtype"];
        paymentDebitModel.transTypeLabel = [paymentDebitResult objectForKey:@"transtypelabel"];
        paymentDebitModel.bankName   = [paymentDebitResult objectForKey:@"bankname"];
        paymentDebitModel.balance    = [paymentDebitResult objectForKey:@"newbalance"];
        paymentDebitModel.activeDate = [paymentDebitResult objectForKey:@"newactivedate"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.paymentDebitData = paymentDebitModel;
        
        [paymentDebitModel release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[paymentDebitDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - GIFT PACKAGE
- (void)parseGiftPackageData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *giftPackageDict = responseJSON;
    NSString *status = [giftPackageDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[giftPackageDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - BUY PACKAGE
- (void)parseBuyPackageData:(NSString*)data {
    
    id responseJSON = [data JSONValue];
    NSDictionary *buyPackageDict = responseJSON;
    NSString *status = [buyPackageDict objectForKey:@"status"];
    
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        
        BuyPackageModel *buyPackageModel = [[BuyPackageModel alloc] init];
        buyPackageModel.smsId = [buyPackageDict valueForKey:@"smsid"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.buyPackageData = buyPackageModel;
        
        //-----------------------------------------------------------------//
        // set show balance FALSE in order to request Balance in Home page //
        //-----------------------------------------------------------------//
        sharedData.showBalance = NO;
        //-----------------------------------------------------------------//
        
        [buyPackageModel release];
        
        reason = [NSString stringWithFormat:@"%@",[buyPackageDict valueForKey:@"message"]];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[buyPackageDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - CONTACT US
- (void)parseContactUsData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *contactUsDict = responseJSON;
    NSString *status = [contactUsDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSArray *items = [contactUsDict valueForKeyPath:@"result"];
        
        NSEnumerator *enumerator = [items objectEnumerator];
        NSDictionary* item;
        NSMutableArray *contactUsDataTmp = [[NSMutableArray alloc] init];
        while (item = (NSDictionary*)[enumerator nextObject]) {
            
            ContactUsModel *contactUsModel = [[ContactUsModel alloc] init];
            //contactUsModel.href = [item objectForKey:@"href"];
            contactUsModel.label = [item objectForKey:@"title"];
            contactUsModel.value = [item objectForKey:@"contact"];
            //contactUsModel.actionValue = [item objectForKey:@"action_value"];
            [contactUsDataTmp addObject:contactUsModel];
            
            [contactUsModel release];
            contactUsModel = nil;
        }
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.contactUsData = [NSArray arrayWithArray:contactUsDataTmp];
        [contactUsDataTmp release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[contactUsDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - ROAMING
- (void)parseRoamingData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *contactUsDict = responseJSON;
    NSString *status = [contactUsDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSArray *items = [contactUsDict valueForKeyPath:@"result"];
        
        NSEnumerator *enumerator = [items objectEnumerator];
        NSDictionary* item;
        NSMutableArray *contactUsDataTmp = [[NSMutableArray alloc] init];
        while (item = (NSDictionary*)[enumerator nextObject]) {
            RoamingModel *roamingModel = [[RoamingModel alloc] init];
            roamingModel.country = [item objectForKey:@"roaming_country"];
            roamingModel.partner = [item objectForKey:@"roaming_partner"];
            roamingModel.data    = [item objectForKey:@"roaming_data"];
            [contactUsDataTmp addObject:roamingModel];
            
            [roamingModel release];
            roamingModel = nil;
        }
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.roamingData = [NSArray arrayWithArray:contactUsDataTmp];
        [contactUsDataTmp release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[contactUsDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - XL STAR
- (void)parseXLStarData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *pukDict = responseJSON;
    NSString *status = [pukDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSDictionary *resultDict = [pukDict objectForKey:@"result"];
        
        LabelValueModel *pukModel = [[LabelValueModel alloc] init];
        pukModel.label = [resultDict valueForKey:@"message"];
        pukModel.value = [resultDict valueForKey:@"xlstar"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.xlStarData = pukModel;
        [pukModel release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[pukDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - PUK
- (void)parsePUKData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *pukDict = responseJSON;
    NSString *status = [pukDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSDictionary *resultDict = [pukDict objectForKey:@"result"];
        
        PUKModel *pukModel = [[PUKModel alloc] init];
        //pukModel.puk1 = [resultDict valueForKey:@"puk1"];
        //pukModel.puk2 = [resultDict valueForKey:@"puk2"];
        
        pukModel.puk1 = [resultDict valueForKey:@"message"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.pukData = pukModel;
        [pukModel release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[pukDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - DEVICE SETTING
- (void)parseDeviceSettingData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *deviceSettingDict = responseJSON;
    NSString *status = [deviceSettingDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        
        NSDictionary *dataDict = [deviceSettingDict objectForKey:@"result"];
        
        NSMutableDictionary *dictTmp = [[NSMutableDictionary alloc] init];
        NSString *name = [dataDict valueForKey:@"name"];
        [dictTmp setObject:name forKey:@"name"]; // save
        
        NSArray *list = [dataDict objectForKey:@"list"];
        if ([list count] != 0) {
            NSEnumerator *enumerator = [list objectEnumerator];
            NSDictionary *item;
            NSMutableArray *listTmp = [[NSMutableArray alloc] init];
            while (item = (NSDictionary*)[enumerator nextObject]) {
                
                DeviceSettingModel *deviceSettingModel = [[DeviceSettingModel alloc] init];
                deviceSettingModel.desc = [item valueForKey:@"description"]; // save
                
                NSArray *dataArray = [item objectForKey:@"data"];
                if ([dataArray count] != 0) {
                    NSEnumerator *enumeratorData = [dataArray objectEnumerator];
                    NSDictionary *itemData;
                    NSMutableArray *dataTmp = [[NSMutableArray alloc] init];
                    while (itemData = (NSDictionary*)[enumeratorData nextObject]) {
                        
                        LabelValueModel *labelValueModel = [[LabelValueModel alloc] init];
                        labelValueModel.label = [itemData valueForKey:@"label"];
                        labelValueModel.value = [itemData valueForKey:@"value"];
                        
                        [dataTmp addObject:labelValueModel];
                        
                        [labelValueModel release];
                        labelValueModel = nil;
                    }
                    
                    deviceSettingModel.data = [NSArray arrayWithArray:dataTmp]; // save
                    [dataTmp release];
                }
                
                [listTmp addObject:deviceSettingModel];
                [deviceSettingModel release];
                deviceSettingModel = nil;
            }
            
            NSArray *listData = [NSArray arrayWithArray:listTmp];
            [listTmp release];
            
            [dictTmp setObject:listData forKey:@"list"]; // save
        }
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.deviceSettingData = [NSDictionary dictionaryWithDictionary:dictTmp];
        [dictTmp release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[deviceSettingDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - SHOP LOCATION
- (void)parseShopLocationData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *shopLocationDict = responseJSON;
    NSString *status = [shopLocationDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSArray *items = [shopLocationDict valueForKeyPath:@"result.markers"];
        
        NSEnumerator *enumerator = [items objectEnumerator];
        NSDictionary* item;
        NSMutableArray *shopLocationDataTmp = [[NSMutableArray alloc] init];
        while (item = (NSDictionary*)[enumerator nextObject]) {
            ShopLocationModel *shopLocationModel = [[ShopLocationModel alloc] init];
            shopLocationModel.latitude  = [item valueForKey:@"latitude"];
            shopLocationModel.longitude = [item valueForKey:@"longitude"];
            shopLocationModel.title     = [item valueForKey:@"title"];
            shopLocationModel.address   = [item valueForKey:@"content"];
            shopLocationModel.time      = [item valueForKey:@"working_time"];
            
            [shopLocationDataTmp addObject:shopLocationModel];
            [shopLocationModel release];
            shopLocationModel = nil;
        }
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.shopLocationData = [NSArray arrayWithArray:shopLocationDataTmp];
        [shopLocationDataTmp release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[shopLocationDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - FAQ
- (void)parseFAQData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *faqDict = responseJSON;
    NSString *status = [faqDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        
        NSArray *dataArray = [faqDict objectForKey:@"result"];
        if ([dataArray count] != 0) {
            NSEnumerator *enumerator = [dataArray objectEnumerator];
            NSDictionary *item;
            
            NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
            
            while (item = (NSDictionary*)[enumerator nextObject]) {
                FAQResultModel *faqModel = [[FAQResultModel alloc] init];
                faqModel.resultId = [item valueForKey:@"id"];
                faqModel.question = [item valueForKey:@"question"];
                faqModel.answer = [item valueForKey:@"answer"];
                faqModel.open = NO;
                faqModel.isWebView = isContainHTMLTag(faqModel.answer);
                
                [contentTmp addObject:faqModel];
                [faqModel release];
                faqModel = nil;
            }
            
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.faqData = [NSArray arrayWithArray:contentTmp];
            [contentTmp release];
        }
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[faqDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - XL TUNAI BALANCE
- (void)parseXLTunaiBalanceData:(NSString*)data
{
    id responseJSON = [data JSONValue];
    NSDictionary *checkBalanceDict = responseJSON;
    NSString *status = [checkBalanceDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    NSString *errorCode = @"";
    if ([status isEqualToString:@"1"]) {
        NSDictionary *result = [checkBalanceDict objectForKey:@"result"];
        
        CheckBalanceModel *checkBalanceModel = [[CheckBalanceModel alloc] init];
        
        checkBalanceModel.accountType = [result valueForKey:@"accounttype"];
        
        checkBalanceModel.accountState = [result valueForKey:@"accountstate"];
        
        checkBalanceModel.accountStatus = [result valueForKey:@"accountstatus"];
        
        checkBalanceModel.activeStopDate = [result valueForKey:@"activestopdate"];
        
        checkBalanceModel.suspendStopDate = [result valueForKeyPath:@"suspendstopdate"];
        
        checkBalanceModel.disableStopDate = [result valueForKey:@"disablestopdate"];
        
        checkBalanceModel.balance = [result valueForKey:@"balance_amt"];
        
        checkBalanceModel.code = [result valueForKey:@"code"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.xlTunaiBalanceData = checkBalanceModel;
        
        [checkBalanceModel release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[checkBalanceDict valueForKey:@"err_desc"]];
        errorCode = [NSString stringWithFormat:@"%@",[checkBalanceDict valueForKey:@"err_code"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:withErrorCode:)])
        [_delegate parseDataDone:isSuccess withReason:reason withErrorCode:errorCode];
}

#pragma mark - XL TUNAI RELOAD
- (void)parseXLTunaiReloadData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *checkBalanceDict = responseJSON;
    NSString *status = [checkBalanceDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    NSString *errorCode = @"";
    if ([status isEqualToString:@"1"]) {
        isSuccess = YES;
        reason = [NSString stringWithFormat:@"%@",[checkBalanceDict valueForKeyPath:@"result.message"]];
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[checkBalanceDict valueForKey:@"err_desc"]];
        errorCode = [NSString stringWithFormat:@"%@",[checkBalanceDict valueForKey:@"err_code"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:withErrorCode:)])
        [_delegate parseDataDone:isSuccess withReason:reason withErrorCode:errorCode];
}

#pragma mark - CHANGE LANGUAGE
- (void)parseChangeLanguageData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *checkBalanceDict = responseJSON;
    NSString *status = [checkBalanceDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    NSString *errorCode = @"";
    if ([status isEqualToString:@"1"]) {
        NSString *newLang = [checkBalanceDict objectForKey:@"lang"];
        [Language setLanguage:newLang];
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.profileData.language = newLang;
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[checkBalanceDict valueForKey:@"err_desc"]];
        errorCode = [NSString stringWithFormat:@"%@",[checkBalanceDict valueForKey:@"err_code"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:withErrorCode:)])
        [_delegate parseDataDone:isSuccess withReason:reason withErrorCode:errorCode];
}

#pragma mark - TERMS AND CONDITIONS
- (void)parseTermAndConditionData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *termDict = responseJSON;
    NSString *status = [termDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        
        LabelValueModel *model = [[LabelValueModel alloc] init];
        model.label = [termDict valueForKey:@"result"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.termAndConditionData = model;
        [model release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[termDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - USER GUIDE
- (void)parseUserGuideData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *termDict = responseJSON;
    NSString *status = [termDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        
        LabelValueModel *model = [[LabelValueModel alloc] init];
        model.label = [termDict valueForKey:@"result"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.userGuideData = model;
        [model release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[termDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - TRANSACTION HISTORY
- (void)parseTrxHistoryData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *trxHistoryDict = responseJSON;
    NSString *status = [trxHistoryDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSArray *items = [trxHistoryDict valueForKeyPath:@"result"];
        
        NSEnumerator *enumerator = [items objectEnumerator];
        NSDictionary* item;
        NSMutableArray *contactUsDataTmp = [[NSMutableArray alloc] init];
        while (item = (NSDictionary*)[enumerator nextObject]) {
            
            // Ganti ya
            ContactUsModel *contactUsModel = [[ContactUsModel alloc] init];
            contactUsModel.href = [item objectForKey:@"href"];
            contactUsModel.label = [item objectForKey:@"label"];
            contactUsModel.value = [item objectForKey:@"value"];
            contactUsModel.actionValue = [item objectForKey:@"action_value"];
            [contactUsDataTmp addObject:contactUsModel];
            
            [contactUsModel release];
            contactUsModel = nil;
        }
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.trxHistoryData = [NSArray arrayWithArray:contactUsDataTmp];
        [contactUsDataTmp release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[trxHistoryDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - CATEGORY
- (void)parseCategoryData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *categoryDict = responseJSON;
    NSString *status = [categoryDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSDictionary *data = [categoryDict valueForKeyPath:@"result.0"];
        NSDictionary *attributes = [data objectForKey:@"@attributes"];
        
        MenuModel *menuModel    = [[MenuModel alloc] init];
        menuModel.menuId        = [attributes valueForKey:@"id"];
        menuModel.parentId      = [attributes valueForKey:@"parentid"];
        menuModel.desc          = [attributes valueForKey:@"desc"];
        menuModel.href          = [attributes valueForKey:@"href"];
        menuModel.menuName      = [data valueForKey:@"0"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        
        NSMutableArray *menuDataTmp = [[NSMutableArray alloc] init];
        [menuDataTmp addObject:menuModel];
        [menuModel release];
        menuModel = nil;
        
        sharedData.categoryData = [NSArray arrayWithArray:menuDataTmp];
        [menuDataTmp release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[categoryDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - PRODUCT
- (void)parseProductData:(NSString*)data {
    id responseJSON = [data JSONValue];
    NSDictionary *productDict = responseJSON;
    NSString *status = [productDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        NSDictionary *data = [productDict objectForKey:@"result"];
        
        NSDictionary *pageData = [data objectForKey:@"page"];
        NSString *currentPage = [pageData valueForKey:@"current"];
        NSString *totalPage = [pageData valueForKey:@"total_page"];
        
        NSMutableArray *productDataTmp = [[NSMutableArray alloc] init];
        
        for (int i=0; i<5; i++) {
            NSString *key = [NSString stringWithFormat:@"%i",i];
            NSDictionary *productData = [data objectForKey:key];
            ProductModel *productModel = [[ProductModel alloc] init];
            productModel.productName = [productData valueForKey:@"0"];
            productModel.currentPage = currentPage;
            productModel.totalPage = totalPage;
            
            NSDictionary *attributes    = [productData objectForKey:@"@attributes"];
            productModel.categoryName   = [attributes valueForKey:@"category_name"];
            productModel.categoryId     = [attributes valueForKey:@"category_id"];
            productModel.productTypeId  = [attributes valueForKey:@"product_type_id"];
            productModel.productType    = [attributes valueForKey:@"product_type"];
            productModel.productId      = [attributes valueForKey:@"product_id"];
            productModel.productCode    = [attributes valueForKey:@"product_code"];
            productModel.productPrice   = [attributes valueForKey:@"product_price"];
            productModel.priceOri       = [attributes valueForKey:@"price_ori"];
            productModel.productDiscount = [attributes valueForKey:@"product_discount"];
            productModel.incTax         = [attributes valueForKey:@"inc_tax"];
            productModel.valueTax       = [attributes valueForKey:@"value_tax"];
            productModel.productImage   = [attributes valueForKey:@"product_image"];
            productModel.captionImage   = [attributes valueForKey:@"caption_image"];
            productModel.thumbnailImage = [attributes valueForKey:@"thumbnail_image"];
            productModel.productDesc    = [attributes valueForKey:@"product_desc"];
            productModel.startPromo     = [attributes valueForKey:@"start_of_promo"];
            productModel.endPromo       = [attributes valueForKey:@"end_of_promo"];
            productModel.quantity       = [attributes valueForKey:@"quantity"];
            productModel.descStock      = [attributes valueForKey:@"desc_stock"];
            
            [productDataTmp addObject:productModel];
            [productModel release];
            productModel = nil;
        }
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.productData = [NSArray arrayWithArray:productDataTmp];
        [productDataTmp release];
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[productDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - QUOTA METER
- (void)parseQuotaMeterData:(NSString*)data
{
    id responseJSON = [data JSONValue];
    NSDictionary *quotaMeterDict = responseJSON;
    NSString *status = [quotaMeterDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    if ([status isEqualToString:@"1"]) {
        
        NSArray *items = [quotaMeterDict valueForKeyPath:@"result"];
        
        if (!!items && ![items isEqual:[NSNull null]]) {
            NSEnumerator *enumerator = [items objectEnumerator];
            NSDictionary* item;
            NSMutableArray *quotaMeterDataTmp = [[NSMutableArray alloc] init];
            
            int modelIndex = 0;
            
            while (item = (NSDictionary*)[enumerator nextObject]) {
                QuotaMeterModel *quotaMeterModel        = [[QuotaMeterModel alloc] init];
                quotaMeterModel.quotaPackageLabel       = [item valueForKey:@"package_name"];
                quotaMeterModel.serviceId               = [item valueForKey:@"serviceId"];
                
                quotaMeterModel.quotaType               = [[item valueForKey:@"type"] intValue];
                //quotaMeterModel.quotaType               = 1;
                quotaMeterModel.quotaTypeLabel          = [item valueForKey:@"label_type"];
                
                quotaMeterModel.quotaNormalUsageFrom    = [item valueForKey:@"label_nusage_from"];
                quotaMeterModel.quotaNormalUsageTo      = [item valueForKey:@"label_nusage_to"];
                quotaMeterModel.quotaNormalUsageDesc    = [item valueForKey:@"label_desc_nusage"];
                quotaMeterModel.quotaNormalUsagePercent = [item valueForKey:@"nusage"];
                
                quotaMeterModel.quotaCurrentUsageDesc    = [item valueForKey:@"label_desc_usage"];
                quotaMeterModel.quotaCurrentUsagePercent = [item valueForKey:@"usage"];
                
                quotaMeterModel.remainingDaysLabel      = [item valueForKey:@"label_remaining_days"];
                
                quotaMeterModel.hintLabel               = [item valueForKey:@"label_hint"];
                quotaMeterModel.hintDescLabel           = [item valueForKey:@"label_desc_hint"];
                
                quotaMeterModel.buttonLabel             = [item valueForKey:@"label_button"];
                quotaMeterModel.hrefButton              = [item valueForKey:@"href_label_button"];
                
                quotaMeterModel.descLabel               = [item valueForKey:@"label_desc"];
                
                quotaMeterModel.banner                  = [item valueForKey:@"banner"];
                quotaMeterModel.hrefBanner              = [item valueForKey:@"href_banner"];
                
                quotaMeterModel.detailButton            = [item valueForKey:@"label_detail"];
                
                quotaMeterModel.variantId               = [item valueForKey:@"variantId"];
                quotaMeterModel.variantName             = [item valueForKey:@"variantName"];
                
                quotaMeterModel.modelIndex = modelIndex;
                modelIndex++;
                
                NSArray *icons = [item objectForKey:@"icon"];
                //NSLog(@"icons = %@",icons);
                
                if ([icons count] != 0) {
                    NSEnumerator *enumeratorIcon = [icons objectEnumerator];
                    NSDictionary* itemIcon;
                    NSMutableArray *iconsDataTmp = [[NSMutableArray alloc] init];
                    while (itemIcon = (NSDictionary*)[enumeratorIcon nextObject]) {
                        
                        AdsModel *iconModel = [[AdsModel alloc] init];
                        iconModel.imageURL = [itemIcon objectForKey:@"image"];
                        iconModel.actionURL = [itemIcon objectForKey:@"href"];
                        
                        [iconsDataTmp addObject:iconModel];
                        
                        [iconModel release];
                        iconModel = nil;
                    }
                    
                    quotaMeterModel.icon = [NSArray arrayWithArray:iconsDataTmp];
                    
                    [iconsDataTmp release];
                }
                else {
                    quotaMeterModel.icon = nil;
                }
                
                [quotaMeterDataTmp addObject:quotaMeterModel];
                [quotaMeterModel release];
                quotaMeterModel = nil;
            }
            
            DataManager *sharedData = [DataManager sharedInstance];
            
            sharedData.quotaMeterData = [NSArray arrayWithArray:quotaMeterDataTmp];
            [quotaMeterDataTmp release];
        }
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[quotaMeterDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - RECOMMENDED PACKAGE XL
- (void)parseRecommendedPackageXLData:(NSString*)data
{
    id responseJSON = [data JSONValue];
    NSDictionary *adsDict = responseJSON;
    adsDict = [self removeNullResponse:[adsDict mutableCopy]];
    NSString *status = [adsDict objectForKey:@"status"];
    BOOL isSuccess;
    NSString *reason = @"";
    NSArray *adsData = nil;
    if ([status isEqualToString:@"1"]) {
        NSArray *items = [adsDict valueForKeyPath:@"result"];
        
        if (!!items && ![items isEqual:[NSNull null]]) {
            NSEnumerator *enumerator = [items objectEnumerator];
            NSDictionary* item;
            NSMutableArray *adsDataTmp = [[NSMutableArray alloc] init];
            while (item = (NSDictionary*)[enumerator nextObject]) {
                
                MenuModel *hotOfferBannerModel  = [[MenuModel alloc] init];
                hotOfferBannerModel.href         = [item valueForKey:@"href"];
                hotOfferBannerModel.hrefweb      = [item valueForKey:@"hrefweb"];
                hotOfferBannerModel.menuId       = [item valueForKey:@"id"];
                hotOfferBannerModel.parentId     = [item valueForKey:@"parentid"];
                hotOfferBannerModel.groupId      = [item valueForKey:@"gid"];
                hotOfferBannerModel.groupName    = [item valueForKey:@"group"];
                hotOfferBannerModel.desc         = [item valueForKey:@"desc"];
                hotOfferBannerModel.duration     = [item valueForKey:@"duration"];
                hotOfferBannerModel.price        = [item valueForKey:@"price"];
                hotOfferBannerModel.volume       = [item valueForKey:@"volume"];
                hotOfferBannerModel.packageId    = [item valueForKey:@"pkgid"];
                hotOfferBannerModel.packageType  = [item valueForKey:@"pkgtype"];
                hotOfferBannerModel.packageGroup = [item valueForKey:@"pkggroup"];
                hotOfferBannerModel.tooltips     = [item valueForKey:@"tooltips"];
                hotOfferBannerModel.topDesc      = [item valueForKey:@"topdesc"];
                hotOfferBannerModel.icon         = [item valueForKey:@"icon"];
                hotOfferBannerModel.hex          = [item valueForKey:@"hex"];
                hotOfferBannerModel.confirmDesc  = [item valueForKey:@"confirmDesc"];
                hotOfferBannerModel.banner       = [item valueForKey:@"banner"];
                hotOfferBannerModel.menuName     = [item valueForKey:@"name"];
                hotOfferBannerModel.open         = NO;
                // TODO : V1.9
                hotOfferBannerModel.shortDesc = [item objectForKey:@"sort_desc"];
                
                [adsDataTmp addObject:hotOfferBannerModel];
                
                [hotOfferBannerModel release];
                hotOfferBannerModel = nil;
            }
            
            adsData = [NSArray arrayWithArray:adsDataTmp];
            [adsDataTmp release];
        }
        
        isSuccess = YES;
    }
    else {
        reason = [NSString stringWithFormat:@"%@",[adsDict valueForKey:@"err_desc"]];
        isSuccess = NO;
    }
    
    // change by iNot 17 May 2013
    /*
    NSString *resultKey = RESULT_KEY;
    NSNumber *resultValue = [NSNumber numberWithBool:isSuccess];
    
    NSString *reasonKey = REASON_KEY;
    NSString *reasonValue = reason;
    
    NSString *dataKey = DATA_KEY;
    NSArray *dataValue = adsData;*/
    
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.recommendedPackageXLData = [NSArray arrayWithArray:adsData];
    
    /*
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                          resultValue, resultKey,
                          reasonValue, reasonKey,
                          dataValue, dataKey,
                          nil];*/
    
    if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
        [_delegate parseDataDone:isSuccess withReason:reason];
}

#pragma mark - SHARE QUOTA
- (void)parseShareQuotaData:(NSString*)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *checkBalanceDict = responseJSON;
        NSString *status = [checkBalanceDict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        NSString *errorCode = @"";
        if ([status isEqualToString:@"1"]) {
            isSuccess = YES;
            reason = [NSString stringWithFormat:@"%@",[checkBalanceDict valueForKeyPath:@"result.message"]];
        }
        else {
            reason = [NSString stringWithFormat:@"%@",[checkBalanceDict valueForKey:@"err_desc"]];
            errorCode = [NSString stringWithFormat:@"%@",[checkBalanceDict valueForKey:@"err_code"]];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:withErrorCode:)])
            [_delegate parseDataDone:isSuccess withReason:reason withErrorCode:errorCode];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

#pragma mark - QUOTA CALCULATOR
- (void)parseQuotaCalculatorData:(NSString*)data
{
    @try {
        
        id responseJSON = [data JSONValue];
        NSDictionary *quotaCalcDict = responseJSON;
        NSString *status = [quotaCalcDict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"]) {
            NSDictionary *data = [quotaCalcDict objectForKey:@"result"];
            if(data)
            {
                QuotaCalculatorModel *quotaCalcModel = [[QuotaCalculatorModel alloc] init];
                quotaCalcModel.title                 = [data valueForKey:@"title"];
                quotaCalcModel.subtitle              = [data valueForKey:@"subtitle"];
                quotaCalcModel.dailyUsage            = [data valueForKey:@"dailyUsage"];
                quotaCalcModel.weeklyUsage           = [data valueForKey:@"weeklyUsage"];
                quotaCalcModel.monthlyUsage          = [data valueForKey:@"monthlyUsage"];
                quotaCalcModel.choosePackage         = [data valueForKey:@"choosePackage"];
                quotaCalcModel.recommended           = [data valueForKey:@"recommended"];
                quotaCalcModel.daily                 = [data valueForKey:@"daily"];
                quotaCalcModel.weekly                = [data valueForKey:@"weekly"];
                quotaCalcModel.monthly               = [data valueForKey:@"monthly"];
                NSDictionary *totalVolume = [data objectForKey:@"totalVolume"];
                quotaCalcModel.totalVolumeDaily      = [[totalVolume valueForKey:@"daily_value"] doubleValue];
                quotaCalcModel.totalVolumeDailyString = [totalVolume valueForKey:@"daily"];
                
                quotaCalcModel.totalVolumeWeekly     = [[totalVolume valueForKey:@"weekly_value"] doubleValue];
                quotaCalcModel.totalVolumeWeeklyString = [totalVolume valueForKey:@"weekly"];
                
                quotaCalcModel.totalVolumeMonthly    = [[totalVolume valueForKey:@"monthly_value"] doubleValue];
                quotaCalcModel.totalVolumeMonthlyString = [totalVolume valueForKey:@"monthly"];
                
                quotaCalcModel.submitText = [data valueForKey:@"label_submit"];
                quotaCalcModel.calculateText = [data valueForKey:@"calculate"];
                
                NSArray *listUsage = [data objectForKey:@"listUsage"];
                if ([listUsage count] != 0) {
                    NSEnumerator *enumerator = [listUsage objectEnumerator];
                    NSDictionary* item;
                    NSMutableArray *listUsageDataTmp = [[NSMutableArray alloc] init];
                    int sliderTag = 0;
                    while (item = (NSDictionary*)[enumerator nextObject]) {
                        
                        InternetUsageModel *inetModel   = [[InternetUsageModel alloc] init];
                        inetModel.code                  = [item objectForKey:@"code"];
                        inetModel.title                 = [item objectForKey:@"title"];
                        inetModel.color                 = [item objectForKey:@"color"];
                        inetModel.iconUrl               = [item objectForKey:@"icon_url"];
                        inetModel.maxScaleValue         = [[item objectForKey:@"max_scale_value"] intValue];
                        inetModel.labelMax              = [item objectForKey:@"label_max"];
                        inetModel.unitScaleValue        = [[item objectForKey:@"unit_scale_value"] intValue];
                        inetModel.unit                  = [item objectForKey:@"unit"];
                        inetModel.conversion            = [item objectForKey:@"conversion"];
                        inetModel.maxVolumeValue        = [[item objectForKey:@"max_volume_value"] doubleValue];
                        inetModel.maxScalePerdayValue   = [[item objectForKey:@"max_scale_perday_value"] doubleValue];
                        inetModel.descriptionDaily      = [item objectForKey:@"description_daily"];
                        inetModel.descriptionWeekly     = [item objectForKey:@"description_weekly"];
                        inetModel.descriptionMonthly    = [item objectForKey:@"description_monthly"];
                        inetModel.sliderValue = 0.0;
                        inetModel.sliderTag = sliderTag;
                        sliderTag++;
                        
                        [listUsageDataTmp addObject:inetModel];
                        
                        [inetModel release];
                        inetModel = nil;
                    }
                    
                    quotaCalcModel.listUsage = [NSArray arrayWithArray:listUsageDataTmp];
                    
                    [listUsageDataTmp release];
                }
                else {
                    quotaCalcModel.listUsage = nil;
                }
                
                DataManager *sharedData = [DataManager sharedInstance];
                
                sharedData.quotaCalculatorData = quotaCalcModel;
                [quotaCalcModel release];
                
                isSuccess = YES;
            }
            else
            {
                reason = [Language get:@"general_error_message" alter:nil];
                isSuccess = NO;
            }
        }
        else {
            reason = [NSString stringWithFormat:@"%@",[quotaCalcDict valueForKey:@"err_desc"]];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

#pragma mark - MAPPING SUKA-SUKA
-(void)parseMappingPaketSukaSukaData:(NSString *)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *dict = responseJSON;
        NSString *status = [dict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"]) {
            NSDictionary *data = [dict objectForKey:@"result"];
            if(data)
            {
                MappingSukaSukaModel *mappingModel = [[MappingSukaSukaModel alloc] init];
                mappingModel.serviceID      = [data valueForKey:@"service_id"];
                mappingModel.internetDetail = [data valueForKey:@"internet_detail"];
                mappingModel.voiceDetail    = [data valueForKey:@"voice_detail"];
                mappingModel.smsDetail      = [data valueForKey:@"sms_detail"];
                mappingModel.price          = [data valueForKey:@"price"];
                mappingModel.sosmed         = [dict valueForKey:@"sosmed"];
                mappingModel.data           = [data valueForKey:@"data"];
                mappingModel.duration       = [data valueForKey:@"duration"];
                mappingModel.custom_pkg     = [dict valueForKey:@"custom_pkg"];
                mappingModel.last_pkg       = [data valueForKey:@"last_pkg"];
                DataManager *sharedData = [DataManager sharedInstance];
                
                sharedData.mappingSukaSukaData = mappingModel;
                [mappingModel release];
                mappingModel = nil;
                isSuccess = YES;
            }
            else
            {
                isSuccess = NO;
            }
        }
        else {
            reason = [NSString stringWithFormat:@"%@",[dict valueForKey:@"err_desc"]];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];

        //NSLog(@"exception %@",[exception reason]);
    }
}

#pragma mark - METRICS SUKA-SUKA
-(void)parseMatrixPaketSukaSukaData:(NSString *)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *dict = responseJSON;
        NSString *status = [dict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"]) {
            NSDictionary *data = [dict objectForKey:@"result"];
            isSuccess = YES;
            
            PackageSukaMatrixModel *matrixModel = [[PackageSukaMatrixModel alloc] init];
            matrixModel.voice                 = [data valueForKey:@"voice"];
            matrixModel.sms              = [data valueForKey:@"sms"];
            matrixModel.data            = [data valueForKey:@"data"];
            matrixModel.notif           = [dict objectForKey:@"notif"];
            matrixModel.label_submit    = [dict objectForKey:@"label_submit"];
            matrixModel.title_pkg    = [dict objectForKey:@"title_pkg"];
            matrixModel.prev_pkg    = [dict objectForKey:@"prev_pkg"];
            
            DataManager *sharedData = [DataManager sharedInstance];
            
            sharedData.matrixSukaSukaData = matrixModel;
            [matrixModel release];
        }
        else {
            reason = [NSString stringWithFormat:@"%@",[dict valueForKey:@"err_desc"]];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

#pragma mark - PAGE SUKA-SUKA
-(void)parsePageSukaSukaData:(NSString *)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *dict = responseJSON;
        NSString *status = [dict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"]) {
            NSDictionary *data = [dict objectForKey:@"result"];
            isSuccess = YES;
            
            PageSukaModel *pageModel = [[PageSukaModel alloc] init];
            pageModel.pageTitle = [dict valueForKey:@"page_title"];
            pageModel.lblBeliSesukamu = [data valueForKey:@"beli_sesukamu"];
            pageModel.lblUbahNama = [data valueForKey:@"ubah_nama"];
            
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.pageSukaSukaData = pageModel;
            [pageModel release];
        }
        else {
            reason = [NSString stringWithFormat:@"%@",[dict valueForKey:@"err_desc"]];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

#pragma mark - CHECK QUOTA SUKA-SUKA
-(void)parseCheckQuotaSukaSukaData:(NSString *)data
{
    @try
    {
        id responseJSON;
        
//        if([data length] > 0)
//        {
//            responseJSON = [data JSONValue];
//        }
//        else
//        {
//            responseJSON = [str JSONValue];
//        }
        responseJSON = [data JSONValue];
        NSDictionary *dict = responseJSON;
        NSString *status = [dict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        DataManager *sharedData = [DataManager sharedInstance];
        if ([status isEqualToString:@"1"]) {
            NSArray *results = [dict objectForKey:@"result"];
            isSuccess = YES;
            NSMutableArray *resultData = [[NSMutableArray alloc] init];
            RenameSukaSukaModel *renameData = [[RenameSukaSukaModel alloc] init];
            renameData.pageTitle = [dict valueForKey:@"page_title"];
            renameData.pageDesc = [dict valueForKey:@"page_desc"];
            if([results count] > 0)
            {
                for (NSDictionary *data in results) {
                    RenamePackageModel *renameModel = [[RenamePackageModel alloc] init];
                    renameModel.packageName = [data valueForKey:@"name"];
                    renameModel.serviceID = [data valueForKey:@"IdService"];
                    [resultData addObject:renameModel];
                    [renameModel release];
                    renameModel = nil;
                }
            }
            
            renameData.packages = [resultData copy];
            sharedData.renameSukaSukaData = renameData;
            [renameData release];
            renameData = nil;
            [resultData release];
            resultData = nil;
        }
        else {
            RenameSukaSukaModel *renameData = [[RenameSukaSukaModel alloc] init];
            renameData.pageTitle = [dict valueForKey:@"page_title"];
            renameData.pageDesc = [dict valueForKey:@"page_desc"];
            sharedData.renameSukaSukaData = renameData;
            reason = [NSString stringWithFormat:@"%@",[dict valueForKey:@"err_desc"]];
            isSuccess = NO;
            [renameData release];
            renameData = nil;
            if(data)
            {
                NSString *voice = [[dict valueForKey:@"voice"] stringValue];
                if([voice length] > 0)
                {
                    PackageSukaMatrixModel *matrixModel = [[PackageSukaMatrixModel alloc] init];
                    matrixModel.voice                 = [data valueForKey:@"voice"];
                    matrixModel.sms              = [data valueForKey:@"sms"];
                    matrixModel.data            = [data valueForKey:@"data"];
                    matrixModel.notif           = [dict objectForKey:@"notif"];
                    matrixModel.label_submit    = [dict objectForKey:@"label_submit"];
                    matrixModel.title_pkg    = [dict objectForKey:@"title_pkg"];
                    
                    DataManager *sharedData = [DataManager sharedInstance];
                    
                    sharedData.matrixSukaSukaData = matrixModel;
                    [matrixModel release];
                    isSuccess = YES;
                }
                else
                {
                    isSuccess = NO;
                }
            }
            else
            {
                isSuccess = NO;
            }
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception)
    {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

#pragma mark - RENAME PACKAGE SUKA-SUKA
-(void)parseRenameSukaSukaData:(NSString *)data
{
    @try
    {
        BOOL isSuccess;
        NSString *reason = @"";
        NSString *errorCode = @"";
        if([data length] > 0)
        {
            id responseJSON = [data JSONValue];
            NSDictionary *dict = responseJSON;
            NSString *status = [dict objectForKey:@"status"];
            if ([status isEqualToString:@"1"]) {
                isSuccess = YES;
                
                DataManager *sharedData = [DataManager sharedInstance];
                NSString *lang = sharedData.profileData.language;
                if ([lang isEqualToString:@"id"]) {
                    reason = [NSString stringWithFormat:@"%@",[dict valueForKey:@"message_id"]];
                }
                else {
                    reason = [NSString stringWithFormat:@"%@",[dict valueForKey:@"message_en"]];
                }
            }
            else {
                reason = [NSString stringWithFormat:@"%@",[dict valueForKey:@"err_desc"]];
                errorCode = [NSString stringWithFormat:@"%@",[dict valueForKey:@"err_code"]];
                isSuccess = NO;
            }
        }
        else
        {
            isSuccess = NO;
            errorCode = @"999";
            reason = [Language get:@"general_error_message" alter:nil];
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:withErrorCode:)])
            [_delegate parseDataDone:isSuccess withReason:reason withErrorCode:errorCode];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:withErrorCode:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil] withErrorCode:@"999"];
    }
}

#pragma mark - 4G USIM

- (void)parseInfo4GUsimData:(NSString*)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *contactUsDict = responseJSON;
        NSString *status = [contactUsDict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"]) {
            
            NSArray *items = [contactUsDict valueForKeyPath:@"result"];
            
            NSEnumerator *enumerator = [items objectEnumerator];
            NSDictionary* item;
            NSMutableArray *contactUsDataTmp = [[NSMutableArray alloc] init];
            while (item = (NSDictionary*)[enumerator nextObject]) {
                
                PromoModel *model = [[PromoModel alloc] init];
                model.promoTitle     = [item objectForKey:@"title"];
                model.promoShortDesc = [item objectForKey:@"description"];
                model.promoUrl       = [item objectForKey:@"url"];
                model.href           = [item objectForKey:@"href"];
                [contactUsDataTmp addObject:model];
                
                [model release];
                model = nil;
            }
            
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.info4gData = [NSArray arrayWithArray:contactUsDataTmp];
            [contactUsDataTmp release];
            
            isSuccess = YES;
        }
        else {
            reason = [NSString stringWithFormat:@"%@",[contactUsDict valueForKey:@"err_desc"]];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception)
    {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

#pragma mark - Replace 4G USIM
- (void)parseReplace4gUsimData:(NSString*)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *contactUsDict = responseJSON;
        NSString *status = [contactUsDict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"]) {
            reason = [NSString stringWithFormat:@"%@",[contactUsDict valueForKey:@"err_desc"]];
            isSuccess = YES;
        }
        else {
            reason = [NSString stringWithFormat:@"%@",[contactUsDict valueForKey:@"err_desc"]];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception)
    {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

- (void)parseRateComment4gUsimData:(NSString*)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *contactUsDict = responseJSON;
        NSString *status = [contactUsDict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"]) {
            reason = [NSString stringWithFormat:@"%@",[contactUsDict valueForKey:@"message"]];
            isSuccess = YES;
        }
        else {
            reason = [NSString stringWithFormat:@"%@",[contactUsDict valueForKey:@"message"]];
            isSuccess = NO;
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception)
    {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

-(void)parseVoucherPromoData:(NSString *)data
{
    @try
    {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:YES withReason:@""];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

-(void)parseCheckPointPromo:(NSString *)data
{
    @try {
        //"result":{"point":"3","stock":"1","date":"08-06-2015"}}
        id responseJSON = [data JSONValue];
        NSDictionary *dict = responseJSON;
        dict = [self removeNullResponse:[dict mutableCopy]];
        NSString *status = [dict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"]) {
            NSDictionary *resultDict = [dict objectForKey:@"result"];
            if([resultDict isKindOfClass:[NSDictionary class]])
            {
                DataManager *sharedData = [DataManager sharedInstance];
                CheckPointModel *checkPointObj = [[CheckPointModel alloc] init];
                checkPointObj.point = [resultDict objectForKey:kCheckPoint_point];
                checkPointObj.stock = [resultDict objectForKey:kCheckPoint_stock];
                checkPointObj.date = [resultDict objectForKey:kCheckPoint_date];
                sharedData.checkPointData = checkPointObj;
                [checkPointObj release];
                checkPointObj = nil;
            }
            isSuccess = YES;
        }
        else {
            isSuccess = NO;
            reason = [dict objectForKey:@"err_desc"];
        }
        
        if([reason isEqualToString:@""])
        {
            reason = [Language get:@"general_error_message" alter:nil];
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
    
}

-(void)parseWinnerListXtraPromo:(NSString *)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *dict = responseJSON;
        dict = [self removeNullResponse:[dict mutableCopy]];
        NSString *status = [dict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"])
        {
            DataManager *sharedData = [DataManager sharedInstance];
            NSArray *winnerArray = [dict objectForKey:@"result"];
            NSMutableArray *winnerList = [[NSMutableArray alloc] init];
            if([winnerArray isKindOfClass:[NSArray class]])
            {
                for(NSDictionary *winnerDict in winnerArray)
                {
                    WinnerListModel *winnerListObj = [[WinnerListModel alloc] init];
                    winnerListObj.eventdate = [winnerDict objectForKey:kWinnerList_EventDate];
                    winnerListObj.name = [winnerDict objectForKey:kWinnerList_Name];
                    winnerListObj.msisdn = [winnerDict objectForKey:kWinnerList_Msisdn];
                    winnerListObj.urlImage = [winnerDict objectForKey:kWinnerList_UrlImage];
                    [winnerList addObject:winnerListObj];
                    [winnerListObj release];
                    winnerListObj = nil;
                }
            }
            else
            {
                sharedData.winnerMessage = [dict objectForKey:@"message"];
            }
            
            sharedData.winnerListData = winnerList;
            [winnerList release];
            winnerList = nil;
            isSuccess = YES;
        }
        else {
            isSuccess = NO;
            reason = [dict objectForKey:@"err_desc"];
        }
        
        if([reason isEqualToString:@""])
        {
            reason = [Language get:@"general_error_message" alter:nil];
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

-(void)parseCheckBonusXtraPromo:(NSString *)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *dict = responseJSON;
        dict = [self removeNullResponse:[dict mutableCopy]];
        NSString *status = [dict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"])
        {
            DataManager *sharedData = [DataManager sharedInstance];
            NSDictionary *resultDict = [dict objectForKey:@"result"];
            XtraCheckBonusModel *bonusModel = [[XtraCheckBonusModel alloc] initWithDictionary:resultDict];
            sharedData.checkBonusModel = bonusModel;
            isSuccess = YES;
        }
        else {
            isSuccess = NO;
            reason = [dict objectForKey:@"err_desc"];
        }
        
        if([reason isEqualToString:@""])
        {
            reason = [Language get:@"general_error_message" alter:nil];
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

-(void)parseGetBonusXtraPromo:(NSString *)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *dict = responseJSON;
        dict = [self removeNullResponse:[dict mutableCopy]];
        NSString *status = [dict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"])
        {
            reason = [NSString stringWithFormat:@"%@",[dict valueForKey:@"message"]];
            isSuccess = YES;
        }
        else
        {
            reason = [NSString stringWithFormat:@"%@",[dict valueForKey:@"err_desc"]];
            isSuccess = NO;
        }
        
        if([reason isEqualToString:@""])
        {
            reason = [Language get:@"general_error_message" alter:nil];
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

-(void)parseFourGPowerPack:(NSString *)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *dict = responseJSON;
        dict = [self removeNullResponse:[dict mutableCopy]];
        NSLog(@"clean dict = %@",dict);
        BOOL isSuccess;
        NSString *reason = @"";
        if([dict isKindOfClass:[NSDictionary class]])
        {
            NSString *status = [dict objectForKey:@"status"];
            if ([status isEqualToString:@"1"])
            {
                DataManager *sharedData = [DataManager sharedInstance];
                NSDictionary *resultDict = [dict objectForKey:@"result"];
                if([resultDict isKindOfClass:[NSDictionary class]])
                {
                    FourGPackageModel *fourGModel = [[FourGPackageModel alloc] initWithDictionary:resultDict];
                    sharedData.fourGModel = fourGModel;
                    [fourGModel release];
                    fourGModel = nil;
                }
                
                isSuccess = YES;
            }
            else {
                isSuccess = NO;
                reason = [dict objectForKey:@"err_desc"];
            }
        }
        else
        {
            isSuccess = NO;
        }
        
        if([reason isEqualToString:@""])
        {
            reason = [Language get:@"general_error_message" alter:nil];
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

-(void)parseFourGPackageStatus:(NSString *)data
{
    @try
    {
        id responseJSON = [data JSONValue];
        NSDictionary *dict = responseJSON;
        dict = [self removeNullResponse:[dict mutableCopy]];
        NSString *status = [dict objectForKey:@"status"];
        BOOL isSuccess;
        NSString *reason = @"";
        if ([status isEqualToString:@"1"])
        {
            DataManager *sharedData = [DataManager sharedInstance];
            NSDictionary *resultDict = [dict objectForKey:@"result"];
            if([resultDict isKindOfClass:[NSDictionary class]])
            {
                sharedData.isFourGPackage = [[resultDict objectForKey:@"isPackage4g"] boolValue];
                //NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                //[prefs setObject:[NSNumber numberWithBool:sharedData.isFourGPackage] forKey:kIsPackage4G];
                //[prefs synchronize];
                saveDataToPreferenceWithKeyAndValue(kIsPackage4G, [NSNumber numberWithBool:sharedData.isFourGPackage]);
                
                //
                QuotaImprovementModel *quotaModel = [[QuotaImprovementModel alloc] initWithDictionary:resultDict];
                sharedData.quotaImprovementData = quotaModel;
                
                /*
                 * TODO: V1.9.5
                 * Save to preference for caching
                 */
                //saveCustomObjectQuota(sharedData.quotaImprovementData, USAGE_CACHE_KEY);
                NSString *key = USAGE_CACHE_KEY;
                saveDataToPreferenceWithKeyAndValue(key, data);
                
                //-------------------------------//
                
                [quotaModel release];
                quotaModel = nil;
                //
            }
            isSuccess = YES;
        }
        else {
            isSuccess = NO;
            reason = [dict objectForKey:@"err_desc"];
        }
        
        if([reason isEqualToString:@""])
        {
            reason = [Language get:@"general_error_message" alter:nil];
        }
        
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:isSuccess withReason:reason];
    }
    @catch (NSException *exception) {
        if ([_delegate respondsToSelector:@selector(parseDataDone:withReason:)])
            [_delegate parseDataDone:NO withReason:[Language get:@"general_error_message" alter:nil]];
    }
}

@end
