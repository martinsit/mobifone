//
//  PackageSukaMatrixModel.m
//  MyXL
//
//  Created by tyegah on 2/16/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PackageSukaMatrixModel.h"

@implementation PackageSukaMatrixModel

- (void)dealloc {
    [_voice release];
    [_sms release];
    [_data release];
    [_label_submit release];
    [_prev_pkg release];
    [_notif release];
    [_title_pkg release];
    [super dealloc];
}

@end
