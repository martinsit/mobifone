//
//  CheckPointModel.h
//  MyXL
//
//  Created by tyegah on 8/6/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckPointModel : NSObject
extern NSString * const kCheckPoint_point;
extern NSString * const kCheckPoint_stock;
extern NSString * const kCheckPoint_date;

@property (nonatomic, copy) NSString *point;
@property (nonatomic, copy) NSString *stock;
@property (nonatomic, copy) NSString *date;

@end
