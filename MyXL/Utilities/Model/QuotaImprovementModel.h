//
//  QuotaImprovementModel.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 10/19/15.
//  Copyright © 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class QuotaDetailModel;
@class BenefitDataModel;
@class DataBarModel;
@class BenefitVoiceSmsModel;

@interface QuotaImprovementModel : NSObject

//@property (nonatomic, assign) NSInteger activePackage;
@property (nonatomic, copy) NSString *activePackage;
@property (nonatomic, copy) NSString *isUnlimited;
@property (nonatomic, copy) NSString *usage;
@property (nonatomic, assign) float percentUsage;
@property (nonatomic, copy) NSString *remaining;
@property (nonatomic, assign) float percentRemaining;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *activeUntil;
@property (nonatomic, copy) NSString *totalHari;
@property (nonatomic, copy) NSString *sisaHari;
@property (nonatomic, assign) float percentSisaHari;
@property (nonatomic, copy) NSString *regPackage;
@property (nonatomic, copy) NSString *wordingInfoTotalQuota;

@property (nonatomic, retain) NSArray *detail;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end

@interface QuotaDetailModel : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *serviceId;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *confirmDesc;
@property (nonatomic, copy) NSString *showRataHarian;
@property (nonatomic, copy) NSString *showEstimasiHabis;

@property (nonatomic, retain) BenefitDataModel *benefitData;
//@property (nonatomic, retain) BenefitVoiceSmsModel *benefitVoice;
//@property (nonatomic, retain) BenefitVoiceSmsModel *benefitSms;
@property (nonatomic, retain) NSArray *benefitVoice;
@property (nonatomic, retain) NSArray *benefitSms;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end

@interface BenefitDataModel : NSObject
@property (nonatomic, copy) NSString *activeUntil;
@property (nonatomic, copy) NSString *isUnlimited;
@property (nonatomic, copy) NSString *isActiveInMyXL;
@property (nonatomic, copy) NSString *paymentMethod;

//@property (nonatomic, copy) NSString *totalHari;
@property (nonatomic, copy) NSString *sisaHari;
@property (nonatomic, copy) NSString *percentSisaHari;
@property (nonatomic, copy) NSString *regPackage;
@property (nonatomic, copy) NSString *rataRataHarian;
@property (nonatomic, copy) NSString *estimasiHabis;
@property (nonatomic, copy) NSString *wordingInfoExpired;
@property (nonatomic, copy) NSArray *dataBar;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end

@interface DataBarModel : NSObject
@property (nonatomic, copy) NSString *packageName;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *percentTotal;
@property (nonatomic, copy) NSString *remaining;
@property (nonatomic, copy) NSString *usage;
@property (nonatomic, copy) NSString *percentRemaining;
@property (nonatomic, assign) float percentUsage;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end

@interface BenefitVoiceSmsModel : NSObject
@property (nonatomic, copy) NSString *sisa;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *benefitName;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
