//
//  QuotaMeterModel.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 4/22/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuotaMeterModel : NSObject {
    NSString *quotaPackageLabel;
    NSString *serviceId;
    
    int quotaType;
    NSString *quotaTypeLabel;
    
    NSString *quotaNormalUsageFrom;
    NSString *quotaNormalUsageTo;
    NSString *quotaNormalUsageDesc;
    NSString *quotaNormalUsagePercent;
    
    NSString *quotaCurrentUsageDesc;
    NSString *quotaCurrentUsagePercent;
    
    NSString *remainingDaysLabel;
    
    NSString *hintLabel;
    NSString *hintDescLabel;
    
    NSString *buttonLabel;
    NSString *hrefButton;
    
    NSString *descLabel;
    
    NSString *banner;
    NSString *hrefBanner;
    
    NSArray *icon;
    
    NSString *detailButton;
    
    NSString *variantId;
    NSString *variantName;
    
    int modelIndex;
}

@property (nonatomic, retain) NSString *quotaPackageLabel;
@property (nonatomic, retain) NSString *serviceId;

@property (readwrite) int quotaType;
@property (nonatomic, retain) NSString *quotaTypeLabel;

@property (nonatomic, retain) NSString *quotaNormalUsageFrom;
@property (nonatomic, retain) NSString *quotaNormalUsageTo;
@property (nonatomic, retain) NSString *quotaNormalUsageDesc;
@property (nonatomic, retain) NSString *quotaNormalUsagePercent;

@property (nonatomic, retain) NSString *quotaCurrentUsageDesc;
@property (nonatomic, retain) NSString *quotaCurrentUsagePercent;

@property (nonatomic, retain) NSString *remainingDaysLabel;

@property (nonatomic, retain) NSString *hintLabel;
@property (nonatomic, retain) NSString *hintDescLabel;

@property (nonatomic, retain) NSString *buttonLabel;
@property (nonatomic, retain) NSString *hrefButton;

@property (nonatomic, retain) NSString *descLabel;

@property (nonatomic, retain) NSString *banner;
@property (nonatomic, retain) NSString *hrefBanner;

@property (nonatomic, retain) NSArray *icon;

@property (nonatomic, retain) NSString *detailButton;

@property (nonatomic, retain) NSString *variantId;
@property (nonatomic, retain) NSString *variantName;

@property (readwrite) int modelIndex;

@end
