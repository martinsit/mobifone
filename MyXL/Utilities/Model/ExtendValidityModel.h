//
//  ExtendValidityModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/10/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExtendValidityModel : NSObject {
    NSString *statusText;
}

@property (nonatomic, retain) NSString *statusText;

@end
