//
//  ProfileModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/10/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ProfileModel.h"

@implementation ProfileModel

@synthesize profileId;
@synthesize name;
@synthesize fullName;
@synthesize msisdn;
@synthesize email;
@synthesize language;
@synthesize location;
@synthesize regional;
@synthesize status;
@synthesize address;
@synthesize address2;
@synthesize address3;
@synthesize city;
@synthesize postcode;
@synthesize confirmEmail;
@synthesize emailNotify;
@synthesize pushNotify;
@synthesize gender;
@synthesize birth;
@synthesize token;
@synthesize fbLink;
@synthesize autologin;
@synthesize device;
@synthesize imei;
@synthesize tac;
@synthesize subscriberType;
@synthesize balanceValue;
@synthesize activeEndDate;
@synthesize graceEndDate;

@synthesize msisdnType;
@synthesize idCardType;
@synthesize idCardNumber;
@synthesize placeBirth;
@synthesize otherPhone;

@synthesize logo;

@synthesize telcoOperator;

@synthesize themesModel;

@synthesize keepSignin;
@synthesize password;

- (void)dealloc {
    [profileId release];
    [name release];
    [fullName release];
    [msisdn release];
    [email release];
    [language release];
    [location release];
    [regional release];
    [status release];
    [address release];
    [address2 release];
    [address3 release];
    [city release];
    [postcode release];
    [confirmEmail release];
    [emailNotify release];
    [pushNotify release];
    [gender release];
    [birth release];
    [token release];
    [autologin release];
    [device release];
    [imei release];
    [tac release];
    [subscriberType release];
    [balanceValue release];
    [activeEndDate release];
    [graceEndDate release];
    
    [idCardType release];
    [idCardNumber release];
    [placeBirth release];
    [otherPhone release];
    
    [logo release];
    
    [telcoOperator release];
    
    [themesModel release];
    
    [keepSignin release];
    [password release];
    
    [super dealloc];
}

@end
