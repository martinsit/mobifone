//
//  PageSukaModel.h
//  MyXL
//
//  Created by tyegah on 3/13/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PageSukaModel : NSObject

@property (nonatomic, retain) NSString *pageTitle;
@property (nonatomic, retain) NSString *lblBeliSesukamu;
@property (nonatomic, retain) NSString *lblUbahNama;
@end
