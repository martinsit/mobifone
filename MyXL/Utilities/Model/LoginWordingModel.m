//
//  LoginWordingModel.m
//  MyXL
//
//  Created by tyegah on 3/4/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "LoginWordingModel.h"

@implementation LoginWordingModel

NSString * const kLoginWording_BtnLogin = @"btn_login";
NSString * const kLoginWording_Welcome = @"welcome";
NSString * const kLoginWording_Info = @"info";
NSString * const kLoginWording_YourMsisdn = @"msisdn";
NSString * const kLoginWording_Password = @"password";
NSString * const kLoginWording_RememberPassword = @"remember";
NSString * const kLoginWording_ForgotPassword = @"guide";
NSString * const kLoginWording_BtnRequest = @"btn_request";

-(void)dealloc
{
    [super dealloc];
    //[_wordingEN release];
    //[_wordingID release];
}

@end
