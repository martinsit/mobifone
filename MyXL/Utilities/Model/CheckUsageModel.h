//
//  CheckUsageModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/28/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckUsageModel : NSObject {
    NSString *packageId;
    NSString *packageCode;
    NSString *packageName;
    NSString *startDate;
    NSString *endDate;
    NSString *packageType;
    NSString *usage;
    NSString *quota;
    NSString *balance;
    NSString *usagePercent;
    NSString *info;
    NSString *unregCmd;
    NSString *unsubCmd;
    NSString *flagUpgrade;
    NSString *s0;
    NSString *t1;
    NSString *s1;
    NSString *t2;
    NSString *s2;
    NSString *t3;
    NSString *s3;
    NSString *t4;
    NSString *s4;
    NSString *t5;
    NSString *s5;
    int remainingPercentage;
    int remainingNumber;
    NSString *remainingType;
    NSString *remainingLabel;
}

@property (nonatomic, retain) NSString *packageId;
@property (nonatomic, retain) NSString *packageCode;
@property (nonatomic, retain) NSString *packageName;
@property (nonatomic, retain) NSString *startDate;
@property (nonatomic, retain) NSString *endDate;
@property (nonatomic, retain) NSString *packageType;
@property (nonatomic, retain) NSString *usage;
@property (nonatomic, retain) NSString *quota;
@property (nonatomic, retain) NSString *balance;
@property (nonatomic, retain) NSString *usagePercent;
@property (nonatomic, retain) NSString *info;
@property (nonatomic, retain) NSString *unregCmd;
@property (nonatomic, retain) NSString *unsubCmd;
@property (nonatomic, retain) NSString *flagUpgrade;
@property (nonatomic, retain) NSString *s0;
@property (nonatomic, retain) NSString *t1;
@property (nonatomic, retain) NSString *s1;
@property (nonatomic, retain) NSString *t2;
@property (nonatomic, retain) NSString *s2;
@property (nonatomic, retain) NSString *t3;
@property (nonatomic, retain) NSString *s3;
@property (nonatomic, retain) NSString *t4;
@property (nonatomic, retain) NSString *s4;
@property (nonatomic, retain) NSString *t5;
@property (nonatomic, retain) NSString *s5;
@property (readwrite) int remainingPercentage;
@property (readwrite) int remainingNumber;
@property (nonatomic, retain) NSString *remainingType;
@property (nonatomic, retain) NSString *remainingLabel;

@end
