//
//  LoginWordingLangModel.h
//  MyXL
//
//  Created by tyegah on 3/4/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginWordingLangModel : NSObject
@property (nonatomic, retain) NSString *btnLogin;
@property (nonatomic, retain) NSString *welcome;
@property (nonatomic, retain) NSString *info;
@property (nonatomic, retain) NSString *msisdn;
@property (nonatomic, retain) NSString *password;
@property (nonatomic, retain) NSString *remember;
@property (nonatomic, retain) NSString *guide;
@property (nonatomic, retain) NSString *btnRequest;
@end
