//
//  SurveyModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "SurveyModel.h"

@implementation SurveyModel

@synthesize surveyId;
@synthesize typeOption;
@synthesize question;
@synthesize minAnswer;
@synthesize maxAnswer;
@synthesize validationMsg;
@synthesize option;

- (void)dealloc {
    [surveyId release];
    [typeOption release];
    [question release];
    [minAnswer release];
    [maxAnswer release];
    [validationMsg release];
    [option release];
    [super dealloc];
}

@end
