//
//  PageSukaModel.m
//  MyXL
//
//  Created by tyegah on 3/13/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PageSukaModel.h"

@implementation PageSukaModel
- (void)dealloc {
    [_lblBeliSesukamu release];
    [_lblUbahNama release];
    
    [super dealloc];
}
@end
