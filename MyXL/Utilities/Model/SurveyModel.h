//
//  SurveyModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SurveyModel : NSObject {
    NSString *surveyId;
    NSString *typeOption;
    NSString *question;
    NSString *minAnswer;
    NSString *maxAnswer;
    NSString *validationMsg;
    NSArray *option;
}

@property (nonatomic, retain) NSString *surveyId;
@property (nonatomic, retain) NSString *typeOption;
@property (nonatomic, retain) NSString *question;
@property (nonatomic, retain) NSString *minAnswer;
@property (nonatomic, retain) NSString *maxAnswer;
@property (nonatomic, retain) NSString *validationMsg;
@property (nonatomic, retain) NSArray *option;

@end
