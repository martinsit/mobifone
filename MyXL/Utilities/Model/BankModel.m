//
//  BankModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BankModel.h"

@implementation BankModel

@synthesize bankId;
@synthesize value;
@synthesize label;
@synthesize methodId;
@synthesize urlRef;
@synthesize labelRefId;
@synthesize labelRefEn;
@synthesize methodName;

- (void)dealloc {
    [bankId release];
    [value release];
    [label release];
    [methodId release];
    [urlRef release];
    [labelRefId release];
    [labelRefEn release];
    [methodName release];
    [super dealloc];
}

@end
