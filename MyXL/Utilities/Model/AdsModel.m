//
//  AdsModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "AdsModel.h"

@implementation AdsModel

@synthesize type;
@synthesize imageURL;
@synthesize image;
@synthesize text;
@synthesize actionURL;

- (void)dealloc {
    [type release];
    [imageURL release];
    [image release];
    [text release];
    [actionURL release];
    [super dealloc];
}

@end
