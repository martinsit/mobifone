//
//  MenuModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/10/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuModel : NSObject {
    NSString *href;
    NSString *hrefweb;
    NSString *menuId;
    NSString *menuName;
    NSString *parentId;
    NSString *groupId; 
    NSString *groupName;      
    NSString *desc;
    NSString *duration;
    NSString *price;
    NSString *volume;
    NSString *packageId;
    NSString *packageType;
    NSString *packageGroup;
    NSString *oriPackage;
    NSString *tooltips;
    NSString *topDesc;
    NSString *icon;
    NSString *hex;
    NSString *confirmDesc;
    NSString *banner;
    UIImage *image;
    BOOL open;
}

@property (nonatomic, retain) NSString *href;
@property (nonatomic, retain) NSString *hrefweb;
@property (nonatomic, retain) NSString *menuId;
@property (nonatomic, retain) NSString *menuName;
@property (nonatomic, retain) NSString *parentId;
@property (nonatomic, retain) NSString *groupId;
@property (nonatomic, retain) NSString *groupName;
@property (nonatomic, retain) NSString *desc;
@property (nonatomic, retain) NSString *duration;
@property (nonatomic, retain) NSString *price;
@property (nonatomic, retain) NSString *volume;
@property (nonatomic, retain) NSString *packageId;
@property (nonatomic, retain) NSString *packageType;
@property (nonatomic, retain) NSString *packageGroup;
@property (nonatomic, retain) NSString *oriPackage;
@property (nonatomic, retain) NSString *tooltips;
@property (nonatomic, retain) NSString *topDesc;
@property (nonatomic, retain) NSString *icon;
@property (nonatomic, retain) NSString *hex;
@property (nonatomic, retain) NSString *confirmDesc;
@property (nonatomic, retain) NSString *banner;
@property (nonatomic, retain) UIImage *image;
@property (readwrite) BOOL open;

// TODO : V1.9
// Payment Method attribute
@property (nonatomic, copy) NSString *paymentMethod;
@property (nonatomic, copy) NSString *shortDesc;

// DROP 2
@property (nonatomic, assign) NSInteger position;
@end
