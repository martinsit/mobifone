//
//  PUKModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/4/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PUKModel.h"

@implementation PUKModel

@synthesize puk1;
@synthesize puk2;

- (void)dealloc {
    [puk1 release];
    [puk2 release];
    [super dealloc];
}

@end
