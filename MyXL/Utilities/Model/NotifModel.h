//
//  NotifModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/4/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotifModel : NSObject {
    NSString *idNotif;
    NSString *message;
    NSString *dateTime;
    NSString *readFlag;
    NSString *notifType;
    NSString *status;
    NSString *source;
    NSString *serviceId;
    NSString *from;
    NSString *detailMessage;
    NSString *month;
    
    NSString *replyStatus;
    NSString *replyValue;
    NSArray *param;
}

@property (nonatomic, retain) NSString *idNotif;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *dateTime;
@property (nonatomic, retain) NSString *readFlag;
@property (nonatomic, retain) NSString *notifType;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString *source;
@property (nonatomic, retain) NSString *serviceId;
@property (nonatomic, retain) NSString *from;
@property (nonatomic, retain) NSString *detailMessage;
@property (nonatomic, retain) NSString *month;

@property (nonatomic, retain) NSString *replyStatus;
@property (nonatomic, retain) NSString *replyValue;
@property (nonatomic, retain) NSArray *param;

@end
