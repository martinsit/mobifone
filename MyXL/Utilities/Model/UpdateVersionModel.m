//
//  UpdateVersionModel.m
//  MyXL
//
//  Created by Tity Septiani on 5/26/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "UpdateVersionModel.h"

@implementation UpdateVersionModel
NSString * const kUpdateVersion_version = @"version";
NSString * const kUpdateVersion_code = @"code";
NSString * const kUpdateVersion_message = @"message";
NSString * const kUpdateVersion_url = @"url";
NSString * const kUpdateVersion_posBtnTitle = @"btnpos";
NSString * const kUpdateVersion_cancelBtnTitle = @"btnneg";
@end
