//
//  ShopLocationModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/13/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShopLocationModel : NSObject {
    NSNumber *latitude;
    NSNumber *longitude;
    NSString *title;
    NSString *address;
    NSArray *time;
}

@property (nonatomic, retain) NSNumber *latitude;
@property (nonatomic, retain) NSNumber *longitude;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSArray *time;

@end
