//
//  DeviceSettingModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "DeviceSettingModel.h"

@implementation DeviceSettingModel

@synthesize desc;
@synthesize data;

- (void)dealloc {
    [desc release];
    [data release];
    [super dealloc];
}

@end
