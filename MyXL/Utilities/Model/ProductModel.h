//
//  ProductModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 7/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductModel : NSObject {
    NSString *productName;
    NSString *categoryName;
    NSString *categoryId;
    NSString *productTypeId;
    NSString *productType;
    NSString *productId;
    NSString *productCode;
    NSString *productPrice;
    NSString *priceOri;
    NSString *productDiscount;
    NSString *incTax;
    NSString *valueTax;
    NSString *productImage;
    NSString *captionImage;
    NSString *thumbnailImage;
    NSString *productDesc;
    NSString *startPromo;
    NSString *endPromo;
    NSString *quantity;
    NSString *descStock;
    NSString *currentPage;
    NSString *totalPage;
}

@property (nonatomic, retain) NSString *productName;
@property (nonatomic, retain) NSString *categoryName;
@property (nonatomic, retain) NSString *categoryId;
@property (nonatomic, retain) NSString *productTypeId;
@property (nonatomic, retain) NSString *productType;
@property (nonatomic, retain) NSString *productId;
@property (nonatomic, retain) NSString *productCode;
@property (nonatomic, retain) NSString *productPrice;
@property (nonatomic, retain) NSString *priceOri;
@property (nonatomic, retain) NSString *productDiscount;
@property (nonatomic, retain) NSString *incTax;
@property (nonatomic, retain) NSString *valueTax;
@property (nonatomic, retain) NSString *productImage;
@property (nonatomic, retain) NSString *captionImage;
@property (nonatomic, retain) NSString *thumbnailImage;
@property (nonatomic, retain) NSString *productDesc;
@property (nonatomic, retain) NSString *startPromo;
@property (nonatomic, retain) NSString *endPromo;
@property (nonatomic, retain) NSString *quantity;
@property (nonatomic, retain) NSString *descStock;
@property (nonatomic, retain) NSString *currentPage;
@property (nonatomic, retain) NSString *totalPage;

@end
