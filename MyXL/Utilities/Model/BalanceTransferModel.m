//
//  BalanceTransferModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BalanceTransferModel.h"

@implementation BalanceTransferModel

@synthesize msisdnTo;
@synthesize amount;
@synthesize charge;
@synthesize balanceNew;
@synthesize targetOldActiveStop;
@synthesize targetNewActiveStop;

- (void)dealloc {
    [msisdnTo release];
    [amount release];
    [charge release];
    [balanceNew release];
    [targetOldActiveStop release];
    [targetNewActiveStop release];
    [super dealloc];
}

@end
