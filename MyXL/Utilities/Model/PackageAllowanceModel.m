//
//  PackageAllowanceModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PackageAllowanceModel.h"

@implementation PackageAllowanceModel

@synthesize type;
@synthesize name;
@synthesize quota;
@synthesize remaining;
@synthesize remainingPercent;

- (void)dealloc {
    [type release];
    [name release];
    [quota release];
    [remaining release];
    [remainingPercent release];
    [super dealloc];
}

@end
