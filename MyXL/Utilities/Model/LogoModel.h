//
//  LogoModel.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 9/12/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LogoModel : NSObject {
    NSString *logoURL;
    NSString *version;
    NSString *telcoOperator;
    BOOL isRetina;
}

@property (nonatomic, retain) NSString *logoURL;
@property (nonatomic, retain) NSString *version;
@property (nonatomic, retain) NSString *telcoOperator;
@property (readwrite) BOOL isRetina;

@end
