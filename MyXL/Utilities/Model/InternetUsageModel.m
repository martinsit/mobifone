//
//  InternetUsageModel.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 6/19/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "InternetUsageModel.h"

@implementation InternetUsageModel

@synthesize code;
@synthesize title;
@synthesize color;
@synthesize iconUrl;
@synthesize maxScaleValue;
@synthesize labelMax;
@synthesize unitScaleValue;
@synthesize unit;
@synthesize conversion;
@synthesize maxVolumeValue;
@synthesize maxScalePerdayValue;
@synthesize descriptionDaily;
@synthesize descriptionWeekly;
@synthesize descriptionMonthly;

@synthesize sliderValue;
@synthesize sliderTag;
@synthesize maxBar;

- (void)dealloc {
    [code release];
    [title release];
    [color release];
    [iconUrl release];
    
    [labelMax release];
    
    [unit release];
    [conversion release];
    
    [descriptionDaily release];
    [descriptionWeekly release];
    [descriptionMonthly release];
    [super dealloc];
}

@end
