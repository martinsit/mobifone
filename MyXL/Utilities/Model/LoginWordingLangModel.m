//
//  LoginWordingLangModel.m
//  MyXL
//
//  Created by tyegah on 3/4/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "LoginWordingLangModel.h"

@implementation LoginWordingLangModel
-(void)dealloc
{
    [super dealloc];
    
    [_btnLogin release];
    [_welcome release];
    [_btnRequest release];
    [_guide release];
    [_info release];
    [_remember release];
    [_msisdn release];
}
@end
