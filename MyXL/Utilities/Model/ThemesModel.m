//
//  ThemesModel.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 12/15/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ThemesModel.h"

@implementation ThemesModel

@synthesize header;
@synthesize headerGradientStart;
@synthesize headerGradientEnd;
//@synthesize featureButton;
//@synthesize button;

@synthesize menuColor;
@synthesize menuH3;

@synthesize menuFeature;
@synthesize menuFeatureTxt;

@synthesize accordionCurrentBg;
@synthesize accordionBg;

@synthesize packageButtonBg;
@synthesize packageButtonTxt;

- (void)dealloc {
    [header release];
    [headerGradientStart release];
    [headerGradientEnd release];
    //[featureButton release];
    //[button release];
    
    [menuColor release];
    [menuH3 release];
    
    [menuFeature release];
    [menuFeatureTxt release];
    
    [accordionCurrentBg release];
    [accordionBg release];
    
    [packageButtonBg release];
    [packageButtonTxt release];
    
    [super dealloc];
}

@end
