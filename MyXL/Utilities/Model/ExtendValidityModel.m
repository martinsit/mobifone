//
//  ExtendValidityModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/10/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ExtendValidityModel.h"

@implementation ExtendValidityModel

@synthesize statusText;

- (void)dealloc {
    [statusText release];
    [super dealloc];
}

@end
