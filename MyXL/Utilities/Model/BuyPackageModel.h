//
//  BuyPackageModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/13/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BuyPackageModel : NSObject {
    NSString *smsId;
}

@property (nonatomic, retain) NSString *smsId;

@end
