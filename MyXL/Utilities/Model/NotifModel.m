//
//  NotifModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/4/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "NotifModel.h"

@implementation NotifModel

@synthesize idNotif;
@synthesize message;
@synthesize dateTime;
@synthesize readFlag;
@synthesize notifType;
@synthesize status;
@synthesize source;
@synthesize serviceId;
@synthesize from;
@synthesize detailMessage;
@synthesize month;

@synthesize replyStatus;
@synthesize replyValue;
@synthesize param;

- (void)dealloc {
    [idNotif release];
    [message release];
    [dateTime release];
    [readFlag release];
    [notifType release];
    [status release];
    [source release];
    [serviceId release];
    [from release];
    [detailMessage release];
    [month release];
    
    [replyStatus release];
    [replyValue release];
    [param release];
    [super dealloc];
}

@end
