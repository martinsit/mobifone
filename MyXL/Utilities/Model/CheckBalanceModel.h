//
//  CheckBalanceModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckBalanceModel : NSObject {
    NSString *msisdn;
    NSString *accountState;
    NSString *accountStatus;
    NSString *accountType;
    NSString *activeStopDate;
    NSString *balance;
    NSString *disableStopDate;
    NSString *suspendStopDate;
    NSArray *bonusInfo;
    NSString *code;
}

@property (nonatomic, retain) NSString *msisdn;
@property (nonatomic, retain) NSString *accountState;
@property (nonatomic, retain) NSString *accountStatus;
@property (nonatomic, retain) NSString *accountType;
@property (nonatomic, retain) NSString *activeStopDate;
@property (nonatomic, retain) NSString *balance;
@property (nonatomic, retain) NSString *disableStopDate;
@property (nonatomic, retain) NSString *suspendStopDate;
@property (nonatomic, retain) NSArray *bonusInfo;
@property (nonatomic, retain) NSString *code;

@end
