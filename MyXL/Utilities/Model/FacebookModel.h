//
//  FacebookModel.h
//  SocialShare
//
//  Created by LinkIT360 on 11/18/14.
//  Copyright (c) 2014 Linkit360. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FacebookModel : NSObject
{
    NSString *fbName;
    NSString *fbEmail;
    NSString *fbGender;
}

@property (nonatomic, copy) NSString *fbName;
@property (nonatomic, copy) NSString *fbEmail;
@property (nonatomic, copy) NSString *fbGender;

@end
