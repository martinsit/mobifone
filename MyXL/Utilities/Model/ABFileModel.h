//
//  ABFileModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABFileModel : NSObject {
    NSString *filename;
    BOOL thumbnailExists;
    long long totalBytes;
    NSDate *lastModifiedDate;
    NSString *dateString;
    NSString *humanReadableSize;
    NSString *icon;
    NSString *path;
    UIImage *photo;
    NSString *rev;
    BOOL selected;
    NSString *name;
    int peopleIndex;
    
    int exportIndex;
    BOOL isExecuting;
    BOOL isFinished;
    BOOL isSuccess;
}

@property (nonatomic, retain) NSString *filename;
@property (readwrite) BOOL thumbnailExists;
@property (readwrite) long long totalBytes;
@property (nonatomic, retain) NSDate *lastModifiedDate;
@property (nonatomic, retain) NSString *dateString;
@property (nonatomic, retain) NSString *humanReadableSize;
@property (nonatomic, retain) NSString *icon;
@property (nonatomic, retain) NSString *path;
@property (nonatomic, retain) UIImage *photo;
@property (nonatomic, retain) NSString *rev;
@property (readwrite) BOOL selected;
@property (nonatomic, retain) NSString *name;
@property (readwrite) int peopleIndex;

@property (readwrite) int exportIndex;
@property (readwrite) BOOL isExecuting;
@property (readwrite) BOOL isFinished;
@property (readwrite) BOOL isSuccess;

@end
