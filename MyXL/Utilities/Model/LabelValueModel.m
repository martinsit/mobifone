//
//  LabelValueModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "LabelValueModel.h"

@implementation LabelValueModel

@synthesize itemId;
@synthesize label;
@synthesize value;

- (void)dealloc {
    [itemId release];
    [label release];
    [value release];
    [super dealloc];
}

@end
