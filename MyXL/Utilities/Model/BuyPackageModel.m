//
//  BuyPackageModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/13/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BuyPackageModel.h"

@implementation BuyPackageModel

@synthesize smsId;

- (void)dealloc {
    [smsId release];
    [super dealloc];
}

@end
