//
//  WinnerListModel.h
//  MyXL
//
//  Created by tyegah on 8/7/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WinnerListModel : NSObject
//{"CorrelationId":"27083147","EventDate":"2015-08-02","RaffleDate":"2015-08-05","Msisdn":"6281807515146","Name":{},"UrlImage":{},"GroupId":"JABODETABEK","Channel":"TRAD","Point":"50","IsWinner":"0","RaffleStatus":"INPROGRESS","RaffledBy":{},"Sequence":"63"}
extern NSString * const kWinnerList_EventDate;
extern NSString * const kWinnerList_RaffleDate;
extern NSString * const kWinnerList_Msisdn;
extern NSString * const kWinnerList_Name;
extern NSString * const kWinnerList_UrlImage;

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *msisdn;
@property (nonatomic, copy) NSString *eventdate;
@property (nonatomic, copy) NSString *urlImage;


@end
