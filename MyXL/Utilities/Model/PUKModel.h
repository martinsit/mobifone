//
//  PUKModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/4/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PUKModel : NSObject {
    NSString *puk1;
    NSString *puk2;
}

@property (nonatomic, retain) NSString *puk1;
@property (nonatomic, retain) NSString *puk2;

@end
