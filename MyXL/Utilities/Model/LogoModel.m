//
//  LogoModel.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 9/12/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "LogoModel.h"

@implementation LogoModel

@synthesize logoURL;
@synthesize version;
@synthesize telcoOperator;
@synthesize isRetina;

- (void)dealloc {
    [logoURL release];
    [version release];
    [telcoOperator release];
    [super dealloc];
}

@end
