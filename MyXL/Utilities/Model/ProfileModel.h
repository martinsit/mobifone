//
//  ProfileModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/10/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThemesModel.h"

@interface ProfileModel : NSObject {
    NSString *profileId;
    NSString *name;
    NSString *fullName;
    NSString *msisdn;
    NSString *email;
    NSString *language;
    NSString *location;
    NSString *regional;
    NSString *status;
    NSString *address;
    NSString *address2;
    NSString *address3;
    NSString *city;
    NSString *postcode;
    NSString *confirmEmail;
    NSString *emailNotify;
    NSString *pushNotify;
    NSString *gender;
    NSString *birth;
    NSString *token;
    int fbLink;
    NSString *autologin;
    NSString *device;
    NSString *imei;
    NSString *tac;
    NSString *subscriberType;
    NSString *balanceValue;
    NSString *activeEndDate;
    NSString *graceEndDate;
    
    /*
     * MSISDN Type :
     * 1 = Prepaid
     * 2 = Postpaid
     */
    int msisdnType;
    NSString *idCardType;
    NSString *idCardNumber;
    NSString *placeBirth;
    NSString *otherPhone;
    
    NSArray *logo;
    
    NSString *telcoOperator;
    
    ThemesModel *themesModel;
    
    // TODO : V1.9.5
    NSString *keepSignin;
    NSString *password;
}

@property (nonatomic, retain) NSString *profileId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *fullName;
@property (nonatomic, retain) NSString *msisdn;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *language;
@property (nonatomic, retain) NSString *location;
@property (nonatomic, retain) NSString *regional;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *address2;
@property (nonatomic, retain) NSString *address3;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *postcode;
@property (nonatomic, retain) NSString *confirmEmail;
@property (nonatomic, retain) NSString *emailNotify;
@property (nonatomic, retain) NSString *pushNotify;
@property (nonatomic, retain) NSString *gender;
@property (nonatomic, retain) NSString *birth;
@property (nonatomic, retain) NSString *token;
@property (readwrite) int fbLink;
@property (nonatomic, retain) NSString *autologin;
@property (nonatomic, retain) NSString *device;
@property (nonatomic, retain) NSString *imei;
@property (nonatomic, retain) NSString *tac;
@property (nonatomic, retain) NSString *subscriberType;
@property (nonatomic, retain) NSString *balanceValue;
@property (nonatomic, retain) NSString *activeEndDate;
@property (nonatomic, retain) NSString *graceEndDate;

@property (readwrite) int msisdnType;
@property (nonatomic, retain) NSString *idCardType;
@property (nonatomic, retain) NSString *idCardNumber;
@property (nonatomic, retain) NSString *placeBirth;
@property (nonatomic, retain) NSString *otherPhone;

@property (nonatomic, retain) NSArray *logo;

@property (nonatomic, retain) NSString *telcoOperator;

@property (nonatomic, retain) ThemesModel *themesModel;

@property (nonatomic, assign) BOOL flagXvaganza;
@property (nonatomic, assign) BOOL flag4Gpowermeter;

@property (nonatomic, retain) NSString *keepSignin;
@property (nonatomic, retain) NSString *password;

@end
