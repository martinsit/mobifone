//
//  ContactUsModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactUsModel : NSObject {
    NSString *href;
    NSString *label;
    NSString *value;
    NSString *actionValue;
    BOOL isData;
}

@property (nonatomic, retain) NSString *href;
@property (nonatomic, retain) NSString *label;
@property (nonatomic, retain) NSString *value;
@property (nonatomic, retain) NSString *actionValue;
@property (readwrite) BOOL isData;

@end
