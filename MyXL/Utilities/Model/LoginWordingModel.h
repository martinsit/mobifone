//
//  LoginWordingModel.h
//  MyXL
//
//  Created by tyegah on 3/4/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginWordingLangModel.h"

@interface LoginWordingModel : NSObject

extern NSString * const kLoginWording_BtnLogin;
extern NSString * const kLoginWording_Welcome;
extern NSString * const kLoginWording_Info;
extern NSString * const kLoginWording_YourMsisdn;
extern NSString * const kLoginWording_Password;
extern NSString * const kLoginWording_RememberPassword;
extern NSString * const kLoginWording_ForgotPassword;
extern NSString * const kLoginWording_BtnRequest;

@property (nonatomic, retain) LoginWordingLangModel *wordingEN;
@property (nonatomic, retain) LoginWordingLangModel *wordingID;
@end
