//
//  QuotaImprovementModel.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 10/19/15.
//  Copyright © 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "QuotaImprovementModel.h"

@implementation QuotaImprovementModel

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    if (self == [super init]) {
        NSDictionary *summaryDict = [dict objectForKey:@"summary"];
        self.activePackage  = [summaryDict objectForKey:@"activePackage"];
        self.isUnlimited    = [summaryDict objectForKey:@"isUnlimited"];
        self.usage          = [summaryDict objectForKey:@"usage"];
        self.percentUsage   = [[summaryDict objectForKey:@"percentUsage"] floatValue];
        self.remaining      = [summaryDict objectForKey:@"remaining"];
        self.percentRemaining = [[summaryDict objectForKey:@"percentRemaining"] floatValue];
        self.total          = [summaryDict objectForKey:@"total"];
        self.activeUntil    = [summaryDict objectForKey:@"activeUntil"];
        self.totalHari      = [summaryDict objectForKey:@"totalHari"];
        self.sisaHari       = [summaryDict objectForKey:@"sisaHari"];
        self.percentSisaHari = [[summaryDict objectForKey:@"percentSisaHari"] floatValue];
        self.regPackage     = [summaryDict objectForKey:@"regPackage"];
        self.wordingInfoTotalQuota = [summaryDict objectForKey:@"wordingInfoTotalQuota"];
        
        NSArray *detailList = [dict objectForKey:@"detail"];
        NSMutableArray *arrayDetail = [[NSMutableArray alloc] initWithCapacity:[detailList count]];
        if ([detailList isKindOfClass:[NSArray class]] && [detailList count] > 0) {
            for (NSDictionary *detailDict in detailList) {
                QuotaDetailModel *item = [[QuotaDetailModel alloc] initWithDictionary:detailDict];
                [arrayDetail addObject:item];
                [item release];
                item = nil;
            }
            self.detail = [arrayDetail copy];
        }
        else {
            self.detail = nil;
        }
        
    }
    return self;
}

@end

// Detail
@implementation QuotaDetailModel
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self == [super init]) {
        
        self.name           = [dict objectForKey:@"name"];
        self.serviceId      = [dict objectForKey:@"serviceId"];
        self.price          = [dict objectForKey:@"price"];
        self.confirmDesc    = [dict objectForKey:@"confirmDesc"];
        self.showRataHarian = [dict objectForKey:@"showRata-rataHarian"];
        self.showEstimasiHabis = [dict objectForKey:@"showEstimasiHabis"];
        
        NSDictionary *dataDict = [dict objectForKey:@"benefitData"];
        if (!!dataDict && ![dataDict isEqual:[NSNull null]]) {
            self.benefitData = [[BenefitDataModel alloc] initWithDictionary:dataDict];
        }
        else {
            self.benefitData = NULL;
        }
        /*
        NSDictionary *voiceDict = [dict objectForKey:@"benefitVoice"];
        if (!!voiceDict && ![voiceDict isEqual:[NSNull null]]) {
            self.benefitVoice = [[BenefitVoiceSmsModel alloc] initWithDictionary:voiceDict];
        }
        else {
            self.benefitVoice = NULL;
        }
        
        NSDictionary *smsDict = [dict objectForKey:@"benefitSms"];
        if (!!smsDict && ![smsDict isEqual:[NSNull null]]) {
            self.benefitSms = [[BenefitVoiceSmsModel alloc] initWithDictionary:smsDict];
        }
        else {
            self.benefitSms = NULL;
        }*/
        
        NSArray *voiceList = [dict objectForKey:@"benefitVoice"];
        NSMutableArray *voiceDetail = [[NSMutableArray alloc] initWithCapacity:[voiceList count]];
        if ([voiceList isKindOfClass:[NSArray class]] && [voiceList count] > 0) {
            for (NSDictionary *detailDict in voiceList) {
                BenefitVoiceSmsModel *item = [[BenefitVoiceSmsModel alloc] initWithDictionary:detailDict];
                [voiceDetail addObject:item];
                [item release];
                item = nil;
            }
            self.benefitVoice = [voiceDetail copy];
        }
        else {
            self.benefitVoice = nil;
        }
        
        NSArray *smsList = [dict objectForKey:@"benefitSms"];
        NSMutableArray *smsDetail = [[NSMutableArray alloc] initWithCapacity:[smsList count]];
        if ([smsList isKindOfClass:[NSArray class]] && [smsList count] > 0) {
            for (NSDictionary *detailDict in smsList) {
                BenefitVoiceSmsModel *item = [[BenefitVoiceSmsModel alloc] initWithDictionary:detailDict];
                [smsDetail addObject:item];
                [item release];
                item = nil;
            }
            self.benefitSms = [smsDetail copy];
        }
        else {
            self.benefitSms = nil;
        }
    }
    return self;
}

@end

// Data
@implementation BenefitDataModel
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self == [super init]) {
        self.activeUntil    = [dict objectForKey:@"activeUntil"];
        self.isUnlimited    = [dict objectForKey:@"isUnlimited"];
        self.isActiveInMyXL = [dict objectForKey:@"isActiveInmyxl"];
        self.paymentMethod  = [dict objectForKey:@"paymentMethod"];
        
        //self.totalHari      = [dict objectForKey:@"totalHari"];
        id sisaHariTmp      = [dict objectForKey:@"sisaHari"];
        if ([sisaHariTmp isKindOfClass:[NSString class]]) {
            self.sisaHari = (NSString*)sisaHariTmp;
        }
        else {
            int sisaHariInt = [sisaHariTmp intValue];
            self.sisaHari = [NSString stringWithFormat:@"%i",sisaHariInt];
        }
        //self.sisaHari       = [dict objectForKey:@"sisaHari"];
        self.percentSisaHari = [dict objectForKey:@"percentSisaHari"];
        self.regPackage     = [dict objectForKey:@"regPackage"];
        self.rataRataHarian = [dict objectForKey:@"rata-rataHarian"];
        self.estimasiHabis  = [dict objectForKey:@"estimasiHabis"];
        self.wordingInfoExpired = [dict objectForKey:@"wordingInfoExpired"];
        
        NSArray *dataBar    = [dict objectForKey:@"dataBar"];
        NSMutableArray *arrayDataBar = [[NSMutableArray alloc] initWithCapacity:[dataBar count]];
        if ([dataBar isKindOfClass:[NSArray class]] && [dataBar count] > 0) {
            for (NSDictionary *dataBarDict in dataBar) {
                DataBarModel *item = [[DataBarModel alloc] initWithDictionary:dataBarDict];
                [arrayDataBar addObject:item];
                [item release];
                item = nil;
            }
            self.dataBar = [arrayDataBar copy];
        }
        else {
            self.dataBar = nil;
        }
        
    }
    return self;
}

@end

// Data Bar
@implementation DataBarModel
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self == [super init]) {
        self.packageName    = [dict objectForKey:@"packageName"];
        self.total          = [dict objectForKey:@"total"];
        self.percentTotal   = [dict objectForKey:@"percentTotal"];
        self.remaining      = [dict objectForKey:@"remaining"];
        self.usage          = [dict objectForKey:@"usage"];
        self.percentRemaining = [dict objectForKey:@"percentRemaining"];
        id usage = [dict objectForKey:@"percentUsage"];
        self.percentUsage = [usage floatValue];
    }
    return self;
}

@end

// Voice and SMS
@implementation BenefitVoiceSmsModel

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self == [super init]) {
        id sisaTmp = [dict objectForKey:@"sisa"];
        if ([sisaTmp isKindOfClass:[NSString class]]) {
            self.sisa = (NSString*)sisaTmp;
        }
        else {
            int sisaInt = [sisaTmp intValue];
            self.sisa = [NSString stringWithFormat:@"%i",sisaInt];
        }
        
        id totalTmp = [dict objectForKey:@"total"];
        if ([totalTmp isKindOfClass:[NSString class]]) {
            self.total = (NSString*)totalTmp;
        }
        else {
            int totalInt = [totalTmp intValue];
            self.total = [NSString stringWithFormat:@"%i",totalInt];
        }
        
        id nameTmp = [dict objectForKey:@"name"];
        if ([nameTmp isKindOfClass:[NSString class]]) {
            self.benefitName = (NSString*)nameTmp;
        }
        else {
            //int totalInt = [totalTmp intValue];
            //self.total = [NSString stringWithFormat:@"%i",totalInt];
            self.benefitName = @"";
        }
    }
    return self;
}

@end
