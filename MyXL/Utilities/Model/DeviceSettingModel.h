//
//  DeviceSettingModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceSettingModel : NSObject {
    NSString *desc;
    NSArray *data;
}

@property (nonatomic, retain) NSString *desc;
@property (nonatomic, retain) NSArray *data;

@end
