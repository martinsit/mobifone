//
//  AdsModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdsModel : NSObject {
    NSString *type;
    NSString *imageURL;
    UIImage *image;
    NSString *text;
    NSString *actionURL;
}

@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *imageURL;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) NSString *actionURL;

@end
