//
//  BonusInfoModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BonusInfoModel : NSObject {
    NSString *accId;
    NSString *accVal;
    NSString *accValType;
    NSString *accExpTime;
    NSString *accBalDesc;
    NSString *desc;
    NSString *bonusVal;
}

@property (nonatomic, retain) NSString *accId;
@property (nonatomic, retain) NSString *accVal;
@property (nonatomic, retain) NSString *accValType;
@property (nonatomic, retain) NSString *accExpTime;
@property (nonatomic, retain) NSString *accBalDesc;
@property (nonatomic, retain) NSString *desc;
@property (nonatomic, retain) NSString *bonusVal;

@end
