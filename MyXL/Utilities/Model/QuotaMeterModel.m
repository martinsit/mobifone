//
//  QuotaMeterModel.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 4/22/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "QuotaMeterModel.h"

@implementation QuotaMeterModel

@synthesize quotaPackageLabel;
@synthesize serviceId;

@synthesize quotaType;
@synthesize quotaTypeLabel;

@synthesize quotaNormalUsageFrom;
@synthesize quotaNormalUsageTo;
@synthesize quotaNormalUsageDesc;
@synthesize quotaNormalUsagePercent;

@synthesize quotaCurrentUsageDesc;
@synthesize quotaCurrentUsagePercent;

@synthesize remainingDaysLabel;

@synthesize hintLabel;
@synthesize hintDescLabel;

@synthesize buttonLabel;
@synthesize hrefButton;

@synthesize descLabel;

@synthesize banner;
@synthesize hrefBanner;

@synthesize icon;

@synthesize detailButton;

@synthesize variantId;
@synthesize variantName;

@synthesize modelIndex;

- (void)dealloc {
    [quotaPackageLabel release];
    [quotaNormalUsageFrom release];
    [quotaNormalUsageTo release];
    [quotaNormalUsageDesc release];
    [quotaNormalUsagePercent release];
    [quotaCurrentUsageDesc release];
    [quotaCurrentUsagePercent release];
    [remainingDaysLabel release];
    [hintLabel release];
    [buttonLabel release];
    [descLabel release];
    [banner release];
    [hrefBanner release];
    [icon release];
    [detailButton release];
    [variantId release];
    [variantName release];
    [super dealloc];
}

@end
