//
//  RoamingModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoamingModel : NSObject {
    NSString *country;
    NSString *partner;
    NSString *data;
}

@property (nonatomic, retain) NSString *country;
@property (nonatomic, retain) NSString *partner;
@property (nonatomic, retain) NSString *data;

@end
