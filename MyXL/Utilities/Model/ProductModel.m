//
//  ProductModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 7/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ProductModel.h"

@implementation ProductModel

@synthesize productName;
@synthesize categoryName;
@synthesize categoryId;
@synthesize productTypeId;
@synthesize productType;
@synthesize productId;
@synthesize productCode;
@synthesize productPrice;
@synthesize priceOri;
@synthesize productDiscount;
@synthesize incTax;
@synthesize valueTax;
@synthesize productImage;
@synthesize captionImage;
@synthesize thumbnailImage;
@synthesize productDesc;
@synthesize startPromo;
@synthesize endPromo;
@synthesize quantity;
@synthesize descStock;
@synthesize currentPage;
@synthesize totalPage;

- (void)dealloc {
    [productName release];
    [categoryName release];
    [categoryId release];
    [productTypeId release];
    [productType release];
    [productId release];
    [productCode release];
    [productPrice release];
    [priceOri release];
    [productDiscount release];
    [incTax release];
    [valueTax release];
    [productImage release];
    [captionImage release];
    [thumbnailImage release];
    [productDesc release];
    [startPromo release];
    [endPromo release];
    [quantity release];
    [descStock release];
    [currentPage release];
    [totalPage release];
    [super dealloc];
}

@end
