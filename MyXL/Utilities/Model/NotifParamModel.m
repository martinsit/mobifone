//
//  NotifParamModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/4/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "NotifParamModel.h"

@implementation NotifParamModel

@synthesize idx;
@synthesize label;
@synthesize type;
@synthesize content;
@synthesize value;

- (void)dealloc {
    [idx release];
    [label release];
    [type release];
    [content release];
    [value release];
    [super dealloc];
}

@end
