//
//  PaymentDebitModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/20/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PaymentDebitModel.h"

@implementation PaymentDebitModel

@synthesize msisdnTo;
@synthesize amount;
@synthesize transType;
@synthesize transTypeLabel;
@synthesize bankName;
@synthesize balance;
@synthesize activeDate;

- (void)dealloc {
    [msisdnTo release];
    [amount release];
    [transType release];
    [transTypeLabel release];
    [bankName release];
    [balance release];
    [activeDate release];
    [super dealloc];
}

@end
