//
//  RenamePackageModel.h
//  MyXL
//
//  Created by tyegah on 3/16/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RenamePackageModel : NSObject
@property (nonatomic, retain) NSString *packageName;
@property (nonatomic, retain) NSString *serviceID;
@end
