//
//  BonusInfoModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BonusInfoModel.h"

@implementation BonusInfoModel

@synthesize accId;
@synthesize accVal;
@synthesize accValType;
@synthesize accExpTime;
@synthesize accBalDesc;
@synthesize desc;
@synthesize bonusVal;

- (void)dealloc {
    [accId release];
    [accVal release];
    [accValType release];
    [accExpTime release];
    [accBalDesc release];
    [desc release];
    [bonusVal release];
    [super dealloc];
}

@end
