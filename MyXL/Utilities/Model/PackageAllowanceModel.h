//
//  PackageAllowanceModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PackageAllowanceModel : NSObject {
    NSString *type;
    NSString *name;
    NSString *quota;
    NSString *remaining;
    NSString *remainingPercent;
    NSString *usage;
}

@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *quota;
@property (nonatomic, retain) NSString *remaining;
@property (nonatomic, retain) NSString *remainingPercent;
// TODO : For Quota Bar Unlimited
@property (nonatomic, copy) NSString *usage;


@end
