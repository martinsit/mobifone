//
//  CheckBalanceModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CheckBalanceModel.h"

@implementation CheckBalanceModel

@synthesize msisdn;
@synthesize accountState;
@synthesize accountStatus;
@synthesize accountType;
@synthesize activeStopDate;
@synthesize balance;
@synthesize disableStopDate;
@synthesize suspendStopDate;
@synthesize bonusInfo;
@synthesize code;

- (void)dealloc {
    [msisdn release];
    [accountState release];
    [accountStatus release];
    [accountType release];
    [activeStopDate release];
    [balance release];
    [disableStopDate release];
    [suspendStopDate release];
    [bonusInfo release];
    [code release];
    [super dealloc];
}

@end
