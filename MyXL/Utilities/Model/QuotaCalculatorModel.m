//
//  QuotaCalculatorModel.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 6/18/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "QuotaCalculatorModel.h"

@implementation QuotaCalculatorModel

@synthesize listUsage;
@synthesize title;
@synthesize subtitle;
@synthesize dailyUsage;
@synthesize weeklyUsage;
@synthesize monthlyUsage;
@synthesize choosePackage;
@synthesize recommended;
@synthesize daily;
@synthesize weekly;
@synthesize monthly;
@synthesize totalVolumeDaily;
@synthesize totalVolumeWeekly;
@synthesize totalVolumeMonthly;
@synthesize totalVolumeDailyString;
@synthesize totalVolumeWeeklyString;
@synthesize totalVolumeMonthlyString;
@synthesize calculateText;
@synthesize submitText;

- (void)dealloc {
    [listUsage release];
    [title release];
    [subtitle release];
    [dailyUsage release];
    [weeklyUsage release];
    [monthlyUsage release];
    [choosePackage release];
    [recommended release];
    [daily release];
    [weekly release];
    [monthly release];
    [totalVolumeDailyString release];
    [totalVolumeWeeklyString release];
    [totalVolumeMonthlyString release];
    [calculateText release];
    [submitText release];
    [super dealloc];
}

@end
