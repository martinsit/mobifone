//
//  CheckUsageXLModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CheckUsageXLModel.h"

@implementation CheckUsageXLModel

@synthesize soccd;
@synthesize idService;
@synthesize name;
@synthesize nameEnc;
@synthesize desc;
@synthesize regDate;
@synthesize recDate;
@synthesize expDate;
@synthesize maxQuota;
@synthesize totalRemaining;
@synthesize totalRemainingPercent;
@synthesize packageAllowance;
@synthesize shareQuota;
@synthesize quotaMeter;
@synthesize shareQuotaList;
@synthesize showbar;
@synthesize isUnlimited;

- (void)dealloc {
    [soccd release];
    [idService release];
    [name release];
    [nameEnc release];
    [desc release];
    [regDate release];
    [recDate release];
    [expDate release];
    [maxQuota release];
    [totalRemaining release];
    [totalRemainingPercent release];
    [packageAllowance release];
    [shareQuota release];
    [quotaMeter release];
    [shareQuotaList release];
    [super dealloc];
}

@end
