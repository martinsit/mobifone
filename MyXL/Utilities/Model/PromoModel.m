//
//  PromoModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PromoModel.h"

@implementation PromoModel

NSString * const kPromo_id = @"promo_id";
NSString * const kPromo_title = @"promo_title";
NSString * const kPromo_body = @"promo_body";
NSString * const kPromo_dateTime = @"promo_datetime";
NSString * const kPromo_URL = @"promo_url";
NSString * const kPromo_status = @"promo_status";
NSString * const kPromo_shortDesc = @"promo_shortdesc";
NSString * const kPromo_menuID = @"promo_menuid";
NSString * const kPromo_type = @"promo_type";
NSString * const kPromo_msisdnType = @"promo_msisdn_type";
NSString * const kPromo_parentID = @"promo_parent_id";
NSString * const kPromo_typeLink = @"promo_type_link";
NSString * const kPromo_buttonAction = @"promo_button_action";
NSString * const kPromo_buttonGenerate = @"promo_button_generate";
NSString * const kPromo_off = @"promo_off";
NSString * const kPromo_image = @"promo_image";
NSString * const kPromo_start = @"promo_start";
NSString * const kPromo_end = @"promo_end";
NSString * const kPromo_show = @"promo_show";
NSString * const kPromo_sequence = @"promo_sequence";
NSString * const kPromo_totChild = @"promo_tot_child";
NSString * const kPromo_child = @"promo_child";
NSString * const kPromo_voucherDesc = @"promo_voucherdesc";
NSString * const kPromo_btnVoucher = @"promo_btnvoucher";
NSString * const kPromo_voucher = @"promo_voucher";
NSString * const kPromo_btnTerm = @"promo_btnterm";
NSString * const kPromo_termDesc = @"promo_termdesc";
NSString * const kPromo_relatedInfo = @"promo_relatedinfo";
NSString * const kPromo_relatedInfoURL = @"promo_relatedinfourl";

@synthesize promoId, promoTitle, promoBody, promoDateTime, promoUrl, promoStatus, promoShortDesc, promoMenuId, href;

- (void)dealloc {
    [promoId release];
    [promoTitle release];
    [promoBody release];
    [promoDateTime release];
    [promoUrl release];
    [promoStatus release];
    [promoShortDesc release];
    [promoMenuId release];
    [href release];
    [super dealloc];
}

@end
