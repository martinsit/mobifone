//
//  UpdateVersionModel.h
//  MyXL
//
//  Created by Tity Septiani on 5/26/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpdateVersionModel : NSObject
extern NSString * const kUpdateVersion_version;
extern NSString * const kUpdateVersion_code;
extern NSString * const kUpdateVersion_message;
extern NSString * const kUpdateVersion_url;
extern NSString * const kUpdateVersion_posBtnTitle;
extern NSString * const kUpdateVersion_cancelBtnTitle;

@property (nonatomic, copy) NSString *version;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *posBtnTitle;
@property (nonatomic, copy) NSString *cancelBtnTitle;

@end
