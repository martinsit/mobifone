//
//  MappingSukaSukaModel.m
//  MyXL
//
//  Created by tyegah on 2/5/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "MappingSukaSukaModel.h"

@implementation MappingSukaSukaModel

@synthesize serviceID;
@synthesize smsDetail;
@synthesize voiceDetail;
@synthesize internetDetail;
@synthesize price;
@synthesize sosmed;
@synthesize data;
@synthesize duration;
@synthesize custom_pkg;
@synthesize last_pkg;

- (void)dealloc {
    [serviceID release];
    [smsDetail release];
    [voiceDetail release];
    //[featureButton release];
    //[button release];
    
    [internetDetail release];
    [price release];
    [sosmed release];
    [data release];
    [duration release];
    [last_pkg release];
    
    [super dealloc];
}

@end
