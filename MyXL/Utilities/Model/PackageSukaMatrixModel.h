//
//  PackageSukaMatrixModel.h
//  MyXL
//
//  Created by tyegah on 2/16/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PackageSukaMatrixModel : NSObject

@property (nonatomic, retain) NSArray *voice;
@property (nonatomic, retain) NSArray *sms;
@property (nonatomic, retain) NSArray *data;
@property (nonatomic, retain) NSString *label_submit;
@property (nonatomic, retain) NSString *title_pkg;
@property (nonatomic, retain) NSString *notif;
@property (nonatomic, retain) NSString *prev_pkg;
@end
