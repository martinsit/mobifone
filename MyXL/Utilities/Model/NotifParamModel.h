//
//  NotifParamModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/4/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotifParamModel : NSObject {
    NSString *idx;
    NSString *label;
    NSString *type;
    NSString *content;
    NSString *value;
}

@property (nonatomic, retain) NSString *idx;
@property (nonatomic, retain) NSString *label;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *content;
@property (nonatomic, retain) NSString *value;

@end
