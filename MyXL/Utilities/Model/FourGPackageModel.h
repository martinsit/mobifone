//
//  FourGPackageModel.h
//  MyXL
//
//  Created by tyegah on 8/28/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FourGPackageImagesModel;
@class FourGPackageRewardsModel;
@class FourGPackageItemModel;
@class FourGPackageModel;

@interface FourGPackageModel : NSObject
@property (nonatomic, copy) NSString *matrix;
@property (nonatomic, copy) NSString *headerTitle;
@property (nonatomic, copy) NSString *headerDesc;

@property (nonatomic, copy) NSString *footerTitle;
@property (nonatomic, copy) NSString *footerUrl;

@property (nonatomic, retain) NSArray *images;
@property (nonatomic, retain) NSArray *packageList;

@property (nonatomic, retain) FourGPackageRewardsModel *rewardsModel;

-(instancetype)initWithDictionary:(NSDictionary *)dict;
@end

@interface FourGPackageItemModel : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *packageName;
@property (nonatomic, copy) NSString *href;
@property (nonatomic, copy) NSString *pkgId;
@property (nonatomic, copy) NSString *volume;
@property (nonatomic, copy) NSString *duration;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *confirmDesc;
@property (nonatomic, copy) NSString *paymentMethod;
@property (nonatomic, assign) BOOL isActive;

-(instancetype)initWithDictionary:(NSDictionary *)dict;
@end

@interface FourGPackageRewardsModel : NSObject
@property (nonatomic, copy) NSString *btnDescReward;
@property (nonatomic, copy) NSString *btnUrlReward;
@property (nonatomic, copy) NSString *linkDescReward;
@property (nonatomic, copy) NSString *linkUrlReward;
@property (nonatomic, copy) NSString *backgroundReward;

-(instancetype)initWithDictionary:(NSDictionary *)dict;
@end

@interface FourGPackageImagesModel : NSObject
@property (nonatomic, copy) NSString *images;
@property (nonatomic, copy) NSString *wordingTopBtn;
@property (nonatomic, copy) NSString *urlTopBtn;
@property (nonatomic, copy) NSString *wordingBtnTitle;
@property (nonatomic, copy) NSString *urlBtnTitle;

-(instancetype)initWithDictionary:(NSDictionary *)dict;
@end
