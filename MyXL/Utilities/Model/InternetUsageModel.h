//
//  InternetUsageModel.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 6/19/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InternetUsageModel : NSObject {
    NSString *code;
    NSString *title;
    NSString *color;
    NSString *iconUrl;
    int maxScaleValue;
    NSString *labelMax;
    int unitScaleValue;
    NSString *unit;
    NSString *conversion;
    double maxVolumeValue;
    double maxScalePerdayValue;
    NSString *descriptionDaily;
    NSString *descriptionWeekly;
    NSString *descriptionMonthly;
    
    float sliderValue;
    int sliderTag;
    float maxBar;
}

@property (nonatomic, retain) NSString *code;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *color;
@property (nonatomic, retain) NSString *iconUrl;
@property (readwrite) int maxScaleValue;
@property (nonatomic, retain) NSString *labelMax;
@property (readwrite) int unitScaleValue;
@property (nonatomic, retain) NSString *unit;
@property (nonatomic, retain) NSString *conversion;
@property (readwrite) double maxVolumeValue;
@property (readwrite) double maxScalePerdayValue;
@property (nonatomic, retain) NSString *descriptionDaily;
@property (nonatomic, retain) NSString *descriptionWeekly;
@property (nonatomic, retain) NSString *descriptionMonthly;

@property (readwrite) float sliderValue;
@property (readwrite) int sliderTag;
@property (readwrite) float maxBar;

@end
