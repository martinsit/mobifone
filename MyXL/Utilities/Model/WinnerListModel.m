//
//  WinnerListModel.m
//  MyXL
//
//  Created by tyegah on 8/7/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "WinnerListModel.h"

@implementation WinnerListModel
NSString * const kWinnerList_EventDate = @"EventDate";
NSString * const kWinnerList_RaffleDate = @"RaffleDate";
NSString * const kWinnerList_Msisdn = @"Msisdn";
NSString * const kWinnerList_Name = @"Name";
NSString * const kWinnerList_UrlImage = @"UrlImage";
@end
