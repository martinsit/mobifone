//
//  CheckUsageModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/28/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CheckUsageModel.h"

@implementation CheckUsageModel

@synthesize packageId;
@synthesize packageCode;
@synthesize packageName;
@synthesize startDate;
@synthesize endDate;
@synthesize packageType;
@synthesize usage;
@synthesize quota;
@synthesize balance;
@synthesize usagePercent;
@synthesize info;
@synthesize unregCmd;
@synthesize unsubCmd;
@synthesize flagUpgrade;
@synthesize s0;
@synthesize t1;
@synthesize s1;
@synthesize t2;
@synthesize s2;
@synthesize t3;
@synthesize s3;
@synthesize t4;
@synthesize s4;
@synthesize t5;
@synthesize s5;
@synthesize remainingPercentage;
@synthesize remainingNumber;
@synthesize remainingType;
@synthesize remainingLabel;

- (void)dealloc {
    [packageId release];
    [packageCode release];
    [packageName release];
    [startDate release];
    [endDate release];
    [packageType release];
    [usage release];
    [quota release];
    [balance release];
    [usagePercent release];
    [info release];
    [unregCmd release];
    [unsubCmd release];
    [flagUpgrade release];
    [s0 release];
    [t1 release];
    [s1 release];
    [t2 release];
    [s2 release];
    [t3 release];
    [s3 release];
    [t4 release];
    [s4 release];
    [t5 release];
    [s5 release];
    [remainingType release];
    [remainingLabel release];
    [super dealloc];
}

@end
