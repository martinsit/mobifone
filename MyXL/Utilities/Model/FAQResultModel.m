//
//  FAQResultModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "FAQResultModel.h"

@implementation FAQResultModel

@synthesize resultId;
@synthesize question;
@synthesize answer;
@synthesize open;
@synthesize isWebView;

- (void)dealloc {
    [super dealloc];
}

@end
