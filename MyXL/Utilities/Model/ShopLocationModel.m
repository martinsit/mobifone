//
//  ShopLocationModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/13/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ShopLocationModel.h"

@implementation ShopLocationModel

@synthesize latitude;
@synthesize longitude;
@synthesize title;
@synthesize address;
@synthesize time;

- (void)dealloc {
    [super dealloc];
}

@end
