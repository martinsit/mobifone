//
//  QuotaCalculatorModel.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 6/18/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuotaCalculatorModel : NSObject {
    NSArray *listUsage;
    NSString *title;
    NSString *subtitle;
    NSString *dailyUsage;
    NSString *weeklyUsage;
    NSString *monthlyUsage;
    NSString *choosePackage;
    NSString *recommended;
    NSString *daily;
    NSString *weekly;
    NSString *monthly;
    double totalVolumeDaily;
    double totalVolumeWeekly;
    double totalVolumeMonthly;
    NSString *totalVolumeDailyString;
    NSString *totalVolumeWeeklyString;
    NSString *totalVolumeMonthlyString;
    NSString *calculateText;
    NSString *submitText;
}

@property (nonatomic, retain) NSArray *listUsage;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *subtitle;
@property (nonatomic, retain) NSString *dailyUsage;
@property (nonatomic, retain) NSString *weeklyUsage;
@property (nonatomic, retain) NSString *monthlyUsage;
@property (nonatomic, retain) NSString *choosePackage;
@property (nonatomic, retain) NSString *recommended;
@property (nonatomic, retain) NSString *daily;
@property (nonatomic, retain) NSString *weekly;
@property (nonatomic, retain) NSString *monthly;
@property (readwrite) double totalVolumeDaily;
@property (readwrite) double totalVolumeWeekly;
@property (readwrite) double totalVolumeMonthly;
@property (nonatomic, retain) NSString *totalVolumeDailyString;
@property (nonatomic, retain) NSString *totalVolumeWeeklyString;
@property (nonatomic, retain) NSString *totalVolumeMonthlyString;
@property (nonatomic, retain) NSString *calculateText;
@property (nonatomic, retain) NSString *submitText;

@end
