//
//  BankModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankModel : NSObject {
    NSString *bankId;
    NSString *value;
    NSString *label;
    NSString *methodId;
    NSString *urlRef;
    NSString *labelRefId;
    NSString *labelRefEn;
    NSString *methodName;
}

@property (nonatomic, retain) NSString *bankId;
@property (nonatomic, retain) NSString *value;
@property (nonatomic, retain) NSString *label;
@property (nonatomic, retain) NSString *methodId;
@property (nonatomic, retain) NSString *urlRef;
@property (nonatomic, retain) NSString *labelRefId;
@property (nonatomic, retain) NSString *labelRefEn;
@property (nonatomic, retain) NSString *methodName;

@end
