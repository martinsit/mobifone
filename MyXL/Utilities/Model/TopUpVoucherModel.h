//
//  TopUpVoucherModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TopUpVoucherModel : NSObject {
    NSString *msisdnTo;
    NSString *balanceNew;
    NSString *activeStopNew;
    NSString *voucherValue;
    NSString *addValidity;
}

@property (nonatomic, retain) NSString *msisdnTo;
@property (nonatomic, retain) NSString *balanceNew;
@property (nonatomic, retain) NSString *activeStopNew;
@property (nonatomic, retain) NSString *voucherValue;
@property (nonatomic, retain) NSString *addValidity;

@end
