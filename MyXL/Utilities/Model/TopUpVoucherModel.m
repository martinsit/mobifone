//
//  TopUpVoucherModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "TopUpVoucherModel.h"

@implementation TopUpVoucherModel

@synthesize msisdnTo;
@synthesize balanceNew;
@synthesize activeStopNew;
@synthesize voucherValue;
@synthesize addValidity;

- (void)dealloc {
    [msisdnTo release];
    [balanceNew release];
    [activeStopNew release];
    [voucherValue release];
    [addValidity release];
    [super dealloc];
}

@end
