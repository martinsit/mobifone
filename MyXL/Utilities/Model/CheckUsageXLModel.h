//
//  CheckUsageXLModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckUsageXLModel : NSObject {
    NSString *soccd;
    NSString *idService;
    NSString *name;
    NSString *nameEnc;
    NSString *desc;
    NSString *regDate;
    NSString *recDate;
    NSString *expDate;
    NSString *maxQuota;
    NSString *totalRemaining;
    NSString *totalRemainingPercent;
    NSArray *packageAllowance;
    NSString *shareQuota;
    NSString *quotaMeter;
    NSArray *shareQuotaList;
    BOOL showbar;
    // TODO : V1.9
    // Quota Bar Unlimited
    BOOL isUnlimited;
}

@property (nonatomic, retain) NSString *soccd;
@property (nonatomic, retain) NSString *idService;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *nameEnc;
@property (nonatomic, retain) NSString *desc;
@property (nonatomic, retain) NSString *regDate;
@property (nonatomic, retain) NSString *recDate;
@property (nonatomic, retain) NSString *expDate;
@property (nonatomic, retain) NSString *maxQuota;
@property (nonatomic, retain) NSString *totalRemaining;
@property (nonatomic, retain) NSString *totalRemainingPercent;
@property (nonatomic, retain) NSArray *packageAllowance;
@property (nonatomic, retain) NSString *shareQuota;
@property (nonatomic, retain) NSString *quotaMeter;
@property (nonatomic, retain) NSArray *shareQuotaList;
@property (readwrite) BOOL showbar;
// TODO : V1.9
// Quota Bar Unlimited
@property (readwrite) BOOL isUnlimited;
@property (nonatomic, copy) NSString *fup;

@end
