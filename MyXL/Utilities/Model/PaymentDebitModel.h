//
//  PaymentDebitModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/20/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentDebitModel : NSObject {
    NSString *msisdnTo;
    NSString *amount;
    NSString *transType;
    NSString *transTypeLabel;
    NSString *bankName;
    NSString *balance;
    NSString *activeDate;
}

@property (nonatomic, retain) NSString *msisdnTo;
@property (nonatomic, retain) NSString *amount;
@property (nonatomic, retain) NSString *transType;
@property (nonatomic, retain) NSString *transTypeLabel;
@property (nonatomic, retain) NSString *bankName;
@property (nonatomic, retain) NSString *balance;
@property (nonatomic, retain) NSString *activeDate;

@end
