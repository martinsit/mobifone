//
//  ThemesModel.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 12/15/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThemesModel : NSObject {
    NSString *header;
    NSString *headerGradientStart;
    NSString *headerGradientEnd;
    
    //NSString *featureButton; // ??
    //NSString *button;        // ??
    
    NSString *menuColor; // Bg Pulsa Saya Home
    NSString *menuH3; // Text Color Pulsa Saya Home
    
    NSString *menuFeature; // Bg Cek Paket Home
    NSString *menuFeatureTxt; // Text Color Cek Paket Home
    
    NSString *accordionCurrentBg; // Bg Accordion Selected
    NSString *accordionBg; // Bg Accordion Normal
    
    NSString *packageButtonBg;   // Buy Button Bg
    NSString *packageButtonTxt;  // Buy Button Text
}

@property (nonatomic, retain) NSString *header;
@property (nonatomic, retain) NSString *headerGradientStart;
@property (nonatomic, retain) NSString *headerGradientEnd;
//@property (nonatomic, retain) NSString *featureButton;
//@property (nonatomic, retain) NSString *button;

@property (nonatomic, retain) NSString *menuColor;
@property (nonatomic, retain) NSString *menuH3;

@property (nonatomic, retain) NSString *menuFeature;
@property (nonatomic, retain) NSString *menuFeatureTxt;

@property (nonatomic, retain) NSString *accordionCurrentBg;
@property (nonatomic, retain) NSString *accordionBg;

@property (nonatomic, retain) NSString *packageButtonBg;
@property (nonatomic, retain) NSString *packageButtonTxt;

@end
