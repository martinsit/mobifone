//
//  MenuModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/10/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "MenuModel.h"

@implementation MenuModel

@synthesize href;
@synthesize hrefweb;
@synthesize menuId;
@synthesize menuName;
@synthesize parentId;
@synthesize groupId;
@synthesize groupName;
@synthesize desc;
@synthesize duration;
@synthesize price;
@synthesize volume;
@synthesize packageId;
@synthesize packageType;
@synthesize packageGroup;
@synthesize oriPackage;
@synthesize tooltips;
@synthesize topDesc;
@synthesize open;
@synthesize icon;
@synthesize hex;
@synthesize confirmDesc;
@synthesize banner;
@synthesize image;

- (void)dealloc {
    [href release];
    [hrefweb release];
    [menuId release];
    [menuName release];
    [parentId release];
    [groupId release];
    [groupName release];
    [desc release];
    [duration release];
    [price release];
    [volume release];
    [packageId release];
    [packageType release];
    [packageGroup release];
    [oriPackage release];
    [tooltips release];
    [topDesc release];
    [icon release];
    [hex release];
    [confirmDesc release];
    [banner release];
    [image release];
    [super dealloc];
}

@end
