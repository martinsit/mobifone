//
//  RenameSukaSukaModel.h
//  MyXL
//
//  Created by tyegah on 3/16/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RenameSukaSukaModel : NSObject
@property(nonatomic, retain) NSString *pageTitle;
@property (nonatomic, retain) NSString *pageDesc;
@property (nonatomic, retain) NSArray *packages;
@end
