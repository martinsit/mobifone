//
//  TransactionHistoryModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/25/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "TransactionHistoryModel.h"

@implementation TransactionHistoryModel

@synthesize destination;
@synthesize type;
@synthesize typeLabel;
@synthesize duration;
@synthesize durationType;
@synthesize callCharge;
@synthesize time;
@synthesize charging;

- (void)dealloc {
    [destination release];
    [type release];
    [typeLabel release];
    [duration release];
    [durationType release];
    [callCharge release];
    [time release];
    [charging release];
    [super dealloc];
}

@end
