//
//  ABFileModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ABFileModel.h"

@implementation ABFileModel

@synthesize filename;
@synthesize thumbnailExists;
@synthesize totalBytes;
@synthesize lastModifiedDate;
@synthesize dateString;
@synthesize humanReadableSize;
@synthesize icon;
@synthesize path;
@synthesize photo;
@synthesize rev;
@synthesize selected;
@synthesize name;
@synthesize peopleIndex;

@synthesize exportIndex;
@synthesize isExecuting;
@synthesize isFinished;
@synthesize isSuccess;

- (void)dealloc {
    [filename release];
    [lastModifiedDate release];
    [dateString release];
    [humanReadableSize release];
    [icon release];
    [path release];
    [photo release];
    [rev release];
    [name release];
    [super dealloc];
}

@end
