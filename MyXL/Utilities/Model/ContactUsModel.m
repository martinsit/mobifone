//
//  ContactUsModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ContactUsModel.h"

@implementation ContactUsModel

@synthesize href;
@synthesize label;
@synthesize value;
@synthesize actionValue;
@synthesize isData;

- (void)dealloc {
    [href release];
    [label release];
    [value release];
    [actionValue release];
    [super dealloc];
}

@end
