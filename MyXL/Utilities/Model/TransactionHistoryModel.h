//
//  TransactionHistoryModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/25/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionHistoryModel : NSObject {
    NSString *destination;
    NSString *type;
    NSString *typeLabel;
    NSString *duration;
    NSString *durationType;
    NSString *callCharge;
    NSString *time;
    NSString *charging;
}

@property (nonatomic, retain) NSString *destination;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *typeLabel;
@property (nonatomic, retain) NSString *duration;
@property (nonatomic, retain) NSString *durationType;
@property (nonatomic, retain) NSString *callCharge;
@property (nonatomic, retain) NSString *time;
@property (nonatomic, retain) NSString *charging;

@end
