//
//  BalanceTransferModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BalanceTransferModel : NSObject {
    NSString *msisdnTo;
    NSString *amount;
    NSString *charge;
    NSString *balanceNew;
    NSString *targetOldActiveStop;
    NSString *targetNewActiveStop;
}

@property (nonatomic, retain) NSString *msisdnTo;
@property (nonatomic, retain) NSString *amount;
@property (nonatomic, retain) NSString *charge;
@property (nonatomic, retain) NSString *balanceNew;
@property (nonatomic, retain) NSString *targetOldActiveStop;
@property (nonatomic, retain) NSString *targetNewActiveStop;

@end
