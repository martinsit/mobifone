//
//  XtraCheckBonusModel.m
//  MyXL
//
//  Created by tyegah on 8/31/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XtraCheckBonusModel.h"

@implementation XtraCheckBonusModel

-(instancetype)initWithDictionary:(NSDictionary *)dict
{
    if(self == [super init])
    {
        self.layanan = [dict objectForKey:@"layanan"];
        self.headerTopLabel = [dict objectForKey:@"header_top_lable"];
        self.headerLabel = [dict objectForKey:@"header_lable"];
        self.footerLabel = [dict objectForKey:@"footer_lable"];
        self.footerTodoLabel = [dict objectForKey:@"footer_todo_lable"];
        self.skLabel = [dict objectForKey:@"sk_lable"];
        self.footerHref = [dict objectForKey:@"footer_href"];
        self.skHref = [dict objectForKey:@"sk_href"];
        self.tombol = [[dict objectForKey:@"tombol"] boolValue];
        self.deskripsi = [dict objectForKey:@"deskripsi"];
        self.tombolLabel = [dict objectForKey:@"tombol_label"];
    }
    return self;
}
@end
