//
//  FAQResultModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAQResultModel : NSObject {
    NSString *resultId;
    NSString *question;
    NSString *answer;
    BOOL open;
    BOOL isWebView;
}

@property (nonatomic, retain) NSString *resultId;
@property (nonatomic, retain) NSString *question;
@property (nonatomic, retain) NSString *answer;
@property (readwrite) BOOL open;
@property (readwrite) BOOL isWebView;

@end
