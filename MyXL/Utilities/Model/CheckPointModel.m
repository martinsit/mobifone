//
//  CheckPointModel.m
//  MyXL
//
//  Created by tyegah on 8/6/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CheckPointModel.h"

@implementation CheckPointModel
NSString * const kCheckPoint_point = @"point";
NSString * const kCheckPoint_stock = @"stock";
NSString * const kCheckPoint_date = @"dateUndian";

@end
