//
//  PromoModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kPromo_id;
extern NSString * const kPromo_title;
extern NSString * const kPromo_body;
extern NSString * const kPromo_dateTime;
extern NSString * const kPromo_URL;
extern NSString * const kPromo_status;
extern NSString * const kPromo_shortDesc;
extern NSString * const kPromo_menuID;
extern NSString * const kPromo_type;
extern NSString * const kPromo_msisdnType;
extern NSString * const kPromo_parentID;
extern NSString * const kPromo_typeLink;
extern NSString * const kPromo_buttonAction;
extern NSString * const kPromo_buttonGenerate;
extern NSString * const kPromo_off;
extern NSString * const kPromo_image;
extern NSString * const kPromo_start;
extern NSString * const kPromo_end;
extern NSString * const kPromo_show;
extern NSString * const kPromo_sequence;
extern NSString * const kPromo_totChild;
extern NSString * const kPromo_child;
extern NSString * const kPromo_voucherDesc;
extern NSString * const kPromo_voucher;
extern NSString * const kPromo_btnVoucher;
extern NSString * const kPromo_btnTerm;
extern NSString * const kPromo_termDesc;
extern NSString * const kPromo_relatedInfo;
extern NSString * const kPromo_relatedInfoURL;

@interface PromoModel : NSObject {
    NSString *promoId;
    NSString *promoTitle;
    NSString *promoBody;
    NSString *promoDateTime;
    NSString *promoUrl;
    NSString *promoStatus;
    NSString *promoShortDesc;
    NSString *promoMenuId;
    NSString *href;
}

@property (nonatomic, retain) NSString *promoId;
@property (nonatomic, retain) NSString *promoTitle;
@property (nonatomic, retain) NSString *promoBody;
@property (nonatomic, retain) NSString *promoDateTime;
@property (nonatomic, retain) NSString *promoUrl;
@property (nonatomic, retain) NSString *promoStatus;
@property (nonatomic, retain) NSString *promoShortDesc;
@property (nonatomic, retain) NSString *promoMenuId;
@property (nonatomic, retain) NSString *href;
// TODO : V1.9
// PROMO MANAGEMENT
@property (nonatomic, copy) NSString *promoStart;
@property (nonatomic, copy) NSString *promoEnd;
@property (nonatomic, copy) NSString *promoShow;
@property (nonatomic, copy) NSString *promoSequence;
@property (nonatomic) NSInteger promoTotalChild;
@property (nonatomic, retain) NSArray *promoChild;
@property (nonatomic, copy) NSString *promoImage;
@property (nonatomic, copy) NSString *promoOff;
@property (nonatomic, copy) NSString *promoBtnGenerate;
@property (nonatomic, copy) NSString *promoBtnAction;
@property (nonatomic, copy) NSString *promoType;
@property (nonatomic, copy) NSString *promoMsisdnType;
@property (nonatomic, copy) NSString *promoParentID;
@property (nonatomic, copy) NSString *promoTypeLink;
@property (nonatomic, copy) NSString *promoVoucherDesc;
@property (nonatomic, copy) NSString *promoBtnVoucher;
@property (nonatomic, copy) NSString *promoBtnTerm;
@property (nonatomic, copy) NSString *promoRelatedInfo;
@property (nonatomic, copy) NSString *promoRelatedInfoURL;
@property (nonatomic, copy) NSString *promoTermDesc;
@property (nonatomic, copy) NSString *promoVoucher;
@end
