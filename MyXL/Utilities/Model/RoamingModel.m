//
//  RoamingModel.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "RoamingModel.h"

@implementation RoamingModel

@synthesize country;
@synthesize partner;
@synthesize data;

- (void)dealloc {
    [country release];
    [partner release];
    [data release];
    [super dealloc];
}

@end
