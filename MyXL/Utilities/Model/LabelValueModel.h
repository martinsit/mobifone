//
//  LabelValueModel.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LabelValueModel : NSObject {
    NSString *itemId;
    NSString *label;
    NSString *value;
}

@property (nonatomic, retain) NSString *itemId;
@property (nonatomic, retain) NSString *label;
@property (nonatomic, retain) NSString *value;

@end
