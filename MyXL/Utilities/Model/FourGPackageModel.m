//
//  FourGPackageModel.m
//  MyXL
//
//  Created by tyegah on 8/28/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "FourGPackageModel.h"

@implementation FourGPackageModel
-(instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self == [super init]) {
        self.matrix = [dict objectForKey:@"matrix"];
        self.headerTitle = [dict objectForKey:@"headerTitle"];
        self.headerDesc = [dict objectForKey:@"headerDesc"];
        
        self.footerTitle = [dict objectForKey:@"footerTitle"];
        self.footerUrl = [dict objectForKey:@"footerUrl"];
        
        NSArray *imgList = [dict objectForKey:@"images"];
        NSMutableArray *arrayImage = [[NSMutableArray alloc] initWithCapacity:[imgList count]];
        if ([imgList isKindOfClass:[NSArray class]] && [imgList count] > 0) {
            for (NSDictionary *imgDict in imgList) {
                FourGPackageImagesModel *item = [[FourGPackageImagesModel alloc] initWithDictionary:imgDict];
                [arrayImage addObject:item];
                [item release];
                item = nil;
            }
        }
        
        self.images = [arrayImage copy];
        
        NSArray *pkgList = [dict objectForKey:@"packageList"];
        if ([pkgList isKindOfClass:[NSArray class]]) {
            NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:[pkgList count]];
            if ([pkgList count] > 0) {
                for (NSDictionary *pkgDict in pkgList) {
                    FourGPackageItemModel *item = [[FourGPackageItemModel alloc] initWithDictionary:pkgDict];
                    [array addObject:item];
                    [item release];
                    item = nil;
                }
            }
            self.packageList = [array copy];
            [array release];
            array = nil;
        }
        else {
            self.packageList = [NSMutableArray array];
        }
        
        if([dict objectForKey:@"rewards"])
        {
            self.rewardsModel = [[FourGPackageRewardsModel alloc] initWithDictionary:[dict objectForKey:@"rewards"]];
        }
        
        
    }
    return self;
}

@end

@implementation FourGPackageItemModel
-(instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self == [super init]) {
        self.ID = [dict objectForKey:@"id"];
        self.packageName = [dict objectForKey:@"name"];
        self.href = [dict objectForKey:@"href"];
        self.pkgId = [dict objectForKey:@"pkgid"];
        self.volume = [dict objectForKey:@"volume"];
        self.duration = [dict objectForKey:@"duration"];
        self.price = [dict objectForKey:@"price"];
        self.confirmDesc = [dict objectForKey:@"confirmDesc"];
        self.paymentMethod = [dict objectForKey:@"payment_method"];
        self.isActive = [[dict objectForKey:@"isActive"] boolValue];
    }
    return self;
}

@end

@implementation FourGPackageRewardsModel
-(instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self == [super init]) {
        self.btnDescReward = [dict objectForKey:@"btnDescReward"];
        self.btnUrlReward = [dict objectForKey:@"btnUrlReward"];
        self.linkDescReward = [dict objectForKey:@"linkDescReward"];
        self.linkUrlReward = [dict objectForKey:@"linkUrlReward"];
        self.backgroundReward = [dict objectForKey:@"backgroundReward"];
    }
    return self;
}

@end

@implementation FourGPackageImagesModel
-(instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self == [super init]) {
        self.images = [dict objectForKey:@"image"];
        self.wordingTopBtn = [dict objectForKey:@"wordingTopBtn"];
        self.urlTopBtn = [dict objectForKey:@"urlTopBtn"];
        self.wordingBtnTitle = [dict objectForKey:@"wordingBtnTitle"];
        self.urlBtnTitle = [dict objectForKey:@"urlBtnTitle"];
    }
    return self;
}

@end
