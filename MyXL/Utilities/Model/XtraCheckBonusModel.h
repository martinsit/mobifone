//
//  XtraCheckBonusModel.h
//  MyXL
//
//  Created by tyegah on 8/31/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XtraCheckBonusModel : NSObject
@property (nonatomic, copy) NSString *headerTopLabel;
@property (nonatomic, copy) NSString *headerLabel;
@property (nonatomic, copy) NSString *footerLabel;
@property (nonatomic, copy) NSString *footerTodoLabel;
@property (nonatomic, copy) NSString *skLabel;
@property (nonatomic, copy) NSString *footerHref;
@property (nonatomic, copy) NSString *skHref;
@property (nonatomic, copy) NSString *layanan;
@property (nonatomic, assign) BOOL tombol;
@property (nonatomic, copy) NSString *deskripsi;
@property (nonatomic, copy) NSString *tombolLabel;

-(instancetype)initWithDictionary:(NSDictionary *)dict;
@end
