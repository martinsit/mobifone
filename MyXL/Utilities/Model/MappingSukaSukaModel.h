//
//  MappingSukaSukaModel.h
//  MyXL
//
//  Created by tyegah on 2/5/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MappingSukaSukaModel : NSObject
{
    NSString *serviceID;
    NSString *voiceDetail;
    NSString *internetDetail;
    NSString *smsDetail;
    NSString *price;
    NSDictionary *sosmed;
    NSDictionary *data;
    NSString *duration;
    NSString *custom_pkg;
    NSString *last_pkg;
}
@property (nonatomic, copy) NSString *serviceID;
@property (nonatomic, copy) NSString *voiceDetail;
@property (nonatomic, copy) NSString *smsDetail;
@property (nonatomic, copy) NSString *internetDetail;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, retain) NSDictionary *sosmed;
@property (nonatomic, retain) NSDictionary *data;
@property (nonatomic, retain) NSString *duration;
@property (nonatomic, retain) NSString *custom_pkg;
@property (nonatomic, retain) NSString *last_pkg;

@end
