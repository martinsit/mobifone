//
//  AXISnetCommon.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/12/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SurveyModel.h"
#import "AXISnetNavigationBar.h"
#import "FourGPowerPackViewController.h"
#import "QuotaImprovementModel.h"

@interface AXISnetCommon : NSObject

// Global Function

void addLeftBottomRoundedRect(CGContextRef context, CGRect rect, CGFloat cornerRadius, CGColorRef rectColor);

void addRightBottomRoundedRect(CGContextRef context, CGRect rect, CGFloat cornerRadius, CGColorRef rectColor);

void addRoundedRect(CGContextRef context, CGRect rect, CGFloat cornerRadius, CGColorRef rectColor);

void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, CGColorRef color);

void bottomRoundedRect(CGContextRef context, CGRect rect, CGFloat cornerRadius, CGColorRef rectColor, CGColorRef strokeColor, CGFloat strokeWidth);

CGRect rectFor1PxStroke(CGRect rect);

void addBackground(CGContextRef context, CGRect rect, CGColorRef rectColor);

UIButton *createIconicButtonWithFrame(CGRect frame,
                                      NSString *title,
                                      UIFont *titleFont,
                                      UIColor *textColor,
                                      UIColor *defaultColor,
                                      UIColor *selectedColor);

UIButton *createButtonWithFrame(CGRect frame,
                                NSString *title,
                                UIFont *titleFont,
                                UIColor *textColor,
                                UIColor *defaultColor,
                                UIColor *selectedColor);

UIButton *createButtonWithIcon(CGRect frame,
                               NSString *icon,
                               UIFont *iconFont,
                               UIColor *iconColor,
                               NSString *title,
                               UIFont *titleFont,
                               UIColor *textColor,
                               UIColor *defaultColor,
                               UIColor *selectedColor);

UIButton *createSquareButtonWithIcon(CGRect frame,
                                     NSString *icon,
                                     UIFont *iconFont,
                                     UIColor *iconColor,
                                     NSString *title,
                                     UIFont *titleFont,
                                     UIColor *titleColor,
                                     UIColor *defaultColor,
                                     UIColor *selectedColor);

UIButton *createButtonWithIconAndArrow(CGRect frame,
                                       NSString *icon,
                                       UIFont *iconFont,
                                       UIColor *iconColor,
                                       NSString *title,
                                       UIFont *titleFont,
                                       UIColor *titleColor,
                                       NSString *arrow,
                                       UIFont *arrowFont,
                                       UIColor *arrowColor,
                                       UIColor *defaultColor,
                                       UIColor *selectedColor);

UILabel *createLabel(CGRect frame, UIFont *font, NSString *text, UIColor *textColor, int numberOfLines, NSTextAlignment textAlignment);

UIImage *imageFromColor(UIColor *color);

UIImage *imageFromButton(UIButton *button);

UIImage *imageFromLabel(UILabel *label);

UIImage *createImageWithRect(CGRect rect, UIColor *color);

NSString *generateUniqueString();

NSString *generateCaptchaURL();

UINavigationController *customizedNavigationController(CGRect rect, BOOL isLogin);

NSString *convertPackageType(NSString *packageType);

CGSize calculateExpectedSize(UILabel *label, NSString *text);

// TODO : Neptune
CGSize calculateLabelSize(CGFloat labelWidth, NSString *text, UIFont *font);

NSString *addThousandsSeparator(NSString *inputString, NSString *lang);

NSString *changeDateFormat(NSString *inputDate);

NSString *changeDateFormatForPackage(NSString *inputDate);

NSString *changeDateTimeFormatForNotification(NSString *inputDate);

NSString *changeDateFormatForNotification(NSString *inputDate, int output);

BOOL isAllDigits(NSString* inputString);

BOOL validateUrl(NSString *candidate);

void setupIcon();

NSString *icon(NSString *menuId);

BOOL isContainHTMLTag(NSString *inputString);

BOOL validateChoice(SurveyModel *surveyModel, int input);

NSString *appVersion();

UIColor *legendColorForBar(int legendColorDef);

BOOL isValidEmail(NSString* checkString);

BOOL isValidCharacter(NSString* checkString);

void resetAllData();

/*
void drawLinearGradient(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef  endColor);
void addRoundedRectWithStroke(CGContextRef context, CGRect rect, CGFloat cornerRadius, CGColorRef rectColor, CGColorRef strokeColor, CGFloat strokeWidth);
void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth, float ovalHeight);*/

UIColor *colorWithHexString(NSString *hex);

BOOL isStandardLogoVersionSame(NSString *logoVersion, NSString *telcoOperator);
BOOL isRetinaLogoVersionSame(NSString *logoVersion, NSString *telcoOperator);
UIImage *radialGradientImage(CGSize size, float startR, float startG, float startB, float endR, float endG, float endB, CGPoint centre, float radius);
UIImage *radialGradientImage2(CGSize size, UIColor *start, UIColor *end, CGPoint centre, float radius);

NSString *applicationDocumentsDirectory();

void saveImage(UIImage *image, NSString *imageName);

void changeNavbarBg(AXISnetNavigationBar *navBar, CGRect rect);

// TODO : Hygiene
void saveDataToPreferenceWithKeyAndValue(NSString *key, id value);

id getDataFromPreferenceWithKey(NSString *key);

void saveCustomObjectQuota(QuotaImprovementModel *object, NSString *key);

QuotaImprovementModel* loadCustomObjectQuotaWithKey(NSString *key);

BOOL validateMsisdn(NSString *msisdn);
// TODO : Neptune
CGSize calculateLabelSize(CGFloat labelWidth, NSString *text, UIFont *font);

// TODO : V1.9
/**
 *  Convert double value into currency value in IDR
 *
 *  @param numberToFormat The number to format (in double)
 *
 *  @return The formatted string like currency
 */
+(NSString *)formatStringWithCurrency:(double)numberToFormat;

// TODO : V1.9
+(void)saveGAITrackingID:(NSString *)trackingID;
+(NSString *)getGAITrackingID;

+(NSString *)formatDateFromString:(NSString *)strDate;

// TODO 4G PowerPack
MatrixType convertMatrix(NSString *input);

@end
