//
//  GroupingAndSorting.h
//  Aconnect
//
//  Created by Tony Hadisiswanto on 9/5/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupingAndSorting : NSObject

+ (NSDictionary*)doGrouping:(NSArray*)data;
+ (NSDictionary*)doSortingByMenuId:(NSDictionary*)data;
+ (NSArray*)doSortingByGroupId:(NSArray*)data;
+ (NSArray*)doSortingByMenuIdForHome:(NSArray*)data;

@end
