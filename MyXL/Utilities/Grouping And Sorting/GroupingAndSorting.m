//
//  GroupingAndSorting.m
//  Aconnect
//
//  Created by Tony Hadisiswanto on 9/5/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import "GroupingAndSorting.h"
#import "MenuModel.h"

@implementation GroupingAndSorting

- (void)dealloc {
	[super dealloc];
}

+ (NSDictionary*)doGrouping:(NSArray*)data {
    NSMutableDictionary *theDictionary = [NSMutableDictionary dictionary];
    
    for (MenuModel *menuModel in data) {
        NSMutableArray *theMutableArray = [theDictionary objectForKey:menuModel.groupId];
        if (theMutableArray == nil) {
            theMutableArray = [NSMutableArray array];
            [theDictionary setObject:theMutableArray forKey:menuModel.groupId];
        }
        
        [theMutableArray addObject:menuModel];
    }
    
    return theDictionary;
}

+ (NSDictionary*)doSortingByMenuId:(NSDictionary*)data {
    NSMutableDictionary *theDictionary = [NSMutableDictionary dictionary];
    
    NSArray *keys = [data allKeys];
    for (int i=0; i<[keys count]; i++) {
        NSArray *tmpArray = [data valueForKey:[keys objectAtIndex:i]];
        tmpArray = [tmpArray sortedArrayUsingComparator:^(MenuModel *a, MenuModel *b) {
            
            /*
             * Enhance by iNot
             */
            if ([a.parentId isEqualToString:@"8"]) {
                NSString *aString = [a.menuId stringByReplacingOccurrencesOfString:@"_" withString:@""];
                
                NSString *bString = [b.menuId stringByReplacingOccurrencesOfString:@"_" withString:@""];
                
                if ([aString integerValue] == 0 && [bString integerValue] == 0)
                {
                    return (NSComparisonResult)NSOrderedSame;
                }
                if ([aString integerValue] == 0)
                {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                if ([bString integerValue] == 0)
                {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                
                if ([aString integerValue] > [bString integerValue])
                {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                
                if ([aString integerValue] < [bString integerValue])
                {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                return (NSComparisonResult)NSOrderedSame;
                
            }
            else {
                // TODO : V1.9
                // Sorting package
                // DROP 2
                if(a.position > 0 && a.position > 0)
                {
                    if (a.position > b.position) {
                        return (NSComparisonResult)NSOrderedAscending;
                    }
                }
                
                if ([a.menuId intValue] > [b.menuId intValue]) {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                return (NSComparisonResult)NSOrderedSame;
            }
        }];
        [theDictionary setObject:tmpArray forKey:[keys objectAtIndex:i]];
    }
    
    return theDictionary;
}

+ (NSArray*)doSortingByGroupId:(NSArray*)data {
    NSArray *sortingResult = [data sortedArrayUsingComparator:^(NSString *a, NSString *b) { 
        return [a compare:b options:NSNumericSearch]; 
    }];
    return sortingResult;
}

+ (NSArray*)doSortingByMenuIdForHome:(NSArray*)data {
    NSArray *sortingResult = [data sortedArrayUsingComparator:^(MenuModel *a, MenuModel *b) {
        return [[a menuId] compare:[b menuId]];
    }];
    return sortingResult;
}

@end
