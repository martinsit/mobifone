//
//  Language.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/23/12.
//  Copyright (c) 2012 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Language : NSObject

+(void)initialize;
+(void)setLanguage:(NSString *)l;
+(NSString *)get:(NSString *)key alter:(NSString *)alternate;
+(NSString *)languageSelectedStringForKey:(NSString*)key;
+(int)getCurrentLanguage;

@end
