//
//  Language.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/23/12.
//  Copyright (c) 2012 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "Language.h"

@implementation Language

static NSBundle *bundle = nil;

+(void)initialize {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *current = [prefs stringForKey:@"language"];
    
    if (current == nil) {
        NSArray* languages = [prefs objectForKey:@"AppleLanguages"];
        current = [[languages objectAtIndex:0] retain];
        
        if (![current isEqualToString:@"en"] && ![current isEqualToString:@"id"]) {
            current = @"id";
        }
        // Default lang = id
        else {
            current = @"id";
        }
        
        [prefs setObject:current
                  forKey:@"language"];
        [prefs synchronize];
    }
    
    [self setLanguage:current];
}

+(void)setLanguage:(NSString *)l {
    //NSLog(@"preferredLang: %@", l);
    NSString *path = [[NSBundle mainBundle] pathForResource:l ofType:@"lproj"];
    bundle = [[NSBundle bundleWithPath:path] retain];
    
    // Set it in the UserPref
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:l forKey:@"language"];
    [prefs synchronize];
}

+(NSString *)get:(NSString *)key alter:(NSString *)alternate {
    return [bundle localizedStringForKey:key value:alternate table:nil];
}

+(NSString *)languageSelectedStringForKey:(NSString*)key
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *current = [prefs stringForKey:@"language"];
    
	NSString *path;
	if([current isEqualToString:@"en"])
		path = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
	else
		path = [[NSBundle mainBundle] pathForResource:@"id" ofType:@"lproj"];
	
	NSBundle *languageBundle = [NSBundle bundleWithPath:path];
	NSString *str = [languageBundle localizedStringForKey:key value:@"" table:nil];
	return str;
}

+(int)getCurrentLanguage {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *current = [prefs stringForKey:@"language"];
    if ([current isEqualToString:@"en"]) {
        return 0;
    }
    else {
        return 1;
    }
}

- (void)dealloc {
	[super dealloc];
}

@end
