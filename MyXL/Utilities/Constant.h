//
//  Constant.h
//  Pouch
//
//  Created by Tony Hadisiswanto on 12/5/12.
//  Copyright (c) 2012 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

// TODO : V1.9
// This Constant seems to be returning invalid result so, it is changed to the new one.
//#ifdef UI_USER_INTERFACE_IDIOM
//#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//#else
//#define IS_IPAD false
//#endif

#ifndef IS_IPAD
#define IS_IPAD ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#endif

#define SCREEN_WIDTH (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)
#define SCREEN_HEIGHT (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)

/*
 * 4G Powerpack Color
 */
#define kDefaultOceanBlueColor      [UIColor colorWithRed:181.0/255.0 green:225.0/255.0 blue:252.0/255.0 alpha:1.0]
#define kDefaultDarkBlueColor       [UIColor colorWithRed:26.0/255.0 green:94.0/255.0 blue:174.0/255.0 alpha:1.0]
//#define kDefaultDarkBlueColor     [UIColor colorWithRed:18.0/255.0 green:70.0/255.0 blue:161.0/255.0 alpha:1.0]
/*------------------*/

/*
 * 4G Tank Color
 */
#define kDefaultRedQuotaColor       @"EF3A13"
#define kDefaultGreenQuotaColor     @"8EC63F"
/*------------------*/

//#define kDefaultBaseColor         [UIColor whiteColor]
#define kDefaultOrangeColor         [UIColor colorWithRed:230.0/255.0 green:93.0/255.0 blue:0.0 alpha:1.0]

#define kDefaultHygieneBgColor      [UIColor colorWithRed:18.0/255.0 green:36.0/255.0 blue:71.0/255.0 alpha:1.0]

#define kDefaultNavyBlueColor       [UIColor colorWithRed:0.0/255.0 green:42.0/255.0 blue:110.0/255.0 alpha:1.0]
#define kDefaultAquaMarineColor     [UIColor colorWithRed:2.0/255.0 green:181.0/255.0 blue:221.0/255.0 alpha:1.0]
#define kDefaultTextHeaderColor     [UIColor colorWithRed:109.0/255.0 green:218.0/255.0 blue:243.0/255.0 alpha:1.0]
#define kDefaultYellowColor         [UIColor colorWithRed:246.0/255.0 green:234.0/255.0 blue:41.0/255.0 alpha:1.0]
#define kDefaultGrayColor           [UIColor colorWithRed:203.0/255.0 green:206.0/255.0 blue:217.0/255.0 alpha:1.0]
#define kDefaultSeparatorColor      [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0]
#define kDefaultWhiteSmokeColor     [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:239.0/255.0 alpha:1.0]


//#define kDefaultBlueColor         [UIColor colorWithRed:81.0/255.0 green:168.0/255.0 blue:241.0/255.0 alpha:1.0]
#define kDefaultBlueColor           [UIColor colorWithRed:35.0/255.0 green:103.0/255.0 blue:178.0/255.0 alpha:1.0]
#define kDefaultWhiteColor          [UIColor whiteColor]
#define kDefaultBlackColor          [UIColor blackColor]
#define kDefaultBaseColor           [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0]

//#define kDefaultPurpleColor       [UIColor colorWithRed:122.0/255.0 green:24.0/255.0 blue:120.0/255.0 alpha:1.0]
#define kDefaultPurpleColor         [UIColor colorWithRed:39.0/255.0 green:66.0/255.0 blue:117.0/255.0 alpha:1.0]

#define kDefaultPinkColor           [UIColor colorWithRed:238.0/255.0 green:43.0/255.0 blue:116.0/255.0 alpha:1.0]
#define kDefaultLightPinkColor      [UIColor colorWithRed:253.0/255.0 green:223.0/255.0 blue:233.0/255.0 alpha:1.0]
#define kDefaultDarkPinkColor       [UIColor colorWithRed:252.0/255.0 green:177.0/255.0 blue:202.0/255.0 alpha:1.0]
#define kDefaultBackgroundGrayColor [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0]
//#define kDefaultButtonGrayColor   [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0]

#define kDefaultButtonGrayColor     [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0]

#define kDefaultButtonYellowColor   [UIColor colorWithRed:254.0/255.0 green:170.0/255.0 blue:29.0/255.0 alpha:1.0]

#define kDefaultTitleFontGrayColor  [UIColor colorWithRed:119.0/255.0 green:119.0/255.0 blue:119.0/255.0 alpha:1.0]
#define kDefaultFontLightGrayColor  [UIColor lightGrayColor]
#define kDefaultFontGrayColor       [UIColor grayColor]
#define kDefaultGreenColor          [UIColor colorWithRed:57.0/255.0 green:141.0/255.0 blue:66.0/255.0 alpha:1.0]

#define kDefaultRedColor            [UIColor colorWithRed:223.0/255.0 green:34.0/255.0 blue:39.0/255.0 alpha:1.0]
#define kDefaultDarkGrayColor       [UIColor colorWithRed:132.0/255.0 green:132.0/255.0 blue:132.0/255.0 alpha:1.0]


#define splashLoadingPinkColor      [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0]
#define splashLoadingPurpleColor    [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0]
#define splashLoadingGrayColor      [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0]
#define splashLoadingWidth          100.0
#define splashLoadingHeight         20.0

//#define leftMenuColor             [UIColor colorWithRed:55.0/255.0 green:55.0/255.0 blue:55.0/255.0 alpha:1.0]
#define leftMenuColor               [UIColor colorWithRed:189.0/255.0 green:189.0/255.0 blue:189.0/255.0 alpha:1.0]
//#define leftMenuSelectedColor     [UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1.0]
#define leftMenuSelectedColor       [UIColor colorWithRed:81.0/255.0 green:168.0/255.0 blue:241.0/255.0 alpha:1.0]
#define leftMenuHeaderColor         [UIColor colorWithRed:36.0/255.0 green:36.0/255.0 blue:36.0/255.0 alpha:1.0]
#define leftMenuHeaderTextColor     [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0]
//#define navigationBarColor        [UIColor colorWithRed:225.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1.0]
#define navigationBarColor          [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0]

#define kDefaultStrokeWidth         1.0
#define kDefaultCornerRadius        7.0 //7
#define kDefaultPadding             20.0
#define kDefaultLeftPadding         10.0

//#define kDefaultFont              @"OpenSans"
//#define kDefaultFont              @"CoText-Regular"
//#define kDefaultFont              @"Roboto-Thin"
#define kDefaultFont              @"GothamRounded-Medium"
//#define kDefaultFont                @"Font Handle Regular"


//#define kDefaultFontLight         @"OpenSans-Light"
//#define kDefaultFontLight         @"CoText-Light"
//#define kDefaultFontLight         @"Roboto-Thin"
#define kDefaultFontLight         @"GothamRounded-Light"
//#define kDefaultFontLight           @"Font Handle Regular"

//#define kDefaultFontBold          @"OpenSans-Extrabold"
//#define kDefaultFontBold          @"CoHeadline-Bold"
//#define kDefaultFontBold          @"Roboto-Regular"
//#define kDefaultFontBold          @"RobotoCondensed-Light"
#define kDefaultFontBold          @"GothamRounded-Bold"
//#define kDefaultFontBold            @"Font Handle Bold"

//#define kDefaultFontBold   @"RobotoCondensed-Bold"
//#define kDefaultFontLight  @"RobotoCondensed-Light"

#define kDefaultFontKSAN            @"K-SAN"
#define kDefaultFontHeaderSize      14.0
#define kDefaultFontSize            13.0 // 14
#define kDefaultFontTitleSize       12.0

//#define kDefaultFontValueSize (IS_IPAD ? 16.0 : 12.0)
#define kDefaultFontValueSize (IS_IPAD ? 14.0 : 10.0)
//#define kDefaultFontValueSize       16.0 //12

//#define kDefaultFontSmallSize       10.0
#define kDefaultFontSmallSize       8.0
#define kDefaultComponentPadding    8.0
#define kDefaultPaddingDeduct       5.0

#define kDefaultButtonWidth         42.0 //45
#define kDefaultButtonHeight        30.0

#define kDefaultBadgeWidth          30.0
#define kDefaultBadgeHeight         30.0

#define kDefaultCellHeight          42.0
//#define kDefaultCellHeight        50.0

#define firstSectionHeaderHeight    87.0
#define otherSectionHeaderHeight    47.0
#define bottomSectionHeaderHeight   7.0

#define defaultSectionHeaderHeight    22.0

#define PREPAID 1
#define POSTPAID 2

#define kDefaultCache               1.0
#define BALANCE_CACHE_KEY @"balance_cache"
#define USAGE_CACHE_KEY   @"usage_cache"
#define PROMO_CACHE_KEY   @"promo_cache"
#define RATING_CACHE_KEY  @"rating_cache"
// TODO : V1.9.5
#define KEEP_SIGNIN_CACHE_KEY       @"keep_signin_cache"
#define PROFILE_MSISDN_CACHE_KEY    @"profile_msisdn_cache"
#define PROFILE_PASSWORD_CACHE_KEY  @"profile_password_cache"
#define PROFILE_TOKEN_CACHE_KEY     @"profile_token_cache"
#define PROFILE_FLAG_KMSI_CACHE_KEY @"profile_flag_kmsi_cache"

#define RESULT_KEY  @"result"
#define REASON_KEY  @"reason"
#define REQUEST_KEY @"request"
#define REPEAT_KEY  @"repeat"
#define DATA_KEY    @"data"
// TODO : Hygiene
#define ERROR_CODE_KEY @"error_code"
#define MAX_MSISDN_LENGTH 20
#define MIN_MSISDN_LENGTH 8
#define MIN_PASSWORD_LENGTH 6
#define MAX_PASSWORD_LENGTH 64

#define AMOUNT_KEY  @"amount"
#define BANK_KEY    @"bank"
#define METHOD_KEY  @"method"

#define USERNAME       @"online";
#define PASSWORD       @"axis";
#define SALT_KEY       @"ax1s0aSis";

#define DEVICE_BRAND   @"Apple";

#define XL_LOGO_STD         @"xl_logo_std";
#define XL_LOGO_RETINA      @"xl_logo_retina";
#define AXIS_LOGO_STD       @"axis_logo_std";
#define AXIS_LOGO_RETINA    @"axis_logo_retina";

#define XL_LOGO_STD_NAME        @"xl_logo.png";
#define XL_LOGO_RETINA_NAME     @"xl_logo_retina.png";
#define AXIS_LOGO_STD_NAME      @"axis_logo.png";
#define AXIS_LOGO_RETINA_NAME   @"axis_logo_retina.png";

/*
// XL
#define SECRET_KEY_SSO (IS_IPAD ? @"198d7c32c525cdbc" : @"198d7c32c525cdbc");
#define SHARED_KEY_SSO (IS_IPAD ? @"b0648ece62941285" : @"b0648ece62941285");*/

// XL With M-Card Feature
#define SECRET_KEY_SSO (IS_IPAD ? @"796eda0f978ceac4" : @"796eda0f978ceac4"); // 3ea47788c4ac6578 --> iPad
#define SHARED_KEY_SSO (IS_IPAD ? @"8e17495c35200d68" : @"8e17495c35200d68"); // b6c31f993abde571 --> iPad

//#define SECRET_KEY_SSO (IS_IPAD ? @"765f0047a1408660" : @"765f0047a1408660"); // Nokia X
//#define SHARED_KEY_SSO (IS_IPAD ? @"ea3446a36539655d" : @"ea3446a36539655d"); // Nokia X

/*
 * The Value for Production Env = 1
 * The Value for Staging Env = 0
 * IF STAGING == 0 -> DEV ENVIRONMENT
 * IF STAGING == 1 -> STAGING ENVIRONMENT
 */

#define IS_STAGING 1

#define IS_PRODUCTION 0

#if IS_PRODUCTION == 1
/*
 * Production
 */
#define BASE_URL @"http://api.my.xl.co.id"

#else
/*
 * Staging
 */
#if IS_STAGING == 1
#define BASE_URL @"http://api123.xl.co.id"    //staging
//#define BASE_URL @"http://api123ddos.xl.co.id"  //ddos
#else
#define BASE_URL @"http://apidev.my.xl.co.id/myxl19"   //dev
#endif
// replica
//#define BASE_URL @"http://api.my.xl.co.id/api"

/*
 * UAT
 */
//#define BASE_URL @"http://api.my.xl.co.id/uat"

#endif

// FIXME: Uncomment this
#if (IS_STAGING == 0 || IS_PRODUCTION == 1)
#define SSO @"/sso"
#else
#define SSO @"/api/sso"
#endif

#define GET_PROFILE @"/getprofile"
#define SIGN_IN @"/signin"
#define SIGN_OUT @"/signout"
#define CAPTCHA @"/captcha"
#define REGISTER @"/register"
#define ACTIVATE_ACCOUNT @"/activate"
#define RESET_PASSWORD @"/password/reset"
#define CHANGE_PASSWORD @"/password/change"
#define RESEND_PIN @"/pin/resent"
#define REG_DEV_TOKEN @"/subs/devicetoken"
#define REG_FB_TOKEN @"/sosmed/fb_link"
#define UNLINK_FB @"/sosmed/fb_unlink"
#define SHARE_FB @"/sosmed/fb_notif"

// TODO : Hygiene
#define REQUEST_PASSWORD @"/password/reset"

#if IS_PRODUCTION == 1
/*
 * Production
 */
#define API @""

#else
/*
 * Staging
 */
#if IS_STAGING == 1
#define API @"/api"   //staging
#else
#define API @""         //dev
#endif

#endif

//#define GET_MENU @"/oasys/menu/ww_menu" //--> menu 1
#define GET_MENU @"/oasys/menu/ww_menu2"
#define UPDATE_PROFILE @"/oasys/profile/update"
#define XL_STAR @"/oasys/profile/xlstar"
#define CHANGE_LANGUAGE @"/oasys/profile/change_language"

#define CHECK_BALANCE @"/oasys/balance/check"
#define TOPUP_VOUCHER @"/oasys/balance/reloadV2"
#define BALANCE_ITEMS @"/oasys/balance/items"
#define BALANCE_TRANSFER @"/oasys/balance/transfer"
#define EXTEND_VALIDITY @"/oasys/balance/extend"

#define CHECK_USAGE @"/oasys/package/usage"
#define CHECK_USAGE_XL @"/oasys/package/usage_xl"
#define RECOMMENDED_PACKAGE @"/oasys/package/recommended"
#define RECOMMENDED_PACKAGE_XL @"/oasys/package/recommended_xl"
#define GIFT_PACKAGE @"/oasys/package/gift"
#define BUY_PACKAGE @"/oasys/package/buy"
#define STOP_PACKAGE @"/oasys/package/unsubscribe"
#define UPGRADE_PACKAGE @"/oasys/package/upgrade"

#define SURVEY @"/selfcare/survey/request"
#define SURVEY_SUBMIT @"/selfcare/survey/submit"
#define LAST_TRANSACTION @"/selfcare/transaction/last5"

#define PROMO @"/oasys/promo/last_promo"

//#define NOTIFICATION @"/oasys/notification/info"
#define NOTIFICATION @"/oasys/notification/header"
#define NOTIFICATION_DETAIL @"/oasys/notification/message"
#define NOTIFICATION_DELETE @"/oasys/notification/delete"
#define NOTIFICATION_REPLY @"/oasys/notification/reply"

#define ADS @"/ads/request/request_ads"

#define PAYMENT_PARAM @"/oasys/payment/param"
#define PAYMENT_HELP @"/oasys/payment/help"
#define PAYMENT_DEBIT @"/oasys/payment/topup_debit"

//#define CONTACT_US @"/selfcare/contactus/request"
#define CONTACT_US @"/selfcare/contactus/requestv2"
#define PUK @"/selfcare/puk/request"
#define DEVICE_SETTING @"/selfcare/device/info_setting"
#define SHOP_LOCATION @"/selfcare/shop/location"
#define FAQ @"/selfcare/faq/request"
#define ROAMING @"/selfcare/roaming/request"

// Info 4G USIM
#define INFO_4G_USIM @"/selfcare/4gusim/request"
#define REPLACE_4G_USIM @"/oasys/profile/replaceCard"
#define RATE_COMMENT_4G_USIM @"/selfcare/4gusim/rate4g"

#define XL_TUNAI_BALANCE @"/selfcare/xltunai/balance"
#define XL_TUNAI_RELOAD @"/selfcare/xltunai/reload"
#define XL_TUNAI_PLN @"/selfcare/xltunai/pln"

#define HOT_OFFER_BANNER @"/selfcare/offer/banner"

#define PROFILE_INFO @"/oasys/profile/getInfo"

#define AXIS_STORE_TERM_AND_CONDITION   @"/onlinechannel/transaction/toc"
#define AXIS_STORE_USER_GUIDE           @"/onlinechannel/transaction/guide"
#define AXIS_STORE_TRX_HISTORY          @"/onlinechannel/transaction/history"
#define AXIS_STORE_CATEGORY             @"/onlinechannel/category/gets"
#define AXIS_STORE_PRODUCT              @"/onlinechannel/product/gets"

#define QUOTA_METER                     @"/oasys/package/quota_meter"
#define SHARE_QUOTA                     @"/oasys/package/share"
#define M_KARTU                         @"/payment/partner/mkartu/?p="
// TODO : v1.9
#define BUY_PKG_M_KARTU                 @"/payment/partner/creditcard/?p="

#define QUOTA_CALCULATOR                @"/oasys/quota/usagesimulator"
#define QUOTA_CALCULATOR_CALCULATE      @"/oasys/quota/calculate"

// TODO : NEPTUNE
#define USAGE_SIMULATOR_SUKASUKA        @"/oasys/quota/usagesimulatorSukasuka"
#define MAPPING_PAKET_SUKASUKA          @"/oasys/sukasuka/mapping"
#define BUY_PAKET_SUKASUKA              @"/oasys/package/buySukasuka"
#define PACKAGE_MATRIX_SUKASUKA         @"/oasys/sukasuka/matrix"
#define PAGE_SUKASUKA                   @"/oasys/sukasuka/pagesesukamu"
#define QUOTA_SUKASUKA                  @"/oasys/sukasuka/quotasesukamu"
#define MAPPING_RENAME_SUKASUKA         @"/oasys/sukasuka/mappingrename"
#define RENAME_SUKASUKA                 @"/oasys/sukasuka/renameproc"

// Update defect push notification
// Separate API for updating device token
#define UPDATE_DEVICE_TOKEN @"/subs/updatedevicetoken"

// TODO : V1.9
#define VOUCHER_PROMO   @"/oasys/voucher/voucher_promo"

// XTRA PROMO
#define XTRAPROMO_CHECK_POINT @"/oasys/xvaganza/checkPoint"
#define XTRAPROMO_WINNER_LIST  @"/oasys/xvaganza/winnerList"
#define XTRAPROMO_TERMSCONS_URL  @"http://60mazda2.xl.co.id/syaratdanketentuan"
#define XTRAPROMO_GETBONUS  @"/oasys/xvaganza/take_ambilbonus"
#define XTRAPROMO_CHECKGETBONUS @"/oasys/xvaganza/ceker_ambilbonus"

// 4G POWERPACK
#define FOURG_POWERPACK @"/oasys/fourgpower/loadPage"
#define FOURG_ISPACKAGE4G @"/oasys/fourgpower/packageQuery"

// TODO : V1.9.5
#define QUOTA_IMPROVEMENT @"/oasys/fourgpower/quotaImprovement"

#define CID_ADS_IPAD @"tyui097f56yt"
#define CID_ADS_IPHONE @"345809koiuy"
//#define CID_ADS_IPHONE @"uitgk87lh"
#define ADS_SIZE_IPAD @"728x90"
#define ADS_SIZE_HOME_IPHONE @"320x50"
#define ADS_SIZE_HOME_IPAD @"1024x66" //1024x66 //1024x160
#define ADS_SIZE_OTHER_IPHONE @"320x50"

#define HOME_PAGE @"1"
#define BALANCE_PAGE @"2"
#define PACKAGE_PAGE @"3"
#define HOT_PROMO_PAGE @"4"
#define AXIS_STORE_PAGE @"5"
#define INFO_HELP_PAGE @"6"
#define CHECK_BALANCE_PAGE @"7"
#define CHECK_BONUS_PAGE @"8"
#define RELOAD_BALANCE_PAGE @"9"
#define BUY_VOUCHER_PAGE @"10"
#define EXTEND_VALIDITY_PAGE @"11"
#define TRX_HISTORY_PAGE @"12"
#define CHECK_USAGE_PAGE @"13"
#define MY_PACKAGE_PAGE @"14"
#define GIFT_PACKAGE_PAGE @"15"
#define RECOMMENDED_PACKAGE_PAGE @"16"
#define BEST_PACKAGE_PAGE @"17"
#define BUY_PACKAGE_PAGE @"18"

#define PROMO_DETAIL_PAGE @"20"
#define CATEGORY_PAGE @"21"
#define TRX_HISTORY2_PAGE @"22"
#define SHOPPING_GUIDE_PAGE @"23"
#define TERMS_CONDITIONS_PAGE @"24"
#define BEST_OFFERS_PAGE @"25"
#define GENERAL_INFO_PAGE @"26"
#define AXIS_SHOP_LOCATOR_PAGE @"27"
#define DEVICE_SETTING_PAGE @"28"
#define PUK_PAGE @"29"
#define CONTACT_US_PAGE @"30"
#define BUY_PRODUCT_ADDRESS_PAGE @"31"
#define BUY_PRODUCT_SHIPPING_PAGE @"32"
#define BUY_PRODUCT_SUMMARY_PAGE @"33"
#define BUY_PRODUCT_PAYMENT_PAGE @"34"
#define BUY_PRODUCT_PAYMENT_TOKEN_PAGE @"35"
#define BUY_PRODUCT_PAYMENT_RESULT_PAGE @"36"
#define BUY_VOUCHER_PAYMENT_PAGE @"37"
#define BUY_VOUCHER_PAYMENT_TOKEN_PAGE @"38"
#define BUY_VOUCHER_RESULT_PAGE @"39"

// TODO : Hygiene
#define CAPTCHA_REFRESH_TIME 300.0
#define kNotificationRefreshTable @"REFRESH_TABLE"

// TODO : V1.9
// Codes for App's version update 
#define kMajorNotificationUpdate 220
#define kMinorNotificationUpdate 221

#define NOTIF_BUTTON_TAG 777
#define BADGE_ICON_TAG 776
#define DEVICE_OS @"ios"

#define kIsPackage4G @"isPackage4G"
#define kFeatureMenu @"Feature Menu V2"
#define kNA @"NA"

@protocol Constant <NSObject>

@end
