//
//  AdsRequest.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 5/20/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "AdsRequest.h"
#import "Constant.h"
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

#import "DataManager.h"
#import "EncryptDecrypt.h"
#import "JSON.h"

@implementation AdsRequest

@synthesize adsDelegate;
@synthesize callback;
@synthesize errorCallback;

#pragma mark - Delegate

- (void)parseAdsDone:(NSDictionary*)result {
    //- (void)parseAdsDone:(BOOL)success withReason:(NSString*)reason {
    
    /*
     if ([_delegate respondsToSelector:@selector(requestDoneWithInfo:)]) {
     NSString *resultKey = RESULT_KEY;
     NSNumber *resultValue = [NSNumber numberWithBool:success];
     
     NSString *reasonKey = REASON_KEY;
     NSString *reasonValue = reason;
     
     NSString *requestKey = REQUEST_KEY;
     NSNumber *requestValue = [NSNumber numberWithInt:requestType];
     
     //NSLog(@"requestType = %i",requestType);
     
     NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:resultValue, resultKey, reasonValue, reasonKey, requestValue, requestKey, nil];
     [_delegate requestDoneWithInfo:dict];
     }*/
    
    
    if (adsDelegate && callback) {
		if ([adsDelegate respondsToSelector:self.callback]) {
            
            /*
             NSNumber *reqResult = [result valueForKey:RESULT_KEY];
             BOOL success = [reqResult boolValue];
             
             NSString *resultKey = RESULT_KEY;
             NSNumber *resultValue = [NSNumber numberWithBool:success];
             
             NSString *reason = [result valueForKey:REASON_KEY];
             NSString *reasonKey = REASON_KEY;
             NSString *reasonValue = reason;
             
             //            NSString *requestKey = REQUEST_KEY;
             //            NSNumber *requestValue = [NSNumber numberWithInt:requestType];
             
             NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
             resultValue, resultKey,
             reasonValue, reasonKey,
             requestValue, requestKey, nil];*/
            
			[adsDelegate performSelector:self.callback withObject:result];
		}
		else {
			//NSLog(@"No response from delegate");
		}
	}
}

#pragma mark - API Hot Offer Banner

- (void)hotOfferBanner:(NSString*)size
              delegate:(id)requestDelegate
       successSelector:(SEL)successSelector
         errorSelector:(SEL)errorSelector {
    
    self.adsDelegate = requestDelegate;
	self.callback = successSelector;
    self.errorCallback = errorSelector;
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 size, @"size",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,HOT_OFFER_BANNER]];
    NSLog(@"url HOT_OFFER_BANNER = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestHotOfferBannerDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestHotOfferBannerDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"HOT_OFFER_BANNER = %@",response);
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"status\":\"1\",\"msisdn\":\"lmhGymIpd8Nrr1vveSEqQA==\",\"trxid\":1407218116,\"result\":[{\"href\":\"#buy_package\",\"hrefweb\":\"#\",\"id\":\"6.0001.0006\",\"parent_id\":\"6.0001\",\"group\":null,\"gid\":\"10\",\"name\":\" HotRod3G+ Bulanan 3.6GB+1.5GB\",\"pkgid\":\"XL99N\",\"pkggroup\":\"DATA\",\"volume\":\"5.1GB\",\"duration\":\"30 hari\",\"pkgtype\":\"recur\",\"price\":\"99000\",\"desc\":null,\"confirmDesc\":\" You choose Monthly HotRod 3G+ Package 3.6GB, IDR99k. Enjoy FAST & LOW PRICE Internet with 3.6GB at 00:00-23:59 + BONUS 1.5GB at 00:00-06:00, all quota apply on 3G network for 30 days! More info go to www.xl.co.id/internet or call 817 . To continue, click Yes.\",\"icon\":null,\"hex\":\"10\",\"banner\":\"http://my.xl.co.id/myxlpanel/public/img/hotover/2014071014471.jpg\"}]}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseHotOfferBannerData:response];
    [dataParser release];
}

#pragma mark - API Ads

- (void)ads:(NSString*)pageId
       size:(NSString*)adsSize
   delegate:(id)requestDelegate
successSelector:(SEL)successSelector
errorSelector:(SEL)errorSelector {
    
    self.adsDelegate = requestDelegate;
	self.callback = successSelector;
    self.errorCallback = errorSelector;
    
    NSString *deviceBrand = DEVICE_BRAND;
    UIDevice *device = [UIDevice currentDevice];
    NSString *deviceModel = device.model;
    
    NSString *cid = @"";
    NSString *size = @"";
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        //Device is iPad
        //iPad tyui097f56yt
        cid = CID_ADS_IPAD;
        size = ADS_SIZE_IPAD;
        //NSLog(@"iPad");
    } else{
        //Device is iPhone
        //iPhone 345809koiuy
        cid = CID_ADS_IPHONE;
        size = adsSize;
        //NSLog(@"iPhone");
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,ADS]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    //NSLog(@"cid = %@",cid);
    //NSLog(@"size = %@",size);
    //NSLog(@"deviceBrand = %@",deviceBrand);
    //NSLog(@"deviceModel = %@",deviceModel);
    //NSLog(@"pageId = %@",pageId);
    
    [request addPostValue:cid forKey:@"cid"];
    [request addPostValue:size forKey:@"size"];
    [request addPostValue:deviceBrand forKey:@"brand"];
    [request addPostValue:deviceModel forKey:@"model"];
    [request addPostValue:@"3" forKey:@"n"];
    [request addPostValue:pageId forKey:@"page"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestAdsDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestAdsDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"ADS = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseAdsData:response];
    [dataParser release];
}

#pragma mark - Download Multiple Image

- (void)downloadMultipleImage:(NSArray*)imagesURL
                     delegate:(id)requestDelegate
              successSelector:(SEL)successSelector
                errorSelector:(SEL)errorSelector
{
    @try
    {
        self.adsDelegate = requestDelegate;
        self.callback = successSelector;
        self.errorCallback = errorSelector;
        
        /*
         * Changes by iNot 28 March 2015:
         * mark comment on if([networkQueue respondsToSelector:@selector(imageFetchComplete:)])
         * mark comment on if([networkQueue respondsToSelector:@selector(imageFetchFailed:)])
         * to resolve ads/hot offer banner is not appear on Home Page
         */
        
        ASINetworkQueue *networkQueue = [[ASINetworkQueue alloc] init];
        [networkQueue reset];
        //[networkQueue setDownloadProgressDelegate:progressIndicator];
        //if([networkQueue respondsToSelector:@selector(imageFetchComplete:)])
            [networkQueue setRequestDidFinishSelector:@selector(imageFetchComplete:)];
        //if([networkQueue respondsToSelector:@selector(imageFetchFailed:)])
            [networkQueue setRequestDidFailSelector:@selector(imageFetchFailed:)];
        //[networkQueue setShowAccurateProgress:[accurateProgress isOn]];
        [networkQueue setDelegate:self];
        
        for (int i=0; i < [imagesURL count]; i++) {
            ASIHTTPRequest *request;
            request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:[imagesURL objectAtIndex:i]]];
            [request setUserInfo:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%i",i] forKey:@"tag"]];
            [networkQueue addOperation:request];
        }
        
        [networkQueue go];
    }
    @catch (NSException *exception) {
        
    }
}

- (void)imageFetchComplete:(ASIHTTPRequest *)request {
    
    //NSLog(@"imageFetchComplete");
    
    BOOL success = YES;
    NSString *reason = @"";
    /*
     if ([_delegate respondsToSelector:@selector(requestDoneWithInfo:)]) {
     NSString *resultKey = RESULT_KEY;
     NSNumber *resultValue = [NSNumber numberWithBool:success];
     
     NSString *reasonKey = REASON_KEY;
     NSString *reasonValue = reason;
     
     NSString *requestKey = REQUEST_KEY;
     NSNumber *requestValue = [NSNumber numberWithInt:requestType];
     
     NSString *imageKey = @"imagekey";
     UIImage *imageValue = [UIImage imageWithData:[request responseData]];
     
     NSString *imageTagKey = @"imagetag";
     NSDictionary *userInfo = [request userInfo];
     NSString *imageTagValue = [userInfo objectForKey:@"tag"];
     //NSLog(@"imageTagValue = %@",imageTagValue);
     
     NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
     resultValue, resultKey,
     reasonValue, reasonKey,
     requestValue, requestKey,
     imageValue, imageKey,
     imageTagValue, imageTagKey, nil];
     [_delegate requestDoneWithInfo:dict];
     }*/
    
    if (adsDelegate && callback) {
		if ([adsDelegate respondsToSelector:self.callback]) {
            NSString *resultKey = RESULT_KEY;
            NSNumber *resultValue = [NSNumber numberWithBool:success];
            
            NSString *reasonKey = REASON_KEY;
            NSString *reasonValue = reason;
            
            //NSString *requestKey = REQUEST_KEY;
            //NSNumber *requestValue = [NSNumber numberWithInt:requestType];
            
            NSString *imageKey = @"imagekey";
            UIImage *imageValue = [UIImage imageWithData:[request responseData]];
            
            NSString *imageTagKey = @"imagetag";
            NSDictionary *userInfo = [request userInfo];
            NSString *imageTagValue = [userInfo objectForKey:@"tag"];
            //NSLog(@"imageTagValue = %@",imageTagValue);
            
            NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                  resultValue, resultKey,
                                  reasonValue, reasonKey,
                                  //requestValue, requestKey,
                                  imageValue, imageKey,
                                  imageTagValue, imageTagKey, nil];
            
			[adsDelegate performSelector:self.callback withObject:dict];
		}
		else {
			//NSLog(@"No response from delegate");
		}
	}
}

- (void)imageFetchFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"error = %@",error);
    if (adsDelegate && errorCallback) {
        if ([adsDelegate respondsToSelector:self.errorCallback]) {
            NSNumber *resultValue = [NSNumber numberWithBool:NO];
            NSDictionary *dict = @{RESULT_KEY:resultValue};
            [adsDelegate performSelector:self.errorCallback withObject:dict];
        }
    }
    
    /*
    if ([_delegate respondsToSelector:@selector(requestDoneWithInfo:)]) {
        NSString *resultKey = RESULT_KEY;
        NSNumber *resultValue = [NSNumber numberWithBool:NO];
        
        NSString *reasonKey = REASON_KEY;
        NSString *reasonValue = [NSString stringWithFormat:@"%@",error];
        
        NSString *requestKey = REQUEST_KEY;
        NSNumber *requestValue = [NSNumber numberWithInt:requestType];
        
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:resultValue, resultKey, reasonValue, reasonKey, requestValue, requestKey, nil];
        [_delegate requestDoneWithInfo:dict];
    }*/
}

@end
