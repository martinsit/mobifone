//
//  AdsRequest.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 5/20/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataParser.h"

@interface AdsRequest : NSObject <DataParserDelegate> {
    id adsDelegate;
	SEL callback;
	SEL errorCallback;
}

@property (nonatomic, retain) id adsDelegate;
@property (nonatomic) SEL callback;
@property (nonatomic) SEL errorCallback;

// Ads
- (void)ads:(NSString*)pageId
       size:(NSString*)adsSize
   delegate:(id)requestDelegate
successSelector:(SEL)successSelector
errorSelector:(SEL)errorSelector;

// Download Image
- (void)downloadMultipleImage:(NSArray*)imagesURL
                     delegate:(id)requestDelegate
              successSelector:(SEL)successSelector
                errorSelector:(SEL)errorSelector;

// Hot Offer Banner
- (void)hotOfferBanner:(NSString*)size
              delegate:(id)requestDelegate
       successSelector:(SEL)successSelector
         errorSelector:(SEL)errorSelector;

@end
