//
//  AXISnetRequest.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/24/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "AXISnetRequest.h"

#import "Constant.h"
#import "EncryptDecrypt.h"
#import "JSON.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"
#import "DataManager.h"
#import "AXISnetCommon.h"

#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"

#import "EncryptDecrypt.h"

static AXISnetRequest *sharedInstance = nil;

@implementation AXISnetRequest

@synthesize requestType;

@synthesize adsDelegate;
@synthesize callback;
@synthesize errorCallback;

// Get the shared instance and create it if necessary.
+ (AXISnetRequest*)sharedInstance {
	if (sharedInstance == nil) {
		sharedInstance = [[super allocWithZone:NULL] init];
	}
	return sharedInstance;
}

// We don't want to allocate a new instance, so return the current one.
+ (id)allocWithZone:(NSZone*)zone {
	return [[self sharedInstance] retain];
}

// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone {
	return self;
}

// Once again - do nothing, as we don't have a retain counter for this object.
- (id)retain {
	return self;
}

// Replace the retain counter so we can never release this object.
- (NSUInteger)retainCount {
	return NSUIntegerMax;
}

// This function is empty, as we don't want to let the user release this object.
- (oneway void)release {
	
}

//Do nothing, other than return the shared instance - as this is expected from autorelease.
- (id)autorelease {
	return self;
}

- (void)dealloc {
	// Should never be called, but just here for clarity really.
	[super dealloc];
}

- (void)repeatLastRequest {
    if (requestType == PROFILE_REQ) {
        
    }
    else if (requestType == PROFILE_REQ) {
        
    }
}

#pragma mark - Delegate

- (void)parseDataDone:(BOOL)success withReason:(NSString*)reason withErrorCode:(NSString*)errorCode {
    
    if (requestCounter < 1) {
        if ([errorCode isEqualToString:@"203"] ||
            [errorCode isEqualToString:@"204"]) {
            
            //[errorCode isEqualToString:@"208"]
            
            needRequestAgain = YES;
            requestCounter++;
            [self profile];
        }
        else {
            if ([_delegate respondsToSelector:@selector(requestDoneWithInfo:)]) {
                NSString *resultKey = RESULT_KEY;
                NSNumber *resultValue = [NSNumber numberWithBool:success];
                
                NSString *reasonKey = REASON_KEY;
                NSString *reasonValue = reason;
                
                NSString *requestKey = REQUEST_KEY;
                NSNumber *requestValue = [NSNumber numberWithInt:requestType];
                
                NSString *repeatKey = REPEAT_KEY;
                NSNumber *repeatValue = [NSNumber numberWithBool:needRequestAgain];
                
                // TODO : Hygiene
                NSString *errorCodeKey = ERROR_CODE_KEY;
                NSString *errorCodeValue = errorCode;
                
                NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:resultValue, resultKey, reasonValue, reasonKey, requestValue, requestKey, repeatValue, repeatKey, errorCodeValue, errorCodeKey, nil];
                [_delegate requestDoneWithInfo:dict];
                
                needRequestAgain = NO;
            }
        }
    }
    else {
        requestCounter = 0;
        if ([_delegate respondsToSelector:@selector(requestDoneWithInfo:)]) {
            NSString *resultKey = RESULT_KEY;
            NSNumber *resultValue = [NSNumber numberWithBool:success];
            
            NSString *reasonKey = REASON_KEY;
            NSString *reasonValue = reason;
            
            NSString *requestKey = REQUEST_KEY;
            NSNumber *requestValue = [NSNumber numberWithInt:requestType];
            
            NSString *repeatKey = REPEAT_KEY;
            NSNumber *repeatValue = [NSNumber numberWithBool:needRequestAgain];
            
            // TODO : Hygiene
            NSString *errorCodeKey = ERROR_CODE_KEY;
            NSString *errorCodeValue = errorCode;
            
            NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:resultValue, resultKey, reasonValue, reasonKey, requestValue, requestKey, repeatValue, repeatKey, errorCodeValue, errorCodeKey, nil];
            [_delegate requestDoneWithInfo:dict];
            
            needRequestAgain = NO;
        }
    }
}

- (void)parseDataDone:(BOOL)success withReason:(NSString*)reason {
    
    if ([_delegate respondsToSelector:@selector(requestDoneWithInfo:)]) {
        NSString *resultKey = RESULT_KEY;
        NSNumber *resultValue = [NSNumber numberWithBool:success];
        
        NSString *reasonKey = REASON_KEY;
        NSString *reasonValue = reason;
        
        NSString *requestKey = REQUEST_KEY;
        NSNumber *requestValue = [NSNumber numberWithInt:requestType];
        
        //NSLog(@"requestType = %i",requestType);
        
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:resultValue, resultKey, reasonValue, reasonKey, requestValue, requestKey, nil];
        [_delegate requestDoneWithInfo:dict];
    }
}

- (void)parseAdsDone:(NSDictionary*)result {
    
    if (adsDelegate && callback) {
        if ([adsDelegate respondsToSelector:self.callback]) {
            [adsDelegate performSelector:self.callback withObject:result];
        }
        else {
            //NSLog(@"No response from delegate");
        }
    }
}

- (void)parseMatrixDone:(NSDictionary*)result {
    
    if (adsDelegate && callback) {
        if ([adsDelegate respondsToSelector:self.callback]) {
            [adsDelegate performSelector:self.callback withObject:result];
        }
        else {
            //NSLog(@"No response from delegate");
        }
    }
}

// TODO : Update Hygiene Defect
- (void)parseRegisterDeviceTokenDone:(NSDictionary*)result {
    
    if (_registerTokenDelegate && _registerTokenCallback) {
        if ([_registerTokenDelegate respondsToSelector:_registerTokenCallback]) {
            [_registerTokenDelegate performSelector:_registerTokenCallback withObject:result];
        }
        else {
            //NSLog(@"No response from delegate");
        }
    }
}

// TODO : V1.9
-(void)parseGetUnreadNotificationDone:(NSDictionary *)result
{
    if (adsDelegate && callback) {
        if ([adsDelegate respondsToSelector:self.callback]) {
            [adsDelegate performSelector:self.callback withObject:result];
        }
        else {
            NSLog(@"No response from delegate");
        }
    }
}

- (void)requestWentWrong:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"error = %@",error);
    
    if ([_delegate respondsToSelector:@selector(requestDoneWithInfo:)]) {
        NSString *resultKey = RESULT_KEY;
        NSNumber *resultValue = [NSNumber numberWithBool:NO];
        
        NSString *reasonKey = REASON_KEY;
        NSString *reasonValue = [Language get:@"general_error_message" alter:nil];
        
        NSString *requestKey = REQUEST_KEY;
        NSNumber *requestValue = [NSNumber numberWithInt:requestType];
        
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:resultValue, resultKey, reasonValue, reasonKey, requestValue, requestKey, nil];
        [_delegate requestDoneWithInfo:dict];
    }
}

#pragma mark - SSO Get Profile
// TODO : HYGIENE
// Profile with device token
- (void)profileWithDeviceToken:(NSString *)deviceToken
{
    // TODO : V1.9.5
    NSString *token = getDataFromPreferenceWithKey(PROFILE_TOKEN_CACHE_KEY);
    NSString *keepSignin = getDataFromPreferenceWithKey(PROFILE_FLAG_KMSI_CACHE_KEY);
    //NSString *keepSignin = @"0";
    
    NSString *deviceBrand = DEVICE_BRAND;
    UIDevice *dev = [UIDevice currentDevice];
    NSString *deviceModel = dev.model;
    NSString *secretKey = SECRET_KEY_SSO;
    
    NSArray *logoSize = [NSArray arrayWithObjects:@"68x44", @"135x88",nil];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 deviceBrand, @"brand",
                                 deviceModel, @"model",
                                 logoSize, @"size",
                                 deviceToken, @"register_token",
                                 // TODO : V1.9.5
                                 token, @"token",
                                 keepSignin, @"keep_signin",
                                 nil];
    
    NSLog(@"profileWithDeviceToken = %@",requestData);
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsondata = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,GET_PROFILE]];
    //    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",@"http://apidev.my.xl.co.id",SSO,GET_PROFILE]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    //NSLog(@"URL GET PROFILE = %@",url);
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestProfileDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)profile {
    
    // TODO : V1.9.5
    NSString *token = getDataFromPreferenceWithKey(PROFILE_TOKEN_CACHE_KEY);
    NSString *keepSignin = getDataFromPreferenceWithKey(PROFILE_FLAG_KMSI_CACHE_KEY);
    //NSString *keepSignin = @"0";
    NSLog(@"Saved Token = %@",token);
    
    NSString *deviceBrand = DEVICE_BRAND;
    UIDevice *dev = [UIDevice currentDevice];
    NSString *deviceModel = dev.model;
    NSString *secretKey = SECRET_KEY_SSO;
    
    NSArray *logoSize = [NSArray arrayWithObjects:@"68x44", @"135x88",nil];
    
    // TODO : V1.9
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 deviceBrand, @"brand",
                                 deviceModel, @"model",
                                 logoSize, @"size",
                                 appVersion,@"version",
                                 DEVICE_OS,@"device_os",
                                 // TODO : V1.9.5
                                 token, @"token",
                                 keepSignin, @"keep_signin",
                                 nil];
    /*
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 @"Nokia", @"brand",
                                 @"unknown", @"model",
                                 @"68x44,135x88", @"size",
                                 nil];*/
    
    NSLog(@"profile = %@",requestData);
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsondata = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,GET_PROFILE]];
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",@"http://apidev.my.xl.co.id",SSO,GET_PROFILE]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    NSLog(@"URL GET PROFILE = %@",url);
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestProfileDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestProfileDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"GET_PROFILE RESPONSE = %@",response);
    
    //response = @"{\"code\":\"208\",\"message\":\"Please sign-in first\",\"data\":{\"version\":\"2.0.1\"}}";
    
    /*
    response = @"{\"code\":\"200\",\"message\":\"Success\",\"data\":{\"id\":\"1546731\",\"name\":\"iNot\",\"full_name\":\"Tony Hadisiswanto\",\"msisdn\":\"dK85TUB4pHNdWXS8-5BZpw==\",\"email\":\"funk_ynot_24@yahoo.com\",\"language\":\"id\",\"location\":\"Jakarta\",\"regional\":\"Jakarta\",\"status\":\"1\",\"channel\":\"IPHONE\",\"group_id\":\"2\",\"application_name\":\"iphone_7\",\"address\":\"JL. Sumbangsih VI\",\"address2\":\"Gang Z no.25\",\"address3\":\"Karet Setiabudi\",\"city\":\"Jakarta Selatan\",\"postcode\":\"24545\",\"confirm_email\":\"0\",\"email_notify\":\"0\",\"push_notify\":\"0\",\"gender\":\"M\",\"birth\":\"09-10-2013\",\"token\":\"3232261135_ec973150303-0\"}}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseGetProfileData:response];
    [dataParser release];
}

#pragma mark - SSO Sign In

- (void)signInWithUsername:(NSString*)username
              withPassword:(NSString*)password
              withLanguage:(NSString*)language
           withDeviceToken:(NSString *)deviceToken
{
    
    // TODO : V1.9.5
    //NSDictionary *profileDict = getDataFromPreferenceWithKey(KEEP_SIGNIN_CACHE_KEY);
    NSString *token = getDataFromPreferenceWithKey(PROFILE_TOKEN_CACHE_KEY);
    NSString *keepSignin = getDataFromPreferenceWithKey(PROFILE_FLAG_KMSI_CACHE_KEY);
    
    NSString *deviceBrand = DEVICE_BRAND;
    UIDevice *device = [UIDevice currentDevice];
    NSString *deviceModel = device.model;
    NSString *secretKey = SECRET_KEY_SSO;
    NSString *sharedKey = SHARED_KEY_SSO;
    
    //NSString *usernameEncrypted = [EncryptDecrypt doCipher:username action:kCCEncrypt withKey:sharedKey];
    NSString *usernameEncrypted = [EncryptDecrypt doCipherForiOS7:username action:kCCEncrypt withKey:sharedKey];
    
    //NSString *passwordEncrypted = [EncryptDecrypt doCipher:password action:kCCEncrypt withKey:sharedKey];
    NSString *passwordEncrypted = [EncryptDecrypt doCipherForiOS7:password action:kCCEncrypt withKey:sharedKey];
    
    NSArray *logoSize = [NSArray arrayWithObjects:@"68x44", @"135x88",nil];
    // TODO : V1.9
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    // TODO ; HYGIENE
    // Add device token param to sign in request
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 usernameEncrypted, @"msisdn",
                                 passwordEncrypted, @"password",
                                 deviceBrand, @"brand",
                                 deviceModel, @"model",
                                 language,@"lang",
                                 logoSize, @"size",
                                 deviceToken, @"register_token",
                                 // TODO : V1.9
                                 appVersion, @"version",
                                 DEVICE_OS, @"device_os",
                                 // TODO : V1.9.5
                                 token, @"token",
                                 keepSignin, @"keep_signin",
                                 nil];
    
    NSLog(@"requestData = %@",requestData);
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString SIGN IN = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,SIGN_IN]];
    NSLog(@"SIGN IN = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestSignInDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestSignInDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"LOGIN RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseGetProfileData:response];
    [dataParser release];
}

#pragma mark - SSO Sign Out

- (void)signOut {
    // TODO : V1.9.5
    //NSString *token = getDataFromPreferenceWithKey(PROFILE_TOKEN_CACHE_KEY);
    //NSString *keepSignin = getDataFromPreferenceWithKey(PROFILE_FLAG_KMSI_CACHE_KEY);
    
    NSString *secretKey = SECRET_KEY_SSO;
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 sharedData.profileData.token, @"token",
                                 // TODO : V1.9.5
                                 //token, @"token",
                                 //keepSignin, @"keep_signin",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString SIGN OUT = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,SIGN_OUT]];
    //NSLog(@"SIGN OUT = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestSignOutDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestSignOutDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"LOGOUT RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseSignOutData:response];
    [dataParser release];
}

#pragma mark - SSO Register

- (void)registerWithUsername:(NSString*)username
                withPassword:(NSString*)password
                  withMsisdn:(NSString*)msisdn
                 withCaptcha:(NSString*)captcha
                     withCid:(NSString*)cid
                withFullname:(NSString*)fullname
                   withEmail:(NSString*)email
                withLanguage:(NSString*)language
             withCurrentLang:(NSString*)currentLang {
    
    NSString *secretKey = SECRET_KEY_SSO;
    NSString *sharedKey = SHARED_KEY_SSO;
    
    NSString *usernameEncrypted = [EncryptDecrypt doCipherForiOS7:username action:kCCEncrypt withKey:sharedKey];
    
    NSString *passwordEncrypted = [EncryptDecrypt doCipherForiOS7:password action:kCCEncrypt withKey:sharedKey];
    
    NSString *msisdnEncrypted = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCEncrypt withKey:sharedKey];
    
    NSString *fullNameEncrypted = [EncryptDecrypt doCipherForiOS7:fullname action:kCCEncrypt withKey:sharedKey];
    
    NSString *emailEncrypted = [EncryptDecrypt doCipherForiOS7:email action:kCCEncrypt withKey:sharedKey];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 usernameEncrypted, @"nickname",
                                 passwordEncrypted, @"password",
                                 msisdnEncrypted, @"msisdn",
                                 fullNameEncrypted, @"fullname",
                                 emailEncrypted, @"email",
                                 language,@"language",
                                 captcha,@"captcha",
                                 cid,@"cid",
                                 currentLang, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,REGISTER]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestRegisterDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestRegisterDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"REGISTER RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseRegisterData:response];
    [dataParser release];
}

#pragma mark - SSO Activate Account

- (void)activateMsisdn:(NSString*)msisdn
               withPin:(NSString*)pin
          withLanguage:(NSString*)language {
    NSString *secretKey = SECRET_KEY_SSO;
    NSString *sharedKey = SHARED_KEY_SSO;
    
    NSString *msisdnEncrypted = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCEncrypt withKey:sharedKey];
    
    NSString *pinEncrypted = [EncryptDecrypt doCipherForiOS7:pin action:kCCEncrypt withKey:sharedKey];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 msisdnEncrypted, @"msisdn",
                                 pinEncrypted, @"pin",
                                 language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,ACTIVATE_ACCOUNT]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestActivationDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestActivationDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"ACTIVATION RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseActivationData:response];
    [dataParser release];
}

#pragma mark - SSO Resend PIN

- (void)resendPin:(NSString*)msisdn
      withCaptcha:(NSString*)captcha
          withCid:(NSString*)cid
     withLanguage:(NSString*)language {
    
    NSString *secretKey = SECRET_KEY_SSO;
    NSString *sharedKey = SHARED_KEY_SSO;
    
    NSString *msisdnEncrypted = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCEncrypt withKey:sharedKey];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 msisdnEncrypted, @"msisdn",
                                 cid, @"cid",
                                 captcha, @"captcha",
                                 language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,RESEND_PIN]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestResendPinDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestResendPinDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"RESEND_PIN = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseResendPinData:response];
    [dataParser release];
}

#pragma mark - SSO Reset Password

- (void)resetPassword:(NSString*)msisdn
          withCaptcha:(NSString*)captcha
              withCid:(NSString*)cid
            withToken:(NSString*)token
         withLanguage:(NSString*)language {
    
    //NSLog(@"msisdn %@", msisdn);
    NSString *secretKey = SECRET_KEY_SSO;
    NSString *sharedKey = SHARED_KEY_SSO;
    
    NSString *msisdnEncrypted = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCEncrypt withKey:sharedKey];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 msisdnEncrypted, @"msisdn",
                                 cid, @"cid",
                                 captcha, @"captcha",
                                 language, @"lang",
                                 token, @"token",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,RESET_PASSWORD]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestResetPasswordDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestResetPasswordDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"RESET PASSWORD = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseResetPasswordData:response];
    [dataParser release];
}

#pragma mark - SSO Change Password

- (void)changePassword:(NSString*)password
       withNewPassword:(NSString*)newPassword
 withNewPasswordRetype:(NSString*)newPasswordRetype
          withLanguage:(NSString*)language {
    
    NSString *secretKey = SECRET_KEY_SSO;
    NSString *sharedKey = SHARED_KEY_SSO;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *passwordEncrypted = [EncryptDecrypt doCipherForiOS7:password action:kCCEncrypt withKey:sharedKey];
    NSString *newPasswordEncrypted = [EncryptDecrypt doCipherForiOS7:newPassword action:kCCEncrypt withKey:sharedKey];
    NSString *newPasswordRetypeEncrypted = [EncryptDecrypt doCipherForiOS7:newPasswordRetype action:kCCEncrypt withKey:sharedKey];
    
    
    NSString *msisdn = sharedData.profileData.msisdn;
    NSString *saltKeyAPI = SALT_KEY;
    msisdn = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCDecrypt withKey:saltKeyAPI];
    msisdn = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCEncrypt withKey:sharedKey];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 msisdn, @"msisdn",
                                 secretKey, @"secret_key",
                                 sharedData.profileData.token, @"token",
                                 passwordEncrypted, @"password",
                                 newPasswordEncrypted, @"newpassword",
                                 newPasswordRetypeEncrypted, @"repassword",
                                 language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"JSON = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,CHANGE_PASSWORD]];
    NSLog(@"url = %@",url);
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestChangePasswordDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestChangePasswordDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"CHANGE PASSWORD = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseChangePasswordData:response];
    [dataParser release];
}

#pragma mark - SSO Register Device Token

// TODO : Update Hygiene
//- (void)registerDeviceToken:(NSString*)devOS
//               withDevToken:(NSString*)devToken {
- (void)registerDeviceToken:(NSString*)devToken
               withDelegate:(id)delegate
                andCallBack:(SEL)successCallback
{
    _registerTokenDelegate = delegate;
    _registerTokenCallback = successCallback;
    
    NSString *secretKey = SECRET_KEY_SSO;
    NSString *sharedKey = SHARED_KEY_SSO;
    
    NSString *saltKeyAPI = SALT_KEY;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *msisdnDecrypted = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    NSString *msisdnEncrypted = [EncryptDecrypt doCipherForiOS7:msisdnDecrypted action:kCCEncrypt withKey:sharedKey];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 sharedData.profileData.token, @"token",
                                 msisdnEncrypted, @"msisdn",
                                 devToken,@"device_token",
                                 sharedData.profileData.language,@"lang",
                                 DEVICE_OS, @"device_os",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"REGISTER TOKEN %@", jsonString);
    // TODO : Hygiene
    // Defect fixing for registering device token
    //    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,REG_DEV_TOKEN]];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,UPDATE_DEVICE_TOKEN]];
    NSLog(@"device token URL %@", url);
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(registerDeviceTokenDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)registerDeviceTokenDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"REG_DEV_TOKEN = %@",response);
    if(_registerTokenCallback && _registerTokenDelegate)
    {
        DataParser *dataParser = [[DataParser alloc] init];
        dataParser.delegate = self;
        [dataParser parseRegisterDeviceTokenData:response];
        [dataParser release];
    }
}


#pragma mark - API Get Menu

- (void)menu {
    // TODO : V1.9.5
    NSString *token = getDataFromPreferenceWithKey(PROFILE_TOKEN_CACHE_KEY);
    NSString *keepSignin = getDataFromPreferenceWithKey(PROFILE_FLAG_KMSI_CACHE_KEY);
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"data = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,GET_MENU]];
    NSLog(@"URL GET MENU= %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    //[request addPostValue:sharedData.profileData.token forKey:@"token"];
    // TODO : V1.9.5
    [request addPostValue:token forKey:@"token"];
    NSLog(@"token = %@",token);
    [request addPostValue:keepSignin forKey:@"keep_signin"];
    NSLog(@"keep_signin = %@",keepSignin);
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestMenuDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestMenuDone:(ASIHTTPRequest *)request {
    NSString *response = [request responseString];
    NSLog(@"GET_MENU RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseGetMenuData:response];
    [dataParser release];
}

#pragma mark - API Update Profile

- (void)updateProfile:(NSString*)nickname
         withFullname:(NSString*)fullname
            withEmail:(NSString*)email
         withLanguage:(NSString*)language
           withGender:(NSString*)gender
         withBirthday:(NSString*)birthday
       withEmailNotif:(NSString*)emailNotif
        withPushNotif:(NSString*)pushNotif
         withAddress1:(NSString*)address1
         withAddress2:(NSString*)address2
         withAddress3:(NSString*)address3
             withCity:(NSString*)city
          withZipcode:(NSString*)zipcode
        withAutologin:(NSString*)autologin
       withIdCardType:(NSString*)idCardType
     withIdCardNumber:(NSString*)idCardNumber
     withPlaceOfBirth:(NSString*)placeOfBirth
      withOtherNumber:(NSString*)otherNumber
           withStatus:(NSString*)status {
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 nickname, @"username_subscriber",
                                 fullname, @"fullname",
                                 email, @"email",
                                 language, @"lang",
                                 gender, @"gender",
                                 birthday, @"birth",
                                 emailNotif, @"email_notify",
                                 pushNotif, @"push_notify",
                                 address1, @"address",
                                 address2, @"address2",
                                 address3, @"address3",
                                 city, @"city",
                                 zipcode, @"postcode",
                                 autologin, @"autologin",
                                 idCardType, @"idcard_type",
                                 idCardNumber, @"idcard_number",
                                 placeOfBirth, @"place_birth",
                                 otherNumber, @"other_phone",
                                 status, @"status",
                                 nil];
    
    //NSLog(@"requestData = %@",requestData);
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString Update Profile = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,UPDATE_PROFILE]];
    //NSLog(@"url = %@",url);
    
    //NSLog(@"sharedData.profileData.token = %@",sharedData.profileData.token);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestUpdateProfileDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestUpdateProfileDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"UPDATE PROFILE RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseUpdateProfileData:response];
    [dataParser release];
}

#pragma mark - API Check Balance

- (void)checkBalance {
    /*
    for (ASIHTTPRequest *req in ASIHTTPRequest.sharedQueue.operations)
    {
        [req cancel];
        [req setDelegate:nil];
    }*/
    //[queue setDelegate:nil];
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *txId = generateUniqueString();
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 txId, @"txid",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,CHECK_BALANCE]];
    //NSLog(@"url BALANCE = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    //NSLog(@"sharedData.profileData.token = %@",sharedData.profileData.token);
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestCheckBalanceDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestCheckBalanceDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"CHECK BALANCE = %@",response);
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"1323174874174\",\"msisdn\":\"JpCYsSi4ab3QVAep2McfuA==\",\"status\":\"1\",\"result\":{\"accounttype\":\"Prepaid\",\"accountstate\":\"Active\",\"accountstatus\":\"1\",\"activestopdate\":\"20120916\",\"suspendstopdate\":\"20121016\",\"disablestopdate\":\"20121016\",\"balance\":\"20237\",\"bonus\":[{\"accid\":\"\",\"accval\":\"180\",\"accvaltype\":\"point\",\"accexptime\":\"10-Okt-2012 00:00\",\"accbaldesc\":\"PRM_OnNet_SMS_Subaccount\",\"desc\":\"Bonus SMS sesama operator\",\"bonusval\":\"180 point\"},{\"accid\":\"\",\"accval\":\"20000\",\"accvaltype\":\"Rp\",\"accexptime\":\"15-Sep-2012 00:00\",\"accbaldesc\":\"PresentBalance_Subaccount\",\"desc\":\"Free balance\",\"bonusval\":\"Rp. 20.000\"},{\"accid\":\"\",\"accval\":\"121\",\"accvaltype\":\"second\",\"accexptime\":\"31-Des-2036 23:00\",\"accbaldesc\":\"FreeTime_OnNet_Call_Subaccount\",\"desc\":\"Free On-Net Call\",\"bonusval\":\"2 Menit dan 1 Detik\"},{\"accid\":\"\",\"accval\":\"9427\",\"accvaltype\":\"point\",\"accexptime\":\"31-Des-2036 23:00\",\"accbaldesc\":\"PRM_OnNet_SMS_AXIS1\",\"desc\":\"PRM_OnNet_SMS_AXIS1\",\"bonusval\":\"9.427 point\"}]}}";*/
    
    //response = @"{\"status\":\"-1\",\"err_code\":\"204\",\"err_desc\":\"SSO Token missing\"}";
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseCheckBalanceData:response];
    [dataParser release];
}

#pragma mark - API Top Up Voucher

- (void)topUpVoucher:(NSString*)voucherPin
            msisdnTo:(NSString*)msisdnTo
         withCaptcha:(NSString*)captcha
             withCid:(NSString*)cid {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *txId = generateUniqueString();
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 txId, @"txid",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 voucherPin, @"voucher_pin",
                                 msisdnTo, @"msisdn_to",
                                 cid, @"cid",
                                 captcha, @"captcha",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,TOPUP_VOUCHER]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestTopUpVoucherDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestTopUpVoucherDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"TOPUP VOUCHER 1 = %@",response);
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"5380817280\",\"msisdn\":\"sGYnYULS1hfy1VTPAnE_Vg==\",\"status\":\"1\",\"result\":{\"msisdnto\":\"sGYnYULS1hfy1VTPAnE_Vg==\",\"newbalance\":\"5820\",\"newactivestop\":\"20130615\",\"vouchervalue\":\"5000\",\"addvalidity\":\"7\"}}";
    NSLog(@"TOPUP VOUCHER 2 = %@",response);*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseTopUpVoucherData:response];
    [dataParser release];
}

#pragma mark - API Check Usage XL

- (void)checkInternetUsageXL {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *txId = generateUniqueString();
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 txId, @"txid",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,CHECK_USAGE_XL]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestCheckUsageXLDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestCheckUsageXLDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"CHECK USAGE XL = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseCheckUsageXLData:response];
    [dataParser release];
}

#pragma mark - API Check Internet Usage

- (void)checkInternetUsage {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *txId = generateUniqueString();
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 txId, @"txid",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,CHECK_USAGE]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestCheckUsageDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestCheckUsageDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"CHECK USAGE = %@",response);
    
    /*
    // PPU
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"3223957500\",\"msisdn\":\"wId6ggUuW2Ysn1UHKOQfJg==\",\"status\":\"1\",\"result\":[{\"packageid\":\"P_REGULR\",\"packagecode\":\"\",\"packagename\":\"Regular Plan\",\"startdate\":\"22-Nov-2012 15:21\",\"enddate\":\"14-Jul-2037 15:21\",\"package_type\":\"0\",\"usage\":\"390.00 KB\",\"quota\":\"\",\"balance\":\"399360\",\"usage_percent\":\"\",\"info\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.\",\"unregcmd\":\"\",\"unsubcmd\":\"\",\"flagupgrade\":\"\",\"s0\":\"\",\"t1\":\"\",\"s1\":\"\",\"t2\":\"\",\"s2\":\"\",\"t3\":\"\",\"s3\":\"\",\"t4\":\"\",\"s4\":\"\",\"t5\":\"\",\"s5\":\"\",\"remaining\":{\"percentage\":1,\"number\":8882,\"type\":\"days\",\"label_remain\":\"8882 hari tersisa\"}}]}";*/
    
    // Quota
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"3129219090\",\"msisdn\":\"wId6ggUuW2Ysn1UHKOQfJg==\",\"status\":\"1\",\"result\":[{\"packageid\":\"P_DAY1_H\",\"packagecode\":\"\",\"packagename\":\"Harian 15MB\",\"startdate\":\"11-Apr-2013 18:31\",\"enddate\":\"12-Apr-2013 18:31\",\"package_type\":\"1\",\"usage\":\"2.43 MB\",\"quota\":\"15.00 MB\",\"balance\":\"12871680\",\"usage_percent\":\"85%\",\"info\":\"\",\"unregcmd\":\"UNREG GIFTHARIANH\",\"unsubcmd\":\"\",\"flagupgrade\":\"\",\"s0\":\"\",\"t1\":\"\",\"s1\":\"\",\"t2\":\"\",\"s2\":\"\",\"t3\":\"\",\"s3\":\"\",\"t4\":\"\",\"s4\":\"\",\"t5\":\"\",\"s5\":\"\",\"remaining\":{\"percentage\":25,\"number\":23,\"type\":\"hours\",\"label_remain\":\"23 jam tersisa\"}},{\"packageid\":\"P_U_M1_H\",\"packagecode\":\"\",\"packagename\":\"Unlimited Bulanan Basic\",\"startdate\":\"21-Mar-2013 17:54\",\"enddate\":\"20-Apr-2013 17:54\",\"package_type\":\"3\",\"usage\":\"2057881600\",\"quota\":\"12288000000\",\"balance\":\"612342118400\",\"usage_percent\":\"16%\",\"info\":\"\",\"unregcmd\":\"UNREG GIFTPROBASIC\",\"unsubcmd\":\"\",\"flagupgrade\":\"\",\"s0\":\"\",\"t1\":\"1536000000\",\"s1\":\"\",\"t2\":\"\",\"s2\":\"\",\"t3\":\"\",\"s3\":\"\",\"t4\":\"\",\"s4\":\"\",\"t5\":\"\",\"s5\":\"\"}]}";*/
    
    /*
    // Unlimited
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"3223957500\",\"msisdn\":\"wId6ggUuW2Ysn1UHKOQfJg==\",\"status\":\"1\",\"result\":[{\"packageid\":\"P_U_W_H\",\"packagename\":\"Weekly Unlimited\",\"startdate\":\"13-Jan-2012 13:34\",\"enddate\":\"20-Jan-2012 13:34\",\"package_type\":\"2\",\"usage\":\"0\",\"quota\":\"Unlimited\",\"balance\":\"207503360\",\"usage_percent\":\"59%\",\"info\":\"\",\"unregcmd\":\"\",\"unsubcmd\":\"\",\"flagupgrade\":\"\",\"s0\":\"\",\"t1\":\"\",\"s1\":\"\",\"t2\":\"\",\"s2\":\"\",\"t3\":\"\",\"s3\":\"\",\"t4\":\"\",\"s4\":\"\",\"t5\":\"\",\"s5\":\"\",\"remaining\":{\"percentage\":1,\"number\":8882,\"type\":\"days\",\"label_remain\":\"8882 hari tersisa\"}}]}";*/
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"5852024310\",\"msisdn\":\"JpCYsSi4ab3QVAep2McfuA==\",\"status\":\"1\",\"result\":[{\"packageid\":\"P_U_M1_H\",\"packagecode\":\"\",\"packagename\":\"Unlimited Bulanan Basic\",\"startdate\":\"10-Apr-2013 12:42\",\"enddate\":\"10-Mei-2013 12:42\",\"package_type\":\"3\",\"usage\":\"10240\",\"quota\":\"12288000000\",\"balance\":\"614399989760\",\"usage_percent\":\"\",\"info\":\"\",\"unregcmd\":\"UNREG SMARTHIGH\",\"unsubcmd\":\"\",\"flagupgrade\":\"SMARTHIGH\",\"s0\":\"\",\"t1\":\"1536000000\",\"s1\":\"\",\"t2\":\"\",\"s2\":\"\",\"t3\":\"\",\"s3\":\"\",\"t4\":\"\",\"s4\":\"\",\"t5\":\"\",\"s5\":\"\",\"remaining\":{\"percentage\":6,\"number\":28,\"type\":\"days\",\"label_remain\":\"28 hari tersisa\"}},{\"packageid\":\"P_U_M1_H\",\"packagecode\":\"\",\"packagename\":\"Unlimited Bulanan Basic\",\"startdate\":\"11-Apr-2013 15:38\",\"enddate\":\"11-Mei-2013 15:38\",\"package_type\":\"3\",\"usage\":\"0\",\"quota\":\"6144000000\",\"balance\":\"614400000000\",\"usage_percent\":\"\",\"info\":\"\",\"unregcmd\":\"UNREG PROBULAN500\",\"unsubcmd\":\"\",\"flagupgrade\":\"PROBULAN500\",\"s0\":\"\",\"t1\":\"1536000000\",\"s1\":\"\",\"t2\":\"\",\"s2\":\"\",\"t3\":\"\",\"s3\":\"\",\"t4\":\"\",\"s4\":\"\",\"t5\":\"\",\"s5\":\"\"},{\"packageid\":\"P_U_M2_H\",\"packagecode\":\"\",\"packagename\":\"Unlimited Bulanan Premium\",\"startdate\":\"09-Apr-2013 21:03\",\"enddate\":\"09-Mei-2013 21:03\",\"package_type\":\"3\",\"usage\":\"0\",\"quota\":\"6144000000\",\"balance\":\"614400000000\",\"usage_percent\":\"\",\"info\":\"\",\"unregcmd\":\"UNREG PROBULAN1GB\",\"unsubcmd\":\"\",\"flagupgrade\":\"PROBULAN1GB\",\"s0\":\"\",\"t1\":\"3072000000\",\"s1\":\"\",\"t2\":\"\",\"s2\":\"\",\"t3\":\"\",\"s3\":\"\",\"t4\":\"\",\"s4\":\"\",\"t5\":\"\",\"s5\":\"\"}]}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseCheckUsageData:response];
    [dataParser release];
}

#pragma mark - API Recommended Package

- (void)recommendedPackage {
    NSString *deviceBrand = DEVICE_BRAND;
    UIDevice *device = [UIDevice currentDevice];
    NSString *deviceModel = device.model;
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 deviceBrand,@"brand",
                                 deviceModel,@"model",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,RECOMMENDED_PACKAGE]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestRecommendedPackageDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestRecommendedPackageDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"RECOMMENDED_PACKAGE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseRecommendedPackageData:response];
    [dataParser release];
}

#pragma mark - API Promo

- (void)promo
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"data = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,PROMO]];
    //NSLog(@"URL PROMO = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestPromoDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestPromoDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"PROMO RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parsePromoData:response];
    [dataParser release];
}

#pragma mark - API Notification

- (void)notification {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,NOTIFICATION]];
    NSLog(@"NOTIFICATION url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestNotificationDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestNotificationDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"NOTIFICATION = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseNotificationData:response isDetailNotif:NO];
    [dataParser release];
}

// TODO : V1.9
-(void)getUnreadMessageNotificationWithDelegate:(id)delegate
                       andCallBack:(SEL)successCallback
{
    adsDelegate = delegate;
    callback = successCallback;
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,NOTIFICATION]];
    //NSLog(@"NOTIFICATION url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = UNREAD_NOTIFICATION_REQ;
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(getUnreadNotificationDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)getUnreadNotificationDone:(ASIHTTPRequest *)request
{
    if(adsDelegate && callback)
    {
        NSString *response = [request responseString];
        DataParser *dataParser = [[DataParser alloc] init];
        dataParser.delegate = self;
        [dataParser parseGetUnreadNotificationData:response];
        [dataParser release];
    }
}

#pragma mark - API Detail Notification

- (void)notificationDetail:(NSString*)mailboxId {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 mailboxId, @"mailboxid",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,NOTIFICATION_DETAIL]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestNotificationDetailDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestNotificationDetailDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"--> NOTIFICATION DETAIL = %@",response);
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"2227589152\",\"msisdn\":\"wlgEmFw-nVSLvcLdDo_spQ==\",\"status\":\"1\",\"result\":{\"mailboxID\":\"6138683\",\"briefMessage\":\"Registration Failure\",\"timestamp\":\"2013-12-17T20:54:04.954+07:00\",\"readFlag\":\"READ\",\"notificationType\":\"REGISTRATION\",\"notificationCondition\":\"FAILED\",\"contactSource\":\"svcBilInternetServicesV2.0\",\"serviceID\":\"XL2\",\"lastReasonCode\":{},\"briefTitle\":\"XL AXIATA\",\"message\":\"Maaf, permintaan Anda utk mengaktifkan Paket Internet HotRod3G+ 15MB, 1hr, 2rb belum dapat diproses. Silakan coba beberapa saat lagi. Info: 817\"}}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseNotificationData:response isDetailNotif:YES];
    [dataParser release];
}

#pragma mark - API Delete Notification

- (void)notificationDelete:(NSString*)mailboxId {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 mailboxId, @"mailboxid",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,NOTIFICATION_DELETE]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestNotificationDeleteDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestNotificationDeleteDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"--> NOTIFICATION DELETE = %@",response);
    /*
     response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"2227589152\",\"msisdn\":\"wlgEmFw-nVSLvcLdDo_spQ==\",\"status\":\"1\",\"result\":{\"mailboxID\":\"6138683\",\"briefMessage\":\"Registration Failure\",\"timestamp\":\"2013-12-17T20:54:04.954+07:00\",\"readFlag\":\"READ\",\"notificationType\":\"REGISTRATION\",\"notificationCondition\":\"FAILED\",\"contactSource\":\"svcBilInternetServicesV2.0\",\"serviceID\":\"XL2\",\"lastReasonCode\":{},\"briefTitle\":\"XL AXIATA\",\"message\":\"Maaf, permintaan Anda utk mengaktifkan Paket Internet HotRod3G+ 15MB, 1hr, 2rb belum dapat diproses. Silakan coba beberapa saat lagi. Info: 817\"}}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseNotificationDeleteData:response];
    [dataParser release];
}

#pragma mark - API Reply Notification

- (void)postNotifParam:(NSString*)notifId
           buttonIndex:(NSString*)buttonIndex
               element:(NSString*)element {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 notifId, @"notif_id",
                                 buttonIndex, @"btn_idx",
                                 element, @"element",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,NOTIFICATION_REPLY]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestReplyNotificationDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestReplyNotificationDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"REPLY NOTIFICATION = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseReplyNotificationData:response];
    [dataParser release];
}

#pragma mark - API Survey

- (void)survey {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,SURVEY]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestSurveyDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestSurveyDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"SURVEY = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseSurveyData:response];
    [dataParser release];
}

#pragma mark - API Survey Submit

- (void)submitSurvey:(NSString*)answer {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 answer, @"answer",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,SURVEY_SUBMIT]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestSubmitSurveyDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestSubmitSurveyDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"SUBMIT SURVEY = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseRecommendedPackageData:response];
    [dataParser release];
}

#pragma mark - API Last Transaction

- (void)lastTransaction:(NSString*)type {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 type, @"type",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,LAST_TRANSACTION]];
    //NSLog(@"url = %@",url);
    
    //NSLog(@"sharedData.profileData.token = %@",sharedData.profileData.token);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestLastTransactionDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestLastTransactionDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"LAST TRANSACTION RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseLastTransactionData:response];
    [dataParser release];
}

#pragma mark - API Ads

- (void)ads:(NSString*)pageId
       size:(NSString*)adsSize
   delegate:(id)requestDelegate
successSelector:(SEL)successSelector
errorSelector:(SEL)errorSelector {
    
    self.adsDelegate = requestDelegate;
	self.callback = successSelector;
    self.errorCallback = errorSelector;
    
    NSString *deviceBrand = DEVICE_BRAND;
    UIDevice *device = [UIDevice currentDevice];
    NSString *deviceModel = device.model;
    
    NSString *cid = @"";
    NSString *size = @"";
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        //Device is iPad
        //iPad tyui097f56yt
        cid = CID_ADS_IPAD;
        size = ADS_SIZE_IPAD;
        //NSLog(@"iPad");
    } else{
        //Device is iPhone
        //iPhone 345809koiuy
        cid = CID_ADS_IPHONE;
        size = adsSize;
        //NSLog(@"iPhone");
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,ADS]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    /*
    NSLog(@"cid = %@",cid);
    NSLog(@"size = %@",size);
    NSLog(@"deviceBrand = %@",deviceBrand);
    NSLog(@"deviceModel = %@",deviceModel);
    NSLog(@"pageId = %@",pageId);*/
    
    [request addPostValue:cid forKey:@"cid"];
    [request addPostValue:size forKey:@"size"];
    [request addPostValue:deviceBrand forKey:@"brand"];
    [request addPostValue:deviceModel forKey:@"model"];
    [request addPostValue:@"3" forKey:@"n"];
    [request addPostValue:pageId forKey:@"page"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestAdsDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestAdsDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"ADS = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseAdsData:response];
    [dataParser release];
}

#pragma mark - Download Multiple Image

- (void)downloadMultipleImage:(NSArray*)imagesURL
                     delegate:(id)requestDelegate
              successSelector:(SEL)successSelector
                errorSelector:(SEL)errorSelector
{
    @try {
        
        self.adsDelegate = requestDelegate;
        self.callback = successSelector;
        self.errorCallback = errorSelector;
        
        ASINetworkQueue *networkQueue = [[ASINetworkQueue alloc] init];
        [networkQueue reset];
        //[networkQueue setDownloadProgressDelegate:progressIndicator];
        if ([networkQueue respondsToSelector:@selector(imageFetchComplete:)])
            [networkQueue setRequestDidFinishSelector:@selector(imageFetchComplete:)];
        if([networkQueue respondsToSelector:@selector(imageFetchFailed:)])
            [networkQueue setRequestDidFailSelector:@selector(imageFetchFailed:)];
        //[networkQueue setShowAccurateProgress:[accurateProgress isOn]];
        [networkQueue setDelegate:self];
        
        for (int i=0; i < [imagesURL count]; i++) {
            ASIHTTPRequest *request;
            request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:[imagesURL objectAtIndex:i]]];
            [request setUserInfo:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%i",i] forKey:@"tag"]];
            [networkQueue addOperation:request];
        }
        
        [networkQueue go];
    }
    @catch (NSException *exception) {
        
    }
}

- (void)imageFetchComplete:(ASIHTTPRequest *)request {
    
    BOOL success = YES;
    NSString *reason = @"";
    
    if (adsDelegate && callback) {
		if ([adsDelegate respondsToSelector:self.callback]) {
            NSString *resultKey = RESULT_KEY;
            NSNumber *resultValue = [NSNumber numberWithBool:success];
            
            NSString *reasonKey = REASON_KEY;
            NSString *reasonValue = reason;
            
            NSString *requestKey = REQUEST_KEY;
            NSNumber *requestValue = [NSNumber numberWithInt:requestType];
            
            NSString *imageKey = @"imagekey";
            UIImage *imageValue = [UIImage imageWithData:[request responseData]];
            
            NSString *imageTagKey = @"imagetag";
            NSDictionary *userInfo = [request userInfo];
            NSString *imageTagValue = [userInfo objectForKey:@"tag"];
            //NSLog(@"imageTagValue = %@",imageTagValue);
            
            NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                  resultValue, resultKey,
                                  reasonValue, reasonKey,
                                  requestValue, requestKey,
                                  imageValue, imageKey,
                                  imageTagValue, imageTagKey, nil];
            
			[adsDelegate performSelector:self.callback withObject:dict];
		}
		else {
			//NSLog(@"No response from delegate");
		}
	}
}

- (void)imageFetchFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    //NSLog(@"error = %@",error);
    
    if ([_delegate respondsToSelector:@selector(requestDoneWithInfo:)]) {
        NSString *resultKey = RESULT_KEY;
        NSNumber *resultValue = [NSNumber numberWithBool:NO];
        
        NSString *reasonKey = REASON_KEY;
        NSString *reasonValue = [NSString stringWithFormat:@"%@",error];
        
        NSString *requestKey = REQUEST_KEY;
        NSNumber *requestValue = [NSNumber numberWithInt:requestType];
        
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:resultValue, resultKey, reasonValue, reasonKey, requestValue, requestKey, nil];
        [_delegate requestDoneWithInfo:dict];
    }
}

#pragma mark - API Balance Items

- (void)balanceItems {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,BALANCE_ITEMS]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestBalanceItemsDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestBalanceItemsDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"BALANCE_ITEMS = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseBalanceItemsData:response];
    [dataParser release];
}

#pragma mark - API Balance Transfer

- (void)balanceTransfer:(NSString*)msisdnTo
             withAmount:(NSString*)amount
            withCaptcha:(NSString*)captcha
                withCid:(NSString*)cid {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 msisdnTo, @"msisdn_to",
                                 amount, @"amount",
                                 //cid, @"cid",
                                 //captcha, @"captcha",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,BALANCE_TRANSFER]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestBalanceTransferDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestBalanceTransferDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"BALANCE_TRANSFER = %@",response);
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"123445\",\"msisdn\":\"cBIq2-XrSlmxJ7jj891mgg==\",\"status\":\"1\",\"result\":{\"msisdnto\":\"A0nBcSOHRfXaCHWbVoFeUA==\",\"amount\":\"50\",\"charge\":\"3000\"}}";*/
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"1295435600\",\"msisdn\":\"wId6ggUuW2Ysn1UHKOQfJg==\",\"status\":\"1\",\"result\":{\"msisdnto\":\"7pOWpi5hLQuBZuiM717WiQ==\",\"amount\":\"10000\",\"charge\":\"500\",\"newbalance\":\"-10500\",\"targetoldactivestop\":\"20130630\",\"targetnewactivestop\":\"20130630\"}}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseBalanceTransferData:response];
    [dataParser release];
}

#pragma mark - API Extend Validity

- (void)extendValidity:(NSString*)amount {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 amount, @"amount",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,EXTEND_VALIDITY]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestExtendValidityDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestExtendValidityDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"EXTEND_VALIDITY = %@",response);
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"0668600820\",\"msisdn\":\"sGYnYULS1hfy1VTPAnE_Vg==\",\"status\":\"1\",\"statustext\":\"Your active period has been extended for 7 days with tariff Rp 2000 which has been deducted from your balance. Thank you.\"}";
    NSLog(@"EXTEND_VALIDITY 2 = %@",response);*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseExtendValidityData:response];
    [dataParser release];
}

#pragma mark - API Payment Param

- (void)paymentParam {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,PAYMENT_PARAM]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestPaymentParamDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestPaymentParamDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"PAYMENT_PARAM = %@",response);
    /*
    response = @"{\"status\":\"1\",\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"msisdn\":\"JpCYsSi4ab3iNNnTrGeuMw==\",\"result\":{\"amount\":[{\"value\":\"25000\",\"label\":\"Rp. 25,000\"},{\"value\":\"50000\",\"label\":\"Rp. 50,000\"},{\"value\":\"75000\",\"label\":\"Rp. 75,000\"},{\"value\":\"100000\",\"label\":\"Rp. 100,000\"},{\"value\":\"150000\",\"label\":\"Rp. 150,000\"},{\"value\":\"200000\",\"label\":\"Rp. 200,000\"},{\"value\":\"300000\",\"label\":\"Rp. 300,000\"}],\"bank\":[{\"id\":\"1\",\"value\":\"Bank Mandiri\",\"label\":\"Bank Mandiri (mandiri clickpay)\",\"method_id\":\"1\",\"url_ref\":\"http://www.bankmandiri.co.id/article/faq-ib.aspx\",\"label_ref_id\":\"Bagaimana cara mengaktifkan mandiri clickpay kamu\",\"label_ref_en\":\"How to activate your mandiri clickpay\",\"method_name\":\"topupDC\"},{\"id\":\"5\",\"value\":\"Visa\",\"label\":\"Visa\",\"method_id\":\"3\",\"url_ref\":\"http://www.visa.com/globalgateway/gg_selectcountry.jsp?retcountry=1\",\"label_ref_id\":\"Cara Penggunaan Visa Internet Payment\",\"label_ref_en\":\"How To Use Visa Internet Payment\",\"method_name\":\"topupCC\"},{\"id\":\"7\",\"value\":\"Master\",\"label\":\"Master\",\"method_id\":\"3\",\"url_ref\":\"\",\"label_ref_id\":\"\",\"label_ref_en\":\"\",\"method_name\":\"topupCC\"}],\"method\":[{\"id\":\"3\",\"value\":\"topupCC\",\"label\":\"Credit Card\"},{\"id\":\"1\",\"value\":\"topupDC\",\"label\":\"Debit Card\"}]}}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parsePaymentParamData:response];
    [dataParser release];
}

#pragma mark - API Payment Help

- (void)paymentHelp:(NSString*)msisdn
         withCardNo:(NSString*)cardNo
           withBank:(NSString*)bank
         withAmount:(NSString*)amount {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdnEncrypted = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCEncrypt withKey:saltKeyAPI];
    NSString *cardNoEncrypted = [EncryptDecrypt doCipherForiOS7:cardNo action:kCCEncrypt withKey:saltKeyAPI];
    NSString *bankEncrypted = [EncryptDecrypt doCipherForiOS7:bank action:kCCEncrypt withKey:saltKeyAPI];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 msisdnEncrypted, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 cardNoEncrypted, @"cardno",
                                 bankEncrypted, @"bank",
                                 amount, @"amount",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,PAYMENT_HELP]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestPaymentHelpDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestPaymentHelpDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"PAYMENT_HELP = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parsePaymentHelpData:response];
    [dataParser release];
}

#pragma mark - API Payment Debit

- (void)topUpDebit:(NSString*)msisdn
        withAmount:(NSString*)amount
       withTrxType:(NSString*)trxType
          withBank:(NSString*)bank
        withCardNo:(NSString*)cardNo
     withTokenResp:(NSString*)tokenResp
       withCaptcha:(NSString*)captcha
           withCid:(NSString*)cid {
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    NSString *saltKeyAPI = SALT_KEY;
    
//    NSString *msisdnEncrypted = [EncryptDecrypt doCipher:msisdn action:kCCEncrypt withKey:saltKeyAPI];
//    NSString *cardNoEncrypted = [EncryptDecrypt doCipher:cardNo action:kCCEncrypt withKey:saltKeyAPI];
//    NSString *bankEncrypted = [EncryptDecrypt doCipher:bank action:kCCEncrypt withKey:saltKeyAPI];
    
    NSDictionary *paymentDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                 msisdn, @"msisdn_to",
                                 amount, @"amount",
                                 trxType, @"transtype",
                                 bank, @"bankname",
                                 cardNo, @"cardno",
                                 tokenResp, @"tokenresp",
                                 //cid, @"cid",
                                 //captcha, @"captcha",
                                 nil];
    
    NSString *paymentData = [paymentDict JSONRepresentation];
    NSString *paymentDataEncrypted = [EncryptDecrypt doCipherForiOS7:paymentData action:kCCEncrypt withKey:saltKeyAPI];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 paymentDataEncrypted, @"paymentdata",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,PAYMENT_DEBIT]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestPaymentDebitDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestPaymentDebitDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"PAYMENT_DEBIT = %@",response);
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"456456478\",\"msisdn\":\"A0nBcSOHRfXaCHWbVoFeUA==\",\"status\":\"1\",\"result\":{\"msisdnto\":\"A0nBcSOHRfXaCHWbVoFeUA==\",\"amount\":\"50000\",\"transtype\":\"paymentDC\",\"bankname\":\"Bank Mandiri\",\"newbalance\":\"51450\",\"newactivedate\":\"20111219014800\"}}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parsePaymentDebitData:response];
    [dataParser release];
}

#pragma mark - API Gift Package

- (void)giftPackage:(NSString*)msisdnTo
        withPackage:(NSString*)packageId
        withCaptcha:(NSString*)captcha
            withCid:(NSString*)cid {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdnToEncrypted = [EncryptDecrypt doCipherForiOS7:msisdnTo action:kCCEncrypt withKey:saltKeyAPI];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 msisdnToEncrypted, @"msisdn_to",
                                 packageId, @"pkgid",
                                 cid, @"cid",
                                 captcha, @"captcha",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,GIFT_PACKAGE]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestGiftPackageDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestGiftPackageDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"GIFT_PACKAGE = %@",response);
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"5252306990\",\"msisdn\":\"JpCYsSi4ab3iNNnTrGeuMw==\",\"status\":\"1\",\"smsid\":\"INCM201203220000178776\"}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseGiftPackageData:response];
    [dataParser release];
}

#pragma mark - API Buy Package

- (void)buyPackage:(NSString*)packageId
        withAmount:(NSString*)amount
         withTrxId:(NSString*)trxId {
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 packageId, @"pkgid",
                                 amount, @"amount",
                                 trxId, @"txid",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString BUY PACKAGE = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,BUY_PACKAGE]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestBuyPackageDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestBuyPackageDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"BUY_PACKAGE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseBuyPackageData:response];
    [dataParser release];
}

#pragma mark - API Stop Package

- (void)stopPackage:(NSString*)unsub
          withUnreg:(NSString*)unreg {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 unsub, @"unsubcmd",
                                 unreg, @"unregcmd",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,STOP_PACKAGE]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestStopPackageDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestStopPackageDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"STOP_PACKAGE = %@",response);
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"123\",\"msisdn\":\"DCNsRHUeNJwrlzvViKKx2A==\",\"status\":\"1\",\"message\":\"Pelanggan Yth, \n \nKami sedang memproses permintaan Berhenti Langganan Anda. \nTerima kasih telah menggunakan layanan XL. \n \nJika Anda membutuhkan bantuan, silakan hubungi kami di 817 melalui nomor XL (Rp.500 telpon) atau +62-21-579 59 817 dari operator lain (biaya telpon tergantung tarif operator yang berlaku).\",\"smsid\":\"124234\"}";
    NSLog(@"STOP_PACKAGE = %@",response);*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseBuyPackageData:response];
    [dataParser release];
}

#pragma mark - API Upgrade Package

- (void)upgradePackage:(NSString*)flagUpgrade {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 flagUpgrade, @"flagupgrade",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,UPGRADE_PACKAGE]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestUpgradePackageDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestUpgradePackageDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"UPGRADE_PACKAGE = %@",response);
    /*
     response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"0911346130\",\"msisdn\":\"wId6ggUuW2Ysn1UHKOQfJg==\",\"status\":\"1\",\"smsid\":\"\"}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseUpgradePackageData:response];
    [dataParser release];
}

#pragma mark - API Contact Us

- (void)contactUs:(NSString*)type {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 type, @"type",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,CONTACT_US]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestContactUsDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestContactUsDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"CONTACT_US = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseContactUsData:response];
    [dataParser release];
}

#pragma mark - API Info Roaming

- (void)infoRoaming:(NSString*)type {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 type, @"type",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,ROAMING]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestInfoRoamingDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestInfoRoamingDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"INFO ROAMING = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseRoamingData:response];
    [dataParser release];
}

#pragma mark - API XL Star

- (void)xlStar {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,XL_STAR]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestXLStarDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestXLStarDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"XL STAR = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseXLStarData:response];
    [dataParser release];
}

#pragma mark - API PUK

- (void)puk:(NSString*)msisdnTo
  withIccid:(NSString*)iccid {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdnToEncrypted = [EncryptDecrypt doCipherForiOS7:msisdnTo action:kCCEncrypt withKey:saltKeyAPI];
    NSString *iccidEncrypted = [EncryptDecrypt doCipherForiOS7:iccid action:kCCEncrypt withKey:saltKeyAPI];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 msisdnToEncrypted, @"msisdn_to",
                                 iccidEncrypted, @"iccid",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,PUK]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestPUKDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestPUKDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"PUK = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parsePUKData:response];
    [dataParser release];
}

#pragma mark - API Device Setting

- (void)deviceSetting:(NSString*)type {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 type, @"type",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,DEVICE_SETTING]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestDeviceSettingDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestDeviceSettingDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"DEVICE_SETTING = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseDeviceSettingData:response];
    [dataParser release];
}

#pragma mark - API Shop Location

- (void)shopLocation:(NSString*)city {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 city, @"city",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,SHOP_LOCATION]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestShopLocationDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestShopLocationDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"SHOP_LOCATION = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseShopLocationData:response];
    [dataParser release];
}

#pragma mark - API FAQ

- (void)faq:(NSString*)parentId
withKeyword:(NSString*)keyword {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 parentId, @"parent_id",
                                 keyword, @"keyword",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,FAQ]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestFAQDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestFAQDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"FAQ = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseFAQData:response];
    [dataParser release];
}

#pragma mark - API XL Tunai Balance

- (void)xlTunaiBalance {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,XL_TUNAI_BALANCE]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestXLTunaiBalanceDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestXLTunaiBalanceDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"XL TUNAI BALANCE = %@",response);
    
    //response = @"{\"status\":\"-1\",\"err_code\":\"204\",\"err_desc\":\"SSO Token missing\"}";
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseXLTunaiBalanceData:response];
    [dataParser release];
}

#pragma mark - API XL Tunai Reload

- (void)xlTunaiReload:(NSString*)voucherPin
             msisdnTo:(NSString*)msisdnTo
           withAmount:(NSString*)amount
          withCaptcha:(NSString*)captcha
              withCid:(NSString*)cid {
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 voucherPin, @"pin",
                                 amount, @"amount",
                                 msisdnTo, @"msisdn_to",
                                 cid, @"cid",
                                 captcha, @"captcha",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"token = %@",sharedData.profileData.token);
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,XL_TUNAI_RELOAD]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestXLTunaiReloadDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestXLTunaiReloadDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"XL TUNAI RELOAD = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseXLTunaiReloadData:response];
    [dataParser release];
}

#pragma mark - API XL Tunai PLN

- (void)xlTunaiPLN:(NSString*)voucherPin
       withMeterId:(NSString*)meterId
        withAmount:(NSString*)amount
       withCaptcha:(NSString*)captcha
           withCid:(NSString*)cid {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 voucherPin, @"pin",
                                 amount, @"amount",
                                 meterId, @"meter_id",
                                 cid, @"cid",
                                 captcha, @"captcha",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,XL_TUNAI_PLN]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestXLTunaiPLNDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestXLTunaiPLNDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"XL TUNAI PLN = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseXLTunaiReloadData:response];
    [dataParser release];
}

#pragma mark - API Change Language

- (void)changeLanguage:(NSString*)newLang {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 newLang, @"new_lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,CHANGE_LANGUAGE]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestChangeLanguageDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestChangeLanguageDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"CHANGE LANGUAGE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseChangeLanguageData:response];
    [dataParser release];
}

#pragma mark - API Profile Info

- (void)profileInfo {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
//    NSLog(@"--> sharedData.profileData.msisdn = %@",sharedData.profileData.msisdn);
//    NSString *saltKeyAPI = SALT_KEY;
//    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
//    NSLog(@"msisdn Decrypt = %@",msisdn);
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,PROFILE_INFO]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestProfileInfoDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestProfileInfoDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"PROFILE_INFO = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseProfileInfoData:response];
    [dataParser release];
}

#pragma mark - API Quota Meter

- (void)quotaMeter {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,QUOTA_METER]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestQuotaMeterDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestQuotaMeterDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"QUOTA_METER = %@",response);
    /*
    //dummy
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":36592250,\"msisdn\":\"cnNDv2eO9xmw528zruowSA==\",\"status\":\"1\",\"result\":{\"quotameter\":{\"label_package\":\"HOTROD\",\"type\":1,\"label_nusage_from\":\"0MB\",\"label_nusage_to\":\"100MB\",\"label_desc_nusage\":\"Normal Pemakaian Limit 60MB\",\"nusage\":\"80%\",\"label_desc_usage\":\"Total Current Data 40MB\",\"usage\":\"50%\"},\"label_remaining_days\":\"Remaining Active Period 4 days\",\"label_hint\":\"Sisa pemakaian internet anda masih banyak. Ayo maksimalkan pemakaian internet kamu, selalu update!!\",\"label_button\":\"\",\"label_desc\":\"Keet update with Hotrog3G+\",\"banner\":\"\",\"href_banner\":\"\",\"icon\":\"\"}}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseQuotaMeterData:response];
    [dataParser release];
}

#pragma mark - API Recommended Package XL

- (void)recommendedPackageXL:(NSString*)serviceId {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 serviceId, @"serviceId",
                                 //@"XL1", @"serviceId",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,RECOMMENDED_PACKAGE_XL]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestRecommendedPackageXLDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestRecommendedPackageXLDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"RECOMMENDED_PACKAGE_XL = %@",response);
    
    // dummy
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"status\":\"1\",\"msisdn\":\"BDnSq2LmKh97Etg2cyRAEQ==\",\"trxid\":1403357913,\"result\":[{\"href\":\"#buy_package\",\"hrefweb\":\"#\",\"id\":\"6.0001.0006\",\"parent_id\":\"6.0001\",\"group\":\"HotRod3G+\",\"gid\":\"10\",\"name\":\"HotRod3G Monthly, 600MB 1.5GB\",\"pkgid\":\"XL25N\",\"pkggroup\":\"DATA\",\"volume\":\"2.1GB\",\"duration\":\"\",\"pkgtype\":\"recur\",\"price\":\"25000\",\"desc\":\"\",\"confirmDesc\":\"You choose Monthly HotRod 3G Package 600MB, IDR25k. Enjoy FAST \",\"icon\":\"\",\"hex\":\"\"}]}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseRecommendedPackageXLData:response];
    [dataParser release];
}

#pragma mark - API Share Quota

- (void)shareQuotaFromServiceIdA:(NSString*)serviceIdA
                    toServiceIdB:(NSString*)serviceIdB
                        toMsisdn:(NSString*)msisdnTo
                     withCaptcha:(NSString*)captcha
                         withCid:(NSString*)cid {
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 msisdnTo, @"msisdn_to",
                                 serviceIdA, @"serviceIdA",
                                 serviceIdB, @"serviceIdB",
                                 cid, @"cid",
                                 captcha, @"captcha",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,SHARE_QUOTA]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestShareQuotaDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestShareQuotaDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"SHARE_QUOTA = %@",response);
    
    // dummy
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"status\":\"1\",\"msisdn\":\"BDnSq2LmKh97Etg2cyRAEQ==\",\"trxid\":1403357913,\"result\":[{\"href\":\"#buy_package\",\"hrefweb\":\"#\",\"id\":\"6.0001.0006\",\"parent_id\":\"6.0001\",\"group\":\"HotRod3G+\",\"gid\":\"10\",\"name\":\"HotRod3G Monthly, 600MB 1.5GB\",\"pkgid\":\"XL25N\",\"pkggroup\":\"DATA\",\"volume\":\"2.1GB\",\"duration\":\"\",\"pkgtype\":\"recur\",\"price\":\"25000\",\"desc\":\"\",\"confirmDesc\":\"You choose Monthly HotRod 3G Package 600MB, IDR25k. Enjoy FAST \",\"icon\":\"\",\"hex\":\"\"}]}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseShareQuotaData:response];
    [dataParser release];
}

#pragma mark - API Quota Calculator

- (void)quotaCalculator {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,QUOTA_CALCULATOR]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestQuotaCalculatorDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestQuotaCalculatorDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"QUOTA_CALCULATOR = %@",response);
    
    // dummy
    /*
     response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"status\":\"1\",\"msisdn\":\"BDnSq2LmKh97Etg2cyRAEQ==\",\"trxid\":1403357913,\"result\":[{\"href\":\"#buy_package\",\"hrefweb\":\"#\",\"id\":\"6.0001.0006\",\"parent_id\":\"6.0001\",\"group\":\"HotRod3G+\",\"gid\":\"10\",\"name\":\"HotRod3G Monthly, 600MB 1.5GB\",\"pkgid\":\"XL25N\",\"pkggroup\":\"DATA\",\"volume\":\"2.1GB\",\"duration\":\"\",\"pkgtype\":\"recur\",\"price\":\"25000\",\"desc\":\"\",\"confirmDesc\":\"You choose Monthly HotRod 3G Package 600MB, IDR25k. Enjoy FAST \",\"icon\":\"\",\"hex\":\"\"}]}";*/
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":4849031,\"msisdn\":\"__E2LaNLJsFmATvKFil-zw==\",\"status\":\"1\",\"result\":{\"listUsage\":[{\"code\":\"stre\",\"title\":\"Streaming\",\"color\":\"30ACFF\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":24,\"label_max\":\"2 Hours\",\"unit_scale_value\":5,\"unit\":\"minutes\",\"conversion\":\"5 KB/Min\",\"max_volume_value\":600,\"max_scale_perday_value\":120,\"description_daily\":\"Select Daily Streaming Access\",\"description_weekly\":\"Select Weekly Streaming Access\",\"description_monthly\":\"Select Monthly Streaming Access\"},{\"code\":\"brow\",\"title\":\"Browsing\",\"color\":\"A62900\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":16,\"label_max\":\"600 KB\",\"unit_scale_value\":15,\"unit\":\"minutes\",\"conversion\":\"19 KB/Min\",\"max_volume_value\":4560,\"max_scale_perday_value\":240,\"description_daily\":\"Select Daily Browsing Access\",\"description_weekly\":\"Select Weekly Browsing Access\",\"description_monthly\":\"Select Monthly Browsing Access\"},{\"code\":\"chat\",\"title\":\"Chatting\",\"color\":\"FFD1A3\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":16,\"label_max\":\"8 hours\",\"unit_scale_value\":30,\"unit\":\"minutes\",\"conversion\":\"6 KB/Min\",\"max_volume_value\":2880,\"max_scale_perday_value\":480,\"description_daily\":\"Select Daily Chatting Access\",\"description_weekly\":\"Select Weekly Chatting Access\",\"description_monthly\":\"Select Monthly Chatting Access\"},{\"code\":\"down\",\"title\":\"Download\",\"color\":\"4AB84A\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":12,\"label_max\":\"6 GB\",\"unit_scale_value\":512,\"unit\":\"MB\",\"conversion\":\"-\",\"max_volume_value\":6291456,\"max_scale_perday_value\":6144,\"description_daily\":\"Select Daily Download Access\",\"description_weekly\":\"Select Weekly Download Access\",\"description_monthly\":\"Select Monthly Download Access\"},{\"code\":\"mail\",\"title\":\"Email\",\"color\":\"663300\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":20,\"label_max\":\"100 Email\",\"unit_scale_value\":5,\"unit\":\"Emails\",\"conversion\":\"270 KB/Email\",\"max_volume_value\":27000,\"max_scale_perday_value\":100,\"description_daily\":\"Select Daily Email Access\",\"description_weekly\":\"Select Weekly Email Access\",\"description_monthly\":\"Select Monthly Email Access\"},{\"code\":\"sosm\",\"title\":\"Social Media\",\"color\":\"FF1975\",\"unit_scale_value\":30,\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":16,\"label_max\":\"8 Hours\",\"unit\":\"minutes\",\"conversion\":\"45 KB/Min\",\"max_volume_value\":21600,\"max_scale_perday_value\":480,\"description_daily\":\"Select Daily Social Media Access\",\"description_weekly\":\"Select Weekly Social Media Access\",\"description_monthly\":\"Select Monthly Social Media Access\"},{\"code\":\"game\",\"title\":\"Online Games\",\"color\":\"AF1975\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":16,\"label_max\":\"4 hours\",\"unit_scale_value\":15,\"unit\":\"minutes\",\"conversion\":\"4 KB/Min\",\"max_volume_value\":960,\"max_scale_perday_value\":240,\"description_daily\":\"Select Daily Online Games Access\",\"description_weekly\":\"Select Weekly Online Games Access\",\"description_monthly\":\"Select Monthly Online Games Access\"},{\"code\":\"app\",\"title\":\"Online Application\",\"color\":\"FA1075\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":16,\"label_max\":\"4 hours\",\"unit\":\"minutes\",\"unit_scale_value\":15,\"conversion\":\"5 KB/Min\",\"max_volume_value\":1200,\"max_scale_perday_value\":240,\"description_daily\":\"Select Daily Online Application Access\",\"description_weekly\":\"Select Weekly Online Application Access\",\"description_monthly\":\"Select Monthly Online Application Access\"}],\"title\":\"Quota Calculator\",\"usage\":\"Usage Simulator\",\"usageDetail\":\"Data Quota Simulator (Estimate Your Daily/Weekly/Monthly)\",\"dailyUsage\":\"Your Daily Average Usage\",\"choosePackage\":\"Choose Your Internet Package\",\"recommended\":\"Recommended Package For You\",\"daily\":\"Daily\",\"weekly\":\"Weekly\",\"monthly\":\"Monthly\",\"totalVolume\":{\"daily\":\"6GB\",\"daily_value\":6291456,\"weekly\":\"42GB\",\"weekly_value\":44040192,\"monthly\":\"181GB\",\"monthly_value\":189792256}}}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseQuotaCalculatorData:response];
    [dataParser release];
}

#pragma mark - API Quota Calculator Calculate

- (void)quotaCalculatorCalculate:(NSString*)dataValue forPlanType:(NSString*)planType {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 planType, @"plan_type",
                                 dataValue, @"data_value",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,QUOTA_CALCULATOR_CALCULATE]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestQuotaCalculatorCalculateDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestQuotaCalculatorCalculateDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"QUOTA_CALCULATOR_CALCULATE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseRecommendedPackageXLData:response];
    [dataParser release];
}

#pragma mark - API AXIS STORE Term & Condition

- (void)termAndConditionAXISStore {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,AXIS_STORE_TERM_AND_CONDITION]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestTermAndConditionDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestTermAndConditionDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"AXIS_STORE_TERM_AND_CONDITION = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseTermAndConditionData:response];
    [dataParser release];
}

#pragma mark - API AXIS STORE User Guide

- (void)userGuideAXISStore {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,AXIS_STORE_USER_GUIDE]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestUserGuideDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestUserGuideDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"AXIS_STORE_USER_GUIDE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseUserGuideData:response];
    [dataParser release];
}

#pragma mark - API AXIS STORE Trx History

- (void)trxHistoryAXISStore {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,AXIS_STORE_TRX_HISTORY]];
    //NSLog(@"url = %@",url);
    
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestTrxHistoryDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestTrxHistoryDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"AXIS_STORE_TRX_HISTORY = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseTrxHistoryData:response];
    [dataParser release];
}

#pragma mark - API AXIS STORE Category

- (void)categoryAXISStore:(NSString*)categoryId
         withCategoryName:(NSString*)categoryName
                withLimit:(NSString*)limit
                 withPage:(NSString*)page {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 categoryId, @"category_id",
                                 categoryName, @"category_name",
                                 limit, @"limit",
                                 page, @"page",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,AXIS_STORE_CATEGORY]];
    //NSLog(@"url = %@",url);
    
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestCategoryDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestCategoryDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"AXIS_STORE_CATEGORY = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseCategoryData:response];
    [dataParser release];
}

#pragma mark - API AXIS STORE Product

- (void)productAXISStore:(NSString*)productId
         withProductName:(NSString*)productName
          withCategoryId:(NSString*)categoryId
             withKeyword:(NSString*)keyword
               withLimit:(NSString*)limit
                withPage:(NSString*)page {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 productId, @"product_id",
                                 productName, @"product_name",
                                 categoryId, @"category_id",
                                 keyword, @"keyword",
                                 limit, @"limit",
                                 page, @"page",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,AXIS_STORE_PRODUCT]];
    //NSLog(@"url = %@",url);
    
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestProductDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestProductDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"AXIS_STORE_PRODUCT = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseProductData:response];
    [dataParser release];
}

#pragma mark - SSO Register FB

- (void)registerFacebookAccount:(NSString*)token
                     withMsisdn:(NSString*)msisdn
                   withFBUserId:(NSString*)fbUserId
                    withFBToken:(NSString*)fbToken
                      withTrxId:(NSString*)trxId {
    
    NSString *secretKey = SECRET_KEY_SSO;
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *sharedKeySSO = SHARED_KEY_SSO;
    NSString *msisdnTmp = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCDecrypt withKey:saltKeyAPI];
    msisdnTmp = [EncryptDecrypt doCipherForiOS7:msisdnTmp action:kCCEncrypt withKey:sharedKeySSO];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 token, @"token",
                                 msisdnTmp, @"msisdn",
                                 fbUserId, @"fb_userid",
                                 fbToken, @"fb_token",
                                 trxId, @"trxid",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,REG_FB_TOKEN]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestRegisterFBTokenDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestRegisterFBTokenDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"FB TOKEN = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseFacebookData:response];
    [dataParser release];
}

#pragma mark - SSO Unlink FB

- (void)unlinkFacebookAccount:(NSString*)token
                   withMsisdn:(NSString*)msisdn {
    NSString *secretKey = SECRET_KEY_SSO;
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *sharedKeySSO = SHARED_KEY_SSO;
    NSString *msisdnTmp = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCDecrypt withKey:saltKeyAPI];
    msisdnTmp = [EncryptDecrypt doCipherForiOS7:msisdnTmp action:kCCEncrypt withKey:sharedKeySSO];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 token, @"token",
                                 msisdnTmp, @"msisdn",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,UNLINK_FB]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestUnlinkFBDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestUnlinkFBDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"UNLINK FB = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseFacebookData:response];
    [dataParser release];
}

#pragma mark - SSO Share to FB

- (void)shareToFacebook:(NSString*)token
             withMsisdn:(NSString*)msisdn
              withTrxId:(NSString*)trxId {
    
    NSString *secretKey = SECRET_KEY_SSO;
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *sharedKeySSO = SHARED_KEY_SSO;
    NSString *msisdnTmp = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCDecrypt withKey:saltKeyAPI];
    msisdnTmp = [EncryptDecrypt doCipherForiOS7:msisdnTmp action:kCCEncrypt withKey:sharedKeySSO];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 token, @"token",
                                 msisdnTmp, @"msisdn",
                                 trxId, @"trxid",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,SHARE_FB]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestShareFBDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestShareFBDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"SHARE FB = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseFacebookData:response];
    [dataParser release];
}

// TODO : NEPTUNE
- (void)paketSukaSuka
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,QUOTA_CALCULATOR]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestPaketSukaSukaDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestPaketSukaSukaDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"Paket suka = %@",response);
    
    // dummy
    /*
     response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"status\":\"1\",\"msisdn\":\"BDnSq2LmKh97Etg2cyRAEQ==\",\"trxid\":1403357913,\"result\":[{\"href\":\"#buy_package\",\"hrefweb\":\"#\",\"id\":\"6.0001.0006\",\"parent_id\":\"6.0001\",\"group\":\"HotRod3G+\",\"gid\":\"10\",\"name\":\"HotRod3G Monthly, 600MB 1.5GB\",\"pkgid\":\"XL25N\",\"pkggroup\":\"DATA\",\"volume\":\"2.1GB\",\"duration\":\"\",\"pkgtype\":\"recur\",\"price\":\"25000\",\"desc\":\"\",\"confirmDesc\":\"You choose Monthly HotRod 3G Package 600MB, IDR25k. Enjoy FAST \",\"icon\":\"\",\"hex\":\"\"}]}";*/
    /*
     response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":4849031,\"msisdn\":\"__E2LaNLJsFmATvKFil-zw==\",\"status\":\"1\",\"result\":{\"listUsage\":[{\"code\":\"stre\",\"title\":\"Streaming\",\"color\":\"30ACFF\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":24,\"label_max\":\"2 Hours\",\"unit_scale_value\":5,\"unit\":\"minutes\",\"conversion\":\"5 KB/Min\",\"max_volume_value\":600,\"max_scale_perday_value\":120,\"description_daily\":\"Select Daily Streaming Access\",\"description_weekly\":\"Select Weekly Streaming Access\",\"description_monthly\":\"Select Monthly Streaming Access\"},{\"code\":\"brow\",\"title\":\"Browsing\",\"color\":\"A62900\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":16,\"label_max\":\"600 KB\",\"unit_scale_value\":15,\"unit\":\"minutes\",\"conversion\":\"19 KB/Min\",\"max_volume_value\":4560,\"max_scale_perday_value\":240,\"description_daily\":\"Select Daily Browsing Access\",\"description_weekly\":\"Select Weekly Browsing Access\",\"description_monthly\":\"Select Monthly Browsing Access\"},{\"code\":\"chat\",\"title\":\"Chatting\",\"color\":\"FFD1A3\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":16,\"label_max\":\"8 hours\",\"unit_scale_value\":30,\"unit\":\"minutes\",\"conversion\":\"6 KB/Min\",\"max_volume_value\":2880,\"max_scale_perday_value\":480,\"description_daily\":\"Select Daily Chatting Access\",\"description_weekly\":\"Select Weekly Chatting Access\",\"description_monthly\":\"Select Monthly Chatting Access\"},{\"code\":\"down\",\"title\":\"Download\",\"color\":\"4AB84A\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":12,\"label_max\":\"6 GB\",\"unit_scale_value\":512,\"unit\":\"MB\",\"conversion\":\"-\",\"max_volume_value\":6291456,\"max_scale_perday_value\":6144,\"description_daily\":\"Select Daily Download Access\",\"description_weekly\":\"Select Weekly Download Access\",\"description_monthly\":\"Select Monthly Download Access\"},{\"code\":\"mail\",\"title\":\"Email\",\"color\":\"663300\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":20,\"label_max\":\"100 Email\",\"unit_scale_value\":5,\"unit\":\"Emails\",\"conversion\":\"270 KB/Email\",\"max_volume_value\":27000,\"max_scale_perday_value\":100,\"description_daily\":\"Select Daily Email Access\",\"description_weekly\":\"Select Weekly Email Access\",\"description_monthly\":\"Select Monthly Email Access\"},{\"code\":\"sosm\",\"title\":\"Social Media\",\"color\":\"FF1975\",\"unit_scale_value\":30,\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":16,\"label_max\":\"8 Hours\",\"unit\":\"minutes\",\"conversion\":\"45 KB/Min\",\"max_volume_value\":21600,\"max_scale_perday_value\":480,\"description_daily\":\"Select Daily Social Media Access\",\"description_weekly\":\"Select Weekly Social Media Access\",\"description_monthly\":\"Select Monthly Social Media Access\"},{\"code\":\"game\",\"title\":\"Online Games\",\"color\":\"AF1975\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":16,\"label_max\":\"4 hours\",\"unit_scale_value\":15,\"unit\":\"minutes\",\"conversion\":\"4 KB/Min\",\"max_volume_value\":960,\"max_scale_perday_value\":240,\"description_daily\":\"Select Daily Online Games Access\",\"description_weekly\":\"Select Weekly Online Games Access\",\"description_monthly\":\"Select Monthly Online Games Access\"},{\"code\":\"app\",\"title\":\"Online Application\",\"color\":\"FA1075\",\"icon_url\":\"http://cdn-img.easyicon.net/png/5249/524984.png\",\"max_scale_value\":16,\"label_max\":\"4 hours\",\"unit\":\"minutes\",\"unit_scale_value\":15,\"conversion\":\"5 KB/Min\",\"max_volume_value\":1200,\"max_scale_perday_value\":240,\"description_daily\":\"Select Daily Online Application Access\",\"description_weekly\":\"Select Weekly Online Application Access\",\"description_monthly\":\"Select Monthly Online Application Access\"}],\"title\":\"Quota Calculator\",\"usage\":\"Usage Simulator\",\"usageDetail\":\"Data Quota Simulator (Estimate Your Daily/Weekly/Monthly)\",\"dailyUsage\":\"Your Daily Average Usage\",\"choosePackage\":\"Choose Your Internet Package\",\"recommended\":\"Recommended Package For You\",\"daily\":\"Daily\",\"weekly\":\"Weekly\",\"monthly\":\"Monthly\",\"totalVolume\":{\"daily\":\"6GB\",\"daily_value\":6291456,\"weekly\":\"42GB\",\"weekly_value\":44040192,\"monthly\":\"181GB\",\"monthly_value\":189792256}}}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseQuotaCalculatorData:response];
    [dataParser release];
}

#pragma mark - USAGE SIMULATOR SUKA-SUKA
// TODO : NEPTUNE
- (void)usageSimulatorSukaSuka
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,USAGE_SIMULATOR_SUKASUKA]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestUsageSimulatorDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestUsageSimulatorDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"Paket suka = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseQuotaCalculatorData:response];
    [dataParser release];
}

#pragma mark - MAPPING PACKAGE SUKA-SUKA
-(void)mappingSukaSukaWithPaketSMS:(NSString *)sms
                          andVoice:(NSString *)voice
                       andInternet:(NSString *)internet
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 sms, @"sms",
                                 voice, @"voice",
                                 internet, @"internet",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,MAPPING_PAKET_SUKASUKA]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestMappingSukaSukaDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestMappingSukaSukaDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"Mapping suka-suka = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseMappingPaketSukaSukaData:response];
    [dataParser release];
}

#pragma mark - BUY PACKAGE SUKA-SUKA
-(void)buyPackageSukaSukaWithName:(NSString *)name
                        andAmount:(NSString *)amount
                     andPackageID:(NSString *)pkgid
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 name, @"name_sukasuka",
                                 amount, @"amount",
                                 pkgid, @"pkgid",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,BUY_PAKET_SUKASUKA]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestBuyPaketSukaSukaDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestBuyPaketSukaSukaDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"Buy Paket suka = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseBuyPackageData:response];
    [dataParser release];
}

// TODO : Hygiene
#pragma mark - SSO REQUEST PASSWORD
- (void)requestPassword:(NSString*)msisdn
          withCaptcha:(NSString*)captcha
               withEmail:(NSString *)email
              withCid:(NSString*)cid
            withToken:(NSString*)token
         withLanguage:(NSString*)language
        withDeviceToken:(NSString *)deviceToken
{
    NSString *secretKey = SECRET_KEY_SSO;
    NSString *sharedKey = SHARED_KEY_SSO;
    
    NSString *msisdnEncrypted = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCEncrypt withKey:sharedKey];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 msisdnEncrypted, @"msisdn",
                                 email, @"email",
                                 cid, @"cid",
                                 captcha, @"captcha",
                                 language, @"lang",
                                 token, @"token",
                                 deviceToken, @"register_token",
                                 nil];
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,SSO,REQUEST_PASSWORD]];
    NSLog(@"url = %@",url);
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:jsonString forKey:@"jsondata"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestPasswordDone:)];
    [request setDelegate:self];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
    
}

- (void)requestPasswordDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"REQUEST PASSWORD = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseResetPasswordData:response];
    [dataParser release];
    dataParser = nil;
}

#pragma mark - GET PACKAGE SUKA-SUKA METRICS
-(void)getPackageSukaMatrix
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,PACKAGE_MATRIX_SUKASUKA]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestPackageSukaMatrixDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestPackageSukaMatrixDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"Package suka matrix = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseMatrixPaketSukaSukaData:response];
    [dataParser release];
    dataParser = nil;
}

#pragma mark - PAGE SUKA-SUKA
- (void)pageSukasuka
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,PAGE_SUKASUKA]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestPageSukaSukaDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestPageSukaSukaDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"PAGE SUKA = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parsePageSukaSukaData:response];
    [dataParser release];
    dataParser = nil;
}

#pragma mark - CHECK QUOTA/PACKAGE SUKA-SUKA
- (void)checkQuotaSukasuka
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,QUOTA_SUKASUKA]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestCheckQuotaSukaSukaDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestCheckQuotaSukaSukaDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"CHECK QUOTA suka = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseCheckQuotaSukaSukaData:response];
    [dataParser release];
    dataParser = nil;
}

#pragma mark - MAPPING RENAME SUKA-SUKA
- (void)mappingRenameSukaSukaWithServiceID:(NSString *)serviceID
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 serviceID, @"service_id",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,MAPPING_RENAME_SUKASUKA]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestMappingRenameSukaSukaDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestMappingRenameSukaSukaDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"MAPPING RENAME suka = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseMappingPaketSukaSukaData:response];
    [dataParser release];
    dataParser = nil;
}

#pragma mark - RENAME PACKAGE SUKA-SUKA
- (void)renamePackageSukaSukaWithName:(NSString *)name
                         andServiceID:(NSString *)serviceID
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    /*
     * Add Param from Mapping Paket Suka-suka:
     * price, sms, voice, internet, last_paket
     */
    
    MappingSukaSukaModel *mappingModel = sharedData.mappingSukaSukaData;
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 serviceID, @"service_id",
                                 name, @"name_sukasuka",
                                 mappingModel.price, @"price",
                                 mappingModel.smsDetail, @"sms",
                                 mappingModel.voiceDetail, @"voice",
                                 mappingModel.internetDetail, @"internet",
                                 mappingModel.last_pkg, @"last_paket",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    //NSLog(@"jsonString = %@",jsonString);
    //NSLog(@"token = %@",sharedData.profileData.token);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,RENAME_SUKASUKA]];
    //NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestRenameSukaSukaDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestRenameSukaSukaDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    //NSLog(@"RENAME suka = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseRenameSukaSukaData:response];
    [dataParser release];
    dataParser = nil;
}

-(void)cancelAllRequest
{
    for(ASIHTTPRequest *request in ASIHTTPRequest.sharedQueue.operations)
    {
        [request cancel];
        [request setDelegate:nil];
    }
}

// TODO : V1.9
// Cancel HTTP request by type
-(void)cancelRequestWithRequestType:(RequestType)reqType
{
    for(ASIHTTPRequest *request in ASIHTTPRequest.sharedQueue.operations)
    {
        if(request.tag == reqType)
        {
            [request cancel];
            [request setDelegate:nil];
        }
    }
}

#pragma mark - INFO 4G USIM

- (void)info4GUsim {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,INFO_4G_USIM]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestInfo4GUsimDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestInfo4GUsimDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"INFO_4G_USIM = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseInfo4GUsimData:response];
    [dataParser release];
}

#pragma mark - REPLACE 4G USIM

- (void)replace4gUsim:(NSString*)nickname
         withFullname:(NSString*)fullname
            withEmail:(NSString*)email
         withLanguage:(NSString*)language
           withGender:(NSString*)gender
         withBirthday:(NSString*)birthday
       withEmailNotif:(NSString*)emailNotif
        withPushNotif:(NSString*)pushNotif
         withAddress1:(NSString*)address1
         withAddress2:(NSString*)address2
         withAddress3:(NSString*)address3
             withCity:(NSString*)city
          withZipcode:(NSString*)zipcode
        withAutologin:(NSString*)autologin
       withIdCardType:(NSString*)idCardType
     withIdCardNumber:(NSString*)idCardNumber
     withPlaceOfBirth:(NSString*)placeOfBirth
      withOtherNumber:(NSString*)otherNumber
           withStatus:(NSString*)status {
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 nickname, @"username_subscriber",
                                 fullname, @"fullname",
                                 email, @"email",
                                 language, @"lang",
                                 gender, @"gender",
                                 birthday, @"birth_date",
                                 emailNotif, @"email_notify",
                                 pushNotif, @"push_notify",
                                 address1, @"address",
                                 address2, @"address2",
                                 address3, @"address3",
                                 city, @"city",
                                 zipcode, @"postcode",
                                 autologin, @"autologin",
                                 idCardType, @"idcard_type",
                                 idCardNumber, @"id_card_number",
                                 placeOfBirth, @"place_birth",
                                 otherNumber, @"other_phone",
                                 status, @"status",
                                 nil];
    
    NSLog(@"--> requestData = %@",requestData);
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString Update Profile = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,REPLACE_4G_USIM]];
    NSLog(@"url = %@",url);
    
    //NSLog(@"sharedData.profileData.token = %@",sharedData.profileData.token);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestReplace4gUsimDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestReplace4gUsimDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"REPLACE_4G_USIM RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseReplace4gUsimData:response];
    [dataParser release];
}

#pragma mark - RATE & COMMENT 4G USIM

- (void)rate4g:(NSString*)rating withComment:(NSString*)comment
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 rating, @"rate",
                                 comment, @"comment",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString Update Profile = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,RATE_COMMENT_4G_USIM]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestRateComment4gUsimDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestRateComment4gUsimDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"RATE_COMMENT_4G_USIM RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseRateComment4gUsimData:response];
    [dataParser release];
}

#pragma mark - VOUCHER PROMO LOYALTY MANAGEMENT
-(void)getVoucherPromoWithVoucherInfo:(NSString *)voucherInfo
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 voucherInfo, @"voucher",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString VOUCHER PROMO = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,VOUCHER_PROMO]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestVoucherPromoDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestVoucherPromoDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"VOUCHER PROMO RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseVoucherPromoData:response];
    [dataParser release];
}

#pragma mark - XTRA CHECK POINT
-(void)checkPointXtraPromo
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString XTRA PROMO = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,XTRAPROMO_CHECK_POINT]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestCheckPointDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestCheckPointDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"CHECK POINT PROMO RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseCheckPointPromo:response];
    [dataParser release];
}

#pragma mark - XTRA WINNER LIST
-(void)getWinnerListXtraPromo
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString XTRA WINNER = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,XTRAPROMO_WINNER_LIST]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestGetWinnerListXtraPromoDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestGetWinnerListXtraPromoDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"WINNER LIST PROMO RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseWinnerListXtraPromo:response];
    [dataParser release];
}

-(void)checkGetBonusXtraPromo
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString CHECK BONUS = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,XTRAPROMO_CHECKGETBONUS]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestCheckBonusXtraPromoDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestCheckBonusXtraPromoDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"CHECK BONUS = %@",response);
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseCheckBonusXtraPromo:response];
    [dataParser release];
}

-(void)getBonusXtraPromoWithLayanan:(NSString *)layanan
{
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 layanan, @"layanan",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString CHECK BONUS = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,XTRAPROMO_GETBONUS]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addPostValue:jsonString forKey:@"data"];
    [request addPostValue:sharedData.profileData.token forKey:@"token"];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestGetBonusXtraPromoDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestGetBonusXtraPromoDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"GET BONUS = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseGetBonusXtraPromo:response];
    [dataParser release];
}

#pragma mark - 4G POWER METER

- (void)getFourGPowerWithMatrix:(NSString *)matrix
{
    // TODO : V1.9.5
    NSString *token = getDataFromPreferenceWithKey(PROFILE_TOKEN_CACHE_KEY);
    NSString *keepSignin = getDataFromPreferenceWithKey(PROFILE_FLAG_KMSI_CACHE_KEY);
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    NSString *device = @"";
    if (IS_IPAD) {
        device = @"ipad";
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 480.0f) {
        device = @"iphone";
    }
    else {
        device = @"iphone";
    }
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 matrix, @"matrix",
                                 device, @"device",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString 4 POWER = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,FOURG_POWERPACK]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    //[request addPostValue:sharedData.profileData.token forKey:@"token"];
    // TODO : V1.9.5
    [request addPostValue:token forKey:@"token"];
    [request addPostValue:keepSignin forKey:@"keep_signin"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestFourGPowerDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestFourGPowerDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"4G MATRIX RESPONSE = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseFourGPowerPack:response];
    [dataParser release];
}

-(void)getFourGPackageStatus
{
    // TODO: V1.9.5
    NSString *token = getDataFromPreferenceWithKey(PROFILE_TOKEN_CACHE_KEY);
    NSString *keepSignin = getDataFromPreferenceWithKey(PROFILE_FLAG_KMSI_CACHE_KEY);
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString CHECK 4G PACKAGE = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,FOURG_ISPACKAGE4G]];
    NSLog(@"url = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    //[request addPostValue:sharedData.profileData.token forKey:@"token"];
    // TODO : V1.9.5
    [request addPostValue:token forKey:@"token"];
    [request addPostValue:keepSignin forKey:@"keep_signin"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestFourGPackageStatusDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

-(void)requestFourGPackageStatusDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"4G STATUS RESPONSE = %@",response);
    /*
    response = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":8397179,\"msisdn\":\"kx41z2fBjrHNM-A36gnCFA==\",\"status\":\"1\",\"result\":{\"isPackage4g\":\"1\",\"summary\":{\"activePackage\":1,\"isUnlimited\":\"0\",\"usage\":\"1.86 GB\",\"percentUsage\":93,\"remaining\":\"147.39 MB\",\"percentRemaining\":7,\"total\":\"2.00 GB\",\"activeUntil\":\"2015-12-19\",\"sisaHari\":\"8\",\"percentSisaHari\":9,\"regPackage\":\"2015-09-21\",\"wordingInfoTotalQuota\":\"Total quota yang ditampilkan merubapakan gabungan dari quoata paket yang anda miliki. Untuk informasi quoata yang lebih detail, silahkan klik tombol 'lihat detail paket & quota'\"},\"detail\":[{\"name\":\"AnyNet Nelp Ke Semua Oprtr 250Mnt, 30hr, Rp60rb\",\"serviceId\":\"1210379\",\"price\":\"60000\",\"confirmDesc\":\"You will activate packet AnyNet Nelp ke Semua Operator 250Mnt IDR 60000, valid for 30 days. To Continue, Tap YES.\",\"showRata-rataHarian\":\"0\",\"showEstimasiHabis\":\"0\",\"benefitData\":{\"activeUntil\":\"2015-12-24\",\"isUnlimited\":\"0\",\"isActiveInmyxl\":\"1\",\"paymentMethod\":\"nc\",\"sisaHari\":\"13\",\"percentSisaHari\":\"45\",\"regPackage\":\"2015-11-25\",\"rata-rataHarian\":\"0 KB\",\"estimasiHabis\":\"2015-12-24\",\"wordingInfoExpired\":\"Perkiraan tanggal habis kuota dihitung berdasar ukuran MB rata-rata pemakaian data harian dan dapat berubah sesuai pola pemakaian harian anda\"},\"benefitVoice\":[{\"sisa\":\"176 Minutes\",\"total\":\"NA\",\"name\":\"Nelp (ke XL/Non XL)\"}]},{\"name\":\"4GTank Bonus\",\"serviceId\":\"4110048\",\"price\":\"0\",\"confirmDesc\":\"\",\"showRata-rataHarian\":\"1\",\"showEstimasiHabis\":\"1\",\"benefitData\":{\"activeUntil\":\"2015-12-31\",\"isUnlimited\":\"0\",\"isActiveInmyxl\":\"0\",\"paymentMethod\":\"nc\",\"sisaHari\":\"20\",\"percentSisaHari\":\"28\",\"regPackage\":\"2015-10-21\",\"rata-rataHarian\":\"0 KB\",\"estimasiHabis\":\"2015-12-31\",\"wordingInfoExpired\":\"Perkiraan tanggal habis kuota dihitung berdasar ukuran MB rata-rata pemakaian data harian dan dapat berubah sesuai pola pemakaian harian anda\"}},{\"name\":\"HotRod 2GB, 30Hari,60rb\",\"serviceId\":\"3210279\",\"price\":\"0\",\"confirmDesc\":\"You are going to re-purchase this package ?\",\"showRata-rataHarian\":\"1\",\"showEstimasiHabis\":\"1\",\"benefitData\":{\"activeUntil\":\"2015-12-19\",\"isUnlimited\":\"0\",\"isActiveInmyxl\":\"0\",\"paymentMethod\":\"nc\",\"sisaHari\":\"8\",\"percentSisaHari\":\"9\",\"regPackage\":\"2015-09-21\",\"rata-rataHarian\":\"23.46 MB\",\"estimasiHabis\":\"2015-12-17\",\"wordingInfoExpired\":\"Perkiraan tanggal habis kuota dihitung berdasar ukuran MB rata-rata pemakaian data harian dan dapat berubah sesuai pola pemakaian harian anda\",\"dataBar\":[{\"packageName\":\"24jam di 2G/3G/4G \",\"total\":\"2.00 GB\",\"remaining\":\"147.39 MB\",\"percentRemaining\":\"7\",\"usage\":\"1.86 GB\",\"percentUsage\":93}]}}]}}";*/
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseFourGPackageStatus:response];
    [dataParser release];
}

#pragma mark - QUOTA IMPROVEMENT

- (void)checkQuotaImprovement {
    // TODO : V1.9.5
    NSString *token = getDataFromPreferenceWithKey(PROFILE_TOKEN_CACHE_KEY);
    NSString *keepSignin = getDataFromPreferenceWithKey(PROFILE_FLAG_KMSI_CACHE_KEY);
    
    NSString *username = USERNAME;
    NSString *password = PASSWORD;
    NSString *md5Pwd = [EncryptDecrypt returnMD5Hash:password];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 username, @"username",
                                 md5Pwd, @"password",
                                 sharedData.profileData.msisdn, @"msisdn",
                                 sharedData.profileData.language, @"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    NSLog(@"jsonString QUOTA_IMPROVEMENT = %@",jsonString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API,QUOTA_IMPROVEMENT]];
    NSLog(@"url QUOTA_IMPROVEMENT = %@",url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request addPostValue:jsonString forKey:@"data"];
    //[request addPostValue:sharedData.profileData.token forKey:@"token"];
    // TODO : V1.9.5
    [request addPostValue:token forKey:@"token"];
    [request addPostValue:keepSignin forKey:@"keep_signin"];
    
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestQuotaImprovementDone:)];
    [request setDidFailSelector:@selector(requestWentWrong:)];
    [request setTimeOutSeconds:60];
    [request startAsynchronous];
}

- (void)requestQuotaImprovementDone:(ASIHTTPRequest *)request
{
    NSString *response = [request responseString];
    NSLog(@"QUOTA_IMPROVEMENT = %@",response);
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseQuotaImprovementData:response];
    [dataParser release];
}

@end
