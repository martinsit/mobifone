//
//  AXISnetRequest.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/24/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataParser.h"

typedef enum {
    PROFILE_REQ = 0,
    MENU_REQ,
    SIGN_IN_REQ,
    SIGN_OUT_REQ,
    REGISTER_REQ,
    ACTIVATION_REQ,
    RESET_PASSWORD_REQ,
    RESEND_PIN_REQ,
    REG_DEV_TOKEN_REQ,
    UPDATE_PROFILE_REQ,
    CHANGE_PASSWORD_REQ,
    CHECK_BALANCE_REQ,
    TOPUP_VOUCHER_REQ,
    CHECK_USAGE_REQ,
    RECOMMENDED_PACKAGE_REQ,
    RECOMMENDED_PACKAGE_XL_REQ,
    PROMO_REQ,
    // TODO : V1.9
    UNREAD_NOTIFICATION_REQ,
    NOTIFICATION_REQ,
    NOTIFICATION_DETAIL_REQ,
    NOTIFICATION_DELETE_REQ,
    REPLY_NOTIFICATION_REQ,
    SURVEY_REQ,
    SURVEY_SUBMIT_REQ,
    TRX_HISTORY_REQ,
    ADS_REQ,
    DOWNLOAD_IMAGE_REQ,
    BALANCE_ITEMS_REQ,
    BALANCE_TRANSFER_REQ,
    EXTEND_VALIDITY_REQ,
    PAYMENT_PARAM_REQ,
    PAYMENT_HELP_REQ,
    PAYMENT_DEBIT_REQ,
    GIFT_PACKAGE_REQ,
    BUY_PACKAGE_REQ,
    STOP_PACKAGE_REQ,
    UPGRADE_PACKAGE_REQ,
    CONTACT_US_REQ,
    PUK_REQ,
    DEVICE_SETTING_REQ,
    SHOP_LOCATION_REQ,
    FAQ_REQ,
    REG_FB_TOKEN_REQ,
    UNLINK_FB_REQ,
    SHARE_FB_REQ,
    AXIS_STORE_TERM_AND_CONDITION_REQ,
    AXIS_STORE_USER_GUIDE_REQ,
    AXIS_STORE_TRX_HISTORY_REQ,
    AXIS_STORE_CATEGORY_REQ,
    AXIS_STORE_PRODUCT_REQ,
    CHECK_USAGE_XL_REQ,
    ROAMING_REQ,
    XL_STAR_REQ,
    XL_TUNAI_BALANCE_REQ,
    XL_TUNAI_RELOAD_REQ,
    XL_TUNAI_PLN_REQ,
    CHANGE_LANGUAGE_REQ,
    PROFILE_INFO_REQ,
    QUOTA_METER_REQ,
    SHARE_QUOTA_REQ,
    QUOTA_CALCULATOR_REQ,
    QUOTA_CALCULATOR_CALCULATE_REQ,
    // TODO : NEPTUNE
    PAKET_SUKA_SUKA_REQ,
    USAGE_SIMULATOR_SUKASUKA_REQ,
    MAPPING_PAKET_SUKASUKA_REQ,
    BUY_PAKET_SUKASUKA_REQ,
    // TODO : Hygiene
    REQUEST_PASSWORD_REQ,
    MATRIX_PAKET_SUKASUKA_REQ,
    PACKAGE_USAGE_SUKASUKA_REQ,
    PAGE_SUKASUKA_REQ,
    QUOTA_SUKASUKA_REQ,
    MAPPING_RENAME_SUKASUKA_REQ,
    RENAME_SUKASUKA_REQ,
    // CR 4G USIM
    INFO_4G_USIM_REQ,
    REPLACE_4G_USIM_REQ,
    RATE_COMMENT_4G_USIM_REQ,
    VOUCHER_PROMO_REQ,
    // MAZDA 2
    CHECK_POINT_XTRAPROMO_REQ,
    WINNER_LIST_XTRAPROMO_REQ,
    CHECKBONUS_XTRAPROMO_REQ,
    GETBONUS_XTRAPROMO_REQ,
    // 4G Powerpack
    FOUR_G_POWERPACK_REQ,
    FOUR_G_PACKAGE_STATUS_REQ,
    QUOTA_IMPROVEMENT_REQ
}RequestType;

@class AXISnetRequest;

@protocol AXISnetRequestDelegate <NSObject>
@optional
//- (void)requestDone:(BOOL)success withReason:(NSString*)reason;
- (void)requestDoneWithInfo:(NSDictionary*)result;
@end

@interface AXISnetRequest : NSObject <DataParserDelegate> {
    id <AXISnetRequestDelegate> _delegate;
    RequestType requestType;
    BOOL needRequestAgain;
    int requestCounter;
    
    id adsDelegate;
	SEL callback;
	SEL errorCallback;
}

@property (assign) id <AXISnetRequestDelegate> delegate;
@property (readwrite) RequestType requestType;

@property (nonatomic, retain) id adsDelegate;
@property (nonatomic) SEL callback;
@property (nonatomic) SEL errorCallback;
// TODO : Update Hygiene defect
@property (nonatomic, retain) id registerTokenDelegate;
@property (nonatomic) SEL registerTokenCallback;

+ (id)sharedInstance;

// Get Profile
- (void)profile;

// TODO : HYGIENE
// Profile with device token
- (void)profileWithDeviceToken:(NSString *)deviceToken;

// Get Menu
- (void)menu;

// Sign In
// TODO : HYGIENE
// Add device token parameter to sign in request
- (void)signInWithUsername:(NSString*)username
              withPassword:(NSString*)password
              withLanguage:(NSString*)language
           withDeviceToken:(NSString *)deviceToken;

//- (void)signInWithUsername:(NSString*)username
//              withPassword:(NSString*)password
//              withLanguage:(NSString*)language;

// Sign Out
- (void)signOut;

// Register
- (void)registerWithUsername:(NSString*)username
                withPassword:(NSString*)password
                  withMsisdn:(NSString*)msisdn
                 withCaptcha:(NSString*)captcha
                     withCid:(NSString*)cid
                withFullname:(NSString*)fullname
                   withEmail:(NSString*)email
                withLanguage:(NSString*)language
             withCurrentLang:(NSString*)currentLang;

// Account Activation
- (void)activateMsisdn:(NSString*)msisdn
               withPin:(NSString*)pin
          withLanguage:(NSString*)language;

// Resend PIN
- (void)resendPin:(NSString*)msisdn
      withCaptcha:(NSString*)captcha
          withCid:(NSString*)cid
     withLanguage:(NSString*)language;

// Reset Password
- (void)resetPassword:(NSString*)msisdn
          withCaptcha:(NSString*)captcha
              withCid:(NSString*)cid
            withToken:(NSString*)token
         withLanguage:(NSString*)language;

// Register Device Token
// TODO : Update Hygiene Defect
//- (void)registerDeviceToken:(NSString*)devOS
//               withDevToken:(NSString*)devToken;
//- (void)registerDeviceToken:(NSString*)devToken;
- (void)registerDeviceToken:(NSString*)devToken
               withDelegate:(id)delegate
                andCallBack:(SEL)callback;

// Change Password
- (void)changePassword:(NSString*)password
       withNewPassword:(NSString*)newPassword
 withNewPasswordRetype:(NSString*)newPasswordRetype
          withLanguage:(NSString*)language;

// Update Profile
- (void)updateProfile:(NSString*)nickname
         withFullname:(NSString*)fullname
            withEmail:(NSString*)email
         withLanguage:(NSString*)language
           withGender:(NSString*)gender
         withBirthday:(NSString*)birthday
       withEmailNotif:(NSString*)emailNotif
        withPushNotif:(NSString*)pushNotif
         withAddress1:(NSString*)address1
         withAddress2:(NSString*)address2
         withAddress3:(NSString*)address3
             withCity:(NSString*)city
          withZipcode:(NSString*)zipcode
        withAutologin:(NSString*)autologin
       withIdCardType:(NSString*)idCardType
     withIdCardNumber:(NSString*)idCardNumber
     withPlaceOfBirth:(NSString*)placeOfBirth
      withOtherNumber:(NSString*)otherNumber
           withStatus:(NSString*)status;

// Check Balance
- (void)checkBalance;

// Check Usage
- (void)checkInternetUsage;

// Check Usage XL
- (void)checkInternetUsageXL;

// Topup Voucher
- (void)topUpVoucher:(NSString*)voucherPin
            msisdnTo:(NSString*)msisdnTo
         withCaptcha:(NSString*)captcha
             withCid:(NSString*)cid;

// Recommended Package
- (void)recommendedPackage;

// Promo
- (void)promo;

// Notification
- (void)notification;

// TODO : V1.9
// Request Notification in Background with delegate
- (void)getUnreadMessageNotificationWithDelegate:(id)delegate
                andCallBack:(SEL)callback;

// Notification Detail
- (void)notificationDetail:(NSString*)mailboxId;

// Notification Delete
- (void)notificationDelete:(NSString*)mailboxId;

// Reply Notification
- (void)postNotifParam:(NSString*)notifId
           buttonIndex:(NSString*)buttonIndex
               element:(NSString*)element;

// Survey
- (void)survey;

// Survey Submit
- (void)submitSurvey:(NSString*)answer;

// Transaction History
- (void)lastTransaction:(NSString*)type;

// Ads
- (void)ads:(NSString*)pageId
       size:(NSString*)adsSize
   delegate:(id)requestDelegate
successSelector:(SEL)successSelector
errorSelector:(SEL)errorSelector;

// Download Image
- (void)downloadMultipleImage:(NSArray*)imagesURL
                     delegate:(id)requestDelegate
              successSelector:(SEL)successSelector
                errorSelector:(SEL)errorSelector;

// Balance Items
- (void)balanceItems;

// Balance Transfer
- (void)balanceTransfer:(NSString*)msisdnTo
             withAmount:(NSString*)amount
            withCaptcha:(NSString*)captcha
                withCid:(NSString*)cid;

// Extend Validity
- (void)extendValidity:(NSString*)amount;

// Payment Param
- (void)paymentParam;

// Payment Help
- (void)paymentHelp:(NSString*)msisdn
         withCardNo:(NSString*)cardNo
           withBank:(NSString*)bank
         withAmount:(NSString*)amount;

// Payment Debit
- (void)topUpDebit:(NSString*)msisdn
        withAmount:(NSString*)amount
       withTrxType:(NSString*)trxType
          withBank:(NSString*)bank
        withCardNo:(NSString*)cardNo
     withTokenResp:(NSString*)tokenResp
       withCaptcha:(NSString*)captcha
           withCid:(NSString*)cid;

// Gift Package
- (void)giftPackage:(NSString*)msisdnTo
        withPackage:(NSString*)packageId
        withCaptcha:(NSString*)captcha
            withCid:(NSString*)cid;

// Buy Package
- (void)buyPackage:(NSString*)packageId
        withAmount:(NSString*)amount
         withTrxId:(NSString*)trxId;

// Stop Package
- (void)stopPackage:(NSString*)unsub
          withUnreg:(NSString*)unreg;

// Upgrade Package
- (void)upgradePackage:(NSString*)flagUpgrade;

// Contact Us
- (void)contactUs:(NSString*)type;

// Roaming
- (void)infoRoaming:(NSString*)type;

// XL Star
- (void)xlStar;

// PUK
- (void)puk:(NSString*)msisdnTo
  withIccid:(NSString*)iccid;

// Device Setting
- (void)deviceSetting:(NSString*)type;

// Shop Location
- (void)shopLocation:(NSString*)city;

// FAQ
- (void)faq:(NSString*)parentId
withKeyword:(NSString*)keyword;

- (void)repeatLastRequest;

// AXIS Store
// Term & Condition
- (void)termAndConditionAXISStore;

// User Guide
- (void)userGuideAXISStore;

// Trx History
- (void)trxHistoryAXISStore;

// Category
- (void)categoryAXISStore:(NSString*)categoryId
         withCategoryName:(NSString*)categoryName
                withLimit:(NSString*)limit
                 withPage:(NSString*)page;

// Product
- (void)productAXISStore:(NSString*)productId
         withProductName:(NSString*)productName
          withCategoryId:(NSString*)categoryId
             withKeyword:(NSString*)keyword
               withLimit:(NSString*)limit
                withPage:(NSString*)page;

// Facebook Register
- (void)registerFacebookAccount:(NSString*)token
                     withMsisdn:(NSString*)msisdn
                   withFBUserId:(NSString*)fbUserId
                    withFBToken:(NSString*)fbToken
                      withTrxId:(NSString*)trxId;

// Facebook Unlink
- (void)unlinkFacebookAccount:(NSString*)token
                   withMsisdn:(NSString*)msisdn;

// Facebook Share
- (void)shareToFacebook:(NSString*)token
             withMsisdn:(NSString*)msisdn
              withTrxId:(NSString*)trxId;

// XL Tunai Balance
- (void)xlTunaiBalance;

// XL Tunai Reload
- (void)xlTunaiReload:(NSString*)voucherPin
             msisdnTo:(NSString*)msisdnTo
           withAmount:(NSString*)amount
          withCaptcha:(NSString*)captcha
              withCid:(NSString*)cid;

// XL Tunai PLN
- (void)xlTunaiPLN:(NSString*)voucherPin
       withMeterId:(NSString*)meterId
        withAmount:(NSString*)amount
       withCaptcha:(NSString*)captcha
           withCid:(NSString*)cid;

// Change Language
- (void)changeLanguage:(NSString*)newLang;

// Get Profile Info
- (void)profileInfo;

// Quota Meter
- (void)quotaMeter;

// Recommended Package XL
- (void)recommendedPackageXL:(NSString*)serviceId;

// Share Quota
- (void)shareQuotaFromServiceIdA:(NSString*)serviceIdA
                    toServiceIdB:(NSString*)serviceIdB
                        toMsisdn:(NSString*)msisdnTo
                     withCaptcha:(NSString*)captcha
                         withCid:(NSString*)cid;

// Quota Calculator
- (void)quotaCalculator;

// Quota Calculator Calculate
- (void)quotaCalculatorCalculate:(NSString*)dataValue forPlanType:(NSString*)planType;

// TODO : NEPTUNE
-(void)paketSukaSuka;

-(void)usageSimulatorSukaSuka;

-(void)mappingSukaSukaWithPaketSMS:(NSString *)sms
                          andVoice:(NSString *)voice
                       andInternet:(NSString *)internet;

-(void)buyPackageSukaSukaWithName:(NSString *)name
                        andAmount:(NSString *)amount
                     andPackageID:(NSString *)pkgid;

// TODO : Hygiene
- (void)requestPassword:(NSString*)msisdn
            withCaptcha:(NSString*)captcha
               withEmail:(NSString *)email
                withCid:(NSString*)cid
              withToken:(NSString*)token
           withLanguage:(NSString*)language
        withDeviceToken:(NSString *)deviceToken;

-(void)getPackageSukaMatrix;

-(void)pageSukasuka;

-(void)checkQuotaSukasuka;

-(void)mappingRenameSukaSukaWithServiceID:(NSString *)serviceID;

-(void)renamePackageSukaSukaWithName:(NSString *)name
                        andServiceID:(NSString *)serviceID;

-(void)cancelAllRequest;

// TODO : V1.9
// Cancel HTTP request by type
-(void)cancelRequestWithRequestType:(RequestType)reqType;

// CR 4G USIM
- (void)info4GUsim;

// Replace 4G USIM
- (void)replace4gUsim:(NSString*)nickname
         withFullname:(NSString*)fullname
            withEmail:(NSString*)email
         withLanguage:(NSString*)language
           withGender:(NSString*)gender
         withBirthday:(NSString*)birthday
       withEmailNotif:(NSString*)emailNotif
        withPushNotif:(NSString*)pushNotif
         withAddress1:(NSString*)address1
         withAddress2:(NSString*)address2
         withAddress3:(NSString*)address3
             withCity:(NSString*)city
          withZipcode:(NSString*)zipcode
        withAutologin:(NSString*)autologin
       withIdCardType:(NSString*)idCardType
     withIdCardNumber:(NSString*)idCardNumber
     withPlaceOfBirth:(NSString*)placeOfBirth
      withOtherNumber:(NSString*)otherNumber
           withStatus:(NSString*)status;

// Rate Comment 4G USIM
- (void)rate4g:(NSString*)rating withComment:(NSString*)comment;

// TODO : V1.9
-(void)getVoucherPromoWithVoucherInfo:(NSString *)voucherInfo;

// CHECK POINT MAZDA 2
-(void)checkPointXtraPromo;
-(void)getWinnerListXtraPromo;
-(void)getBonusXtraPromoWithLayanan:(NSString *)layanan;
-(void)checkGetBonusXtraPromo;

// 4G POWERPACK
-(void)getFourGPowerWithMatrix:(NSString *)matrix;
-(void)getFourGPackageStatus;

// Quota Improvement
- (void)checkQuotaImprovement;

@end
