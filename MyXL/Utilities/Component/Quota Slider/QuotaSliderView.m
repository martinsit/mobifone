//
//  QuotaSliderView.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 6/20/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "QuotaSliderView.h"
#import "AsyncImageView.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "DataManager.h"

@implementation QuotaSliderView

@synthesize sliderValueView;
@synthesize sliderValueLabel;
@synthesize unit;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
           withData:(InternetUsageModel*)data
isQuotaCalculatorView:(BOOL)isQuotaCalculatorView
{
    if (self = [super initWithFrame:frame])
    {
        AsyncImageView *imageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        imageView.imageURL = [NSURL URLWithString:data.iconUrl];
        [self addSubview:imageView];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageView.frame.origin.x + imageView.frame.size.width + kDefaultComponentPadding, imageView.frame.origin.y, 200, 20)];
        titleLabel.text = [data.title uppercaseString];
        titleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        titleLabel.textColor = kDefaultTitleFontGrayColor;
        [self addSubview:titleLabel];
        
        UILabel *subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageView.frame.origin.x + imageView.frame.size.width + kDefaultComponentPadding, titleLabel.frame.origin.y + titleLabel.frame.size.height, 180, 20)];
        subtitleLabel.text = data.descriptionDaily;
        subtitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        subtitleLabel.textColor = kDefaultTitleFontGrayColor;
        //subtitleLabel.backgroundColor = [UIColor redColor];
        subtitleLabel.numberOfLines = 0;
        subtitleLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        CGSize expected = calculateExpectedSize(subtitleLabel, data.descriptionDaily);
        CGRect frameSubtitleLabel = subtitleLabel.frame;
        frameSubtitleLabel.size.height = expected.height;
        subtitleLabel.frame = frameSubtitleLabel;
        
        [self addSubview:subtitleLabel];
        
        ASValuePopUpView *sliderValueViewTmp = [[ASValuePopUpView alloc] initWithFrame:CGRectMake(frame.origin.x + frame.size.width - kDefaultComponentPadding - 80 + 15, subtitleLabel.frame.origin.y, 80, 40)];
        sliderValueViewTmp.color = kDefaultNavyBlueColor;
        sliderValueViewTmp.cornerRadius = 4.0;
        [sliderValueViewTmp setFont:[UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize]];
        [sliderValueViewTmp setTextColor:kDefaultWhiteColor];
        [sliderValueViewTmp setString:[NSString stringWithFormat:@"%.0f %@",data.sliderValue,data.unit]];
        self.sliderValueView = sliderValueViewTmp;
        self.unit = data.unit;
        [self addSubview:self.sliderValueView];
        
        /*
        UILabel *sliderValueLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(frame.origin.x + frame.size.width - kDefaultComponentPadding - 80, subtitleLabel.frame.origin.y, 80, 20)];
        sliderValueLabelTmp.text = [NSString stringWithFormat:@"0 %@",data.unit];
        sliderValueLabelTmp.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        sliderValueLabelTmp.textColor = kDefaultWhiteColor;
        sliderValueLabelTmp.backgroundColor = kDefaultNavyBlueColor;
        sliderValueLabelTmp.textAlignment = NSTextAlignmentRight;
        self.sliderValueLabel = sliderValueLabelTmp;
        self.unit = data.unit;
        [self addSubview:self.sliderValueLabel];*/
        
        ASValueTrackingSlider *slider = [[ASValueTrackingSlider alloc] initWithFrame:CGRectMake(0, subtitleLabel.frame.origin.y + subtitleLabel.frame.size.height, frame.size.width, 40)];
        slider.tag = data.sliderTag;
        slider.popUpViewColor = colorWithHexString([NSString stringWithFormat:@"%@",data.color]);
        slider.minimumValue = 0.0;
        slider.maximumValue = data.maxScalePerdayValue;
        slider.value = data.sliderValue;

        // TODO : V1.9
        _isQuotaCalculator = isQuotaCalculatorView;
        if(_isQuotaCalculator)
        {
            slider.continuous = NO;
        }
        
        [slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        slider.delegate = self;
        [self addSubview:slider];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
/*
- (UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}*/

#pragma mark - ASValueTrackingSliderDelegate

- (void)sliderWillDisplayPopUpView:(ASValueTrackingSlider *)slider {
    
}

- (void)sliderDidHidePopUpView:(ASValueTrackingSlider *)slider {
    
}

- (IBAction)sliderValueChanged:(ASValueTrackingSlider *)slider
{
    DataManager *sharedData = [DataManager sharedInstance];
    InternetUsageModel *data = [sharedData.quotaCalculatorData.listUsage objectAtIndex:slider.tag];
    // TODO : V1.9
    if(_isQuotaCalculator)
    {
        NSInteger index = ceilf(slider.value/data.unitScaleValue);
        if(_lastIndex == index)
        {
            if(index > 0)
                index -= 1;
        }
        float newSliderValue = index * data.unitScaleValue;
        slider.value = newSliderValue;
        data.sliderValue = slider.value;
        _lastIndex = index;
    }
    else
    {
        data.sliderValue = slider.value;
    }
   
//    data.sliderValue = slider.value;
    
    //self.sliderValueLabel.text = [NSString stringWithFormat:@"%.0f %@",slider.value,self.unit];
    
    // TODO : V1.9
    if(([[self.unit lowercaseString] isEqualToString:@"minutes"] || [[self.unit lowercaseString] isEqualToString:@"menit"]) && slider.value >= 60)
    {
        int minutes = (int)slider.value%60;
        int hour = (int)slider.value/60;
        NSString *strHour = [Language get:@"hours" alter:nil];
        if(hour == 1)
            strHour = [Language get:@"hour" alter:nil];
        if(minutes > 0)
        {
            CGRect frame = self.sliderValueView.frame;
            //{{217, 0}, {80, 40}}
//            NSLog(@"frame %@", NSStringFromCGRect(frame));
            frame.size.width = 120;
            frame.origin.x = 177;
            self.sliderValueView.frame = frame;
            [self.sliderValueView setString:[NSString stringWithFormat:@"%d %@ %d %@",hour,strHour,minutes,self.unit]];
        }
        else
        {
            self.sliderValueView.frame = CGRectMake(217, 0, 80, 40);
            [self.sliderValueView setString:[NSString stringWithFormat:@"%d %@",hour,strHour]];
        }
    }
    else
    {
        [self.sliderValueView setString:[NSString stringWithFormat:@"%.0f %@",slider.value,self.unit]];
    }
//    [self.sliderValueView setString:[NSString stringWithFormat:@"%.0f %@",slider.value,self.unit]];
    
    //NSLog(@"slider %i value = %f", slider.tag, slider.value);
    
    if ([self.delegate respondsToSelector:@selector(quotaSliderValueChanged:)]) {
        [self.delegate quotaSliderValueChanged:slider];
    }
}

@end
