//
//  QuotaSliderView.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 6/20/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InternetUsageModel.h"
#import "ASValueTrackingSlider.h"
#import "ASValuePopUpView.h"

@protocol QuotaSliderViewDelegate;

@interface QuotaSliderView : UIView <ASValueTrackingSliderDelegate> {
    ASValuePopUpView *sliderValueView;
    UILabel *sliderValueLabel;
    NSString *unit;
}

@property (nonatomic, retain) ASValuePopUpView *sliderValueView;
@property (nonatomic, retain) UILabel *sliderValueLabel;
@property (nonatomic, retain) NSString *unit;
@property (strong, nonatomic) id<QuotaSliderViewDelegate> delegate;
// TODO : V1.9
// Save value of the last index on the slider to determine the movement (slider is going left or right)
@property (nonatomic) NSInteger lastIndex;
@property (nonatomic) BOOL isQuotaCalculator;

// TODO : V1.9
// Adding isQuotaCalculatorView parameter to keep Quota Calculator in paket sesukamu unchanged
- (id)initWithFrame:(CGRect)frame
           withData:(InternetUsageModel*)data
isQuotaCalculatorView:(BOOL)isQuotaCalculatorView;

@end

@protocol QuotaSliderViewDelegate <NSObject>

@optional
- (void)quotaSliderValueChanged:(ASValueTrackingSlider*)slider;

@end
