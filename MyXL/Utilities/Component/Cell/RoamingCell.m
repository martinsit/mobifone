//
//  RoamingCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "RoamingCell.h"
#import "Constant.h"
#import "DataManager.h"

@implementation RoamingCell

@synthesize titleLabel;
@synthesize descLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        DataManager *sharedData = [DataManager sharedInstance];
        self.backgroundColor = [UIColor clearColor];
        
        titleLabel = [[UILabel alloc] init];
        titleLabel.textAlignment = UITextAlignmentLeft;
        titleLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
        titleLabel.textColor = kDefaultTitleFontGrayColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        descLabel = [[UILabel alloc] init];
        if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
            descLabel.textAlignment = UITextAlignmentRight;
        }
        else {
            descLabel.textAlignment = UITextAlignmentLeft;
        }
        
        descLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontTitleSize];
        descLabel.textColor = kDefaultBlackColor;
        descLabel.backgroundColor = [UIColor clearColor];
        descLabel.numberOfLines = 0;
        descLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        [self.contentView addSubview:titleLabel];
        [self.contentView addSubview:descLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    
    CGFloat labelLeftPadding = 15.0;
    CGFloat height = 20.0;
    
    CGRect frame;
    
    frame = CGRectMake(labelLeftPadding,
                       kDefaultComponentPadding,
                       contentRect.size.width - (labelLeftPadding*2),
                       height);
    titleLabel.frame = frame;
    
    frame = CGRectMake(labelLeftPadding,
                       titleLabel.frame.origin.y + titleLabel.frame.size.height,
                       contentRect.size.width - (labelLeftPadding*2),
                       height*1.5);
    
    descLabel.frame = frame;
    
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.profileData.telcoOperator isEqualToString:@"AXIS"]) {
        [descLabel sizeToFit];
    }
    
    
}

- (void)dealloc
{
    [titleLabel release];
    [descLabel release];
    [super dealloc];
}

@end
