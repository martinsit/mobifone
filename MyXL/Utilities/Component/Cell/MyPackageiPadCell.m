//
//  MyPackageiPadCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/15/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "MyPackageiPadCell.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "Language.h"

@implementation MyPackageiPadCell

@synthesize separatorView;

@synthesize packageValueLabel;

@synthesize validityValueLabel;

@synthesize usageValueLabel;

@synthesize typeValueLabel;

@synthesize stopButton;
@synthesize upgradeButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        separatorView = [[UIView alloc] init];
        separatorView.backgroundColor = kDefaultWhiteSmokeColor;
        
        packageValueLabel = [[UILabel alloc] init];
        packageValueLabel.textAlignment = UITextAlignmentLeft;
        packageValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        packageValueLabel.textColor = kDefaultPurpleColor;
        packageValueLabel.backgroundColor = [UIColor clearColor];
        
        typeValueLabel = [[UILabel alloc] init];
        typeValueLabel.textAlignment = UITextAlignmentLeft;
        typeValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        typeValueLabel.textColor = kDefaultPurpleColor;
        typeValueLabel.backgroundColor = [UIColor clearColor];
        
        usageValueLabel = [[UILabel alloc] init];
        usageValueLabel.textAlignment = UITextAlignmentLeft;
        usageValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        usageValueLabel.textColor = kDefaultPurpleColor;
        usageValueLabel.backgroundColor = [UIColor clearColor];
        
        validityValueLabel = [[UILabel alloc] init];
        validityValueLabel.textAlignment = UITextAlignmentLeft;
        validityValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        validityValueLabel.textColor = kDefaultPurpleColor;
        validityValueLabel.backgroundColor = [UIColor clearColor];
        
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        
        stopButton = createButtonWithFrame(CGRectMake(0, 0, 60, kDefaultButtonHeight),
                                           [Language get:@"stop" alter:nil],
                                           buttonFont,
                                           kDefaultBaseColor,
                                           kDefaultPinkColor,
                                           kDefaultPurpleColor);
        
        upgradeButton = createButtonWithFrame(CGRectMake(0, 0, 60, kDefaultButtonHeight),
                                              [Language get:@"upgrade" alter:nil],
                                              buttonFont,
                                              kDefaultBaseColor,
                                              kDefaultPinkColor,
                                              kDefaultPurpleColor);
        
        [self.contentView addSubview:separatorView];
        
        [self.contentView addSubview:packageValueLabel];
        
        [self.contentView addSubview:validityValueLabel];
        
        [self.contentView addSubview:usageValueLabel];
        
        [self.contentView addSubview:typeValueLabel];
        
        [self.contentView addSubview:stopButton];
        [self.contentView addSubview:upgradeButton];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat leftPadding = 20.0;
    CGFloat topPadding = 5.0;
    CGFloat width = (self.frame.size.width - (45*2))/5;
    CGFloat height = 20.0;
    //CGFloat deduction = 5.0;
    
    CGRect frame;
    
    // Package
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width + (width/2.0),
                       height);
    packageValueLabel.frame = frame;
    
    // Type
    frame = CGRectMake(packageValueLabel.frame.origin.x + packageValueLabel.frame.size.width,
                       topPadding,
                       width - (width/4.0),
                       height);
    typeValueLabel.frame = frame;
    
    // Usage
    frame = CGRectMake(typeValueLabel.frame.origin.x + typeValueLabel.frame.size.width,
                       topPadding,
                       width - (width/4.0),
                       height);
    usageValueLabel.frame = frame;
    
    // Validity
    frame = CGRectMake(usageValueLabel.frame.origin.x + usageValueLabel.frame.size.width,
                       topPadding,
                       width,
                       height);
    validityValueLabel.frame = frame;
    
    CGFloat buttonPadding = 5.0;
    CGFloat tableWidth = 0.0;
    frame = stopButton.frame;
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        tableWidth = self.frame.size.width - (45*2);
    }
    else {
        tableWidth = self.frame.size.width - (10*2);
    }
    
    //frame.origin.x = tableWidth - stopButton.frame.size.width - leftPadding - buttonPadding;
    frame.origin.x = tableWidth - stopButton.frame.size.width - leftPadding;
    //frame.origin.y = validityValueLabel.frame.origin.y + validityValueLabel.frame.size.height + topPadding + buttonPadding;
    frame.origin.y = topPadding;
    stopButton.frame = frame;
    
    frame = upgradeButton.frame;
    frame.origin.x = tableWidth - stopButton.frame.size.width - upgradeButton.frame.size.width - 10 - leftPadding - buttonPadding;
    //frame.origin.y = validityValueLabel.frame.origin.y + validityValueLabel.frame.size.height + topPadding + buttonPadding;
    frame.origin.y = topPadding;
    upgradeButton.frame = frame;
    
    /*
    frame = CGRectMake(leftPadding,
                       validityValueLabel.frame.origin.y + validityValueLabel.frame.size.height + topPadding,
                       tableWidth - (leftPadding*2),
                       kDefaultButtonHeight + (buttonPadding*2));
     */
    frame = CGRectMake(leftPadding,
                       0.0,
                       tableWidth - (leftPadding*2),
                       1.0);
    separatorView.frame = frame;
}

- (void)dealloc
{
    [separatorView release];
    
    [packageValueLabel release];
    
    [validityValueLabel release];
    
    [usageValueLabel release];
    
    [typeValueLabel release];
    
    [stopButton release];
    [upgradeButton release];
    
    [super dealloc];
}

@end
