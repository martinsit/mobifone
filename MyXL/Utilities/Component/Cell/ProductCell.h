//
//  ProductCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 8/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell {
    UIView *borderView;
    UIImageView *imageView;
    UILabel *productNameLabel;
    UILabel *productPriceLabel;
    UIButton *buyButton;
    UIButton *detailButton;
}

@property (nonatomic, retain) UIView *borderView;
@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) UILabel *productNameLabel;
@property (nonatomic, retain) UILabel *productPriceLabel;
@property (nonatomic, retain) UIButton *buyButton;
@property (nonatomic, retain) UIButton *detailButton;

@end
