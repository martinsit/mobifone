//
//  LoadMoreCell.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 1/16/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "LoadMoreCell.h"
#import "Constant.h"

@implementation LoadMoreCell

@synthesize titleLabel;
@synthesize arrowLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        titleLabel = [[UILabel alloc] init];
        titleLabel.textAlignment = UITextAlignmentRight;
        titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
        titleLabel.textColor = kDefaultWhiteColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        arrowLabel = [[UILabel alloc] init];
        arrowLabel.textAlignment = UITextAlignmentRight;
        arrowLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize];
        arrowLabel.textColor = kDefaultWhiteColor;
        arrowLabel.backgroundColor = [UIColor clearColor];
        arrowLabel.numberOfLines = 0;
        arrowLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        [self.contentView addSubview:titleLabel];
        [self.contentView addSubview:arrowLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    
    CGFloat labelLeftPadding = 15.0;
    CGFloat height = 20.0;
    
    CGRect frame;
    
    frame = CGRectMake(contentRect.size.width - labelLeftPadding - height,
                       (contentRect.size.height - height)/2,
                       height,
                       height);
    arrowLabel.frame = frame;
    
    frame = CGRectMake(arrowLabel.frame.origin.x - 200.0,
                       arrowLabel.frame.origin.y,
                       200.0,
                       height);
    titleLabel.frame = frame;
}

- (void)dealloc
{
    [titleLabel release];
    [arrowLabel release];
    [super dealloc];
}

@end
