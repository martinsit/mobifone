//
//  LeftCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/27/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftCell : UITableViewCell {
    UILabel *iconLabel;
    UILabel *titleLabel;
    UILabel *arrowLabel;
}

@property (nonatomic, retain) UILabel *iconLabel;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *arrowLabel;

@end
