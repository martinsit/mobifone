//
//  AXISboxAccountCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/18/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "AXISboxAccountCell.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "Language.h"

@implementation AXISboxAccountCell

@synthesize accountTitleLabel;
@synthesize accountValueLabel;

@synthesize usageTitleLabel;
@synthesize usageValueLabel;

@synthesize quotaTitleLabel;
@synthesize quotaValueLabel;

@synthesize changeButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        accountTitleLabel = [[UILabel alloc] init];
        accountTitleLabel.textAlignment = UITextAlignmentLeft;
        accountTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        accountTitleLabel.textColor = kDefaultTitleFontGrayColor;
        accountTitleLabel.backgroundColor = [UIColor clearColor];
        
        accountValueLabel = [[UILabel alloc] init];
        accountValueLabel.textAlignment = UITextAlignmentLeft;
        accountValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        accountValueLabel.textColor = kDefaultPurpleColor;
        accountValueLabel.backgroundColor = [UIColor clearColor];
        accountValueLabel.numberOfLines = 0;
        accountValueLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        usageTitleLabel = [[UILabel alloc] init];
        usageTitleLabel.textAlignment = UITextAlignmentLeft;
        usageTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        usageTitleLabel.textColor = kDefaultTitleFontGrayColor;
        usageTitleLabel.backgroundColor = [UIColor clearColor];
        
        usageValueLabel = [[UILabel alloc] init];
        usageValueLabel.textAlignment = UITextAlignmentLeft;
        usageValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        usageValueLabel.textColor = kDefaultPurpleColor;
        usageValueLabel.backgroundColor = [UIColor clearColor];
        
        quotaTitleLabel = [[UILabel alloc] init];
        quotaTitleLabel.textAlignment = UITextAlignmentLeft;
        quotaTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        quotaTitleLabel.textColor = kDefaultTitleFontGrayColor;
        quotaTitleLabel.backgroundColor = [UIColor clearColor];
        
        quotaValueLabel = [[UILabel alloc] init];
        quotaValueLabel.textAlignment = UITextAlignmentLeft;
        quotaValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        quotaValueLabel.textColor = kDefaultPurpleColor;
        quotaValueLabel.backgroundColor = [UIColor clearColor];
        
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        
        changeButton = createButtonWithFrame(CGRectMake(0, 0, 120, kDefaultButtonHeight),
                                             [Language get:@"stop" alter:nil],
                                             buttonFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
        
        [self.contentView addSubview:accountTitleLabel];
        [self.contentView addSubview:accountValueLabel];
        
        [self.contentView addSubview:usageTitleLabel];
        [self.contentView addSubview:usageValueLabel];
        
        [self.contentView addSubview:quotaTitleLabel];
        [self.contentView addSubview:quotaValueLabel];
        
        [self.contentView addSubview:changeButton];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    //CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 20.0;
    CGFloat topPadding = 5.0;
    
    CGFloat width = 0.0;
    if (IS_IPAD) {
        width = (self.frame.size.width - (45*2) - (leftPadding*2))/2.0;
    }
    else {
        width = (self.frame.size.width - (10*2) - (leftPadding*2))/2.0;
    }
    
    CGFloat height = 20.0;
    CGFloat deduction = 5.0;
    
    CGRect frame;
    
    // Left Side
    // Package
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width,
                       height);
    accountTitleLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       accountTitleLabel.frame.origin.y + accountTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    accountValueLabel.frame = frame;
    [accountValueLabel sizeToFit];
    
    // Quota
    frame = CGRectMake(leftPadding,
                       accountValueLabel.frame.origin.y + accountValueLabel.frame.size.height + topPadding,
                       width,
                       height);
    quotaTitleLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       quotaTitleLabel.frame.origin.y + quotaTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    quotaValueLabel.frame = frame;
    
    // Right Side
    // Usage
    CGFloat rightSideTextPadding = accountTitleLabel.frame.origin.x + accountTitleLabel.frame.size.width;
    frame = CGRectMake(rightSideTextPadding,
                       accountTitleLabel.frame.origin.y,
                       width,
                       height);
    usageTitleLabel.frame = frame;
    
    frame = CGRectMake(rightSideTextPadding,
                       accountValueLabel.frame.origin.y,
                       width,
                       height);
    usageValueLabel.frame = frame;
    [usageValueLabel sizeToFit];
    
    //CGFloat buttonPadding = 5.0;
    CGFloat tableWidth = 0.0;
    frame = changeButton.frame;
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        tableWidth = self.frame.size.width - (45*2);
    }
    else {
        tableWidth = self.frame.size.width - (10*2);
    }
    
    frame.origin.x = tableWidth - changeButton.frame.size.width - leftPadding;
    frame.origin.y = quotaTitleLabel.frame.origin.y;
    changeButton.frame = frame;
}

- (void)dealloc
{
    [accountTitleLabel release];
    [accountValueLabel release];
    
    [usageTitleLabel release];
    [usageValueLabel release];
    
    [quotaTitleLabel release];
    [quotaValueLabel release];
    
    [changeButton release];
    
    [super dealloc];
}

@end
