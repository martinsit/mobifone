//
//  CommonCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/8/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CommonCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Constant.h"
#import "AXISnetCommon.h"

@implementation CommonCell

@synthesize iconLabel;
@synthesize titleLabel;
@synthesize arrowLabel;
@synthesize lineView;
@synthesize refreshButton;
@synthesize withRefreshButton = _withRefreshButton;

@synthesize descLabel;
@synthesize infoButton;
@synthesize withInfoButton = _withInfoButton;
@synthesize needResize = _needResize;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        iconLabel = [[UILabel alloc] init];
        iconLabel.textAlignment = UITextAlignmentCenter;
        iconLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:18.0];
        iconLabel.textColor = kDefaultNavyBlueColor;
        iconLabel.backgroundColor = kDefaultYellowColor;
        
        titleLabel = [[UILabel alloc] init];
        titleLabel.textAlignment = UITextAlignmentLeft;
        titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
        titleLabel.textColor = kDefaultWhiteColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        // Cell With Description
        descLabel = [[UILabel alloc] init];
        descLabel.textAlignment = UITextAlignmentLeft;
        descLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
        descLabel.textColor = kDefaultBlackColor;
        descLabel.backgroundColor = [UIColor clearColor];
        descLabel.numberOfLines = 0;
        descLabel.lineBreakMode = UILineBreakModeWordWrap;
        //----------------------
        
        arrowLabel = [[UILabel alloc] init];
        arrowLabel.textAlignment = UITextAlignmentLeft;
        arrowLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:18.0];
        arrowLabel.textColor = kDefaultWhiteColor;
        arrowLabel.backgroundColor = [UIColor clearColor];
        arrowLabel.text = @"d";
        
        lineView = [[UIView alloc] initWithFrame:CGRectZero];
        lineView.backgroundColor = kDefaultNavyBlueColor;
        
        refreshButton = createButtonWithFrame(CGRectMake(0.0, 0.0, kDefaultButtonHeight, kDefaultButtonHeight),
                                              @"}",
                                              [UIFont fontWithName:kDefaultFontKSAN size:17.0],
                                              kDefaultNavyBlueColor,
                                              kDefaultYellowColor,
                                              kDefaultYellowColor);
        
        infoButton = createButtonWithFrame(CGRectMake(0.0, 0.0, kDefaultButtonHeight, kDefaultButtonHeight),
                                           @"X",
                                           [UIFont fontWithName:kDefaultFontKSAN size:17.0],
                                           kDefaultSeparatorColor,
                                           [UIColor clearColor],
                                           [UIColor clearColor]);
        [infoButton setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        
        [self.contentView addSubview:iconLabel];
        [self.contentView addSubview:titleLabel];
        [self.contentView addSubview:arrowLabel];
        [self.contentView addSubview:lineView];
        [self.contentView addSubview:refreshButton];
        
        [self.contentView addSubview:descLabel];
        [self.contentView addSubview:infoButton];
        //[descLabelTmp release];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    
    //CGFloat topPadding = 0.0;
    CGFloat labelLeftPadding = 15.0; //25
    CGFloat width = 30.0;
    CGFloat height = 30.0; // 28
    
    CGFloat titleY = 6.0;
    
    CGRect frame;
    
    if ([iconLabel.text length] > 0) {
        
        iconLabel.hidden = NO;
        
        frame = CGRectMake(labelLeftPadding, (contentRect.size.height - height)/2, width, height);
        iconLabel.frame = frame;
        iconLabel.layer.cornerRadius = kDefaultCornerRadius;
        iconLabel.layer.masksToBounds = YES;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            frame = CGRectMake(iconLabel.frame.origin.x + iconLabel.frame.size.width + 8.0,
                               (contentRect.size.height - height)/2,
                               contentRect.size.width - (labelLeftPadding) - (width*2) - 8.0,
                               height);
        }
        else {
            frame = CGRectMake(iconLabel.frame.origin.x + iconLabel.frame.size.width + 8.0,
                               (contentRect.size.height - height)/2,
                               contentRect.size.width - (labelLeftPadding) - (width*2) - 8.0,
                               height + 4.0);
        }
        
        titleLabel.frame = frame;
        //titleLabel.backgroundColor = [UIColor redColor];
        
        if (_withRefreshButton) {
            frame = CGRectMake(contentRect.size.width - width -  15.0,
                               (contentRect.size.height - height)/2,
                               width,
                               height);
            refreshButton.frame = frame;
        }
        else {
            frame = CGRectMake(titleLabel.frame.origin.x + titleLabel.frame.size.width,
                               (contentRect.size.height - height)/2,
                               width,
                               height);
            arrowLabel.frame = frame;
        }
        
        /*
         * Info Desc
         */
        frame = CGRectMake(arrowLabel.frame.origin.x - width,
                           (contentRect.size.height - height)/2,
                           width,
                           height);
        infoButton.frame = frame;
        
//        if (_needResize) {
//            [titleLabel sizeToFit];
//        }
        
        frame = CGRectMake(titleLabel.frame.origin.x,
                           titleLabel.frame.origin.y + titleLabel.frame.size.height,
                           titleLabel.frame.size.width,
                           height);
        descLabel.frame = frame;
    }
    else {
        
        iconLabel.hidden = YES;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            frame = CGRectMake(labelLeftPadding,
                               titleY, //(contentRect.size.height - height)/2
                               contentRect.size.width - ((labelLeftPadding*2) + (width*2)) + labelLeftPadding,
                               height);
        }
        else {
            frame = CGRectMake(labelLeftPadding,
                               titleY, //(contentRect.size.height - height)/2
                               contentRect.size.width - ((labelLeftPadding*2) + (width*2)) + labelLeftPadding,
                               height + 4.0);
        }
        
        //titleLabel.frame = frame;
        CGSize lblSize = calculateExpectedSize(titleLabel, titleLabel.text);
        lblSize.height += 8;
        frame.size.height = lblSize.height;
        frame.origin.y = (contentRect.size.height - frame.size.height)/2;
        titleLabel.frame = CGRectIntegral(frame);
        
        //titleLabel.frame.origin.x + titleLabel.frame.size.width
        frame = CGRectMake(contentRect.size.width - (width),
                           (contentRect.size.height - height)/2,
                           width,
                           height);
        arrowLabel.frame = frame;
        
        /*
         * Info Desc
         */
        frame = CGRectMake(arrowLabel.frame.origin.x - width,
                           (contentRect.size.height - height)/2,
                           width,
                           height);
        infoButton.frame = frame;
        
        //        if (_needResize) {
        //            [titleLabel sizeToFit];
        //        }
        
        frame = CGRectMake(titleLabel.frame.origin.x,
                           titleLabel.frame.origin.y + titleLabel.frame.size.height,
                           titleLabel.frame.size.width,
                           height);
        descLabel.frame = frame;
    }
    [descLabel sizeToFit];
    
    frame = CGRectMake(titleLabel.frame.origin.x,
                       contentRect.size.height - 1,
                       contentRect.size.width - titleLabel.frame.origin.x,
                       0.5);
    lineView.frame = frame;
}

- (void)dealloc
{
    [iconLabel release];
    [titleLabel release];
    [arrowLabel release];
    [lineView release];
    [refreshButton release];
    
    [descLabel release];
    [infoButton release];
    [super dealloc];
}

@end
