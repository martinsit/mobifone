//
//  RectBackground.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 11/20/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RectBackground : UIView {
    UIColor *rectColor;
    UIColor *rectColorSelected;
    BOOL _selected;
}

@property (nonatomic, retain) UIColor *rectColor;
@property (nonatomic, retain) UIColor *rectColorSelected;
@property  BOOL selected;

- (id)initWithFrame:(CGRect)frame withBasicColor:(UIColor*)basicColor withSelectedColor:(UIColor*)selectedColor;

@end
