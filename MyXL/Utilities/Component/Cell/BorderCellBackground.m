//
//  BorderCellBackground.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BorderCellBackground.h"
#import "Constant.h"
#import "AXISnetCommon.h"

@implementation BorderCellBackground

@synthesize strokeColor;
@synthesize strokeWidth;
@synthesize grayBackgroundColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = NO;
        //self.strokeColor = kDefaultBlueColor;
        self.strokeColor = kDefaultWhiteColor;
        self.strokeWidth = kDefaultStrokeWidth;
        self.grayBackgroundColor = kDefaultWhiteColor;
        [super setBackgroundColor:[UIColor clearColor]];
        //[super setBackgroundColor:kDefaultButtonGrayColor];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withBorderColor:(UIColor*)borderColor {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = NO;
        //self.strokeColor = kDefaultBlueColor;
        self.strokeColor = borderColor;
        self.strokeWidth = kDefaultStrokeWidth;
        self.grayBackgroundColor = kDefaultWhiteColor;
        [super setBackgroundColor:[UIColor clearColor]];
        //[super setBackgroundColor:kDefaultButtonGrayColor];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Add in color section
    CGColorRef lineColor = self.strokeColor.CGColor;
    
    // Add line at left
    CGFloat leftPaddingLine = 10.0;
    CGPoint startPoint = CGPointMake(leftPaddingLine + 1, self.bounds.origin.y);
    CGPoint endPoint = CGPointMake(leftPaddingLine + 1, self.bounds.size.height);
    draw1PxStroke(context, startPoint, endPoint, lineColor);
    
    // Add White Background
    CGFloat leftPadding = 11.0;
    CGFloat width = self.bounds.size.width - (leftPaddingLine*2) - (1*2);
    CGRect leftBackgroundRect = CGRectMake(leftPadding + 1, self.bounds.origin.y, width, self.bounds.size.height);
    addBackground(context, leftBackgroundRect, [UIColor whiteColor].CGColor);
    
    // Add line at right
    startPoint = CGPointMake(self.bounds.size.width - leftPaddingLine - (1*2), self.bounds.origin.y);
    endPoint = CGPointMake(self.bounds.size.width - leftPaddingLine - (1*2), self.bounds.size.height);
    draw1PxStroke(context, startPoint, endPoint, lineColor);
    
    // Left Background
    
    leftPadding = 0.0;
    width = 10.0;
    
    leftBackgroundRect = CGRectMake(leftPadding + 1, self.bounds.origin.y, width, self.bounds.size.height);
    addBackground(context, leftBackgroundRect, self.grayBackgroundColor.CGColor);
    
    // Right Background
    leftPadding = self.bounds.size.width - width;
    CGRect rightBackgroundRect = CGRectMake(leftPadding - 1, self.bounds.origin.y, width, self.bounds.size.height);
    addBackground(context, rightBackgroundRect, self.grayBackgroundColor.CGColor);
}

- (void)dealloc {
    [strokeColor release];
    [grayBackgroundColor release];
    [super dealloc];
}

@end
