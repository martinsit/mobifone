//
//  XLPackageButtonCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XLPackageButtonCell.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "Language.h"

@implementation XLPackageButtonCell

@synthesize stopButton;
@synthesize shareQuotaButton;
@synthesize quotaMeterButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontTitleSize];
        stopButton = createButtonWithFrame(CGRectMake(0, 0, 60, kDefaultButtonHeight),
                                           [[Language get:@"stop" alter:nil] uppercaseString],
                                           buttonFont,
                                           kDefaultWhiteColor,
                                           kDefaultAquaMarineColor,
                                           kDefaultBlueColor);
        [self.contentView addSubview:stopButton];
        
        shareQuotaButton = createButtonWithFrame(CGRectMake(0, 0, 100, kDefaultButtonHeight), //100
                                                 [[Language get:@"share_quota" alter:nil] uppercaseString],
                                                 buttonFont,
                                                 kDefaultWhiteColor,
                                                 kDefaultAquaMarineColor,
                                                 kDefaultBlueColor);
        [self.contentView addSubview:shareQuotaButton];
        
        quotaMeterButton = createButtonWithFrame(CGRectMake(0, 0, 110, kDefaultButtonHeight), //120
                                                 [[Language get:@"quota_meter" alter:nil] uppercaseString],
                                                 buttonFont,
                                                 kDefaultWhiteColor,
                                                 kDefaultAquaMarineColor,
                                                 kDefaultBlueColor);
        [self.contentView addSubview:quotaMeterButton];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    CGFloat labelLeftPadding = 15.0;
    
    CGRect frame;
    
    frame = stopButton.frame;
    frame.origin.x = labelLeftPadding;
    frame.origin.y = (contentRect.size.height - kDefaultButtonHeight)/2;
    stopButton.frame = frame;
    
    frame = shareQuotaButton.frame;
    frame.origin.x = stopButton.frame.origin.x + stopButton.frame.size.width + 5;
    frame.origin.y = (contentRect.size.height - kDefaultButtonHeight)/2;
    shareQuotaButton.frame = frame;
    
    frame = quotaMeterButton.frame;
    frame.origin.x = contentRect.size.width - labelLeftPadding - quotaMeterButton.frame.size.width;
    frame.origin.y = (contentRect.size.height - kDefaultButtonHeight)/2;
    quotaMeterButton.frame = frame;
}

- (void)dealloc
{
    [stopButton release];
    [shareQuotaButton release];
    [quotaMeterButton release];
    [super dealloc];
}

@end
