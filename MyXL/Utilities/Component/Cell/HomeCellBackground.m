//
//  HomeCellBackground.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "HomeCellBackground.h"
#import "AXISnetCommon.h"
#import "Constant.h"

@implementation HomeCellBackground

@synthesize rectColor;
@synthesize rectColorSelected;
@synthesize cornerRadius;
@synthesize selected = _selected;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = NO;
        self.rectColor = kDefaultPurpleColor;
        self.rectColorSelected = kDefaultPinkColor;
        self.cornerRadius = kDefaultCornerRadius;
        [super setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [super drawRect:rect];
    
    // Drawing code
    
    //NSLog(@"rect.size.height = %f",rect.size.height);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat leftPadding = 20.0;
    CGFloat topPadding = 2.0;
    
    CGRect backgroundRect = CGRectMake(leftPadding, topPadding, rect.size.width - (leftPadding*2), rect.size.height - (topPadding*2));
    
    //CGContextClearRect(context, rect);
    
    if (_selected) {
        addRoundedRect(context, backgroundRect, cornerRadius, self.rectColorSelected.CGColor);
    }
    else {
        addRoundedRect(context, backgroundRect, cornerRadius, self.rectColor.CGColor);
    }
}

- (void)dealloc {
    [rectColor release];
    [rectColorSelected release];
    [super dealloc];
}

@end
