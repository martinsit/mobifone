//
//  XLPackageBarCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XLPackageBarCell : UITableViewCell {
    UIView *barView;
}

@property (nonatomic, retain) UIView *barView;

@end
