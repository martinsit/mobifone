//
//  MyPackageTitleiPadCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/15/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "MyPackageTitleiPadCell.h"
#import "Constant.h"

@implementation MyPackageTitleiPadCell

@synthesize packageTitleLabel;
@synthesize typeTitleLabel;
@synthesize usageTitleLabel;
@synthesize validityTitleLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        packageTitleLabel = [[UILabel alloc] init];
        packageTitleLabel.textAlignment = UITextAlignmentLeft;
        packageTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        packageTitleLabel.textColor = kDefaultTitleFontGrayColor;
        packageTitleLabel.backgroundColor = [UIColor clearColor];
        
        typeTitleLabel = [[UILabel alloc] init];
        typeTitleLabel.textAlignment = UITextAlignmentLeft;
        typeTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        typeTitleLabel.textColor = kDefaultTitleFontGrayColor;
        typeTitleLabel.backgroundColor = [UIColor clearColor];
        
        usageTitleLabel = [[UILabel alloc] init];
        usageTitleLabel.textAlignment = UITextAlignmentLeft;
        usageTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        usageTitleLabel.textColor = kDefaultTitleFontGrayColor;
        usageTitleLabel.backgroundColor = [UIColor clearColor];
        
        validityTitleLabel = [[UILabel alloc] init];
        validityTitleLabel.textAlignment = UITextAlignmentLeft;
        validityTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        validityTitleLabel.textColor = kDefaultTitleFontGrayColor;
        validityTitleLabel.backgroundColor = [UIColor clearColor];
        
        [self.contentView addSubview:packageTitleLabel];
        [self.contentView addSubview:typeTitleLabel];
        [self.contentView addSubview:usageTitleLabel];
        [self.contentView addSubview:validityTitleLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat leftPadding = 20.0;
    CGFloat topPadding = 5.0;
    CGFloat width = (self.frame.size.width - (45*2))/5;
    
    CGFloat height = 20.0;
    CGRect frame;
    
    // Package
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width + (width/2.0),
                       height);
    packageTitleLabel.frame = frame;
    
    // Type
//    CGFloat rightSideTextPadding = self.frame.size.width/2;
    leftPadding = packageTitleLabel.frame.origin.x + packageTitleLabel.frame.size.width;
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width - (width/4.0),
                       height);
    typeTitleLabel.frame = frame;
    
    // Usage
    leftPadding = typeTitleLabel.frame.origin.x + typeTitleLabel.frame.size.width;
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width - (width/4.0),
                       height);
    usageTitleLabel.frame = frame;
    
    // Validity
    leftPadding = usageTitleLabel.frame.origin.x + usageTitleLabel.frame.size.width;
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width,
                       height);
    validityTitleLabel.frame = frame;
}

- (void)dealloc
{
    [packageTitleLabel release];
    [typeTitleLabel release];
    [usageTitleLabel release];
    [validityTitleLabel release];
    [super dealloc];
}

@end
