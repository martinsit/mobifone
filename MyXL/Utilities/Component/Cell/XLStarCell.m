//
//  XLStarCell.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 1/9/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XLStarCell.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import <QuartzCore/QuartzCore.h>

@implementation XLStarCell

@synthesize imageView;
@synthesize descLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        imageView = [[UIImageView alloc] init];
        imageView.image = [UIImage imageNamed:@"xlstar"];
        imageView.contentMode = UIViewContentModeCenter;
        
        descLabel = [[UILabel alloc] init];
        descLabel.textAlignment = UITextAlignmentLeft;
        descLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        descLabel.textColor = kDefaultTitleFontGrayColor;
        descLabel.backgroundColor = [UIColor clearColor];
        descLabel.numberOfLines = 0;
        descLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        [self.contentView addSubview:imageView];
        [self.contentView addSubview:descLabel];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 15.0;
    CGRect frame;
    
    // UIImageView
    frame = CGRectMake(leftPadding,
                       10.0,
                       contentRect.size.width - (leftPadding*2),
                       80.0);
    imageView.frame = frame;
    
    // Desc Label
    frame = CGRectMake(leftPadding,
                       imageView.frame.origin.y + imageView.frame.size.height + kDefaultComponentPadding,
                       contentRect.size.width - (leftPadding*2),
                       70.0);
    descLabel.frame = frame;
}

- (void)dealloc
{
    [imageView release];
    [descLabel release];
    [super dealloc];
}

@end
