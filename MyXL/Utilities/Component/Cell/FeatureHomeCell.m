//
//  FeatureHomeCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 11/27/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "FeatureHomeCell.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "DataManager.h"

@implementation FeatureHomeCell

@synthesize button1;
@synthesize button2;
@synthesize isTwoButtons = _isTwoButtons;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        DataManager *dataManager = [DataManager sharedInstance];
        
        UIFont *iconFont = [UIFont fontWithName:kDefaultFontKSAN size:18.0];
        UIFont *titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
        /*
        button1 = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, 320/2, 50.0)
                                               icon:@""
                                           iconFont:iconFont
                                          iconColor:kDefaultNavyBlueColor
                                              title:@""
                                          titleFont:titleFont
                                         titleColor:kDefaultNavyBlueColor
                                            bgColor:kDefaultAquaMarineColor
                                      selectedColor:kDefaultAquaMarineColor];
        
        button2 = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, 320/2, 50.0)
                                               icon:@""
                                           iconFont:iconFont
                                          iconColor:kDefaultNavyBlueColor
                                              title:@""
                                          titleFont:titleFont
                                         titleColor:kDefaultNavyBlueColor
                                            bgColor:kDefaultAquaMarineColor
                                      selectedColor:kDefaultAquaMarineColor];*/
        
        
        button1 = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, 320/2, 50.0)
                                               icon:@""
                                           iconFont:iconFont
                                          iconColor:colorWithHexString(dataManager.profileData.themesModel.menuColor)
                                              title:@""
                                          titleFont:titleFont
                                         titleColor:colorWithHexString(dataManager.profileData.themesModel.menuFeatureTxt)
                                            bgColor:colorWithHexString(dataManager.profileData.themesModel.menuFeature)
                                      selectedColor:colorWithHexString(dataManager.profileData.themesModel.menuFeature)];
        
        button2 = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, 320/2, 50.0)
                                               icon:@""
                                           iconFont:iconFont
                                          iconColor:colorWithHexString(dataManager.profileData.themesModel.menuColor)
                                              title:@""
                                          titleFont:titleFont
                                         titleColor:colorWithHexString(dataManager.profileData.themesModel.menuFeatureTxt)
                                            bgColor:colorWithHexString(dataManager.profileData.themesModel.menuFeature)
                                      selectedColor:colorWithHexString(dataManager.profileData.themesModel.menuFeature)];
        
        [self.contentView addSubview:button1];
        [self.contentView addSubview:button2];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect contentRect = self.contentView.bounds;
    
    CGRect frame;
    
    if (_isTwoButtons) {
        frame = button1.frame;
        frame.origin.x = contentRect.origin.x;
        frame.origin.y = contentRect.origin.y;
        frame.size.width = (contentRect.size.width/2) - 1;
        frame.size.height = contentRect.size.height;
        button1.frame = frame;
        // TODO : NEPTUNE
//        if(CGRectGetWidth(button1.titleLbl.frame) > CGRectGetWidth(button1.frame))
//        {
        CGRect lblFrame = button1.titleLbl.frame;
        lblFrame.size.width = CGRectGetWidth(button1.frame) - CGRectGetMaxX(button1.iconLbl.frame) - 16;
        button1.titleLbl.frame = lblFrame;
//        }
        //NSLog(@"label frame %@", NSStringFromCGRect(button1.titleLbl.frame));
        
        frame = button2.frame;
        frame.origin.x = (contentRect.size.width/2) + 1;
        frame.origin.y = contentRect.origin.y;
        frame.size.width = (contentRect.size.width/2) - 1;
        frame.size.height = contentRect.size.height;
        button2.frame = frame;
        
        // TODO : NEPTUNE
//        if(IS_IPAD)
//        {
            lblFrame = button2.titleLbl.frame;
            lblFrame.size.width = CGRectGetWidth(button2.frame) - CGRectGetMaxX(button2.iconLbl.frame) - 16;
            button2.titleLbl.frame = lblFrame;
//        }
        
        //NSLog(@"label 2 frame %@", NSStringFromCGRect(button2.titleLbl.frame));
        
        button2.hidden = NO;
    }
    else {
        frame = CGRectMake(0.0, 0.0, contentRect.size.width, contentRect.size.height);
        button1.frame = frame;
        for (UILabel *label in [button1 subviews])
        {
            if (label.tag == 10) {
                frame = label.frame;
                frame.size.width = 250;
                label.frame = frame;
            }
        }
        button2.hidden = YES;
    }
}

- (void)dealloc
{
    [button1 release];
    [button1 release];
    [super dealloc];
}

@end
