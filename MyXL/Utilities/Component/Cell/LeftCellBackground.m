//
//  LeftCellBackground.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/27/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "LeftCellBackground.h"
#import "AXISnetCommon.h"
#import "Constant.h"

@implementation LeftCellBackground

@synthesize rectColor;
@synthesize rectColorSelected;
//@synthesize cornerRadius;
@synthesize selected = _selected;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = NO;
//        self.rectColor = leftMenuColor;
//        self.rectColorSelected = leftMenuSelectedColor;
        
        self.rectColor = [UIColor clearColor];
        self.rectColorSelected = kDefaultAquaMarineColor;
        
        //self.cornerRadius = kDefaultCornerRadius;
        [super setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat leftPadding = 0.0;
    CGFloat topPadding = 0.0;
    
    CGRect backgroundRect = CGRectMake(leftPadding, topPadding, rect.size.width - (leftPadding*2), rect.size.height - (topPadding*2));
    
    //CGContextClearRect(context, rect);
    
    if (_selected) {
        //addRoundedRect(context, backgroundRect, cornerRadius, self.rectColorSelected.CGColor);
        
        addBackground(context, backgroundRect, self.rectColorSelected.CGColor);
    }
    else {
        //addRoundedRect(context, backgroundRect, cornerRadius, self.rectColor.CGColor);
        
        addBackground(context, backgroundRect, self.rectColor.CGColor);
    }
    
    
    //CGColorRef lineColor = [UIColor colorWithRed:36.0/255.0 green:36.0/255.0 blue:36.0/255.0 alpha:1.0].CGColor;
    //CGColorRef lineColor = kDefaultFontLightGrayColor.CGColor;
    CGColorRef lineColor = kDefaultWhiteColor.CGColor;
    
    // Add 1st line at bottom
    CGPoint startPoint = CGPointMake(self.bounds.origin.x, self.bounds.origin.y + self.bounds.size.height - 1);
    CGPoint endPoint = CGPointMake(self.bounds.origin.x + self.bounds.size.width,
                                   self.bounds.origin.y + self.bounds.size.height - 1);
    
    draw1PxStroke(context, startPoint, endPoint, lineColor);
    
    // Add 2nd line at bottom //36
    /*
    lineColor = [UIColor colorWithRed:80.0/255.0 green:80.0/255.0 blue:80.0/255.0 alpha:1.0].CGColor;
    startPoint = CGPointMake(self.bounds.origin.x, self.bounds.origin.y + self.bounds.size.height - 1);
    endPoint = CGPointMake(self.bounds.origin.x + self.bounds.size.width,
                           self.bounds.origin.y + self.bounds.size.height - 1);
    
    draw1PxStroke(context, startPoint, endPoint, lineColor);*/
}

- (void)dealloc {
    [rectColor release];
    [rectColorSelected release];
    [super dealloc];
}

@end
