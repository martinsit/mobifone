//
//  ContactUsCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUsCell : UITableViewCell {
    UILabel *titleLabel;
    UILabel *valueLabel;
}

@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *valueLabel;

@end
