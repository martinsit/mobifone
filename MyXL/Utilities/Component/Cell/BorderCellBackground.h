//
//  BorderCellBackground.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BorderCellBackground : UIView {
    UIColor *strokeColor;
    CGFloat strokeWidth;
    UIColor *grayBackgroundColor;
}

@property (nonatomic, retain) UIColor *strokeColor;
@property CGFloat strokeWidth;
@property (nonatomic, retain) UIColor *grayBackgroundColor;

- (id)initWithFrame:(CGRect)frame withBorderColor:(UIColor*)borderColor;

@end
