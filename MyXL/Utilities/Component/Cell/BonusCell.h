//
//  BonusCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/16/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BonusCell : UITableViewCell {
    UILabel *titleLabel;
    UILabel *valueLabel;
    UILabel *validityLabel;
    UILabel *validityValueLabel;
}

@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *valueLabel;
@property (nonatomic, retain) UILabel *validityLabel;
@property (nonatomic, retain) UILabel *validityValueLabel;

@end
