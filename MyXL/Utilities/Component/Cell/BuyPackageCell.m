//
//  BuyPackageCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/2/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BuyPackageCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Constant.h"
#import "AXISnetCommon.h"
#import "Language.h"

@implementation BuyPackageCell

@synthesize titleLabel;
@synthesize priceLabel;
@synthesize descLabel;

@synthesize infoButton;
@synthesize activateButton;

@synthesize lineView;

@synthesize needResize = _needResize;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        titleLabel = [[UILabel alloc] init];
        titleLabel.textAlignment = UITextAlignmentLeft;
        titleLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontTitleSize];
        titleLabel.textColor = kDefaultBlackColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        priceLabel = [[UILabel alloc] init];
        priceLabel.textAlignment = UITextAlignmentLeft;
        priceLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontValueSize];
        priceLabel.textColor = kDefaultOrangeColor;
        priceLabel.backgroundColor = [UIColor clearColor];
        priceLabel.numberOfLines = 0;
        priceLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        UILabel *descLabelTmp = [[UILabel alloc] init];
        descLabelTmp.textAlignment = UITextAlignmentLeft;
        if (IS_IPAD) {
            descLabelTmp.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        }
        else {
            descLabelTmp.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        }
        
        descLabelTmp.textColor = kDefaultBlackColor;
        descLabelTmp.backgroundColor = [UIColor clearColor];
        descLabelTmp.numberOfLines = 0;
        descLabelTmp.lineBreakMode = UILineBreakModeWordWrap;
        descLabel = descLabelTmp;
        
        infoButton = createButtonWithFrame(CGRectMake(0, 0, 60, kDefaultButtonHeight),
                                           @"X",
                                           [UIFont fontWithName:kDefaultFontKSAN size:17.0],
                                           kDefaultBaseColor,
                                           [UIColor clearColor],
                                           [UIColor clearColor]);
        [infoButton setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSmallSize];
        activateButton = createButtonWithFrame(CGRectMake(0, 0, 60, kDefaultButtonHeight),
                                               [[Language get:@"activate" alter:nil] uppercaseString],
                                               buttonFont,
                                               kDefaultWhiteColor,
                                               kDefaultAquaMarineColor,
                                               kDefaultBlueColor);
        [activateButton setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        
        lineView = [[UIView alloc] initWithFrame:CGRectZero];
        lineView.backgroundColor = kDefaultNavyBlueColor;
        
        [self.contentView addSubview:titleLabel];
        [self.contentView addSubview:priceLabel];
        [self.contentView addSubview:descLabel];
        
        [self.contentView addSubview:infoButton];
        [self.contentView addSubview:activateButton];
        
        [self.contentView addSubview:lineView];
        [descLabelTmp release];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    
    CGFloat topPadding = 5.0;
    CGFloat labelLeftPadding = 15.0;
    
    CGFloat height = 20.0;
    
    CGRect frame;
    
    frame = CGRectMake(labelLeftPadding,
                       topPadding,
                       contentRect.size.width - ((labelLeftPadding*2) + activateButton.frame.size.width),
                       height);
    titleLabel.frame = frame;
    if ([titleLabel.text length] > 29) {
        [titleLabel sizeToFit];
    }
    
    frame = CGRectMake(labelLeftPadding,
                       titleLabel.frame.origin.y + titleLabel.frame.size.height,
                       titleLabel.frame.size.width,
                       height);
    priceLabel.frame = frame;
    
    frame = CGRectMake(labelLeftPadding,
                       priceLabel.frame.origin.y + priceLabel.frame.size.height,
                       priceLabel.frame.size.width,
                       height);
    // TODO : V1.9
    // Adjust the label frame height for long text
    descLabel.frame = frame;
    CGSize lblSize = calculateExpectedSize(descLabel, descLabel.text);
    lblSize.height += 8;
    frame.size = lblSize;
    descLabel.frame = CGRectIntegral(frame);
    NSLog(@"desc label frame on cell %@ and text %@", NSStringFromCGRect(frame), descLabel.text);
    
    frame = activateButton.frame;
    frame.origin.x = contentRect.size.width - labelLeftPadding - activateButton.frame.size.width;
    frame.origin.y = (contentRect.size.height - frame.size.height)/2.0;
    activateButton.frame = frame;
    
    frame = CGRectMake(titleLabel.frame.origin.x,
                       contentRect.size.height - 1.0,
                       contentRect.size.width - titleLabel.frame.origin.x,
                       0.5);
    lineView.frame = frame;
}

- (void)dealloc
{
    [titleLabel release];
    [priceLabel release];
    [descLabel release];
    [infoButton release];
    [activateButton release];
    [lineView release];
    [super dealloc];
}

@end
