//
//  AXISboxAccountCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/18/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXISboxAccountCell : UITableViewCell {
    UILabel *accountTitleLabel;
    UILabel *accountValueLabel;
    
    UILabel *usageTitleLabel;
    UILabel *usageValueLabel;
    
    UILabel *quotaTitleLabel;
    UILabel *quotaValueLabel;
    
    UIButton *changeButton;
}

@property (nonatomic, retain) UILabel *accountTitleLabel;
@property (nonatomic, retain) UILabel *accountValueLabel;

@property (nonatomic, retain) UILabel *usageTitleLabel;
@property (nonatomic, retain) UILabel *usageValueLabel;

@property (nonatomic, retain) UILabel *quotaTitleLabel;
@property (nonatomic, retain) UILabel *quotaValueLabel;

@property (nonatomic, retain) UIButton *changeButton;

@end
