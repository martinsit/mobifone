//
//  RoamingCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoamingCell : UITableViewCell {
    UILabel *titleLabel;
    UILabel *descLabel;
}

@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *descLabel;

@end
