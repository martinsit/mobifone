//
//  BalanceHomeCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 11/18/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BalanceHomeCell : UITableViewCell {
    //UILabel *iconLabel;
    //UILabel *titleLabel;
    UILabel *balanceLabel;
    UILabel *validityLabel;
    UIButton *chevronButton;
}

//@property (nonatomic, retain) UILabel *iconLabel;
//@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *balanceLabel;
@property (nonatomic, retain) UILabel *validityLabel;
@property (nonatomic, retain) UIButton *chevronButton;

@end
