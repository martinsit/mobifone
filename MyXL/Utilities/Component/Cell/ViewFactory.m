//
//  ViewFactory.m
//  AXISnet
//
//  Created by pegolon on 11/25/10.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ViewFactory.h"

static ViewFactory *sharedInstanceVF = nil;

@implementation ViewFactory

- (id)initWithNib:(NSString*)aNibName
{
    if (self == [super init]) {
        viewTemplateStore = [[NSMutableDictionary alloc] init];
        NSArray * templates = [[NSBundle mainBundle] loadNibNamed:aNibName owner:self options:nil];
        
        for (id template in templates) {
            if ([template isKindOfClass:[UITableViewCell class]]) {
                UITableViewCell * cellTemplate = (UITableViewCell *)template;
                NSString * key = cellTemplate.reuseIdentifier;
                
                if (key) {
                    [viewTemplateStore setObject:[NSKeyedArchiver archivedDataWithRootObject:template]
                                          forKey:key];
                } else {
                    @throw [NSException exceptionWithName:@"Unknown cell"
                                                   reason:@"Cell has no reuseIdentifier"
                                                 userInfo:nil];
                }
            }
        }
    }
	
    return self;
}

- (UITableViewCell*)cellOfKind:(NSString*)theCellKind forTable:(UITableView*)aTableView
{
    
	UITableViewCell *cell = (UITableViewCell*)[aTableView dequeueReusableCellWithIdentifier:theCellKind];
	
    if (!cell) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:theCellKind] autorelease];
        
		NSData * cellData = [viewTemplateStore objectForKey:theCellKind];
        if (cellData) {
            cell = [NSKeyedUnarchiver unarchiveObjectWithData:cellData];
        } else {
            //NSLog(@"Don't know nothing about cell of kind %@", theCellKind);
        }
    }
	
    return cell;
}

//Singleton

// Get the shared instance and create it if necessary.
+ (ViewFactory*)sharedInstanceVF {
	if (sharedInstanceVF == nil) {
		sharedInstanceVF = [[super allocWithZone:NULL] init];
	}
	return sharedInstanceVF;
}

// We don't want to allocate a new instance, so return the current one.
+ (id)allocWithZone:(NSZone*)zone {
	return [[self sharedInstanceVF] retain];
}

// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone {
	return self;
}

// Once again - do nothing, as we don't have a retain counter for this object.
- (id)retain {
	return self;
}

// Replace the retain counter so we can never release this object.
- (NSUInteger)retainCount {
	return NSUIntegerMax;
}

// This function is empty, as we don't want to let the user release this object.
- (oneway void)release {
	
}

//Do nothing, other than return the shared instance - as this is expected from autorelease.
- (id)autorelease {
	return self;
}

- (void)dealloc
{
    [viewTemplateStore release];
    [super dealloc];
}

@end
