//
//  LanguageCell.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 12/28/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageCell : UITableViewCell {
    UILabel *iconLabel;
    UILabel *titleLabel;
    UIView *lineView;
    UIButton *indonesiaButton;
    UIButton *englishButton;
}

@property (nonatomic, retain) UILabel *iconLabel;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UIView *lineView;
@property (nonatomic, retain) UIButton *indonesiaButton;
@property (nonatomic, retain) UIButton *englishButton;

@end
