//
//  GeneralInfoCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/8/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralInfoCell : UITableViewCell {
    UILabel *infoLabel;
}

@property (nonatomic, retain) UILabel *infoLabel;

@end
