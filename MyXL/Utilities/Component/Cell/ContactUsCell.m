//
//  ContactUsCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ContactUsCell.h"
#import "Constant.h"

@implementation ContactUsCell

@synthesize titleLabel;
@synthesize valueLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        titleLabel = [[UILabel alloc] init];
        titleLabel.textAlignment = UITextAlignmentLeft;
        titleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        titleLabel.textColor = kDefaultTitleFontGrayColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        //titleLabel.text = @"FREE CALL TO AXIS WHICH CAN BE USED UNTIL 24.00";
        
        valueLabel = [[UILabel alloc] init];
        valueLabel.textAlignment = UITextAlignmentLeft;
        valueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        valueLabel.textColor = kDefaultPurpleColor;
        valueLabel.backgroundColor = [UIColor clearColor];
        valueLabel.numberOfLines = 0;
        valueLabel.lineBreakMode = UILineBreakModeWordWrap;
        //valueLabel.text = @"2 Hour(s) and 30 Minute(s)";
        
        [self.contentView addSubview:titleLabel];
        [self.contentView addSubview:valueLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 20.0;
    CGFloat topPadding = 5.0;
    CGFloat width = contentRect.size.width - (leftPadding*2);
    CGFloat height = 20.0;
    CGFloat deduction = 5.0;
    
    CGRect frame;
    
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width,
                       height);
    titleLabel.frame = frame;
    
    CGFloat startY = topPadding;
    if ([titleLabel.text length] > 0) {
        startY = titleLabel.frame.origin.y + titleLabel.frame.size.height - deduction;
    }
    
    frame = CGRectMake(leftPadding,
                       startY,
                       width,
                       height);
    valueLabel.frame = frame;
    [valueLabel sizeToFit];
}

- (void)dealloc
{
    [titleLabel release];
    [valueLabel release];
    [super dealloc];
}

@end
