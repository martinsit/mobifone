//
//  MyPackageCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPackageCell : UITableViewCell {
    UILabel *packageTitleLabel;
    UILabel *packageValueLabel;
    
    UILabel *validityTitleLabel;
    UILabel *validityValueLabel;
    
    UILabel *usageTitleLabel;
    UILabel *usageValueLabel;
    
    UILabel *typeTitleLabel;
    UILabel *typeValueLabel;
    
    UIView *separatorView;
    UIButton *stopButton;
    UIButton *upgradeButton;
}

@property (nonatomic, retain) UILabel *packageTitleLabel;
@property (nonatomic, retain) UILabel *packageValueLabel;

@property (nonatomic, retain) UILabel *validityTitleLabel;
@property (nonatomic, retain) UILabel *validityValueLabel;

@property (nonatomic, retain) UILabel *usageTitleLabel;
@property (nonatomic, retain) UILabel *usageValueLabel;

@property (nonatomic, retain) UILabel *typeTitleLabel;
@property (nonatomic, retain) UILabel *typeValueLabel;

@property (nonatomic, retain) UIView *separatorView;
@property (nonatomic, retain) UIButton *stopButton;
@property (nonatomic, retain) UIButton *upgradeButton;

@end
