//
//  FeatureHomeCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 11/27/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlatButton.h"

@interface FeatureHomeCell : UITableViewCell {
    FlatButton *button1;
    FlatButton *button2;
    BOOL _isTwoButtons;
}

@property (nonatomic, retain) FlatButton *button1;
@property (nonatomic, retain) FlatButton *button2;
@property (nonatomic, assign) BOOL isTwoButtons;

@end
