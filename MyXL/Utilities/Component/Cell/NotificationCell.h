//
//  NotificationCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/19/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell {
    UIImageView *notifTypeView;
    UILabel *dateLabel;
    UILabel *monthLabel;
    UILabel *notifLabel;
    UILabel *fromLabel;
    UILabel *arrowLabel;
}

@property (nonatomic, retain) UIImageView *notifTypeView;
@property (nonatomic, retain) UILabel *dateLabel;
@property (nonatomic, retain) UILabel *monthLabel;
@property (nonatomic, retain) UILabel *notifLabel;
@property (nonatomic, retain) UILabel *fromLabel;
@property (nonatomic, retain) UILabel *arrowLabel;

@end
