//
//  TransactionHistoryCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/25/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "TransactionHistoryCell.h"
#import "Constant.h"

@implementation TransactionHistoryCell

@synthesize iconLabel;
@synthesize typeLabel;
@synthesize timeLabel;
@synthesize destinationLabel;
@synthesize durationLabel;
@synthesize chargingLabel;

@synthesize lineView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
//        iconLabel = [[UILabel alloc] init];
//        iconLabel.textAlignment = UITextAlignmentCenter;
//        iconLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:20.0];
//        iconLabel.textColor = kDefaultFontGrayColor;
//        iconLabel.backgroundColor = [UIColor clearColor];
        
        typeLabel = [[UILabel alloc] init];
        typeLabel.textAlignment = UITextAlignmentLeft;
        typeLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontValueSize];
        typeLabel.textColor = kDefaultBlackColor;
        typeLabel.backgroundColor = [UIColor clearColor];
        typeLabel.lineBreakMode = UILineBreakModeWordWrap;
        typeLabel.numberOfLines = 0;
        
        timeLabel = [[UILabel alloc] init];
        timeLabel.textAlignment = UITextAlignmentLeft;
        timeLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        timeLabel.textColor = kDefaultBlackColor;
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.lineBreakMode = UILineBreakModeWordWrap;
        timeLabel.numberOfLines = 0;
        
//        destinationLabel = [[UILabel alloc] init];
//        destinationLabel.textAlignment = UITextAlignmentLeft;
//        destinationLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontValueSize];
//        destinationLabel.textColor = kDefaultPurpleColor;
//        destinationLabel.backgroundColor = [UIColor clearColor];
//        destinationLabel.lineBreakMode = UILineBreakModeWordWrap;
//        destinationLabel.numberOfLines = 0;
        
//        durationLabel = [[UILabel alloc] init];
//        durationLabel.textAlignment = UITextAlignmentLeft;
//        durationLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontValueSize];
//        durationLabel.textColor = kDefaultPurpleColor;
//        durationLabel.backgroundColor = [UIColor clearColor];
//        durationLabel.lineBreakMode = UILineBreakModeWordWrap;
//        durationLabel.numberOfLines = 0;
        
        chargingLabel = [[UILabel alloc] init];
        chargingLabel.textAlignment = UITextAlignmentRight;
        chargingLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        chargingLabel.textColor = kDefaultBlackColor;
        chargingLabel.backgroundColor = [UIColor clearColor];
        chargingLabel.lineBreakMode = UILineBreakModeWordWrap;
        chargingLabel.numberOfLines = 0;
        
        lineView = [[UIView alloc] initWithFrame:CGRectZero];
        lineView.backgroundColor = kDefaultSeparatorColor;
        
        //[self.contentView addSubview:iconLabel];
        [self.contentView addSubview:typeLabel];
        [self.contentView addSubview:timeLabel];
        //[self.contentView addSubview:destinationLabel];
        //[self.contentView addSubview:durationLabel];
        [self.contentView addSubview:chargingLabel];
        [self.contentView addSubview:lineView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 15.0;
    CGFloat topPadding = 0.0;
    
    //CGFloat height = contentRect.size.height - (topPadding*2);
    //CGFloat width = 150.0;
    
    CGFloat titleLabelHeight = 20.0;
    
    CGRect frame;
    
//    frame = CGRectMake(leftPadding,
//                       topPadding,
//                       40.0,
//                       40.0);
//    iconLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       topPadding,
                       contentRect.size.width - (leftPadding*2),
                       titleLabelHeight);
    typeLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       typeLabel.frame.origin.y + typeLabel.frame.size.height,
                       (contentRect.size.width - (leftPadding*2))*0.7,
                       titleLabelHeight);
    timeLabel.frame = frame;
    
    frame = CGRectMake(timeLabel.frame.origin.x + timeLabel.frame.size.width,
                       timeLabel.frame.origin.y,
                       (contentRect.size.width - (leftPadding*2))*0.3,
                       titleLabelHeight);
    chargingLabel.frame = frame;
    
//    frame = CGRectMake(leftPadding + iconLabel.frame.size.width,
//                       topPadding + timeLabel.frame.size.height,
//                       width,
//                       titleLabelHeight);
//    destinationLabel.frame = frame;
    
//    frame = CGRectMake(leftPadding + iconLabel.frame.size.width + timeLabel.frame.size.width,
//                       topPadding,
//                       60.0,
//                       titleLabelHeight);
//    durationLabel.frame = frame;
    
    frame = CGRectMake(typeLabel.frame.origin.x,
                       contentRect.size.height - 1.0,
                       contentRect.size.width - typeLabel.frame.origin.x,
                       1.0);
    lineView.frame = frame;

}

- (void)dealloc
{
    //[iconLabel release];
    [timeLabel release];
    [typeLabel release];
    //[destinationLabel release];
    //[durationLabel release];
    [chargingLabel release];
    [lineView release];
    [super dealloc];
}

@end
