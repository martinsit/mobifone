//
//  AXISnetCellBackground.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/12/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXISnetCellBackground : UIView {
    UIColor     *strokeColor;
    UIColor     *rectColor;
    UIColor     *rectColorSelected;
    UIColor     *grayBackgroundColor;
    CGFloat     strokeWidth;
    CGFloat     cornerRadius;
    
    BOOL _selected;
}

@property (nonatomic, retain) UIColor *strokeColor;
@property (nonatomic, retain) UIColor *rectColor;
@property (nonatomic, retain) UIColor *rectColorSelected;
@property (nonatomic, retain) UIColor *grayBackgroundColor;
@property CGFloat strokeWidth;
@property CGFloat cornerRadius;

@property  BOOL selected;

- (id)initWithFrame:(CGRect)frame withDefaultColor:(UIColor*)defaultColor withSelectedColor:(UIColor*)selectedColor;

@end
