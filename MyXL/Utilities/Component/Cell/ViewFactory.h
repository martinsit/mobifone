//
//  ViewFactory.h
//  AXISnet
//
//  Created by pegolon on 11/25/10.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ViewFactory : NSObject {
	NSMutableDictionary * viewTemplateStore;
}

- (id)initWithNib:(NSString*)aNibName;
- (UITableViewCell*)cellOfKind:(NSString*)theCellKind forTable:(UITableView*)aTableView;

+ (id)sharedInstanceVF;

@end
