//
//  CommonCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/8/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonCell : UITableViewCell {
    UILabel *iconLabel;
    UILabel *titleLabel;
    UILabel *arrowLabel;
    UIView *lineView;
    UIButton *refreshButton;
    BOOL _withRefreshButton;
    
    UILabel *descLabel;
    UIButton *infoButton;
    BOOL _withInfoButton;
    BOOL _needResize;
}

@property (nonatomic, retain) UILabel *iconLabel;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *arrowLabel;
@property (nonatomic, retain) UIView *lineView;
@property (nonatomic, retain) UIButton *refreshButton;
@property (nonatomic, assign) BOOL withRefreshButton;

@property (nonatomic, retain) UILabel *descLabel;
@property (nonatomic, retain) UIButton *infoButton;
@property (nonatomic, assign) BOOL withInfoButton;
@property (nonatomic, assign) BOOL needResize;

@end
