//
//  MyPackageiPadCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/15/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPackageiPadCell : UITableViewCell {
    
    UIView *separatorView;
    
    UILabel *packageValueLabel;
    
    UILabel *validityValueLabel;
    
    UILabel *usageValueLabel;
    
    UILabel *typeValueLabel;
    
    UIButton *stopButton;
    UIButton *upgradeButton;
}

@property (nonatomic, retain) UIView *separatorView;

@property (nonatomic, retain) UILabel *packageValueLabel;

@property (nonatomic, retain) UILabel *validityValueLabel;

@property (nonatomic, retain) UILabel *usageValueLabel;

@property (nonatomic, retain) UILabel *typeValueLabel;

@property (nonatomic, retain) UIButton *stopButton;
@property (nonatomic, retain) UIButton *upgradeButton;

@end
