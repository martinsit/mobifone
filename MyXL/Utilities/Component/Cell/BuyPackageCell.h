//
//  BuyPackageCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/2/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyPackageCell : UITableViewCell {
    
    UILabel *titleLabel;
    UILabel *priceLabel;
    UILabel *descLabel;
    
    UIButton *infoButton;
    UIButton *activateButton;
    
    UIView *lineView;
    
    BOOL _needResize;
}


@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *priceLabel;
@property (nonatomic, retain) UILabel *descLabel;

@property (nonatomic, retain) UIButton *infoButton;
@property (nonatomic, retain) UIButton *activateButton;

@property (nonatomic, retain) UIView *lineView;

@property (nonatomic, assign) BOOL needResize;

@end
