//
//  ProductCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 8/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ProductCell.h"
#import "Constant.h"
#import "Language.h"
#import "AXISnetCommon.h"
#import <QuartzCore/QuartzCore.h>

@implementation ProductCell

@synthesize borderView;
@synthesize imageView;
@synthesize productNameLabel;
@synthesize productPriceLabel;
@synthesize buyButton;
@synthesize detailButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        borderView = [[UIView alloc] init];
        borderView.backgroundColor = [UIColor clearColor];
        [borderView.layer setCornerRadius:kDefaultCornerRadius];
        // border
        [borderView.layer setBorderColor:kDefaultBaseColor.CGColor];
        [borderView.layer setBorderWidth:1.0f];
        
        imageView = [[UIImageView alloc] init];
        imageView.image = [UIImage imageNamed:@"sp_kaskus_150.jpg"];
        
        productNameLabel = [[UILabel alloc] init];
        productNameLabel.textAlignment = UITextAlignmentLeft;
        productNameLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        productNameLabel.textColor = kDefaultTitleFontGrayColor;
        productNameLabel.backgroundColor = [UIColor clearColor];
        productNameLabel.numberOfLines = 0;
        productNameLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        productPriceLabel = [[UILabel alloc] init];
        productPriceLabel.textAlignment = UITextAlignmentLeft;
        productPriceLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        productPriceLabel.textColor = kDefaultPurpleColor;
        productPriceLabel.backgroundColor = [UIColor clearColor];
        productPriceLabel.numberOfLines = 0;
        productPriceLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        
        buyButton = createButtonWithFrame(CGRectMake(0, 0, 80, kDefaultButtonHeight),
                                          [Language get:@"buy" alter:nil],
                                          buttonFont,
                                          kDefaultBaseColor,
                                          kDefaultPinkColor,
                                          kDefaultPurpleColor);
        
        detailButton = createButtonWithFrame(CGRectMake(0, 0, 80, kDefaultButtonHeight),
                                             [Language get:@"detail" alter:nil],
                                             buttonFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
        
        [self.contentView addSubview:borderView];
        [self.contentView addSubview:imageView];
        [self.contentView addSubview:productNameLabel];
        [self.contentView addSubview:productPriceLabel];
        [self.contentView addSubview:buyButton];
        [self.contentView addSubview:detailButton];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    //CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 20.0;
    CGFloat topPadding = 5.0;
    
    CGFloat width = 0.0;
    if (IS_IPAD) {
        width = (self.frame.size.width - (45*2) - (leftPadding*2));
    }
    else {
        width = (self.frame.size.width - (10*2) - (leftPadding*2));
    }
    
    CGFloat height = 20.0;
    CGFloat deduction = 5.0;
    
    CGRect frame;
    
    // Border Rounded Corner
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width,
                       self.frame.size.height - (topPadding*2));
    borderView.frame = frame;
    
    leftPadding = 30.0;
    topPadding = 15.0;
    if (IS_IPAD) {
        width = (self.frame.size.width - (45*2) - (leftPadding*2));
    }
    else {
        width = (self.frame.size.width - (10*2) - (leftPadding*2));
    }
    
    // UIImageView
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width,
                       192.0);
    imageView.frame = frame;
    
    // Product Name Label
    frame = CGRectMake(leftPadding,
                       imageView.frame.origin.y + imageView.frame.size.height + kDefaultComponentPadding,
                       width,
                       height);
    productNameLabel.frame = frame;
    
    // Product Price Label
    frame = CGRectMake(leftPadding,
                       productNameLabel.frame.origin.y + productNameLabel.frame.size.height - deduction,
                       width,
                       height);
    productPriceLabel.frame = frame;
    
    // Buy Button
    frame = buyButton.frame;
    frame.origin.y = productPriceLabel.frame.origin.y + productPriceLabel.frame.size.height + kDefaultComponentPadding;
    frame.origin.x = productPriceLabel.frame.origin.x;
    buyButton.frame = frame;
    
    // Detail Button
    frame = detailButton.frame;
    frame.origin.y = buyButton.frame.origin.y;
    frame.origin.x = productPriceLabel.frame.origin.x + productPriceLabel.frame.size.width - 80;
    detailButton.frame = frame;
}

- (void)dealloc
{
    [borderView release];
    [imageView release];
    [productNameLabel release];
    [productPriceLabel release];
    [buyButton release];
    [detailButton release];
    [super dealloc];
}

@end
