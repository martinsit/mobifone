//
//  BalanceCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BalanceCell.h"
#import "Constant.h"

@implementation BalanceCell

@synthesize msisdnTitleLabel;
@synthesize msisdnValueLabel;
@synthesize statusTitleLabel;
@synthesize statusValueLabel;
@synthesize balanceTitleLabel;
@synthesize balanceValueLabel;
@synthesize validityTitleLabel;
@synthesize validityValueLabel;
@synthesize typeTitleLabel;
@synthesize typeValueLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        //self.backgroundColor = [UIColor yellowColor];
        
        msisdnTitleLabel = [[UILabel alloc] init];
        msisdnTitleLabel.textAlignment = UITextAlignmentLeft;
        msisdnTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        msisdnTitleLabel.textColor = kDefaultTitleFontGrayColor;
        msisdnTitleLabel.backgroundColor = [UIColor clearColor];
        msisdnTitleLabel.text = @"YOUR AXIS NUMBER";
        //msisdnTitleLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        
        msisdnValueLabel = [[UILabel alloc] init];
        msisdnValueLabel.textAlignment = UITextAlignmentLeft;
        msisdnValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        msisdnValueLabel.textColor = kDefaultPurpleColor;
        msisdnValueLabel.backgroundColor = [UIColor clearColor];
        msisdnValueLabel.text = @"083899664600";
        
        statusTitleLabel = [[UILabel alloc] init];
        statusTitleLabel.textAlignment = UITextAlignmentLeft;
        statusTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        statusTitleLabel.textColor = kDefaultTitleFontGrayColor;
        statusTitleLabel.backgroundColor = [UIColor clearColor];
        statusTitleLabel.text = @"STATUS";
        //statusTitleLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        
        statusValueLabel = [[UILabel alloc] init];
        statusValueLabel.textAlignment = UITextAlignmentLeft;
        statusValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        statusValueLabel.textColor = kDefaultPurpleColor;
        statusValueLabel.backgroundColor = [UIColor clearColor];
        statusValueLabel.text = @"Active";
        
        balanceTitleLabel = [[UILabel alloc] init];
        balanceTitleLabel.textAlignment = UITextAlignmentLeft;
        balanceTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        balanceTitleLabel.textColor = kDefaultTitleFontGrayColor;
        balanceTitleLabel.backgroundColor = [UIColor clearColor];
        balanceTitleLabel.text = @"YOUR BALANCE";
        
        balanceValueLabel = [[UILabel alloc] init];
        balanceValueLabel.textAlignment = UITextAlignmentLeft;
        balanceValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        balanceValueLabel.textColor = kDefaultPurpleColor;
        balanceValueLabel.backgroundColor = [UIColor clearColor];
        balanceValueLabel.text = @"Rp. 2.500";
        
        validityTitleLabel = [[UILabel alloc] init];
        validityTitleLabel.textAlignment = UITextAlignmentLeft;
        validityTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        validityTitleLabel.textColor = kDefaultTitleFontGrayColor;
        validityTitleLabel.backgroundColor = [UIColor clearColor];
        validityTitleLabel.text = @"VALIDITY";
        
        validityValueLabel = [[UILabel alloc] init];
        validityValueLabel.textAlignment = UITextAlignmentLeft;
        validityValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        validityValueLabel.textColor = kDefaultPurpleColor;
        validityValueLabel.backgroundColor = [UIColor clearColor];
        validityValueLabel.text = @"08-Jun-2013";
        
        typeTitleLabel = [[UILabel alloc] init];
        typeTitleLabel.textAlignment = UITextAlignmentLeft;
        typeTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        typeTitleLabel.textColor = kDefaultTitleFontGrayColor;
        typeTitleLabel.backgroundColor = [UIColor clearColor];
        typeTitleLabel.text = @"TYPE";
        
        typeValueLabel = [[UILabel alloc] init];
        typeValueLabel.textAlignment = UITextAlignmentLeft;
        typeValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        typeValueLabel.textColor = kDefaultPurpleColor;
        typeValueLabel.backgroundColor = [UIColor clearColor];
        typeValueLabel.text = @"Prepaid";
        
        [self.contentView addSubview:msisdnTitleLabel];
        [self.contentView addSubview:msisdnValueLabel];
        [self.contentView addSubview:statusTitleLabel];
        [self.contentView addSubview:statusValueLabel];
        [self.contentView addSubview:balanceTitleLabel];
        [self.contentView addSubview:balanceValueLabel];
        [self.contentView addSubview:validityTitleLabel];
        [self.contentView addSubview:validityValueLabel];
        [self.contentView addSubview:typeTitleLabel];
        [self.contentView addSubview:typeValueLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    //CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 20.0;
    CGFloat topPadding = 5.0;
    CGFloat width = 150.0;
    CGFloat height = 20.0;
    CGFloat deduction = 5.0;
    
    CGRect frame;
    
    // MSISDN
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width,
                       height);
    msisdnTitleLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       msisdnTitleLabel.frame.origin.y + msisdnTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    msisdnValueLabel.frame = frame;
    
    // Balance
    frame = CGRectMake(leftPadding,
                       msisdnValueLabel.frame.origin.y + msisdnValueLabel.frame.size.height + topPadding,
                       width,
                       height);
    balanceTitleLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       balanceTitleLabel.frame.origin.y + balanceTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    balanceValueLabel.frame = frame;
    
    // Status
    //CGFloat rightSideTextPadding = leftPadding + msisdnTitleLabel.frame.size.width;
    CGFloat rightSideTextPadding = self.frame.size.width/2;
    frame = CGRectMake(rightSideTextPadding,
                       topPadding,
                       width,
                       height);
    statusTitleLabel.frame = frame;
    
    frame = CGRectMake(rightSideTextPadding,
                       statusTitleLabel.frame.origin.y + statusTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    statusValueLabel.frame = frame;
    
    // Validity
    frame = CGRectMake(rightSideTextPadding,
                       statusValueLabel.frame.origin.y + statusValueLabel.frame.size.height + topPadding,
                       width,
                       height);
    validityTitleLabel.frame = frame;
    
    frame = CGRectMake(rightSideTextPadding,
                       validityTitleLabel.frame.origin.y + validityTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    validityValueLabel.frame = frame;
    
    // Type
    frame = CGRectMake(leftPadding,
                       balanceValueLabel.frame.origin.y + balanceValueLabel.frame.size.height + topPadding,
                       width,
                       height);
    typeTitleLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       typeTitleLabel.frame.origin.y + typeTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    typeValueLabel.frame = frame;
}

- (void)dealloc
{
    [msisdnTitleLabel release];
    [msisdnValueLabel release];
    [statusTitleLabel release];
    [statusValueLabel release];
    [balanceTitleLabel release];
    [balanceValueLabel release];
    [validityTitleLabel release];
    [validityValueLabel release];
    [typeTitleLabel release];
    [typeValueLabel release];
    [super dealloc];
}

@end
