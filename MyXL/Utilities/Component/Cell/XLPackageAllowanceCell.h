//
//  XLPackageAllowanceCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XLPackageAllowanceCell : UITableViewCell {
    UIView *legendView;
    UILabel *allowanceName;
    UILabel *remaining;
    UIView *lineView;
}

@property (nonatomic, retain) UIView *legendView;
@property (nonatomic, retain) UILabel *allowanceName;
@property (nonatomic, retain) UILabel *remaining;
@property (nonatomic, retain) UIView *lineView;

@end
