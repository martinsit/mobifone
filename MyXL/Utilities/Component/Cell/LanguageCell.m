//
//  LanguageCell.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 12/28/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "LanguageCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Constant.h"
#import "AXISnetCommon.h"

@implementation LanguageCell

@synthesize iconLabel;
@synthesize titleLabel;
@synthesize lineView;
@synthesize indonesiaButton;
@synthesize englishButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        iconLabel = [[UILabel alloc] init];
        iconLabel.textAlignment = UITextAlignmentCenter;
        iconLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:18.0];
        iconLabel.textColor = kDefaultNavyBlueColor;
        iconLabel.backgroundColor = kDefaultYellowColor;
        
        titleLabel = [[UILabel alloc] init];
        titleLabel.textAlignment = UITextAlignmentLeft;
        titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
        titleLabel.textColor = kDefaultWhiteColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        lineView = [[UIView alloc] initWithFrame:CGRectZero];
        lineView.backgroundColor = kDefaultNavyBlueColor;
        
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
        indonesiaButton = createButtonWithFrame(CGRectMake(0.0, 0.0, kDefaultButtonHeight, kDefaultButtonHeight),
                                                @"ID",
                                                buttonFont,
                                                kDefaultNavyBlueColor,
                                                kDefaultYellowColor,
                                                kDefaultAquaMarineColor);
        
        englishButton = createButtonWithFrame(CGRectMake(0.0, 0.0, kDefaultButtonHeight, kDefaultButtonHeight),
                                              @"EN",
                                              buttonFont,
                                              kDefaultNavyBlueColor,
                                              kDefaultYellowColor,
                                              kDefaultAquaMarineColor);
        
        [self.contentView addSubview:iconLabel];
        [self.contentView addSubview:titleLabel];
        [self.contentView addSubview:lineView];
        [self.contentView addSubview:indonesiaButton];
        [self.contentView addSubview:englishButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    
    CGFloat labelLeftPadding = 15.0;
    CGFloat width = 30.0;
    CGFloat height = 30.0;
    
    CGRect frame;
    
    if ([iconLabel.text length] > 0) {
        
        iconLabel.hidden = NO;
        
        frame = CGRectMake(labelLeftPadding, (contentRect.size.height - height)/2, width, height);
        iconLabel.frame = frame;
        iconLabel.layer.cornerRadius = kDefaultCornerRadius;
        iconLabel.layer.masksToBounds = YES;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            frame = CGRectMake(iconLabel.frame.origin.x + iconLabel.frame.size.width + 8.0,
                               (contentRect.size.height - height)/2,
                               contentRect.size.width - (labelLeftPadding) - (width*2) - 8.0,
                               height);
        }
        else {
            frame = CGRectMake(iconLabel.frame.origin.x + iconLabel.frame.size.width + 8.0,
                               (contentRect.size.height - height)/2,
                               contentRect.size.width - (labelLeftPadding) - (width*2) - 8.0,
                               height + 4.0);
        }
        
        titleLabel.frame = frame;
    }
    else {
        
        iconLabel.hidden = YES;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            frame = CGRectMake(labelLeftPadding,
                               (contentRect.size.height - height)/2,
                               contentRect.size.width - ((labelLeftPadding*2) + width) + labelLeftPadding,
                               height);
        }
        else {
            frame = CGRectMake(labelLeftPadding,
                               (contentRect.size.height - height)/2,
                               contentRect.size.width - ((labelLeftPadding*2) + width) + labelLeftPadding,
                               height + 4.0);
        }
        
        titleLabel.frame = frame;
    }
    
    frame = CGRectMake(contentRect.size.width - width -  15.0,
                       (contentRect.size.height - height)/2,
                       width,
                       height);
    englishButton.frame = frame;
    
    frame = CGRectMake(englishButton.frame.origin.x - 5.0 - width,
                       (contentRect.size.height - height)/2,
                       width,
                       height);
    indonesiaButton.frame = frame;
    
    frame = CGRectMake(titleLabel.frame.origin.x,
                       contentRect.size.height - 1.0,
                       contentRect.size.width - titleLabel.frame.origin.x,
                       0.5);
    lineView.frame = frame;
}

- (void)dealloc
{
    [iconLabel release];
    [titleLabel release];
    [lineView release];
    [englishButton release];
    [indonesiaButton release];
    [super dealloc];
}

@end
