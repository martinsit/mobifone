//
//  LoadMoreCell.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 1/16/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadMoreCell : UITableViewCell {
    UILabel *titleLabel;
    UILabel *arrowLabel;
}

@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *arrowLabel;

@end
