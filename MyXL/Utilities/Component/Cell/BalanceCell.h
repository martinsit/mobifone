//
//  BalanceCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BalanceCell : UITableViewCell {
    UILabel *msisdnTitleLabel;
    UILabel *msisdnValueLabel;
    UILabel *statusTitleLabel;
    UILabel *statusValueLabel;
    UILabel *balanceTitleLabel;
    UILabel *balanceValueLabel;
    UILabel *validityTitleLabel;
    UILabel *validityValueLabel;
    UILabel *typeTitleLabel;
    UILabel *typeValueLabel;
}

@property (nonatomic, retain) UILabel *msisdnTitleLabel;
@property (nonatomic, retain) UILabel *msisdnValueLabel;
@property (nonatomic, retain) UILabel *statusTitleLabel;
@property (nonatomic, retain) UILabel *statusValueLabel;
@property (nonatomic, retain) UILabel *balanceTitleLabel;
@property (nonatomic, retain) UILabel *balanceValueLabel;
@property (nonatomic, retain) UILabel *validityTitleLabel;
@property (nonatomic, retain) UILabel *validityValueLabel;
@property (nonatomic, retain) UILabel *typeTitleLabel;
@property (nonatomic, retain) UILabel *typeValueLabel;

@end
