//
//  LeftCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/27/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "LeftCell.h"
#import "Constant.h"

@implementation LeftCell

@synthesize iconLabel;
@synthesize titleLabel;
@synthesize arrowLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        iconLabel = [[UILabel alloc] init];
        iconLabel.textAlignment = UITextAlignmentCenter;
        //iconLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize];
        iconLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:17.0];
        iconLabel.textColor = kDefaultWhiteColor;
        iconLabel.backgroundColor = [UIColor clearColor];
        
        titleLabel = [[UILabel alloc] init];
        titleLabel.textAlignment = UITextAlignmentLeft;
        titleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
        titleLabel.textColor = kDefaultWhiteColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        
        arrowLabel = [[UILabel alloc] init];
        arrowLabel.textAlignment = UITextAlignmentCenter;
        arrowLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:17.0];
        arrowLabel.textColor = kDefaultWhiteColor;
        arrowLabel.backgroundColor = [UIColor clearColor];
        
        [self.contentView addSubview:iconLabel];
        [self.contentView addSubview:titleLabel];
        [self.contentView addSubview:arrowLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    
    //NSLog(@"self.bounds.size.width = %f",self.bounds.size.width);
    //NSLog(@"self.bounds.size.height = %f",self.bounds.size.height);
    
    //CGFloat leftPadding = 20.0;
    CGFloat topPadding = 5.0;
    CGFloat labelLeftPadding = 0.0;
    CGFloat symbolWidth = 30;
    CGFloat rightPadding = 45;
    //CGFloat boundsX = contentRect.origin.x;
    
    //CGFloat height = 28.0;
    
    CGRect frame;
    
    frame = CGRectMake(labelLeftPadding, topPadding, symbolWidth, contentRect.size.height - (topPadding*2));
    iconLabel.frame = frame;
    
    frame = CGRectMake(labelLeftPadding + symbolWidth, topPadding, contentRect.size.width - ((labelLeftPadding + symbolWidth)*2) - rightPadding, contentRect.size.height - (topPadding*2));
    titleLabel.frame = frame;
    
    frame = CGRectMake(labelLeftPadding + symbolWidth + titleLabel.frame.size.width, topPadding, symbolWidth, contentRect.size.height - (topPadding*2));
    arrowLabel.frame = frame;
}

- (void)dealloc
{
    [iconLabel release];
    [titleLabel release];
    [arrowLabel release];
    [super dealloc];
}

@end
