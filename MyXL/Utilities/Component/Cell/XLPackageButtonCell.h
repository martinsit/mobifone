//
//  XLPackageButtonCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XLPackageButtonCell : UITableViewCell {
    UIButton *stopButton;
    UIButton *shareQuotaButton;
    UIButton *quotaMeterButton;
}

@property (nonatomic, retain) UIButton *stopButton;
@property (nonatomic, retain) UIButton *shareQuotaButton;
@property (nonatomic, retain) UIButton *quotaMeterButton;

@end
