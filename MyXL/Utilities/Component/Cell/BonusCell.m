//
//  BonusCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/16/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BonusCell.h"
#import "Constant.h"

@implementation BonusCell

@synthesize titleLabel;
@synthesize valueLabel;
@synthesize validityLabel;
@synthesize validityValueLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        titleLabel = [[UILabel alloc] init];
        titleLabel.textAlignment = UITextAlignmentLeft;
        titleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        titleLabel.textColor = kDefaultTitleFontGrayColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.text = @"FREE CALL TO AXIS WHICH CAN BE USED UNTIL 24.00";
        
        valueLabel = [[UILabel alloc] init];
        valueLabel.textAlignment = UITextAlignmentLeft;
        valueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        valueLabel.textColor = kDefaultPurpleColor;
        valueLabel.backgroundColor = [UIColor clearColor];
        valueLabel.text = @"2 Hour(s) and 30 Minute(s)";
        
        validityLabel = [[UILabel alloc] init];
        validityLabel.textAlignment = UITextAlignmentLeft;
        validityLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        validityLabel.textColor = kDefaultTitleFontGrayColor;
        validityLabel.backgroundColor = [UIColor clearColor];
        validityLabel.text = @"VALIDITY";
        
        validityValueLabel = [[UILabel alloc] init];
        validityValueLabel.textAlignment = UITextAlignmentLeft;
        validityValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        validityValueLabel.textColor = kDefaultPurpleColor;
        validityValueLabel.backgroundColor = [UIColor clearColor];
        validityValueLabel.text = @"09-Feb-2013";
        
        [self.contentView addSubview:titleLabel];
        [self.contentView addSubview:valueLabel];
        [self.contentView addSubview:validityLabel];
        [self.contentView addSubview:validityValueLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 20.0;
    CGFloat topPadding = 5.0;
    CGFloat width = contentRect.size.width - (leftPadding*2);
    CGFloat height = 20.0;
    CGFloat deduction = 5.0;
    
    CGRect frame;
    
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width,
                       height);
    titleLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       titleLabel.frame.origin.y + titleLabel.frame.size.height - deduction,
                       width,
                       height);
    valueLabel.frame = frame;
    
    // Validity
    frame = CGRectMake(leftPadding,
                       valueLabel.frame.origin.y + valueLabel.frame.size.height + topPadding,
                       width,
                       height);
    validityLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       validityLabel.frame.origin.y + validityLabel.frame.size.height - deduction,
                       width,
                       height);
    validityValueLabel.frame = frame;
}

- (void)dealloc
{
    [titleLabel release];
    [valueLabel release];
    [validityLabel release];
    [validityValueLabel release];
    [super dealloc];
}

@end
