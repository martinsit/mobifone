//
//  PromoCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PromoCell.h"
#import "Constant.h"
#import "AXISnetCommon.h"

@implementation PromoCell

@synthesize lineView;
@synthesize titleLabel;
@synthesize shortDescLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        lineView = [[UIImageView alloc] init];
        lineView.backgroundColor = [UIColor clearColor];
        UIImage *backgroundImage = imageFromColor(kDefaultFontLightGrayColor);
        lineView.image = backgroundImage;
        
        titleLabel = [[UILabel alloc] init];
        titleLabel.textAlignment = UITextAlignmentLeft;
        titleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        titleLabel.textColor = kDefaultPurpleColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.lineBreakMode = UILineBreakModeWordWrap;
        titleLabel.numberOfLines = 0;
        
        shortDescLabel = [[UILabel alloc] init];
        shortDescLabel.textAlignment = UITextAlignmentLeft;
        shortDescLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        shortDescLabel.textColor = kDefaultPurpleColor;
        shortDescLabel.backgroundColor = [UIColor clearColor];
        shortDescLabel.lineBreakMode = UILineBreakModeWordWrap;
        shortDescLabel.numberOfLines = 0;
        
        [self.contentView addSubview:lineView];
        [self.contentView addSubview:titleLabel];
        [self.contentView addSubview:shortDescLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 30.0;
    CGFloat topPadding = 15.0;
    
    CGFloat height = contentRect.size.height - (topPadding*2);
    
    CGFloat titleLabelHeight = 20.0;
    
    CGRect frame;
    
    frame = CGRectMake(leftPadding,
                       topPadding,
                       10.0,
                       height);
    lineView.frame = frame;
    
    frame = CGRectMake(leftPadding + lineView.frame.size.width + 10,
                       topPadding,
                       contentRect.size.width - (leftPadding + lineView.frame.size.width + 10) - leftPadding,
                       titleLabelHeight);
    titleLabel.frame = frame;
    [titleLabel sizeToFit];
    
    frame = CGRectMake(leftPadding + lineView.frame.size.width + 10,
                       topPadding + titleLabel.frame.size.height,
                       contentRect.size.width - (leftPadding + lineView.frame.size.width + 10) - leftPadding,
                       height - titleLabel.frame.size.height);
    shortDescLabel.frame = frame;
    [shortDescLabel sizeToFit];
    
    CGRect newFrame = lineView.frame;
    newFrame.size.height = titleLabel.frame.size.height + shortDescLabel.frame.size.height;
    lineView.frame = newFrame;
}

- (void)dealloc
{
    [lineView release];
    [titleLabel release];
    [shortDescLabel release];
    [super dealloc];
}

@end
