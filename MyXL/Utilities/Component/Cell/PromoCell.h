//
//  PromoCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromoCell : UITableViewCell {
    UIImageView *lineView;
    UILabel *titleLabel;
    UILabel *shortDescLabel;
}

@property (nonatomic, retain) UIImageView *lineView;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *shortDescLabel;

@end
