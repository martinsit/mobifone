//
//  AXISnetCellBackground.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/12/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "AXISnetCellBackground.h"
#import "AXISnetCommon.h"
#import "Constant.h"

@implementation AXISnetCellBackground

@synthesize strokeColor;
@synthesize rectColor;
@synthesize rectColorSelected;
@synthesize grayBackgroundColor;
@synthesize strokeWidth;
@synthesize cornerRadius;

@synthesize selected = _selected;

/*
- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super initWithCoder:decoder])
    {
        self.strokeColor = kDefaultStrokeColor;
        //self.backgroundColor = [UIColor whiteColor];
        self.strokeWidth = kDefaultStrokeWidth;
        self.rectColor = kDefaultRectColor;
        self.cornerRadius = kDefaultCornerRadius;
        [super setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.opaque = NO;
        self.strokeColor = kDefaultPinkColor;
        self.rectColor = kDefaultPurpleColor;
        self.rectColorSelected = kDefaultPinkColor;
        self.grayBackgroundColor = kDefaultButtonGrayColor;
        self.strokeWidth = kDefaultStrokeWidth;
        self.cornerRadius = kDefaultCornerRadius;
        [super setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withDefaultColor:(UIColor*)defaultColor withSelectedColor:(UIColor*)selectedColor {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.opaque = NO;
        self.strokeColor = kDefaultBlueColor;
        self.rectColor = defaultColor;
        self.rectColorSelected = selectedColor;
        self.grayBackgroundColor = kDefaultWhiteColor;
        self.strokeWidth = kDefaultStrokeWidth;
        self.cornerRadius = kDefaultCornerRadius;
        [super setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

/*
- (void)setBackgroundColor:(UIColor *)newBGColor
{
    // Ignore any attempt to set background color - backgroundColor must stay set to clearColor
    // We could throw an exception here, but that would cause problems with IB, since backgroundColor
    // is a palletized property, IB will attempt to set backgroundColor for any view that is loaded
    // from a nib, so instead, we just quietly ignore this.
    //
    // Alternatively, we could put an NSLog statement here to tell the programmer to set rectColor...
}

- (void)setOpaque:(BOOL)newIsOpaque
{
    // Ignore attempt to set opaque to YES.
}*/

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    // Drawing code
    
    self.backgroundColor = kDefaultBaseColor;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Add White Background
    CGFloat leftPadding = 11.0;
    CGFloat width = self.bounds.size.width - (leftPadding*2);
    CGRect leftBackgroundRect = CGRectMake(leftPadding + 1, self.bounds.origin.y, width, self.bounds.size.height);
    addBackground(context, leftBackgroundRect, [UIColor whiteColor].CGColor);
    
    leftPadding = 20.0;
    CGFloat topPadding = 2.0;
    
    CGRect backgroundRect = CGRectMake(leftPadding, topPadding, rect.size.width - (leftPadding*2), rect.size.height - (topPadding*2));
    
    //CGRect backgroundRect = CGRectMake(leftPadding, topPadding, rect.size.width - (leftPadding*2), rect.size.height - (topPadding));
    
    //CGContextClearRect(context, rect);
    
    if (_selected) {
        addRoundedRect(context, backgroundRect, cornerRadius, self.rectColorSelected.CGColor);
    }
    else {
        addRoundedRect(context, backgroundRect, cornerRadius, self.rectColor.CGColor);
    }
    
    // Add in color section
    CGColorRef lineColor = self.strokeColor.CGColor;
    
    // Add line at left
    CGFloat leftPaddingLine = 10.0;
    CGPoint startPoint = CGPointMake(leftPaddingLine + 1, self.bounds.origin.y);
    CGPoint endPoint = CGPointMake(leftPaddingLine + 1, self.bounds.size.height);
    draw1PxStroke(context, startPoint, endPoint, lineColor);
    
    // Add line at right
    startPoint = CGPointMake(self.bounds.size.width - leftPaddingLine - (1*2), self.bounds.origin.y);
    endPoint = CGPointMake(self.bounds.size.width - leftPaddingLine - (1*2), self.bounds.size.height);
    draw1PxStroke(context, startPoint, endPoint, lineColor);
    
    // Left Background
    leftPadding = 0.0;
    width = 10.0;
    leftBackgroundRect = CGRectMake(leftPadding + 1, self.bounds.origin.y, width, self.bounds.size.height);
    addBackground(context, leftBackgroundRect, self.grayBackgroundColor.CGColor);
    
    // Right Background
    leftPadding = self.bounds.size.width - width;
    CGRect rightBackgroundRect = CGRectMake(leftPadding - 1, self.bounds.origin.y, width, self.bounds.size.height);
    addBackground(context, rightBackgroundRect, self.grayBackgroundColor.CGColor);
    
    //CGColorRef whiteColor = [UIColor whiteColor].CGColor;
    //CGContextSetFillColorWithColor(context, whiteColor);
    //CGContextFillRect(context, self.bounds);
    
    //[self singleCorner:context];
    
    //[self topRoundedCornerView:context];
    
    //addRoundedRectToPath(context, self.bounds, 5.0, 5.0);
    
    
    //CGContextRef context = UIGraphicsGetCurrentContext();
    //--
    //CGContextSetLineWidth(context, strokeWidth);
    //CGContextSetStrokeColorWithColor(context, self.strokeColor.CGColor);
    
    /*
    CGContextSetFillColorWithColor(context, self.rectColor.CGColor);
    
    CGRect rrect = self.bounds;
    
    CGFloat radius = cornerRadius;
    CGFloat width = CGRectGetWidth(rrect);
    CGFloat height = CGRectGetHeight(rrect);
    
    // Make sure corner radius isn't larger than half the shorter side
    if (radius > width/2.0)
        radius = width/2.0;
    if (radius > height/2.0)
        radius = height/2.0;
    
    CGFloat minx = CGRectGetMinX(rrect);
    CGFloat midx = CGRectGetMidX(rrect);
    CGFloat maxx = CGRectGetMaxX(rrect);
    CGFloat miny = CGRectGetMinY(rrect);
    CGFloat midy = CGRectGetMidY(rrect);
    CGFloat maxy = CGRectGetMaxY(rrect);
    CGContextMoveToPoint(context, minx, midy);
    CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
    CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);
    CGContextClosePath(context);
    //CGContextDrawPath(context, kCGPathFillStroke);
    CGContextDrawPath(context, kCGPathFill);*/
    
    /*
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGColorRef purpleColor = [UIColor colorWithRed:122.0/255.0 green:24.0/255.0 blue:120.0/255.0 alpha:1.0].CGColor;
    CGContextSetFillColorWithColor(context, purpleColor);
    CGContextFillRect(context, self.bounds);
    CGRect paperRect = self.bounds;
    addRoundedRectToPath(context, paperRect, 5.0, 5.0);
    
    //addRoundedRect(context, rect, 5.0);*/
    
    /*
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGColorRef whiteColor = [UIColor colorWithRed:1.0 green:1.0
                                             blue:1.0 alpha:1.0].CGColor;
    CGColorRef lightGrayColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0
                                                 blue:230.0/255.0 alpha:1.0].CGColor;
    
    CGRect paperRect = self.bounds;
    
    drawLinearGradient(context, paperRect, whiteColor, lightGrayColor);*/
    
    /*
    // Add a color for red up where the colors are
    CGColorRef redColor = [UIColor colorWithRed:1.0 green:0.0
                                           blue:0.0 alpha:1.0].CGColor;
    
    // Add down at the bottom
    CGRect strokeRect = CGRectInset(rect, 5.0, 5.0);
    
    CGContextSetStrokeColorWithColor(context, redColor);
    CGContextSetLineWidth(context, 1.0);
    CGContextStrokeRect(context, strokeRect);*/
}

- (void)dealloc {
    [strokeColor release];
    [rectColor release];
    [rectColorSelected release];
    [grayBackgroundColor release];
    [super dealloc];
}

- (void)singleCorner:(CGContextRef)context
{
    // Drawing with a white stroke color
    CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0);
    
    CGContextSetRGBFillColor(context, 1.0, 0, 0, 1.0);
    
    // Draw them with a 2.0 stroke width so they are a bit more visible.
    CGContextSetLineWidth(context, 5.0);
    
    //NOTE:Give your view frame heare
    
    CGRect rrect = CGRectMake(60, 90.0, 200.0, 200.0);
    CGFloat radius = 5.0;
    // In order to create the 4 arcs correctly, we need to know the
    // min, mid and max Positions
    // on the x and y lengths of the given rectangle.
    CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
    
    CGFloat miny = CGRectGetMinY(rrect),
    midy = CGRectGetMidY(rrect),
    maxy = CGRectGetMaxY(rrect);
    
    // Start at 1
    CGContextMoveToPoint(context, minx, midy);
    // Add an arc through 2 to 3
    CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
    CGContextAddLineToPoint(context,maxx, miny); //draw line
    CGContextAddLineToPoint(context,maxx, maxy); //draw right line
    CGContextAddLineToPoint(context,minx, maxy); //draw bottom line
    CGContextClosePath(context); //close the path
    // Fill & stroke the path
    CGContextDrawPath(context, kCGPathFillStroke);
}

- (void)topRoundedCornerView:(CGContextRef)context
{
    // Drawing with a white stroke color
    CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0);
    
    CGContextSetRGBFillColor(context, 1.0, 0, 0, 1.0);
    // Draw them with a 2.0 stroke width so they are a bit more visible.
    CGContextSetLineWidth(context, 5.0);
    
    CGRect rrect = self.bounds;
    //CGRect rrect = CGRectMake(0.0, 0.0, 360.0, 240.0);
    CGFloat radius = 10.0;
    // In order to create the 4 arcs correctly, we need to know the min, mid and max positions
    // on the x and y lengths of the given rectangle.
    CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
    
    CGFloat miny = CGRectGetMinY(rrect),
    midy = CGRectGetMidY(rrect),
    maxy = CGRectGetMaxY(rrect);
    
    // Start at 1
    CGContextMoveToPoint(context, minx, midy);
    // Add an arc through 2 to 3
    CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);
    CGContextAddLineToPoint(context,maxx, maxy);
    CGContextAddLineToPoint(context,minx, maxy);
    CGContextClosePath(context);
    // Fill & stroke the path
    CGContextDrawPath(context, kCGPathFillStroke);
    
}

@end
