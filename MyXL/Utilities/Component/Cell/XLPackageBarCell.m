//
//  XLPackageBarCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XLPackageBarCell.h"

@implementation XLPackageBarCell

@synthesize barView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        barView = [[UIView alloc] init];
        [self.contentView addSubview:barView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    barView.frame = frame;
}

- (void)dealloc
{
    [barView release];
    [super dealloc];
}

@end
