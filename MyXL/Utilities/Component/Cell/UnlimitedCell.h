//
//  UnlimitedCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/11/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnlimitedCell : UITableViewCell {
    UILabel *packageNameTitleLabel;
    UILabel *packageNameValueLabel;
    
    UILabel *packageTypeTitleLabel;
    UILabel *packageTypeValueLabel;
    
    UILabel *usageTitleLabel;
    UILabel *usageValueLabel;
    
    UILabel *validityTitleLabel;
    UILabel *validityValueLabel;
    
    UILabel *infoTitleLabel;
    UILabel *infoValueLabel;
}

@property (nonatomic, retain) UILabel *packageNameTitleLabel;
@property (nonatomic, retain) UILabel *packageNameValueLabel;

@property (nonatomic, retain) UILabel *packageTypeTitleLabel;
@property (nonatomic, retain) UILabel *packageTypeValueLabel;

@property (nonatomic, retain) UILabel *usageTitleLabel;
@property (nonatomic, retain) UILabel *usageValueLabel;

@property (nonatomic, retain) UILabel *validityTitleLabel;
@property (nonatomic, retain) UILabel *validityValueLabel;

@property (nonatomic, retain) UILabel *infoTitleLabel;
@property (nonatomic, retain) UILabel *infoValueLabel;

@end
