//
//  PPUCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/20/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PPUCell.h"
#import "Constant.h"

#import "DataManager.h"
#import "CheckUsageModel.h"
#import "AXISnetCommon.h"

@implementation PPUCell

@synthesize packageNameTitleLabel;
@synthesize packageNameValueLabel;
@synthesize packageTypeTitleLabel;
@synthesize packageTypeValueLabel;
@synthesize usageTitleLabel;
@synthesize usageValueLabel;
@synthesize infoTitleLabel;
@synthesize infoValueLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        packageNameTitleLabel = [[UILabel alloc] init];
        packageNameTitleLabel.textAlignment = UITextAlignmentLeft;
        packageNameTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        packageNameTitleLabel.textColor = kDefaultTitleFontGrayColor;
        packageNameTitleLabel.backgroundColor = [UIColor clearColor];
        
        packageNameValueLabel = [[UILabel alloc] init];
        packageNameValueLabel.textAlignment = UITextAlignmentLeft;
        packageNameValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        packageNameValueLabel.textColor = kDefaultPurpleColor;
        packageNameValueLabel.backgroundColor = [UIColor clearColor];
        packageNameValueLabel.numberOfLines = 0;
        packageNameValueLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        packageTypeTitleLabel = [[UILabel alloc] init];
        packageTypeTitleLabel.textAlignment = UITextAlignmentLeft;
        packageTypeTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        packageTypeTitleLabel.textColor = kDefaultTitleFontGrayColor;
        packageTypeTitleLabel.backgroundColor = [UIColor clearColor];
        
        packageTypeValueLabel = [[UILabel alloc] init];
        packageTypeValueLabel.textAlignment = UITextAlignmentLeft;
        packageTypeValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        packageTypeValueLabel.textColor = kDefaultPurpleColor;
        packageTypeValueLabel.backgroundColor = [UIColor clearColor];
        packageTypeValueLabel.numberOfLines = 0;
        packageTypeValueLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        usageTitleLabel = [[UILabel alloc] init];
        usageTitleLabel.textAlignment = UITextAlignmentLeft;
        usageTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        usageTitleLabel.textColor = kDefaultTitleFontGrayColor;
        usageTitleLabel.backgroundColor = [UIColor clearColor];
        
        usageValueLabel = [[UILabel alloc] init];
        usageValueLabel.textAlignment = UITextAlignmentLeft;
        usageValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        usageValueLabel.textColor = kDefaultPurpleColor;
        usageValueLabel.backgroundColor = [UIColor clearColor];
        usageValueLabel.numberOfLines = 0;
        usageValueLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        infoTitleLabel = [[UILabel alloc] init];
        infoTitleLabel.textAlignment = UITextAlignmentLeft;
        infoTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        infoTitleLabel.textColor = kDefaultTitleFontGrayColor;
        infoTitleLabel.backgroundColor = [UIColor clearColor];
        
        infoValueLabel = [[UILabel alloc] init];
        infoValueLabel.textAlignment = UITextAlignmentLeft;
        infoValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        infoValueLabel.textColor = kDefaultPurpleColor;
        infoValueLabel.backgroundColor = [UIColor clearColor];
        infoValueLabel.numberOfLines = 0;
        infoValueLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        [self.contentView addSubview:packageNameTitleLabel];
        [self.contentView addSubview:packageNameValueLabel];
        [self.contentView addSubview:packageTypeTitleLabel];
        [self.contentView addSubview:packageTypeValueLabel];
        [self.contentView addSubview:usageTitleLabel];
        [self.contentView addSubview:usageValueLabel];
        [self.contentView addSubview:infoTitleLabel];
        [self.contentView addSubview:infoValueLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    //CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 20.0;
    CGFloat topPadding = 5.0;
    CGFloat width = (self.frame.size.width - (leftPadding*4)) / 2; // 150
    CGFloat height = 20.0;
    CGFloat deduction = 5.0;
    
    CGRect frame;
    
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width,
                       height);
    packageNameTitleLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       packageNameTitleLabel.frame.origin.y + packageNameTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    packageNameValueLabel.frame = frame;
    
    // Usage
    frame = CGRectMake(leftPadding,
                       packageNameValueLabel.frame.origin.y + packageNameValueLabel.frame.size.height + topPadding,
                       width,
                       height);
    usageTitleLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       usageTitleLabel.frame.origin.y + usageTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    usageValueLabel.frame = frame;
    
    // Info
    frame = CGRectMake(leftPadding,
                       usageValueLabel.frame.origin.y + usageValueLabel.frame.size.height + topPadding,
                       width,
                       height);
    infoTitleLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       infoTitleLabel.frame.origin.y + infoTitleLabel.frame.size.height - deduction,
                       (width*2) - (leftPadding*2),
                       height);
    infoValueLabel.frame = frame;
    /*
    DataManager *sharedData = [DataManager sharedInstance];
    CheckUsageModel *checkUsageModel = [sharedData.checkUsageData objectAtIndex:0];
    NSString *infoValue = checkUsageModel.info;
    CGSize infoValueSize = calculateExpectedSize(infoValueLabel, infoValue);
    frame = infoValueLabel.frame;
    frame.size.height = infoValueSize.height;
    infoValueLabel.frame = frame;*/
    
    [infoValueLabel sizeToFit];
    
    // Right Side
    //CGFloat rightSideTextPadding = leftPadding + packageNameTitleLabel.frame.size.width;
    CGFloat rightSideTextPadding = self.frame.size.width/2;
    frame = CGRectMake(rightSideTextPadding,
                       topPadding,
                       width,
                       height);
    packageTypeTitleLabel.frame = frame;
    
    frame = CGRectMake(rightSideTextPadding,
                       packageTypeTitleLabel.frame.origin.y + packageTypeTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    packageTypeValueLabel.frame = frame;
}

- (void)dealloc
{
    [packageNameTitleLabel release];
    [packageNameValueLabel release];
    [packageTypeTitleLabel release];
    [packageTypeValueLabel release];
    [usageTitleLabel release];
    [usageValueLabel release];
    [infoTitleLabel release];
    [infoValueLabel release];
    [super dealloc];
}

@end
