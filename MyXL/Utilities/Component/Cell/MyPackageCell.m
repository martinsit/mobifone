//
//  MyPackageCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "MyPackageCell.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "Language.h"

@implementation MyPackageCell

@synthesize packageTitleLabel;
@synthesize packageValueLabel;

@synthesize validityTitleLabel;
@synthesize validityValueLabel;

@synthesize usageTitleLabel;
@synthesize usageValueLabel;

@synthesize typeTitleLabel;
@synthesize typeValueLabel;

@synthesize separatorView;
@synthesize stopButton;
@synthesize upgradeButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        packageTitleLabel = [[UILabel alloc] init];
        packageTitleLabel.textAlignment = UITextAlignmentLeft;
        packageTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        packageTitleLabel.textColor = kDefaultTitleFontGrayColor;
        packageTitleLabel.backgroundColor = [UIColor clearColor];
        //packageTitleLabel.text = @"PACKAGE";
        
        packageValueLabel = [[UILabel alloc] init];
        packageValueLabel.textAlignment = UITextAlignmentLeft;
        packageValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        packageValueLabel.textColor = kDefaultPurpleColor;
        packageValueLabel.backgroundColor = [UIColor clearColor];
        packageValueLabel.numberOfLines = 0;
        packageValueLabel.lineBreakMode = UILineBreakModeWordWrap;
        //packageValueLabel.text = @"Monthly Ultimate Unlimited";
        
        validityTitleLabel = [[UILabel alloc] init];
        validityTitleLabel.textAlignment = UITextAlignmentLeft;
        validityTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        validityTitleLabel.textColor = kDefaultTitleFontGrayColor;
        validityTitleLabel.backgroundColor = [UIColor clearColor];
        //validityTitleLabel.text = @"VALIDITY";
        
        validityValueLabel = [[UILabel alloc] init];
        validityValueLabel.textAlignment = UITextAlignmentLeft;
        validityValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        validityValueLabel.textColor = kDefaultPurpleColor;
        validityValueLabel.backgroundColor = [UIColor clearColor];
        //validityValueLabel.text = @"08-Feb-2013";
        
        usageTitleLabel = [[UILabel alloc] init];
        usageTitleLabel.textAlignment = UITextAlignmentLeft;
        usageTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        usageTitleLabel.textColor = kDefaultTitleFontGrayColor;
        usageTitleLabel.backgroundColor = [UIColor clearColor];
        
        usageValueLabel = [[UILabel alloc] init];
        usageValueLabel.textAlignment = UITextAlignmentLeft;
        usageValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        usageValueLabel.textColor = kDefaultPurpleColor;
        usageValueLabel.backgroundColor = [UIColor clearColor];
        
        typeTitleLabel = [[UILabel alloc] init];
        typeTitleLabel.textAlignment = UITextAlignmentLeft;
        typeTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        typeTitleLabel.textColor = kDefaultTitleFontGrayColor;
        typeTitleLabel.backgroundColor = [UIColor clearColor];
        //typeTitleLabel.text = @"TYPE";
        
        typeValueLabel = [[UILabel alloc] init];
        typeValueLabel.textAlignment = UITextAlignmentLeft;
        typeValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        typeValueLabel.textColor = kDefaultPurpleColor;
        typeValueLabel.backgroundColor = [UIColor clearColor];
        //typeValueLabel.text = @"Unlimited";
        
        separatorView = [[UIView alloc] init];
        separatorView.backgroundColor = kDefaultWhiteSmokeColor;
        
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        
        stopButton = createButtonWithFrame(CGRectMake(0, 0, 60, kDefaultButtonHeight),
                                                 [Language get:@"stop" alter:nil],
                                                 buttonFont,
                                                 kDefaultBaseColor,
                                                 kDefaultPinkColor,
                                                 kDefaultPurpleColor);
        
        upgradeButton = createButtonWithFrame(CGRectMake(0, 0, 60, kDefaultButtonHeight),
                                           [Language get:@"upgrade" alter:nil],
                                           buttonFont,
                                           kDefaultBaseColor,
                                           kDefaultPinkColor,
                                           kDefaultPurpleColor);
        
        [self.contentView addSubview:packageTitleLabel];
        [self.contentView addSubview:packageValueLabel];
        
        [self.contentView addSubview:validityTitleLabel];
        [self.contentView addSubview:validityValueLabel];
        
        [self.contentView addSubview:usageTitleLabel];
        [self.contentView addSubview:usageValueLabel];
        
        [self.contentView addSubview:typeTitleLabel];
        [self.contentView addSubview:typeValueLabel];
        
        [self.contentView addSubview:separatorView];
        [self.contentView addSubview:stopButton];
        [self.contentView addSubview:upgradeButton];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    //CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 20.0;
    CGFloat topPadding = 5.0;
    CGFloat width = (self.frame.size.width - (10*2) - (leftPadding*2))/2.0;//150
    CGFloat height = 20.0;
    CGFloat deduction = 5.0;
    
    CGRect frame;
    
    // Left Side
    // Package
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width,
                       height);
    packageTitleLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       packageTitleLabel.frame.origin.y + packageTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    packageValueLabel.frame = frame;
    [packageValueLabel sizeToFit];
    
    // Usage
    frame = CGRectMake(leftPadding,
                       packageValueLabel.frame.origin.y + packageValueLabel.frame.size.height + topPadding,
                       width,
                       height);
    usageTitleLabel.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       usageTitleLabel.frame.origin.y + usageTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    usageValueLabel.frame = frame;
    
    // Right Side
    // Type
    //CGFloat rightSideTextPadding = self.frame.size.width/2;
    CGFloat rightSideTextPadding = packageTitleLabel.frame.origin.x + packageTitleLabel.frame.size.width;
    frame = CGRectMake(rightSideTextPadding,
                       topPadding,
                       width,
                       height);
    typeTitleLabel.frame = frame;
    
    frame = CGRectMake(rightSideTextPadding,
                       typeTitleLabel.frame.origin.y + typeTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    typeValueLabel.frame = frame;
    
    // Validity
    frame = CGRectMake(rightSideTextPadding,
                       usageTitleLabel.frame.origin.y,
                       width,
                       height);
    validityTitleLabel.frame = frame;
    
    frame = CGRectMake(rightSideTextPadding,
                       validityTitleLabel.frame.origin.y + validityTitleLabel.frame.size.height - deduction,
                       width,
                       height);
    validityValueLabel.frame = frame;
    
    CGFloat buttonPadding = 5.0;
    CGFloat tableWidth = 0.0;
    frame = stopButton.frame;
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        tableWidth = self.frame.size.width - (45*2);
    }
    else {
        tableWidth = self.frame.size.width - (10*2);
    }
    
    frame.origin.x = tableWidth - stopButton.frame.size.width - leftPadding - buttonPadding;
    frame.origin.y = validityValueLabel.frame.origin.y + validityValueLabel.frame.size.height + topPadding + buttonPadding;
    stopButton.frame = frame;
    
    frame = upgradeButton.frame;
    frame.origin.x = tableWidth - stopButton.frame.size.width - upgradeButton.frame.size.width - 10 - leftPadding - buttonPadding;
    frame.origin.y = validityValueLabel.frame.origin.y + validityValueLabel.frame.size.height + topPadding + buttonPadding;
    upgradeButton.frame = frame;
    
    frame = CGRectMake(leftPadding,
                       validityValueLabel.frame.origin.y + validityValueLabel.frame.size.height + topPadding,
                       tableWidth - (leftPadding*2),
                       kDefaultButtonHeight + (buttonPadding*2));
    separatorView.frame = frame;
}

- (void)dealloc
{
    [packageTitleLabel release];
    [packageValueLabel release];
    
    [validityTitleLabel release];
    [validityValueLabel release];
    
    [usageTitleLabel release];
    [usageValueLabel release];
    
    [typeTitleLabel release];
    [typeValueLabel release];
    
    [separatorView release];
    [stopButton release];
    [upgradeButton release];
    
    [super dealloc];
}

@end
