//
//  XLPackageAllowanceCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XLPackageAllowanceCell.h"
#import "Constant.h"

@implementation XLPackageAllowanceCell

@synthesize legendView;
@synthesize allowanceName;
@synthesize remaining;
@synthesize lineView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        legendView = [[UIView alloc] init];
        legendView.backgroundColor = kDefaultNavyBlueColor;
        
        allowanceName = [[UILabel alloc] init];
        allowanceName.textAlignment = UITextAlignmentLeft;
        allowanceName.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
        allowanceName.textColor = kDefaultNavyBlueColor;
        //allowanceName.backgroundColor = [UIColor redColor];
        
        remaining = [[UILabel alloc] init];
        remaining.textAlignment = UITextAlignmentRight;
        remaining.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
        remaining.textColor = kDefaultNavyBlueColor;
        //remaining.backgroundColor = [UIColor yellowColor];
        
        lineView = [[UIView alloc] init];
        lineView.backgroundColor = kDefaultSeparatorColor;
        
        [self.contentView addSubview:legendView];
        [self.contentView addSubview:allowanceName];
        [self.contentView addSubview:remaining];
        [self.contentView addSubview:lineView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    CGFloat labelLeftPadding = 15.0;
    CGFloat legendWidth = 10.0;
    CGFloat height = 30.0;
    CGFloat remainingWidth = 80.0;
    
    CGRect frame;
    
    frame = CGRectMake(labelLeftPadding,
                       (contentRect.size.height - legendWidth)/2,
                       legendWidth,
                       legendWidth);
    legendView.frame = frame;
    legendView.layer.cornerRadius = legendWidth/2;
    legendView.layer.masksToBounds = YES;
    
    frame = CGRectMake(legendView.frame.origin.x + legendView.frame.size.width + kDefaultComponentPadding,
                       (contentRect.size.height - height)/2,
                       contentRect.size.width - (legendView.frame.origin.x + legendView.frame.size.width + kDefaultComponentPadding) - labelLeftPadding - remainingWidth,
                       height);
    allowanceName.frame = frame;
    
    frame = CGRectMake(contentRect.size.width - labelLeftPadding - remainingWidth,
                       (contentRect.size.height - height)/2,
                       remainingWidth,
                       height);
    remaining.frame = frame;
    
    frame = CGRectMake(legendView.frame.origin.x,
                       allowanceName.frame.origin.y + allowanceName.frame.size.height - 1,
                       contentRect.size.width - (labelLeftPadding*2),
                       1.0);
    lineView.frame = frame;
}

- (void)dealloc
{
    [legendView release];
    [allowanceName release];
    [remaining release];
    [lineView release];
    [super dealloc];
}

@end
