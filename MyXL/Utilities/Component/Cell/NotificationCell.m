//
//  NotificationCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/19/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "NotificationCell.h"
#import "Constant.h"
#import "AXISnetCommon.h"

@implementation NotificationCell

@synthesize notifTypeView;
@synthesize dateLabel;
@synthesize monthLabel;
@synthesize notifLabel;
@synthesize fromLabel;
@synthesize arrowLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        notifTypeView = [[UIImageView alloc] init];
        notifTypeView.backgroundColor = [UIColor clearColor];
        UIImage *backgroundImage = imageFromColor([UIColor redColor]);
        notifTypeView.image = backgroundImage;
        
        monthLabel = [[UILabel alloc] init];
        monthLabel.textAlignment = UITextAlignmentCenter;
        monthLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        monthLabel.textColor = kDefaultBlackColor;
        monthLabel.backgroundColor = [UIColor clearColor];
        
        dateLabel = [[UILabel alloc] init];
        dateLabel.textAlignment = UITextAlignmentCenter;
        dateLabel.font = [UIFont fontWithName:kDefaultFontLight size:24.0];
        dateLabel.textColor = kDefaultBlackColor;
        dateLabel.backgroundColor = [UIColor clearColor];
        
        fromLabel = [[UILabel alloc] init];
        fromLabel.textAlignment = UITextAlignmentLeft;
        fromLabel.font = [UIFont fontWithName:kDefaultFont size:18.0];
        fromLabel.textColor = kDefaultBlackColor;
        fromLabel.lineBreakMode = UILineBreakModeTailTruncation;
        fromLabel.numberOfLines = 0;
        fromLabel.backgroundColor = [UIColor clearColor];
        
        notifLabel = [[UILabel alloc] init];
        notifLabel.textAlignment = UITextAlignmentLeft;
        notifLabel.font = [UIFont fontWithName:kDefaultFontLight size:18.0];
        notifLabel.textColor = kDefaultBlackColor;
        notifLabel.lineBreakMode = UILineBreakModeTailTruncation;
        notifLabel.numberOfLines = 0;
        notifLabel.backgroundColor = [UIColor clearColor];
        
        arrowLabel = [[UILabel alloc] init];
        arrowLabel.textAlignment = UITextAlignmentLeft;
        arrowLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:18.0];
        arrowLabel.textColor = kDefaultSeparatorColor;
        arrowLabel.backgroundColor = [UIColor clearColor];
        arrowLabel.text = @"d";
        
        [self.contentView addSubview:notifTypeView];
        [self.contentView addSubview:monthLabel];
        [self.contentView addSubview:dateLabel];
        [self.contentView addSubview:notifLabel];
        [self.contentView addSubview:fromLabel];
        [self.contentView addSubview:arrowLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 15.0;
    CGFloat topPadding = 5.0;
    
    CGFloat height = contentRect.size.height - (topPadding*2);
    CGFloat dateLabelWidth = 60.0;
    CGFloat dateLabelHeight = 20.0;
    
    CGFloat widthArrow = 30.0;
    CGFloat heightArrow = 30.0;
    
    CGRect frame;
    
    if (IS_IPAD) {
        dateLabelWidth = dateLabelWidth*2;
        frame = CGRectMake(leftPadding + notifTypeView.frame.size.width,
                           topPadding,
                           dateLabelWidth,
                           dateLabelHeight);
        monthLabel.frame = frame;
        
        frame = CGRectMake(leftPadding + notifTypeView.frame.size.width,
                           topPadding + monthLabel.frame.size.height,
                           dateLabelWidth,
                           dateLabelHeight*2);
        dateLabel.frame = frame;
        
        frame = CGRectMake(leftPadding + monthLabel.frame.size.width,
                           topPadding,
                           2.0,
                           height);
        notifTypeView.frame = frame;
        
        frame = CGRectMake(notifTypeView.frame.origin.x + notifTypeView.frame.size.width + kDefaultComponentPadding,
                           topPadding,
                           contentRect.size.width - (leftPadding*2) - notifTypeView.frame.size.width - dateLabel.frame.size.width - (dateLabelWidth/2) - widthArrow, //175
                           20);
        fromLabel.frame = frame;
        
        frame = CGRectMake(notifTypeView.frame.origin.x + notifTypeView.frame.size.width + kDefaultComponentPadding,
                           fromLabel.frame.origin.y + fromLabel.frame.size.height,
                           contentRect.size.width - (leftPadding*2) - notifTypeView.frame.size.width - dateLabel.frame.size.width - widthArrow, //175
                           height - 20);
        notifLabel.frame = frame;
        
        arrowLabel.textAlignment = UITextAlignmentRight;
        frame = CGRectMake(contentRect.size.width - widthArrow - 15.0,
                           (contentRect.size.height - widthArrow)/2,
                           widthArrow,
                           heightArrow);
        arrowLabel.frame = frame;
    }
    else {
        frame = CGRectMake(leftPadding,
                           topPadding,
                           dateLabelWidth,
                           dateLabelHeight);
        monthLabel.frame = frame;
        
        frame = CGRectMake(leftPadding,
                           topPadding + monthLabel.frame.size.height,
                           dateLabelWidth,
                           dateLabelHeight*2);
        dateLabel.frame = frame;
        
        frame = CGRectMake(kDefaultComponentPadding + monthLabel.frame.size.width + monthLabel.frame.origin.x,
                           topPadding,
                           2.0,
                           height);
        notifTypeView.frame = frame;
        
        frame = CGRectMake(notifTypeView.frame.origin.x + notifTypeView.frame.size.width + kDefaultComponentPadding,
                           topPadding,
                           contentRect.size.width - (leftPadding*2) - notifTypeView.frame.size.width - dateLabel.frame.size.width - widthArrow, //175
                           20);
        fromLabel.frame = frame;
        
        frame = CGRectMake(notifTypeView.frame.origin.x + notifTypeView.frame.size.width + kDefaultComponentPadding,
                           fromLabel.frame.origin.y + fromLabel.frame.size.height,
                           contentRect.size.width - (leftPadding*2) - notifTypeView.frame.size.width - dateLabel.frame.size.width - kDefaultComponentPadding - widthArrow, //175
                           height - 20);
        notifLabel.frame = frame;
        
        frame = CGRectMake(fromLabel.frame.origin.x + fromLabel.frame.size.width,
                           (contentRect.size.height - widthArrow)/2,
                           widthArrow,
                           heightArrow);
        arrowLabel.frame = frame;
    }
    
    
}

- (void)dealloc
{
    [notifTypeView release];
    [dateLabel release];
    [monthLabel release];
    [notifLabel release];
    [fromLabel release];
    [arrowLabel release];
    [super dealloc];
}

@end
