//
//  BalanceHomeCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 11/18/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BalanceHomeCell.h"
#import "Constant.h"
#import "AXISnetCommon.h"

@implementation BalanceHomeCell

//@synthesize iconLabel;
//@synthesize titleLabel;
@synthesize balanceLabel;
@synthesize validityLabel;
@synthesize chevronButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        /*
        iconLabel = [[UILabel alloc] init];
        iconLabel.textAlignment = UITextAlignmentLeft;
        iconLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize];
        iconLabel.textColor = kDefaultWhiteColor;
        iconLabel.backgroundColor = [UIColor clearColor];
        
        titleLabel = [[UILabel alloc] init];
        titleLabel.textAlignment = UITextAlignmentLeft;
        titleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
        titleLabel.textColor = kDefaultWhiteColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = UILineBreakModeWordWrap;*/
        
        balanceLabel = [[UILabel alloc] init];
        balanceLabel.textAlignment = UITextAlignmentLeft;
        balanceLabel.font = [UIFont fontWithName:kDefaultFont size:18.0];
        balanceLabel.textColor = kDefaultNavyBlueColor;
        balanceLabel.backgroundColor = [UIColor clearColor];
        balanceLabel.numberOfLines = 0;
        balanceLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        validityLabel = [[UILabel alloc] init];
        validityLabel.textAlignment = UITextAlignmentRight;
        validityLabel.font = [UIFont fontWithName:kDefaultFont size:11.0];
        validityLabel.textColor = kDefaultNavyBlueColor;
        validityLabel.backgroundColor = [UIColor clearColor];
        validityLabel.numberOfLines = 0;
        validityLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        chevronButton = createButtonWithFrame(CGRectMake(0, 0, 30, kDefaultButtonHeight),
                                           @"d",
                                           [UIFont fontWithName:kDefaultFontKSAN size:17.0],
                                           kDefaultWhiteColor,
                                           kDefaultNavyBlueColor,
                                           kDefaultNavyBlueColor);
        [chevronButton setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        
        //[self.contentView addSubview:iconLabel];
        //[self.contentView addSubview:titleLabel];
        [self.contentView addSubview:balanceLabel];
        [self.contentView addSubview:validityLabel];
        [self.contentView addSubview:chevronButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect contentRect = self.contentView.bounds;
    
    CGFloat topPadding = 0.0;
    CGFloat labelLeftPadding = 15.0; //25
    //CGFloat symbolWidth = 30;
    CGFloat height = 50.0; //28 kDefaultCellHeight
    
    CGFloat buttonWidth = 30.0;
    
    CGRect frame;
    
    //frame = CGRectMake(labelLeftPadding, topPadding, (contentRect.size.width - (labelLeftPadding*2))/2, height*1.5);
    frame = CGRectMake(labelLeftPadding, topPadding, (contentRect.size.width - (labelLeftPadding*2))/2, height);
    balanceLabel.frame = frame;
    
    
    //frame = CGRectMake(balanceLabel.frame.origin.x + balanceLabel.frame.size.width, topPadding, (contentRect.size.width - (labelLeftPadding*2))/2, height*1.5);
    frame = CGRectMake(balanceLabel.frame.origin.x + balanceLabel.frame.size.width, topPadding, (contentRect.size.width - (labelLeftPadding*2))/2 - buttonWidth - 5.0, height);
    validityLabel.frame = frame;
    
    
    frame = CGRectMake(contentRect.size.width - buttonWidth - labelLeftPadding,
                       (contentRect.size.height - buttonWidth)/2,
                       buttonWidth,
                       buttonWidth);
    chevronButton.frame = frame;
    
    /*
    frame = CGRectMake(labelLeftPadding, topPadding, symbolWidth, height);
    iconLabel.frame = frame;
    
    if ([iconLabel.text length] > 0) {
        frame = CGRectMake(labelLeftPadding + symbolWidth, topPadding, contentRect.size.width - ((labelLeftPadding + symbolWidth)*2), height);
        titleLabel.frame = frame;
        
        frame = CGRectMake(labelLeftPadding, titleLabel.frame.origin.y + titleLabel.frame.size.height, (contentRect.size.width - (labelLeftPadding*2))/2, height*2);
        balanceLabel.frame = frame;
        
        frame = CGRectMake(balanceLabel.frame.origin.x + balanceLabel.frame.size.width, titleLabel.frame.origin.y + titleLabel.frame.size.height, (contentRect.size.width - (labelLeftPadding*2))/2, height*2);
        validityLabel.frame = frame;
    }
    else {
        frame = CGRectMake(labelLeftPadding, topPadding, contentRect.size.width - ((labelLeftPadding*2) + symbolWidth), height);
        titleLabel.frame = frame;
    }*/
    
    //[balanceLabel sizeToFit];
    //[validityLabel sizeToFit];
}

- (void)dealloc
{
    //[iconLabel release];
    //[titleLabel release];
    [balanceLabel release];
    [validityLabel release];
    [chevronButton release];
    [super dealloc];
}

@end
