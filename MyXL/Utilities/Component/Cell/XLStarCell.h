//
//  XLStarCell.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 1/9/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XLStarCell : UITableViewCell {
    UIImageView *imageView;
    UILabel *descLabel;
}

@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) UILabel *descLabel;

@end
