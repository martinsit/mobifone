//
//  RectBackground.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 11/20/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "RectBackground.h"
#import "AXISnetCommon.h"
#import "Constant.h"

@implementation RectBackground

@synthesize rectColor;
@synthesize rectColorSelected;
@synthesize selected = _selected;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = NO;
        self.rectColor = kDefaultNavyBlueColor;
        self.rectColorSelected = kDefaultNavyBlueColor;
        [super setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withBasicColor:(UIColor*)basicColor withSelectedColor:(UIColor*)selectedColor {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = NO;
        self.rectColor = basicColor;
        self.rectColorSelected = selectedColor;
        [super setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGRect backgroundRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    
    if (_selected) {
        addBackground(context,backgroundRect,self.rectColorSelected.CGColor);
    }
    else {
        addBackground(context,backgroundRect,self.rectColor.CGColor);
    }
}

- (void)dealloc {
    [rectColor release];
    [rectColorSelected release];
    [super dealloc];
}

@end
