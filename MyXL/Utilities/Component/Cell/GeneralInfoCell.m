//
//  GeneralInfoCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/8/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "GeneralInfoCell.h"
#import "Constant.h"

@implementation GeneralInfoCell

@synthesize infoLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        infoLabel = [[UILabel alloc] init];
        infoLabel.textAlignment = UITextAlignmentLeft;
        infoLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        infoLabel.textColor = kDefaultPurpleColor;
        infoLabel.backgroundColor = [UIColor clearColor];
        infoLabel.numberOfLines = 0;
        infoLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        [self.contentView addSubview:infoLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    //CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 15.0;
    CGFloat topPadding = 5.0;
    
    CGRect frame;
    CGFloat tableWidth = 0.0;
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        tableWidth = self.frame.size.width - (45*2);
    }
    else {
        tableWidth = self.frame.size.width - (10*2);
    }
    
    CGFloat width = tableWidth - (leftPadding*2);
    CGFloat height = 20.0;
    
    frame = CGRectMake(leftPadding,
                       topPadding,
                       width,
                       height);
    infoLabel.frame = frame;
    [infoLabel sizeToFit];
}

- (void)dealloc
{
    [infoLabel release];
    [super dealloc];
}

@end
