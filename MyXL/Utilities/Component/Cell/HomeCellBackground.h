//
//  HomeCellBackground.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCellBackground : UIView {
    UIColor *rectColor;
    UIColor *rectColorSelected;
    CGFloat cornerRadius;
    BOOL _selected;
}

@property (nonatomic, retain) UIColor *rectColor;
@property (nonatomic, retain) UIColor *rectColorSelected;
@property CGFloat cornerRadius;
@property  BOOL selected;

@end
