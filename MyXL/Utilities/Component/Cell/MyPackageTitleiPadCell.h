//
//  MyPackageTitleiPadCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/15/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPackageTitleiPadCell : UITableViewCell {
    UILabel *packageTitleLabel;
    UILabel *typeTitleLabel;
    UILabel *usageTitleLabel;
    UILabel *validityTitleLabel;
}

@property (nonatomic, retain) UILabel *packageTitleLabel;
@property (nonatomic, retain) UILabel *typeTitleLabel;
@property (nonatomic, retain) UILabel *usageTitleLabel;
@property (nonatomic, retain) UILabel *validityTitleLabel;

@end
