//
//  TransactionHistoryCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/25/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionHistoryCell : UITableViewCell {
    UILabel *iconLabel;
    UILabel *typeLabel;
    UILabel *timeLabel;
    UILabel *destinationLabel;
    UILabel *durationLabel;
    UILabel *chargingLabel;
    
    UIView *lineView;
}

@property (nonatomic, retain) UILabel *iconLabel;
@property (nonatomic, retain) UILabel *typeLabel;
@property (nonatomic, retain) UILabel *timeLabel;
@property (nonatomic, retain) UILabel *destinationLabel;
@property (nonatomic, retain) UILabel *durationLabel;
@property (nonatomic, retain) UILabel *chargingLabel;

@property (nonatomic, retain) UIView *lineView;

@end
