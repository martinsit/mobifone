//
//  ContentView.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/15/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ContentView.h"
#import "AXISnetCommon.h"
#import "Constant.h"

@implementation ContentView

@synthesize strokeColor;
@synthesize rectColor;
//@synthesize grayBackgroundColor;
@synthesize strokeWidth;
@synthesize cornerRadius;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = NO;
        self.strokeColor = kDefaultBlueColor;
        //self.rectColor = kDefaultBaseColor;
        self.rectColor = kDefaultWhiteColor;
        //self.grayBackgroundColor = kDefaultButtonGrayColor;
        self.strokeWidth = kDefaultStrokeWidth;
        self.cornerRadius = kDefaultCornerRadius;
        //self.backgroundColor = kDefaultButtonGrayColor;
        self.backgroundColor = kDefaultWhiteColor;
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect strokeRect = rect;
    strokeRect = rectFor1PxStroke(strokeRect);
    bottomRoundedRect(context, strokeRect, cornerRadius, self.rectColor.CGColor, self.strokeColor.CGColor, strokeWidth);
}

- (void)dealloc {
    [strokeColor release];
    [rectColor release];
    [super dealloc];
}

@end
