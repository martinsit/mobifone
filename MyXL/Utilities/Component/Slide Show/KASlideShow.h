//
//  KASlideShow.h
//
//  Created by Alexis Creuzot on 23/01/13.
//  Copyright (c) 2013 Alexis Creuzot. All rights reserved.
//

#import "AsyncImageView.h"

#import "MenuModel.h"

typedef NS_ENUM(NSInteger, KASlideShowTransitionType) {
    KASlideShowTransitionFade,
    KASlideShowTransitionSlide
};

@class KASlideShowDelegate;
@protocol KASlideShowDelegate <NSObject>
@optional
- (void) kaSlideShowDidNext;
- (void) kaSlideShowDidPrevious;
- (void) kaSlideShowDidSelectBanner:(int)selectedPackage;
@end

@interface KASlideShow : UIView

@property (nonatomic, strong) IBOutlet id <KASlideShowDelegate> delegate;

@property float delay;
@property float transitionDuration;
@property (atomic) KASlideShowTransitionType transitionType;
@property (atomic) UIViewContentMode imagesContentMode;
@property (strong,nonatomic) NSMutableArray * images;

@property (strong,nonatomic) UIImageView * topImageView;
@property (strong,nonatomic) UIImageView * bottomImageView;

//@property (strong,nonatomic) AsyncImageView2 * topImageView;
//@property (strong,nonatomic) AsyncImageView2 * bottomImageView;

- (void) addImagesFromResources:(NSArray *) names;
- (void) addImage:(UIImage *) image;
- (void) start;
- (void) stop;
- (void) previous;
- (void) next;

- (void) addImagesFromURL:(NSArray *)urls;

@end

