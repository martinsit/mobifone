//
//  KASlideShow.m
//
// Copyright 2013 Alexis Creuzot
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#import "KASlideShow2.h"

#define kSwipeTransitionDuration 0.25

typedef NS_ENUM(NSInteger, KASlideShowSlideMode) {
    KASlideShowSlideModeForward,
    KASlideShowSlideModeBackward
};

@interface KASlideShow2()
@property (atomic) BOOL doStop;
@property (atomic) BOOL isAnimating;
@property (strong,nonatomic) AsyncImageView * topImageView;
@property (strong,nonatomic) AsyncImageView * bottomImageView;
@end

@implementation KASlideShow2

@synthesize delegate;
@synthesize delay;
@synthesize transitionDuration;
@synthesize transitionType;
@synthesize images;

- (void)awakeFromNib
{
    [self setDefaultValues];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setDefaultValues];
    }
    return self;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
	
	// Do not reposition the embedded imageViews.
	frame.origin.x = 0;
	frame.origin.y = 0;
	
    _topImageView.frame = frame;
    _bottomImageView.frame = frame;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (!CGRectEqualToRect(self.bounds, _topImageView.bounds)) {
        _topImageView.frame = self.bounds;
//        _topTextView.frame = self.bounds;
    }
    
    if (!CGRectEqualToRect(self.bounds, _bottomImageView.bounds)) {
        _bottomImageView.frame = self.bounds;
//        _bottomTextView.frame = self.bounds;
    }
}

- (void)setDefaultValues
{
    self.clipsToBounds = YES;
    self.images = [NSMutableArray array];
    _currentIndex = 0;
    delay = 3;
    
    transitionDuration = 1;
    transitionType = KASlideShowTransitionFade;
    _doStop = YES;
    _isAnimating = NO;
    
    _topImageView = [[AsyncImageView alloc] initWithFrame:self.bounds];
    _bottomImageView = [[AsyncImageView alloc] initWithFrame:self.bounds];
//    _topTextView = [[UITextView alloc] initWithFrame:self.bounds];
//    _topTextView.textColor = [UIColor blackColor];
//    _topTextView.hidden = YES;
//    _topTextView.userInteractionEnabled = NO;
//    _bottomTextView = [[UITextView alloc] initWithFrame:self.bounds];
//    _bottomTextView.textColor = [UIColor blackColor];
//    _bottomTextView.hidden = YES;
//    _bottomTextView.userInteractionEnabled = NO;
    
    _topImageView.autoresizingMask = _bottomImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _topImageView.clipsToBounds = YES;
    _bottomImageView.clipsToBounds = YES;
    
//    _topTextView.autoresizingMask = _bottomTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
//    _topTextView.clipsToBounds = YES;
//    _bottomTextView.clipsToBounds = YES;
    [self setImagesContentMode:UIViewContentModeScaleAspectFit];
    
    [self addSubview:_bottomImageView];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":_bottomImageView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":_bottomImageView}]];
    
    [self addSubview:_topImageView];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":_topImageView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":_topImageView}]];
    
//    [self addSubview:_bottomTextView];
//    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":_bottomTextView}]];
//    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":_bottomTextView}]];
//    
//    [self addSubview:_topTextView];
//    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":_topTextView}]];
//    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":_topTextView}]];
}

- (void) setImagesContentMode:(UIViewContentMode)mode
{
    _topImageView.contentMode = mode;
    _bottomImageView.contentMode = mode;
}

- (UIViewContentMode) imagesContentMode
{
    return _topImageView.contentMode;
}

- (void) addGesture:(KASlideShowGestureType)gestureType
{
    switch (gestureType)
    {
        case KASlideShowGestureTap:
            [self addGestureTap];
            break;
        case KASlideShowGestureSwipe:
            [self addGestureSwipe];
            break;
        case KASlideShowGestureAll:
            [self addGestureTap];
            [self addGestureSwipe];
            break;
        default:
            break;
    }
}

- (void) removeGestures
{
    self.gestureRecognizers = nil;
}

- (void) addImagesFromResources:(NSArray *) names
{
    for(NSString * name in names){
        [self addImage:[UIImage imageNamed:name]];
    }
}

-(void)addImagesFromURLs:(NSArray *)URLs
{
    for(NSString *url in URLs){
        [self addImageFromURL:url];
    }
}

- (void) setImagesDataSource:(NSMutableArray *)array {
    self.images = array;
    _topImageView.image = [array firstObject];
}

-(void)addImageFromURL:(NSString *)strURL
{
    [self.images addObject:strURL];    
    NSURL *url = [NSURL URLWithString:strURL];
    if(url)
    {
        if([self.images count] == 1){
            _topImageView.imageURL = url;
        }else if([self.images count] == 2){
            _bottomImageView.imageURL = url;
        }
    }
}

- (void) addImage:(UIImage*) image
{
    [self.images addObject:image];
    
    if([self.images count] == 1){
        _topImageView.image = image;
    }else if([self.images count] == 2){
        _bottomImageView.image = image;
    }
}

- (void) emptyAndAddImagesFromResources:(NSArray *)names
{
    [self.images removeAllObjects];
    _currentIndex = 0;
    [self addImagesFromResources:names];
}

- (void) emptyAndAddImages:(NSArray *)imageArray
{
    [self.images removeAllObjects];
    _currentIndex = 0;
    for (UIImage *image in imageArray){
        [self addImage:image];
    }
}

- (void) start
{
    _doStop = NO;
    [self next];
}

- (void) next
{
    if(! _isAnimating && ([self.images count] >1 || self.dataSource)) {
        
        if ([self.delegate respondsToSelector:@selector(kaSlideShowWillShowNext:)]) [self.delegate kaSlideShowWillShowNext:self];
        
        if(!_isResourceFromURL)
        {
            // Next Image
            if (self.dataSource) {
                _topImageView.image = [self.dataSource slideShow:self imageForPosition:KASlideShowPositionTop];
                _bottomImageView.image = [self.dataSource slideShow:self imageForPosition:KASlideShowPositionBottom];
            } else {
                NSUInteger nextIndex = (_currentIndex+1)%[self.images count];
                _topImageView.image = self.images[_currentIndex];
                _bottomImageView.image = self.images[nextIndex];
                _currentIndex = nextIndex;
            }
            
            // Animate
            switch (transitionType) {
                case KASlideShowTransitionFade:
                    [self animateFade];
                    break;
                    
                case KASlideShowTransitionSlide:
                    [self animateSlide:KASlideShowSlideModeForward];
                    break;
                    
            }
            
            // Call delegate
            if([delegate respondsToSelector:@selector(kaSlideShowDidShowNext:)]){
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, transitionDuration * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [delegate kaSlideShowDidShowNext:self];
                });
            }
        }
        else
        {
            // Next Image
            if (self.dataSource) {
                _topImageView.image = [self.dataSource slideShow:self imageForPosition:KASlideShowPositionTop];
                _bottomImageView.image = [self.dataSource slideShow:self imageForPosition:KASlideShowPositionBottom];
            } else {
                NSUInteger nextIndex = (_currentIndex+1)%[self.images count];
                _topImageView.imageURL = [NSURL URLWithString:self.images[_currentIndex]];
                _bottomImageView.imageURL = [NSURL URLWithString:self.images[nextIndex]];
//                NSString *currentStr = self.images[_currentIndex];
//                NSString *nextStr = self.images[nextIndex];
//                if([currentStr hasPrefix:@"http"] && [nextStr hasPrefix:@"http"])
//                {
//                    _topImageView.imageURL = [NSURL URLWithString:self.images[_currentIndex]];
//                    _bottomImageView.imageURL = [NSURL URLWithString:self.images[nextIndex]];
//                    _topImageView.hidden = NO;
//                    _bottomImageView.hidden = NO;
//                    _topTextView.hidden = YES;
//                    _bottomTextView.hidden = YES;
//                }
//                else if ([currentStr hasPrefix:@"http"] && ![nextStr hasPrefix:@"http"])
//                {
//                    _topImageView.imageURL = [NSURL URLWithString:self.images[_currentIndex]];
//                    _bottomTextView.text = nextStr;
//                    _bottomImageView.hidden = YES;
//                    _topTextView.hidden = YES;
//                    _bottomTextView.hidden = NO;
//                    _topImageView.hidden = NO;
//                }
//                else if(![currentStr hasPrefix:@"http"] && [nextStr hasPrefix:@"http"])
//                {
//                    _topTextView.text = currentStr;
//                    _bottomImageView.imageURL = [NSURL URLWithString:self.images[nextIndex]];
//                    _topTextView.hidden = YES;
//                    _topImageView.hidden = NO;
//                    _bottomImageView.hidden = NO;
//                    _bottomTextView.hidden = YES;
//                }
//                else if(![currentStr hasPrefix:@"http"] && ![nextStr hasPrefix:@"http"])
//                {
//                    _topTextView.text = currentStr;
//                    _bottomTextView.text = nextStr;
//                    _topImageView.hidden = YES;
//                    _bottomImageView.hidden = YES;
//                    _topTextView.hidden = NO;
//                    _bottomTextView.hidden = NO;
//                }
               
                _currentIndex = nextIndex;
            }
            
            // Animate
            switch (transitionType) {
                case KASlideShowTransitionFade:
                    [self animateFade];
                    break;
                    
                case KASlideShowTransitionSlide:
                    [self animateSlide:KASlideShowSlideModeForward];
                    break;
                    
            }
            
            // Call delegate
            if([delegate respondsToSelector:@selector(kaSlideShowDidShowNext:)]){
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, transitionDuration * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [delegate kaSlideShowDidShowNext:self];
                });
            }
        }
    }
}

- (void) previous
{
    if(! _isAnimating && ([self.images count] >1 || self.dataSource)){
        
        if ([self.delegate respondsToSelector:@selector(kaSlideShowWillShowPrevious:)]) [self.delegate kaSlideShowWillShowPrevious:self];
        
        if(!_isResourceFromURL)
        {
            // Previous image
            if (self.dataSource) {
                _topImageView.image = [self.dataSource slideShow:self imageForPosition:KASlideShowPositionTop];
                _bottomImageView.image = [self.dataSource slideShow:self imageForPosition:KASlideShowPositionBottom];
            } else {
                NSUInteger prevIndex;
                if(_currentIndex == 0){
                    prevIndex = [self.images count] - 1;
                }else{
                    prevIndex = (_currentIndex-1)%[self.images count];
                }
                _topImageView.image = self.images[_currentIndex];
                _bottomImageView.image = self.images[prevIndex];
                _currentIndex = prevIndex;
            }
            
            // Animate
            switch (transitionType) {
                case KASlideShowTransitionFade:
                    [self animateFade];
                    break;
                    
                case KASlideShowTransitionSlide:
                    [self animateSlide:KASlideShowSlideModeBackward];
                    break;
            }
            
            // Call delegate
            if([delegate respondsToSelector:@selector(kaSlideShowDidShowPrevious:)]){
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, transitionDuration * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [delegate kaSlideShowDidShowPrevious:self];
                });
            }
        }
        else
        {
            // Previous image
            if (self.dataSource) {
                _topImageView.image = [self.dataSource slideShow:self imageForPosition:KASlideShowPositionTop];
                _bottomImageView.image = [self.dataSource slideShow:self imageForPosition:KASlideShowPositionBottom];
            } else {
                NSUInteger prevIndex;
                if(_currentIndex == 0){
                    prevIndex = [self.images count] - 1;
                }else{
                    prevIndex = (_currentIndex-1)%[self.images count];
                }
            
                _topImageView.imageURL = [NSURL URLWithString:self.images[_currentIndex]];
                _bottomImageView.imageURL = [NSURL URLWithString:self.images[prevIndex]];
                _currentIndex = prevIndex;
            }
            
            // Animate
            switch (transitionType) {
                case KASlideShowTransitionFade:
                    [self animateFade];
                    break;
                    
                case KASlideShowTransitionSlide:
                    [self animateSlide:KASlideShowSlideModeBackward];
                    break;
            }
            
            // Call delegate
            if([delegate respondsToSelector:@selector(kaSlideShowDidShowPrevious:)]){
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, transitionDuration * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [delegate kaSlideShowDidShowPrevious:self];
                });
            }
        }
    }
    
}

- (void) animateFade
{
    _isAnimating = YES;
    
    [UIView animateWithDuration:transitionDuration
                     animations:^{
                         _topImageView.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         
                         _topImageView.image = _bottomImageView.image;
                         _topImageView.alpha = 1;
                         
                         _isAnimating = NO;
                         
                         if(! _doStop){
                             [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(next) object:nil];
                             [self performSelector:@selector(next) withObject:nil afterDelay:delay];
                         }
                     }];
}

- (void) animateSlide:(KASlideShowSlideMode) mode
{
    _isAnimating = YES;
    
    if(mode == KASlideShowSlideModeBackward){
        _bottomImageView.transform = CGAffineTransformMakeTranslation(- _bottomImageView.frame.size.width, 0);
        _bottomImageView.transform = CGAffineTransformMakeTranslation(- _bottomImageView.frame.size.width, 0);
    }else if(mode == KASlideShowSlideModeForward){
        _bottomImageView.transform = CGAffineTransformMakeTranslation(_bottomImageView.frame.size.width, 0);
//        _bottomTextView.transform = CGAffineTransformMakeTranslation(_bottomTextView.frame.size.width, 0);
    }
    
    
    [UIView animateWithDuration:transitionDuration
                     animations:^{
                         
                         if(mode == KASlideShowSlideModeBackward){
                             _topImageView.transform = CGAffineTransformMakeTranslation( _topImageView.frame.size.width, 0);
                             _bottomImageView.transform = CGAffineTransformMakeTranslation(0, 0);
//                             _topTextView.transform = CGAffineTransformMakeTranslation( _topImageView.frame.size.width, 0);
//                             _bottomTextView.transform = CGAffineTransformMakeTranslation(0, 0);
                         }else if(mode == KASlideShowSlideModeForward){
                             _topImageView.transform = CGAffineTransformMakeTranslation(- _topImageView.frame.size.width, 0);
                             _bottomImageView.transform = CGAffineTransformMakeTranslation(0, 0);
//                             _topTextView.transform = CGAffineTransformMakeTranslation(- _topTextView.frame.size.width, 0);
//                             _bottomTextView.transform = CGAffineTransformMakeTranslation(0, 0);
                         }
                     }
                     completion:^(BOOL finished){
                         
                         _topImageView.image = _bottomImageView.image;
                         _topImageView.transform = CGAffineTransformMakeTranslation(0, 0);
//                         _topTextView.text = _bottomTextView.text;
//                         _topTextView.transform = CGAffineTransformMakeTranslation(0, 0);
                         
                         _isAnimating = NO;
                         
                         if(! _doStop){
                             [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(next) object:nil];
                             [self performSelector:@selector(next) withObject:nil afterDelay:delay];
                         }
                     }];
}


- (void) stop
{
    _doStop = YES;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(next) object:nil];
}

- (KASlideShowState)state
{
    return !_doStop;
}

#pragma mark - Gesture Recognizers initializers
- (void) addGestureTap
{
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [self addGestureRecognizer:singleTapGestureRecognizer];
}

- (void) addGestureSwipe
{
    UISwipeGestureRecognizer* swipeLeftGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipeLeftGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    
    UISwipeGestureRecognizer* swipeRightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self addGestureRecognizer:swipeLeftGestureRecognizer];
    [self addGestureRecognizer:swipeRightGestureRecognizer];
}

#pragma mark - Gesture Recognizers handling
- (void)handleSingleTap:(id)sender
{
//    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *)sender;
//    CGPoint pointTouched = [gesture locationInView:self.topImageView];
    [self imageTapped];
//    if (pointTouched.x <= self.topImageView.center.x){
//        [self previous];
//    }else {
//        [self next];
//    }
}

-(void)imageTapped
{
    if ([self.delegate respondsToSelector:@selector(kaSlideShowImageTapped:)])
        [self.delegate kaSlideShowImageTapped:self];
}

- (void) handleSwipe:(id)sender
{
    UISwipeGestureRecognizer *gesture = (UISwipeGestureRecognizer *)sender;
    
    float oldTransitionDuration = self.transitionDuration;
    
    self.transitionDuration = kSwipeTransitionDuration;
    if (gesture.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        [self next];
    }
    else if (gesture.direction == UISwipeGestureRecognizerDirectionRight)
    {
        [self previous];
    }
    
    self.transitionDuration = oldTransitionDuration;
}

@end

