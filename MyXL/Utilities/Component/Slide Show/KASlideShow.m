//
//  KASlideShow.m
//
//  Created by Alexis Creuzot on 23/01/13.
//  Copyright (c) 2013 Alexis Creuzot. All rights reserved.
//

#import "KASlideShow.h"
#import "DataManager.h"
#import "AdsModel.h"

typedef NS_ENUM(NSInteger, KASlideShowSlideMode) {
    KASlideShowSlideModeForward,
    KASlideShowSlideModeBackward
};

@interface KASlideShow()
@property (atomic) BOOL doStop;
@property (atomic) BOOL isAnimating;

@property (nonatomic) NSUInteger currentIndex;

@end

@implementation KASlideShow

@synthesize delegate;
@synthesize delay;
@synthesize transitionDuration;
@synthesize transitionType;
@synthesize images;

@synthesize topImageView;
@synthesize bottomImageView;

- (void)awakeFromNib
{
    //NSLog(@"--> awakeFromNib");
    [self setDefaultValues];
}

- (id)initWithFrame:(CGRect)frame
{
    //NSLog(@"--> initWithFrame");
    self = [super initWithFrame:frame];
    if (self) {
        [self setDefaultValues];
    }
    return self;
}

- (void) setDefaultValues
{
    self.clipsToBounds = YES;
    self.images = [NSMutableArray array];
    _currentIndex = 0;
    delay = 3;
    
    transitionDuration = 1;
    transitionType = KASlideShowTransitionFade;
    _doStop = YES;
    _isAnimating = NO;
    
    self.topImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    //NSLog(@"_topImageView self.bounds.size.width = %f",self.bounds.size.width);
    self.bottomImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    //NSLog(@"_bottomImageView self.bounds.size.width = %f",self.bounds.size.width);
    self.topImageView.clipsToBounds = YES;
    self.bottomImageView.clipsToBounds = YES;
    [self setImagesContentMode:UIViewContentModeScaleAspectFit];
    
    [self addSubview:self.bottomImageView];
    [self addSubview:self.topImageView];
}

- (void) setImagesContentMode:(UIViewContentMode)mode
{
    self.topImageView.contentMode = mode;
    self.bottomImageView.contentMode = mode;
}

- (UIViewContentMode) imagesContentMode
{
    return self.topImageView.contentMode;
}

- (void) addImagesFromURL:(NSArray *)urls {
    for(NSString * name in urls){
        //[self addImage:[UIImage imageNamed:name]];
        
    }
}

- (void) addImagesFromResources:(NSArray *) names
{
    for(NSString * name in names){
        [self addImage:[UIImage imageNamed:name]];
    }
}

- (void) addImage:(UIImage*) image
{
    [self.images addObject:image];
    //NSLog(@"self.images count = %i",[self.images count]);
    
    if([self.images count] == 1){
        self.topImageView.image = image;
        
        self.topImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(didTapImageWithGesture:)];
        [self.topImageView addGestureRecognizer:tapGesture];
        [tapGesture release];
    }else if([self.images count] == 2){
        self.bottomImageView.image = image;
        
        self.bottomImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(didTapImageWithGesture:)];
        [self.bottomImageView addGestureRecognizer:tapGesture];
        [tapGesture release];
    }
}

- (void)didTapImageWithGesture:(UITapGestureRecognizer *)tapGesture {
    /*
    UIApplication *ourApplication = [UIApplication sharedApplication];
    
    DataManager *sharedData = [DataManager sharedInstance];
    AdsModel *adsModel = [sharedData.adsData objectAtIndex:_currentIndex];
    
    NSString *ourPath = adsModel.actionURL;
    NSURL *ourURL = [NSURL URLWithString:ourPath];
    [ourApplication openURL:ourURL];*/
    
    //DataManager *sharedData = [DataManager sharedInstance];
    //MenuModel *adsModel = [sharedData.hotOfferBannerData objectAtIndex:_currentIndex];
    
    // Call delegate
    if([delegate respondsToSelector:@selector(kaSlideShowDidSelectBanner:)]){
        [delegate kaSlideShowDidSelectBanner:_currentIndex];
    }
}

- (void) start
{
    if([self.images count] <= 1){
        return;
    }
    
    _doStop = NO;
    [self performSelector:@selector(next) withObject:nil afterDelay:delay];
}

- (void) next
{    
    if(! _isAnimating){
        
        // Next Image
        NSUInteger nextIndex = (_currentIndex+1)%[self.images count];
        self.topImageView.image = self.images[_currentIndex];
        self.bottomImageView.image = self.images[nextIndex];
        _currentIndex = nextIndex;
        
        // Animate
        switch (transitionType) {
            case KASlideShowTransitionFade:
                [self animateFade];
                break;
                
            case KASlideShowTransitionSlide:
                [self animateSlide:KASlideShowSlideModeForward];
                break;

        }
        
        // Call delegate
        if([delegate respondsToSelector:@selector(kaSlideShowDidNext)]){
            [delegate kaSlideShowDidNext];
        }
    }
}


- (void) previous
{
    if(! _isAnimating){
        
        // Previous image
        NSUInteger prevIndex;
        if(_currentIndex == 0){
            prevIndex = [self.images count] - 1;
        }else{
            prevIndex = (_currentIndex-1)%[self.images count];
        }
        self.topImageView.image = self.images[_currentIndex];
        self.bottomImageView.image = self.images[prevIndex];
        _currentIndex = prevIndex;
        
        // Animate
        switch (transitionType) {
            case KASlideShowTransitionFade:
                [self animateFade];
                break;
                
            case KASlideShowTransitionSlide:
                [self animateSlide:KASlideShowSlideModeBackward];
                break;
        }
        
        // Call delegate
        if([delegate respondsToSelector:@selector(kaSlideShowDidPrevious)]){
            [delegate kaSlideShowDidPrevious];
        }
    }
    
}

- (void) animateFade
{
    _isAnimating = YES;
    
    [UIView animateWithDuration:transitionDuration
                     animations:^{
                         self.topImageView.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         
                         self.topImageView.image = self.bottomImageView.image;
                         self.topImageView.alpha = 1;
                         
                         _isAnimating = NO;
                         
                         if(! _doStop){
                             [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(next) object:nil];
                             [self performSelector:@selector(next) withObject:nil afterDelay:delay];
                         }
                     }];
}

- (void) animateSlide:(KASlideShowSlideMode) mode
{
    _isAnimating = YES;
    
    if(mode == KASlideShowSlideModeBackward){
        self.bottomImageView.transform = CGAffineTransformMakeTranslation(- self.bottomImageView.frame.size.width, 0);
    }else if(mode == KASlideShowSlideModeForward){
       self.bottomImageView.transform = CGAffineTransformMakeTranslation(self.bottomImageView.frame.size.width, 0); 
    }
    
    
    [UIView animateWithDuration:transitionDuration
                     animations:^{
                         
                         if(mode == KASlideShowSlideModeBackward){
                             self.topImageView.transform = CGAffineTransformMakeTranslation( self.topImageView.frame.size.width, 0);
                             self.bottomImageView.transform = CGAffineTransformMakeTranslation(0, 0);
                         }else if(mode == KASlideShowSlideModeForward){
                             self.topImageView.transform = CGAffineTransformMakeTranslation(- self.topImageView.frame.size.width, 0);
                             self.bottomImageView.transform = CGAffineTransformMakeTranslation(0, 0);
                         }
                     }
                     completion:^(BOOL finished){
                         
                         self.topImageView.image = self.bottomImageView.image;
                         self.topImageView.transform = CGAffineTransformMakeTranslation(0, 0);
                         
                         _isAnimating = NO;
                         
                         if(! _doStop){
                             [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(next) object:nil];
                             [self performSelector:@selector(next) withObject:nil afterDelay:delay];
                         }
                     }];
}


- (void) stop
{
    _doStop = YES;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(next) object:nil];
}


@end

