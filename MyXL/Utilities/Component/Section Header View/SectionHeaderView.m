//
//  SectionHeaderView.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/9/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "SectionHeaderView.h"
#import "Constant.h"
#import <QuartzCore/QuartzCore.h>
#import "AXISnetCommon.h"
#import "Language.h"
#import "DataManager.h"

@interface SectionHeaderView ()

@property (nonatomic, retain) UILabel *iconLabel;
//@property (nonatomic, retain) UILabel *titleLabel;

@end

@implementation SectionHeaderView

@synthesize icon = _icon;
@synthesize title = _title;
@synthesize subtitle = _subtitle;

@synthesize titleLabel = _titleLabel;
@synthesize subtitleLabel = _subtitleLabel;
@synthesize packageButton = _packageButton;
@synthesize quotaCalculatorButton = _quotaCalculatorButton;

@synthesize editProfileButton = _editProfileButton;

@synthesize starLabel = _starLabel;

@synthesize unreadLabel = _unreadLabel;

//@synthesize firstSection = _firstSection;
//@synthesize grayBackgroundView = _grayBackgroundView;

/*
@property (nonatomic, nonatomic, ithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGFloat symbolWidth = 30.0;
        CGFloat leftPadding = 20.0;
        
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(leftPadding, 0, frame.size.width - (leftPadding*2), frame.size.height)];
        backgroundView.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:20.0/255.0 blue:99.0/255.0 alpha:1];
        [self addSubview:backgroundView];
        [backgroundView release];
        
        UILabel *iconLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, 0, symbolWidth, frame.size.height)];
        iconLabel.text = @"C";
        iconLabel.textColor = [UIColor whiteColor];
        iconLabel.font = [UIFont fontWithName:@"K-SAN" size:20.0];
        iconLabel.backgroundColor = [UIColor clearColor];
        iconLabel.textAlignment = UITextAlignmentCenter;
        [self addSubview:iconLabel];
        [iconLabel release];
        
        UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding + symbolWidth, 0, frame.size.width - symbolWidth - leftPadding, frame.size.height)];
        titleLabelTmp.text = @"";
        titleLabelTmp.textColor = [UIColor whiteColor];
        titleLabelTmp.font = [UIFont fontWithName:@"OpenSans-Extrabold" size:20.0];
        titleLabelTmp.backgroundColor = [UIColor clearColor];
        
        //titleLabel = titleLabelTmp;
        
        [self addSubview:titleLabelTmp];
        [titleLabelTmp release];
        
        //self.layer.shadowColor = [[UIColor blackColor] CGColor];
		//self.layer.shadowOffset = CGSizeMake(1.0, 1.0);
		//self.layer.shadowOpacity = 0.40;
    }
    return self;
}
 */

- (id)initWithFrame:(CGRect)frame withLabel:(NSString *)headerTitle withIcon:(NSString *)headerIcon isFirstSection:(BOOL)firstSectionCell {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor = kDefaultWhiteColor;
        //self.backgroundColor = [UIColor whiteColor];
        
        CGFloat leftPadding = 0.0;
        if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            leftPadding = 45.0;
        }
        else {
            if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO) {
                leftPadding = 5.0;
            }
            else {
                leftPadding = 10.0;
            }
        }
        CGFloat topPadding = 0.0;
        UIView *grayBackgroundViewTmp = nil;
        if (firstSectionCell) {
            topPadding = 20.0;
            CGRect grayBackgroundViewFrame = CGRectMake(leftPadding, topPadding, frame.size.width - (leftPadding*2), frame.size.height - topPadding);
            grayBackgroundViewTmp = [[UIView alloc] initWithFrame:grayBackgroundViewFrame];
            
            // Top Rounded Corner
            //[grayBackgroundViewTmp.layer setCornerRadius:kDefaultCornerRadius];
            //[grayBackgroundViewTmp.layer setMasksToBounds:YES];
            
            UIBezierPath *maskPath;
            maskPath = [UIBezierPath bezierPathWithRoundedRect:grayBackgroundViewTmp.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(kDefaultCornerRadius, kDefaultCornerRadius)];
            
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = self.bounds;
            maskLayer.path = maskPath.CGPath;
            grayBackgroundViewTmp.layer.mask = maskLayer;
            [maskLayer release];
        }
        else {
            grayBackgroundViewTmp = [[UIView alloc] initWithFrame:CGRectMake(leftPadding, topPadding, frame.size.width - (leftPadding*2), frame.size.height - topPadding)];
        }
        
        //grayBackgroundViewTmp.backgroundColor = kDefaultButtonGrayColor;
        grayBackgroundViewTmp.backgroundColor = kDefaultWhiteColor;
        //self.grayBackgroundView = grayBackgroundViewTmp;
        [self addSubview:grayBackgroundViewTmp];
        [grayBackgroundViewTmp release];
        
        CGFloat symbolWidth = 30.0;
        if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            leftPadding = 55.0;
        }
        else {
            if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO) {
                leftPadding = 11.0;
            }
            else {
                leftPadding = 20.0;
            }
            
        }
        
        if (firstSectionCell) {
            topPadding = topPadding + 20.0;
        }
        
        // new
        CGFloat bottomPadding = bottomSectionHeaderHeight;
        //
        
        // Pink Background
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(leftPadding, topPadding, frame.size.width - (leftPadding*2), frame.size.height - topPadding - bottomPadding)];
        backgroundView.backgroundColor = kDefaultBlueColor;
        [self addSubview:backgroundView];
        [backgroundView release];
        
        UILabel *iconLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, topPadding, symbolWidth, frame.size.height - topPadding - bottomPadding)];
        iconLabel.text = headerIcon;
        iconLabel.textColor = kDefaultWhiteColor;
        iconLabel.font = [UIFont fontWithName:@"K-SAN" size:18.0];
        iconLabel.backgroundColor = [UIColor clearColor];
        iconLabel.textAlignment = UITextAlignmentCenter;
        [self addSubview:iconLabel];
        [iconLabel release];
        
        UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding + symbolWidth, topPadding, frame.size.width - symbolWidth - leftPadding, frame.size.height - topPadding - bottomPadding)];
        titleLabelTmp.text = [headerTitle uppercaseString];
        titleLabelTmp.textColor = kDefaultWhiteColor;
        titleLabelTmp.font = [UIFont fontWithName:kDefaultFontBold size:18.0];
        titleLabelTmp.backgroundColor = [UIColor clearColor];
        
        //titleLabel = titleLabelTmp;
        
        [self addSubview:titleLabelTmp];
        [titleLabelTmp release];
        
        //self.layer.shadowColor = [[UIColor blackColor] CGColor];
		//self.layer.shadowOffset = CGSizeMake(1.0, 1.0);
		//self.layer.shadowOpacity = 0.40;
        
        // Bottom View
        CGFloat startY = frame.size.height - topPadding - bottomPadding;
        //CGFloat bottomHeight = bottomSectionHeaderHeight;
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(leftPadding,
                                                                      topPadding + startY,
                                                                      frame.size.width - (leftPadding*2),
                                                                      bottomPadding)];
        //bottomView.backgroundColor = kDefaultBaseColor;
        bottomView.backgroundColor = [UIColor whiteColor];
        [self addSubview:bottomView];
        [bottomView release];
        
        // Left Bottom View
        UIView *leftBottomView = [[UIView alloc] initWithFrame:CGRectMake(leftPadding,
                                                                      topPadding + startY,
                                                                      1,
                                                                      bottomPadding)];
        leftBottomView.backgroundColor = kDefaultBlueColor;
        [self addSubview:leftBottomView];
        [leftBottomView release];
        
        // Right Bottom View
        CGFloat startX = leftPadding + (frame.size.width - (leftPadding*2)) - 1;
        UIView *rightBottomView = [[UIView alloc] initWithFrame:CGRectMake(startX,
                                                                          topPadding + startY,
                                                                          1,
                                                                          bottomPadding)];
        rightBottomView.backgroundColor = kDefaultBlueColor;
        [self addSubview:rightBottomView];
        [rightBottomView release];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withLabel:(NSString *)title isFirstSection:(BOOL)firstSectionCell
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor = kDefaultBaseColor;
        
        CGFloat leftPadding = 0.0;
        if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            leftPadding = 45.0;
        }
        else {
            leftPadding = 10.0;
        }
        
        CGFloat topPadding = 0.0;
        UIView *grayBackgroundViewTmp = nil;
        if (firstSectionCell) {
            topPadding = 20.0;
            CGRect grayBackgroundViewFrame = CGRectMake(leftPadding, topPadding, frame.size.width - (leftPadding*2), frame.size.height - topPadding);
            grayBackgroundViewTmp = [[UIView alloc] initWithFrame:grayBackgroundViewFrame];
            
            // Top Rounded Corner
            //[grayBackgroundViewTmp.layer setCornerRadius:kDefaultCornerRadius];
            //[grayBackgroundViewTmp.layer setMasksToBounds:YES];
            
            UIBezierPath *maskPath;
            maskPath = [UIBezierPath bezierPathWithRoundedRect:grayBackgroundViewTmp.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(kDefaultCornerRadius, kDefaultCornerRadius)];
            
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = self.bounds;
            maskLayer.path = maskPath.CGPath;
            grayBackgroundViewTmp.layer.mask = maskLayer;
            [maskLayer release];
        }
        else {
            grayBackgroundViewTmp = [[UIView alloc] initWithFrame:CGRectMake(leftPadding, topPadding, frame.size.width - (leftPadding*2), frame.size.height - topPadding)];
        }
        
        grayBackgroundViewTmp.backgroundColor = kDefaultButtonGrayColor;
        //self.grayBackgroundView = grayBackgroundViewTmp;
        [self addSubview:grayBackgroundViewTmp];
        [grayBackgroundViewTmp release];
        
        CGFloat symbolWidth = 30.0;
        
        if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            leftPadding = 55.0;
        }
        else {
            leftPadding = 20.0;
        }
        
        if (firstSectionCell) {
            topPadding = topPadding + 20.0;
        }
        
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(leftPadding, topPadding, frame.size.width - (leftPadding*2), frame.size.height - topPadding)];
        
        backgroundView.backgroundColor = kDefaultPinkColor;
        [self addSubview:backgroundView];
        [backgroundView release];
        
        UILabel *iconLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, topPadding, symbolWidth, frame.size.height - topPadding)];
        iconLabel.text = @"C";
        iconLabel.textColor = kDefaultWhiteColor;
        iconLabel.font = [UIFont fontWithName:@"K-SAN" size:18.0];
        iconLabel.backgroundColor = [UIColor clearColor];
        iconLabel.textAlignment = UITextAlignmentCenter;
        [self addSubview:iconLabel];
        [iconLabel release];
        
        UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding + symbolWidth, topPadding, frame.size.width - symbolWidth - leftPadding, frame.size.height - topPadding)];
        titleLabelTmp.text = [title uppercaseString];
        titleLabelTmp.textColor = kDefaultWhiteColor;
        titleLabelTmp.font = [UIFont fontWithName:kDefaultFontBold size:18.0];
        titleLabelTmp.backgroundColor = [UIColor clearColor];
        
        //titleLabel = titleLabelTmp;
        
        [self addSubview:titleLabelTmp];
        [titleLabelTmp release];
        
        //self.layer.shadowColor = [[UIColor blackColor] CGColor];
		//self.layer.shadowOffset = CGSizeMake(1.0, 1.0);
		//self.layer.shadowOpacity = 0.40;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withLabel:(NSString *)title {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        CGFloat leftPadding = 20.0;
        CGFloat topPadding = 0.0;
        //CGFloat symbolWidth = 30.0;
        
        UIView *backgroundView = [[UIView alloc] initWithFrame:frame];
        backgroundView.backgroundColor = kDefaultNavyBlueColor;
        [self addSubview:backgroundView];
        [backgroundView release];
        /*
        UILabel *iconLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, topPadding, symbolWidth, frame.size.height - topPadding)];
        iconLabelTmp.text = @"C";
        iconLabelTmp.textColor = kDefaultWhiteColor;
        iconLabelTmp.font = [UIFont fontWithName:@"K-SAN" size:20.0];
        iconLabelTmp.backgroundColor = [UIColor clearColor];
        iconLabelTmp.textAlignment = UITextAlignmentCenter;
        
        _iconLabel = iconLabelTmp;
        [self addSubview:_iconLabel];
        [iconLabelTmp release];*/
        
        UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, topPadding, frame.size.width - leftPadding, frame.size.height - topPadding)];
        titleLabelTmp.text = [title uppercaseString];
        titleLabelTmp.textColor = kDefaultWhiteColor;
        titleLabelTmp.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontHeaderSize];
        titleLabelTmp.backgroundColor = [UIColor clearColor];
        
        _titleLabel = titleLabelTmp;
        [self addSubview:_titleLabel];
        [titleLabelTmp release];
    }
    return self;
}

// Buy Package
- (id)initWithFrame:(CGRect)frame withTitle:(NSString*)title withSubtitle:(NSString*)subtitle isFirstSection:(BOOL)firstSectionCell isZeroTopPadding:(BOOL)zeroTopPadding {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        CGFloat leftPadding = 15.0;
        CGFloat topPadding = 0.0;
        
        UIColor *bgColor;
        UIColor *titleColor;
        
        if (firstSectionCell) {
            if (zeroTopPadding) {
                topPadding = 0.0;
            }
            else {
                topPadding = kDefaultCellHeight;
            }
            
            bgColor = [UIColor clearColor];
            titleColor = kDefaultBlackColor;
            
            UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, topPadding, frame.size.width, frame.size.height)];
            backgroundView.backgroundColor = bgColor;
            [self addSubview:backgroundView];
            [backgroundView release];
            
            UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, topPadding, frame.size.width - 100 - 38 - (leftPadding*2), frame.size.height)];
            titleLabelTmp.text = [title uppercaseString];
            titleLabelTmp.textColor = titleColor;
            titleLabelTmp.font = [UIFont fontWithName:kDefaultFontLight size:18.0];
            titleLabelTmp.backgroundColor = [UIColor clearColor];
            titleLabelTmp.numberOfLines = 0;
            titleLabelTmp.lineBreakMode = UILineBreakModeWordWrap;
            _titleLabel = titleLabelTmp;
            [self addSubview:_titleLabel];
            [titleLabelTmp release];
            
            DataManager *dataManager = [DataManager sharedInstance];
            
            UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:11.0];
            /*
            UIButton *button = createButtonWithFrame(CGRectMake(frame.size.width - 100 - leftPadding,
                                                                ((frame.size.height - kDefaultButtonHeight)/2) + topPadding, //topPadding + 5
                                                                100,
                                                                kDefaultButtonHeight),
                                                     [subtitle uppercaseString],
                                                     buttonFont,
                                                     kDefaultWhiteColor,
                                                     colorWithHexString(dataManager.profileData.themesModel.menuFeature),
                                                     colorWithHexString(dataManager.profileData.themesModel.menuColor));*/
            UIButton *button;
            
            //------------------//
            // Reskinning       //
            //------------------//
            if ([dataManager.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
                [dataManager.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
                button = createButtonWithFrame(CGRectMake(frame.size.width - 100 - leftPadding,
                                                          ((frame.size.height - kDefaultButtonHeight)/2) + topPadding,
                                                          100,
                                                          kDefaultButtonHeight),
                                               [subtitle uppercaseString],
                                               buttonFont,
                                               kDefaultWhiteColor,
                                               colorWithHexString(dataManager.profileData.themesModel.menuColor),
                                               colorWithHexString(dataManager.profileData.themesModel.menuFeature));
            }
            else {
                button = createButtonWithFrame(CGRectMake(frame.size.width - 100 - leftPadding,
                                                          ((frame.size.height - kDefaultButtonHeight)/2) + topPadding,
                                                          100,
                                                          kDefaultButtonHeight),
                                               [subtitle uppercaseString],
                                               buttonFont,
                                               kDefaultWhiteColor,
                                               colorWithHexString(dataManager.profileData.themesModel.menuFeature),
                                               colorWithHexString(dataManager.profileData.themesModel.menuColor));
            }
            
            _packageButton = button;
            [self addSubview:_packageButton];
            
            // Quota Calculator
            
            if (dataManager.haveQuotaCalculator) {
                buttonFont = [UIFont fontWithName:kDefaultFontKSAN size:11];
                CGRect frame = _packageButton.frame;
                button = createButtonWithFrame(CGRectMake(frame.origin.x - 30 - 8,
                                                          frame.origin.y,
                                                          30,
                                                          kDefaultButtonHeight),
                                               @"w",
                                               buttonFont,
                                               kDefaultWhiteColor,
                                               colorWithHexString(dataManager.profileData.themesModel.menuFeature),
                                               colorWithHexString(dataManager.profileData.themesModel.menuColor));
                _quotaCalculatorButton = button;
                [self addSubview:_quotaCalculatorButton];
            }
        }
        else {
            /*
             * Restructure & Reskinning
             */
            DataManager *sharedData = [DataManager sharedInstance];
            bgColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
            titleColor = colorWithHexString(sharedData.profileData.themesModel.menuH3);
            
            // Background
            UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, topPadding, frame.size.width, frame.size.height)];
            backgroundView.backgroundColor = bgColor;
            [self addSubview:backgroundView];
            [backgroundView release];
            
            // Title
            UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, topPadding, 0.6*frame.size.width, frame.size.height)];
            titleLabelTmp.text = [title uppercaseString];
            titleLabelTmp.textColor = titleColor;
            titleLabelTmp.backgroundColor = [UIColor clearColor];
            
            if ([titleLabelTmp.text length] > 60) {
                titleLabelTmp.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSmallSize];
            }
            else {
                titleLabelTmp.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            }
            
            titleLabelTmp.numberOfLines = 0;
            titleLabelTmp.lineBreakMode = UILineBreakModeWordWrap;
            //titleLabelTmp.minimumFontSize = kDefaultFontSmallSize;
            //titleLabelTmp.adjustsFontSizeToFitWidth = YES;
            _titleLabel = titleLabelTmp;
            [self addSubview:_titleLabel];
            [titleLabelTmp release];
            
            // Subtitle
            UILabel *subtitleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width - (0.4*frame.size.width), topPadding, 0.4*frame.size.width - leftPadding, frame.size.height)];
            //subtitleLabelTmp.text = [subtitle uppercaseString];
            subtitleLabelTmp.text = subtitle;
            subtitleLabelTmp.textColor = titleColor;
            subtitleLabelTmp.backgroundColor = [UIColor clearColor];
            subtitleLabelTmp.textAlignment = UITextAlignmentRight;
            subtitleLabelTmp.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            _subtitleLabel = subtitleLabelTmp;
            [self addSubview:_subtitleLabel];
            [subtitleLabelTmp release];
        }
    }
    return self;
}

// Common Section View Header
- (id)initWithFrame:(CGRect)frame withLabelForXL:(NSString *)title isFirstSection:(BOOL)firstSectionCell {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        CGFloat leftPadding = 15.0;
        CGFloat topPadding = 0.0;
        
        UIColor *bgColor;
        UIColor *titleColor;
        UIFont *titleFont;
        
        if (firstSectionCell) {
            bgColor = kDefaultWhiteSmokeColor;
            titleColor = kDefaultBlackColor;
            titleFont = [UIFont fontWithName:kDefaultFontLight size:24.0];
        }
        else {
            /*
             * Restructure & Reskinning
             */
            DataManager *sharedData = [DataManager sharedInstance];
            if ([sharedData.profileData.themesModel.menuColor length] > 0) {
                bgColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
                titleColor = colorWithHexString(sharedData.profileData.themesModel.menuH3);
                
            }
            else {
                bgColor = kDefaultNavyBlueColor;
                titleColor = kDefaultAquaMarineColor;
            }
            
            
            titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontHeaderSize];
        }
        
        self.backgroundColor = bgColor;
        
        UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, topPadding, frame.size.width - leftPadding, frame.size.height)];
        titleLabelTmp.text = [title uppercaseString];
        titleLabelTmp.textColor = titleColor;
        titleLabelTmp.font = titleFont;
        titleLabelTmp.backgroundColor = [UIColor clearColor];
        titleLabelTmp.numberOfLines = 0;
        titleLabelTmp.lineBreakMode = UILineBreakModeWordWrap;
        
        _titleLabel = titleLabelTmp;
        [self addSubview:_titleLabel];
        [titleLabelTmp release];
    }
    return self;
}

// TODO : V1.9
// Created for XL Tunai Balance on section header
- (id)initWithFrame:(CGRect)frame
     withLabelForXL:(NSString *)title
        andSubtitle:(NSString *)subtitle
  andIsFirstSection:(BOOL)firstSectionCell
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        CGFloat leftPadding = 15.0;
        CGFloat topPadding = 0.0;
        
        UIColor *bgColor;
        UIColor *titleColor;
        UIFont *titleFont;
        UIFont *subtitleFont;
        
        if (firstSectionCell) {
            bgColor = kDefaultWhiteSmokeColor;
            titleColor = kDefaultBlackColor;
            titleFont = [UIFont fontWithName:kDefaultFontLight size:24.0];
            if(IS_IPAD)
                subtitleFont = [UIFont fontWithName:kDefaultFontLight size:17.0];
            else
                subtitleFont = [UIFont fontWithName:kDefaultFontLight size:15.0];
        }
        else {
            /*
             * Restructure & Reskinning
             */
            DataManager *sharedData = [DataManager sharedInstance];
            if ([sharedData.profileData.themesModel.menuColor length] > 0) {
                bgColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
                titleColor = colorWithHexString(sharedData.profileData.themesModel.menuH3);
                
            }
            else {
                bgColor = kDefaultNavyBlueColor;
                titleColor = kDefaultAquaMarineColor;
            }
            
            
            titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontHeaderSize];
        }
        
        self.backgroundColor = bgColor;
        
        UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, topPadding, frame.size.width*0.6 - leftPadding, frame.size.height)];
        titleLabelTmp.text = [title uppercaseString];
        titleLabelTmp.textColor = titleColor;
        titleLabelTmp.font = titleFont;
        titleLabelTmp.backgroundColor = [UIColor clearColor];
        titleLabelTmp.numberOfLines = 0;
        titleLabelTmp.lineBreakMode = UILineBreakModeWordWrap;
    
        CGSize subtitleSize = calculateLabelSize(frame.size.width*0.4, subtitle, subtitleFont);
        if(subtitleSize.height > 21)
        {
           subtitleFont = [UIFont fontWithName:kDefaultFontLight size:13.0];
        }
        
        UILabel *subtitleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(titleLabelTmp.frame) + 8, topPadding, frame.size.width*0.4 - leftPadding, frame.size.height)];
//        subtitleLabelTmp.backgroundColor = [UIColor grayColor];
        subtitleLabelTmp.text = [subtitle uppercaseString];
        subtitleLabelTmp.textColor = titleColor;
        subtitleLabelTmp.font = subtitleFont;
//        subtitleLabelTmp.backgroundColor = [UIColor clearColor];
        subtitleLabelTmp.numberOfLines = 0;
        subtitleLabelTmp.lineBreakMode = UILineBreakModeWordWrap;
        subtitleLabelTmp.textAlignment = NSTextAlignmentRight;
        
        _titleLabel = titleLabelTmp;
        [self addSubview:_titleLabel];
        [self addSubview:subtitleLabelTmp];
        
        [titleLabelTmp release];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withLabelForXL:(NSString *)title isFirstSection:(BOOL)firstSectionCell isForLeftMenu:(BOOL)leftMenu {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        CGFloat leftPadding = 15.0;
        CGFloat topPadding = 0.0;
        
        UIColor *bgColor;
        UIColor *titleColor;
        UIFont *titleFont;
        
        if (!leftMenu) {
            if (firstSectionCell) {
                //topPadding = defaultSectionHeaderHeight*2;
                bgColor = kDefaultWhiteSmokeColor;
                titleColor = kDefaultBlackColor;
                titleFont = [UIFont fontWithName:kDefaultFontLight size:24.0];
            }
            else {
                //topPadding = defaultSectionHeaderHeight;
                bgColor = kDefaultNavyBlueColor;
                titleColor = kDefaultTextHeaderColor;
                titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontHeaderSize];
            }
            
//            UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, topPadding, frame.size.width, frame.size.height)];
//            backgroundView.backgroundColor = kDefaultNavyBlueColor;
//            [self addSubview:backgroundView];
//            [backgroundView release];
            
            self.backgroundColor = bgColor;
        }
        
        
        
        UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, topPadding, frame.size.width - leftPadding - 100, frame.size.height)];
        titleLabelTmp.text = [title uppercaseString];
        titleLabelTmp.textColor = titleColor;
        titleLabelTmp.font = titleFont;
        titleLabelTmp.backgroundColor = [UIColor clearColor];
        
        _titleLabel = titleLabelTmp;
        [self addSubview:_titleLabel];
        [titleLabelTmp release];
    }
    return self;
}

#pragma mark Edit Profile

- (id)initForProfileWithFrame:(CGRect)frame withTitle:(NSString*)title withEditButton:(NSString*)buttonTitle {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        DataManager *sharedData = [DataManager sharedInstance];
        
        CGFloat leftPadding = 15.0;
        CGFloat topPadding = 0.0;
        
        UIColor *bgColor;
        UIColor *titleColor;
        UIFont *titleFont;
        
        bgColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
        titleColor = colorWithHexString(sharedData.profileData.themesModel.menuH3);
        titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontHeaderSize];
        
        self.backgroundColor = bgColor;
        
        UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, topPadding, frame.size.width - leftPadding, frame.size.height)];
        titleLabelTmp.text = [title uppercaseString];
        titleLabelTmp.textColor = titleColor;
        titleLabelTmp.font = titleFont;
        titleLabelTmp.backgroundColor = [UIColor clearColor];
        titleLabelTmp.numberOfLines = 0;
        titleLabelTmp.lineBreakMode = UILineBreakModeWordWrap;
        
        _titleLabel = titleLabelTmp;
        [self addSubview:_titleLabel];
        
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFontKSAN size:20.0];
        UIButton *button = createButtonWithFrame(CGRectMake(frame.size.width - kDefaultButtonHeight - leftPadding,
                                                            ((frame.size.height - kDefaultButtonHeight)/2) + topPadding, //topPadding + 5
                                                            kDefaultButtonHeight,
                                                            kDefaultButtonHeight),
                                                 buttonTitle,
                                                 buttonFont,
                                                 colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                 kDefaultYellowColor,
                                                 kDefaultYellowColor);
        
        _editProfileButton = button;
        [self addSubview:_editProfileButton];
        
        [titleLabelTmp release];
    }
    return self;
}

#pragma mark XL Star

- (id)initForXLStarWithFrame:(CGRect)frame withTitle:(NSString*)title withStar:(NSString*)star {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        CGFloat leftPadding = 15.0;
        CGFloat topPadding = 0.0;
        
        UIColor *bgColor;
        UIColor *titleColor;
        UIFont *titleFont;
        
        bgColor = kDefaultNavyBlueColor;
        titleColor = kDefaultTextHeaderColor;
        titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontHeaderSize];
        
        self.backgroundColor = bgColor;
        
        UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, topPadding, frame.size.width - leftPadding, frame.size.height)];
        titleLabelTmp.text = [title uppercaseString];
        titleLabelTmp.textColor = titleColor;
        titleLabelTmp.font = titleFont;
        titleLabelTmp.backgroundColor = [UIColor clearColor];
        titleLabelTmp.numberOfLines = 0;
        titleLabelTmp.lineBreakMode = UILineBreakModeWordWrap;
        
        _titleLabel = titleLabelTmp;
        [self addSubview:_titleLabel];
        [titleLabelTmp release];
        
        // Star
        UILabel *starLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width - (0.4*frame.size.width), topPadding, 0.4*frame.size.width - leftPadding, frame.size.height)];
        starLabelTmp.text = star;
        starLabelTmp.textColor = kDefaultYellowColor;
        starLabelTmp.backgroundColor = [UIColor clearColor];
        starLabelTmp.textAlignment = UITextAlignmentRight;
        starLabelTmp.font = [UIFont fontWithName:kDefaultFontKSAN size:20.0];
        _starLabel = starLabelTmp;
        [self addSubview:_starLabel];
        [starLabelTmp release];
    }
    return self;
}

#pragma mark Inbox

- (id)initForInboxWithFrame:(CGRect)frame withTitle:(NSString*)title withUnread:(NSString*)unread {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        DataManager *sharedData = [DataManager sharedInstance];
        
        CGFloat leftPadding = 15.0;
        CGFloat topPadding = 0.0;
        
        UIColor *bgColor;
        UIColor *titleColor;
        UIFont *titleFont;
        
        bgColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
        titleColor = colorWithHexString(sharedData.profileData.themesModel.menuH3);
        titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontHeaderSize];
        
        self.backgroundColor = bgColor;
        
        UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, topPadding, frame.size.width - leftPadding, frame.size.height)];
        titleLabelTmp.text = [title uppercaseString];
        titleLabelTmp.textColor = titleColor;
        titleLabelTmp.font = titleFont;
        titleLabelTmp.backgroundColor = [UIColor clearColor];
        titleLabelTmp.numberOfLines = 0;
        titleLabelTmp.lineBreakMode = UILineBreakModeWordWrap;
        
        _titleLabel = titleLabelTmp;
        [self addSubview:_titleLabel];
        
        UILabel *unreadLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width - kDefaultButtonHeight - leftPadding, ((frame.size.height - kDefaultButtonHeight)/2) + topPadding, kDefaultButtonHeight, kDefaultButtonHeight)];
        unreadLabelTmp.textAlignment = UITextAlignmentCenter;
        unreadLabelTmp.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontHeaderSize];
        unreadLabelTmp.textColor = [UIColor whiteColor];
        unreadLabelTmp.backgroundColor = [UIColor redColor];
        unreadLabelTmp.text = unread;
        
        unreadLabelTmp.layer.cornerRadius = kDefaultCornerRadius;
        unreadLabelTmp.layer.masksToBounds = YES;
        
        /*
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontHeaderSize];
        UIButton *button = createButtonWithFrame(CGRectMake(frame.size.width - kDefaultButtonHeight - leftPadding,
                                                            ((frame.size.height - kDefaultButtonHeight)/2) + topPadding, //topPadding + 5
                                                            kDefaultButtonHeight,
                                                            kDefaultButtonHeight),
                                                 buttonTitle,
                                                 buttonFont,
                                                 kDefaultNavyBlueColor,
                                                 kDefaultYellowColor,
                                                 kDefaultYellowColor);
        
        _editProfileButton = button;*/
        _unreadLabel = unreadLabelTmp;
        [self addSubview:_unreadLabel];
        
        [titleLabelTmp release];
        [unreadLabelTmp release];
    }
    return self;
}

#pragma mark Dealloc

- (void)dealloc {
    [_icon release];
    _icon = nil;
    
    [_title release];
    _title = nil;
    
    [_subtitle release];
    _subtitle = nil;
    
    [super dealloc];
}

#pragma mark Setting Properties

- (void)setTitle:(NSString*)theTitle {
    [_title release];
    _title = [theTitle retain];
    
    _titleLabel.text = [_title uppercaseString];
}

- (void)setSubtitle:(NSString*)theSubtitleLabel {
    [_subtitle release];
    _subtitle = [theSubtitleLabel retain];
    
    _subtitleLabel.text = [_subtitle uppercaseString];
}

- (void)setIcon:(NSString *)theIcon {
    [_icon release];
    _icon = [theIcon retain];
    
    _iconLabel.text = _icon;
}

@end
