//
//  SectionHeaderView.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/9/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionHeaderView : UIView {
    NSString *_icon;
    NSString *_title;
    NSString *_subtitle;
    
    //UIView *_grayBackgroundView;
    //BOOL _firstSection;
    
    UILabel *_titleLabel;
    UILabel *_subtitleLabel;
    UIButton *_packageButton;
    UIButton *_quotaCalculatorButton;
    
    UIButton *_editProfileButton;
    UILabel *_starLabel;
    
    UILabel *_unreadLabel;
}

@property (nonatomic, retain) NSString *icon;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *subtitle;

@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *subtitleLabel;
@property (nonatomic, retain) UIButton *packageButton;
@property (nonatomic, retain) UIButton *quotaCalculatorButton;

@property (nonatomic, retain) UIButton *editProfileButton;

@property (nonatomic, retain) UILabel *starLabel;

@property (nonatomic, retain) UILabel *unreadLabel;

//@property (nonatomic, retain) UIView *grayBackgroundView;
//@property (nonatomic, readwrite) BOOL firstSection;

- (id)initWithFrame:(CGRect)frame withLabel:(NSString *)headerTitle withIcon:(NSString *)headerIcon isFirstSection:(BOOL)firstSectionCell;

- (id)initWithFrame:(CGRect)frame withLabel:(NSString *)title isFirstSection:(BOOL)firstSectionCell;

- (id)initWithFrame:(CGRect)frame withLabel:(NSString *)title;

- (id)initWithFrame:(CGRect)frame withLabelForXL:(NSString *)title isFirstSection:(BOOL)firstSectionCell;

- (id)initWithFrame:(CGRect)frame withLabelForXL:(NSString *)title isFirstSection:(BOOL)firstSectionCell isForLeftMenu:(BOOL)leftMenu;

- (id)initWithFrame:(CGRect)frame
          withTitle:(NSString*)title
       withSubtitle:(NSString*)subtitle
     isFirstSection:(BOOL)firstSectionCell
   isZeroTopPadding:(BOOL)zeroTopPadding;

- (id)initForProfileWithFrame:(CGRect)frame withTitle:(NSString*)title withEditButton:(NSString*)buttonTitle;

- (id)initForXLStarWithFrame:(CGRect)frame withTitle:(NSString*)title withStar:(NSString*)star;

- (id)initForInboxWithFrame:(CGRect)frame withTitle:(NSString*)title withUnread:(NSString*)unread;

// TODO : V1.9
// Created for XL Tunai Balance on section header
/**
 *  Section Header View with two labels on the left (Title) and right(Subtitle).
 *
 *  @param frame            The view's frame
 *  @param title            The Section's title
 *  @param subtitle         The Section's subtitle
 *  @param firstSectionCell YES if the view is on the first section
 *
 *  @return The Section View with title and subtitle label
 */
- (id)initWithFrame:(CGRect)frame
     withLabelForXL:(NSString *)title
        andSubtitle:(NSString *)subtitle
  andIsFirstSection:(BOOL)firstSectionCell;

@end
