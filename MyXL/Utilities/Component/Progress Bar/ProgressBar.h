//
//  ProgressBar.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/11/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckUsageXLModel.h"

typedef enum
{
    ProgressBarRed,
    ProgressBarYellow,
    ProgressBarGreen,
    ProgressBarBlack
} ProgressBarColor;

@interface ProgressBar : UIView {
@private
    
    UIView *percentView;
    
    UIView *trackView;
    
    UIView *progressView;
    
    //UIImage *progressFillImage;
}

@property (nonatomic, readwrite, assign) CGFloat progress;

- (id)initWithFrame:(CGRect)frame andProgressBarColor:(ProgressBarColor)barColor withPercentView:(BOOL)showPercentView;

- (UIColor*)colorFromBarDefinition:(ProgressBarColor)barDef;

- (void)addVerticalLine:(float)xPos withText:(NSString*)sizeText;

- (void)addRightText:(NSString*)text;

- (id)initWithFrame:(CGRect)frame withData:(CheckUsageXLModel*)data;

@end
