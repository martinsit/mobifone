//
//  ProgressBar.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/11/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ProgressBar.h"
#import "Constant.h"
#import "PackageAllowanceModel.h"
#import "AXISnetCommon.h"
#import "Language.h"

@implementation ProgressBar

@synthesize progress;

- (id)initWithFrame:(CGRect)frame withData:(CheckUsageXLModel*)data {
    NSLog(@"progressbar frame %@", NSStringFromCGRect(frame));
    if (self = [super initWithFrame:frame])
    {
        // total height space = 80
        CGFloat barHeight = 20.0;
        if(!data.isUnlimited)
        {
            trackView = [[UIView alloc] initWithFrame:CGRectMake(0, 35.0, frame.size.width, barHeight)]; //25
            trackView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:219.0/255.0 blue:219.0/255.0 alpha:1.0];
            [self addSubview:trackView];
            
            CGFloat startX = 0.0;
            CGFloat fullBarWidth = frame.size.width;
            for (int i = 0; i < [data.packageAllowance count]; i++) {
                PackageAllowanceModel *model = [data.packageAllowance objectAtIndex:i];
                NSArray *chunks = [model.remainingPercent componentsSeparatedByString:@"%"];
                NSString *remaining = [chunks objectAtIndex:0];
                CGFloat remainingValue = [remaining floatValue];
                UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(startX, 35.0, (remainingValue/100)*fullBarWidth, barHeight)];
                bar.backgroundColor = legendColorForBar(i);
                [self addSubview:bar];
                startX = bar.frame.origin.x + bar.frame.size.width;
                [bar release];
            }
            
            CGFloat labelHeight = 15;
            CGFloat labelWidth = 100;
            
            // add remaining quota label
            UILabel *remainingLabel = [[UILabel alloc] initWithFrame:CGRectMake(trackView.frame.origin.x, trackView.frame.origin.y - (labelHeight*2), labelWidth, labelHeight)];
            remainingLabel.textAlignment = UITextAlignmentLeft;
            remainingLabel.font = [UIFont fontWithName:kDefaultFontBold size:10.0];
            remainingLabel.textColor = kDefaultNavyBlueColor;
            remainingLabel.backgroundColor = [UIColor clearColor];
            remainingLabel.text = [Language get:@"remaining_quota" alter:nil];
            [self addSubview:remainingLabel];
            [remainingLabel release];
            //
            
            UILabel *zero = [[UILabel alloc] initWithFrame:CGRectMake(trackView.frame.origin.x, trackView.frame.origin.y - labelHeight, labelWidth, labelHeight)];
            zero.textAlignment = UITextAlignmentLeft;
            zero.font = [UIFont fontWithName:kDefaultFontLight size:10.0];
            zero.textColor = kDefaultNavyBlueColor;
            zero.backgroundColor = [UIColor clearColor];
            zero.text = @"0";
            [self addSubview:zero];
            [zero release];
            
            UILabel *maxQuota = [[UILabel alloc] initWithFrame:CGRectMake(trackView.frame.size.width - labelWidth, trackView.frame.origin.y - labelHeight, labelWidth, labelHeight)];
            maxQuota.textAlignment = UITextAlignmentRight;
            maxQuota.font = [UIFont fontWithName:kDefaultFontLight size:10.0];
            maxQuota.textColor = kDefaultNavyBlueColor;
            maxQuota.backgroundColor = [UIColor clearColor];
            maxQuota.text = data.maxQuota;
            [self addSubview:maxQuota];
            [maxQuota release];
            
            labelHeight = 20;
            UILabel *totalRemaining = [[UILabel alloc] initWithFrame:CGRectMake(trackView.frame.origin.x + trackView.frame.size.width - labelWidth,
                                                                                trackView.frame.origin.y + ((trackView.frame.size.height - labelHeight)/2) + 1,
                                                                                labelWidth,
                                                                                labelHeight)];
            totalRemaining.textAlignment = UITextAlignmentRight;
            totalRemaining.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            
            NSArray *chunks = [data.totalRemainingPercent componentsSeparatedByString:@"%"];
            NSString *remaining = [chunks objectAtIndex:0];
            CGFloat remainingValue = [remaining floatValue];
            
            if (remainingValue > 94) {
                totalRemaining.textColor = kDefaultWhiteColor;
            }
            else {
                totalRemaining.textColor = kDefaultBlackColor;
            }
            
            totalRemaining.text = data.totalRemainingPercent;
            totalRemaining.backgroundColor = [UIColor clearColor];
            [self addSubview:totalRemaining];
            [totalRemaining release];
            totalRemaining = nil;
        }
        // TODO: MyXL V1.9
        else // FOR UNLIMITED PACKAGE
        {
            trackView = [[UIView alloc] initWithFrame:CGRectMake(0, 35.0, frame.size.width, barHeight)]; //25
            trackView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:219.0/255.0 blue:219.0/255.0 alpha:1.0];
            [self addSubview:trackView];
            
            CGFloat startX = 0.0;
            CGFloat fullBarWidth = frame.size.width;
            CGFloat FupBarWidth = fullBarWidth * 80/100;
            CGFloat NonFUPBarWidth = fullBarWidth - FupBarWidth;
            
//            NSArray *chunks = [data.totalRemainingPercent componentsSeparatedByString:@"%"];
//            NSString *remaining = [chunks objectAtIndex:0];
//            CGFloat remainingValue = 100 - [remaining floatValue];
            
            // TODO : Take the remaining quota from Package Allowance
            CGFloat usageValue = 0;
            if([data.packageAllowance count] > 0)
            {
                PackageAllowanceModel *pkgModel = [data.packageAllowance objectAtIndex:0];
                NSArray *chunks = [pkgModel.remainingPercent componentsSeparatedByString:@"%"];
                NSString *remaining = [chunks objectAtIndex:0];
                usageValue = 100-[remaining floatValue];
            }
            else
            {
                NSArray *chunks = [data.totalRemainingPercent componentsSeparatedByString:@"%"];
                NSString *remaining = [chunks objectAtIndex:0];
                usageValue = 100-[remaining floatValue];
            }
            
            UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(startX, 35.0, (usageValue/100)*FupBarWidth, barHeight)];
            // Green color
            bar.backgroundColor = legendColorForBar(0);
            [self addSubview:bar];
            startX = bar.frame.origin.x + bar.frame.size.width;
            [bar release];
            bar = nil;
            
            if(usageValue >= 100)
            {
                UIView *nonFUPBar = [[UIView alloc] initWithFrame:CGRectMake(startX + 2, CGRectGetMinY(trackView.frame), 0.5*NonFUPBarWidth, barHeight)];
                // Yellow color
                nonFUPBar.backgroundColor = legendColorForBar(2);
                [self addSubview:nonFUPBar];
                //                        startX = bar.frame.origin.x + bar.frame.size.width;
                [nonFUPBar release];
                nonFUPBar = nil;
            }
            
            // Add FUP limit line inside the bar
            UIView *FUPLimitBar = [[UIView alloc] initWithFrame:CGRectMake(FupBarWidth, 35.0,2, barHeight)];
            // Yellow color
            FUPLimitBar.backgroundColor = [UIColor blackColor];
            [self addSubview:FUPLimitBar];
            [FUPLimitBar release];
            FUPLimitBar = nil;
            
            CGFloat labelHeight = 15;
            CGFloat labelWidth = 200;
            
            // add remaining quota label
            UILabel *remainingLabel = [[UILabel alloc] initWithFrame:CGRectMake(trackView.frame.origin.x, trackView.frame.origin.y - (labelHeight*2), labelWidth, labelHeight)];
            remainingLabel.textAlignment = UITextAlignmentLeft;
            remainingLabel.font = [UIFont fontWithName:kDefaultFontBold size:10.0];
            remainingLabel.textColor = kDefaultNavyBlueColor;
            remainingLabel.backgroundColor = [UIColor clearColor];
            remainingLabel.text = [Language get:@"your_data_usage" alter:nil];
            [self addSubview:remainingLabel];
            [remainingLabel release];
            remainingLabel = nil;
            
            UILabel *zero = [[UILabel alloc] initWithFrame:CGRectMake(trackView.frame.origin.x, trackView.frame.origin.y - labelHeight, 50, labelHeight)];
            zero.textAlignment = UITextAlignmentLeft;
            zero.font = [UIFont fontWithName:kDefaultFontLight size:10.0];
            zero.textColor = kDefaultNavyBlueColor;
            zero.backgroundColor = [UIColor clearColor];
            zero.text = @"0";
            [self addSubview:zero];
            [zero release];
            zero = nil;
            
            UILabel *lblFUP = [[UILabel alloc] initWithFrame:CGRectMake(trackView.frame.origin.x + FupBarWidth - 15, trackView.frame.origin.y - labelHeight, 30, labelHeight)];
            lblFUP.textAlignment = UITextAlignmentCenter;
            lblFUP.font = [UIFont fontWithName:kDefaultFontLight size:10.0];
            lblFUP.textColor = kDefaultNavyBlueColor;
            lblFUP.backgroundColor = [UIColor clearColor];
            lblFUP.text = @"FUP";
            [self addSubview:lblFUP];
            [lblFUP release];
            lblFUP = nil;
            
            UILabel *maxQuota = [[UILabel alloc] initWithFrame:CGRectMake(trackView.frame.size.width - 20, trackView.frame.origin.y - labelHeight, 20, labelHeight)];
            maxQuota.textAlignment = UITextAlignmentRight;
            maxQuota.font = [UIFont fontWithName:kDefaultFontLight size:20.0];
            maxQuota.textColor = kDefaultNavyBlueColor;
            maxQuota.backgroundColor = [UIColor clearColor];
//            maxQuota.backgroundColor = [UIColor redColor];
            maxQuota.text = @"~";
            [self addSubview:maxQuota];
            [maxQuota release];
            maxQuota = nil;
            labelHeight = 20;
        }
    }

    for (UIView *v in self.subviews) {
        NSLog(@"v class %@ and tag %d",[v class], v.tag);
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame andProgressBarColor:(ProgressBarColor)barColor withPercentView:(BOOL)showPercentView
{
    if (self = [super initWithFrame:frame])
    {
        //NSString* progressFillStr = [self getImageNameFromBarDefinition:barColor];
        //progressFillImage = [UIImage imageNamed:progressFillStr];
        
        trackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        trackView.backgroundColor = kDefaultButtonGrayColor;
        [self addSubview:trackView];
        
        progressView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, frame.size.height)];
        UIColor *progressViewColor = [self colorFromBarDefinition:barColor];
        progressView.backgroundColor = progressViewColor;
        [self addSubview:progressView];
        
        if (showPercentView) {
            /*
            percentView = [[UIView alloc] initWithFrame:CGRectMake(LEFT_PADDING, 6, 32, 17)];
            
            //UIImageView* percentImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 17)];
            
            //[percentImageView setImage:[UIImage imageNamed:@"progress-count.png"]];
            
            UILabel* percentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 32, 17)];
            
            [percentLabel setTag:1];
            [percentLabel setText:@"0%"];
            [percentLabel setBackgroundColor:[UIColor clearColor]];
            [percentLabel setFont:[UIFont boldSystemFontOfSize:13]];
            [percentLabel setTextAlignment:UITextAlignmentCenter];
            [percentLabel setAdjustsFontSizeToFitWidth:YES];
            
            //[percentView addSubview:percentImageView];
            [percentView addSubview:percentLabel];
            
            [self addSubview:percentView];*/
        }
        
        self.progress = 0.0f;
    }
    
    return self;
}

- (void)setProgress:(CGFloat)theProgress {
    if (self.progress != theProgress) {
        
        if (theProgress >= 0 && theProgress <= 1) {
            
            progress = theProgress;
            
            CGRect frame = progressView.frame;
            frame.size.width = trackView.frame.size.width * progress;
            progressView.frame = frame;
            
            /*
            CGRect percentFrame = percentView.frame;
            //float percentViewWidth = percentView.frame.size.width;
            //float leftEdge = (progressImageView.frame.size.width - percentViewWidth) - RIGHT_PADDING;
            float leftEdge = (bgImageView.frame.size.width - percentFrame.size.width) / 2;
            float topEdge = (progressImageView.frame.size.height - percentFrame.size.height) / 2;
            //percentFrame.origin.x = (leftEdge < LEFT_PADDING) ? LEFT_PADDING : leftEdge;
            percentFrame.origin.x = leftEdge;
            percentFrame.origin.y = topEdge + 2;
            percentView.frame = percentFrame;
            
            UILabel* percentLabel = (UILabel*)[percentView viewWithTag:1];
            [percentLabel setText:[NSString  stringWithFormat:@"%d%%", (int)(progress*100)]];*/
            
        }
    }
}

- (UIColor*)colorFromBarDefinition:(ProgressBarColor)barDef {
    UIColor *color = nil;
    switch (barDef) {
        case ProgressBarRed:
            //color = [UIColor colorWithRed:223.0/255.0 green:34.0/255.0 blue:39.0/255.0 alpha:1.0];
            color = kDefaultRedColor;
            break;
        case ProgressBarYellow:
            //color = [UIColor colorWithRed:251.0/255.0 green:209.0/255.0 blue:10.0/255.0 alpha:1.0];
            color = kDefaultYellowColor;
            break;
        case ProgressBarGreen:
            //color = [UIColor colorWithRed:57.0/255.0 green:141.0/255.0 blue:66.0/255.0 alpha:1.0];
            color = kDefaultGreenColor;
            break;
        case ProgressBarBlack:
            //color = [UIColor colorWithRed:132.0/255.0 green:132.0/255.0 blue:132.0/255.0 alpha:1.0];
            color = kDefaultDarkGrayColor;
            break;
        default:
            color = kDefaultGreenColor;
            break;
    }
    return color;
}

- (void)addVerticalLine:(float)xPos withText:(NSString*)sizeText {
    float y = trackView.frame.origin.y + 1; // +4
    CGFloat barHeight = trackView.frame.size.height; // -4
    //NSLog(@"barHeight=%f",barHeight);
    float lineHeight = 4.0; //4
    float totalBar = (int)(barHeight/lineHeight);
    //NSLog(@"totalBar=%f",totalBar);
    
    //xPos = (trackView.frame.size.width - 4) * xPos;
    xPos = trackView.frame.size.width * xPos;
    
    for (int i = 0; i < totalBar; i++) {
        //NSLog(@"y=%f",y);
        if (y>barHeight) {
            break;
        }
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(xPos, y, 1, lineHeight)];
        lineView.backgroundColor = [UIColor blackColor];
        [self addSubview:lineView];
        [lineView release];
        y+=lineHeight+1;
    }
    
    CGFloat labelWidth = 50.0;
    CGFloat labelHeight = 10.0;
    //UILabel *sizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos - (labelWidth/2), y+2, labelWidth, labelHeight)];
    UILabel *sizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(xPos - (labelWidth/2),
                                                                   progressView.frame.origin.y + progressView.frame.size.height,
                                                                   labelWidth,
                                                                   labelHeight)];
    sizeLabel.text = sizeText;
    sizeLabel.font = [UIFont fontWithName:kDefaultFontLight size:8.0];
    sizeLabel.adjustsFontSizeToFitWidth = YES;
    sizeLabel.minimumFontSize = 8.0f;
    sizeLabel.textAlignment = UITextAlignmentCenter;
    sizeLabel.textColor = kDefaultTitleFontGrayColor;
    sizeLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:sizeLabel];
    [sizeLabel release];
}

- (void)addRightText:(NSString*)text {
    CGFloat labelWidth = 100.0;
    CGFloat labelHeight = 10.0;
    UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(trackView.frame.origin.x + trackView.frame.size.width - labelWidth,
                                                                    trackView.frame.origin.y + trackView.frame.size.height,
                                                                    labelWidth,
                                                                    labelHeight)];
    rightLabel.text = text;
    rightLabel.font = [UIFont fontWithName:kDefaultFontLight size:8.0];
    rightLabel.adjustsFontSizeToFitWidth = YES;
    rightLabel.minimumFontSize = 8.0f;
    rightLabel.textAlignment = UITextAlignmentRight;
    rightLabel.textColor = kDefaultTitleFontGrayColor;
    rightLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:rightLabel];
    [rightLabel release];
}

@end
