//
//  QuotaCalculatorBar.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 6/27/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuotaCalculatorBar : UIView {
    UIView *bar;
    float barValue;
    float maxBar;
    NSArray *barViewData;
    NSString *quotaString;
    UILabel *quotaLabel;
}

- (id)initWithFrame:(CGRect)frame withData:(NSArray*)data;

@property (nonatomic, retain) UIView *bar;
@property (assign, nonatomic) float barValue;

@property (readwrite) float maxBar;

@property (nonatomic, retain) NSArray *barViewData;

@property (nonatomic, retain) NSString *quotaString;
@property (nonatomic, retain) UILabel *quotaLabel;

- (void)updateValueForBarIndexZero:(float)value;
- (void)updateValueForBarIndexOther:(float)value;

- (void)updateValueForBarIndex:(int)index withValue:(float)value;

@end
