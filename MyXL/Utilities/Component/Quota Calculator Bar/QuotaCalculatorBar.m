//
//  QuotaCalculatorBar.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 6/27/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "QuotaCalculatorBar.h"
#import "InternetUsageModel.h"
#import "AXISnetCommon.h"
#import "DataManager.h"

@implementation QuotaCalculatorBar

@synthesize bar;
@synthesize barValue;

@synthesize maxBar;

@synthesize barViewData;

@synthesize quotaString;
@synthesize quotaLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withData:(NSArray*)data {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        DataManager *sharedData = [DataManager sharedInstance];
        
        //self.maxBar = frame.size.width/[data count];
        
        //NSLog(@"--> frame.size.width = %f",frame.size.width);
        
        NSMutableArray *viewData = [[NSMutableArray alloc] init];
        CGFloat startX = 0.0;
        for (int i = 0; i < [data count]; i++) {
            InternetUsageModel *model = [data objectAtIndex:i];
            
            model.maxBar = model.maxVolumeValue / sharedData.quotaCalculatorData.totalVolumeDaily * frame.size.width;
            //NSLog(@"--> model.maxBar = %f",model.maxBar);
            
            //NSLog(@"--> MAX VOLUME is %f of %f",model.maxVolumeValue,sharedData.quotaCalculatorData.totalVolumeDaily);
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(startX, frame.origin.y, model.sliderValue/model.maxScalePerdayValue*model.maxBar, frame.size.height)];
            view.backgroundColor = colorWithHexString([NSString stringWithFormat:@"%@",model.color]);
            view.tag = i;
            startX += view.frame.size.width;
            
            [viewData addObject:model];
            
            [self addSubview:view];
        }
        
        self.barViewData = [NSArray arrayWithArray:viewData];
        [viewData release];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setBarValue:(float)barValueData {
    /*
    for (UIView *subview in self.subviews)
    {
        if (subview.tag == 0) {
            CGRect frame = subview.frame;
            frame.size.width = barValue1Data/1*maxBar;
            subview.frame = frame;
            
            frame = bar2.frame;
            frame.origin.x = bar1.frame.origin.x + bar1.frame.size.width;
            bar2.frame = frame;
        }
    }*/
    
    
}

- (void)updateValueForBarIndexZero:(float)value {
    
}

- (void)updateValueForBarIndexOther:(float)value {
    
}

- (void)updateValueForBarIndex:(int)index withValue:(float)value {
    
    InternetUsageModel *model = [self.barViewData objectAtIndex:index];
    
    for (UIView *subview in self.subviews) {
        if (subview.tag == index) {
            //NSLog(@"---> subview.tag = %i",subview.tag);
            CGRect frame = subview.frame;
            frame.size.width = value/model.maxScalePerdayValue*model.maxBar;
            //NSLog(@"---> frame.size.width = %f",frame.size.width);
            subview.frame = frame;
            
            int nextIndex = subview.tag + 1;
            UIView *currentView = subview;
            
            while (nextIndex < [self.barViewData count])
            {
                UIView *nextSubview = [self.subviews objectAtIndex:nextIndex];
                frame = nextSubview.frame;
                frame.origin.x = currentView.frame.origin.x + currentView.frame.size.width;
                nextSubview.frame = frame;
                
                nextIndex++;
                currentView = nextSubview;
            }
        }
        
    }
}

@end
