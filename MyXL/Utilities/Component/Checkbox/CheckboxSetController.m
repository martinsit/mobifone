//
//  CheckboxSetController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/21/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CheckboxSetController.h"

@interface CheckboxSetController()
- (void)handleButtonTap:(id)sender;
@end

@implementation CheckboxSetController

@synthesize buttons = _buttons;
@synthesize selectedIndex = _selectedIndex;
@synthesize delegate = _delegate;

#pragma mark - Object lifecycle

- (id)init
{
    self = [super init];
    if (self)
    {
        self.selectedIndex = NSNotFound;
    }
    return self;
}

- (void)dealloc
{
    // Remove this object as a target for events on self.buttons
    for (UIButton *button in self.buttons)
    {
        [button removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
    }
    
    [super dealloc];
}

#pragma mark - Custom accessors

- (void)setButtons:(NSArray *)buttons
{
    if (buttons != _buttons)
    {
        // Remove event handlers from old buttons (no-op if _buttons is nil)
        for (UIButton *button in _buttons)
        {
            [button removeTarget:self action:@selector(handleButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        // Assign new buttons array
        _buttons = buttons;
        
        // Set event handlers on new buttons (no-op if _buttons is nil)
        for (UIButton *button in _buttons)
        {
            [button addTarget:self action:@selector(handleButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
    if (selectedIndex != _selectedIndex)
    {
        _selectedIndex = selectedIndex;
        for (NSUInteger i = 0; i < [self.buttons count]; i++)
        {
            UIButton *button = [self.buttons objectAtIndex:i];
            button.selected = i == selectedIndex;
        }
    }
}

#pragma mark - UI event handlers

- (void)handleButtonTap:(id)sender
{
    UIButton *tappedButton = sender;
    tappedButton.selected = !tappedButton.selected;
    
    NSMutableArray *selectedButton = [NSMutableArray new];
    
    for (UIButton *button in self.buttons)
    {
        //NSLog(@"index %i isSelected: %i",button.tag, button.selected);
        if (button.selected) {
            NSNumber *tag = [NSNumber numberWithInt:button.tag];
            [selectedButton addObject:tag];
        }
    }
    
    NSArray *buttons = [NSArray arrayWithArray:selectedButton];
    [selectedButton release];
    
    if ([self.delegate respondsToSelector:@selector(checkboxSetController:didSelectButtons:)]) {
        [self.delegate checkboxSetController:self
                            didSelectButtons:buttons];
    }
}

@end
