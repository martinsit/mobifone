//
//  CheckboxSetController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/21/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CheckboxSetControllerDelegate;

@interface CheckboxSetController : NSObject

@property (nonatomic, strong) IBOutletCollection (UIButton) NSArray *buttons;
@property (nonatomic, strong) IBOutlet id <CheckboxSetControllerDelegate> delegate;

@property (nonatomic) NSUInteger selectedIndex;

@end

@protocol CheckboxSetControllerDelegate <NSObject>
- (void)checkboxSetController:(CheckboxSetController *)controller
             didSelectButtons:(NSArray*)selectedButtons;
@end