//
//  FBAlertView.m
//  AGAlertViewWithProgressbar Demo
//
//  Created by Tony Hadisiswanto on 5/23/13.
//  Copyright (c) 2013 Artur Grigor. All rights reserved.
//

#import "FBAlertView.h"

@interface FBAlertView ()
{
    UIAlertView *alertView;
    UIButton *checkButton;
    UILabel *shareFBLabel;
    
    struct {
        unsigned int delegateClickedButtonAtIndex:1;
        unsigned int delegateCancel:1;
        unsigned int delegateWillPresentAlertView:1;
        unsigned int delegateDidPresentAlertView:1;
        unsigned int delegateWillDismissWithButtonIndex:1;
        unsigned int delegateDidDismissWithButtonIndex:1;
        unsigned int delegateShouldEnableFirstOtherButton:1;
    } supportedDelegateMethods;
}

@property (nonatomic, retain) UIAlertView *alertView;
@property (nonatomic, retain) UIButton *checkButton;
@property (nonatomic, retain) UILabel *shareFBLabel;

- (void)repositionControls;
- (void)setAutoresizingMask;
- (void)setupAlertView;

@end

@implementation FBAlertView

#pragma mark - Properties

@synthesize shareFBSelected, tag, title, message, delegate, cancelButtonTitle, otherButtonTitles;

- (BOOL)isVisible
{
    return self.alertView.visible;
}

- (void)setShareFBSelected:(BOOL)theShareFBSelected
{
    if (theShareFBSelected) {
        //NSLog(@"Alert FB YES");
		[self.checkButton setSelected:YES];
        shareFBSelected = YES;
	}
	else {
        //NSLog(@"Alert FB NO");
		[self.checkButton setSelected:NO];
        shareFBSelected = NO;
	}
}

/*
- (void)setShareFBWording:(NSString *)theWording {
    if (shareFBWording != theWording)
    {
        if (shareFBWording != nil) {
            [shareFBWording release];
        }
        shareFBWording = [theWording retain];
        
        self.shareFBLabel.text = shareFBWording;
    }
}*/

- (void)setTag:(int)theTag
{
    if (tag != theTag)
    {
        tag = theTag;
        self.alertView.tag = tag;
    }
}

- (void)setTitle:(NSString *)theTitle
{
    if (title != theTitle)
    {
        if (title != nil) {
            [title release];
        }
        title = [theTitle retain];
        
        self.alertView.title = title;
    }
}

- (void)setMessage:(NSString *)theMessage
{
    if (message != theMessage)
    {
        if (message != nil) {
            [message release];
        }
        
        message = [theMessage retain];
        
        self.alertView.message = message;
    }
}

- (void)setDelegate:(id<UIAlertViewDelegate>)theDelegate
{
    if (delegate != theDelegate)
    {
        delegate = theDelegate;
        
        supportedDelegateMethods.delegateClickedButtonAtIndex = ([self.delegate respondsToSelector:@selector(alertView:clickedButtonAtIndex:)]);
        supportedDelegateMethods.delegateCancel = ([self.delegate respondsToSelector:@selector(alertViewCancel:)]);
        supportedDelegateMethods.delegateWillPresentAlertView = ([self.delegate respondsToSelector:@selector(willPresentAlertView:)]);
        supportedDelegateMethods.delegateDidPresentAlertView = ([self.delegate respondsToSelector:@selector(didPresentAlertView:)]);
        supportedDelegateMethods.delegateWillDismissWithButtonIndex = ([self.delegate respondsToSelector:@selector(alertView:willDismissWithButtonIndex:)]);
        supportedDelegateMethods.delegateDidDismissWithButtonIndex = ([self.delegate respondsToSelector:@selector(alertView:didDismissWithButtonIndex:)]);
        supportedDelegateMethods.delegateShouldEnableFirstOtherButton = ([self.delegate respondsToSelector:@selector(alertViewShouldEnableFirstOtherButton:)]);
    }
}

- (void)setCancelButtonTitle:(NSString *)theCancelButtonTitle
{
    if (cancelButtonTitle != theCancelButtonTitle)
    {
        if (cancelButtonTitle != nil) {
            [cancelButtonTitle release];
        }
        
        cancelButtonTitle = [theCancelButtonTitle retain];
        
        [self hide];
        self.alertView = nil;
    }
}

- (void)setOtherButtonTitles:(NSArray *)theOtherButtonTitles
{
    if (otherButtonTitles != theOtherButtonTitles)
    {
        if (otherButtonTitles != nil) {
            [otherButtonTitles release];
        }
        
        otherButtonTitles = [theOtherButtonTitles retain];
        
        [self hide];
        self.alertView = nil;
    }
}

@synthesize alertView, checkButton, shareFBLabel;

- (UIAlertView *)alertView
{
    if (alertView == nil)
    {
        alertView = [[UIAlertView alloc] initWithTitle:self.title message:self.message delegate:self cancelButtonTitle:self.cancelButtonTitle otherButtonTitles:nil];
        
        for (NSString *arg in self.otherButtonTitles) {
            [alertView addButtonWithTitle:arg];
        }
        
        [self setupAlertView];
    }
    
    return alertView;
}

#pragma mark - Object Lifecycle

- (void)dealloc
{
    [self hide];
    
    self.alertView = nil;
    self.checkButton = nil;
    self.shareFBLabel = nil;
    
    [super dealloc];
}

- (id)initWithTitle:(NSString *)theTitle message:(NSString *)theMessage andDelegate:(id<UIAlertViewDelegate>)theDelegate
{
    return [self initWithTitle:theTitle message:theMessage delegate:theDelegate cancelButtonTitle:nil otherButtonTitles:nil];
}

- (id)initWithTitle:(NSString *)theTitle message:(NSString *)theMessage delegate:(id)theDelegate cancelButtonTitle:(NSString *)titleForTheCancelButton otherButtonTitles:(NSString *)titleForTheFirstButton, ... NS_REQUIRES_NIL_TERMINATION
{
    self = [super init];
    if (self)
    {
        NSMutableArray *otherButtonTitlesArray = [[NSMutableArray alloc] init];
        
        va_list args;
        va_start(args, titleForTheFirstButton);
        for (NSString *arg = titleForTheFirstButton; arg != nil; arg = va_arg(args, NSString*))
        {
            [otherButtonTitlesArray addObject:arg];
        }
        va_end(args);
        
        self.shareFBSelected = TRUE;
        self.message = theMessage;
        self.title = theTitle;
        self.delegate = theDelegate;
        self.cancelButtonTitle = titleForTheCancelButton;
        self.otherButtonTitles = otherButtonTitlesArray;
        
        [otherButtonTitlesArray release];
    }
    
    return self;
}

#pragma mark - Public Methods

- (void)show
{
    if (! self.visible)
    {
        [self.alertView show];
    }
}

- (void)hide
{
    if (self.visible)
    {
        [self.alertView dismissWithClickedButtonIndex:0 animated:YES];
    }
}

#pragma mark - UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)thisAlertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (supportedDelegateMethods.delegateClickedButtonAtIndex)
        [self.delegate alertView:thisAlertView clickedButtonAtIndex:buttonIndex];
}

- (void)alertViewCancel:(UIAlertView *)thisAlertView
{
    if (supportedDelegateMethods.delegateCancel)
        [self.delegate alertViewCancel:thisAlertView];
}

- (void)willPresentAlertView:(UIAlertView *)thisAlertView
{
    [self repositionControls];
    
    if (supportedDelegateMethods.delegateWillPresentAlertView)
        [self.delegate willPresentAlertView:thisAlertView];
}

- (void)didPresentAlertView:(UIAlertView *)thisAlertView
{
    if (supportedDelegateMethods.delegateDidPresentAlertView)
        [self.delegate didPresentAlertView:thisAlertView];
}

- (void)alertView:(UIAlertView *)thisAlertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (supportedDelegateMethods.delegateWillDismissWithButtonIndex)
        [self.delegate alertView:thisAlertView willDismissWithButtonIndex:buttonIndex];
}

- (void)alertView:(UIAlertView *)thisAlertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    self.alertView = nil;
    
    if (supportedDelegateMethods.delegateDidDismissWithButtonIndex)
        [self.delegate alertView:thisAlertView didDismissWithButtonIndex:buttonIndex];
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)thisAlertView
{
    if (supportedDelegateMethods.delegateShouldEnableFirstOtherButton)
    {
        return [self.delegate alertViewShouldEnableFirstOtherButton:thisAlertView];
    } else {
        return NO;
    }
}

#pragma mark - Private Methods

- (void)repositionControls
{
    UILabel *messageLabel = nil;
    
    NSInteger idx = 0;
    for (UIView *subview in self.alertView.subviews)
    {
        if (([subview isKindOfClass:[UILabel class]]) && subview != self.shareFBLabel)
        {
            idx++;
            
            if (idx == 2)
            {
                // Second label is the label that's displaying the message
                messageLabel = (UILabel *)subview;
                break;
            }
        }
    }
    
    CGFloat y = messageLabel.frame.origin.y + messageLabel.frame.size.height + 20.f;
    
    self.checkButton.frame = CGRectMake(12.0f, y, 22.0f, 22.0f);
    self.shareFBLabel.frame = CGRectMake(12.0f + 22.0f + 8.0f, y, 230.f, 22.0f);
    
    if (alertView.numberOfButtons > 0)
    {
        [self setAutoresizingMask];
        
        self.alertView.frame = (CGRect){self.alertView.frame.origin, {self.alertView.frame.size.width, self.alertView.frame.size.height + 40.f}};
    }
}

- (void)setAutoresizingMask
{
    for (UIView *subview in self.alertView.subviews)
    {
        if (([subview isKindOfClass:[UIButton class]]) && subview != self.checkButton)
        {
            subview.autoresizingMask = subview.autoresizingMask ^ UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        }
    }
}

- (void)setupAlertView
{
    self.alertView.autoresizesSubviews = YES;
    
    checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.checkButton addTarget:self
                         action:@selector(checkButtonPressed:)
               forControlEvents:UIControlEventTouchDown];
    UIImage *checkOff = [UIImage imageNamed:@"check-off"];
    [self.checkButton setBackgroundImage:checkOff forState:UIControlStateNormal];
    
    UIImage *checkOn = [UIImage imageNamed:@"check-on"];
    [self.checkButton setBackgroundImage:checkOn forState:UIControlStateSelected];
    
    //[self.checkButton setSelected:YES];
    //shareFBSelected = YES;

    [self.alertView addSubview:self.checkButton];
    
    shareFBLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.shareFBLabel.backgroundColor = [UIColor clearColor];
    self.shareFBLabel.textColor = [UIColor whiteColor];
    self.shareFBLabel.font = [UIFont systemFontOfSize:14.0f];
    self.shareFBLabel.text = [NSString stringWithFormat:@"Share to Facebook"];
    self.shareFBLabel.tag = 1;
    //self.shareFBLabel.textAlignment = UITextAlignmentCenter;
    [self.alertView addSubview:self.shareFBLabel];
}

- (void)checkButtonPressed:(id)sender {
    if (!shareFBSelected) {
		[self.checkButton setSelected:YES];
		shareFBSelected = YES;
	}
	else {
		[self.checkButton setSelected:NO];
		shareFBSelected = NO;
	}
}

@end
