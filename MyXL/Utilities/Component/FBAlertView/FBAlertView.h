//
//  FBAlertView.h
//  AGAlertViewWithProgressbar Demo
//
//  Created by Tony Hadisiswanto on 5/23/13.
//  Copyright (c) 2013 Artur Grigor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBAlertView : NSObject <UIAlertViewDelegate> {
    BOOL shareFBSelected;
    //NSString *shareFBWording;
    int tag;
    
    NSString *title;
    NSString *message;
    NSString *cancelButtonTitle;
    NSArray *otherButtonTitles;
}

//@property (nonatomic, readonly, getter=isShareFBSelected) BOOL shareFBSelected;
@property (nonatomic, readwrite) BOOL shareFBSelected;
//@property (nonatomic, retain) NSString *shareFBWording;
@property (nonatomic, readwrite) int tag;

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *cancelButtonTitle;
@property (nonatomic, retain) NSArray *otherButtonTitles;
@property (nonatomic, assign) id<UIAlertViewDelegate> delegate;
@property (nonatomic, readonly, getter=isVisible) BOOL visible;

- (id)initWithTitle:(NSString *)theTitle message:(NSString *)theMessage andDelegate:(id<UIAlertViewDelegate>)theDelegate;
- (id)initWithTitle:(NSString *)theTitle message:(NSString *)theMessage delegate:(id)theDelegate cancelButtonTitle:(NSString *)titleForTheCancelButton otherButtonTitles:(NSString *)titleForTheFirstButton, ... NS_REQUIRES_NIL_TERMINATION;

- (void)show;
- (void)hide;

@end
