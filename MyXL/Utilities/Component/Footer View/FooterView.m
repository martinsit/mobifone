//
//  FooterView.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/3/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "FooterView.h"
#import <QuartzCore/QuartzCore.h>
#import "Constant.h"
#import "AXISnetCommon.h"

@implementation FooterView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor = [UIColor clearColor];
        
        //UILabel *copyrightLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 20)];
        UILabel *copyrightLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(4, 4, frame.size.width, 20)];
        copyrightLabel1.text = @"© 2013 PT. XL AXIATA INDONESIA.";
        //copyrightLabel1.textColor = leftMenuHeaderTextColor; //kDefaultFontGrayColor
        copyrightLabel1.textColor = kDefaultWhiteColor;
        copyrightLabel1.font = [UIFont fontWithName:kDefaultFontLight size:10.0];
        //copyrightLabel1.backgroundColor = leftMenuColor;
        copyrightLabel1.backgroundColor = [UIColor clearColor];
        
        copyrightLabel1.numberOfLines = 0;
        copyrightLabel1.lineBreakMode = UILineBreakModeWordWrap;
        //[copyrightLabel1 sizeToFit];
        
        CGSize size = calculateExpectedSize(copyrightLabel1, copyrightLabel1.text);
        CGRect frameLabel = copyrightLabel1.frame;
        frameLabel.size.height = size.height;
        copyrightLabel1.frame = frameLabel;
        
        //copyrightLabel1.backgroundColor = [UIColor yellowColor];
        [self addSubview:copyrightLabel1];
        [copyrightLabel1 release];
        
        UILabel *copyrightLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(4, copyrightLabel1.frame.origin.y + copyrightLabel1.frame.size.height, frame.size.width, 20)];
        copyrightLabel2.numberOfLines = 0;
        copyrightLabel2.lineBreakMode = UILineBreakModeWordWrap;
        copyrightLabel2.text = @"All rights reserved.";
        copyrightLabel2.textColor = kDefaultWhiteColor;
        copyrightLabel2.font = [UIFont fontWithName:kDefaultFontLight size:10.0];
        //copyrightLabel2.backgroundColor = leftMenuColor;
        copyrightLabel2.backgroundColor = [UIColor clearColor];
        //[copyrightLabel2 sizeToFit];
        //copyrightLabel2.backgroundColor = [UIColor redColor];
        [self addSubview:copyrightLabel2];
        [copyrightLabel2 release];
        
        CGFloat width = 100.0;
        UILabel *versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width - width - (4.0*2), copyrightLabel1.frame.origin.y + copyrightLabel1.frame.size.height, width - 10.0, 20.0)];
        versionLabel.textAlignment = UITextAlignmentRight;
        versionLabel.numberOfLines = 0;
        versionLabel.lineBreakMode = UILineBreakModeWordWrap;
        versionLabel.text = @"Ver. 1.0";
        versionLabel.textColor = kDefaultWhiteColor;
        versionLabel.font = [UIFont fontWithName:kDefaultFontLight size:10.0];
        //versionLabel.backgroundColor = leftMenuColor;
        versionLabel.backgroundColor = [UIColor clearColor];
        //[versionLabel sizeToFit];
        //versionLabel.backgroundColor = [UIColor redColor];
        [self addSubview:versionLabel];
        [versionLabel release];
        
//        self.layer.shadowColor = [[UIColor blackColor] CGColor];
//		self.layer.shadowOffset = CGSizeMake(1.0, 1.0);
//		self.layer.shadowOpacity = 0.40;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
