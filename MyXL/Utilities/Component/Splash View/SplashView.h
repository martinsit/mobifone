//
//  SplashView.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/18/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashView : UIView {
    UIImageView *imageView;
}

@property (nonatomic, retain) UIImageView *imageView;

@end
