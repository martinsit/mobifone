//
//  SplashView.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/18/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "SplashView.h"
#import "UIDeviceHardware.h"

@implementation SplashView

@synthesize imageView = _imageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        NSString *platform = [UIDeviceHardware platformString];
        
        UIImageView *imageViewTmp = [[UIImageView alloc] initWithFrame:frame];
        
        if ([platform isEqualToString:@"iPhone 5"] || [platform isEqualToString:@"iPhone 5 (GSM+CDMA)"]) {
            imageViewTmp.image = [UIImage imageNamed:@"AXISnet-splash-iPhone5-640x1136.png"];
        }
        else {
            if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
                imageViewTmp.image = [UIImage imageNamed:@"AXISnet-splash-iPad-1024x768.png"];
            }
            else {
                imageViewTmp.image = [UIImage imageNamed:@"AXISnet-splash-iPhone4-640x960.png"];
            }
        }
        
        _imageView = imageViewTmp;
        [self addSubview:_imageView];
        [imageViewTmp release];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
