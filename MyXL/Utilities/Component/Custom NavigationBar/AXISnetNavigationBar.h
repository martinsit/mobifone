//
//  AXISnetNavigationBar.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/27/12.
//  Copyright (c) 2012 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXISnetNavigationBar : UINavigationBar {
    //UIButton *theBackButton;
    UIImageView *_contactView;
    UIImageView *_logoView;
    UILabel *_hiLabel;
    UILabel *_msisdnLabel;
    UILabel *_deviceLabel;
    NSString *_msisdn;
    NSString *_device;
    BOOL _isLogin;
    
    UIView *_bottomWhiteBar;
}

//@property (nonatomic, retain) UIButton *theBackButton;
@property (nonatomic, retain) UIImageView *contactView;
@property (nonatomic, retain) UIImageView *logoView;
@property (nonatomic, retain) UILabel *hiLabel;
@property (nonatomic, retain) UILabel *msisdnLabel;
@property (nonatomic, retain) UILabel *deviceLabel;
@property (nonatomic, retain) NSString *msisdn;
@property (nonatomic, retain) NSString *device;
@property (nonatomic, readwrite) BOOL isLogin;
@property (nonatomic, retain) UIView *bottomWhiteBar;
// TODO : Hygiene
@property (nonatomic,readwrite) BOOL showDeviceLabel;
@property (nonatomic, readwrite) BOOL showMsisdnLabel;

- (void)setBackgroundImage:(UIImage *)backgroundImage forBarMetrics:(UIBarMetrics)barMetrics;
- (void)updateLogo:(NSString *)logoName;
- (void)updateGreeting;

@end
