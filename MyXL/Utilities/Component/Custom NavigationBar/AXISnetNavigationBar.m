//
//  AXISnetNavigationBar.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/27/12.
//  Copyright (c) 2012 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "AXISnetNavigationBar.h"
#import <QuartzCore/QuartzCore.h>
#import "Constant.h"
#import "AppDelegate.h"
#import "DataManager.h"
#import "EncryptDecrypt.h"
#import "Language.h"
#import "UIDeviceHardware.h"
#import "AXISnetCommon.h"

@interface AXISnetNavigationBar ()

@property (nonatomic, retain) UIImageView *backgroundImageView;
@property (nonatomic, retain) NSMutableDictionary *backgroundImages;

- (void)updateBackgroundImage;

- (void)updateView;
- (void)addAXISLogo;
- (void)didTapAXISLogoWithGesture:(UITapGestureRecognizer *)tapGesture;
- (void)addContactView;
- (void)addHiLabel;
- (void)addMsisdnLabel;
- (void)addDeviceLabel;
- (void)addMenuButton;
- (void)showMenu:(id)sender;
- (void)addBackButton;
- (void)goBack:(id)sender;
- (void)addSignOutButton;
- (void)signOut:(id)sender;

@end

@implementation AXISnetNavigationBar

@synthesize backgroundImages = _backgroundImages;
@synthesize backgroundImageView = _backgroundImageView;
@synthesize logoView = _logoView;
@synthesize contactView = _contactView;
@synthesize hiLabel = _hiLabel;
@synthesize msisdnLabel = _msisdnLabel;
@synthesize msisdn = _msisdn;
@synthesize isLogin = _isLogin;
@synthesize bottomWhiteBar = _bottomWhiteBar;
@synthesize deviceLabel = _deviceLabel;
@synthesize device = _device;

//@synthesize theBackButton;

#pragma mark - View Lifecycle

- (void)dealloc
{
    [_backgroundImages release];
    [_backgroundImageView release];
    //[theBackButton release];
    
    [_logoView release];
    _logoView = nil;
    
    [_contactView release];
    _contactView = nil;
    
    [_hiLabel release];
    _hiLabel = nil;
    
    [_msisdnLabel release];
    _msisdnLabel = nil;
    
    [_msisdn release];
    _msisdn = nil;
    
    [_deviceLabel release];
    _deviceLabel = nil;
    
    [_device release];
    _device = nil;
    
    [super dealloc];
}

#pragma mark - Background Image

- (NSMutableDictionary *)backgroundImages
{
    if (_backgroundImages == nil)
    {
        _backgroundImages = [[NSMutableDictionary alloc] init];
    }
    
    return _backgroundImages;
}

- (UIImageView *)backgroundImageView
{
    if (_backgroundImageView == nil)
    {
        //_backgroundImageView = [[UIImageView alloc] initWithFrame:[self bounds]];
        
        // Fixing UINavigationBar for iOS 7
        if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO) {
            //NSLog(@"iOS 7 UINavBar");
            _backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 84.0)];
        } else {
            _backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 64.0)];
        }
        //---------------------------------
        
        [_backgroundImageView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self insertSubview:_backgroundImageView atIndex:0];
    }
    
    return _backgroundImageView;
}

- (void)setBackgroundImage:(UIImage *)backgroundImage forBarMetrics:(UIBarMetrics)barMetrics
{
    if ([UINavigationBar instancesRespondToSelector:@selector(setBackgroundImage:forBarMetrics:)])
    {
        if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO) {
            [super setBackgroundImage:backgroundImage forBarPosition:UIBarPositionAny barMetrics:barMetrics];
        }
        else {
            [super setBackgroundImage:backgroundImage forBarMetrics:barMetrics];
        }
    }
    else
    {
        [[self backgroundImages] setObject:backgroundImage forKey:[NSNumber numberWithInt:barMetrics]];
        [self updateBackgroundImage];
    }
}

- (void)updateBackgroundImage
{
    //NSLog(@"updateBackgroundImage");
    UIBarMetrics metrics = ([self bounds].size.height > 40.0) ? UIBarMetricsDefault : UIBarMetricsLandscapePhone;
    UIImage *image = [[self backgroundImages] objectForKey:[NSNumber numberWithInt:metrics]];
    if (image == nil && metrics != UIBarMetricsDefault)
    {
        image = [[self backgroundImages] objectForKey:[NSNumber numberWithInt:UIBarMetricsDefault]];
    }
    
    if (image != nil)
    {
        [[self backgroundImageView] setImage:image];
    }
}

#pragma mark - Layout

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (_backgroundImageView != nil)
    {
        [self updateBackgroundImage];
        [self sendSubviewToBack:_backgroundImageView];
        
        [self updateView];
    }
    
    else {
        [self updateView];
    }
}

#pragma mark - Selector

- (void)updateView {
    if (_isLogin) {
        
        //NSLog(@"is LOGIN");
        
        /*
        if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO) {
            
            if (_bottomWhiteBar == nil) {
                [self addWhiteBar];
            }
            
            if (_logoView == nil) {
                [self addAXISLogo];
            }
        }
        else {
            if (_logoView == nil) {
                [self addAXISLogo];
            }
        }*/
        /*
        if (_bottomWhiteBar == nil) {
            [self addWhiteBar];
        }*/
        
        if (_logoView == nil) {
            [self addAXISLogo];
        }
        
        // TODO : Hygiene
        if(_showDeviceLabel)
        {
            if (_bottomWhiteBar == nil) {
                [self addWhiteBar];
            }
            
            if (_deviceLabel == nil) {
                [self addDeviceLabel];
            }
            
            // clear any previous msisdn text
            if (_msisdnLabel != nil) {
                _msisdnLabel.text = @"";
                _msisdnLabel = nil;
            }
            
            // clear any previous name text
            if (_hiLabel != nil) {
                _hiLabel.text = @"";
                _hiLabel = nil;
            }
            
            // clear any previous name text
            if (_contactView != nil) {
                _contactView = nil;
            }
        }
        
        if(_showMsisdnLabel)
        {
            if (_msisdnLabel != nil) {
                _msisdnLabel = nil;
            }
            [self addMsisdnLabel];
            
            if (_hiLabel != nil) {
                _hiLabel = nil;
            }
            [self addHiLabel];
            
            if (_contactView != nil) {
                _contactView = nil;
            }
            [self addContactView];
        }
        
    }
    else {
        /*
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            if (_bottomWhiteBar == nil) {
                [self addWhiteBar];
            }
        }*/
        
        if (_bottomWhiteBar == nil) {
            [self addWhiteBar];
        }
        
        if (_logoView == nil) {
            [self addAXISLogo];
        }
        
        if (_contactView == nil) {
            [self addContactView];
        }
        
        if (_hiLabel == nil) {
            [self addHiLabel];
        }
        
        if (_msisdnLabel == nil) {
            [self addMsisdnLabel];
        }
        
        if (_deviceLabel == nil) {
            [self addDeviceLabel];
        }
    }
}

- (void)addAXISLogo {
    NSString *documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *logoName = @"";
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else if ([sharedData.profileData.telcoOperator isEqualToString:@"AXIS"]) {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    else {
        logoName = XL_LOGO_RETINA_NAME;
    }
    NSString *mediaPath = [documentDirectory stringByAppendingPathComponent:logoName];
    //UIImage *logo = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MyXL-logo" ofType:@"png"]];
    UIImage *logo = [UIImage imageWithContentsOfFile:mediaPath];
    
    // TODO : BUG FIX MISSING LOGO
    if(logo == nil)
    {
        if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
            logo = [UIImage imageNamed:@"xl-logo"];
        }
        else if ([sharedData.profileData.telcoOperator isEqualToString:@"AXIS"]) {
            logo = [UIImage imageNamed:@""];
        }
        else
        {
            logo = [UIImage imageNamed:@"xl-logo@2x.png"];
        }
    }
    
    CGFloat width = 62.0;
    CGFloat height = 40.0;
    //CGFloat width = 121.0;
    //CGFloat height = 44.0;
    /*
    UIImageView *logoViewTmp = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width - width,
                                                                          64/2 - height/2,
                                                                          width,
                                                                          height)];*/
    
    UIImageView *logoViewTmp = nil;
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
//        CGRect screenBound = [[UIScreen mainScreen] bounds];
//        CGSize screenSize = screenBound.size;
        //CGFloat screenWidth = screenSize.width;
        //NSLog(@"screenWidth = %f",screenWidth);
//        CGFloat screenHeight = screenSize.height;
        //NSLog(@"screenHeight = %f",screenHeight);
        
        // TODO : Hygiene
        /*
         * Changes by iNot 28 March 2015:
         * define macro SCREEN_WIDTH and SCREEN_HEIGHT in Constant.h
         */
        logoViewTmp = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_HEIGHT - width - 10.0,
                                                                    0,
                                                                    width,
                                                                    height)];
//        logoViewTmp = [[UIImageView alloc] initWithFrame:CGRectMake(screenHeight - width - 10.0,
//                                                                    0.0,
//                                                                    width,
//                                                                    height)];
    }
    else {
        /*
        // Center
        logoViewTmp = [[UIImageView alloc] initWithFrame:CGRectMake((self.bounds.size.width - width)/2.0,
                                                                    0.0,
                                                                    width,
                                                                    height)];*/
        
        // Right
        logoViewTmp = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width - width - 10.0,
                                                                    (self.bounds.size.height - height)/2,
                                                                    width,
                                                                    height)];
        
        //NSLog(@"logo %@", NSStringFromCGRect(logoViewTmp.frame));
    }
    
    logoViewTmp.image = logo;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(didTapAXISLogoWithGesture:)];
    [logoViewTmp setUserInteractionEnabled:YES];
    [logoViewTmp addGestureRecognizer:tapGesture];
    [tapGesture release];
    
    _logoView = logoViewTmp;
    
    [self addSubview:_logoView];
    [logoViewTmp release];
}

- (void)didTapAXISLogoWithGesture:(UITapGestureRecognizer *)tapGesture {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.navigationController popToRootViewControllerAnimated:YES];
}

- (void)addHiLabel {
    //UILabel *hiLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(5, 44 - 1, 30, 20)];
    UILabel *hiLabelTmp;
    if (IS_IPAD) {
        hiLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(15, 44, 200, 15)];
    }
    else {
        if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO) {
            hiLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(15, 44, 200, 15)];
        }
        else {
            hiLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(5, 47, 200, 15)];
        }
    }
    hiLabelTmp.backgroundColor = [UIColor clearColor];
    DataManager *sharedData = [DataManager sharedInstance];
    hiLabelTmp.text = [NSString stringWithFormat:@"%@, %@",[Language get:@"hi" alter:nil],sharedData.profileData.fullName];
    /*
     * Restructure & Reskinning
     */
    
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        hiLabelTmp.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
    }
    else {
        hiLabelTmp.textColor = kDefaultWhiteColor;
    }
    hiLabelTmp.font = [UIFont fontWithName:kDefaultFontLight size:10.0];
	
    _hiLabel = hiLabelTmp;
    
    [self addSubview:_hiLabel];
    [hiLabelTmp release];
}

- (void)addContactView {
    UIImageView *contactTmp;
    if (IS_IPAD) {
        contactTmp = [[UIImageView alloc] initWithFrame:CGRectMake(15, 44+17, 11, 8)];
    }
    else {
        if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO) {
            contactTmp = [[UIImageView alloc] initWithFrame:CGRectMake(15, 44+17, 11, 8)];
        }
        else {
            contactTmp = [[UIImageView alloc] initWithFrame:CGRectMake(5, 47+17, 11, 8)];
        }
    }
    contactTmp.image = [UIImage imageNamed:@"mobile-qi-numb.png"];
    
    _contactView = contactTmp;
    
    [self addSubview:_contactView];
    [_contactView release];
}

- (void)addMsisdnLabel {
    
    //UILabel *msisdnLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(35, 44 - 1, 200, 20)];
    UILabel *msisdnLabelTmp;
    if (IS_IPAD) {
        msisdnLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(15+19, 44+14, 200, 15)];
    }
    else {
        if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO) {
            msisdnLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(15+19, 44+14, 200, 15)];
        }
        else {
            msisdnLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(5+19, 47+14, 200, 15)];
        }
    }
    
    msisdnLabelTmp.backgroundColor = [UIColor clearColor];
    
    /*
     * Restructure & Reskinning
     */
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        msisdnLabelTmp.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
    }
    else {
        msisdnLabelTmp.textColor = kDefaultWhiteColor;
    }
    
    msisdnLabelTmp.font = [UIFont fontWithName:kDefaultFontLight size:10.0];
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    
    // TODO : Hygiene
    // Show msisdn label at the top of disclaimer page
    if(_showMsisdnLabel && [sharedData.tempMsisdn length] > 0)
    {
        msisdn = sharedData.tempMsisdn;
    }
    
    if ([msisdn length] > 0) {
        // TODO : Hygiene
        if([msisdn hasPrefix:@"62"])
            msisdn = [msisdn stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"0"];
    }
    
    _msisdn = msisdn;
    
    msisdnLabelTmp.text = [NSString stringWithFormat:@"%@",_msisdn];
    
    _msisdnLabel = msisdnLabelTmp;
    
    [self addSubview:_msisdnLabel];
    [msisdnLabelTmp release];
}

- (void)addDeviceLabel {
    UILabel *deviceLabelTmp;
    if (IS_IPAD) {
        deviceLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(1024.0 - 200 - 15, 44, 200, 30)];
    }
    else {
        if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
            deviceLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width - 150 - 15, 44, 150, 30)];
        }
        else {
            deviceLabelTmp = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width - 150 - 15, 47, 150, 30)];
        }
    }
    deviceLabelTmp.backgroundColor = [UIColor clearColor];
    
    /*
     * Restructure & Reskinning
     */
    DataManager *sharedData = [DataManager sharedInstance];
    //NSLog(@"header gradient %@ and %@",sharedData.profileData.themesModel.headerGradientStart, sharedData.profileData.themesModel.headerGradientEnd );
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        deviceLabelTmp.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
    }
    else {
        deviceLabelTmp.textColor = kDefaultWhiteColor;
    }
    
    deviceLabelTmp.font = [UIFont fontWithName:kDefaultFontLight size:10.0];
    deviceLabelTmp.textAlignment = UITextAlignmentRight;
    _device = [UIDeviceHardware platformString];
    deviceLabelTmp.text = _device;
    
    _deviceLabel = deviceLabelTmp;
    
    [self addSubview:_deviceLabel];
    [deviceLabelTmp release];
}

- (void)addWhiteBar {
    
    UIView *viewBar;
    if (IS_IPAD) {
        viewBar = [[UIView alloc] initWithFrame:CGRectMake(0.0, 44.0, 1024.0, 30.0)];
    }
    else {
        viewBar = [[UIView alloc] initWithFrame:CGRectMake(0.0, 44.0, self.frame.size.width, 30.0)];
    }
    
    /*
     * Restructure & Reskinning
     */
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        // AXIS
        viewBar.backgroundColor = colorWithHexString(sharedData.profileData.themesModel.header);
    }
    else if ([sharedData.profileData.themesModel.header isEqualToString:@""]) {
        // XL
        viewBar.backgroundColor = colorWithHexString(sharedData.profileData.themesModel.headerGradientEnd);
    }
    else {
        viewBar.backgroundColor = kDefaultNavyBlueColor;
    }
    
    // Old Color
    //viewBar.backgroundColor = [UIColor colorWithRed:25.0/255.0 green:47.0/255.0 blue:124.0/255.0 alpha:1.0];
    
    CGSize mainViewSize = viewBar.bounds.size;
    CGFloat borderWidth = 1.0;
    
    UIColor *borderColor = [UIColor whiteColor];
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, mainViewSize.width, borderWidth)];
    topView.opaque = YES;
    topView.backgroundColor = borderColor;
    topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    [viewBar addSubview:topView];
    
    _bottomWhiteBar = viewBar;
    
    [self addSubview:_bottomWhiteBar];
    [viewBar release];
}

- (void)addMenuButton {
    
    // A button stretched with separate images for normal and highlighted states
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(5.0, 10.0, 45.0, 30.0);
    //[button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //[button setBackgroundImage:buttonPressedImage forState:UIControlStateHighlighted];
    //[button setBackgroundColor:[UIColor colorWithRed:0.93 green:0.17 blue:0.45 alpha:1]];
    [button setBackgroundColor:kDefaultPinkColor];
    
//    button.layer.borderColor = [UIColor grayColor].CGColor;
//    button.layer.borderWidth = 0.5f;
    button.layer.cornerRadius = kDefaultCornerRadius;
    
    UniChar club = 0x005C;
    NSString *titleString = [NSString stringWithCharacters:&club length:1];
    [button setTitle:titleString forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"K-SAN" size:22.0];
    [button addTarget:self
               action:@selector(showMenu:)
     forControlEvents:UIControlEventTouchDown];
    //
    
    [self addSubview:button];
}

- (void)showMenu:(id)sender {
    //NSLog(@"showMenu");
}

- (void)addBackButton {
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(55.0, 10.0, 45.0, 30.0);
    [button setBackgroundColor:kDefaultButtonGrayColor];
    //button.layer.borderColor = [UIColor grayColor].CGColor;
    //button.layer.borderWidth = 0.5f;
    button.layer.cornerRadius = kDefaultCornerRadius;
    [button setTitle:@"f" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"K-SAN" size:22.0];
    button.titleLabel.textColor = kDefaultPurpleColor;
    [button addTarget:self
               action:@selector(goBack:)
     forControlEvents:UIControlEventTouchDown];
    
    [self addSubview:button];
}

- (void)goBack:(id)sender {
    //NSLog(@"go back");
    //UINavigationController *navController =
}

- (void)addSignOutButton {
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(105.0, 10.0, 45.0, 30.0);
    [button setBackgroundColor:kDefaultButtonGrayColor];
    //button.layer.borderColor = [UIColor grayColor].CGColor;
    //button.layer.borderWidth = 0.5f;
    button.layer.cornerRadius = kDefaultCornerRadius;
    [button setTitle:@"E" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"K-SAN" size:22.0];
    button.titleLabel.textColor = kDefaultPinkColor;
    [button addTarget:self
               action:@selector(signOut:)
     forControlEvents:UIControlEventTouchDown];
    
    [self addSubview:button];
}

- (void)signOut:(id)sender {
    //NSLog(@"sign out");
}
/*
- (CGSize)sizeThatFits:(CGSize)size {
    CGSize newSize = CGSizeMake(self.frame.size.width,64.0);
    return newSize;
}*/

- (void)updateLogo:(NSString *)logoName {
    DataManager *sharedData = [DataManager sharedInstance];
    UIImage *logo = [UIImage imageWithContentsOfFile:logoName];
    NSLog(@"--> LOGO = %@",logoName);
    // TODO : BUG FIX MISSING LOGO
    if(logo == nil)
    {
        NSLog(@"--> NIL <--");
        if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
            logo = [UIImage imageNamed:@"xl-logo"];
        }
        else if ([sharedData.profileData.telcoOperator isEqualToString:@"AXIS"]) {
            logo = [UIImage imageNamed:@""];
        }
        else
        {
            logo = [UIImage imageNamed:@"xl-logo@2x.png"];
        }
    }
    _logoView.image = logo;
//    _logoView.image = [UIImage imageWithContentsOfFile:logoName];
}

- (void)updateGreeting {
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *name = sharedData.profileData.fullName;
    _hiLabel.text = [NSString stringWithFormat:@"%@, %@",[Language get:@"hi" alter:nil], name];
}

#pragma mark Setting Properties

- (void)setIsLogin:(BOOL)isLogin {
    _isLogin = isLogin;
    //[self refresh];
}

- (void)refresh {
//    if (_isLogin) {
//        _hiLabel.hidden = YES;
//        _msisdnLabel.hidden = YES;
//    }
    
    //NSLog(@"REFRESH");
    _msisdnLabel.text = _msisdn;
}

- (void)setMsisdn:(NSString *)msisdn {
    [_msisdn release];
    _msisdn = [msisdn retain];
    [self refresh];
}

@end
