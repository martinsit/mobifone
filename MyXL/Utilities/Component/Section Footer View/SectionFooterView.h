//
//  SectionFooterView.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/13/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionFooterView : UIView {
    UIColor     *strokeColor;
    UIColor     *rectColor;
    UIColor     *grayBackgroundColor;
    CGFloat     strokeWidth;
    CGFloat     cornerRadius;
    BOOL        isLastSection;
}

@property (nonatomic, retain) UIColor *strokeColor;
@property (nonatomic, retain) UIColor *rectColor;
@property (nonatomic, retain) UIColor *grayBackgroundColor;
@property CGFloat strokeWidth;
@property CGFloat cornerRadius;
@property (readwrite) BOOL isLastSection;

- (id)initWithFrame:(CGRect)frame lastSection:(BOOL)lastSectionCell;

@end
