//
//  SectionFooterView.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/13/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "SectionFooterView.h"
#import "AXISnetCommon.h"
#import "Constant.h"

@implementation SectionFooterView

@synthesize strokeColor;
@synthesize rectColor;
@synthesize grayBackgroundColor;
@synthesize strokeWidth;
@synthesize cornerRadius;
@synthesize isLastSection;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = NO;
        self.strokeColor = kDefaultPinkColor;
        self.rectColor = kDefaultBaseColor;
        self.grayBackgroundColor = kDefaultButtonGrayColor;
        self.strokeWidth = kDefaultStrokeWidth;
        self.cornerRadius = kDefaultCornerRadius;
        self.backgroundColor = kDefaultButtonGrayColor;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame lastSection:(BOOL)lastSectionCell {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = NO;
        self.strokeColor = kDefaultBlueColor;
        self.rectColor = kDefaultWhiteColor;
        //self.rectColor = [UIColor whiteColor];
        self.grayBackgroundColor = kDefaultWhiteColor;
        self.strokeWidth = kDefaultStrokeWidth;
        self.cornerRadius = kDefaultCornerRadius;
        self.backgroundColor = kDefaultWhiteColor;
        self.isLastSection = lastSectionCell;
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat leftPadding = 0.0;
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        leftPadding = 55.0;
    }
    else {
        if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO) {
            leftPadding = 11.0;
        }
        else {
            leftPadding = 20.0;
        }
    }
    
    //CGRect backgroundRect = CGRectMake(leftPadding + 0.5, rect.origin.y, rect.size.width - (leftPadding*2) - (0.5*2), rect.size.height/4.0);
    CGRect backgroundRect = CGRectMake(leftPadding, rect.origin.y, rect.size.width - (leftPadding*2), rect.size.height/3);
    //CGRect strokeRect = rectFor1PxStroke(CGRectInset(backgroundRect, 5.0, 5.0));
    CGRect strokeRect = backgroundRect;
    //strokeRect.size.height -= 1;
    strokeRect = rectFor1PxStroke(strokeRect);
    
    //bottomRoundedRect(context, strokeRect, cornerRadius, self.rectColor.CGColor, self.strokeColor.CGColor, strokeWidth);
    bottomRoundedRect(context, strokeRect, cornerRadius, [UIColor whiteColor].CGColor, self.strokeColor.CGColor, strokeWidth);
    
    leftPadding = 0.0;
    CGFloat width = 0.0;
    
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        leftPadding = 0.0;
        width = 45.0;
    }
    else {
        leftPadding = 0.0;
        width = 10.0;
    }
    
    CGRect leftBackgroundRect = CGRectZero;
    CGRect rightBackgroundRect = CGRectZero;
    if (isLastSection) {
        leftBackgroundRect = CGRectMake(leftPadding, self.bounds.origin.y, width*2, self.bounds.size.height);
        addLeftBottomRoundedRect(context, leftBackgroundRect, cornerRadius, self.rectColor.CGColor);
        
        leftPadding = self.bounds.size.width - (width*2);
        rightBackgroundRect = CGRectMake(leftPadding, self.bounds.origin.y, width*2, self.bounds.size.height);
        addRightBottomRoundedRect(context, rightBackgroundRect, cornerRadius, self.rectColor.CGColor);
    }
    else {
        leftBackgroundRect = CGRectMake(leftPadding, self.bounds.origin.y, width, self.bounds.size.height);
        addBackground(context, leftBackgroundRect, self.rectColor.CGColor);
        
        leftPadding = self.bounds.size.width - width;
        rightBackgroundRect = CGRectMake(leftPadding, self.bounds.origin.y, width, self.bounds.size.height);
        addBackground(context, rightBackgroundRect, self.rectColor.CGColor);
    }
    
    /*
    CGRect leftBackgroundRect = CGRectMake(leftPadding, self.bounds.origin.y, width, self.bounds.size.height);
    addBackground(context, leftBackgroundRect, self.rectColor.CGColor);
    
    leftPadding = self.bounds.size.width - width;
    CGRect rightBackgroundRect = CGRectMake(leftPadding, self.bounds.origin.y, width, self.bounds.size.height);
    addBackground(context, rightBackgroundRect, self.rectColor.CGColor);*/
}

- (void)dealloc {
    [strokeColor release];
    [rectColor release];
    [super dealloc];
}

@end
