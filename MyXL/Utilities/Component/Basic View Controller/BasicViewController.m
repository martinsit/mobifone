//
//  BasicViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/7/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BasicViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "AppDelegate.h"

#import "UpdateProfileViewController.h"
#import "ChangePasswordViewController.h"
#import "NotificationViewController.h"
#import "IIViewDeckController.h"

#import "DataManager.h"

#import "AdsRequest.h"
#import "AdsModel.h"
#import "KASlideShow.h"

#import "AXISnetNavigationBar.h"
#import "GroupingAndSorting.h"

#import "ContactUsModel.h"
#import "ContactUsViewController.h"

#import "HomeViewController.h"
#import "CustomBadge.h"

@interface BasicViewController ()

- (void)createBackButton;
- (void)createAccountAndNotificationButton;
- (void)createAccountNotificationAndBackButton;
- (void)createMenuAccountNotificationAndBackButton;
- (void)createMenuAccountNotificationAndBackButtonLevel2;
- (void)createMenuAccountNotificationSearchAndBackButton;

@property (strong, nonatomic) AdsRequest *adsRequest;
@property (strong, nonatomic) KASlideShow *slideshow;

@property (strong, readwrite, nonatomic) REMenu *menu;

// TODO : V1.9
// Badge Icon Count
@property (nonatomic) NSInteger unreadMessageCount;

@end

@implementation BasicViewController

@synthesize hud;
@synthesize headerTitleMaster = _headerTitleMaster;
@synthesize headerIconMaster = _headerIconMaster;
@synthesize barButtonItemType;
@synthesize badgeNotif = _badgeNotif;

@synthesize adsContent = _adsContent;

@synthesize settingBtn;
@synthesize inboxBtn;

//@synthesize dropdownMenuCreated;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.view.backgroundColor = kDefaultWhiteSmokeColor;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    float currentVersion = 7.0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= currentVersion) {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    
    [self createBackButton];
}

// TODO : Hygiene
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(_isBackToRoot)
    {
        [self createBackToRootButton];
    }
}

// TODO : V1.9
-(void)viewDidAppear:(BOOL)animated
{
//    self.screenName = @"BASICVIEW";
    // TODO : V1.9
    [self requestUnreadNotification];
    [super viewDidAppear:animated];
}

// TODO : Hygiene
-(void)viewDidDisappear:(BOOL)animated
{
    [self.hud hide:YES];
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.headerTitleMaster = nil;
    self.headerIconMaster = nil;
    self.badgeNotif = nil;
    self.adsContent = nil;
}

- (void)dealloc {
	// Should never be called, but just here for clarity really.
    [hud release];
    hud = nil;
    
    [_headerTitleMaster release];
    _headerTitleMaster = nil;
    [_headerIconMaster release];
    _headerIconMaster = nil;
    
    [_badgeNotif release];
    _badgeNotif = nil;
    
    [_adsContent release];
    _adsContent = nil;
    
	[super dealloc];
}

// Autorotation (iOS <= 5.x)
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
                interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    }
    else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}

// Autorotation (iOS >= 6.0)
- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
    }
    else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

// For iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Selector

- (void)activePage:(NSString*)pageName {
    self.screenName = pageName;
}

- (void)createBarButtonItem:(BarButtonItemType)type {
    self.navigationItem.leftBarButtonItem = nil;
    
    barButtonItemType = type;
    
    if (type == PROFILE_NOTIF) {
        [self createAccountAndNotificationButton];
    }
    else if (type == PROFILE_NOTIF_BACK) {
        [self createAccountNotificationAndBackButton];
    }
    else if (type == MENU_PROFILE_NOTIF_BACK) {
        [self createMenuAccountNotificationAndBackButton];
    }
    else if (type == MENU_PROFILE_NOTIF_BACK_LEVEL2) { //done
        [self createMenuAccountNotificationAndBackButtonLevel2];
    }
    else if (type == MENU_PROFILE_NOTIF_SEARCH_BACK) {
        [self createMenuAccountNotificationSearchAndBackButton];
    }
    else if (type == BACK_NOTIF) {
        [self createBackAndNotificationButton];
    }
    else if (type == MENU_BACK_LEVEL2) { //
        [self createMenuAndBackButtonLevel2];
    }
    // TODO : NEPTUNE
    else if(type == MENU_NOTIF)
    {
        [self createMenuAndNotification];
    }
    else {
        [self createBackButton]; //done
    }
}

// TODO : NEPTUNE
-(void)createMenuAndNotification
{
    DataManager *sharedData = [DataManager sharedInstance];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 44)]; //84
    view.backgroundColor = [UIColor clearColor];
    
    UniChar menu = 0x005C;
    NSString *menuString = [NSString stringWithCharacters:&menu length:1];
    UIButton *menuButton = nil;
    //------------------//
    // Reskinning       //
    //------------------//
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        
        // AXIS
        
        menuButton = createIconicButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 menuString,
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 kDefaultWhiteColor,
                                                 colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                 colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    else {
        
        // XL
        /*
         menuButton = createIconicButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
         menuString,
         [UIFont fontWithName:kDefaultFontKSAN size:18.0],
         colorWithHexString(sharedData.profileData.themesModel.menuColor),
         kDefaultWhiteColor,
         colorWithHexString(sharedData.profileData.themesModel.menuFeature));*/
        
        menuButton = createIconicButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 menuString,
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 kDefaultWhiteColor,
                                                 [UIColor clearColor],
                                                 colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    
    [menuButton addTarget:self
                   action:@selector(performMenu:)
         forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:menuButton];
    
//    UIButton *backButton = nil;
//    //------------------//
//    // Reskinning       //
//    //------------------//
//    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
//        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
//        
//        // AXIS
//        
//        backButton = createIconicButtonWithFrame(CGRectMake(menuButton.frame.origin.x + menuButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
//                                                 @"f",
//                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
//                                                 colorWithHexString(sharedData.profileData.themesModel.menuColor),
//                                                 [UIColor clearColor],
//                                                 [UIColor clearColor]);
//    }
//    else {
//        
//        // XL
//        
//        backButton = createIconicButtonWithFrame(CGRectMake(menuButton.frame.origin.x + menuButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
//                                                 @"f",
//                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
//                                                 kDefaultWhiteColor,
//                                                 [UIColor clearColor],
//                                                 colorWithHexString(sharedData.profileData.themesModel.menuFeature));
//    }
//    
//    [backButton addTarget:self
//                   action:@selector(performBackToRoot:)
//         forControlEvents:UIControlEventTouchDown];
//    
//    [view addSubview:backButton];
    
    //------------------//
    // Reskinning       //
    //------------------//
    UIButton *notifButton = nil;
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        
        // AXIS
        
        notifButton = createIconicButtonWithFrame(CGRectMake(CGRectGetMaxX(menuButton.frame) + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                  @"P",
                                                  [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                  colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                  [UIColor clearColor],
                                                  [UIColor clearColor]);
    }
    else {
        
        // XL
        
        notifButton = createIconicButtonWithFrame(CGRectMake(CGRectGetMaxX(menuButton.frame) + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                  @"P",
                                                  [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                  kDefaultWhiteColor,
                                                  [UIColor clearColor],
                                                  colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    
    
    [notifButton addTarget:self
                    action:@selector(performNotification:)
          forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:notifButton];
    
    UIBarButtonItem *tmp = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = tmp;
    
    [tmp release];
    [view release];
}

- (void)createBackAndNotificationButton {
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    view.backgroundColor = [UIColor clearColor];
    
    //------------------//
    // Reskinning       //
    //------------------//
    
    UIButton *backButton = nil;
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        // AXIS
        backButton = createButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                           @"f",
                                           [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                           colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                           [UIColor clearColor],
                                           [UIColor clearColor]);
    }
    else {
        // XL
        backButton = createButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                           @"f",
                                           [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                           kDefaultWhiteColor,
                                           [UIColor clearColor],
                                           colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    
    [backButton addTarget:self
                   action:@selector(performBack:)
         forControlEvents:UIControlEventTouchDown];
    [view addSubview:backButton];
    
    //------------------//
    // Reskinning       //
    //------------------//
    UIButton *notifButton = nil;
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        // AXIS
        notifButton = createButtonWithFrame(CGRectMake(backButton.frame.origin.x + backButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                            @"P",
                                            [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                            colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                            [UIColor clearColor],
                                            [UIColor clearColor]);
    }
    else {
        // XL
        notifButton = createButtonWithFrame(CGRectMake(backButton.frame.origin.x + backButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                            @"P",
                                            [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                            kDefaultWhiteColor,
                                            [UIColor clearColor],
                                            colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    
    // TODO : V1.9
    // Add badge count on Message Icon
    notifButton.tag = NOTIF_BUTTON_TAG;
    
    [notifButton addTarget:self
                    action:@selector(performNotification:)
          forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:notifButton];
    
    UIBarButtonItem *tmp = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = tmp;
    
    [tmp release];
    [view release];
}

- (void)createBackButton {
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDefaultButtonWidth, 44)]; //84
    view.backgroundColor = [UIColor clearColor];
    
    //------------------//
    // Reskinning       //
    //------------------//
    UIButton *button = nil;
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        // AXIS
        button = createButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                       @"f",
                                       [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                       colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                       [UIColor clearColor],
                                       [UIColor clearColor]);
    }
    else {
        // XL
        button = createButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                       @"f",
                                       [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                       kDefaultWhiteColor,
                                       [UIColor clearColor],
                                       colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    
    [button addTarget:self
               action:@selector(performBack:)
     forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:button];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:view];
}

// TODO : Hygiene
- (void)createBackToRootButton {
    
    if(self.navigationItem.leftBarButtonItem != nil)
    {
        self.navigationItem.leftBarButtonItem = nil;
    }
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDefaultButtonWidth, 44)]; //84
    view.backgroundColor = [UIColor clearColor];
    
    //------------------//
    // Reskinning       //
    //------------------//
    UIButton *button = nil;
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        // AXIS
        button = createButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                       @"f",
                                       [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                       colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                       [UIColor clearColor],
                                       [UIColor clearColor]);
    }
    else {
        // XL
        button = createButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                       @"f",
                                       [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                       kDefaultWhiteColor,
                                       [UIColor clearColor],
                                       colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    
    [button addTarget:self
               action:@selector(performBackToRoot:)
     forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:button];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:view];
}

- (void)performBack:(id)sender {
    if(!_isCustomBack)
        [self.navigationController popViewControllerAnimated:YES];
    else
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.navigationController popViewControllerAnimated:YES];
    }
}

- (void)createAccountAndNotificationButton {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    view.backgroundColor = [UIColor clearColor];
    
    UIButton *accountButton = createIconicButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                          @"\u0040", //A
                                                          [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                          kDefaultNavyBlueColor,
                                                          kDefaultWhiteColor,//kDefaultButtonGrayColor
                                                          kDefaultGrayColor); //kDefaultFontLightGrayColor
    
    [accountButton addTarget:self
                      action:@selector(performAccount:)
            forControlEvents:UIControlEventTouchDown];
    
    self.settingBtn = accountButton;
    
    [view addSubview:self.settingBtn];
    
    UIButton *notifButton = createIconicButtonWithFrame(CGRectMake(accountButton.frame.origin.x + accountButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                        @"P",
                                                        [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                        kDefaultWhiteColor,
                                                        [UIColor clearColor],
                                                        kDefaultAquaMarineColor);
    
    // TODO : V1.9
    // Add badge count on Message Icon
    notifButton.tag = NOTIF_BUTTON_TAG;
    
    [notifButton addTarget:self
                    action:@selector(performNotification:)
          forControlEvents:UIControlEventTouchDown];
    self.inboxBtn = notifButton;
    [view addSubview:self.inboxBtn];
    
    // 6 May 2013
    
    MKNumberBadgeView *badgeNotifTmp = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(50 + notifButton.frame.size.width - (kDefaultBadgeWidth/2),
                                                                                        0.0,
                                                                                        kDefaultBadgeWidth,
                                                                                        kDefaultBadgeHeight)];
    badgeNotifTmp.fillColor = [UIColor redColor];
    badgeNotifTmp.hideWhenZero = YES;
    
    
    _badgeNotif = badgeNotifTmp;
    
    [view addSubview:_badgeNotif];
    [badgeNotifTmp release];
    //
    
    UIBarButtonItem *tmp = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = tmp;
    
    [tmp release];
    [view release];
}

- (void)performAccount:(id)sender {
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    if (!sharedData.dropdownMenuCreated) {
        [self setupDropdownMenu];
    }
    
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    //AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[appDelegate.navigationController navigationBar];
    
    if (self.menu.isOpen) {
        //navBar.msisdnLabel.hidden = NO;
        //navBar.deviceLabel.hidden = NO;
        //navBar.bottomWhiteBar.hidden = NO;
        return [self.menu close];
    }
    else {
        //navBar.msisdnLabel.hidden = YES;
        //navBar.deviceLabel.hidden = YES;
        //navBar.bottomWhiteBar.hidden = YES;
    }
    
    [self.menu showFromNavigationController:appDelegate.navigationController];
    
    /*
    if(!navPopover) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 220, 110)]; //84
        view.backgroundColor = [UIColor clearColor];
        
        CGFloat padding = 10.0;
        CGFloat width = view.frame.size.width - (padding*2);
        
        UIButton *button = createButtonWithIconAndArrow(CGRectMake(padding, padding, width, kDefaultButtonHeight),
                                                        @"Q",
                                                        [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                                        kDefaultNavyBlueColor,
                                                        [[Language get:@"change_profile" alter:nil] uppercaseString],
                                                        [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize],
                                                        kDefaultNavyBlueColor,
                                                        @"d",
                                                        [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                                        kDefaultNavyBlueColor,
                                                        kDefaultButtonGrayColor,
                                                        kDefaultFontLightGrayColor);
        [button addTarget:self
                   action:@selector(performUpdateProfile:)
         forControlEvents:UIControlEventTouchDown];
        [view addSubview:button];
        
        UIButton *changePasswordButton = createButtonWithIconAndArrow(CGRectMake(padding, button.frame.origin.y + button.frame.size.height + padding, width, kDefaultButtonHeight),
                                                                      @"U",
                                                                      [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                                                      kDefaultNavyBlueColor,
                                                                      [[Language get:@"change_password" alter:nil] uppercaseString],
                                                                      [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize],
                                                                      kDefaultNavyBlueColor,
                                                                      @"d",
                                                                      [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                                                      kDefaultNavyBlueColor,
                                                                      kDefaultButtonGrayColor,
                                                                      kDefaultFontLightGrayColor);
        [changePasswordButton addTarget:self
                                 action:@selector(performChangePassword:)
                       forControlEvents:UIControlEventTouchDown];
        
        [view addSubview:changePasswordButton];
        
        CGFloat height = changePasswordButton.frame.origin.y + changePasswordButton.frame.size.height + padding;
        
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        if (!appDelegate.isAutoLogin) {
            UIButton *logoutButton = createButtonWithIconAndArrow(CGRectMake(padding, changePasswordButton.frame.origin.y + changePasswordButton.frame.size.height + padding, width, kDefaultButtonHeight),
                                                                  @"F",
                                                                  [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                                                  kDefaultNavyBlueColor,
                                                                  [[Language get:@"logout" alter:nil] uppercaseString],
                                                                  [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize],
                                                                  kDefaultNavyBlueColor,
                                                                  @"d",
                                                                  [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                                                  kDefaultNavyBlueColor,
                                                                  kDefaultButtonGrayColor,
                                                                  kDefaultFontLightGrayColor);
            [logoutButton addTarget:self
                             action:@selector(performLogout:)
                   forControlEvents:UIControlEventTouchDown];
            
            [view addSubview:logoutButton];
            
            height = logoutButton.frame.origin.y + logoutButton.frame.size.height + padding;
        }
        
        CGRect frame = view.frame;
        frame.size.height = height;
        view.frame = frame;
        
        UIViewController *viewCon = [[UIViewController alloc] init];
        viewCon.view = view;
        viewCon.contentSizeForViewInPopover = view.frame.size;
        
        navPopover = [[WEPopoverController alloc] initWithContentViewController:viewCon];
        [navPopover setDelegate:self];
    }
    
    if ([navPopover isPopoverVisible]) {
        [navPopover dismissPopoverAnimated:YES];
        [navPopover setDelegate:nil];
        navPopover = nil;
    }
    else {
        if (barButtonItemType != PROFILE_NOTIF && barButtonItemType != PROFILE_NOTIF_BACK) {
            [navPopover presentPopoverFromRect:CGRectMake(0, 0, 170, 57)
                                        inView:self.navigationController.view
                      permittedArrowDirections:UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown
                                      animated:YES];
        }
        else {
            [navPopover presentPopoverFromRect:CGRectMake(0, 0, 50, 57)
                                        inView:self.navigationController.view
                      permittedArrowDirections:UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown
                                      animated:YES];
        }
    }*/
}

- (void)performUpdateProfile:(id)sender {
    
    if ([navPopover isPopoverVisible]) {
        [navPopover dismissPopoverAnimated:YES];
        [navPopover setDelegate:nil];
        navPopover = nil;
    }
    
    UpdateProfileViewController *updateProfileViewController = [[UpdateProfileViewController alloc] initWithNibName:@"UpdateProfileViewController" bundle:nil];
    [updateProfileViewController createBarButtonItem:BACK];
    [self.navigationController pushViewController:updateProfileViewController animated:YES];
    updateProfileViewController = nil;
    [updateProfileViewController release];
}

- (void)performChangePassword:(id)sender {
    /*
    if ([navPopover isPopoverVisible]) {
        [navPopover dismissPopoverAnimated:YES];
        [navPopover setDelegate:nil];
        navPopover = nil;
    }*/
    
    ChangePasswordViewController *changePasswordViewController = [[ChangePasswordViewController alloc] initWithNibName:@"ChangePasswordViewController" bundle:nil];
    
    [changePasswordViewController createBarButtonItem:BACK];
    [self.navigationController pushViewController:changePasswordViewController animated:YES];
    changePasswordViewController = nil;
    [changePasswordViewController release];
}

- (void)performLogout:(id)sender {
    /*
    if ([navPopover isPopoverVisible]) {
        [navPopover dismissPopoverAnimated:YES];
        [navPopover setDelegate:nil];
        navPopover = nil;
    }*/
    
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = SIGN_OUT_REQ;
    [request signOut];
}

- (void)performNotification:(id)sender {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = NOTIFICATION_REQ;
    [request notification];
}

- (void)createAccountNotificationAndBackButton {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 44)]; //84
    view.backgroundColor = [UIColor clearColor];
    
    UIButton *accountButton = createButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonWidth, kDefaultButtonHeight),
                                                    @"A",
                                                    [UIFont fontWithName:kDefaultFontKSAN size:20.0],
                                                    kDefaultPurpleColor,
                                                    kDefaultButtonGrayColor,
                                                    kDefaultFontLightGrayColor);
    [accountButton addTarget:self
                      action:@selector(performAccount:)
            forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:accountButton];
    
    UIButton *notifButton = createButtonWithFrame(CGRectMake(50, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonWidth, kDefaultButtonHeight),
                                                  @"P",
                                                  [UIFont fontWithName:kDefaultFontKSAN size:20.0],
                                                  kDefaultPurpleColor,
                                                  kDefaultButtonGrayColor,
                                                  kDefaultFontLightGrayColor);
    
    // TODO : V1.9
    // Add badge count on Message Icon
    notifButton.tag = NOTIF_BUTTON_TAG;
    
    [notifButton addTarget:self
                    action:@selector(performNotification:)
          forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:notifButton];
    
    UIButton *backButton = createButtonWithFrame(CGRectMake(100, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonWidth, kDefaultButtonHeight),
                                                 @"f",
                                                 [UIFont fontWithName:kDefaultFontKSAN size:20.0],
                                                 kDefaultPurpleColor,
                                                 kDefaultButtonGrayColor,
                                                 kDefaultFontLightGrayColor);
    
    [backButton addTarget:self
                   action:@selector(performBack:)
         forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:backButton];
    
    UIBarButtonItem *tmp = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = tmp;
    
    [tmp release];
    [view release];
}

// Header Button Level 3 and so on
- (void)createMenuAccountNotificationAndBackButton {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 44)]; //84
    view.backgroundColor = [UIColor clearColor];
    
    UniChar menu = 0x005C;
    NSString *menuString = [NSString stringWithCharacters:&menu length:1];
    UIButton *menuButton = createButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 menuString,
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 kDefaultWhiteColor,
                                                 kDefaultNavyBlueColor,
                                                 kDefaultAquaMarineColor);
    [menuButton addTarget:self
                   action:@selector(performMenuForLevel3:)
         forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:menuButton];
    
    UIButton *backButton = createButtonWithFrame(CGRectMake(menuButton.frame.origin.x + menuButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 @"f",
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 kDefaultNavyBlueColor,
                                                 kDefaultWhiteColor,
                                                 kDefaultWhiteColor);
    
    [backButton addTarget:self
                   action:@selector(performBack:)
         forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:backButton];
    
    UIButton *notifButton = createButtonWithFrame(CGRectMake(backButton.frame.origin.x + backButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                  @"P",
                                                  [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                  kDefaultNavyBlueColor,
                                                  kDefaultWhiteColor,
                                                  kDefaultWhiteColor);
    
    // TODO : V1.9
    // Add badge count on Message Icon
    notifButton.tag = NOTIF_BUTTON_TAG;
    
    [notifButton addTarget:self
                    action:@selector(performNotification:)
          forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:notifButton];
    
    
    
    UIBarButtonItem *tmp = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = tmp;
    
    [tmp release];
    [view release];
}

- (void)performMenu:(id)sender {
    [self.viewDeckController toggleLeftView];
}

- (void)performMenuForLevel3:(id)sender {
    [self.viewDeckController toggleLeftView];
}

- (void)performBackToRoot:(id)sender {
    
    // TODO : Hygiene
    // The backtoroot bool is used for disclaimer view when the user is not logged in.
    if(_isBackToRoot)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.navigationController popToRootViewControllerAnimated:YES];
    }
    
//    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    [appDelegate.navigationController popToRootViewControllerAnimated:YES];
}

- (void)createMenuAccountNotificationSearchAndBackButton {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 44)]; //84
    view.backgroundColor = [UIColor clearColor];
    
    UniChar menu = 0x005C;
    NSString *menuString = [NSString stringWithCharacters:&menu length:1];
    UIButton *menuButton = createButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonWidth, kDefaultButtonHeight),
                                                 menuString,
                                                 [UIFont fontWithName:kDefaultFontKSAN size:20.0],
                                                 kDefaultBaseColor,
                                                 kDefaultPinkColor,
                                                 kDefaultPurpleColor);
    [menuButton addTarget:self
                   action:@selector(performMenu:)
         forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:menuButton];
    
    UIButton *accountButton = createButtonWithFrame(CGRectMake(50, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonWidth, kDefaultButtonHeight),
                                                    @"A",
                                                    [UIFont fontWithName:kDefaultFontKSAN size:20.0],
                                                    kDefaultPurpleColor,
                                                    kDefaultButtonGrayColor,
                                                    kDefaultFontLightGrayColor);
    [accountButton addTarget:self
                      action:@selector(performAccount:)
            forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:accountButton];
    
    UIButton *notifButton = createButtonWithFrame(CGRectMake(100, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonWidth, kDefaultButtonHeight),
                                                  @"P",
                                                  [UIFont fontWithName:kDefaultFontKSAN size:20.0],
                                                  kDefaultPurpleColor,
                                                  kDefaultButtonGrayColor,
                                                  kDefaultFontLightGrayColor);
    
    // TODO : V1.9
    // Add badge count on Message Icon
    notifButton.tag = NOTIF_BUTTON_TAG;
    
    [notifButton addTarget:self
                    action:@selector(performNotification:)
          forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:notifButton];
    
    UIButton *searchButton = createButtonWithFrame(CGRectMake(150, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonWidth, kDefaultButtonHeight),
                                                   @"V",
                                                   [UIFont fontWithName:kDefaultFontKSAN size:20.0],
                                                   kDefaultPurpleColor,
                                                   kDefaultButtonGrayColor,
                                                   kDefaultFontLightGrayColor);
    
    [searchButton addTarget:self
                     action:@selector(performSearch:)
           forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:searchButton];
    
    UIButton *backButton = createButtonWithFrame(CGRectMake(200, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonWidth, kDefaultButtonHeight),
                                                 @"f",
                                                 [UIFont fontWithName:kDefaultFontKSAN size:20.0],
                                                 kDefaultPurpleColor,
                                                 kDefaultButtonGrayColor,
                                                 kDefaultFontLightGrayColor);
    
    [backButton addTarget:self
                   action:@selector(performBackToRoot:)
         forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:backButton];
    
    UIBarButtonItem *tmp = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = tmp;
    
    [tmp release];
    [view release];
}

- (void)performSearch:(id)sender {
}

- (void)createMenuAndBackButtonLevel2 {
    DataManager *sharedData = [DataManager sharedInstance];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    view.backgroundColor = [UIColor clearColor];
    
    UniChar menu = 0x005C;
    NSString *menuString = [NSString stringWithCharacters:&menu length:1];
    UIButton *menuButton = nil;
    //------------------//
    // Reskinning       //
    //------------------//
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        
        // AXIS
        menuButton = createIconicButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 menuString,
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 kDefaultWhiteColor,
                                                 colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                 colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    else {
        
        // XL
        menuButton = createIconicButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 menuString,
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 kDefaultWhiteColor,
                                                 [UIColor clearColor],
                                                 colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    
    [menuButton addTarget:self
                   action:@selector(performMenu:)
         forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:menuButton];
    
    UIButton *backButton = nil;
    //------------------//
    // Reskinning       //
    //------------------//
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        
        // AXIS
        backButton = createIconicButtonWithFrame(CGRectMake(menuButton.frame.origin.x + menuButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 @"f",
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                 [UIColor clearColor],
                                                 [UIColor clearColor]);
    }
    else {
        
        // XL
        backButton = createIconicButtonWithFrame(CGRectMake(menuButton.frame.origin.x + menuButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 @"f",
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 kDefaultWhiteColor,
                                                 [UIColor clearColor],
                                                 colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    
    [backButton addTarget:self
                   action:@selector(performBackToRoot:)
         forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:backButton];
    
    UIBarButtonItem *tmp = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = tmp;
    
    [tmp release];
    [view release];
}

- (void)createMenuAccountNotificationAndBackButtonLevel2 {
    DataManager *sharedData = [DataManager sharedInstance];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 44)]; //84
    view.backgroundColor = [UIColor clearColor];
    
    UniChar menu = 0x005C;
    NSString *menuString = [NSString stringWithCharacters:&menu length:1];
    UIButton *menuButton = nil;
    //------------------//
    // Reskinning       //
    //------------------//
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        
        // AXIS
        
        menuButton = createIconicButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 menuString,
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 kDefaultWhiteColor,
                                                 colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                 colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    else {
        
        // XL
        /*
        menuButton = createIconicButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 menuString,
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                 kDefaultWhiteColor,
                                                 colorWithHexString(sharedData.profileData.themesModel.menuFeature));*/
        
        menuButton = createIconicButtonWithFrame(CGRectMake(0, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 menuString,
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 kDefaultWhiteColor,
                                                 [UIColor clearColor],
                                                 colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    
    [menuButton addTarget:self
                   action:@selector(performMenu:)
         forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:menuButton];
    
    UIButton *backButton = nil;
    //------------------//
    // Reskinning       //
    //------------------//
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        
        // AXIS
        
        backButton = createIconicButtonWithFrame(CGRectMake(menuButton.frame.origin.x + menuButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 @"f",
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                 [UIColor clearColor],
                                                 [UIColor clearColor]);
    }
    else {
        
        // XL
        
        backButton = createIconicButtonWithFrame(CGRectMake(menuButton.frame.origin.x + menuButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                 @"f",
                                                 [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                 kDefaultWhiteColor,
                                                 [UIColor clearColor],
                                                 colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    
    [backButton addTarget:self
                   action:@selector(performBackToRoot:)
         forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:backButton];
    
    //------------------//
    // Reskinning       //
    //------------------//
    UIButton *notifButton = nil;
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        
        // AXIS
        
        notifButton = createIconicButtonWithFrame(CGRectMake(backButton.frame.origin.x + backButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                  @"P",
                                                  [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                  colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                  [UIColor clearColor],
                                                  [UIColor clearColor]);
    }
    else {
        
        // XL
        
        notifButton = createIconicButtonWithFrame(CGRectMake(backButton.frame.origin.x + backButton.frame.size.width + kDefaultComponentPadding, view.frame.size.height/2 - kDefaultButtonHeight/2, kDefaultButtonHeight, kDefaultButtonHeight),
                                                  @"P",
                                                  [UIFont fontWithName:kDefaultFontKSAN size:18.0],
                                                  kDefaultWhiteColor,
                                                  [UIColor clearColor],
                                                  colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    
    // TODO : V1.9
    // Add badge count on Message Icon
    notifButton.tag = NOTIF_BUTTON_TAG;
    
    [notifButton addTarget:self
                    action:@selector(performNotification:)
          forControlEvents:UIControlEventTouchDown];
    
    [view addSubview:notifButton];
    
    UIBarButtonItem *tmp = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = tmp;
    
    [tmp release];
    [view release];
}

- (void)performHotOfferBanner:(NSString*)size {
    _adsRequest = [[AdsRequest alloc] init];
    [_adsRequest hotOfferBanner:size
                       delegate:self
                successSelector:@selector(hotOfferCallback:)
                  errorSelector:@selector(hotOfferErrorCallback:)];
}

- (void)performAds:(NSString*)page withSize:(NSString*)size {
    _adsRequest = [[AdsRequest alloc] init];
    [_adsRequest ads:page
                size:size
            delegate:self
     successSelector:@selector(adsCallback:)
       errorSelector:@selector(adsErrorCallback:)];
}

- (void)downloadAdsImages:(NSArray*)images {
    _adsRequest = [[AdsRequest alloc] init];
    [_adsRequest downloadMultipleImage:images
                              delegate:self
                       successSelector:@selector(imageCallback:)
                         errorSelector:@selector(imageErrorCallback:)];
}

- (void)setupAds {
}

- (void)setupDropdownMenu {
    
    DataManager *sharedData = [DataManager sharedInstance];
    //NSArray *content = [NSArray arrayWithArray:[sharedData.menuData valueForKey:@"0"]];
    //------------------//
    // Restructure Menu //
    //------------------//
    NSArray *content = [NSArray arrayWithArray:sharedData.homeMenu];
    //-------------------------//
    // End of Restructure Menu //
    //-------------------------//
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:content];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    //NSLog(@"sortingResult = %@",sortingResult);
    
    NSArray *sortedMenu = [sortingResult allKeys];
    sortedMenu = [GroupingAndSorting doSortingByGroupId:sortedMenu];
    
    NSArray *objectsForMenu = [sortingResult objectForKey:@"22"];
    NSMutableArray *itemArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [objectsForMenu count]; i++) {
        MenuModel *model = [objectsForMenu objectAtIndex:i];
        //NSLog(@"model.menuName = %@",model.menuName);
        
        UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kDefaultCellHeight)];
        
        NSString *icon = model.icon;
        if ([icon length] <= 0) {
            icon = model.hex;
        }
        
        CGFloat labelLeftPadding = 15.0;
        CGFloat width = 30.0;
        CGFloat height = 30.0;
        
        UILabel *iconLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelLeftPadding, (customView.frame.size.height - height)/2, width, height)];
        iconLabel.textAlignment = UITextAlignmentCenter;
        iconLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:18.0];
        iconLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
        iconLabel.backgroundColor = kDefaultGrayColor;
        iconLabel.text = icon;
        iconLabel.layer.cornerRadius = kDefaultCornerRadius;
        iconLabel.layer.masksToBounds = YES;
        [customView addSubview:iconLabel];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(iconLabel.frame.origin.x + iconLabel.frame.size.width + 8.0,
                                                                        (customView.frame.size.height - height)/2,
                                                                        customView.frame.size.width - (labelLeftPadding) - (width*2) - 8.0,
                                                                        height)];
        titleLabel.textAlignment = UITextAlignmentLeft;
        titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
        titleLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = UILineBreakModeWordWrap;
        titleLabel.text = model.menuName;
        [customView addSubview:titleLabel];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x,
                                                                    customView.frame.origin.x + customView.frame.size.height - 1,
                                                                    customView.frame.size.width - titleLabel.frame.origin.x,
                                                                    1)];
        lineView.backgroundColor = kDefaultSeparatorColor;
        
        if (i != [objectsForMenu count]-1) {
            [customView addSubview:lineView];
        }
        
        if ([model.href isEqualToString:@"#change_password"] ||
            [model.href isEqualToString:@"#about"] ||
            [model.href isEqualToString:@"#logout"]) {
            UILabel *arrowLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x + titleLabel.frame.size.width,
                                                                            (customView.frame.size.height - height)/2,
                                                                            width,
                                                                            height)];
            arrowLabel.textAlignment = UITextAlignmentLeft;
            arrowLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:18.0];
            arrowLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
            arrowLabel.backgroundColor = [UIColor clearColor];
            arrowLabel.text = @"d";
            [customView addSubview:arrowLabel];
        }
        else {
            UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            CGFloat btnHeight = 30.0;
            CGFloat btnWidth = 30.0;
            
            int currentLang = [Language getCurrentLanguage];
            
            // EN
            if (currentLang == 0) {
                UIButton *buttonEN = createButtonWithFrame(CGRectMake(customView.frame.size.width - 5 - btnWidth,
                                                                      (customView.frame.size.height - btnHeight)/2,
                                                                      btnWidth,
                                                                      btnHeight),
                                                           @"EN",
                                                           buttonFont,
                                                           colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                           kDefaultYellowColor,
                                                           kDefaultAquaMarineColor);
                [buttonEN addTarget:self action:@selector(performLangEN) forControlEvents:UIControlEventTouchUpInside];
                [customView addSubview:buttonEN];
                
                UIButton *buttonID = createButtonWithFrame(CGRectMake(buttonEN.frame.origin.x - 5 - btnWidth,
                                                                      (customView.frame.size.height - btnHeight)/2,
                                                                      btnWidth,
                                                                      btnHeight),
                                                           @"ID",
                                                           buttonFont,
                                                           kDefaultWhiteColor,
                                                           colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                           kDefaultAquaMarineColor);
                [buttonID addTarget:self action:@selector(performLangID) forControlEvents:UIControlEventTouchUpInside];
                [customView addSubview:buttonID];
            }
            else {
                UIButton *buttonEN = createButtonWithFrame(CGRectMake(customView.frame.size.width - 5 - btnWidth,
                                                                      (customView.frame.size.height - btnHeight)/2,
                                                                      btnWidth,
                                                                      btnHeight),
                                                           @"EN",
                                                           buttonFont,
                                                           kDefaultWhiteColor,
                                                           colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                           kDefaultAquaMarineColor);
                [buttonEN addTarget:self action:@selector(performLangEN) forControlEvents:UIControlEventTouchUpInside];
                [customView addSubview:buttonEN];
                
                UIButton *buttonID = createButtonWithFrame(CGRectMake(buttonEN.frame.origin.x - 5 - btnWidth,
                                                                      (customView.frame.size.height - btnHeight)/2,
                                                                      btnWidth,
                                                                      btnHeight),
                                                           @"ID",
                                                           buttonFont,
                                                           colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                           kDefaultYellowColor,
                                                           kDefaultAquaMarineColor);
                [buttonID addTarget:self action:@selector(performLangID) forControlEvents:UIControlEventTouchUpInside];
                [customView addSubview:buttonID];
            }
        }
        
        REMenuItem *customViewItem = [[REMenuItem alloc] initWithCustomView:customView action:^(REMenuItem *item) {
            
            //[self performDropdownMenu:customViewItem];
            if ([model.href isEqualToString:@"#change_password"]) {
                [self performChangePassword:nil];
            }
            else if ([model.href isEqualToString:@"#about"]) {
                [self performAbout:model];
            }
            else if ([model.href isEqualToString:@"#logout"]) {
                [self performLogout:nil];
            }
            else {
                
            }
        }];
        
        customViewItem.tag = i;
        [itemArray addObject:customViewItem];
    }
    
    
    
    // You can also assign a custom view for any particular item
    // Uncomment the code below and add `customViewItem` to `initWithItems` array, for example:
    // self.menu = [[REMenu alloc] initWithItems:@[homeItem, exploreItem, activityItem, profileItem, customViewItem]]
    //
    
    NSArray *finalItem = [NSArray arrayWithArray:itemArray];
    self.menu = [[REMenu alloc] initWithItems:finalItem];
    [itemArray release];
    
    // Style Setting
    self.menu.backgroundColor = kDefaultWhiteSmokeColor;
    //self.menu.textAlignment = UITextAlignmentLeft;
    self.menu.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    self.menu.textColor = kDefaultBlackColor;
    //self.menu.separatorColor = [UIColor lightGrayColor];
    self.menu.separatorColor = [UIColor clearColor];
    self.menu.separatorHeight = 1.0;
    self.menu.borderColor = kDefaultWhiteColor;
    
    // Background view
    //
    //self.menu.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    //self.menu.backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //self.menu.backgroundView.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.600];
    
    //self.menu.imageAlignment = REMenuImageAlignmentRight;
    //self.menu.closeOnSelection = NO;
    //self.menu.appearsBehindNavigationBar = NO; // Affects only iOS 7
    if (!REUIKitIsFlatMode()) {
        self.menu.cornerRadius = 4;
        self.menu.shadowRadius = 4;
        self.menu.shadowColor = [UIColor blackColor];
        self.menu.shadowOffset = CGSizeMake(0, 1);
        self.menu.shadowOpacity = 1;
    }
    
    // Blurred background in iOS 7
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        self.menu.liveBlur = YES;
        self.menu.liveBlurBackgroundStyle = REMenuLiveBackgroundStyleLight;
        //self.menu.liveBlurTintColor = [UIColor redColor];
    }
    
    self.menu.imageOffset = CGSizeMake(5, -1);
    self.menu.waitUntilAnimationIsComplete = NO;
    self.menu.badgeLabelConfigurationBlock = ^(UILabel *badgeLabel, REMenuItem *item) {
        badgeLabel.backgroundColor = [UIColor colorWithRed:0 green:179/255.0 blue:134/255.0 alpha:1];
        badgeLabel.layer.borderColor = [UIColor colorWithRed:0.000 green:0.648 blue:0.507 alpha:1.000].CGColor;
    };
    
    sharedData.dropdownMenuCreated = YES;
}

- (void)performAbout:(MenuModel*)aboutMenuModel {
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
    ContactUsModel *model = [[ContactUsModel alloc] init];
    
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        model.label = [[Language get:@"about_title_1" alter:nil] uppercaseString];
        model.value = [Language get:@"about_desc_1" alter:nil];
        [contentTmp addObject:model];
        [model release];
        model = nil;
        
        model = [[ContactUsModel alloc] init];
        model.label = [[Language get:@"about_title_2" alter:nil] uppercaseString];
        model.value = [Language get:@"about_desc_2" alter:nil];
        [contentTmp addObject:model];
        [model release];
        model = nil;
    }
    else {
        model.label = [[Language get:@"about_title_1_axis" alter:nil] uppercaseString];
        model.value = [Language get:@"about_desc_1_axis" alter:nil];
        [contentTmp addObject:model];
        [model release];
        model = nil;
    }
    
    NSArray *contentAbout = [NSArray arrayWithArray:contentTmp];
    [contentTmp release];
    
    ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
    [contactUsViewController createBarButtonItem:BACK];
    contactUsViewController.content = contentAbout;
    contactUsViewController.accordionType = ABOUT_ACC;
    contactUsViewController.menuModel = aboutMenuModel;
    
    [self.navigationController pushViewController:contactUsViewController animated:YES];
    contactUsViewController = nil;
    [contactUsViewController release];
}

- (void)performDropdownMenu:(REMenuItem*)menuItem {
    DataManager *sharedData = [DataManager sharedInstance];
    NSArray *content = [NSArray arrayWithArray:[sharedData.menuData valueForKey:@"0"]];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:content];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    NSArray *sortedMenu = [sortingResult allKeys];
    sortedMenu = [GroupingAndSorting doSortingByGroupId:sortedMenu];
    
    NSArray *objectsForMenu = [sortingResult objectForKey:@"21"];
    MenuModel *model = [objectsForMenu objectAtIndex:menuItem.tag];
    
    if ([model.href isEqualToString:@"#change_password"]) {
        [self performChangePassword:nil];
    }
    else if ([model.href isEqualToString:@"#about"]) {
        
    }
    else if ([model.href isEqualToString:@"#logout"]) {
        [self performLogout:nil];
    }
    else {
        
    }
}

- (void)performLangEN {
    
    NSLog(@"Change to EN");
    
    int currentLang = [Language getCurrentLanguage];
    
    // EN
    if (currentLang == 0) {
        NSLog(@"Do Nothing");
    }
    else {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = CHANGE_LANGUAGE_REQ;
        [request changeLanguage:@"en"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.dropdownMenuCreated = NO;
    }
    
    if (self.menu.isOpen) {
        return [self.menu close];
    }
}

- (void)performLangID {
    
    NSLog(@"Change to ID");
    
    int currentLang = [Language getCurrentLanguage];
    
    // ID
    if (currentLang == 1) {
        NSLog(@"Do Nothing");
    }
    else {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = CHANGE_LANGUAGE_REQ;
        [request changeLanguage:@"id"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.dropdownMenuCreated = NO;
    }
    
    if (self.menu.isOpen) {
        return [self.menu close];
    }
}
/*
- (void)requestMenu {
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = MENU_REQ;
    [request menu];
}*/

#pragma mark - Callback

- (void)hotOfferCallback:(NSDictionary*)result {
    
    [_adsRequest release];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSArray *adsData = [result valueForKey:DATA_KEY];
        self.adsContent = [NSArray arrayWithArray:adsData];
        
        NSMutableArray *imagesTmp = [[NSMutableArray alloc] init];
        for (int i=0; i<[self.adsContent count]; i++) {
            MenuModel *menuModel = [self.adsContent objectAtIndex:i];
            [imagesTmp addObject:menuModel.banner];
        }
        
        // hide banner when there's no ads
        if([imagesTmp count] ==0)
        {
            [self setupAds];
        }
        else
        {
            NSArray *adsImageURL = [imagesTmp copy];
            [self downloadAdsImages:adsImageURL];
        }
//        NSArray *adsImageURL = [imagesTmp copy];
//        [self downloadAdsImages:adsImageURL];
        [imagesTmp release];
    }
    else {
        [self setupAds];
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)hotOfferErrorCallback:(NSDictionary*)result {
    [self setupAds];
    [_adsRequest release];
}

- (void)adsCallback:(NSDictionary*)result {
    //NSLog(@"Ads Callback");
    [_adsRequest release];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        /*
         NSNumber *repeatResult = [result valueForKey:REPEAT_KEY];
         BOOL repeat = [repeatResult boolValue];
         
         NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
         int type = [typeNumber intValue];*/
        
        //DataManager *sharedData = [DataManager sharedInstance];
        NSArray *adsData = [result valueForKey:DATA_KEY];
        self.adsContent = [NSArray arrayWithArray:adsData];
        
        NSMutableArray *imagesTmp = [[NSMutableArray alloc] init];
        for (int i=0; i<[self.adsContent count]; i++) {
            //NSLog(@"Ads Model");
            AdsModel *adsModel = [self.adsContent objectAtIndex:i];
            //NSLog(@"adsModel.imageURL %i = %@",i,adsModel.imageURL);
            
            /*
             BOOL validURL = validateUrl(adsModel.imageURL);
             if (validURL) {
             [imagesTmp addObject:adsModel.imageURL];
             }*/
            
            [imagesTmp addObject:adsModel.imageURL];
            
            //NSLog(@"valid URL = %i",validURL);
        }
        
        //NSArray *adsImageURL = [NSArray arrayWithArray:imagesTmp];
        NSArray *adsImageURL = [imagesTmp copy];
        //NSLog(@"adsImageURL count = %i",[adsImageURL count]);
        
        [self downloadAdsImages:adsImageURL];
        
        [imagesTmp release];
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)adsErrorCallback:(NSDictionary*)result {
    //NSLog(@"ERROR Ads Callback");
    [_adsRequest release];
}

- (void)imageCallback:(NSDictionary*)result
{
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success)
    {
        adsImageCounter++;
//        NSLog(@"adsImageCounter = %i",adsImageCounter);
        
        NSString *imageTag = [result objectForKey:@"imagetag"];
        //NSLog(@"result Dict = %@",result);
        //NSLog(@"imageTag = %@",imageTag);
        
        //DataManager *sharedData = [DataManager sharedInstance];
        
        //AdsModel *adsModel = [self.adsContent objectAtIndex:[imageTag intValue]];
        MenuModel *adsModel = [self.adsContent objectAtIndex:[imageTag intValue]];
        adsModel.image = [result objectForKey:@"imagekey"];
        
        if (adsImageCounter == [self.adsContent count]) {
            [_adsRequest release];
            [self setupAds];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)imageErrorCallback:(NSDictionary*)result {
    //NSLog(@"ERROR Download Image Callback");
    adsImageCounter++;
    if (adsImageCounter == [self.adsContent count]) {
        [_adsRequest release];
        [self setupAds];
    }
}

#pragma mark - PopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController {
    //NSLog(@"Did dismiss");
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    //NSLog(@"Should dismiss");
    return YES;
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            [notificationViewController createBarButtonItem:BACK];
            [self.navigationController pushViewController:notificationViewController animated:YES];
            [notificationViewController release];
            notificationViewController = nil;
        }
        
        if (type == SIGN_OUT_REQ) {
            
            resetAllData();
            
            // Reset Image Ads Counter
            adsImageCounter = 0;
            
            AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            appDelegate.loggedIn = NO;
            
            // TODO : V1.9
            // set update device token to be YES again.
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.shouldUpdateDeviceToken = YES;
            // TODO : Hygiene
            // Reset this autologin variable on the login view instead
//            appDelegate.isAutoLogin = NO;
            
            [appDelegate showLoginView];
        }
        
        if (type == CHANGE_LANGUAGE_REQ) {
            //NSLog(@"--> Change Lang Success");
            //[self requestMenu];
        }
        
        if (type == MENU_REQ) {
            //NSLog(@"--> Refresh Table View");
//            AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
//            UINavigationController *navController = appDelegate.navigationController;
//            NSArray *viewControllers = navController.viewControllers;
//            UIViewController *vc = [viewControllers objectAtIndex:0];
//            if ([vc isKindOfClass:[HomeViewController class]]) {
//                HomeViewController *homeVC = (HomeViewController*)vc;
//                [homeVC.theTableView reloadData];
//            }
        }
    }
    else {
        
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

// TODO : V1.9
#pragma mark - Badge Count For Inbox Icon
-(void)createBadgeOnMessageIcon
{
    @try
    {
        UIBarButtonItem *barItem = self.navigationItem.leftBarButtonItem;
        UIView *barItemView = barItem.customView;
        DataManager *sharedData = [DataManager sharedInstance];
        NSLog(@"unread local = %li",(long)_unreadMessageCount);
        NSLog(@"unread share = %i",sharedData.unreadMsg);
        //FIXME: uncomment this
        //if(_unreadMessageCount != sharedData.unreadMsg)
        //{
            _unreadMessageCount = sharedData.unreadMsg;
            for (UIView *subView in barItemView.subviews) {
                if(subView.tag == BADGE_ICON_TAG)
                {
                    [subView removeFromSuperview];
                    break;
                }
            }
            
            if(_unreadMessageCount > 0)
            {
                CustomBadge *badge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%ld",(long)_unreadMessageCount]];
                badge.tag = BADGE_ICON_TAG;
                badge.badgeScaleFactor = 0.7;
                UIButton *btnMsg = (UIButton *)[barItem.customView viewWithTag:NOTIF_BUTTON_TAG];
                if(btnMsg)
                {
                    CGRect btnFrame = btnMsg.frame;
                    [badge setFrame:CGRectMake(CGRectGetMaxX(btnFrame)- (CGRectGetWidth(btnFrame)/2), 2, 20, 20)];
                    [barItemView addSubview:badge];
                }
            }
        //}
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

-(void)requestUnreadNotification
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    {
        if(appDelegate.loggedIn)
        {
            AXISnetRequest *request = [AXISnetRequest sharedInstance];
            request.delegate = self;
//            request.requestType = UNREAD_NOTIFICATION_REQ;
            [request getUnreadMessageNotificationWithDelegate:self andCallBack:@selector(requestUnreadNotificationDone)];
        }
    }
}

-(void)requestUnreadNotificationDone
{
    [self createBadgeOnMessageIcon];
}

-(void)showHudToView:(UIView *)view withText:(NSString *)strText
{
    [self.hud hide:YES];
    self.hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    self.hud.labelText = [Language get:strText alter:nil];
}

@end
