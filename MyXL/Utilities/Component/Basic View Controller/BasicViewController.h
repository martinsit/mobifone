//
//  BasicViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/7/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "Language.h"
#import "WEPopoverController.h"
#import "AXISnetRequest.h"
#import "MKNumberBadgeView.h"

#import "REMenu.h"
#import "GAI.h"

typedef enum {
    BACK = 0,
    PROFILE_NOTIF,
    PROFILE_NOTIF_BACK,
    MENU_PROFILE_NOTIF_BACK,
    MENU_PROFILE_NOTIF_BACK_LEVEL2,
    MENU_PROFILE_NOTIF_SEARCH_BACK,
    BACK_NOTIF,
    MENU_BACK_LEVEL2,
    // TODO : NEPTUNE
    MENU_NOTIF
}BarButtonItemType;

@interface BasicViewController : GAITrackedViewController <PopoverControllerDelegate, AXISnetRequestDelegate> {
    MBProgressHUD *hud;
    NSString *_headerTitleMaster;
    NSString *_headerIconMaster;
    BarButtonItemType barButtonItemType;
    WEPopoverController *navPopover;
    MKNumberBadgeView *_badgeNotif;
    
    NSArray *_adsContent;
    int adsImageCounter;
    
    UIButton *settingBtn;
    UIButton *inboxBtn;
}

@property (strong, nonatomic) MBProgressHUD *hud;
@property (nonatomic, retain) NSString *headerTitleMaster;
@property (nonatomic, retain) NSString *headerIconMaster;
@property (readwrite) BarButtonItemType barButtonItemType;
@property (strong, nonatomic) MKNumberBadgeView *badgeNotif;

@property (nonatomic, retain) NSArray *adsContent;

@property (strong, readonly, nonatomic) REMenu *menu;

@property (nonatomic, retain) UIButton *settingBtn;
@property (nonatomic, retain) UIButton *inboxBtn;

//TODO : Hygiene
// for paket suka view controller
@property (nonatomic,readwrite) BOOL isBackToRoot;

// for push notif
@property (nonatomic, readwrite) BOOL isCustomBack;

//@property (readwrite) BOOL dropdownMenuCreated;

- (void)createBarButtonItem:(BarButtonItemType)type;
- (void)performAds:(NSString*)page withSize:(NSString*)size;
- (void)performHotOfferBanner:(NSString*)size;
- (void)setupDropdownMenu;

- (void)activePage:(NSString*)pageName;

- (void)downloadAdsImages:(NSArray*)images;

// TODO : V1.9
-(void)requestUnreadNotification;
-(void)showHudToView:(UIView *)view withText:(NSString *)strText;

-(void)createBadgeOnMessageIcon;

@end
