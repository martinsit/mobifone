//
//  TransparentToolbar.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/17/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransparentToolbar : UIToolbar

@end
