//
//  TransparentToolbar.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/17/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "TransparentToolbar.h"

@implementation TransparentToolbar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self applyTranslucentBackground];
    }
    return self;
}

- (id)init
{
    self = [super init];
    [self applyTranslucentBackground];
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)applyTranslucentBackground
{
    self.backgroundColor = [UIColor clearColor];
    self.opaque = NO;
    self.translucent = YES;
}

@end
