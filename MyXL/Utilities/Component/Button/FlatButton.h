//
//  FlatButton.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlatButton : UIButton {
    NSString *iconString;
    NSString *titleString;
}

@property (nonatomic, retain) NSString *iconString;
@property (nonatomic, retain) NSString *titleString;
@property (nonatomic, retain) UILabel *iconLbl;
@property (nonatomic, retain) UILabel *titleLbl;

@property (strong, nonatomic) UIColor *buttonBackgroundColor;
@property (strong, nonatomic) UIColor *buttonSelectedColor;
@property (strong, nonatomic) NSString *buttonTitle;
@property (strong, nonatomic) UIFont *buttonTitleFont;
@property (strong, nonatomic) UIColor *buttonTitleColor;

- (id)initWithFrame:(CGRect)frame
               icon:(NSString*)icon
           iconFont:(UIFont*)iconFont
          iconColor:(UIColor*)iconColor
              title:(NSString*)title
          titleFont:(UIFont*)titleFont
         titleColor:(UIColor*)titleColor
            bgColor:(UIColor*)bgColor
      selectedColor:(UIColor*)selectedColor;

- (id)initWithFrame:(CGRect)frame
              title:(NSString*)title
          titleFont:(UIFont*)titleFont
         titleColor:(UIColor*)titleColor
            bgColor:(UIColor*)bgColor
      selectedColor:(UIColor*)selectedColor;

- (void)setIconString:(NSString *)icon;
- (void)setTitleString:(NSString *)title;

- (void)refreshButtonAppearance;

@end
