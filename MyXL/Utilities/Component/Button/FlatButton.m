//
//  FlatButton.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "FlatButton.h"
#import "AXISnetCommon.h"
#import "Constant.h"

@interface FlatButton ()

@property (strong, nonatomic) UILabel *buttonTitleLabel;

- (void)setup;


@end

@implementation FlatButton

@synthesize iconString;
@synthesize titleString;
@synthesize iconLbl = _iconLbl;
@synthesize titleLbl = _titleLbl;

@synthesize buttonBackgroundColor;
@synthesize buttonSelectedColor;
@synthesize buttonTitle;
@synthesize buttonTitleFont;
@synthesize buttonTitleColor;
@synthesize buttonTitleLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
               icon:(NSString*)icon
           iconFont:(UIFont*)iconFont
          iconColor:(UIColor*)iconColor
              title:(NSString*)title
          titleFont:(UIFont*)titleFont
         titleColor:(UIColor*)titleColor
            bgColor:(UIColor*)bgColor
      selectedColor:(UIColor*)selectedColor {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        //self = [UIButton buttonWithType:UIButtonTypeCustom];
        self.frame = frame;
        
        [self setBackgroundImage:imageFromColor(bgColor)
                        forState:UIControlStateNormal];
        [self setBackgroundImage:imageFromColor(selectedColor)
                        forState:UIControlStateHighlighted];
        
        // TODO : NEPTUNE
//        CGFloat labelLeftPadding = 15.0;
        CGFloat labelLeftPadding = 10.0;
        CGFloat iconWidth = 30.0;
        CGFloat iconHeight = 30.0;
        CGRect labelFrame;
        
        // Icon
        labelFrame = CGRectMake(labelLeftPadding, (frame.size.height - iconHeight)/2, iconWidth, iconHeight);
        UILabel *iconLabelTmp = [[UILabel alloc] initWithFrame:labelFrame];
        iconLabelTmp.textAlignment = UITextAlignmentCenter;
        iconLabelTmp.font = iconFont;
        iconLabelTmp.textColor = iconColor;
        iconLabelTmp.text = icon;
        iconLabelTmp.backgroundColor = kDefaultYellowColor;
        
        iconLabelTmp.layer.cornerRadius = kDefaultCornerRadius;
        iconLabelTmp.layer.masksToBounds = YES;
        
        _iconLbl = iconLabelTmp;
        [self addSubview:_iconLbl];
        
        // Title
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            labelFrame = CGRectMake(iconLabelTmp.frame.origin.x + iconLabelTmp.frame.size.width + 8.0,
                                    (frame.size.height - iconHeight)/2,
                                    frame.size.width - (iconLabelTmp.frame.origin.x + iconLabelTmp.frame.size.width + 8.0),
                                    iconHeight);
        }
        else {
            labelFrame = CGRectMake(iconLabelTmp.frame.origin.x + iconLabelTmp.frame.size.width + 8.0,
                                    ((frame.size.height - iconHeight)/2) + 4.0, //3.0
                                    frame.size.width - (iconLabelTmp.frame.origin.x + iconLabelTmp.frame.size.width + 8.0),
                                    iconHeight);
        }
        
        UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:labelFrame];
        titleLabelTmp.textAlignment = UITextAlignmentLeft;
        titleLabelTmp.font = titleFont;
        titleLabelTmp.textColor = titleColor;
        titleLabelTmp.text = title;
        titleLabelTmp.backgroundColor = [UIColor clearColor];
        titleLabelTmp.lineBreakMode = UILineBreakModeWordWrap;
        titleLabelTmp.numberOfLines = 0;
        titleLabelTmp.tag = 10;
        _titleLbl = titleLabelTmp;
        [self addSubview:_titleLbl];
        
        [titleLabelTmp release];
        [iconLabelTmp release];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
              title:(NSString*)title
          titleFont:(UIFont*)titleFont
         titleColor:(UIColor*)titleColor
            bgColor:(UIColor*)bgColor
      selectedColor:(UIColor*)selectedColor {
    self = [super initWithFrame:frame];
    if (self) {
        //self = [UIButton buttonWithType:UIButtonTypeCustom];
        self.frame = frame;
        
        [self setBackgroundImage:imageFromColor(bgColor)
                        forState:UIControlStateNormal];
        [self setBackgroundImage:imageFromColor(selectedColor)
                        forState:UIControlStateHighlighted];
        
        CGFloat labelLeftPadding = 15.0;
        CGFloat height = 30.0;
        CGRect labelFrame;
        
        // Title
        labelFrame = CGRectMake(labelLeftPadding,
                                (frame.size.height - height)/2,
                                frame.size.width - (labelLeftPadding*2),
                                height);
        UILabel *titleLabelTmp = [[UILabel alloc] initWithFrame:labelFrame];
        titleLabelTmp.textAlignment = UITextAlignmentLeft;
        titleLabelTmp.font = titleFont;
        titleLabelTmp.textColor = titleColor;
        titleLabelTmp.text = title;
        titleLabelTmp.backgroundColor = [UIColor clearColor];
        titleLabelTmp.tag = 10;
        titleLabelTmp.numberOfLines = 0;
        titleLabelTmp.lineBreakMode = UILineBreakModeWordWrap;
        _titleLbl = titleLabelTmp;
        [self addSubview:_titleLbl];
        
        [titleLabelTmp release];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [iconString release];
    iconString = nil;
    
    [titleString release];
    titleString = nil;
    
    [_titleLbl release];
    _titleLbl = nil;
    
    [_iconLbl release];
    _iconLbl = nil;
    
    [super dealloc];
}

- (void)setIconString:(NSString *)icon {
    [iconString release];
    iconString = [icon retain];
    _iconLbl.text = iconString;
}

- (void)setTitleString:(NSString *)title {
    [titleString release];
    titleString = [title retain];
    _titleLbl.text = [titleString uppercaseString];
}

#pragma mark - Utilities

- (void)setup {
    self.buttonBackgroundColor = kDefaultAquaMarineColor;
    self.buttonSelectedColor = kDefaultBlueColor;
    self.buttonTitle = @"Default";
    self.buttonTitleFont = [UIFont systemFontOfSize:14.0];
    self.buttonTitleColor = kDefaultWhiteColor;
}

- (void)refreshButtonAppearance {
    if (buttonTitleLabel == nil) {
        [self setBackgroundImage:imageFromColor(buttonBackgroundColor)
                        forState:UIControlStateNormal];
        [self setBackgroundImage:imageFromColor(buttonSelectedColor)
                        forState:UIControlStateHighlighted];
        
        self.layer.cornerRadius = kDefaultCornerRadius;
        self.layer.masksToBounds = YES;
        
        // Title
        CGRect labelFrame;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            labelFrame = CGRectMake(0.0, 0.0, self.frame.size.width, self.frame.size.height);
        }
        else {
            labelFrame = CGRectMake(0.0, 2.0, self.frame.size.width, self.frame.size.height);
        }
        
        buttonTitleLabel = [[UILabel alloc] initWithFrame:labelFrame];
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0) {
            buttonTitleLabel.textAlignment = NSTextAlignmentCenter;
            buttonTitleLabel.baselineAdjustment = NSTextAlignmentCenter;
        }
        else {
            buttonTitleLabel.textAlignment = UITextAlignmentCenter;
            buttonTitleLabel.baselineAdjustment = UITextAlignmentCenter;
        }
        buttonTitleLabel.numberOfLines = 0;
        buttonTitleLabel.lineBreakMode = UILineBreakModeWordWrap;
        buttonTitleLabel.font = buttonTitleFont;
        buttonTitleLabel.textColor = buttonTitleColor;
        buttonTitleLabel.text = buttonTitle;
        buttonTitleLabel.backgroundColor = [UIColor clearColor];
        buttonTitleLabel.tag = 99;
        
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
        [self addSubview:buttonTitleLabel];
    }
}

#pragma mark - Setters

- (void)setButtonBackgroundColor:(UIColor *)color
{
    //self.backgroundColor = color;
    buttonBackgroundColor = color;
    //[self refreshButtonAppearance];
}

- (void)setButtonSelectedColor:(UIColor *)color
{
    buttonSelectedColor = color;
    //[self refreshButtonAppearance];
}

- (void)setButtonTitle:(NSString *)btnTitle
{
    buttonTitle = btnTitle;
    //[self refreshButtonAppearance];
}

- (void)setButtonTitleFont:(UIFont *)btnTitleFont
{
    buttonTitleFont = btnTitleFont;
    //[self refreshButtonAppearance];
}

- (void)setButtonTitleColor:(UIColor *)color
{
    buttonTitleColor = color;
    //[self refreshButtonAppearance];
}

@end
