//
//  CustomButton.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/16/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CustomButton.h"
#import <QuartzCore/QuartzCore.h>
#import "Constant.h"

@implementation CustomButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    [self setBackgroundColor:kDefaultPinkColor];
}

- (id)initWithFrame:(CGRect)frame
          withLabel:(NSString *)title
           withFont:(UIFont *)titleFont
     withLabelColor:(UIColor *)textColor
   withDefaultColor:(UIColor *)defaultColor
  withSelectedColor:(UIColor *)selectedColor {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        /*
        UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = frame;
        [button setBackgroundColor:defaultColor];
        button.layer.cornerRadius = kDefaultCornerRadius;
        [button setTitle:title forState:UIControlStateNormal];
        button.titleLabel.font = titleFont;
        button.titleLabel.textColor = textColor;
        
        [self addSubview:button];*/
        
        self = [UIButton buttonWithType:UIButtonTypeCustom];
        self.frame = frame;
        [self setBackgroundColor:defaultColor];
        self.layer.cornerRadius = kDefaultCornerRadius;
        [self setTitle:title forState:UIControlStateNormal];
        self.titleLabel.font = titleFont;
        self.titleLabel.textColor = textColor;
        
        //[self sizeToFit];
        
        return self;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
