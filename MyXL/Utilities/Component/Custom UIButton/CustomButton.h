//
//  CustomButton.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/16/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomButton : UIButton

- (id)initWithFrame:(CGRect)frame
          withLabel:(NSString *)title
           withFont:(UIFont *)titleFont
     withLabelColor:(UIColor *)textColor
   withDefaultColor:(UIColor *)defaultColor
  withSelectedColor:(UIColor *)selectedColor;


@end
