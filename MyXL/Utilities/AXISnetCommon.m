//
//  AXISnetCommon.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/12/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "AXISnetCommon.h"
#import <QuartzCore/QuartzCore.h>
#import "Constant.h"
#import "DataManager.h"
#import "AXISnetNavigationBar.h"
#import "EncryptDecrypt.h"
#include <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import "ASIHTTPRequest.h"

@implementation AXISnetCommon

void addLeftBottomRoundedRect(CGContextRef context, CGRect rect, CGFloat cornerRadius, CGColorRef rectColor) {
    
    CGContextSetFillColorWithColor(context, rectColor);
    
    CGRect rrect = rect;
    CGFloat radius = cornerRadius;
    // In order to create the 4 arcs correctly, we need to know the min, mid and max positions
    // on the x and y lengths of the given rectangle.
    CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
    
    CGFloat miny = CGRectGetMinY(rrect),
    midy = CGRectGetMidY(rrect),
    maxy = CGRectGetMaxY(rrect);
    
    // Start at 1
    CGContextMoveToPoint(context, minx, miny);
    
    CGContextAddLineToPoint(context,midx, miny);
    CGContextAddLineToPoint(context,midx, midy);
    
    CGContextAddArcToPoint(context, midx, maxy, maxx, maxy, radius);
    
    CGContextAddLineToPoint(context,minx, maxy);
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathFill);
}

void addRightBottomRoundedRect(CGContextRef context, CGRect rect, CGFloat cornerRadius, CGColorRef rectColor) {
    CGContextSetFillColorWithColor(context, rectColor);
    
    CGRect rrect = rect;
    CGFloat radius = cornerRadius;
    // In order to create the 4 arcs correctly, we need to know the min, mid and max positions
    // on the x and y lengths of the given rectangle.
    CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
    
    CGFloat miny = CGRectGetMinY(rrect),
    midy = CGRectGetMidY(rrect),
    maxy = CGRectGetMaxY(rrect);
    
    // Start at 1
    CGContextMoveToPoint(context, midx, miny);
    
    CGContextAddLineToPoint(context,maxx, miny);
    CGContextAddLineToPoint(context,maxx, maxy);
    CGContextAddLineToPoint(context,minx, maxy);
    
    //CGContextAddLineToPoint(context,minx, midy);
    //CGContextAddLineToPoint(context,midx, midy);
    
    CGContextAddArcToPoint(context, midx, maxy, midx, midy, radius);
    
    //CGContextAddLineToPoint(context,midx, miny);
    
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathFill);
}

void addRoundedRect(CGContextRef context, CGRect rect, CGFloat cornerRadius, CGColorRef rectColor) {
    CGContextSetFillColorWithColor(context, rectColor);
    
    CGRect rrect = rect;
    
    CGFloat radius = cornerRadius;
    CGFloat width = CGRectGetWidth(rrect);
    CGFloat height = CGRectGetHeight(rrect);
    
    // Make sure corner radius isn't larger than half the shorter side
    if (radius > width/2.0)
        radius = width/2.0;
    if (radius > height/2.0)
        radius = height/2.0;
    
    CGFloat minx = CGRectGetMinX(rrect);
    CGFloat midx = CGRectGetMidX(rrect);
    CGFloat maxx = CGRectGetMaxX(rrect);
    CGFloat miny = CGRectGetMinY(rrect);
    CGFloat midy = CGRectGetMidY(rrect);
    CGFloat maxy = CGRectGetMaxY(rrect);
    CGContextMoveToPoint(context, minx, midy);
    CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
    CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathFill);
}

void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, CGColorRef color) {
    CGContextSaveGState(context);
    CGContextSetLineCap(context, kCGLineCapSquare);
    CGContextSetStrokeColorWithColor(context, color);
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, startPoint.x + 0.5, startPoint.y + 0.5);
    CGContextAddLineToPoint(context, endPoint.x + 0.5, endPoint.y + 0.5);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
}

CGRect rectFor1PxStroke(CGRect rect) {
    return CGRectMake(rect.origin.x + 0.5, rect.origin.y + 0.5,
                      rect.size.width - 1, rect.size.height - 1);
}

void bottomRoundedRect(CGContextRef context, CGRect rect, CGFloat cornerRadius, CGColorRef rectColor, CGColorRef strokeColor, CGFloat strokeWidth) {
    
    // Drawing stroke color
    
    //CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0);
    //CGContextSetRGBFillColor(context, 1.0, 0, 0, 1.0);
    
    CGContextSetStrokeColorWithColor(context, strokeColor);
    CGContextSetFillColorWithColor(context, rectColor);
    
    // Draw them with a 1.0 stroke width so they are a bit more visible.
    CGContextSetLineWidth(context, strokeWidth);
    
    //--
//    CGRect tmpRect = self.bounds;
//    CGFloat leftPadding = 20.0;
//    
//    CGRect backgroundRect = CGRectMake(leftPadding + 1, tmpRect.origin.y, tmpRect.size.width - (leftPadding*2) - (1*2), tmpRect.size.height/2);
    //--
    
    CGRect rrect = rect;
    CGFloat radius = cornerRadius;
    // In order to create the 4 arcs correctly, we need to know the min, mid and max positions
    // on the x and y lengths of the given rectangle.
    CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
    
    CGFloat miny = CGRectGetMinY(rrect),
    midy = CGRectGetMidY(rrect),
    maxy = CGRectGetMaxY(rrect);
    
    // Start at 1
    CGContextMoveToPoint(context, minx, midy);
    // Add an arc through 2 to 3
    CGContextAddArcToPoint(context, minx, maxy, midx, maxy, radius);
    CGContextAddArcToPoint(context, maxx, maxy, maxx, midy, radius);
    CGContextAddLineToPoint(context,maxx, miny -1);
    CGContextAddLineToPoint(context,minx, miny -1);
    CGContextClosePath(context);
    // Fill & stroke the path
    CGContextDrawPath(context, kCGPathFillStroke);
}

void addBackground(CGContextRef context, CGRect rect, CGColorRef rectColor) {
    CGContextSetFillColorWithColor(context, rectColor);
    CGContextFillRect(context, rect);
}

UIButton *createIconicButtonWithFrame(CGRect frame,
                                      NSString *title,
                                      UIFont *titleFont,
                                      UIColor *textColor,
                                      UIColor *defaultColor,
                                      UIColor *selectedColor) {
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    
    [button setBackgroundImage:imageFromColor(defaultColor)
                      forState:UIControlStateNormal];
    [button setBackgroundImage:imageFromColor(selectedColor)
                      forState:UIControlStateHighlighted];
    
    button.layer.cornerRadius = kDefaultCornerRadius;
    button.layer.masksToBounds = YES;
    
    // Title
    CGRect labelFrame;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        labelFrame = CGRectMake(0.0, 0.0, button.frame.size.width, button.frame.size.height);
    }
    else {
        labelFrame = CGRectMake(0.0, 1.0, button.frame.size.width, button.frame.size.height);
    }
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0) {
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.baselineAdjustment = NSTextAlignmentCenter;
    }
    else {
        titleLabel.textAlignment = UITextAlignmentCenter;
        titleLabel.baselineAdjustment = UITextAlignmentCenter;
    }
    titleLabel.numberOfLines = 0;
    titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    titleLabel.font = titleFont;
    titleLabel.textColor = textColor;
    titleLabel.text = title;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.tag = 99;
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [button addSubview:titleLabel];
    
    return button;
}

UIButton *createButtonWithFrame(CGRect frame, NSString *title, UIFont *titleFont, UIColor *textColor, UIColor *defaultColor, UIColor *selectedColor) {
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    
    //[button setBackgroundColor:defaultColor];
    
    [button setBackgroundImage:imageFromColor(defaultColor)
                      forState:UIControlStateNormal];
    [button setBackgroundImage:imageFromColor(selectedColor)
                      forState:UIControlStateHighlighted];
    
    button.layer.cornerRadius = 1;   //kDefaultCornerRadius;
    button.layer.masksToBounds = YES;
    
    /*
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = titleFont;
    //button.titleLabel.textColor = textColor;
    [button setTitleColor:textColor forState:UIControlStateNormal];
    [button setTitleColor:textColor forState:UIControlStateHighlighted];
    button.titleLabel.backgroundColor = [UIColor redColor];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0) {
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    else {
        button.titleLabel.textAlignment = UITextAlignmentCenter;
    }*/
    
    // Title
    CGRect labelFrame;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        labelFrame = CGRectMake(0.0, 0.0, button.frame.size.width, button.frame.size.height);
    }
    else {
        labelFrame = CGRectMake(0.0, 2.0, button.frame.size.width, button.frame.size.height);
    }
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0) {
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.baselineAdjustment = NSTextAlignmentCenter;
    }
    else {
        titleLabel.textAlignment = UITextAlignmentCenter;
        titleLabel.baselineAdjustment = UITextAlignmentCenter;
    }
    titleLabel.numberOfLines = 0;
    titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    titleLabel.font = titleFont;
    titleLabel.textColor = textColor;
    titleLabel.text = title;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.tag = 99;
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [button addSubview:titleLabel];
    
    return button;
}

UIButton *createButtonWithFrame2(CGRect frame, NSString *title, UIFont *titleFont, UIColor *textColor, UIColor *defaultColor, UIColor *selectedColor) {
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    
    //[button setBackgroundColor:defaultColor];
    
    [button setBackgroundImage:imageFromColor(defaultColor)
                      forState:UIControlStateNormal];
    [button setBackgroundImage:imageFromColor(selectedColor)
                      forState:UIControlStateHighlighted];
    
    button.layer.cornerRadius = kDefaultCornerRadius;
    button.layer.masksToBounds = YES;
    
    /*
     [button setTitle:title forState:UIControlStateNormal];
     button.titleLabel.font = titleFont;
     //button.titleLabel.textColor = textColor;
     [button setTitleColor:textColor forState:UIControlStateNormal];
     [button setTitleColor:textColor forState:UIControlStateHighlighted];
     button.titleLabel.backgroundColor = [UIColor redColor];
     if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0) {
     button.titleLabel.textAlignment = NSTextAlignmentCenter;
     }
     else {
     button.titleLabel.textAlignment = UITextAlignmentCenter;
     }*/
    
    // Title
    CGRect labelFrame;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        labelFrame = CGRectMake(0.0, 0.0, button.frame.size.width, button.frame.size.height);
    }
    else {
        labelFrame = CGRectMake(0.0, 2.0, button.frame.size.width, button.frame.size.height);
    }
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0) {
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.baselineAdjustment = NSTextAlignmentCenter;
    }
    else {
        titleLabel.textAlignment = UITextAlignmentCenter;
        titleLabel.baselineAdjustment = UITextAlignmentCenter;
    }
    titleLabel.numberOfLines = 0;
    titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    titleLabel.font = titleFont;
    titleLabel.textColor = textColor;
    titleLabel.text = title;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.tag = 99;
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [button addSubview:titleLabel];
    
    return button;
}

UIButton *createButtonWithIcon(CGRect frame,
                               NSString *icon,
                               UIFont *iconFont,
                               UIColor *iconColor,
                               NSString *title,
                               UIFont *titleFont,
                               UIColor *titleColor,
                               UIColor *defaultColor,
                               UIColor *selectedColor) {
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    
    [button setBackgroundImage:imageFromColor(defaultColor)
                      forState:UIControlStateNormal];
    [button setBackgroundImage:imageFromColor(selectedColor)
                      forState:UIControlStateHighlighted];
    
    button.layer.cornerRadius = kDefaultCornerRadius;
    button.layer.masksToBounds = YES;
    
    CGFloat topPadding = 5.0;
    CGFloat labelLeftPadding = 0.0;
    CGFloat symbolWidth = 30;
    CGRect labelFrame;
    
    // Icon
    labelFrame = CGRectMake(labelLeftPadding, topPadding, symbolWidth, frame.size.height - (topPadding*2));
    UILabel *iconLabel = [[UILabel alloc] initWithFrame:labelFrame];
    iconLabel.textAlignment = UITextAlignmentCenter;
    iconLabel.font = iconFont;
    iconLabel.textColor = iconColor;
    iconLabel.text = icon;
    iconLabel.backgroundColor = [UIColor clearColor];
    [button addSubview:iconLabel];
    
    // Title
    labelFrame = CGRectMake(labelLeftPadding + symbolWidth, topPadding, frame.size.width - ((labelLeftPadding + symbolWidth)*2) , frame.size.height - (topPadding*2));
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
    titleLabel.textAlignment = UITextAlignmentLeft;
    titleLabel.font = titleFont;
    titleLabel.textColor = titleColor;
    titleLabel.text = title;
    titleLabel.backgroundColor = [UIColor clearColor];
    [button addSubview:titleLabel];
    
    CGRect newframe = button.frame;
    newframe.size.width = labelFrame.origin.x + labelFrame.size.width + labelLeftPadding + (iconLabel.frame.size.width/2);
    button.frame = newframe;
    
    //    [button setTitle:title forState:UIControlStateNormal];
    //    button.titleLabel.font = titleFont;
    //
    //    [button setTitleColor:textColor forState:UIControlStateNormal];
    //    [button setTitleColor:textColor forState:UIControlStateHighlighted];
    return button;
}

UIButton *createSquareButtonWithIcon(CGRect frame,
                                     NSString *icon,
                                     UIFont *iconFont,
                                     UIColor *iconColor,
                                     NSString *title,
                                     UIFont *titleFont,
                                     UIColor *titleColor,
                                     UIColor *defaultColor,
                                     UIColor *selectedColor) {
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    
    [button setBackgroundImage:imageFromColor(defaultColor)
                      forState:UIControlStateNormal];
    [button setBackgroundImage:imageFromColor(selectedColor)
                      forState:UIControlStateHighlighted];
    
    CGFloat topPadding = 5.0;
    CGFloat labelLeftPadding = 15.0;
    CGFloat symbolWidth = 30;
    CGRect labelFrame;
    
    // Icon
    labelFrame = CGRectMake(labelLeftPadding, topPadding, symbolWidth, frame.size.height - (topPadding*2));
    UILabel *iconLabel = [[UILabel alloc] initWithFrame:labelFrame];
    iconLabel.textAlignment = UITextAlignmentCenter;
    iconLabel.font = iconFont;
    iconLabel.textColor = iconColor;
    iconLabel.text = icon;
    iconLabel.backgroundColor = kDefaultYellowColor;
    
    iconLabel.layer.cornerRadius = kDefaultCornerRadius;
    iconLabel.layer.masksToBounds = YES;
    
    [button addSubview:iconLabel];
    
    // Title
    labelFrame = CGRectMake(labelLeftPadding + symbolWidth + 15, topPadding, frame.size.width - (labelLeftPadding + symbolWidth +15), frame.size.height - (topPadding*2));
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
    titleLabel.textAlignment = UITextAlignmentLeft;
    titleLabel.font = titleFont;
    titleLabel.textColor = titleColor;
    titleLabel.text = title;
    titleLabel.backgroundColor = [UIColor clearColor];
    [button addSubview:titleLabel];
    
    return button;
}

UIButton *createButtonWithIconAndArrow(CGRect frame,
                                       NSString *icon,
                                       UIFont *iconFont,
                                       UIColor *iconColor,
                                       NSString *title,
                                       UIFont *titleFont,
                                       UIColor *titleColor,
                                       NSString *arrow,
                                       UIFont *arrowFont,
                                       UIColor *arrowColor,
                                       UIColor *defaultColor,
                                       UIColor *selectedColor) {
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    
    [button setBackgroundImage:imageFromColor(defaultColor)
                      forState:UIControlStateNormal];
    [button setBackgroundImage:imageFromColor(selectedColor)
                      forState:UIControlStateHighlighted];
    
    button.layer.cornerRadius = kDefaultCornerRadius;
    button.layer.masksToBounds = YES;
    
    CGFloat topPadding = 5.0;
    CGFloat labelLeftPadding = 0.0;
    CGFloat symbolWidth = 30;
    CGRect labelFrame;
    
    // Icon
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        labelFrame = CGRectMake(labelLeftPadding, topPadding - 1.0, symbolWidth, frame.size.height - (topPadding*2));
    }
    else {
        labelFrame = CGRectMake(labelLeftPadding, topPadding + 1.0, symbolWidth, frame.size.height - (topPadding*2));
    }
    
    UILabel *iconLabel = [[UILabel alloc] initWithFrame:labelFrame];
    iconLabel.textAlignment = UITextAlignmentCenter;
    iconLabel.font = iconFont;
    iconLabel.textColor = iconColor;
    iconLabel.text = icon;
    iconLabel.backgroundColor = [UIColor clearColor];
    [button addSubview:iconLabel];
    
    // Title
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        labelFrame = CGRectMake(labelLeftPadding + symbolWidth, topPadding + 1.0, frame.size.width - ((labelLeftPadding + symbolWidth)*2) , frame.size.height - (topPadding*2));
    }
    else {
        labelFrame = CGRectMake(labelLeftPadding + symbolWidth, topPadding + 2.0, frame.size.width - ((labelLeftPadding + symbolWidth)*2) , frame.size.height - (topPadding*2));
    }
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
    titleLabel.textAlignment = UITextAlignmentLeft;
    titleLabel.font = titleFont;
    titleLabel.textColor = titleColor;
    titleLabel.text = title;
    titleLabel.backgroundColor = [UIColor clearColor];
    [button addSubview:titleLabel];
    
    // Arrow
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        labelFrame = CGRectMake(labelLeftPadding + symbolWidth + titleLabel.frame.size.width - kDefaultComponentPadding, topPadding, symbolWidth, frame.size.height - (topPadding*2));
    }
    else {
        labelFrame = CGRectMake(labelLeftPadding + symbolWidth + titleLabel.frame.size.width - kDefaultComponentPadding, topPadding + 1.0, symbolWidth, frame.size.height - (topPadding*2));
    }
    
    UILabel *arrowLabel = [[UILabel alloc] initWithFrame:labelFrame];
    arrowLabel.textAlignment = UITextAlignmentRight;
    arrowLabel.font = arrowFont;
    arrowLabel.textColor = arrowColor;
    arrowLabel.text = arrow;
    arrowLabel.backgroundColor = [UIColor clearColor];
    [button addSubview:arrowLabel];
    
//    [button setTitle:title forState:UIControlStateNormal];
//    button.titleLabel.font = titleFont;
//    
//    [button setTitleColor:textColor forState:UIControlStateNormal];
//    [button setTitleColor:textColor forState:UIControlStateHighlighted];
    return button;
}

UILabel *createLabel(CGRect frame, UIFont *font, NSString *text, UIColor *textColor, int numberOfLines, NSTextAlignment textAlignment)
{
    UILabel *label = [[[UILabel alloc] init] autorelease];
    label.frame = frame;
    label.numberOfLines = numberOfLines;
    label.font = font;
    label.textColor = textColor;
    label.textAlignment = textAlignment;
    label.text = text;
    label.backgroundColor = [UIColor clearColor];
    
    return label;
}

UIImage *imageFromColor(UIColor *color) {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    //  [[UIColor colorWithRed:222./255 green:227./255 blue: 229./255 alpha:1] CGColor]) ;
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

UIImage *imageFromButton(UIButton *button) {
    CGRect rect = CGRectMake(0, 0, 30, 30);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [button.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
    
    /*
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    //  [[UIColor colorWithRed:222./255 green:227./255 blue: 229./255 alpha:1] CGColor]) ;
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;*/
}

UIImage *imageFromLabel(UILabel *label) {
    CGRect rect = CGRectMake(0, 0, 30, 30);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [label.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

UIImage *createImageWithRect(CGRect rect, UIColor *color) {
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    //  [[UIColor colorWithRed:222./255 green:227./255 blue: 229./255 alpha:1] CGColor]) ;
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

NSString *generateUniqueString() {
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef uuidStr = CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    [(NSString *)uuidStr autorelease];
    return (NSString *)uuidStr;
}

NSString *generateCaptchaURL() {
    NSString *cid = generateUniqueString();
    
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.cid = cid;
    
    NSString *captchaURL = [NSString stringWithFormat:@"%@%@%@?cid=%@",BASE_URL,SSO,CAPTCHA,cid];
    return captchaURL;
}

UINavigationController *customizedNavigationController(CGRect rect, BOOL isLogin) {
    
    UINavigationController *navController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
    
    // Ensure the UINavigationBar is created so that it can be archived. If we do not access the
    // navigation bar then it will not be allocated, and thus, it will not be archived by the
    // NSKeyedArchvier.
    [navController navigationBar];
    
    // Archive the navigation controller.
    NSMutableData *data = [NSMutableData data];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:navController forKey:@"root"];
    [archiver finishEncoding];
    [archiver release];
    [navController release];
    
    // Unarchive the navigation controller and ensure that our UINavigationBar subclass is used.
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [unarchiver setClass:[AXISnetNavigationBar class] forClassName:@"UINavigationBar"];
    UINavigationController *customizedNavController = [unarchiver decodeObjectForKey:@"root"];
    [unarchiver finishDecoding];
    [unarchiver release];
    
    //NSLog(@"navcontroller height = %f",customizedNavController.navigationBar.frame.size.height);
    
    // Modify the navigation bar to have a background image.
    AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[customizedNavController navigationBar];
    
    [navBar setTintColor:[UIColor whiteColor]];
    //[navBar setBackgroundImage:[UIImage imageNamed:@"NavBar-iPhone.png"] forBarMetrics:UIBarMetricsDefault];
    //[navBar setBackgroundImage:[UIImage imageNamed:@"NavBar-iPhone.png"] forBarMetrics:UIBarMetricsLandscapePhone];
    
    // Fixing UINavigationBar for iOS 7
    CGRect backgroundRect;
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO) {
        //NSLog(@"iOS 7 Common");
        backgroundRect = CGRectMake(0.0, 0.0, rect.size.width*2, 84.0*2);
    } else {
        backgroundRect = CGRectMake(0.0, 0.0, rect.size.width, 64.0);
        /*
        if (isLogin) {
            backgroundRect = CGRectMake(0.0, 0.0, rect.size.width, 44.0);
        }
        else {
            backgroundRect = CGRectMake(0.0, 0.0, rect.size.width, 64.0);
        }*/
    }
    //---------------------------------
    
    //UIImage *backgroundImage = createImageWithRect(backgroundRect, navigationBarColor);
    
    //--------------------//
    // The old background //
    //--------------------//
    //UIImage *backgroundImage = createImageWithRect(backgroundRect, [UIColor whiteColor]);
    
    //----------------//
    // CR Change Logo //
    //----------------//
    CGSize bgSize;
    CGPoint centerPoint;
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO) {
        //bgSize = CGSizeMake(rect.size.width*2, 84.0*2);
        if (IS_IPAD) {
            bgSize = CGSizeMake(1024.0, 64.0);
        }
        else {
            bgSize = CGSizeMake(rect.size.width, 64.0);
        }
        
        centerPoint = CGPointMake(1, 1/4);
    } else {
        if (IS_IPAD) {
            bgSize = CGSizeMake(1024.0, 64.0);
        }
        else {
            bgSize = CGSizeMake(rect.size.width, 64.0);
        }
        
        centerPoint = CGPointMake(1, 1/4);
    }
    
    /*
     * Restructure & Reskinning
     */
    DataManager *sharedData = [DataManager sharedInstance];
    UIImage *backgroundImage;
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        /*
        backgroundImage = radialGradientImage(bgSize, 0.0/255.0, 91.0/255.0, 170.0/255.0,
                                                       25.0/255.0, 47.0/255.0, 124.0/255.0, centerPoint, 3.0);*/
        
        backgroundImage = radialGradientImage2(bgSize, colorWithHexString(sharedData.profileData.themesModel.header),
                                              colorWithHexString(sharedData.profileData.themesModel.header), centerPoint, 3.0);
        navBar.bottomWhiteBar.backgroundColor = colorWithHexString(sharedData.profileData.themesModel.header);
        navBar.msisdnLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
        navBar.deviceLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
        navBar.hiLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
    }
    else if ([sharedData.profileData.themesModel.header isEqualToString:@""]) {
        backgroundImage = radialGradientImage2(bgSize, colorWithHexString(sharedData.profileData.themesModel.headerGradientStart),
                                              colorWithHexString(sharedData.profileData.themesModel.headerGradientEnd), centerPoint, 3.0);
        navBar.bottomWhiteBar.backgroundColor = colorWithHexString(sharedData.profileData.themesModel.headerGradientEnd);
        navBar.msisdnLabel.textColor = kDefaultWhiteColor;
        navBar.deviceLabel.textColor = kDefaultWhiteColor;
        navBar.hiLabel.textColor = kDefaultWhiteColor;
    }
    else {
        backgroundImage = radialGradientImage(bgSize, 0.0/255.0, 91.0/255.0, 170.0/255.0,
                                              25.0/255.0, 47.0/255.0, 124.0/255.0, centerPoint, 3.0);
        navBar.bottomWhiteBar.backgroundColor = kDefaultNavyBlueColor;
        navBar.msisdnLabel.textColor = kDefaultWhiteColor;
        navBar.deviceLabel.textColor = kDefaultWhiteColor;
        navBar.hiLabel.textColor = kDefaultWhiteColor;
    }
    //-------------------------
    
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    [navBar setIsLogin:isLogin];
    [navBar updateGreeting];
    
    //navBar.layer.shadowColor = [[UIColor blackColor] CGColor];
    //navBar.layer.shadowOffset = CGSizeMake(1.0, 1.0);
    //navBar.layer.shadowOpacity = 0.40;
    
    return customizedNavController;
}

NSString *convertPackageType(NSString *packageType) {
    if ([packageType isEqualToString:@"0"]) {
        return @"Pay Per Use";
    }
    else if ([packageType isEqualToString:@"1"]) {
        return @"Quota";
    }
    else if ([packageType isEqualToString:@"2"]) {
        return @"Unlimited";
    }
    else {
        return @"Unlimited";
    }
}

// TODO : Neptune
CGSize calculateLabelSize(CGFloat labelWidth, NSString *text, UIFont *font) {
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          font, NSFontAttributeName,
                                          nil];
    CGRect lblframe = [text boundingRectWithSize:CGSizeMake(labelWidth, CGFLOAT_MAX)
                                         options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                      attributes:attributesDictionary
                                         context:nil];
    lblframe = CGRectIntegral(lblframe);
    
    return lblframe.size;
}

CGSize calculateExpectedSize(UILabel *label, NSString *text) {
    NSLog(@"---- text = %@",text);
    NSLog(@"label width & height = %f : %f",label.frame.size.width, label.frame.size.height);
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width,9999);
    CGSize expectedLabelSize = [text sizeWithFont:label.font
                                constrainedToSize:maximumLabelSize
                                    lineBreakMode:label.lineBreakMode];
    NSLog(@"*****************");
    NSLog(@"Expected label width & height = %f : %f",expectedLabelSize.width, expectedLabelSize.height);
    NSLog(@"*****************");
    return expectedLabelSize;
}

NSString *addThousandsSeparator(NSString *inputString, NSString *lang) {
    NSString *currentLang = lang;
    
	NSString *displayedString = @"";
	if ([inputString length] == 0){
		displayedString = @"Rp.0";
	}
	else {
		if ([inputString length] > 3) {
			int lengthStringOri = [inputString length];
			for (int i=lengthStringOri; i>0; i-=3) {
				if (i > 3) {
					NSString *myStringPrt1 = [inputString substringWithRange:NSMakeRange(0,i-3)];
					NSString *myStringPrt2 = [inputString substringFromIndex:i-3];
                    NSString *combinedString;
                    if ([currentLang isEqualToString:@"id"]) {
                        combinedString = [myStringPrt1 stringByAppendingString:@"."];
                    }
					else if ([currentLang isEqualToString:@"en"]) {
                        combinedString = [myStringPrt1 stringByAppendingString:@","];
                    }
                    else {
                        combinedString = [myStringPrt1 stringByAppendingString:@"."];
                    }
					combinedString = [combinedString stringByAppendingString:myStringPrt2];
					inputString = combinedString;
                    displayedString = [NSString stringWithFormat:@"Rp.%@",combinedString];
				}
			}
		}
		else {
			displayedString = [NSString stringWithFormat:@"Rp.%@",inputString];
		}
	}
	return displayedString;
}

NSString *changeDateFormat(NSString *inputDate) {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    NSDate *date = [dateFormatter dateFromString:inputDate];
    
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

NSString *changeDateFormatForPackage(NSString *inputDate) {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:inputDate];
    
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

NSString *changeDateTimeFormatForNotification(NSString *inputDate) {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //2013-11-27T10:25:24.723+07:00
    //yyyy-MM-dd'T'HH:mm:SS.SSS'Z'
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:SS.SSSZZZZ"];
    NSDate *date = [dateFormatter dateFromString:inputDate];
    
    [dateFormatter setDateFormat:@"dd-MMM-yyyy HH:mm:SS"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

NSString *changeDateFormatForNotification(NSString *inputDate, int output) {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:inputDate];
    
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    
    NSString *outputString = @"";
    
    // 0 = date
    // 1 = month & year
    // 2 = complete
    // 3 = year
    if (output == 0) {
        outputString = [formattedDate substringToIndex:2];
    }
    else if (output == 1) {
        outputString = [formattedDate substringFromIndex:3];
    }
    else if (output == 3) {
        outputString = [formattedDate substringFromIndex:7];
    }
    else {
        [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
        outputString = [dateFormatter stringFromDate:date];
    }
    
    return outputString;
}

BOOL isAllDigits(NSString* inputString) {
    NSCharacterSet* nonNumbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    NSRange r = [inputString rangeOfCharacterFromSet:nonNumbers];
    return r.location == NSNotFound;
}

BOOL validateUrl(NSString *candidate) {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}

void setupIcon() {
    NSMutableDictionary *iconDict = [[NSMutableDictionary alloc] init];
    
    /*
     * Home : Feature Menu
     */
    
    
    // Used in Menu
    //[iconDict setObject:@"\ue01f" forKey:@"1"];         // Balance
    [iconDict setObject:@"\ue001" forKey:@"1"];         // Balance
    [iconDict setObject:@"\ue01f" forKey:@"1_003"];     // Check Balance
    [iconDict setObject:@"\ue025" forKey:@"2_0002"];     // Reload balance
    [iconDict setObject:@"\ue007" forKey:@"1_005"];     // Buy voucher
    [iconDict setObject:@"\ue026" forKey:@"1_006"];     // Balance transfer
    //[iconDict setObject:@"R" forKey:@"1_006"];     // Balance transfer
    [iconDict setObject:@"\ue02C" forKey:@"1_007"];     // Extend validity
    [iconDict setObject:@"\ue02C" forKey:@"1_007_001"]; // Extend validity 7 day
    [iconDict setObject:@"\ue039" forKey:@"1_0031"];    // Check bonus
    [iconDict setObject:@"\ue030" forKey:@"1_0071"];    // Transaction history
    [iconDict setObject:@"v" forKey:@"8"];              // AXIS box
    [iconDict setObject:@"\ue01a" forKey:@"8_1"];       // Contact
    [iconDict setObject:@"\ue018" forKey:@"8_1_1"];     // Contact Backup
    [iconDict setObject:@"\ue019" forKey:@"8_1_2"];     // Contact Restore
    
    [iconDict setObject:@"1" forKey:@"8_2"];            // Photo
    [iconDict setObject:@"\ue018" forKey:@"8_2_1"];     // Photo Export
    [iconDict setObject:@"\ue019" forKey:@"8_2_2"];     // Photo Import
    [iconDict setObject:@"+" forKey:@"8_4"];            // Terms & Conditions
    
    //[iconDict setObject:@"C" forKey:@"2"];               // Package
    
    //[iconDict setObject:@"A" forKey:@"2"];               // Package
    [iconDict setObject:@"\ue01B" forKey:@"2"];               // Package
    
    [iconDict setObject:@"\ue001" forKey:@"2_001"];      // Check Usage
    [iconDict setObject:@"C" forKey:@"2_001111"];        // My Package
    [iconDict setObject:@"\ue026" forKey:@"2_002"];      // Transfer Package
    [iconDict setObject:@"J" forKey:@"2_003"];           // Recommendation
    //[iconDict setObject:@"6" forKey:@"2_0199"];          // Survey
    [iconDict setObject:@"6" forKey:@"2_0121"];          // Survey
    
    //[iconDict setObject:@"P" forKey:@"3"];      // Notification
    
    [iconDict setObject:@"C" forKey:@"3"];      // Notification
    
    [iconDict setObject:@"\ue00a" forKey:@"4"]; // Promo
    
    [iconDict setObject:@"\ue023" forKey:@"7"]; // AXIS Store
    [iconDict setObject:@"\ue00e" forKey:@"7_2"]; // AXIS Store : Category
    [iconDict setObject:@"\ue030" forKey:@"7_5"]; // AXIS Store : Trx History
    [iconDict setObject:@"\ue024" forKey:@"7_6"]; // AXIS Store : Guide
    [iconDict setObject:@"\ue00f" forKey:@"7_7"]; // AXIS Store : Term & Condition
    
    //[iconDict setObject:@"S" forKey:@"9"];           // Info
    
    [iconDict setObject:@"S" forKey:@"5"];           // Info
    
    [iconDict setObject:@"\ue008" forKey:@"9_001"];  // FAQ
    [iconDict setObject:@"B" forKey:@"9_002"];       // Store Locator
    [iconDict setObject:@"\u0040" forKey:@"9_003"];  // Device Setting
    [iconDict setObject:@"\u0040" forKey:@"9_003_001"];  // Device Setting SMS
    [iconDict setObject:@"\u0040" forKey:@"9_003_002"];  // Device Setting Manual
    [iconDict setObject:@"G" forKey:@"9_004"];       // PUK
    [iconDict setObject:@"5" forKey:@"9_005"];       // Contact Us
    
    // Used in Group Menu
    //[iconDict setObject:@"\ue01f" forKey:@"gid_4"];     // Group Balance
    [iconDict setObject:@"A" forKey:@"gid_4"];     // Group Balance
    //[iconDict setObject:@"\u0040" forKey:@"gid_60"];    // Group Other
    [iconDict setObject:@"v" forKey:@"gid_60"];    // Group Other
    [iconDict setObject:@"v" forKey:@"gid_61"];    // Group AXIS box
    [iconDict setObject:@"\ue01a" forKey:@"gid_62"];    // Group Contact
    [iconDict setObject:@"1" forKey:@"gid_63"];    // Group Photo
    
    [iconDict setObject:@"C" forKey:@"gid_5"];     // Group My Package
    [iconDict setObject:@"\ue026" forKey:@"gid_20"]; // Group Transfer Package
    [iconDict setObject:@"J" forKey:@"gid_2_003"]; // Group Recommendation
    [iconDict setObject:@"6" forKey:@"gid_2_0199"];// Group Survey
    [iconDict setObject:@"C" forKey:@"gid_6"];     // Group Package
    [iconDict setObject:@"C" forKey:@"gid_25"];    // Other Package
    [iconDict setObject:@"\ue00a" forKey:@"gid_50"]; // Group Promo
    [iconDict setObject:@"Z" forKey:@"gid_91"];    // Group Info
    [iconDict setObject:@"S" forKey:@"gid_92"];    // Group Help
    [iconDict setObject:@"\u0040" forKey:@"gid_94"];    // Group Device Setting
    [iconDict setObject:@"\ue02C" forKey:@"gid_7"]; // Group Extend Validity
    
    [iconDict setObject:@"C" forKey:@"default_icon"];    // Default Icon
    
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.iconData = [NSDictionary dictionaryWithDictionary:iconDict];
    [iconDict release];
}

NSString *icon(NSString *menuId) {
    DataManager *sharedData = [DataManager sharedInstance];
    return [sharedData.iconData objectForKey:menuId];
}

BOOL isContainHTMLTag(NSString *inputString) {
    BOOL result;
    if (inputString)
    {
        if ([inputString length] > 0)
        {
            NSRange r;
            
            if ((r = [inputString rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
            {
                result = YES;
            }
            else {
                result = NO;
            }
        }
        else {
            result = NO;
        }
    }
    else {
        result = NO;
    }
    return result;
}

BOOL validateChoice(SurveyModel *surveyModel, int input) {
    //NSLog(@"input = %i",input);
    BOOL result = NO;
    int min = [surveyModel.minAnswer intValue];
    int max = [surveyModel.maxAnswer intValue];
    if (input >= min && input <= max) {
        result = YES;
    }
    return  result;
}

NSString *appVersion() {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    //NSString *appDisplayName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    //NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    //return [NSString stringWithFormat:@"%@, Version %@ (%@)",appDisplayName, majorVersion, minorVersion];
    
    //NSLog(@"majorVersion = %@",majorVersion);
    
    return majorVersion;
}

UIColor *legendColorForBar(int legendColorDef) {
    UIColor *color = nil;
    switch (legendColorDef) {
        case 0:
            // green
            color = [UIColor colorWithRed:0.0 green:166.0/255.0 blue:81.0/255.0 alpha:1.0];
            break;
        case 1:
            // ocean blue
            color = [UIColor colorWithRed:0.0 green:114.0/255.0 blue:188.0/255.0 alpha:1.0];
            break;
        case 2:
            // yellow
            color = [UIColor colorWithRed:251.0/255.0 green:170.0/255.0 blue:32.0/255.0 alpha:1.0];
            break;
        case 3:
            // navy blue
            color = [UIColor colorWithRed:24.0/255.0 green:67.0/255.0 blue:118.0/255.0 alpha:1.0];
            break;
        default:
            // red
            color = [UIColor colorWithRed:237.0/255.0 green:20.0/255.0 blue:91.0/255.0 alpha:1.0];
            break;
    }
    return color;
}

BOOL isValidEmail(NSString* checkString) {
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,4})$";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

BOOL isValidCharacter(NSString* checkString) {
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ "] invertedSet];
    if ([checkString rangeOfCharacterFromSet:set].location == NSNotFound) {
        return YES;
    }
    else {
        return NO;
    }
}

/*
void drawLinearGradient(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef endColor) {
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = [NSArray arrayWithObjects:(id)startColor, (id)endColor, nil];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace,
                                                        (CFArrayRef) colors, locations);
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextSaveGState(context);
    CGContextAddRect(context, rect);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

void addRoundedRectWithStroke(CGContextRef context, CGRect rect, CGFloat cornerRadius, CGColorRef rectColor, CGColorRef strokeColor, CGFloat strokeWidth) {
    
    CGContextSetLineWidth(context, strokeWidth);
    CGContextSetStrokeColorWithColor(context, strokeColor);
    CGContextSetFillColorWithColor(context, rectColor);
    
    CGRect rrect = rect;
    
    CGFloat radius = cornerRadius;
    CGFloat width = CGRectGetWidth(rrect);
    CGFloat height = CGRectGetHeight(rrect);
    
    // Make sure corner radius isn't larger than half the shorter side
    if (radius > width/2.0)
        radius = width/2.0;
    if (radius > height/2.0)
        radius = height/2.0;
    
    CGFloat minx = CGRectGetMinX(rrect);
    CGFloat midx = CGRectGetMidX(rrect);
    CGFloat maxx = CGRectGetMaxX(rrect);
    CGFloat miny = CGRectGetMinY(rrect);
    CGFloat midy = CGRectGetMidY(rrect);
    CGFloat maxy = CGRectGetMaxY(rrect);
    CGContextMoveToPoint(context, minx, midy);
    CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
    CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathFillStroke);
}

void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth, float ovalHeight)
{
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) {
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
    CGContextTranslateCTM (context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth (rect) / ovalWidth;
    fh = CGRectGetHeight (rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}*/

void resetAllData() {
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.showBalance = NO;
    sharedData.dropdownMenuCreated = NO;
    sharedData.showBanner = NO;
    
    // TODO : Hygiene
    sharedData.profileData = nil;
    
    // 4G_USIM
    sharedData.ratingValue = 0;
    sharedData.commentValue = @"";
    
    // TODO : V1.9
    sharedData.unreadMsg = 0;
    
    // TODO : V1.9.5
    saveDataToPreferenceWithKeyAndValue(PROFILE_MSISDN_CACHE_KEY, @"");
    saveDataToPreferenceWithKeyAndValue(PROFILE_PASSWORD_CACHE_KEY, @"");
    saveDataToPreferenceWithKeyAndValue(PROFILE_FLAG_KMSI_CACHE_KEY, @"0");
    saveDataToPreferenceWithKeyAndValue(PROFILE_TOKEN_CACHE_KEY, @"");
    
    /*
     * TODO: V1.9.5
     * Save to preference for caching
     */
    NSString *key = USAGE_CACHE_KEY;
    saveDataToPreferenceWithKeyAndValue(key, @"");
    sharedData.quotaImprovementData = nil;
    
    // Clear All HTTP Request
    for (ASIHTTPRequest *request in ASIHTTPRequest.sharedQueue.operations) {
        [request cancel];
        [request setDelegate:nil];
    }
}

UIColor *colorWithHexString(NSString *hex) {
    
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    // strip # if it appears
    if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

BOOL isStandardLogoVersionSame(NSString *logoVersion, NSString *telcoOperator) {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

    NSString *current;
    if ([telcoOperator isEqualToString:@"XL"]) {
        NSString *version = XL_LOGO_STD;
        current = [prefs stringForKey:version];
    }
    else {
        NSString *version = AXIS_LOGO_STD;
        current = [prefs stringForKey:version];
    }
    
    if (current == nil) {
        //NSLog(@"----> LOGO KOSONG");
        return NO;
    }
    else if ([current isEqualToString:logoVersion]) {
        //NSLog(@"----> LOGO SAMA");
        return YES;
    }
    else {
        //NSLog(@"----> LOGO BEDA VERSI");
        return NO;
    }
}

BOOL isRetinaLogoVersionSame(NSString *logoVersion, NSString *telcoOperator) {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString *current = @"";
    if ([telcoOperator isEqualToString:@"XL"]) {
        NSString *version = XL_LOGO_RETINA;
        current = [prefs stringForKey:version];
    }
    else {
        NSString *version = AXIS_LOGO_RETINA;
        current = [prefs stringForKey:version];
    }
    
    if (current == nil) {
        //NSLog(@"----> LOGO KOSONG");
        return NO;
    }
    else if ([current isEqualToString:logoVersion]) {
        //NSLog(@"----> LOGO SAMA");
        return YES;
    }
    else {
        //NSLog(@"----> LOGO BEDA VERSI");
        return NO;
    }
}

UIImage *radialGradientImage(CGSize size, float startR, float startG, float startB, float endR, float endG, float endB, CGPoint centre, float radius) {
    // Render a radial background
    // http://developer.apple.com/library/ios/#documentation/GraphicsImaging/Conceptual/drawingwithquartz2d/dq_shadings/dq_shadings.html
    
    // Initialise
    UIGraphicsBeginImageContextWithOptions(size, YES, 1);
    
    // Create the gradient's colours
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = { startR,startG,startB, 1.0,  // Start color
        endR,endG,endB, 1.0 }; // End color
    
    CGColorSpaceRef myColorspace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef myGradient = CGGradientCreateWithColorComponents (myColorspace, components, locations, num_locations);
    
    // Normalise the 0-1 ranged inputs to the width of the image
    CGPoint myCentrePoint = CGPointMake(centre.x * size.width, centre.y * size.height);
    float myRadius = MIN(size.width, size.height) * radius;
    
    // Draw it!
    CGContextDrawRadialGradient (UIGraphicsGetCurrentContext(), myGradient, myCentrePoint,
                                 0, myCentrePoint, myRadius,
                                 kCGGradientDrawsAfterEndLocation);
    
    // Grab it as an autoreleased image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Clean up
    CGColorSpaceRelease(myColorspace); // Necessary?
    CGGradientRelease(myGradient); // Necessary?
    UIGraphicsEndImageContext(); // Clean up
    return image;
}

UIImage *radialGradientImage2(CGSize size, UIColor *start, UIColor *end, CGPoint centre, float radius) {
    // Render a radial background
    // http://developer.apple.com/library/ios/#documentation/GraphicsImaging/Conceptual/drawingwithquartz2d/dq_shadings/dq_shadings.html
    
    // Initialise
    UIGraphicsBeginImageContextWithOptions(size, 0, 1);
    
    // Create the gradient's colours
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = { 0,0,0,0,  // Start color
        0,0,0,0 }; // End color
    [start getRed:&components[0] green:&components[1] blue:&components[2] alpha:&components[3]];
    [end getRed:&components[4] green:&components[5] blue:&components[6] alpha:&components[7]];
    
    CGColorSpaceRef myColorspace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef myGradient = CGGradientCreateWithColorComponents (myColorspace, components, locations, num_locations);
    
    // Normalise the 0-1 ranged inputs to the width of the image
    CGPoint myCentrePoint = CGPointMake(centre.x * size.width, centre.y * size.height);
    float myRadius = MIN(size.width, size.height) * radius;
    
    // Draw it!
    CGContextDrawRadialGradient (UIGraphicsGetCurrentContext(), myGradient, myCentrePoint,
                                 0, myCentrePoint, myRadius,
                                 kCGGradientDrawsAfterEndLocation);
    
    // Grab it as an autoreleased image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Clean up
    CGColorSpaceRelease(myColorspace); // Necessary?
    CGGradientRelease(myGradient); // Necessary?
    UIGraphicsEndImageContext(); // Clean up
    return image;
}

NSString *applicationDocumentsDirectory() {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

void saveImage(UIImage *image, NSString *imageName)
{
    NSData *imageData = UIImagePNGRepresentation(image); //convert image into .png format.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imageName]]; //add our image to the path
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
    
    //NSLog(@"image saved");
}

void changeNavbarBg(AXISnetNavigationBar *navigationBar, CGRect rectangle) {
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    AXISnetNavigationBar *navBar = navigationBar;
    
    /*
     * Restructure & Reskinning
     */
    CGRect rect = rectangle;
    CGSize bgSize;
    CGPoint centerPoint;
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO) {
        
        if (IS_IPAD) {
            bgSize = CGSizeMake(1024.0, 64.0);
        }
        else {
            bgSize = CGSizeMake(rect.size.width, 64.0);
        }
        
        centerPoint = CGPointMake(1, 1/4);
    } else {
        if (IS_IPAD) {
            bgSize = CGSizeMake(1024.0, 64.0);
        }
        else {
            bgSize = CGSizeMake(rect.size.width, 64.0);
        }
        
        centerPoint = CGPointMake(1, 1/4);
    }
    
    UIImage *backgroundImage;
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        // AXIS
        backgroundImage = radialGradientImage2(bgSize, colorWithHexString(sharedData.profileData.themesModel.header),
                                               colorWithHexString(sharedData.profileData.themesModel.header), centerPoint, 3.0);
        navBar.bottomWhiteBar.backgroundColor = colorWithHexString(sharedData.profileData.themesModel.header);
        navBar.msisdnLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
        navBar.deviceLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
    }
    else if ([sharedData.profileData.themesModel.header isEqualToString:@""]) {
        // XL
        backgroundImage = radialGradientImage2(bgSize, colorWithHexString(sharedData.profileData.themesModel.headerGradientStart),
                                               colorWithHexString(sharedData.profileData.themesModel.headerGradientEnd), centerPoint, 3.0);
        navBar.bottomWhiteBar.backgroundColor = colorWithHexString(sharedData.profileData.themesModel.headerGradientEnd);
        navBar.msisdnLabel.textColor = kDefaultWhiteColor;
        navBar.deviceLabel.textColor = kDefaultWhiteColor;
    }
    else {
        backgroundImage = radialGradientImage(bgSize, 0.0/255.0, 91.0/255.0, 170.0/255.0,
                                              25.0/255.0, 47.0/255.0, 124.0/255.0, centerPoint, 3.0);
        navBar.bottomWhiteBar.backgroundColor = kDefaultNavyBlueColor;
        navBar.msisdnLabel.textColor = kDefaultWhiteColor;
        navBar.deviceLabel.textColor = kDefaultWhiteColor;
    }
    
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
}

// TODO : Hygiene
void saveDataToPreferenceWithKeyAndValue(NSString *key, id value)
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:value forKey:key];
    [prefs synchronize];
}

id getDataFromPreferenceWithKey(NSString *key)
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [prefs objectForKey:key];
}

void saveCustomObjectQuota(QuotaImprovementModel *object, NSString *key) {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
}

QuotaImprovementModel* loadCustomObjectQuotaWithKey(NSString *key) {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    QuotaImprovementModel *object = (QuotaImprovementModel*)[NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

BOOL validateMsisdn(NSString *msisdn)
{
    msisdn = [msisdn stringByReplacingOccurrencesOfString:@"+" withString:@""];
    BOOL valid = NO;
    NSArray *msisdnPrefixes = [NSArray arrayWithObjects:@"0817",@"0818",@"0819",@"0877",@"0878",@"0859",@"083",@"62818",@"62817",@"62819",@"62877",@"62878",@"62859",@"6283",nil];
    for (NSString *prefix in msisdnPrefixes)
    {
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF like %@",
                                  [prefix stringByAppendingString:@"*"]];
        valid = [predicate evaluateWithObject:msisdn];
        if(valid)
            break;
    }
    
    return valid;
}

+(NSString *)formatStringWithCurrency:(double)numberToFormat
{
    @try {
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [numberFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"id"]];
        [numberFormatter setCurrencySymbol:@""];
        
        // get rid of the decimals
        [numberFormatter setMaximumFractionDigits:0];
        NSString *strNumber = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:numberToFormat]];
        
        return strNumber;
    }
    @catch (NSException *exception) {
        NSLog(@"exception on formating currency %@", [exception reason]);
    }
    return @"";
}

+(void)saveGAITrackingID:(NSString *)trackingID
{
    NSString *oldTrackingID = [self getGAITrackingID];
    if(![oldTrackingID isEqualToString:trackingID])
    {
        NSLog(@"new tracking ID");
        NSString *sharedKey = SHARED_KEY_SSO;
        NSString *encryptedTrackingID = [EncryptDecrypt doCipherAES:trackingID action:kCCEncrypt withKey:sharedKey];
//        [[NSUserDefaults standardUserDefaults] registerDefaults:[NSDictionary dictionaryWithObject:encryptedTrackingID forKey:@"tracking_id"]];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:encryptedTrackingID forKey:@"tracking_id"];
        [defaults synchronize];
    }
}

+(NSString *)getGAITrackingID
{
    NSString *sharedKey = SHARED_KEY_SSO;
    NSString *trackingID = [[NSUserDefaults standardUserDefaults] objectForKey:@"tracking_id"];
    if(trackingID)
    {
        trackingID = [EncryptDecrypt doCipherAES:trackingID action:kCCDecrypt withKey:sharedKey];
        return trackingID;
    }
    return @"";
}

+(NSString *)formatDateFromString:(NSString *)strDate
{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSDate *date = [dateFormatter dateFromString:strDate];
    [dateFormatter setDateFormat:@"dd MMMM yyyy"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+0700"]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"id"]];
    return [dateFormatter stringFromDate:date];
}

MatrixType convertMatrix(NSString *input) {
    if ([input isEqualToString:@"000"]) {
        return MATRIX_000;
    }
    else if ([input isEqualToString:@"001"]) {
        return MATRIX_001;
    }
    else if ([input isEqualToString:@"010"]) {
        return MATRIX_010;
    }
    else if ([input isEqualToString:@"011"]) {
        return MATRIX_011;
    }
    else if ([input isEqualToString:@"100"]) {
        return MATRIX_100;
    }
    else if ([input isEqualToString:@"101"]) {
        return MATRIX_101;
    }
    else if ([input isEqualToString:@"110"]) {
        return MATRIX_110;
    }
    else {
        return MATRIX_111;
    }
}

@end
