//
//  DataManager.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/14/11.
//  Copyright (c) 2012 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProfileModel.h"
#import "CheckBalanceModel.h"
#import "TopUpVoucherModel.h"
#import "BalanceTransferModel.h"
#import "PaymentDebitModel.h"
#import "PUKModel.h"
#import "ExtendValidityModel.h"
#import "BuyPackageModel.h"
#import "MenuModel.h"
#import "LabelValueModel.h"
#import "NotifModel.h"
#import "QuotaMeterModel.h"
#import "QuotaCalculatorModel.h"
#import "MappingSukaSukaModel.h"
#import "LoginWordingModel.h"
#import "PackageSukaMatrixModel.h"
#import "FacebookModel.h"
#import "PageSukaModel.h"
#import "RenameSukaSukaModel.h"
// TODO : V1.9
#import "UpdateVersionModel.h"
#import "CheckPointModel.h"
#import "WinnerListModel.h"
#import "XtraCheckBonusModel.h"

#import "FourGPackageModel.h"

#import "QuotaImprovementModel.h"

@interface DataManager : NSObject {
    NSDictionary *iconData;
    
    ProfileModel *profileData;
    NSDictionary *menuData;
    NSArray *homeMenu;
    NSArray *sideMenu;
    NSArray *balanceMenu;
    CheckBalanceModel *checkBalanceData;
    TopUpVoucherModel *topUpVoucherModel;
    ExtendValidityModel *extendValidityData;
    BuyPackageModel *buyPackageData;
    NSArray *checkUsageData;
    NSArray *checkUsageXLData;
    NSArray *recommendedData;
    NSArray *upgradePackageData;
    NSArray *surveyData;
    NSArray *promoData;
    NSArray *info4gData;
    
    NSArray *notificationData;
    NotifModel *detailNotificationData;
    NSString *replyNotifDesc;
    
    NSArray *lastTransactionData;
    NSString *parentId;
    MenuModel *menuModel;
    NSString *cid;
    UINavigationController *navController;
    
    NSArray *adsData;
    
    NSArray *balanceItemsData;
    BalanceTransferModel *balanceTransferData;
    
    NSDictionary *paymentParamData;
    NSString *paymentHelpData;
    PaymentDebitModel *paymentDebitData;
    
    NSArray *contactUsData;
    NSArray *roamingData;
    PUKModel *pukData;
    NSDictionary *deviceSettingData;
    NSArray *shopLocationData;
    NSArray *faqData;
    
    LabelValueModel *termAndConditionData;
    LabelValueModel *userGuideData;
    NSArray *trxHistoryData;
    NSArray *categoryData;
    NSArray *productData;
    
    NSArray *dropboxFileData;
    
    int unread;
    NSString *currentVersion;
    
    NSString *packageId;
    NSString *xlstarId;
    LabelValueModel *xlStarData;
    
    //NSDictionary *notifDetailData;
    CheckBalanceModel *xlTunaiBalanceData;
    
    // To maintain change language in dropdown menu at Home Page
    BOOL dropdownMenuCreated;
    
    // To maintain Balance Info Show or Not
    BOOL showBalance;
    
    // To maintain Banner Show or Not
    BOOL showBanner;
    
    // Hot Offer Banner
    NSArray *hotOfferBannerData;
    
    // Left Menu
    NSArray *leftMenuData;
    
    BOOL isPostpaid;
    
    // Unread Message Inbox
    int unreadMsg;
    
    // Quota Meter Data
    NSArray *quotaMeterData;
    
    // To maintain Banner On Quota Meter Low Type, Show or Not
    BOOL showBannerLowQuotaMeter;
    
    // Recommended Package XL
    NSArray *recommendedPackageXLData;
    
    // Payment Method
    MenuModel *paymentMethodMenu;
    
    // Quota Calculator
    QuotaCalculatorModel *quotaCalculatorData;
    
    // Quota Calculator feature exist
    BOOL haveQuotaCalculator;
    
    // TODO : NEPTUNE
    BOOL isPaketSukaSuka;
    PackageSukaMatrixModel *matrixSukaSukaData;
//    FacebookModel *fbData;
    MappingSukaSukaModel *mappingSukaSukaData;
    // TODO : Hygiene
    // Used for displaying msisdn label on after request password
    NSString *tempMsisdn;
    LoginWordingModel *loginWordingData;
    
    // For Notification
    BOOL isRemoteNotificationAvailable;
    BOOL isRemoteNotificationShown;
    NSDictionary *remoteNotificationInfo;
    NSString *deviceToken;
    FacebookModel *fbData;
    RenameSukaSukaModel *renameSukaSukaData;
    
    // For 4G USIM
    MenuModel *package4gMenu;
    int ratingValue;
    NSString *commentValue;
    
    // 4G Powerpack
    NSArray *featureMenu;
    
    // TODO : v1.9.5
    QuotaImprovementModel *quotaImprovementData;
}

@property (nonatomic, retain) NSDictionary *iconData;

@property (nonatomic, retain) ProfileModel *profileData;
@property (nonatomic, retain) NSDictionary *menuData;
@property (nonatomic, retain) NSArray *homeMenu;
@property (nonatomic, retain) NSArray *sideMenu;
@property (nonatomic, retain) NSArray *balanceMenu;

@property (nonatomic, retain) CheckBalanceModel *checkBalanceData;
@property (nonatomic, retain) TopUpVoucherModel *topUpVoucherModel;
@property (nonatomic, retain) ExtendValidityModel *extendValidityData;
@property (nonatomic, retain) BuyPackageModel *buyPackageData;
@property (nonatomic, retain) NSArray *checkUsageData;
@property (nonatomic, retain) NSArray *checkUsageXLData;
@property (nonatomic, retain) NSArray *recommendedData;
@property (nonatomic, retain) NSArray *upgradePackageData;
@property (nonatomic, retain) NSArray *surveyData;
@property (nonatomic, retain) NSArray *promoData;
@property (nonatomic, retain) NSArray *info4gData;

@property (nonatomic, retain) NSArray *notificationData;
@property (nonatomic, retain) NotifModel *detailNotificationData;
@property (nonatomic, retain) NSString *replyNotifDesc;

@property (nonatomic, retain) NSArray *lastTransactionData;
@property (nonatomic, retain) NSString *parentId;
@property (nonatomic, retain) MenuModel *menuModel;
@property (nonatomic, retain) NSString *cid;
@property (nonatomic, retain) UINavigationController *navController;

@property (nonatomic, retain) NSArray *adsData;

@property (nonatomic, retain) NSArray *balanceItemsData;
@property (nonatomic, retain) BalanceTransferModel *balanceTransferData;

@property (nonatomic, retain) NSDictionary *paymentParamData;
@property (nonatomic, retain) NSString *paymentHelpData;
@property (nonatomic, retain) PaymentDebitModel *paymentDebitData;

@property (nonatomic, retain) NSArray *contactUsData;
@property (nonatomic, retain) NSArray *roamingData;
@property (nonatomic, retain) PUKModel *pukData;
@property (nonatomic, retain) NSDictionary *deviceSettingData;
@property (nonatomic, retain) NSArray *shopLocationData;
@property (nonatomic, retain) NSArray *faqData;

@property (nonatomic, retain) LabelValueModel *termAndConditionData;
@property (nonatomic, retain) LabelValueModel *userGuideData;
@property (nonatomic, retain) NSArray *trxHistoryData;
@property (nonatomic, retain) NSArray *categoryData;
@property (nonatomic, retain) NSArray *productData;

@property (nonatomic, retain) NSArray *dropboxFileData;

@property (readwrite) int unread;
@property (nonatomic, retain) NSString *currentVersion;

@property (nonatomic, retain) MenuModel *packageMenu;
@property (nonatomic, retain) MenuModel *xlstarMenu;
@property (nonatomic, retain) LabelValueModel *xlStarData;

//@property (nonatomic, retain) NSDictionary *notifDetailData;
@property (nonatomic, retain) CheckBalanceModel *xlTunaiBalanceData;

@property (nonatomic, retain) NSArray *hotOfferBannerData;

@property (nonatomic, retain) NSArray *leftMenuData;

@property (readwrite) BOOL dropdownMenuCreated;

@property (readwrite) BOOL showBalance;
@property (readwrite) BOOL showBanner;

@property (readwrite) BOOL isPostpaid;

@property (readwrite) int unreadMsg;

@property (nonatomic, retain) NSArray *quotaMeterData;

@property (readwrite) BOOL showBannerLowQuotaMeter;

@property (nonatomic, retain) NSArray *recommendedPackageXLData;

@property (nonatomic, retain) MenuModel *paymentMethodMenu;

@property (nonatomic, retain) QuotaCalculatorModel *quotaCalculatorData;

@property (readwrite) BOOL haveQuotaCalculator;

@property (readwrite) BOOL isPaketSukaSuka;

@property (nonatomic, retain) MappingSukaSukaModel *mappingSukaSukaData;

@property (nonatomic, retain) PackageSukaMatrixModel *matrixSukaSukaData;

//@property (nonatomic, retain) FacebookModel *fbData;

@property (nonatomic, retain) LoginWordingModel *loginWordingData;

@property (nonatomic, retain) NSString *tempMsisdn;

@property (nonatomic, retain) NSDictionary *remoteNotificationInfo;
@property (nonatomic,assign) BOOL isRemoteNotificationAvailable;
@property (nonatomic,assign) BOOL isRemoteNotificationShown;
@property (nonatomic, retain) NSString *deviceToken;

@property (nonatomic, retain) FacebookModel *fbData;
@property (nonatomic, retain) PageSukaModel *pageSukaSukaData;
@property (nonatomic, retain) RenameSukaSukaModel *renameSukaSukaData;

@property (nonatomic, retain) MenuModel *package4gMenu;
@property (readwrite) int ratingValue;
@property (nonatomic, retain) NSString *commentValue;

// TODO : V1.9
/**
 *  Update Version Object containing version, code, message, and URL
 */
@property (nonatomic, retain) UpdateVersionModel *updateVersionModel;

@property (nonatomic, retain) CheckPointModel *checkPointData;
@property (nonatomic, retain) XtraCheckBonusModel *checkBonusModel;

@property (nonatomic, retain) NSArray *winnerListData;
@property (nonatomic, copy) NSString *winnerMessage;

// To avoid sending device token whenever the user views the home page
@property (readwrite) BOOL shouldUpdateDeviceToken;

@property (nonatomic, retain) FourGPackageModel *fourGModel;
@property (nonatomic, readwrite) BOOL isFourGPackage;
@property (nonatomic, readwrite) BOOL isFourGSim;
@property (nonatomic, readwrite) BOOL isFourGHandset;

// TODO : 4G Powerpack
@property (nonatomic, retain) NSArray *featureMenu;

// TODO : v1.9.5
@property (nonatomic, retain) QuotaImprovementModel *quotaImprovementData;

+ (id)sharedInstance;

@end
