//
//  DataManager.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/14/11.
//  Copyright (c) 2012 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "DataManager.h"

static DataManager *sharedInstance = nil;

@implementation DataManager

@synthesize iconData;

@synthesize profileData;
@synthesize menuData;
@synthesize homeMenu;
@synthesize sideMenu;
@synthesize balanceMenu;

@synthesize checkBalanceData;
@synthesize topUpVoucherModel;
@synthesize extendValidityData;
@synthesize buyPackageData;
@synthesize checkUsageData;
@synthesize checkUsageXLData;
@synthesize recommendedData;
@synthesize upgradePackageData;
@synthesize surveyData;
@synthesize promoData;
@synthesize info4gData;

@synthesize notificationData;
@synthesize detailNotificationData;
@synthesize replyNotifDesc;

@synthesize lastTransactionData;
@synthesize parentId;
@synthesize menuModel;
@synthesize cid;
@synthesize navController;

@synthesize adsData;

@synthesize balanceItemsData;
@synthesize balanceTransferData;

@synthesize paymentParamData;
@synthesize paymentHelpData;
@synthesize paymentDebitData;

@synthesize contactUsData;
@synthesize roamingData;
@synthesize pukData;
@synthesize deviceSettingData;
@synthesize shopLocationData;
@synthesize faqData;

@synthesize termAndConditionData;
@synthesize userGuideData;
@synthesize trxHistoryData;
@synthesize categoryData;
@synthesize productData;

@synthesize dropboxFileData;

@synthesize unread;
@synthesize currentVersion;

@synthesize packageMenu;
@synthesize xlstarMenu;
@synthesize xlStarData;

//@synthesize notifDetailData;
@synthesize xlTunaiBalanceData;

@synthesize dropdownMenuCreated;

@synthesize hotOfferBannerData;

@synthesize showBalance;
@synthesize showBanner;

@synthesize leftMenuData;

@synthesize isPostpaid;

@synthesize unreadMsg;

@synthesize quotaMeterData;

@synthesize showBannerLowQuotaMeter;

@synthesize recommendedPackageXLData;

@synthesize paymentMethodMenu;

@synthesize quotaCalculatorData;

@synthesize haveQuotaCalculator;

// TODO : NEPTUNE
@synthesize isPaketSukaSuka;
@synthesize mappingSukaSukaData;
@synthesize matrixSukaSukaData;
//@synthesize fbData;

// TODO : Hygiene
@synthesize tempMsisdn;
@synthesize loginWordingData;
@synthesize remoteNotificationInfo;
@synthesize isRemoteNotificationAvailable;

@synthesize renameSukaSukaData, pageSukaSukaData;

@synthesize package4gMenu;
@synthesize ratingValue;
@synthesize commentValue;

@synthesize featureMenu;

// TODO : v1.9.5
@synthesize quotaImprovementData;

// Get the shared instance and create it if necessary.
+ (DataManager*)sharedInstance {
	if (sharedInstance == nil) {
		sharedInstance = [[super allocWithZone:NULL] init];
	}
	return sharedInstance;
}

// We don't want to allocate a new instance, so return the current one.
+ (id)allocWithZone:(NSZone*)zone {
	return [[self sharedInstance] retain];
}

// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone {
	return self;
}

// Once again - do nothing, as we don't have a retain counter for this object.
- (id)retain {
	return self;
}

// Replace the retain counter so we can never release this object.
- (NSUInteger)retainCount {
	return NSUIntegerMax;
}

// This function is empty, as we don't want to let the user release this object.
- (oneway void)release {
	
}

//Do nothing, other than return the shared instance - as this is expected from autorelease.
- (id)autorelease {
	return self;
}

- (void)dealloc {
	// Should never be called, but just here for clarity really.
    
    [iconData release];
    
    [profileData release];
    [menuData release];
    [homeMenu release];
    [sideMenu release];
    [balanceMenu release];
    
    [checkBalanceData release];
    [topUpVoucherModel release];
    [extendValidityData release];
    [buyPackageData release];
    [checkUsageData release];
    [checkUsageXLData release];
    [recommendedData release];
    [upgradePackageData release];
    [surveyData release];
    [promoData release];
    [info4gData release];
    
    [notificationData release];
    [detailNotificationData release];
    [replyNotifDesc release];
    
    [lastTransactionData release];
    [parentId release];
    [menuModel release];
    [cid release];
    [navController release];
    
    [adsData release];
    
    [balanceItemsData release];
    [balanceTransferData release];
    
    [paymentParamData release];
    [paymentHelpData release];
    [paymentDebitData release];
    
    [contactUsData release];
    [roamingData release];
    [pukData release];
    [deviceSettingData release];
    [shopLocationData release];
    [faqData release];
    
    [termAndConditionData release];
    [userGuideData release];
    [trxHistoryData release];
    [categoryData release];
    [productData release];
    
    [dropboxFileData release];
    
    [currentVersion release];
    
    [packageMenu release];
    [xlstarMenu release];
    [xlStarData release];
    
    //[notifDetailData release];
    [xlTunaiBalanceData release];
    
    [hotOfferBannerData release];
    
    [leftMenuData release];
    
    [quotaMeterData release];
    
    [recommendedPackageXLData release];
    
    [paymentMethodMenu release];
    
    [quotaCalculatorData release];
    
    [mappingSukaSukaData release];

    [tempMsisdn release];
    
    [loginWordingData release];
    [matrixSukaSukaData release];
    
    [remoteNotificationInfo release];
    
//    [fbData release];
    
    [pageSukaSukaData release];
    
    [renameSukaSukaData release];
    
    [package4gMenu release];
    [commentValue release];
    
    [featureMenu release];
    
    [quotaImprovementData release];
    
	[super dealloc];
}

@end
