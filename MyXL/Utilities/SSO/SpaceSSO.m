//
//  SpaceSSO.m
//  Aconnect
//
//  Created by Tony Hadisiswanto on 12/13/11.
//  Copyright (c) 2011 Link IT. All rights reserved.
//

#import "SpaceSSO.h"
#import "EncryptDecrypt.h"
#import "JSON/JSON.h"
#import "UniqueString.h"
#import "Macros.h"
#import "DataManager.h"

@implementation SpaceSSO

@synthesize username;
@synthesize password;
@synthesize receiveData;
@synthesize delegate;
@synthesize callback;
@synthesize errorCallback;

#pragma mark - EncryptDecrypt

- (NSData*)modifiedBase64DecodeSpace:(NSString*)inputText {
    NSArray *search = [NSArray arrayWithObjects:@"-", @"_", nil];
    NSArray *replace = [NSArray arrayWithObjects:@"+", @"/", nil];
    for (int i=0; i<2; i++) {
        inputText = [inputText stringByReplacingOccurrencesOfString:[search objectAtIndex:i] withString:[replace objectAtIndex:i]];
    }
    [Base64 initialize];
    NSData *b64DecData = [Base64 decode:inputText];
    return b64DecData;
}

- (NSString*)modifiedBase64EncodeSpace:(NSData*)inputData {
    [Base64 initialize];
    NSString *b64EncStr = [Base64 encode:inputData];
    //NSLog(@"b64EncStr = %@",b64EncStr);
    NSArray *search = [NSArray arrayWithObjects:@"+", @"/", nil];
    NSArray *replace = [NSArray arrayWithObjects:@"-", @"_", nil];
    NSString *result;
    NSString *inputStr = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];
    //NSLog(@"input = %@",inputStr);
    result = b64EncStr;
    for (int i=0; i<2; i++) {
        result = [result stringByReplacingOccurrencesOfString:[search objectAtIndex:i] withString:[replace objectAtIndex:i]];
        //NSLog(@"replaced = %@",result);
    }
    result = [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //NSLog(@"output = %@",result);
    [inputStr release];
    return result;
}

- (NSString*)removeNonASCII:(NSString*)inputStr {
    //NSString *test = @"Olé, señor!";
    NSString *output;
    
    NSMutableString *asciiCharacters = [NSMutableString string];
    for (NSInteger i = 32; i < 127; i++)  {
        [asciiCharacters appendFormat:@"%c", i];
    }
    
    NSCharacterSet *nonAsciiCharacterSet = [[NSCharacterSet characterSetWithCharactersInString:asciiCharacters] invertedSet];
    
    output = [[inputStr componentsSeparatedByCharactersInSet:nonAsciiCharacterSet] componentsJoinedByString:@""];
    
    //NSLog(@"%@", output); // Prints @"Ol, seor!"
    return output;
}

- (NSString*)doCipherSpace:(NSString*)plainText action:(CCOperation)encryptOrDecrypt withKey:(NSString*)saltKey {
    const void *vplainText;
    size_t plainTextBufferSize;
    
    //[plainText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (encryptOrDecrypt == kCCDecrypt) {
        NSData *EncryptData = [self modifiedBase64DecodeSpace:plainText];
        plainTextBufferSize = [EncryptData length];
        vplainText = [EncryptData bytes];
    }
    else {
        //
        int plainTextLength = [plainText length];
        int divideResult = plainTextLength/8;
        //int modResult = plainTextLength % 8;
        //int addChar = 8 - modResult;
        
        char bytes[] = "";
        NSData * data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
        NSString *strFromData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        plainText = [plainText stringByPaddingToLength:(divideResult+1)*8 withString:strFromData startingAtIndex:0];
        //NSLog(@"plain = %@",plainText);
        //NSLog(@"plain length = %i",[plainText length]);
        //
        NSData *plainTextData = [plainText dataUsingEncoding:NSUTF8StringEncoding];
        plainTextBufferSize = [plainTextData length]; 
        vplainText = [plainTextData bytes];
        [strFromData release];
    }
    
    CCCryptorStatus ccStatus;
    uint8_t *bufferPtr = NULL;
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    
    bufferPtrSize = (plainTextBufferSize + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    bufferPtr = malloc( bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x0, bufferPtrSize);
    
    NSString *key = saltKey;
    const void *vkey = (const void *)[key UTF8String];
    
    ccStatus = CCCrypt(encryptOrDecrypt,
                       kCCAlgorithm3DES,
                       kCCOptionECBMode,
                       vkey,
                       kCCKeySize3DES,
                       nil,
                       vplainText,
                       plainTextBufferSize,
                       (void *)bufferPtr,
                       bufferPtrSize,
                       &movedBytes);
    
    if (ccStatus == kCCSuccess) NSLog(@"SUCCESS");
    else if (ccStatus == kCCParamError) return @"PARAM ERROR";
    else if (ccStatus == kCCBufferTooSmall) return @"BUFFER TOO SMALL";
    else if (ccStatus == kCCMemoryFailure) return @"MEMORY FAILURE";
    else if (ccStatus == kCCAlignmentError) return @"ALIGNMENT";
    else if (ccStatus == kCCDecodeError) return @"DECODE ERROR";
    else if (ccStatus == kCCUnimplemented) return @"UNIMPLEMENTED";
    
    NSString *result;
    
    if (encryptOrDecrypt == kCCDecrypt) {
        result = [[[NSString alloc] initWithData:[NSData dataWithBytes:(const void *)bufferPtr length:(NSUInteger)movedBytes] encoding:NSASCIIStringEncoding] autorelease];
        //result = [[[NSString alloc] initWithData:[NSData dataWithBytes:(const void *)bufferPtr length:(NSUInteger)movedBytes] encoding:NSUTF8StringEncoding] autorelease];
        //NSLog(@"## result length = %i",[result length]);
        //[result stringByReplacingOccurrencesOfString:@" " withString:@""];
        [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        //NSLog(@"## result length = %i",[result length]);
        //NSLog(@"movedBytes %i",(NSUInteger)movedBytes);
        //NSLog(@"movedBytes %i",(NSUInteger)movedBytes);
        
        result = [self removeNonASCII:result];
        //NSLog(@"## result length = %i",[result length]);
    }
    else {
        NSData *myData = [NSData dataWithBytes:(const void *)bufferPtr length:(NSUInteger)movedBytes];
        NSString *str = [[NSString alloc] initWithData:myData encoding:NSUTF8StringEncoding];
        //NSLog(@"str = %@",str);
        result = [self modifiedBase64EncodeSpace:myData];
        [str release];
    }
    return result;
}

#pragma mark - Selector

// Register
- (void)spaceSSORegister:(NSString*)secretKey
             withSaltKey:(NSString*)saltKey
            withUsername:(NSString*)uname
            withPassword:(NSString*)pwd
              withMsisdn:(NSString*)msisdn
             withCaptcha:(NSString*)captcha
                 withCid:(NSString*)cid
            withFullname:(NSString*)fullname
               withEmail:(NSString*)email
            withLanguage:(NSString*)language
                delegate:(id)requestDelegate
         requestSelector:(SEL)requestSelector {
    isPost = YES;
    isSynchronous = YES;
    self.delegate = requestDelegate;
	self.callback = requestSelector;
    
    NSString *usernameEncrypted = [self doCipherSpace:uname action:kCCEncrypt withKey:saltKey];
    NSString *passwordEncrypted = [self doCipherSpace:pwd action:kCCEncrypt withKey:saltKey];
    NSString *msisdnEncrypted   = [self doCipherSpace:msisdn action:kCCEncrypt withKey:saltKey];
    NSString *fullNameEncrypted = [self doCipherSpace:fullname action:kCCEncrypt withKey:saltKey];
    NSString *emailEncrypted = [self doCipherSpace:email action:kCCEncrypt withKey:saltKey];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 usernameEncrypted, @"nickname",
                                 msisdnEncrypted, @"msisdn",
                                 passwordEncrypted, @"password",
                                 captcha,@"captcha",
                                 cid,@"cid",
                                 fullNameEncrypted,@"fullname",
                                 emailEncrypted,@"email",
                                 language,@"language",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    requestBody = [NSString stringWithFormat:@"jsondata=%@",jsonString];
    //NSLog(@"requestBody register = %@",requestBody);
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *host = sharedData.hostSSO;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@register",host];
    NSURL *url = [NSURL URLWithString:urlStr];
    
	[self request:url];
}

//Get Profile
- (void)spaceSSOGetProfile:(NSString*)secretKey
                 delegate:(id)requestDelegate
          requestSelector:(SEL)requestSelector {
    isPost = YES;
    isSynchronous = YES;
    self.delegate = requestDelegate;
	self.callback = requestSelector;
    
    NSString *deviceBrand = DEVICE_BRAND;
    UIDevice *dev = [UIDevice currentDevice];
    NSString *deviceModel = dev.model;

    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 deviceBrand, @"brand",
                                 deviceModel, @"model",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    requestBody = [NSString stringWithFormat:@"jsondata=%@",jsonString];
    NSLog(@"requestBody get profile = %@",requestBody);
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *host = sharedData.hostSSO;
    //NSString *host = HOST_SSO;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@getprofile",host];
    NSLog(@"urlStr Get Profile = %@",urlStr);
    NSURL *url = [NSURL URLWithString:urlStr];
    
	[self request:url];
}

// Sign In
- (void)spaceSSOSignIn:(NSString*)secretKey
           withSaltKey:(NSString*)saltKey
          withUsername:(NSString*)uname
          withPassword:(NSString*)pwd
              delegate:(id)requestDelegate
       requestSelector:(SEL)requestSelector {
    isPost = YES;
    isSynchronous = YES;
    self.delegate = requestDelegate;
	self.callback = requestSelector;
    
    NSString *usernameEncrypted = [self doCipherSpace:uname action:kCCEncrypt withKey:saltKey];
    NSString *passwordEncrypted = [self doCipherSpace:pwd action:kCCEncrypt withKey:saltKey];
    
    NSString *deviceBrand = DEVICE_BRAND;
    UIDevice *dev = [UIDevice currentDevice];
    NSString *deviceModel = dev.model;
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 usernameEncrypted, @"msisdn",
                                 passwordEncrypted, @"password",
                                 deviceBrand, @"brand",
                                 deviceModel, @"model",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    requestBody = [NSString stringWithFormat:@"jsondata=%@",jsonString];
    //NSLog(@"requestBody sign in = %@",requestBody);
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *host = sharedData.hostSSO;
    //NSString *host = HOST_SSO;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@signin",host];
    NSURL *url = [NSURL URLWithString:urlStr];
    
	[self request:url];
}

// Sign Out
- (void)spaceSSOSignOut:(NSString*)secretKey
              withToken:(NSString*)token
               delegate:(id)requestDelegate
        requestSelector:(SEL)requestSelector {
    isPost = YES;
    isSynchronous = YES;
    self.delegate = requestDelegate;
	self.callback = requestSelector;
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 token, @"token",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    requestBody = [NSString stringWithFormat:@"jsondata=%@",jsonString];
    //NSLog(@"requestBody sign out = %@",requestBody);
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *host = sharedData.hostSSO;
    //NSString *host = HOST_SSO;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@signout",host];
    NSURL *url = [NSURL URLWithString:urlStr];
    
	[self request:url];
}

//Has Privilege
- (void)spaceSSOHasPrivilege:(NSString*)secretKey
                   withToken:(NSString*)token
               withPrivilege:(NSArray*)privilege
                    delegate:(id)requestDelegate
             requestSelector:(SEL)requestSelector {
    isPost = YES;
    isSynchronous = YES;
    self.delegate = requestDelegate;
	self.callback = requestSelector;
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 token, @"token",
                                 privilege, @"privilege",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    requestBody = [NSString stringWithFormat:@"jsondata=%@",jsonString];
    //NSLog(@"requestBody privilege = %@",requestBody);
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *host = sharedData.hostSSO;
    //NSString *host = HOST_SSO;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@privilege",host];
    NSURL *url = [NSURL URLWithString:urlStr];
    
	[self request:url];
}

//Activate
- (void)spaceSSOActivate:(NSString*)secretKey
             withSaltKey:(NSString*)saltKey
              withMsisdn:(NSString*)msisdn
                 withPin:(NSString*)pin
                delegate:(id)requestDelegate
         requestSelector:(SEL)requestSelector {
    isPost = YES;
    isSynchronous = YES;
    self.delegate = requestDelegate;
	self.callback = requestSelector;
    
    NSString *msisdnEncrypted   = [self doCipherSpace:msisdn action:kCCEncrypt withKey:saltKey];
    NSString *pinEncrypted = [self doCipherSpace:pin action:kCCEncrypt withKey:saltKey];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 msisdnEncrypted, @"msisdn",
                                 pinEncrypted, @"pin",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    requestBody = [NSString stringWithFormat:@"jsondata=%@",jsonString];
    //NSLog(@"requestBody activate = %@",requestBody);
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *host = sharedData.hostSSO;
    //NSString *host = HOST_SSO;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@activate",host];
    NSURL *url = [NSURL URLWithString:urlStr];
    
	[self request:url];
}

//Resend PIN
- (void)spaceSSOResendPin:(NSString*)secretKey
              withSaltKey:(NSString*)saltKey
               withMsisdn:(NSString*)msisdn
              withCaptcha:(NSString*)captcha
                  withCid:(NSString*)cid
                 delegate:(id)requestDelegate
          requestSelector:(SEL)requestSelector {
    isPost = YES;
    isSynchronous = YES;
    self.delegate = requestDelegate;
	self.callback = requestSelector;
    
    NSString *msisdnEncrypted = [self doCipherSpace:msisdn action:kCCEncrypt withKey:saltKey];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *lang   = [prefs stringForKey:@"language"];
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 msisdnEncrypted, @"msisdn",
                                 cid,@"cid",
                                 captcha,@"captcha",
                                 lang,@"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    requestBody = [NSString stringWithFormat:@"jsondata=%@",jsonString];
    //NSLog(@"requestBody resend PIN = %@",requestBody);
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *host = sharedData.hostSSO;
    //NSString *host = HOST_SSO;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@pin/resent",host];
    NSURL *url = [NSURL URLWithString:urlStr];
    
	[self request:url];
}

//Reset Password
- (void)spaceSSOResetPassword:(NSString*)secretKey
                  withSaltKey:(NSString*)saltKey
                 withUsername:(NSString*)uname
                  withCaptcha:(NSString*)captcha
                      withCid:(NSString*)cid
                    withToken:(NSString*)token
                     delegate:(id)requestDelegate
              requestSelector:(SEL)requestSelector {
    isPost = YES;
    isSynchronous = YES;
    self.delegate = requestDelegate;
	self.callback = requestSelector;
    
    NSString *usernameEncrypted = [self doCipherSpace:uname action:kCCEncrypt withKey:saltKey];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *lang   = [prefs stringForKey:@"language"];
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 usernameEncrypted, @"msisdn",
                                 cid,@"cid",
                                 captcha,@"captcha",
                                 lang,@"lang",
                                 token,@"token",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    requestBody = [NSString stringWithFormat:@"jsondata=%@",jsonString];
    //NSLog(@"requestBody reset password = %@",requestBody);
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *host = sharedData.hostSSO;
    //NSString *host = HOST_SSO;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@password/reset",host];
    NSURL *url = [NSURL URLWithString:urlStr];
    
	[self request:url];
}

//Change Password
- (void)spaceSSOChangePassword:(NSString*)secretKey
                   withSaltKey:(NSString*)saltKey
                     withToken:(NSString*)token
                  withPassword:(NSString*)pwd
               withNewPassword:(NSString*)newPwd
         withNewPasswordRetype:(NSString*)newPwdRetype
                      delegate:(id)requestDelegate
               requestSelector:(SEL)requestSelector {
    isPost = YES;
    isSynchronous = YES;
    self.delegate = requestDelegate;
	self.callback = requestSelector;
    

    NSString *passwordEncrypted = [self doCipherSpace:pwd action:kCCEncrypt withKey:saltKey];
    NSString *newPasswordEncrypted = [self doCipherSpace:newPwd action:kCCEncrypt withKey:saltKey];
    NSString *newPasswordRetypeEncrypted = [self doCipherSpace:newPwdRetype action:kCCEncrypt withKey:saltKey];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 token, @"token",
                                 passwordEncrypted, @"password",
                                 newPasswordEncrypted, @"newpassword",
                                 newPasswordRetypeEncrypted, @"repassword",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    requestBody = [NSString stringWithFormat:@"jsondata=%@",jsonString];
    //NSLog(@"requestBody change password = %@",requestBody);
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *host = sharedData.hostSSO;
    //NSString *host = HOST_SSO;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@password/change",host];
    NSURL *url = [NSURL URLWithString:urlStr];
    
	[self request:url];
}

//Register Device Token for Push Notification
- (void)spaceSSORegDeviceToken:(NSString*)devOS
                  withDevToken:(NSString*)devToken
                      withLang:(NSString*)language
                 withSecretKey:(NSString*)secretKey
                   withSaltKey:(NSString*)saltKey
                     withToken:(NSString*)token
                    withMsisdn:(NSString*)msisdn
                      delegate:(id)requestDelegate 
               requestSelector:(SEL)requestSelector {
    isPost = YES;
    isSynchronous = YES;
    self.delegate = requestDelegate;
	self.callback = requestSelector;
    
    NSString *msisdnEncrypted = [self doCipherSpace:msisdn action:kCCEncrypt withKey:saltKey];
    
    NSDictionary *requestData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 secretKey, @"secret_key",
                                 token, @"token",
                                 msisdnEncrypted, @"msisdn",
                                 devOS, @"device_os",
                                 devToken,@"device_token",
                                 language,@"lang",
                                 nil];
    
    NSString *jsonString = [requestData JSONRepresentation];
    requestBody = [NSString stringWithFormat:@"jsondata=%@",jsonString];
    //NSLog(@"requestBody reset password = %@",requestBody);
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *host = sharedData.hostSSO;
    //NSString *host = HOST_SSO;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@subs/devicetoken",host];
    
    //testing
    //urlStr = [NSString stringWithFormat:@"http://net.axisworld.co.id/ssodev/subs/devicetoken"];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    
	[self request:url];
}

#pragma mark -
#pragma mark Start Request

- (void)request:(NSURL *)url {
	theRequest = [[NSMutableURLRequest alloc] initWithURL:url];
	
	if(isPost) {
		//NSLog(@"is post");
		[theRequest setHTTPMethod:@"POST"];
        //[theRequest setValue:@"application/json" forHTTPHeaderField:@"Content-type"]; //NSASCIIStringEncoding
		[theRequest setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]]; //NSUTF8StringEncoding
	}
	//NSLog(@"request = %@",theRequest);
    
    if (isSynchronous) {
        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&error];
        
        if (delegate && callback) {
            if ([delegate respondsToSelector:self.callback]) {
                [delegate performSelector:self.callback withObject:data];
            }
            else {
                //NSLog(@"No response from delegate");
            }
        }
        
        [theRequest release];
    }
    else {
        theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if (theConnection){
            receiveData = [[NSMutableData data] retain];
        }
        else {
            //NSLog(@"ga bisa donlot cuy");
        }
    }
}

@end
