//
//  SpaceSSO.h
//  Aconnect
//
//  Created by Tony Hadisiswanto on 12/13/11.
//  Copyright (c) 2011 Link IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Base64.h"
#include <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>

//#define HOST_SSO @"http://10.22.51.43/axis-sso/"

//#define HOST_SSO_AXIS   @"http://axis.net/sso/"
//#define HOST_SSO_OTHERS @"http://203.78.115.115/sso/"
//#define HOST_SSO_OTHERS @"http://app.axisworld.co.id/sso/"

// --- Production ---
#define HOST_SSO_OTHERS @"http://net.axisworld.co.id/sso/"
#define HOST_SSO_AXIS   @"http://net.axisworld.co.id/sso/"
// --- Production ---

//#define HOST_SSO_AXIS   @"http://app.axisworld.co.id/sso/"
//#define HOST_SSO_AXIS   @"http://10.8.0.136/sso/"
//#define HOST_SSO_OTHERS @"http://10.8.0.136/sso/"

// a68f5115d5c8e97e9cf464cb3317166a <-- secret_key
// 729fb41ca00f7cb82d6d8a22ea7ab14d <-- salt_key

// Staging
/*
#define HOST_SSO_OTHERS @"http://net.axisworld.co.id/sso_stg/"
#define HOST_SSO_AXIS   @"http://net.axisworld.co.id/sso_stg/"*/

// Development
//#define HOST_SSO_OTHERS @"http://net.axisworld.co.id/ssodev/"
//#define HOST_SSO_AXIS   @"http://net.axisworld.co.id/ssodev/"

@interface SpaceSSO : NSObject {
    NSString *username;
	NSString *password;
	NSMutableData *receiveData;
	NSMutableURLRequest *theRequest;
	NSURLConnection *theConnection;
	id delegate;
	SEL callback;
	SEL errorCallback;
	BOOL isPost;
    BOOL isSynchronous;
	NSString *requestBody;
}

@property (nonatomic, retain) NSString *username;
@property (nonatomic, retain) NSString *password;
@property (nonatomic, retain) NSMutableData *receiveData;
@property (nonatomic, retain) id delegate;
@property (nonatomic) SEL callback;
@property (nonatomic) SEL errorCallback;

- (NSData*)modifiedBase64DecodeSpace:(NSString*)inputText;
- (NSString*)modifiedBase64EncodeSpace:(NSData*)inputData;
- (NSString*)doCipherSpace:(NSString*)plainText action:(CCOperation)encryptOrDecrypt withKey:(NSString*)saltKey;
- (NSString*)removeNonASCII:(NSString*)inputStr;

// Request
- (void)request:(NSURL *)url;

//Register
- (void)spaceSSORegister:(NSString*)secretKey
             withSaltKey:(NSString*)saltKey
            withUsername:(NSString*)uname
            withPassword:(NSString*)pwd
              withMsisdn:(NSString*)msisdn
             withCaptcha:(NSString*)captcha
                 withCid:(NSString*)cid
            withFullname:(NSString*)fullname
               withEmail:(NSString*)email
            withLanguage:(NSString*)language
                delegate:(id)requestDelegate
         requestSelector:(SEL)requestSelector;

//Get Profile
- (void)spaceSSOGetProfile:(NSString*)secretKey
                 delegate:(id)requestDelegate
          requestSelector:(SEL)requestSelector;

//Sign In
- (void)spaceSSOSignIn:(NSString*)secretKey
           withSaltKey:(NSString*)saltKey
          withUsername:(NSString*)uname
          withPassword:(NSString*)pwd
              delegate:(id)requestDelegate
       requestSelector:(SEL)requestSelector;

//Sign Out
- (void)spaceSSOSignOut:(NSString*)secretKey
              withToken:(NSString*)token
               delegate:(id)requestDelegate
        requestSelector:(SEL)requestSelector;

//Has Privilege
- (void)spaceSSOHasPrivilege:(NSString*)secretKey
                   withToken:(NSString*)token
               withPrivilege:(NSArray*)privilege
                    delegate:(id)requestDelegate
             requestSelector:(SEL)requestSelector;

//Activate
- (void)spaceSSOActivate:(NSString*)secretKey
             withSaltKey:(NSString*)saltKey
              withMsisdn:(NSString*)msisdn
                 withPin:(NSString*)pin
                delegate:(id)requestDelegate
         requestSelector:(SEL)requestSelector;

//Resend PIN
- (void)spaceSSOResendPin:(NSString*)secretKey
              withSaltKey:(NSString*)saltKey
               withMsisdn:(NSString*)msisdn
              withCaptcha:(NSString*)captcha
                  withCid:(NSString*)cid
                 delegate:(id)requestDelegate
          requestSelector:(SEL)requestSelector;

//Reset Password
- (void)spaceSSOResetPassword:(NSString*)secretKey
                  withSaltKey:(NSString*)saltKey
                 withUsername:(NSString*)uname
                  withCaptcha:(NSString*)captcha
                      withCid:(NSString*)cid
                    withToken:(NSString*)token
                     delegate:(id)requestDelegate
              requestSelector:(SEL)requestSelector;

//Change Password
- (void)spaceSSOChangePassword:(NSString*)secretKey
                   withSaltKey:(NSString*)saltKey
                     withToken:(NSString*)token
                  withPassword:(NSString*)pwd
               withNewPassword:(NSString*)newPwd
         withNewPasswordRetype:(NSString*)newPwdRetype
                      delegate:(id)requestDelegate
               requestSelector:(SEL)requestSelector;

//Register Device Token for Push Notification
- (void)spaceSSORegDeviceToken:(NSString*)devOS
                  withDevToken:(NSString*)devToken
                      withLang:(NSString*)language
                 withSecretKey:(NSString*)secretKey
                   withSaltKey:(NSString*)saltKey
                     withToken:(NSString*)token
                    withMsisdn:(NSString*)msisdn
                      delegate:(id)requestDelegate 
               requestSelector:(SEL)requestSelector;


@end
