function rotateThis(target,value,speed){
	if(value <= 25){
		var degree = ((value * 3.6)/2) + 45
		target.animate({
			rotate: degree+'deg'
		},speed,'easeOutQuad')
		return false
	}
	if(value <= 50){
		var value1 = 90
		var value2 = value - 25
		var degree = value2 * 3.6
		var lastSpeed = (speed * (degree/90))*2
		target.animate({
			rotate: value1+'deg'
		},speed, 'linear', function(){
			target.hide()
			target.next().animate({
				rotate: degree+'deg'
			},lastSpeed, 'easeOutQuad')
		})
		return false
	}
	if(value <= 75){
		var value1 = 90
		var value2 = value - 50
		var degree = value2 * 3.6
		var lastSpeed = (speed * (degree/90))*2
		target.animate({
			rotate: value1+'deg'
		},speed, 'linear', function(){
			target.hide()
			target.next().animate({
				rotate: value1+'deg'
			},speed, 'linear', function(){
				target.next().hide()
				target.next().next().animate({
					rotate: degree+'deg'
				},lastSpeed, 'easeOutQuad')
			})
		})
		return false
	}
	if(value <= 100){
		var value1 = 90
		var value2 = value - 75
		var degree =(value2 * 3.6)/2
		var lastSpeed = (speed * (degree/90))*2
		target.animate({
			rotate: value1+'deg'
		},speed, 'linear', function(){
			target.hide()
			target.next().animate({
				rotate: value1+'deg'
			},speed, 'linear', function(){
				target.next().hide()
				target.next().next().animate({
					rotate: value1+'deg'
				},speed, 'linear', function(){
					target.next().next().hide()
					target.next().next().next().animate({
						rotate: degree+'deg'
					},lastSpeed, 'easeOutQuad')
				})
			})
		})
		return false
	}
}
function rotateArrow(target,value,speed){
	if(value <= 100){
		var degree = 225 + ((((value - 75)/2) * 3.6))
	}
	if(value <= 75){
		var degree = 135 + ((value - 50) * 3.6)
	}
	if(value <= 50){
		var degree = 45 + ((value - 25) * 3.6)
	}
	if(value <= 25){
		var degree = ((value * 3.6)/2)
	}
	if(target.hasClass('arrow')){
		$('.usagetext').css({
			rotate: '-62deg'
			}).animate({
				rotate: (value - 42 )+'deg'
		}, 800*4)
	}
	if(target.hasClass('narrow')){
		$('.nusagetext').css({
			rotate: '-85deg'
		}).animate({
			rotate: (value - 65 )+'deg'
		}, 800*4)
	}
	target.animate({
		rotate: degree+'deg'
	}, speed)
}
function fitScreen(){
	var winWidth = $(window).width()
	var winHeight = $(window).height()
	if(winWidth < 360){
		var size = winWidth/360
		$('body, html').css({
			width: winWidth
		})
		$('.quotawrapper').css({
			scale: [size, size]
		})
	}
}
$(document).ready(function(e){
	//fitscreen
	fitScreen()
	$(window).resize(function(){
		fitScreen()
	})
	//get quota value
	var usage = $('.qcontainer').data('usage')
	var nusage = $('.qcontainer').data('nusage')
	//chart start
	$(window).load(function(){
		$('.qcontainer').animate({
			opacity: 1
		},function(){
			rotateThis($('.usage .lb'),usage,800)
			rotateThis($('.nusage .lb'),nusage,800)
			rotateArrow($('.arrow'),usage,800)
			rotateArrow($('.narrow'),nusage,800)
		})
		//curved text
		$('.nusagetext').arctext({
			radius: 130,
			dir: 1
		})
		$('.usagetext').arctext({
			radius: 97, 
			dir: 1
		})
	})
	
});
