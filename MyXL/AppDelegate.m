//
//  AppDelegate.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/5/12.
//  Copyright (c) 2012 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "AppDelegate.h"

#import "MasterViewController.h"

#import "MSNavigationPaneViewController.h"

//
#import "Constant.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "EncryptDecrypt.h"
#import "DataManager.h"
#import "HomeViewController.h"

//#import "AXISnetNavigationBar.h"

#import "AXISnetCommon.h"
#import <QuartzCore/QuartzCore.h>
//

#import "EncryptDecrypt.h"

#import "SplashView.h"
#import "YLActivityIndicatorView.h"
#import "SplashViewController.h"

//#import <DropboxSDK/DropboxSDK.h>
//#import "ABMenuViewController.h"

#import "AudioToolbox/AudioToolbox.h"

#import "NotificationViewController.h"

//#import <FacebookSDK/FacebookSDK.h>

#import "NSData+Encryption.h"

#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "LogoModel.h"
//#import <CoreTelephony/CTTelephonyNetworkInfo.h>
//#import <CoreTelephony/CTCarrier.h>
#import "FacebookModel.h"

// TODO : Neptune
#import "ThankYouViewController.h"
#import "IIWrapController.h"
#import "IIViewDeckController.h"

// TODO : HYGIENE
#import "LeftViewController.h"

#import "CheckQuotaViewController.h"
#import "PaymentChoiceViewController.h"
#import "ContactUsViewController.h"

#define DegreesToRadians(x) ((x) * M_PI / 180.0)

/******* Set your tracking ID here *******/
//static NSString *const kTrackingId = @"UA-15439722-1";
static NSString *const kAllowTracking = @"allowTracking";

@interface AppDelegate () <UIAlertViewDelegate>

@property (strong, nonatomic) HomeViewController *homeViewController;
@property (strong, nonatomic) LoginViewController *loginViewController;

@property (strong, nonatomic) SplashView *splashView;
@property (strong, nonatomic) YLActivityIndicatorView *loadingView;
@property (strong, nonatomic) SplashViewController *splashViewController;

@property (nonatomic, retain) UIAlertView *notifAlert;
@property (nonatomic, readwrite) BOOL applicationDidSentToBackground;

- (void)requestProfile;
//- (void)showLoginView;
- (void)createAndPresentLoginView;
- (void)showHomeView;

- (void)dummyDataProfile;
- (void)dummyDataMenu;

- (void)createHomeViewController;

// TODO : Update Hygiene Defect
//- (void)registerDeviceToken;
//- (void)requestRegisterDeviceToken:(NSString*)devOS withDevToken:(NSString*)devToken;

@end

@implementation AppDelegate

//NSString *const FBSessionStateChangedNotification = @"com.example.Login:FBSessionStateChangedNotification";

@synthesize homeViewController = _homeViewController;
@synthesize loginViewController = _loginViewController;
@synthesize splashView = _splashView;
@synthesize loadingView = _loadingView;

@synthesize session = _session;

@synthesize hud;

@synthesize customNavBar = _customNavBar;

- (void)dealloc
{
    [_window release];
    [_navigationController release];
    [_loginNavigationController release];
    [_splashView release];
    [_loadingView release];
    [_customNavBar release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // TODO : V1.9
    // Google analytics tracking
    NSDictionary *appDefaults = @{kAllowTracking: @(YES)};
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    // User must be able to opt out of tracking
    [GAI sharedInstance].optOut = ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
//    [AXISnetCommon saveGAITrackingID:@"UA-64251181-1"];
    [AXISnetCommon saveGAITrackingID:@"UA-15439722-1"]; // XL
    
    [GAI sharedInstance].dispatchInterval = 0;
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    NSLog(@"GAI Tracking ID %@", [AXISnetCommon getGAITrackingID]);
//    if(!IS_PRODUCTION)
//        [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    self.tracker = [[GAI sharedInstance] trackerWithName:@"MyXL"
                                              trackingId:[AXISnetCommon getGAITrackingID]];
    
    //     Test Font Name
//    for (NSString *familyName in [UIFont familyNames]) {
//        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
//            NSLog(@"%@", fontName);
//        }
//    }
    
    // Lock Rotation for iPad
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        application.statusBarOrientation = UIInterfaceOrientationLandscapeRight;
    }
    
    // Clear Badge for Push Notif
	application.applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    NSURL *launchURL = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
	NSInteger majorVersion = [[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] integerValue];
	if (launchURL && majorVersion < 4) {
		// Pre-iOS 4.0 won't call application:handleOpenURL; this code is only needed if you support
		// iOS versions 3.2 or below
		[self application:application handleOpenURL:launchURL];
		return NO;
	}
    // -------------
    
    // Setup Icon
    setupIcon();
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    
    // Splash Screen
    SplashViewController *splashViewControllerTmp = [[SplashViewController alloc] initWithNibName:@"SplashViewController" bundle:nil];
    self.splashViewController = splashViewControllerTmp;
    
    self.window.rootViewController = self.splashViewController;
    [self.window makeKeyAndVisible];
    
    [splashViewControllerTmp release];
    
    [self registerForRemoteNotification];
    
    // TODO : HYGIENE - Push Notif
    //    int badgeCount = [UIApplication sharedApplication].applicationIconBadgeNumber;
    //    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    //    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeCount];
    
    //int badgeCount = [UIApplication sharedApplication].applicationIconBadgeNumber;
    //NSLog(@"badge count %d", badgeCount);
    if (launchOptions != nil)
    {
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        NSDictionary* dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (dictionary != nil)
        {
            NSLog(@"Launched from push notification: %@", dictionary);
            [self handleRemoteNotification:dictionary];
        }
    }
    
    // TODO : HYGIENE
    // UPDATE : Because of a defect in registering device token, this old function is used again
    [self requestProfile];
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.shouldUpdateDeviceToken = YES;
    //[self dummyDataProfile];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    // Changes by iNot at 24 May 2013
    // Set Current Datetime to nil
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:nil
              forKey:BALANCE_CACHE_KEY];
    [prefs setObject:nil
              forKey:USAGE_CACHE_KEY];
    [prefs setObject:nil
              forKey:PROMO_CACHE_KEY];
    [prefs synchronize];
    
    _applicationDidSentToBackground = YES;
    //
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    // TODO : HYGIENE
//    int badgeCount = [UIApplication sharedApplication].applicationIconBadgeNumber;
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeCount];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // TODO : V1.9
    [GAI sharedInstance].optOut = ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    NSLog(@"APp did become active");
    if(_applicationDidSentToBackground)
    {
        // TODO : HYGIENE
        UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"NOTIF IN WILL ENTER FOREGROUND" message:@"" delegate:nil cancelButtonTitle:[[Language get:@"close" alter:nil] uppercaseString] otherButtonTitles:nil, nil] autorelease];
        [alertView show];
        
        DataManager *sharedData = [DataManager sharedInstance];
        if (sharedData.isRemoteNotificationAvailable) {
            [self processPushNotification:sharedData.remoteNotificationInfo];
        }
        
        _applicationDidSentToBackground = NO;
    }
    //->[GAI sharedInstance].optOut = ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //-[FBAppCall handleDidBecomeActive];
    //->[FBAppCall handleDidBecomeActiveWithSession:self.session];
    
    //[FBSession.activeSession handleDidBecomeActive];
    
    //NSLog(@"applicationDidBecomeActive");
    
    // TODO : Neptune
//     Logs 'install' and 'app activate' App Events.
//    [FBAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    //-[FBSession.activeSession close];
    //->[self.session close];
    
    //NSLog(@"applicationWillTerminate");
    
    //[self.session close];
}

/*
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
	if ([[DBSession sharedSession] handleOpenURL:url]) {
		if ([[DBSession sharedSession] isLinked]) {
            
            DataManager *sharedData = [DataManager sharedInstance];
            
            ABMenuViewController *dropboxMenuViewController = [[ABMenuViewController alloc] initWithNibName:@"ABMenuViewController" bundle:nil];
            dropboxMenuViewController.parentId = sharedData.parentId;
            [sharedData.navController pushViewController:dropboxMenuViewController animated:YES];
            dropboxMenuViewController = nil;
            [dropboxMenuViewController release];
		}
		return YES;
	}
	
	return NO;
}*/

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        return UIInterfaceOrientationMaskLandscape;
    }
    else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

// TODO : HYGIENE
#pragma mark - Remote Notifications

#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

- (void)registerForRemoteNotification
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        NSLog(@"push notif enabled");
        UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    }
    else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}
#endif

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    NSString *newToken = [deviceToken description];
    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"Device token: %@",newToken);
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.deviceToken = newToken;
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Failed to get device token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"Receiving push notification: %@",userInfo);
    if ( application.applicationState == UIApplicationStateActive )
    {
        [self processPushNotification:userInfo];
    }
    else
    {
        // TODO : HYGIENE
//        UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"NOTIF IN APPLICATION IN BACKGROUND" message:@"" delegate:nil cancelButtonTitle:[[Language get:@"close" alter:nil] uppercaseString] otherButtonTitles:nil, nil] autorelease];
//        [alertView show];
    }
}

#pragma mark - Process Push Notification
-(void)processPushNotification:(NSDictionary *)userInfo
{
    @try
    {
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        DataManager *sharedData = [DataManager sharedInstance];
        if(self.loggedIn)
        {
            //NSLog(@"User logged in on push notif");
            [self handleRemoteNotification:userInfo];
            if(sharedData.menuData != nil)
            {
                NSLog(@"menu data is not nil");
                NSDictionary *apsDict = [userInfo valueForKey:@"aps"];
                if([apsDict isKindOfClass:[NSDictionary class]])
                {
                    NSLog(@"apsDict %@", apsDict);
                    NSString *message = [apsDict valueForKey:@"alert"];
                    UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@""
                                                                         message:[NSString stringWithFormat:@"%@",message]
                                                                        delegate:self
                                                               cancelButtonTitle:[[Language get:@"close" alter:nil] uppercaseString]
                                                               otherButtonTitles:[[Language get:@"view" alter:nil] uppercaseString],nil] autorelease];
                    alertView.tag = 999;
                    [alertView performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
                    sharedData.isRemoteNotificationShown = YES;
                    //NSLog(@"push notif shown");
                }
            }
            else
            {
                //NSLog(@"menu data is nil");
                sharedData.isRemoteNotificationShown = NO;
            }
        }
        else
        {
            if([self.window.rootViewController isKindOfClass:[SplashViewController class]])
            {
                [self handleRemoteNotification:userInfo];
                sharedData.isRemoteNotificationShown = NO;
            }
            else
            {
                NSDictionary *apsDict = [userInfo valueForKey:@"aps"];
                if(apsDict)
                {
                    NSString *message = [apsDict valueForKey:@"alert"];
                    UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@",message] delegate:nil cancelButtonTitle:[[Language get:@"close" alter:nil] uppercaseString] otherButtonTitles:nil, nil] autorelease];
                    [alertView show];
                    sharedData.isRemoteNotificationShown = YES;
                }
            }
        }

    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

-(void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler
{
    //NSLog(@"HANDLE REMOTE NOTIFICATION CALLED");
//    [[[UIAlertView alloc] initWithTitle:@"HANDLE REMOTE NOTIFICATION CALLED" message:[NSString stringWithFormat:@"%@",userInfo] delegate:nil cancelButtonTitle:[[Language get:@"close" alter:nil] uppercaseString] otherButtonTitles:nil, nil] autorelease];
}

#pragma mark - NOTIFICATION HANDLER

- (void)handleRemoteNotification:(NSDictionary *)userInfo
{
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.remoteNotificationInfo = userInfo;
    sharedData.isRemoteNotificationAvailable = YES;
}

#pragma mark - Facebook methods

/*
 * Callback for session changes.
 */
/*
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
            if (!error) {
                // We have a valid session
                NSLog(@"User session found");
                
                [FBRequestConnection
                 startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                     if (!error) {
                         NSString *facebookId = [result objectForKey:@"id"];
                         
                         DataManager *sharedData = [DataManager sharedInstance];
                         
                         AXISnetRequest *request = [AXISnetRequest sharedInstance];
                         request.delegate = self;
                         request.requestType = REG_FB_TOKEN_REQ;
                         
                         [request registerFacebookAccount:sharedData.profileData.token
                                               withMsisdn:sharedData.profileData.msisdn
                                             withFBUserId:facebookId
                                              withFBToken:session.accessTokenData.accessToken
                                                withTrxId:@""];
                     }
                 }];
            }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:FBSessionStateChangedNotification
     object:session];
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}*/

/*
 * Opens a Facebook session and optionally shows the login UX.
 */
/*
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"email",
                            //@"publish_­stream",
                            nil];
    
    return [FBSession openActiveSessionWithReadPermissions:permissions
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session,
                                                             FBSessionState state,
                                                             NSError *error) {
                                             [self sessionStateChanged:session
                                                                 state:state
                                                                 error:error];
                                         }];
    
    // can include any of the "publish" or "manage" permissions
    //NSArray *permissions = [NSArray arrayWithObjects:@"publish_actions", nil];
}*/

/*
 * If we have a valid session at the time of openURL call, we handle
 * Facebook transitions by passing the url argument to handleOpenURL
 */
/*
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBSession.activeSession handleOpenURL:url];
}*/

// TODO : Neptune

//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation {
//    // attempt to extract a token from the url
//    
//    /*
//    return [FBAppCall handleOpenURL:url
//                  sourceApplication:sourceApplication
//                    fallbackHandler:^(FBAppCall *call) {
//                        NSLog(@"In fallback handler");
//                    }];*/
//    
//    return [FBAppCall handleOpenURL:url
//                  sourceApplication:sourceApplication
//                        withSession:self.session];
//    
//    //return [FBSession.activeSession handleOpenURL:url];
//    //return [self.session handleOpenURL:url];
//}

#pragma mark -
#pragma mark DBSessionDelegate methods

//- (void)sessionDidReceiveAuthorizationFailure:(DBSession*)session userId:(NSString *)userId {
//	relinkUserId = [userId retain];
//	[[[[UIAlertView alloc] initWithTitle:@"Dropbox Session Ended"
//                                 message:@"Do you want to relink?"
//                                delegate:self
//                       cancelButtonTitle:@"Cancel"
//                       otherButtonTitles:@"Relink", nil]
//      autorelease]
//     show];
//}

#pragma mark - Selector

- (void)createHomeViewController
{
    HomeViewController *homeViewControllerTmp = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    self.homeViewController = homeViewControllerTmp;
    
    UINavigationController *homeNavController = customizedNavigationController(self.window.bounds, NO);
    
    [homeNavController setViewControllers:[NSArray arrayWithObject:self.homeViewController]];
    [self setNavigationController:homeNavController];
    
    [homeViewControllerTmp release];
    
    self.window.rootViewController = self.navigationController;
    //[self.window makeKeyAndVisible];
    
    //-----------
    DataManager *sharedData = [DataManager sharedInstance];
    
    //AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[self.navigationController navigationBar];
    _customNavBar = (AXISnetNavigationBar *)[self.navigationController navigationBar];
    
    NSString *saltKeyAPI = SALT_KEY;
    //msisdn = [EncryptDecrypt doCipher:msisdn action:kCCEncrypt withKey:saltKeyAPI];
    //NSLog(@"msisdn API encrypt = %@",msisdn);
    
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    //NSLog(@"msisdn API decrypt = %@",msisdn);
    if ([msisdn length] > 0) {
        msisdn = [msisdn stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"0"];
    }
    //NSLog(@"%@",msisdn);
    
    _customNavBar.hiLabel.text = [NSString stringWithFormat:@"%@, %@",[Language get:@"hi" alter:nil],sharedData.profileData.fullName];
    _customNavBar.msisdnLabel.text = [NSString stringWithFormat:@"%@",msisdn];
    //navBar.deviceLabel.text = sharedData.profileData.device;
    
    //------------
    
    //[self.homeViewController updateView];
}

// TODO : Update Hygiene Defect
//-(void)requestProfileWithDeviceToken:(NSString *)deviceToken
//{
//    AXISnetRequest *request = [AXISnetRequest sharedInstance];
//    request.delegate = self;
//    request.requestType = PROFILE_REQ;
//    [request profileWithDeviceToken:deviceToken];
//}

- (void)requestProfile
{
    // TODO : V1.9
    // Loading Time for GAI
    _dateToday = [[NSDate alloc] init];
    NSLog(@"Autologin start %@", _dateToday);
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = PROFILE_REQ;
    [request profile];
}

- (void)requestMenu {
    
    if (!_isAutoLogin) {
        UIViewController *topViewController = [self.window rootViewController];
        [self.hud hide:YES];
        self.hud = [MBProgressHUD showHUDAddedTo:topViewController.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
    }
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = MENU_REQ;
    [request menu];
}

- (void)showLoginView {
    UIViewController *topViewController = [self.window rootViewController];
    //NSLog(@"topViewController = %@",topViewController);
    UIViewController *modalViewController = [topViewController modalViewController];
    //NSLog(@"modalViewController = %@",modalViewController);
    
    // If the login screen is not already displayed, display it. If the login screen is
    // displayed, then getting back here means the login in progress did not successfully
    // complete. In that case, notify the login view so it can update its UI appropriately.
    
    if (![modalViewController isKindOfClass:[UINavigationController class]]) {
    //if (![modalViewController isKindOfClass:[LoginViewController class]]) {
        //LoginViewController* loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        //UINavigationController *loginNavController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
        
        if (self.loginNavigationController) {
            [topViewController presentModalViewController:self.loginNavigationController animated:NO];
        }
        else {
            LoginViewController* loginViewControllerTmp = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
            loginViewControllerTmp.delegate = self;
            self.loginViewController = loginViewControllerTmp;
            
            UINavigationController *loginNavController = customizedNavigationController(self.window.bounds,YES);
            
            [loginNavController setViewControllers:[NSArray arrayWithObject:self.loginViewController]];
            [self setLoginNavigationController:loginNavController];
            
            [loginViewControllerTmp release];
            
            [topViewController presentModalViewController:loginNavController animated:NO];
        }
    }
    else {
    }
}

- (void)createAndPresentLoginView {
    if (self.loginViewController == nil) {
        
        LoginViewController* loginViewControllerTmp = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        loginViewControllerTmp.delegate = self;
        self.loginViewController = loginViewControllerTmp;
        
        UINavigationController *loginNavController = customizedNavigationController(self.window.bounds,YES);
        
        [loginNavController setViewControllers:[NSArray arrayWithObject:self.loginViewController]];
        [self setLoginNavigationController:loginNavController];
        
        [loginViewControllerTmp release];
        
        UIViewController *topViewController = [self.navigationController topViewController];
        [topViewController presentModalViewController:self.loginNavigationController animated:NO];
        
        
        /*
        self.loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        self.loginViewController.delegate = self;
        
        UINavigationController *loginNavController = customizedNavigationController(self.window.bounds,YES);
        
        [loginNavController setViewControllers:[NSArray arrayWithObject:self.loginViewController]];
        
        [self setLoginNavigationController:loginNavController];
        
        //self.loginNavigationController = [[UINavigationController alloc] initWithRootViewController:self.loginViewController];
        
        UIViewController *topViewController = [self.navigationController topViewController];
        [topViewController presentModalViewController:self.loginNavigationController animated:NO];*/
    }
}

- (void)showHomeView {
    /*
    if (self.OViewController != nil) {
        UIViewController *topViewController = [self.loginNavigationController topViewController];
        [topViewController dismissModalViewControllerAnimated:YES];
        self.loginViewController = nil;
    }*/
    
    UIViewController *topViewController = [self.window rootViewController];
    if ([[topViewController modalViewController] isKindOfClass:[UINavigationController class]]) {
        [topViewController dismissModalViewControllerAnimated:YES];
    }
}

- (void)dummyDataProfile {
    NSString *data = @"{\"code\":\"200\",\"message\":\"Success\",\"data\":{\"id\":\"23113\",\"name\":\"6283873465015\",\"full_name\":\"\",\"msisdn\":\"WkdimdbcMUNASIXVQOMe_A==\",\"email\":\"\",\"language\":\"id\",\"location\":\"Jakarta\",\"regional\":\"Jakarta\",\"status\":\"1\",\"address\":null,\"address2\":null,\"address3\":null,\"city\":\"\",\"postcode\":\"\",\"confirm_email\":\"0\",\"email_notify\":\"0\",\"push_notify\":\"0\",\"gender\":\"\",\"birth\":\"\",\"token\":\"3232261135_b75e0266252-5\"}}";
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseGetProfileData:data];
    [dataParser release];
}

- (void)dummyDataMenu {
    NSString *data = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"txid\":\"7843226540\",\"msisdn\":\"6283873465015\",\"status\":\"1\",\"0\":{\"result\":[{\"menu\":{\"@attributes\":{\"href\":\"#balance\",\"id\":\"1\",\"parentid\":\"0\",\"gid\":\"1\",\"group\":\"Akun\",\"desc\":\"\"},\"0\":\"Akun\"}},{\"menu\":{\"@attributes\":{\"href\":\"#change_profile\",\"id\":\"1_001\",\"parentid\":\"1\",\"gid\":\"3\",\"group\":\"Profil\",\"desc\":\"\"},\"0\":\"Ubah Profil\"}},{\"menu\":{\"@attributes\":{\"href\":\"#change_password\",\"id\":\"1_002\",\"parentid\":\"1\",\"gid\":\"3\",\"group\":\"Profil\",\"desc\":\"\"},\"0\":\"Ubah Password\"}},{\"menu\":{\"@attributes\":{\"href\":\"#check_balance\",\"id\":\"1_003\",\"parentid\":\"1\",\"gid\":\"4\",\"group\":\"Pulsa\",\"desc\":\"\"},\"0\":\"Cek Pulsa\"}},{\"menu\":{\"@attributes\":{\"href\":\"#topup_hvrn\",\"id\":\"1_004\",\"parentid\":\"1\",\"gid\":\"4\",\"group\":\"Pulsa\",\"desc\":\"\"},\"0\":\"Isi Pulsa\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_voucher\",\"id\":\"1_005\",\"parentid\":\"1\",\"gid\":\"4\",\"group\":\"Pulsa\",\"desc\":\"\"},\"0\":\"Beli Voucher\"}},{\"menu\":{\"@attributes\":{\"href\":\"#balance_transfer\",\"id\":\"1_006\",\"parentid\":\"1\",\"gid\":\"4\",\"group\":\"Pulsa\",\"desc\":\"\"},\"0\":\"Kirim Pulsa\"}},{\"menu\":{\"@attributes\":{\"href\":\"#\",\"id\":\"1_007\",\"parentid\":\"1\",\"gid\":\"4\",\"group\":\"Pulsa\",\"desc\":\"\"},\"0\":\"Perpanjangan Masa Aktif\"}},{\"menu\":{\"@attributes\":{\"href\":\"#extend_validity\",\"id\":\"1_007_001\",\"parentid\":\"1_007\",\"gid\":\"7\",\"group\":\"Perpanjang Masa Aktif\",\"duration\":\"7 hari\",\"price\":\"2000\",\"desc\":\"\"},\"0\":\"7 Hari\"}},{\"menu\":{\"@attributes\":{\"href\":\"#package\",\"id\":\"2\",\"parentid\":\"0\",\"gid\":\"2\",\"group\":\"Paket\",\"desc\":\"\"},\"0\":\"Paket\"}},{\"menu\":{\"@attributes\":{\"href\":\"#check_usage\",\"id\":\"2_001\",\"parentid\":\"2\",\"gid\":\"5\",\"group\":\"Paketku\",\"desc\":\"\"},\"0\":\"Cek Pemakaian Internet\"}},{\"menu\":{\"@attributes\":{\"href\":\"#\",\"id\":\"2_002\",\"parentid\":\"2\",\"gid\":\"5\",\"group\":\"Paketku\",\"desc\":\"Ayo cari tahu bagaimana kamu bisa berbagi berbagai paket seru dari AXIS ke semua yang kamu sayangi\"},\"0\":\"Transfer Paket\"}},{\"menu\":{\"@attributes\":{\"href\":\"#gift_package\",\"id\":\"2_002_001\",\"parentid\":\"2_002\",\"pkgid\":\"GIFTHARIANH\",\"gid\":\"20\",\"group\":\"Transfer Paket\",\"volume\":\"15360000\",\"duration\":\"1 hari\",\"pkgtype\":\"onetime\",\"price\":\"1000\",\"desc\":\"Paket harian ini berlaku 24 jam untuk pemakaian hingga 15MB\"},\"0\":\"Kuota Harian 15MB\"}},{\"menu\":{\"@attributes\":{\"href\":\"#gift_package\",\"id\":\"2_002_002\",\"parentid\":\"2_002\",\"pkgid\":\"RAMADHAN2012GIFT\",\"gid\":\"20\",\"group\":\"Transfer Paket\",\"volume\":\"\",\"duration\":\"1 hari\",\"pkgtype\":\"onetime\",\"price\":\"1000\",\"desc\":\"Nikmati internet, telpon dan SMS ke semua AXIS sepuasnya dan 10 SMS ke operator lain. Berlaku jam 12 malam-6pagi di semua jaringan AXIS\"},\"0\":\"Paket Begadang\"}},{\"menu\":{\"@attributes\":{\"href\":\"#gift_package\",\"id\":\"2_002_003\",\"parentid\":\"2_002\",\"pkgid\":\"GIFTSOCIALMINGGU\",\"gid\":\"20\",\"group\":\"Transfer Paket\",\"volume\":\"614400000000\",\"duration\":\"7 hari\",\"pkgtype\":\"onetime\",\"price\":\"9900\",\"desc\":\"Paket Unlimited Mingguan ini berlaku selama 7 hari. Untuk pemakaian diatas 200MB, kecepatan internet maksimum menjadi 64kbps\"},\"0\":\"Mingguan INTERNETGaul\"}},{\"menu\":{\"@attributes\":{\"href\":\"#gift_package\",\"id\":\"2_002_004\",\"parentid\":\"2_002\",\"pkgid\":\"GIFTPROBASIC\",\"gid\":\"20\",\"group\":\"Transfer Paket\",\"volume\":\"614400000000\",\"duration\":\"30 hari\",\"pkgtype\":\"onetime\",\"price\":\"49000\",\"desc\":\"Paket Unlimited Bulanan ini berlaku selama 30 hari. Untuk pemakaian diatas 1.5GB, kecepatan internet maksimum menjadi 128kbps\"},\"0\":\"Bulanan Pro Basic\"}},{\"menu\":{\"@attributes\":{\"href\":\"#recommended_package\",\"id\":\"2_003\",\"parentid\":\"2\",\"gid\":\"5\",\"group\":\"Paketku\",\"desc\":\"Pilih paket sesuai dengan kebutuhan kamu\"},\"0\":\"Paket Rekomendasi\"}},{\"menu\":{\"@attributes\":{\"href\":\"#\",\"id\":\"2_004\",\"parentid\":\"2\",\"gid\":\"6\",\"group\":\"Paket\",\"desc\":\"Bagi yang suka Internetan seperti browsing, streaming, download video, online game, belanja online maupun update terus di sosial media maka paket ini cocok untukmu\"},\"0\":\"AXIS Pro - Apple\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_004_001\",\"parentid\":\"2_004\",\"pkgid\":\"PROMINGGU\",\"gid\":\"9\",\"group\":\"AXIS Pro - Apple\",\"volume\":\"102400000000\",\"duration\":\"7 hari\",\"pkgtype\":\"recur\",\"price\":\"24900\",\"desc\":\"Paket Unlimited Mingguan ini berlaku selama 7 hari. Untuk pemakaian diatas 750MB, kecepatan internet maksimum menjadi 128Kbps\"},\"0\":\"Mingguan\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_004_002\",\"parentid\":\"2_004\",\"pkgid\":\"PROBULAN500\",\"gid\":\"9\",\"group\":\"AXIS Pro - Apple\",\"volume\":\"614400000000\",\"duration\":\"30 hari\",\"pkgtype\":\"recur\",\"price\":\"49000\",\"desc\":\"Paket Unlimited Bulanan ini berlaku selama 30 hari. Untuk pemakaian diatas 1.5GB, kecepatan internet maksimum menjadi 128kbps\"},\"0\":\"Bulanan Basic\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_004_003\",\"parentid\":\"2_004\",\"pkgid\":\"PROBULAN1GB\",\"gid\":\"9\",\"group\":\"AXIS Pro - Apple\",\"volume\":\"614400000000\",\"duration\":\"30 hari\",\"pkgtype\":\"recur\",\"price\":\"79000\",\"desc\":\"Paket Premium Unlimited Bulanan ini berlaku selama 30 hari. Untuk pemakaian diatas 3GB, kecepatan internet maksimum menjadi 128kbps\"},\"0\":\"Bulanan Premium\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_004_004\",\"parentid\":\"2_004\",\"pkgid\":\"PROBULAN2GB\",\"gid\":\"9\",\"group\":\"AXIS Pro - Apple\",\"volume\":\"614400000000\",\"duration\":\"30 hari\",\"pkgtype\":\"recur\",\"price\":\"149000\",\"desc\":\"Paket Ultimate Unlimited Bulanan ini berlaku selama 30 hari. Untuk pemakaian diatas 6GB, kecepatan internet maksimum menjadi 128kbps\"},\"0\":\"Bulanan Ultimate\"}},{\"menu\":{\"@attributes\":{\"href\":\"#\",\"id\":\"2_005\",\"parentid\":\"2\",\"gid\":\"6\",\"group\":\"Paket\",\"desc\":\"Suka eksis di sosial media? Sekarang bisa terus online di sosial media dengan harga hemat pake INTERNETGaul\"},\"0\":\"INTERNETGaul AXIS - Apple\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_005_001\",\"parentid\":\"2_005\",\"pkgid\":\"SOCIALMINGGU\",\"gid\":\"10\",\"group\":\"INTERNETGaul AXIS - Apple\",\"volume\":\"614400000000\",\"duration\":\"7 hari\",\"pkgtype\":\"recur\",\"price\":\"9900\",\"desc\":\"Paket Unlimited Mingguan ini berlaku selama 7 hari. Untuk pemakaian diatas 200MB, kecepatan internet maksimum menjadi 64kbps\"},\"0\":\"Mingguan\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_005_002\",\"parentid\":\"2_005\",\"pkgid\":\"SOCIALBULAN\",\"gid\":\"10\",\"group\":\"INTERNETGaul AXIS - Apple\",\"volume\":\"614400000000\",\"duration\":\"30 hari\",\"pkgtype\":\"recur\",\"price\":\"29900\",\"desc\":\"Paket Unlimited Bulanan ini berlaku selama 30 hari. Untuk pemakaian diatas 800MB, kecepatan internet maksimum menjadi 64kbps\"},\"0\":\"Bulanan\"}},{\"menu\":{\"@attributes\":{\"href\":\"#\",\"id\":\"2_0051\",\"parentid\":\"2\",\"gid\":\"6\",\"group\":\"Paket\",\"desc\":\"Paket Internet yang paling sesuai buat Smartphone kamu? Pilih AXIS Smartphone Package Internet Unlimited hingga 1.5GB, Nelpon dan SMS SEPUASNYA ke SEMUA AXIS, dan  1000SMS ke OPERATOR LAIN mulai Rp.34.900. Berlaku selama 30 hari\"},\"0\":\"Paket Smartphone\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_0051_001\",\"parentid\":\"2_0051\",\"pkgid\":\"SMARTLOW\",\"gid\":\"24\",\"group\":\"Paket Smartphone\",\"volume\":\"614400000000\",\"duration\":\"30 days\",\"pkgtype\":\"recur\",\"price\":\"34900\",\"desc\":\"Paket Smartphone Basic berlaku selama 30 hari. Pulsa kamu akan berkurang sebesar Rp 34.900  Nikmati Internet Unlimited dengan FUP 800 MB, 200 menit nelpon dan 200 SMS ke semua AXIS.\"},\"0\":\"Smartphone Basic\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_0051_002\",\"parentid\":\"2_0051\",\"pkgid\":\"SMARTHIGH\",\"gid\":\"24\",\"group\":\"Paket Smartphone\",\"volume\":\"614400000000\",\"duration\":\"30 days\",\"pkgtype\":\"recur\",\"price\":\"69900\",\"desc\":\"Paket Smartphone Premium berlaku selama 30 hari. Pulsa kamu akan berkurang sebesar Rp 69.900  Nikmati Internet Unlimited dengan FUP 1.5 GB, nelpon dan SMS sepuasnya ke semua AXIS, dan 1000 SMS ke OPERATOR LAIN.\"},\"0\":\"Smartphone Premium\"}},{\"menu\":{\"@attributes\":{\"href\":\"#\",\"id\":\"2_006\",\"parentid\":\"2\",\"gid\":\"6\",\"group\":\"Paket\",\"desc\":\"Biar internetan ngga terganggu, gunakan speed Booster untuk mengembalikan kualitas kecepatan maksimum setelah mencapai batas pemakaian wajar\"},\"0\":\"Speed Booster\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_006_001\",\"parentid\":\"2_006\",\"pkgid\":\"BOOSTER150MB\",\"gid\":\"11\",\"group\":\"Speed Booster\",\"volume\":\"153600000\",\"duration\":\"7 hari\",\"pkgtype\":\"onetime\",\"price\":\"15000\",\"desc\":\"Speed booster ini berlaku selama 7 hari untuk pemakaian hingga 150MB\"},\"0\":\"7 hari 150MB\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_006_002\",\"parentid\":\"2_006\",\"pkgid\":\"BOOSTER500MB\",\"gid\":\"11\",\"group\":\"Speed Booster\",\"volume\":\"512000000\",\"duration\":\"30 hari\",\"pkgtype\":\"onetime\",\"price\":\"49000\",\"desc\":\"Speed booster ini berlaku selama 30 hari untuk pemakaian hingga 500MB\"},\"0\":\"30 hari 500MB\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_006_003\",\"parentid\":\"2_006\",\"pkgid\":\"BOOSTER1GB\",\"gid\":\"11\",\"group\":\"Speed Booster\",\"volume\":\"1024000000\",\"duration\":\"30 hari\",\"pkgtype\":\"onetime\",\"price\":\"79000\",\"desc\":\"Speed booster ini berlaku selama 30 hari untuk pemakaian hingga 1GB\"},\"0\":\"30 hari 1GB\"}},{\"menu\":{\"@attributes\":{\"href\":\"#\",\"id\":\"2_007\",\"parentid\":\"2\",\"gid\":\"6\",\"group\":\"Paket\",\"desc\":\"Paket HEMAT internet harian ada disini\"},\"0\":\"Internet Harian\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_007_001\",\"parentid\":\"2_007\",\"pkgid\":\"HARIANH\",\"gid\":\"12\",\"group\":\"Internet Harian\",\"volume\":\"15360000\",\"duration\":\"1 hari\",\"pkgtype\":\"onetime\",\"price\":\"1000\",\"desc\":\"Paket harian ini berlaku 24 jam untuk pemakaian hingga 15MB\"},\"0\":\"Kuota Harian 15MB\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_007_002\",\"parentid\":\"2_007\",\"pkgid\":\"RAMADHAN2012\",\"gid\":\"12\",\"group\":\"Internet Harian\",\"volume\":\"\",\"duration\":\"1 hari\",\"pkgtype\":\"onetime\",\"price\":\"1000\",\"desc\":\"Nikmati internet, telpon dan SMS ke semua AXIS sepuasnya dan 10 SMS ke operator lain. Berlaku jam 12 malam-6pagi di semua jaringan AXIS\"},\"0\":\"Paket Begadang\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_007_003\",\"parentid\":\"2_007\",\"pkgid\":\"SMSDATAHARI\",\"gid\":\"12\",\"group\":\"Internet Harian\",\"volume\":\"\",\"duration\":\"1 hari\",\"pkgtype\":\"onetime\",\"price\":\"1500\",\"desc\":\"Paket harian ini berlaku di seluruh jaringan AXIS sampai pukul 23:59\"},\"0\":\"Paket Internet dan SMS\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_007_004\",\"parentid\":\"2_007\",\"pkgid\":\"INTERNETHEMAT\",\"gid\":\"12\",\"group\":\"Internet Harian\",\"volume\":\"0\",\"duration\":\"tanpa batas\",\"pkgtype\":\"onetime\",\"price\":\"GRATIS\",\"desc\":\"Dapatkan GRATIS internet sepuasnya setelah pemakaian Rp.4.000\"},\"0\":\"Internet Hemat Rp 2KB\"}},{\"menu\":{\"@attributes\":{\"href\":\"#\",\"id\":\"2_008\",\"parentid\":\"2\",\"gid\":\"13\",\"group\":\"Paket Lainnya\",\"desc\":\"Nikmati internet, telpon dan SMS ke semua AXIS sepuasnya dan 10 SMS ke operator lain. Berlaku jam 12 malam-6pagi di semua jaringan AXIS\"},\"0\":\"Paket Begadang\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_008_001\",\"parentid\":\"2_008\",\"pkgid\":\"RAMADHAN2012\",\"gid\":\"22\",\"group\":\"Paket Begadang\",\"volume\":\"\",\"duration\":\"1 hari\",\"pkgtype\":\"onetime\",\"price\":\"1000\",\"desc\":\"Nikmati internet, telpon dan SMS ke semua AXIS sepuasnya dan 10 SMS ke operator lain. Berlaku jam 12 malam-6pagi di semua jaringan AXIS\"},\"0\":\"Paket Begadang\"}},{\"menu\":{\"@attributes\":{\"href\":\"#\",\"id\":\"2_009\",\"parentid\":\"2\",\"gid\":\"13\",\"group\":\"Paket Lainnya\",\"desc\":\"Buat yang suka internetan dan SMSan sepuasnya seharian, ini paket yang tepat buat kamu. GRATIS internet 15 MB, SMS sepuasnya ke semua AXIS dan 50 SMS ke operator lain cuma Rp1.500\"},\"0\":\"Paket Internet dan SMS\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_009_001\",\"parentid\":\"2_009\",\"pkgid\":\"SMSDATAHARI\",\"gid\":\"23\",\"group\":\"Paket Internet dan SMS\",\"volume\":\"\",\"duration\":\"1 hari\",\"pkgtype\":\"onetime\",\"price\":\"1500\",\"desc\":\"Paket harian ini berlaku di seluruh jaringan AXIS sampai pukul 23:59\"},\"0\":\"Harian\"}},{\"menu\":{\"@attributes\":{\"href\":\"#\",\"id\":\"2_010\",\"parentid\":\"2\",\"gid\":\"13\",\"group\":\"Paket Lainnya\",\"desc\":\"\"},\"0\":\"Paket Super Hemat\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_010_001\",\"parentid\":\"2_010\",\"pkgid\":\"COMBOHARI\",\"gid\":\"21\",\"group\":\"Paket Super Hemat\",\"volume\":\"\",\"duration\":\"1 hari\",\"pkgtype\":\"onetime\",\"price\":\"5000\",\"desc\":\"GRATIS nelpon dan 10.000 SMS ke semua AXIS, 50 SMS ke operator lain, dan Internetan unlimited Seharian. Berlaku di Jabodetabek, Jawa, Bali, dan Kep.Riau \"},\"0\":\"Harian\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_010_002\",\"parentid\":\"2_010\",\"pkgid\":\"COMBOMINGGU\",\"gid\":\"21\",\"group\":\"Paket Super Hemat\",\"volume\":\"\",\"duration\":\"7 hari\",\"pkgtype\":\"recur\",\"price\":\"19900\",\"desc\":\"GRATIS nelpon dan 10.000 SMS ke semua AXIS, 200 SMS ke operator lain, dan Internetan unlimited Seharian. Berlaku untuk 7 hari di Jabodetabek, Jawa, Bali, dan Kep.Riau\"},\"0\":\"Mingguan\"}},{\"menu\":{\"@attributes\":{\"href\":\"#buy_package\",\"id\":\"2_010_003\",\"parentid\":\"2_010\",\"pkgid\":\"COMBOBULAN\",\"gid\":\"21\",\"group\":\"Paket Super Hemat\",\"volume\":\"\",\"duration\":\"30 hari\",\"pkgtype\":\"recur\",\"price\":\"69900\",\"desc\":\"GRATIS nelpon dan 10.000 SMS ke semua AXIS, 1.000 SMS ke operator lain, dan Internetan unlimited Seharian. Berlaku untuk 30 hari di Jabodetabek, Jawa, Bali, dan Kep.Riau\"},\"0\":\"Bulanan\"}},{\"menu\":{\"@attributes\":{\"href\":\"#check_bonus\",\"images\":\"\",\"id\":\"1_0031\",\"parentid\":\"1\",\"gid\":\"4\",\"pkgid\":\"\",\"group\":\"Pulsa\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Cek Bonus\"}},{\"menu\":{\"@attributes\":{\"href\":\"#history\",\"images\":\"\",\"id\":\"1_0071\",\"parentid\":\"1\",\"gid\":\"4\",\"pkgid\":\"\",\"group\":\"Pulsa\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Transaksi Terakhir\"}},{\"menu\":{\"@attributes\":{\"href\":\"#my_package\",\"images\":\"\",\"id\":\"2_0011\",\"parentid\":\"2\",\"gid\":\"5\",\"pkgid\":\"\",\"group\":\"Paket Ku\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Paket Ku\"}},{\"menu\":{\"@attributes\":{\"href\":\"#survey\",\"images\":\"\",\"id\":\"2_0199\",\"parentid\":\"2\",\"gid\":\"5\",\"pkgid\":\"\",\"group\":\"My Package\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"Hi...bingung memilih Paket Internet yang sesuai dengan kebutuhanmu? Yuk cek disini supaya engga salah pilih\",\"tooltips\":\"\"},\"0\":\"Paket Terbaik Untukmu\"}},{\"menu\":{\"@attributes\":{\"href\":\"#notification\",\"images\":\"\",\"id\":\"3\",\"parentid\":\"0\",\"gid\":\"19\",\"pkgid\":\"\",\"group\":\"Notifikasi\",\"volume\":\"0\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Notifikasi\"}},{\"menu\":{\"@attributes\":{\"href\":\"#promo\",\"images\":\"\",\"id\":\"4\",\"parentid\":\"0\",\"gid\":\"50\",\"pkgid\":\"\",\"group\":\"Promo\",\"volume\":\"0\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Promo\"}},{\"menu\":{\"@attributes\":{\"href\":\"#axis_shop\",\"images\":\"\",\"id\":\"7\",\"parentid\":\"0\",\"gid\":\"30\",\"pkgid\":\"\",\"group\":\"AXIS Shop\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"AXIS Store\"}},{\"menu\":{\"@attributes\":{\"href\":\"#axis_box\",\"images\":\"\",\"id\":\"8\",\"parentid\":\"1\",\"gid\":\"60\",\"pkgid\":\"\",\"group\":\"Lainnya\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"AXIS box\"}},{\"menu\":{\"@attributes\":{\"href\":\"#contacts\",\"images\":\"icon_wallpaper.gif\",\"id\":\"8_1\",\"parentid\":\"8\",\"gid\":\"61\",\"pkgid\":\"\",\"group\":\"AXIS box\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Contacts\"}},{\"menu\":{\"@attributes\":{\"href\":\"#backup_contacts\",\"images\":\"icon_wallpaper.gif\",\"id\":\"8_1_1\",\"parentid\":\"8_1\",\"gid\":\"62\",\"pkgid\":\"\",\"group\":\"Contacts\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Backup\"}},{\"menu\":{\"@attributes\":{\"href\":\"#restore_contacts\",\"images\":\"icon_wallpaper.gif\",\"id\":\"8_1_2\",\"parentid\":\"8_1\",\"gid\":\"62\",\"pkgid\":\"\",\"group\":\"Contacts\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Restore\"}},{\"menu\":{\"@attributes\":{\"href\":\"#photos\",\"images\":\"icon_wallpaper.gif\",\"id\":\"8_2\",\"parentid\":\"8\",\"gid\":\"61\",\"pkgid\":\"\",\"group\":\"AXIS box\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Photos\"}},{\"menu\":{\"@attributes\":{\"href\":\"#export_photos\",\"images\":\"icon_wallpaper.gif\",\"id\":\"8_2_1\",\"parentid\":\"8_2\",\"gid\":\"63\",\"pkgid\":\"\",\"group\":\"Photos\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Export\"}},{\"menu\":{\"@attributes\":{\"href\":\"#import_photos\",\"images\":\"icon_wallpaper.gif\",\"id\":\"8_2_2\",\"parentid\":\"8_2\",\"gid\":\"63\",\"pkgid\":\"\",\"group\":\"Photos\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Import\"}},{\"menu\":{\"@attributes\":{\"href\":\"#term_conditions\",\"images\":\"icon_wallpaper.gif\",\"id\":\"8_4\",\"parentid\":\"8\",\"gid\":\"61\",\"pkgid\":\"\",\"group\":\"AXIS box\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"<style> li{margin-left:-18px;padding-bottom:5px;} body {  font-size:100%; } ol {  margin-top:-13px; } <style> <p>Silakan membaca syarat dan ketentuan ini terlebih dahulu. Dengan mengakses atau menggunakan situs ini atau layanan ini, kamu setuju dan terikat dengan seluruh syarat dan ketentuan dalam situs ini  https:www.dropbox.comterms#terms. Jika kamu tidak setuju dengan seluruh syarat dan ketentuan ini, harap tidak menggunakan layanan ini.<p> <ol> <li>Kamu harus melakukan registrasi dengan lengkap dan benar terlebih dahulu atau memiliki akun di AXIS box untuk menggunakannya.<li> <li>File yang bisa kamu upload adalah: buku telepon, gambar dan musik. Pelanggan disarankan untuk tidak memasukkan file yang penting ke dalam AXIS box, AXIS tidak bertanggung jawab atas segala bentuk kehilangan atau kerusakan data, segala bentuk kerugian langsung maupun tidak langsung, hilangnya peluang bisnis dan keuntungan, dan hal lainnya yang disebabkan hilangnya file.<li> <li>Semakin besar ukuran file, semakin lama waktu yang kamu butuhkan untuk meng-uploadnya. Besarnya ukuran file akan berpengaruh pada tarif internetmu, sehingga sebaiknya kamu memahami tarif internet AXIS sebelum melakukan proses download.<li> <li>Pelanggan mengetahui dan menyetujui bahwa layanan ini sangat bergantung pada seluruh sistem dan elemen lainnya (kegagalan koneksi Internet, masalah dalam Dropbox server) karena itu, pelanggan juga diharapkan telah menyimpan data cadangan terlebih dulu sebelum memasukkan file ke dalam AXIS box. Pelanggan diharapkan untuk segera menghubungi layanan pelanggan AXIS bila hal itu terjadi, sehingga AXIS mungkin dapat melakukan upaya untuk mendapatkan kembali data yang hilang sepanjang hal tersebut dimungkinkan oleh kondisi sistem.<li> <li>AXIS memiliki hak, dengan atau tanpa pemberitahuan terlebih dulu kepada pelanggan, untuk mengganti konfigurasi teknik bila diperlukan untuk kualitas layanan AXIS box yang optimal.<li> <li>Pelanggan memahami bahwa AXIS tidak bertanggung jawab atas isi konten yang di upload, baik itu melanggar atau tidak, hukum, norma dan kaidah-kaidah yang berlaku di Indonesia. AXIS atas kebijaksanaannya sendiri dapat memilih untuk tidak mengijinkanmemberhentikan menghapus konten yang dianggap bertentangan dengan hukum dan norma-norma tersebut.<li> <ol>\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Term & Conditions\"}},{\"menu\":{\"@attributes\":{\"href\":\"#info\",\"images\":\"\",\"id\":\"9\",\"parentid\":\"0\",\"gid\":\"90\",\"pkgid\":\"\",\"group\":\"Info\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Info & Bantuan\"}},{\"menu\":{\"@attributes\":{\"href\":\"#faq\",\"images\":\"\",\"id\":\"9_001\",\"parentid\":\"9\",\"gid\":\"91\",\"pkgid\":\"\",\"group\":\"Info\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Informasi Umum\"}},{\"menu\":{\"@attributes\":{\"href\":\"#faq_detail\",\"images\":\"\",\"id\":\"9_001_001\",\"parentid\":\"9_001\",\"gid\":\"93\",\"pkgid\":\"\",\"group\":\"Informasi Umum\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"AXIS net\"}},{\"menu\":{\"@attributes\":{\"href\":\"#faq_detail\",\"images\":\"\",\"id\":\"9_001_002\",\"parentid\":\"9_001\",\"gid\":\"93\",\"pkgid\":\"\",\"group\":\"Informasi Umum\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Layanan Pesan Suara\"}},{\"menu\":{\"@attributes\":{\"href\":\"#faq_detail\",\"images\":\"\",\"id\":\"9_001_003\",\"parentid\":\"9_001\",\"gid\":\"93\",\"pkgid\":\"\",\"group\":\"Informasi Umum\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"SIM Card AXIS\"}},{\"menu\":{\"@attributes\":{\"href\":\"#faq_detail\",\"images\":\"\",\"id\":\"9_001_004\",\"parentid\":\"9_001\",\"gid\":\"93\",\"pkgid\":\"\",\"group\":\"Informasi Umum\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Cek Pulsa\"}},{\"menu\":{\"@attributes\":{\"href\":\"#faq_detail\",\"images\":\"\",\"id\":\"9_001_005\",\"parentid\":\"9_001\",\"gid\":\"93\",\"pkgid\":\"\",\"group\":\"Informasi Umum\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Isi Ulang\"}},{\"menu\":{\"@attributes\":{\"href\":\"#faq_detail\",\"images\":\"\",\"id\":\"9_001_006\",\"parentid\":\"9_001\",\"gid\":\"93\",\"pkgid\":\"\",\"group\":\"Informasi Umum\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Transfer Pulsa\"}},{\"menu\":{\"@attributes\":{\"href\":\"#faq_detail\",\"images\":\"\",\"id\":\"9_001_007\",\"parentid\":\"9_001\",\"gid\":\"93\",\"pkgid\":\"\",\"group\":\"Informasi Umum\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"PUK\"}},{\"menu\":{\"@attributes\":{\"href\":\"#faq_detail\",\"images\":\"\",\"id\":\"9_001_008\",\"parentid\":\"9_001\",\"gid\":\"93\",\"pkgid\":\"\",\"group\":\"Informasi Umum\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Masa Aktif & Tenggang\"}},{\"menu\":{\"@attributes\":{\"href\":\"#store_locator\",\"images\":\"\",\"id\":\"9_002\",\"parentid\":\"9\",\"gid\":\"91\",\"pkgid\":\"\",\"group\":\"Info\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Lokasi AXIS Shop\"}},{\"menu\":{\"@attributes\":{\"href\":\"#\",\"images\":\"\",\"id\":\"9_003\",\"parentid\":\"9\",\"gid\":\"91\",\"pkgid\":\"\",\"group\":\"Info\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"Setting layanan data akan dikirimkan secara otomatis ke handphone kamu dan dapat langsung kamu nikmati. Apabila dalam proses pengaturan kamu mengalami kendala, maka lakukan pengaturan manual\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Pengaturan handset\"}},{\"menu\":{\"@attributes\":{\"href\":\"#sms\",\"images\":\"\",\"id\":\"9_003_001\",\"parentid\":\"9_003\",\"gid\":\"94\",\"pkgid\":\"\",\"group\":\"Pengaturan handset\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Setting SMS\"}},{\"menu\":{\"@attributes\":{\"href\":\"#manual\",\"images\":\"\",\"id\":\"9_003_002\",\"parentid\":\"9_003\",\"gid\":\"94\",\"pkgid\":\"\",\"group\":\"Pengaturan handset\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Setting Manual\"}},{\"menu\":{\"@attributes\":{\"href\":\"#puk\",\"images\":\"\",\"id\":\"9_004\",\"parentid\":\"9\",\"gid\":\"92\",\"pkgid\":\"\",\"group\":\"Bantuan\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"PUK adalah kode yang diperlukan untuk membuka kartu SIM kamu yang terblokir karena salah memasukkan PIN sebanyak tiga kali\",\"topdesc\":\"\",\"tooltips\":\"ICCID adalah nomor pengenal, yang disimpan di belakang SIM Card. Contoh: 8962xxxxxxxx\"},\"0\":\"PUK\"}},{\"menu\":{\"@attributes\":{\"href\":\"#contactus\",\"images\":\"\",\"id\":\"9_005\",\"parentid\":\"9\",\"gid\":\"92\",\"pkgid\":\"\",\"group\":\"Bantuan\",\"volume\":\"\",\"duration\":\"\",\"price\":\"\",\"pkgtype\":\"\",\"pkggroup\":\"\",\"desc\":\"\",\"topdesc\":\"\",\"tooltips\":\"\"},\"0\":\"Hubungi Kami\"}}]}}";
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parseGetMenuData:data];
    [dataParser release];
}

- (void)registerDeviceToken {
    // Add registration for remote notifications
    
    //[[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    // TODO : Update Hygiene Defect
    DataManager *sharedData = [DataManager sharedInstance];
    if([sharedData.deviceToken length] > 0)
    {
        if(sharedData.shouldUpdateDeviceToken)
            [self requestRegisterDeviceToken:sharedData.deviceToken];
    }
}

// TODO : Update Hygiene Defect
//- (void)requestRegisterDeviceToken:(NSString*)devOS withDevToken:(NSString*)devToken
#pragma mark - Register Device Token
- (void)requestRegisterDeviceToken:(NSString*)devToken
{
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = REG_DEV_TOKEN_REQ;
    [request registerDeviceToken:devToken
                    withDelegate:self
                     andCallBack:@selector(registerDeviceTokenDone:)];
}

// TODO : Update Hygiene Defect
-(void)registerDeviceTokenDone:(NSDictionary *)result
{
    if(result)
    {
        NSString *success = [result objectForKey:@"success"];
        if([success isEqualToString:@"1"])
        {
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.shouldUpdateDeviceToken = NO;
        }
    }
    NSLog(@"device token registration done");
}

- (NSData*) encryptString:(NSString*)plaintext withKey:(NSString*)key {
	return [[plaintext dataUsingEncoding:NSUTF8StringEncoding] AES256EncryptWithKey:key];
}

- (NSString*) decryptData:(NSData*)ciphertext withKey:(NSString*)key {
	return [[[NSString alloc] initWithData:[ciphertext AES256DecryptWithKey:key]
	                              encoding:NSUTF8StringEncoding] autorelease];
}

- (void)performBalance {
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = CHECK_BALANCE_REQ;
    [request checkBalance];
    
    /*
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDate *balanceCacheDate = [prefs objectForKey:BALANCE_CACHE_KEY];
    NSDate *currentDate = [NSDate date];
    NSTimeInterval secondsBetween = [currentDate timeIntervalSinceDate:balanceCacheDate];
    float minutes = secondsBetween / 60;
    
    if (balanceCacheDate == nil || minutes > kDefaultCache) {
        
//        UIViewController *topViewController = [self.window rootViewController];
//        self.hud = [MBProgressHUD showHUDAddedTo:topViewController.view animated:YES];
//        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = CHECK_BALANCE_REQ;
        [request checkBalance];
    }*/
}

- (void)performPackage {
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = CHECK_USAGE_REQ;
    [request checkInternetUsage];
    
    /*
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDate *usageCacheDate = [prefs objectForKey:USAGE_CACHE_KEY];
    NSDate *currentDate = [NSDate date];
    NSTimeInterval secondsBetween = [currentDate timeIntervalSinceDate:usageCacheDate];
    float minutes = secondsBetween / 60;
    
    if (usageCacheDate == nil || minutes > kDefaultCache) {
//        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = CHECK_USAGE_REQ;
        [request checkInternetUsage];
    }*/
}

-(void)requestCheckUsageXL
{
    NSLog(@"quota calc");
    UIViewController *topViewController = [self.window rootViewController];
    [self.hud hide:YES];
    self.hud = [MBProgressHUD showHUDAddedTo:topViewController.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = CHECK_USAGE_XL_REQ;
    [request checkInternetUsageXL];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    NSLog(@"request done with result %@", result);
//    UIViewController *topViewController = [self.window rootViewController];
//    [MBProgressHUD hideHUDForView:topViewController.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    
    NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
    int type = [typeNumber intValue];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        if (type == PROFILE_REQ) {
            
            _loggedIn = YES;
            _isAutoLogin = YES;
            // TODO : Hygiene
            _isOffNet = NO;
            
            //----------------//
            // CR Change Logo //
            //----------------//
            // 1. Check Existing Version for Standard Logo
            DataManager *sharedData = [DataManager sharedInstance];
            
            // 5. Check Existing Version for Retina Logo
            LogoModel *logoModel1 = [sharedData.profileData.logo objectAtIndex:1];
            BOOL retinaLogoSame = isRetinaLogoVersionSame(logoModel1.version,sharedData.profileData.telcoOperator);
            if (!retinaLogoSame) {
                
                ASINetworkQueue *networkQueue = [[ASINetworkQueue alloc] init];
                [networkQueue reset];
                [networkQueue setRequestDidFinishSelector:@selector(requestRetinaLogoCompleted:)];
                [networkQueue setRequestDidFailSelector:@selector(requestLogoFailed:)];
                [networkQueue setDelegate:self];
                
                ASIHTTPRequest *request;
                request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:logoModel1.logoURL]];
                [request setUserInfo:[NSDictionary dictionaryWithObject:@"1" forKey:@"tag"]];
                [networkQueue addOperation:request];
                
                [networkQueue go];
            }
            else {
                NSString *documentsDirectory = applicationDocumentsDirectory();
                NSString *logoName = @"";
                if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
                    logoName = XL_LOGO_RETINA_NAME;
                }
                else {
                    logoName = AXIS_LOGO_RETINA_NAME;
                }
                NSString *mediaPath = [documentsDirectory stringByAppendingPathComponent:logoName];
                
                // 8. Set Logo
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate.customNavBar updateLogo:mediaPath];
                
                // TODO : V1.9
                [self updateVersionCheckWithNegativeSelector:@selector(requestMenu)];
                
                // Req Menu
//                [self requestMenu];
            }
            //----------------//
        }
        
        if (type == REG_DEV_TOKEN_REQ) {
            [self requestMenu];
            //[self dummyDataMenu];
        }
        
        if (type == MENU_REQ) {
            
            
            if (!_isAutoLogin) {
                //[self performBalance];
                
                UIViewController *topViewController = [self.window rootViewController];
                [MBProgressHUD hideHUDForView:topViewController.view animated:YES];
                
                [self showHomeView];
            }
            else {
                [self.splashViewController.loadingView stopAnimating];
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.window cache:YES];
                [UIView setAnimationDuration:0.5];
                [UIView setAnimationDelegate:self];
                [self.splashViewController.view removeFromSuperview];
                [UIView commitAnimations];
                
                [self createHomeViewController];
                /*
                //
                [_loadingView stopAnimating];
                [_splashView removeFromSuperview];
                
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.window cache:YES];
                [UIView setAnimationDuration:0.5];
                [UIView setAnimationDelegate:self];
                [_splashView removeFromSuperview];
                [UIView commitAnimations];
                //
                
                //[self performBalance];
                 */
            }
        }
        
        if (type == CHECK_BALANCE_REQ) {
            /*
            // Changes by iNot at 24 May 2013
            // Set Current Datetime to Preference
            NSDate *current = [NSDate date];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setObject:current
                      forKey:BALANCE_CACHE_KEY];
            [prefs synchronize];
            //
             */
            
            [self performPackage];
        }
        
        if (type == CHECK_USAGE_REQ) {
            /*
            // Changes by iNot at 24 May 2013
            // Set Current Datetime to Preference
            NSDate *current = [NSDate date];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setObject:current
                      forKey:USAGE_CACHE_KEY];
            [prefs synchronize];
            //
             */
            
            if (!_isAutoLogin) {
                UIViewController *topViewController = [self.window rootViewController];
                [MBProgressHUD hideHUDForView:topViewController.view animated:YES];
                
                [self showHomeView];
            }
            else {
                [self.splashViewController.loadingView stopAnimating];
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.window cache:YES];
                [UIView setAnimationDuration:0.5];
                [UIView setAnimationDelegate:self];
                [self.splashViewController.view removeFromSuperview];
                [UIView commitAnimations];
                
                [self createHomeViewController];
            }
        }
        
        if(type == NOTIFICATION_REQ)
        {
            [self.hud hide:YES];
            [self showNotificationView];
        }
        
        if(type == CHECK_USAGE_XL_REQ)
        {
            [self.hud hide:YES];
            [self showQuotaUsageView];
        }
        
        if (type == PROMO_REQ)
        {
            [self.hud hide:YES];
            NSDate *current = [NSDate date];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setObject:current
                      forKey:PROMO_CACHE_KEY];
            [prefs synchronize];
            [self redirectPushNotifToPromoView];
        }
    }
    else {
        
        if(type == NOTIFICATION_REQ || type == CHECK_USAGE_XL_REQ || type == PROMO_REQ)
        {
            [self.hud hide:YES];
            if(type == NOTIFICATION_REQ)
            {
                [self showNotificationView];
            }
        }
        
        if (type == PROFILE_REQ) {
            // TODO : V1.9
            // Loading Time
            _dateToday = nil;
            
            _isAutoLogin = NO;
            // TODO : Hygiene
            _isOffNet = YES;
            
            [self.splashViewController.loadingView stopAnimating];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.window cache:YES];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationDelegate:self];
            [self.splashViewController.view removeFromSuperview];
            [UIView commitAnimations];
            
            [self createHomeViewController];
            
            // TODO : V1.9
            [self updateVersionCheckWithNegativeSelector:@selector(showLoginView)];
//            [self showLoginView];
        }
        
        else if (type == REG_DEV_TOKEN_REQ) {
            [self requestMenu];
            //[self dummyDataMenu];
        }
        else {
            NSString *reason = [result valueForKey:REASON_KEY];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                            message:reason
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
}

#pragma mark - Logo Delegate

- (void)requestRetinaLogoCompleted:(ASIHTTPRequest *)request {
    
    UIImage *imageValue = [UIImage imageWithData:[request responseData]];
    NSString *logoName = @"";
    
    DataManager *sharedData = [DataManager sharedInstance];
    LogoModel *logoModel1 = [sharedData.profileData.logo objectAtIndex:1];
    
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    
    saveImage(imageValue, logoName);
    
    // 7. Set Preferences
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *key = @"";
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        key = XL_LOGO_RETINA;
    }
    else {
        key = AXIS_LOGO_RETINA;
    }
    [prefs setObject:logoModel1.version
              forKey:key];
    [prefs synchronize];
    
    // 8. Set Logo
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *documentsDirectory = applicationDocumentsDirectory();
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",logoName]];
    [appDelegate.customNavBar updateLogo:fullPath];
    
    // TODO : V1.9
    [self updateVersionCheckWithNegativeSelector:@selector(requestMenu)];
//    [self requestMenu];
}

- (void)requestLogoFailed:(ASIHTTPRequest *)request {
    //NSError *error = [request error];
    //NSLog(@"error = %@",error);
    
    UIImage *imageValue = nil;
    NSString *logoName = @"";
    
    DataManager *sharedData = [DataManager sharedInstance];
    LogoModel *logoModel1 = [sharedData.profileData.logo objectAtIndex:1];
    
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    
    saveImage(imageValue, logoName);
    
    // 7. Set Preferences
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *key = @"";
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        key = XL_LOGO_RETINA;
    }
    else {
        key = AXIS_LOGO_RETINA;
    }
    [prefs setObject:logoModel1.version
              forKey:key];
    [prefs synchronize];
    
    // 8. Set Logo
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *documentsDirectory = applicationDocumentsDirectory();
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",logoName]];
    [appDelegate.customNavBar updateLogo:fullPath];
    
    // TODO : V1.9
    [self updateVersionCheckWithNegativeSelector:@selector(requestMenu)];
    
    // Req Menu
//    [self requestMenu];
}

-(void)updateVersionCheckWithNegativeSelector:(SEL)selector
{
    // TODO : V1.9
    // NEED TO PUT VERSION CHECK BEFORE THIS LINE
    DataManager *sharedData = [DataManager sharedInstance];
    if(sharedData.updateVersionModel)
    {
        if([sharedData.updateVersionModel.code integerValue] == kMajorNotificationUpdate)
        {
            [self applicationHasMajorUpdateWithMessage:sharedData.updateVersionModel.message andPosBtnTitle:sharedData.updateVersionModel.posBtnTitle];
        }
        else if([sharedData.updateVersionModel.code integerValue] == kMinorNotificationUpdate)
        {
            [self applicationHasMinorUpdateWithMessage:sharedData.updateVersionModel.message andPosBtnTitle:sharedData.updateVersionModel.posBtnTitle andNegativeBtnTitle:sharedData.updateVersionModel.cancelBtnTitle];
        }
        else
        {
            if([self respondsToSelector:selector])
                [self performSelector:selector withObject:nil afterDelay:0];
        }
    }
    else
    {
        // Go straight to getmenu if there's no update
        // Req Menu
        if([self respondsToSelector:selector])
            [self performSelector:selector withObject:nil afterDelay:0];
    }
}

#pragma mark - Callback

// TODO : TESTING

//
//- (void)requestProfileDone:(ASIHTTPRequest *)request
//{
//    //[MBProgressHUD hideHUDForView:self.view animated:YES];
//    
//    NSString *response = [request responseString];
//    //NSLog(@"responseString = %@",response);
//    
//    DataParser *dataParser = [[DataParser alloc] init];
//    dataParser.delegate = self;
//    [dataParser parseGetProfileData:response];
//    [dataParser release];
//}
//
//- (void)requestProfileWentWrong:(ASIHTTPRequest *)request
//{
//    //[MBProgressHUD hideHUDForView:self.view animated:YES];
//    
//    //_isSuccessRequest = NO;
//    
//    /*
//    NSError *error = [request error];
//    NSLog(@"error = %@",error);
//    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ERROR"
//                                                    message:[NSString stringWithFormat:@"%@",error]
//                                                   delegate:self
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//    [alert show];
//    [alert release];*/
//    
//    [self showLoginView];
//}
//
//- (void)requestMenuDone:(ASIHTTPRequest *)request
//{
//    //[MBProgressHUD hideHUDForView:self.view animated:YES];
//    
//    NSString *response = [request responseString];
//    //NSLog(@"responseString = %@",response);
//    
//    DataParser *dataParser = [[DataParser alloc] init];
//    dataParser.delegate = self;
//    [dataParser parseGetMenuData:response];
//    [dataParser release];
//}
//
//- (void)requestMenuWentWrong:(ASIHTTPRequest *)request
//{
//    //[MBProgressHUD hideHUDForView:self.view animated:YES];
//    
//    //_isSuccessRequest = NO;
//    
//    NSError *error = [request error];
//    //NSLog(@"error = %@",error);
//    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
//                                                    message:[NSString stringWithFormat:@"%@",error]
//                                                   delegate:self
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//    [alert show];
//    [alert release];
//}

#pragma mark - DataParserDelegate

- (void)parseDataDone:(BOOL)success withReason:(NSString*)reason {
    if (success) {
        /*
        DataManager *sharedData = [DataManager sharedInstance];
        
        AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[self.navigationController navigationBar];
        
        NSString *saltKeyAPI = SALT_KEY;
        //msisdn = [EncryptDecrypt doCipher:msisdn action:kCCEncrypt withKey:saltKeyAPI];
        //NSLog(@"msisdn API encrypt = %@",msisdn);
        
        NSString *msisdn = [EncryptDecrypt doCipher:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
        NSLog(@"msisdn API decrypt = %@",msisdn);
        
        navBar.msisdnLabel.text = msisdn;
        
        _isAutoLogin = YES;
        [self requestMenu];
        //[self dummyDataMenu];*/
        
        if (!_isAutoLogin) {
            [self showHomeView];
        }
        else {
            [self.homeViewController updateView];
        }
        
    }
    else {
        /*
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ERROR"
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];*/
        
        _isAutoLogin = NO;
        
        // TODO : Hygiene
        _isOffNet = YES;
        [self showLoginView];
        
        // TODO : Hygiene
        // Must implement redirection to registration page when network is OFFNET
        
    }
}

- (void)parseMenuDone:(BOOL)success withReason:(NSString*)reason {
    if (success) {
        if (!_isAutoLogin) {
            [self showHomeView];
        }
        else {
            [self.homeViewController updateView];
        }
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - LoginViewControllerDelegate

- (void)viewControllerDidLoginSuccess:(LoginViewController*)viewController
{
    DataManager *sharedData = [DataManager sharedInstance];
    AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[self.navigationController navigationBar];
    // Change NavBar BgColor
    changeNavbarBg(navBar, self.window.bounds);
    //----------------------
    NSString *saltKeyAPI = SALT_KEY;
    //msisdn = [EncryptDecrypt doCipher:msisdn action:kCCEncrypt withKey:saltKeyAPI];
    //NSLog(@"msisdn API encrypt = %@",msisdn);
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    //NSLog(@"msisdn API decrypt = %@",msisdn);
    if ([msisdn length] > 0) {
        msisdn = [msisdn stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"0"];
    }
    navBar.hiLabel.text = [NSString stringWithFormat:@"%@, %@",[Language get:@"hi" alter:nil],sharedData.profileData.fullName];
    navBar.msisdnLabel.text = [NSString stringWithFormat:@"%@",msisdn];
    //navBar.deviceLabel.text = sharedData.profileData.device;
    
    self.loggedIn = YES;
    
    //[self requestMenu];
    
    //UIViewController *topViewController = [self.window rootViewController];
    //[MBProgressHUD hideHUDForView:topViewController.view animated:YES];
    
    [self showHomeView];
    
    /*
    
#if TARGET_IPHONE_SIMULATOR
	
	[self requestMenu];
    //[self dummyDataMenu];
    
#else
    
    //-- Register device token for Push Notification changes on 21 Nov 2012 by iNot --
    [self registerDeviceToken];
    //-- end --
	
#endif*/
    
}

#pragma mark - UIAlertView Delegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)index {
    NSLog(@"Getting into alert handler");
    if (alertView == _notifAlert) {
        if (index != 0) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        }
    }
    /*
    else {
        if (index != alertView.cancelButtonIndex) {
            [[DBSession sharedSession] linkUserId:relinkUserId];
        }
        [relinkUserId release];
        relinkUserId = nil;
    }*/
    
    if (alertView.tag == 101) {
        
        NSString *iTunesLink = @"https://itunes.apple.com/id/app/axis-net/id497146073?l=en&mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.com/apps/axisnet"]];
    }
    // TODO : HYGIENE
    else if(alertView.tag == 999)
    {
        NSLog(@"getting into push notification alert");
        if(index != 0)
        {
            DataManager *sharedData = [DataManager sharedInstance];
            NSString *pageID = [sharedData.remoteNotificationInfo valueForKey:@"page_id"];
            if([pageID isEqual:@"1"])
            {
                NSLog(@"getting into pageID 1 %@", pageID);
                [self requestNotification];
            }
            else if([pageID isEqual:@"2"])
            {
                NSLog(@"getting into pageID 2 %@", pageID);
                [self requestCheckUsageXL];
            }
            else if([pageID isEqual:@"3"])
            {
                NSLog(@"getting into pageID 3 %@", pageID);
                [self showReloadBalanceView];
            }
            else if([pageID isEqual:@"4"])
            {
                // promo
                NSLog(@"getting into pageID 4 %@", pageID);
                [self showPromoView];
            }
        }
    }
    // TODO : V1.9
    // Application version update notification
    else if(alertView.tag == kMajorNotificationUpdate)
    {
        [self updateApplication];
    }
    else if(alertView.tag == kMinorNotificationUpdate)
    {
        if(index > 0)
        {
            [self updateApplication];
        }
        else
        {
            // TODO : V1.9
            // check if the login is manual or auto
            UIViewController *rootViewController = [self.window rootViewController];
            if(self.loginNavigationController && rootViewController.presentedViewController == self.loginNavigationController)
            {
                LoginViewController *loginVC = (LoginViewController *)self.loginNavigationController.topViewController;
                if(loginVC)
                {
                    if([loginVC respondsToSelector:@selector(requestMenu)])
                    {
                        [loginVC performSelector:@selector(requestMenu) withObject:nil];
                    }
                }
            }
            else
            {
                [self requestMenu];
            }
        }
    }
}

/**
 *  Open AppStore and show the new version of the application
 */
-(void)updateApplication
{
    DataManager *sharedData = [DataManager sharedInstance];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sharedData.updateVersionModel.url]];
}

// TODO : HYGIENE - PUSH NOTIF
#pragma mark - Push notification Handler
- (void)requestNotification
{
    UIViewController *topViewController = [self.window rootViewController];
    [self.hud hide:YES];
    self.hud = [MBProgressHUD showHUDAddedTo:topViewController.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];

    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = NOTIFICATION_REQ;
    [request notification];
}

// TODO : V1.9
// Redirect to MyPackage View from push notification. PageID = 2
-(void)showQuotaUsageView
{
    if(![[self.navigationController topViewController] isKindOfClass:[CheckQuotaViewController class]])
    {
        if([[self.navigationController topViewController] isKindOfClass:[IIWrapController class]])
        {
            DataManager *sharedData = [DataManager sharedInstance];
            LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
            leftViewController.content = sharedData.leftMenuData;
            
            CheckQuotaViewController *checkQuotaVC = [[CheckQuotaViewController alloc] initWithNibName:@"CheckQuotaViewController" bundle:nil];
            checkQuotaVC.isLevel2 = YES;
            
            UIViewController *packageViewController = checkQuotaVC;
            
            IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:packageViewController leftViewController:leftViewController];
            deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
            deckController.leftSize = [UIScreen mainScreen].bounds.size.width - 260;
            packageViewController = deckController;
            
            UINavigationController *navController = customizedNavigationController([UIScreen mainScreen].bounds,NO);
            [navController setViewControllers:[NSArray arrayWithObject:packageViewController]];
        
            packageViewController = [[IIWrapController alloc] initWithViewController:navController];
            
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            [self.navigationController pushViewController:packageViewController animated:YES];
        }
        else
        {
            CheckQuotaViewController *checkQuotaVC = [[CheckQuotaViewController alloc] initWithNibName:@"CheckQuotaViewController" bundle:nil];
            checkQuotaVC.isLevel2 = YES;
            [checkQuotaVC createBarButtonItem:BACK];
            [self.navigationController pushViewController:checkQuotaVC animated:YES];
            [checkQuotaVC release];
            checkQuotaVC = nil;
        }
    }
}

// Redirect to ReloadBalance from push notification. PageID = 3
-(void)showReloadBalanceView
{
    if(![[self.navigationController topViewController] isKindOfClass:[PaymentChoiceViewController class]])
    {
        DataManager *sharedData = [DataManager sharedInstance];
        NSArray *contentTmp = nil;
        NSDictionary *dict = [GroupingAndSorting doGrouping:sharedData.homeMenu];
        NSArray *objectsForMenu = [dict objectForKey:@"20"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"href==%@",@"#reload_pg"];
        NSArray *filteredArray = [objectsForMenu filteredArrayUsingPredicate:predicate];
        if([[self.navigationController topViewController] isKindOfClass:[IIWrapController class]])
        {
            IIWrapController *iWrapController = (IIWrapController *)self.navigationController.topViewController;
            NSLog(@"Wrapped Controller %@", iWrapController.wrappedController.class);
            LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
            leftViewController.content = sharedData.leftMenuData;
            
            PaymentChoiceViewController *subMenuVC = [[PaymentChoiceViewController alloc] initWithNibName:@"PaymentChoiceViewController" bundle:nil];
            
            if([filteredArray count] > 0)
            {
                MenuModel *menuModel = [filteredArray objectAtIndex:0];
                contentTmp = [sharedData.menuData valueForKey:menuModel.menuId]; //-> 5 is menuID for reloadBalance
                // Grouping
                NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
                
                // Sorting
                NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
                
                NSArray *paymentMenu = [sortingResult allKeys];
                paymentMenu = [GroupingAndSorting doSortingByGroupId:paymentMenu];
                
                NSMutableArray *paymentMenuTmp = [[NSMutableArray alloc] init];
                for (int i=0; i<[paymentMenu count]; i++) {
                    NSString *groupIdOfMenu = [paymentMenu objectAtIndex:i];
                    NSArray *objectsForMenu = [sortingResult objectForKey:groupIdOfMenu];
                    for (MenuModel *menu in objectsForMenu) {
                        [paymentMenuTmp addObject:menu];
                    }
                }
                
                subMenuVC.paymentMenu = paymentMenuTmp;
                subMenuVC.grayHeaderTitle = [Language get:@"balance_management" alter:nil];
                subMenuVC.blueHeaderTitle = menuModel.menuName;
                subMenuVC.isLevel2 = YES;
                
                //---------------------------//
                
                UIViewController *infoViewController = subMenuVC;
                
                IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:infoViewController leftViewController:leftViewController];
                deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
                deckController.leftSize = [UIScreen mainScreen].bounds.size.width - 260; //270
                infoViewController = deckController;
                
                UINavigationController *navController = customizedNavigationController([UIScreen mainScreen].bounds,NO);
                [navController setViewControllers:[NSArray arrayWithObject:infoViewController]];
                
                infoViewController = [[IIWrapController alloc] initWithViewController:navController];
                
                [self.navigationController setNavigationBarHidden:YES animated:NO];
                [self.navigationController pushViewController:infoViewController animated:YES];
                
                [paymentMenuTmp release];
            }
        }
        else
        {
            NSLog(@"Reload balance");
            if([filteredArray count] > 0)
            {
                MenuModel *menuModel = [filteredArray objectAtIndex:0];
                contentTmp = [sharedData.menuData valueForKey:menuModel.menuId]; //-> 5 is menuID for reloadBalance
                // Grouping
                NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
                
                // Sorting
                NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
                
                NSArray *paymentMenu = [sortingResult allKeys];
                paymentMenu = [GroupingAndSorting doSortingByGroupId:paymentMenu];
                
                NSMutableArray *paymentMenuTmp = [[NSMutableArray alloc] init];
                for (int i=0; i<[paymentMenu count]; i++) {
                    NSString *groupIdOfMenu = [paymentMenu objectAtIndex:i];
                    NSArray *objectsForMenu = [sortingResult objectForKey:groupIdOfMenu];
                    for (MenuModel *menu in objectsForMenu) {
                        [paymentMenuTmp addObject:menu];
                    }
                }
                PaymentChoiceViewController *subMenuVC = [[PaymentChoiceViewController alloc] initWithNibName:@"PaymentChoiceViewController" bundle:nil];
                subMenuVC.paymentMenu = paymentMenuTmp;
                subMenuVC.grayHeaderTitle = [Language get:@"balance_management" alter:nil];
                subMenuVC.blueHeaderTitle = menuModel.menuName;
                subMenuVC.isLevel2 = YES;
                [subMenuVC createBarButtonItem:BACK];
                [self.navigationController pushViewController:subMenuVC animated:YES];
                [subMenuVC release];
                subMenuVC = nil;
            }
        }
    }
}

// Redirect to PromoView from push notification. PageID = 4
-(void)showPromoView
{
    if(![[self.navigationController topViewController] isKindOfClass:[ContactUsViewController class]])
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSDate *promoCacheDate = [prefs objectForKey:PROMO_CACHE_KEY];
        NSDate *currentDate = [NSDate date];
        NSTimeInterval secondsBetween = [currentDate timeIntervalSinceDate:promoCacheDate];
        float minutes = secondsBetween / 60;
        
        if (promoCacheDate == nil || minutes > kDefaultCache) {
            UIViewController *topViewController = [self.window rootViewController];
            [self.hud hide:YES];
            self.hud = [MBProgressHUD showHUDAddedTo:topViewController.view animated:YES];
            self.hud.labelText = [Language get:@"loading" alter:nil];
            
            AXISnetRequest *request = [AXISnetRequest sharedInstance];
            request.delegate = self;
            request.requestType = PROMO_REQ;
            [request promo];
        }
        else
        {
            [self redirectPushNotifToPromoView];
        }
    }
}

-(void)redirectPushNotifToPromoView
{
    DataManager *sharedData = [DataManager sharedInstance];
    NSDictionary *dict = [GroupingAndSorting doGrouping:sharedData.homeMenu];
    NSArray *objectsForMenu = [dict objectForKey:@"20"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"href==%@",@"#promo"];
    NSArray *filteredArray = [objectsForMenu filteredArrayUsingPredicate:predicate];
    if([filteredArray count] > 0)
    {
        MenuModel *menuModel = [filteredArray objectAtIndex:0];
        sharedData.parentId = menuModel.menuId;
        sharedData.menuModel = menuModel;
        if([[self.navigationController topViewController] isKindOfClass:[IIWrapController class]])
        {
            IIWrapController *iWrapController = (IIWrapController *)self.navigationController.topViewController;
            NSLog(@"Wrapped Controller %@", iWrapController.wrappedController.class);
            LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
            leftViewController.content = sharedData.leftMenuData;
            
            ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
            contactUsViewController.content = sharedData.promoData;
            contactUsViewController.accordionType = PROMO_ACC;
            contactUsViewController.menuModel = menuModel;
            contactUsViewController.isLevel2 = YES;
            
            UIViewController *infoViewController = contactUsViewController;
            
            IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:infoViewController leftViewController:leftViewController];
            deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
            deckController.leftSize = [UIScreen mainScreen].bounds.size.width - 260; //270
            infoViewController = deckController;
            
            UINavigationController *navController = customizedNavigationController([UIScreen mainScreen].bounds,NO);
            [navController setViewControllers:[NSArray arrayWithObject:infoViewController]];
            
            infoViewController = [[IIWrapController alloc] initWithViewController:navController];
            
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            [self.navigationController pushViewController:infoViewController animated:YES];
        }
        else
        {
            ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
            contactUsViewController.content = sharedData.promoData;
            contactUsViewController.accordionType = PROMO_ACC;
            contactUsViewController.menuModel = menuModel;
            contactUsViewController.isLevel2 = YES;
            [contactUsViewController createBarButtonItem:BACK];
            [self.navigationController pushViewController:contactUsViewController animated:YES];
            [contactUsViewController release];
            contactUsViewController = nil;
        }
    }
}

// TODO : HYGIENE - PUSH NOTIF
// Redirect to Inbox Notification View. PageID = 1
-(void)showNotificationView
{
    if(![[self.navigationController topViewController] isKindOfClass:[NotificationViewController class]])
    {
        if([[self.navigationController topViewController] isKindOfClass:[IIWrapController class]])
        {
            DataManager *sharedData = [DataManager sharedInstance];
            LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
            leftViewController.content = sharedData.leftMenuData;
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            notificationViewController.content = sharedData.notificationData;
            notificationViewController.shouldProvideBackButton = YES;
            notificationViewController.isCustomBack = YES;
            [notificationViewController createBarButtonItem:BACK];
            
            UIViewController *infoViewController = notificationViewController;
            
            IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:infoViewController leftViewController:leftViewController];
            deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
            deckController.leftSize = [UIScreen mainScreen].bounds.size.width - 260; //270
            infoViewController = deckController;
            
            UINavigationController *navController = customizedNavigationController([UIScreen mainScreen].bounds,NO);
            // Change NavBar BgColor
            AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[navController navigationBar];
            changeNavbarBg(navBar, app.window.bounds);
            //----------------------
            [navController setViewControllers:[NSArray arrayWithObject:infoViewController]];
            
            infoViewController = [[IIWrapController alloc] initWithViewController:navController];
            
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            [self.navigationController pushViewController:infoViewController animated:YES];
        }
        else
        {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            [notificationViewController createBarButtonItem:BACK];
            [self.navigationController pushViewController:notificationViewController animated:YES];
            [notificationViewController release];
            notificationViewController = nil;
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRefreshTable object:nil];
    }
}

// TODO : V1.9
// Application version update handler
#pragma mark - Application Version Update Notification
/**
 *  Shows pop up notification when there's a major update
 *
 *  @param message The update notification message
 *
 *  @param btnTitle The positive button title
 */
-(void)applicationHasMajorUpdateWithMessage:(NSString *)message andPosBtnTitle:(NSString *)btnTitle
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:[btnTitle uppercaseString]
                                          otherButtonTitles:nil];
    alert.tag = kMajorNotificationUpdate;
    [alert show];
    [alert release];
}

/**
 *  Shows pop up notification when there's a major update
 *
 *  @param message The update notification message
 *
 *  @param btnTitle The positive button title
 *
 *  @param cancelBtnTitle The negative button title
 */
-(void)applicationHasMinorUpdateWithMessage:(NSString *)message andPosBtnTitle:(NSString *)btnTitle andNegativeBtnTitle:(NSString *)cancelBtnTitle
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:[cancelBtnTitle uppercaseString]
                                          otherButtonTitles:[btnTitle uppercaseString],nil];
    alert.tag = kMinorNotificationUpdate;
    [alert show];
    [alert release];
}

#pragma mark - Facebook Integration

//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation
//{
//    // attempt to extract a token from the url
////    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
//    BOOL urlWasHandled = [FBAppCall handleOpenURL:url
//                                sourceApplication:sourceApplication
//                                  fallbackHandler:^(FBAppCall *call) {
//                                      NSLog(@"Unhandled deep link: %@", url);
//                                      // Here goes the code to handle the links
//                                      // Use the links to show a relevant view of your app to the user
//                                  }];
//    
//    if ([[url scheme] isEqualToString:@"myxl"] == NO) return NO;
//    
//    NSDictionary *d = [self parametersDictionaryFromQueryString:[url query]];
//    
//    NSString *token = d[@"oauth_token"];
//    NSString *verifier = d[@"oauth_verifier"];
//    
//    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:token,@"token",verifier,@"verifier", nil];
//    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"TWITTER_AUTH" object:nil userInfo:userInfo];
//    
//    return YES;
//}
//
//-(NSDictionary *)parametersDictionaryFromQueryString:(NSString *)queryString {
//    
//    NSMutableDictionary *md = [NSMutableDictionary dictionary];
//    
//    NSArray *queryComponents = [queryString componentsSeparatedByString:@"&"];
//    
//    for(NSString *s in queryComponents) {
//        NSArray *pair = [s componentsSeparatedByString:@"="];
//        if([pair count] != 2) continue;
//        
//        NSString *key = pair[0];
//        NSString *value = pair[1];
//        
//        md[key] = value;
//    }
//    
//    return md;
//}

// This method will handle ALL the session state changes in the app
//- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
//{
//    // If the session was opened successfully
//    if (!error && state == FBSessionStateOpen)
//    {
//        @try
//        {
//            
//            NSLog(@"Session opened");
//            DataManager *sharedData = [DataManager sharedInstance];
//            FacebookModel *fbModel = [[[FacebookModel alloc] init] autorelease];
//            [FBRequestConnection startWithGraphPath:@"/me"
//                                         parameters:nil
//                                         HTTPMethod:@"GET"
//                                  completionHandler:^(
//                                                      FBRequestConnection *connection,
//                                                      NSDictionary *result,
//                                                      NSError *error
//                                                      ) {
//                                      /* handle the result */
//                                      if(!error)
//                                      {
//                                          NSLog(@"result %@", result);
//                                          fbModel.fbEmail = [result objectForKey:@"email"];
//                                          fbModel.fbName = [result objectForKey:@"name"];
//                                          fbModel.fbGender = [result objectForKey:@"gender"];
////                                          sharedData.fbData = fbModel;
//                                      }
//                                  }];
//            
//            // Show the user the logged-in UI
//            //        [self userLoggedIn];
//            return;
//        }
//        @catch (NSException *exception) {
//            NSLog(@"Exception %@", [exception reason]);
//        }
//    }
//    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
//        // If the session is closed
//        NSLog(@"Session closed");
//        DataManager *sharedData = [DataManager sharedInstance];
////        sharedData.fbData = nil;
//        // Show the user the logged-out UI
//        //        [self userLoggedOut];
//    }
//    
//    // Handle errors
//    if (error)
//    {
//        NSLog(@"Error");
//        NSString *alertText;
//        NSString *alertTitle;
//        // If the error requires people using an app to make an action outside of the app in order to recover
//        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
////            alertTitle = @"Something went wrong";
////            alertText = [FBErrorUtility userMessageForError:error];
//        } else {
//            
//            // If the user cancelled login, do nothing
//            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
//                NSLog(@"User cancelled login");
//                
//                // Handle session closures that happen outside of the app
//            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
//                alertTitle = @"Session Error";
//                alertText = @"Your current session is no longer valid. Please log in again.";
//                
//                // Here we will handle all other errors with a generic error message.
//                // We recommend you check our Handling Errors guide for more information
//                // https://developers.facebook.com/docs/ios/errors/
//            } else
//            {
//                //Get more error information from the error
//                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
//                
//                // Show the user an error message
//                alertTitle = @"Something went wrong";
//                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
//            }
//        }
//        // Clear this token
//        [FBSession.activeSession closeAndClearTokenInformation];
//        // Show the user the logged-out UI
//        //        [self userLoggedOut];
//    }
//}

@end
