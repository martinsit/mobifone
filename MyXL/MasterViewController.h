//
//  MasterViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/5/12.
//  Copyright (c) 2012 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSNavigationPaneViewController.h"
#import "WEPopoverController.h"

typedef NS_ENUM(NSUInteger, MSPaneViewControllerType) {
    MSPaneViewControllerType1,
    MSPaneViewControllerType2,
    MSPaneViewControllerType3,
    MSPaneViewControllerType4,
    MSPaneViewControllerType5,
    MSPaneViewControllerType6,
    MSPaneViewControllerTypeCount,
};

@interface MasterViewController : UITableViewController <PopoverControllerDelegate> {
    WEPopoverController *navPopover;
    CGRect accountFrame;
}

@property (nonatomic, assign) MSPaneViewControllerType paneViewControllerType;
@property (nonatomic, retain) MSNavigationPaneViewController *navigationPaneViewController;

@property (nonatomic, retain) NSArray *content;

- (void)transitionToViewController:(MSPaneViewControllerType)paneViewControllerType;

@end
