//
//  AppDelegate.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/5/12.
//  Copyright (c) 2012 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataParser.h"
#import "AXISnetRequest.h"
#import "LoginViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "MBProgressHUD.h"

#import "GAI.h"

#import "AXISnetNavigationBar.h"

//extern NSString *const FBSessionStateChangedNotification;

@class MSNavigationPaneViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, DataParserDelegate, AXISnetRequestDelegate, LoginViewControllerDelegate, UIAlertViewDelegate> {
    
    NSString *relinkUserId;
    NSString *viewId;
    
    MBProgressHUD *hud;
    
    AXISnetNavigationBar *customNavBar;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@property (strong, nonatomic) UINavigationController *loginNavigationController;

@property (strong, nonatomic) MSNavigationPaneViewController *navigationPaneViewController;

@property (readwrite) BOOL loggedIn;

@property (readwrite) BOOL isAutoLogin;

@property (strong, nonatomic) FBSession *session;

@property (strong, nonatomic) MBProgressHUD *hud;

@property (nonatomic, assign) id<GAITracker> tracker;

@property (strong, nonatomic) AXISnetNavigationBar *customNavBar;

// TODO : Hygiene
@property (readwrite) BOOL isOffNet;
@property (nonatomic, retain) NSString *deviceToken;

// TODO : V1.9
// Loading Time
@property (nonatomic, retain) NSDate *dateToday;

//- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;

- (void)requestMenu;
- (void)showLoginView;
- (void)requestNotification;
// TODO : Update Hygiene Defect
- (void)registerDeviceToken;

// TODO : V1.9
// Version update notification
-(void)applicationHasMajorUpdateWithMessage:(NSString *)message andPosBtnTitle:(NSString *)btnTitle;
-(void)applicationHasMinorUpdateWithMessage:(NSString *)message andPosBtnTitle:(NSString *)btnTitle andNegativeBtnTitle:(NSString *)cancelBtnTitle;

-(void)showReloadBalanceView;

@end
