//
//  CategoryViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 7/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CategoryViewController.h"
#import "CommonCell.h"
#import "AXISnetCellBackground.h"
#import "AXISnetCommon.h"
#import "SectionFooterView.h"
#import "SectionHeaderView.h"
#import "Constant.h"
#import "MTPopupWindow.h"
#import "DataManager.h"
#import "Constant.h"
#import "ProductCatalogueViewController.h"

@interface CategoryViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (strong, nonatomic) IBOutlet UIView *blockBgView;
@property (strong, nonatomic) IBOutlet UIView *popUpView;
@property (strong, nonatomic) IBOutlet UIWebView *theWebView;

- (IBAction)closePopUp:(id)sender;

@end

@implementation CategoryViewController

@synthesize content = _content;
@synthesize selectedMenuModel = _selectedMenuModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.content = nil;
    self.selectedMenuModel = nil;
}

- (void)dealloc {
    [_content release];
    _content = nil;
    [_selectedMenuModel release];
    _selectedMenuModel = nil;
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_content count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierCommon = @"Common";
    
    CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierCommon];
    
    if (cell == nil) {
        cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierCommon];
        
        [cell.infoButton addTarget:self
                            action:@selector(performInfo:)
                  forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.backgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
    cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
    ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
    
    MenuModel *menuModel = [_content objectAtIndex:indexPath.row];
    
    cell.iconLabel.text = icon(@"7");
    
    cell.titleLabel.text = [menuModel.menuName uppercaseString];
    CGSize size = calculateExpectedSize(cell.titleLabel, cell.titleLabel.text);
    CGRect newFrame = cell.titleLabel.frame;
    newFrame.size.height = size.height;
    cell.titleLabel.frame = newFrame;
    cell.needResize = YES;
    
    if ([menuModel.desc length] > 0) {
        cell.arrowLabel.hidden = YES;
        cell.infoButton.hidden = NO;
    }
    else {
        cell.arrowLabel.hidden = NO;
        cell.infoButton.hidden = YES;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.headerTitleMaster;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat labelHeightDefault = 28.0;
    
    MenuModel *menuModel = [self.content objectAtIndex:indexPath.row];
    
    NSString *text = [menuModel.menuName uppercaseString];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 240.0, 28.0)];
    CGSize size = calculateExpectedSize(label, text);
    
    CGFloat extraHeightForTitle = 0.0;
    if (size.height > labelHeightDefault) {
        extraHeightForTitle = size.height - labelHeightDefault;
    }
    
    return kDefaultCellHeight + extraHeightForTitle;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuModel *menuModel = [_content objectAtIndex:indexPath.row];
    _selectedMenuModel = menuModel;
    [self performProduct:menuModel.menuId];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:NO] autorelease];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

#pragma mark - Selector

- (void)performInfo:(id)sender {
    UIButton *button = (UIButton *)sender;
    CommonCell *cell = (CommonCell *)[[button superview] superview];
    int section = [_theTableView indexPathForCell:cell].section;
    int row = [_theTableView indexPathForCell:cell].row;
    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
    
    MenuModel *menuModel = [_content objectAtIndex:selectedIndexPath.row];
    
    NSString *body = [NSString stringWithFormat:@"<html><head><style> body, label,table, td {font-family:Helvetica; font-size:12px} p {padding:0;margin:0} label {font-weight:bold} p.Q {font-size:16px; color:#B5005B;font-weight:bold} .left-pad {margin-top:0;padding-left:7px;} .left-pad li {margin-left:10px !important;overflow:visible !important;} .tbl-faq {text-align:center;font-size:12px;color:#333333;border:1px solid #666666;border-collapse: collapse;} .tbl-faq.head {background-color: #dedede;} .tbl-faq.body {background-color:#ffffff; font-weight:normal;} </style> </head> <body> <p class=\"Q\"></p><p class=\"A\"><br/>%@</p> </body> </html>",menuModel.desc];
    
    NSString *reqSysVer = @"6.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending) {
        [MTPopupWindow showWindowWithHTMLFile:body insideView:self.view];
    }
    else {
        _blockBgView.hidden = NO;
        _popUpView.hidden = NO;
        [_theWebView loadHTMLString:body baseURL:nil];
    }
    
    [_theTableView reloadData];
}

- (IBAction)closePopUp:(id)sender {
    _blockBgView.hidden = YES;
    _popUpView.hidden = YES;
}

- (void)performProduct:(NSString*)categoryId {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = AXIS_STORE_PRODUCT_REQ;
    
    [request productAXISStore:@""
              withProductName:@""
               withCategoryId:categoryId
                  withKeyword:@""
                    withLimit:@"5"
                     withPage:@""];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == AXIS_STORE_PRODUCT_REQ) {
            //NSLog(@"Go to catalogue");
            ProductCatalogueViewController *productVC = [[ProductCatalogueViewController alloc] initWithNibName:@"ProductCatalogueViewController" bundle:nil];
            productVC.headerTitleMaster = _selectedMenuModel.menuName;
            productVC.headerIconMaster = icon(@"7");
            productVC.content = sharedData.productData;
            
            [self.navigationController pushViewController:productVC animated:YES];
            productVC = nil;
            [productVC release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
