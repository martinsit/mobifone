//
//  CategoryViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 7/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "MenuModel.h"

@interface CategoryViewController : BasicViewController {
    NSArray *_content;
    MenuModel *_selectedMenuModel;
}

@property (nonatomic, retain) NSArray *content;
@property (nonatomic, retain) MenuModel *selectedMenuModel;

@end
