//
//  ASTermViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 7/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface ASTermViewController : BasicViewController <UIWebViewDelegate> {
    NSString *_htmlString;
}

@property (nonatomic, retain) NSString *htmlString;

@end
