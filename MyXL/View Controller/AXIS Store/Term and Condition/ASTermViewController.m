//
//  ASTermViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 7/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ASTermViewController.h"
//#import "DataManager.h"
#import "Constant.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

@interface ASTermViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;
@property (strong, nonatomic) ContentView *containerView;
@property (strong, nonatomic) IBOutlet UIWebView *helpView;

@end

@implementation ASTermViewController

@synthesize htmlString = _htmlString;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _helpView.frame.origin.y + _helpView.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    
    /*
     if (_sectionHeaderView) {
     [_sectionHeaderView removeFromSuperview];
     }*/
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:@""];
    _sectionHeaderView = headerView;
    _sectionHeaderView.icon = self.headerIconMaster;
    _sectionHeaderView.title = self.headerTitleMaster;
    [_contentView addSubview:_sectionHeaderView];
    [_contentView sendSubviewToBack:_sectionHeaderView];
    [headerView release];
    
    /*
     if (_containerView) {
     [_containerView removeFromSuperview];
     }*/
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    _containerView = contentViewTmp;
    [_contentView addSubview:_containerView];
    [_contentView sendSubviewToBack:_containerView];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 150.0);
    
    //---------------//
    // Setup WebView //
    //---------------//
    
    //DataManager *sharedData = [DataManager sharedInstance];
    //NSString *html = [NSString stringWithFormat:@"<html><head><style>body {background:transparent; font-family:'Helvetica Neue', Helvetica; font-size:14px; color:#662584;} h2{margin:0 0 6px; font-size:16px;} p{line-height:18px; margin:0 0 10px} a{color:#B5005B; text-decoration:none; font-weight:bold;}</style></head><body><br/><h2>%@</h2><p>%@</p><p>%@</p><p>&nbsp;</p><p>&nbsp;</p></body></html>",title,body,href];
    
    //NSString *html = [NSString stringWithFormat:@"<html><body>%@</body></html>",sharedData.termAndConditionData.label];
    NSString *html = [NSString stringWithFormat:@"<html><style> body, label,table, td {font-family:OpenSans-Light; font-size:12px} p {padding:0;margin:0} label {font-weight:bold} p.Q {font-size:16px; color:#B5005B;font-weight:bold} .left-pad {margin-top:0;padding-left:7px;} .left-pad li {margin-left:10px !important;overflow:visible !important;} .tbl-faq {text-align:center;font-size:12px;color:#333333;border:1px solid #666666;border-collapse: collapse;} .tbl-faq.head {background-color: #dedede;} .tbl-faq.body {background-color:#ffffff; font-weight:normal;} </style><body> %@ </body></html>",_htmlString];
    
    //NSString *body = sharedData.paymentHelpData;
    //NSString *html = [NSString stringWithFormat:@"<html><head><meta name=\"viewport\" content=\"initial-scale=1, user-scalable=no, width=device-width\" /></head><body>%@</body></html>",body];
    
    [_helpView loadHTMLString:[NSString stringWithFormat:@"%@",html] baseURL:nil];
    [_helpView setBackgroundColor:[UIColor clearColor]];
    
    for (UIView *wview in [[[_helpView subviews] objectAtIndex:0] subviews]) {
        if([wview isKindOfClass:[UIImageView class]])
        {
            wview.hidden = YES;
        }
    }
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    self.htmlString = nil;
}

- (void)dealloc {
    [_htmlString release];
    _htmlString = nil;
    
    [super dealloc];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    CGFloat webViewHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.height"] floatValue];
    
    CGRect frame = [_helpView frame];
    frame.size.height = webViewHeight;
    [_helpView setFrame:frame];
    
    //-----------------------//
    // Relayout Content View //
    //-----------------------//
    frame = [_contentView frame];
    frame.size.height = _helpView.frame.origin.y + _helpView.frame.size.height + kDefaultPadding;
    [_contentView setFrame:frame];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    
    NSArray *viewsToRemove = [_contentView subviews];
    for (UIView *v in viewsToRemove) {
        if ([v isKindOfClass:[ContentView class]]) {
            [v removeFromSuperview];
        }
    }
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //-------------------------------//
    // Relayout Grey Background View //
    //-------------------------------//
    CGFloat x = _contentView.frame.origin.x - kDefaultLeftPadding;
    CGFloat y = _contentView.frame.origin.y - kDefaultPadding;
    CGFloat width = _contentView.frame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = _contentView.frame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 150.0);
}

@end
