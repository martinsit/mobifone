//
//  DetailProductCatalogueViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 8/21/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "DetailProductCatalogueViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

#import "DataManager.h"

@interface DetailProductCatalogueViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *productNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (strong, nonatomic) IBOutlet UIWebView *descView;
@property (strong, nonatomic) UIButton *buyButton;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;
@property (strong, nonatomic) ContentView *containerView;

@end

@implementation DetailProductCatalogueViewController

@synthesize productModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //-------------//
    // Setup Image //
    //-------------//
    _imageView.image = [UIImage imageNamed:@"sp_kaskus_150.jpg"];
}

- (void)viewWillAppear:(BOOL)animated {
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    //-------------//
    // Setup Label //
    //-------------//
    _productNameLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _productNameLabel.textColor = kDefaultTitleFontGrayColor;
    _productNameLabel.text = [productModel.productName uppercaseString];
    
    _productPriceLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _productPriceLabel.textColor = kDefaultPurpleColor;
    _productPriceLabel.backgroundColor = [UIColor clearColor];
    _productPriceLabel.text = addThousandsSeparator(productModel.productPrice, sharedData.profileData.language);
    
    //--------------//
    // Setup Button //
    //--------------//
    // Next Button
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    
    if (_buyButton) {
        [_buyButton removeFromSuperview];
    }
    
    UIButton *button = createButtonWithFrame(CGRectMake(_descView.frame.origin.x,
                                                        _descView.frame.origin.y + _descView.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             
                                             [Language get:@"buy" alter:nil],
                                             buttonFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
    [button addTarget:self action:@selector(performBuy) forControlEvents:UIControlEventTouchUpInside];
    _buyButton = button;
    [_contentView addSubview:_buyButton];
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _buyButton.frame.origin.y + _buyButton.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:@"AXIS STORE"];
    _sectionHeaderView = headerView;
    _sectionHeaderView.icon = self.headerIconMaster;
    _sectionHeaderView.title = self.headerTitleMaster;
    [_contentView addSubview:_sectionHeaderView];
    [_contentView sendSubviewToBack:_sectionHeaderView];
    [headerView release];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    _containerView = contentViewTmp;
    [_contentView addSubview:_containerView];
    [_contentView sendSubviewToBack:_containerView];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 150.0);
    
    //---------------//
    // Setup WebView //
    //---------------//
    NSString *html = [NSString stringWithFormat:@"<html><style> body {font-family:OpenSans-Light; font-size:12px} </style><body>%@</body></html>",productModel.productDesc];
    
    [_descView loadHTMLString:[NSString stringWithFormat:@"%@",html] baseURL:nil];
    [_descView setBackgroundColor:[UIColor clearColor]];
    
    for (UIView *wview in [[[_descView subviews] objectAtIndex:0] subviews]) {
        if([wview isKindOfClass:[UIImageView class]])
        {
            wview.hidden = YES;
        }
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [productModel release];
    [super dealloc];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (webView == _descView) {
        
        CGFloat webViewHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.height"] floatValue];
        
        CGRect frame = [_descView frame];
        frame.size.height = webViewHeight;
        [_descView setFrame:frame];
        
        //-----------------//
        // Relayout Button //
        //-----------------//
        frame = [_buyButton frame];
        frame.origin.y = _descView.frame.origin.y + _descView.frame.size.height + kDefaultComponentPadding;
        [_buyButton setFrame:frame];
        
        //-----------------------//
        // Relayout Content View //
        //-----------------------//
        frame = [_contentView frame];
        frame.size.height = _buyButton.frame.origin.y + _buyButton.frame.size.height + kDefaultPadding;
        [_contentView setFrame:frame];
        
        ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
        
        NSArray *viewsToRemove = [_contentView subviews];
        for (UIView *v in viewsToRemove) {
            if ([v isKindOfClass:[ContentView class]]) {
                [v removeFromSuperview];
            }
        }
        [_contentView addSubview:contentViewTmp];
        [_contentView sendSubviewToBack:contentViewTmp];
        [contentViewTmp release];
        
        //-------------------------------//
        // Relayout Grey Background View //
        //-------------------------------//
        CGFloat x = _contentView.frame.origin.x - kDefaultLeftPadding;
        CGFloat y = _contentView.frame.origin.y - kDefaultPadding;
        CGFloat width = _contentView.frame.size.width + (kDefaultLeftPadding*2);
        CGFloat height = _contentView.frame.size.height + (kDefaultPadding*2);
        _greyBackgroundView.frame = CGRectMake(x, y, width, height);
        
        _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 150.0);
    }
}

@end
