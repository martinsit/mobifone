//
//  ProductCatalogueViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 8/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ProductCatalogueViewController.h"
#import "ProductCell.h"
#import "BorderCellBackground.h"
#import "ProductModel.h"
#import "SectionFooterView.h"
#import "SectionHeaderView.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "DataManager.h"

#import "DetailProductCatalogueViewController.h"
#import "AddressViewController.h"

@interface ProductCatalogueViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@end

@implementation ProductCatalogueViewController

@synthesize content = _content;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.content = nil;
}

- (void)dealloc {
    [_content release];
    _content = nil;
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_content count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierProduct = @"Product";
    
    ProductCell *cell = (ProductCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierProduct];
    
    if (cell == nil) {
        cell = [[ProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierProduct];
        
        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
        
        [cell.buyButton addTarget:self
                           action:@selector(performBuy:)
                 forControlEvents:UIControlEventTouchUpInside];
        
        [cell.detailButton addTarget:self
                              action:@selector(performDetail:)
                    forControlEvents:UIControlEventTouchUpInside];
    }
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    ProductModel *productModel = [_content objectAtIndex:indexPath.row];
    cell.productNameLabel.text = productModel.productName;
    cell.productPriceLabel.text = addThousandsSeparator(productModel.productPrice, sharedData.profileData.language);
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.headerTitleMaster;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 303.0;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:NO] autorelease];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

#pragma mark - Selector

- (void)performBuy:(id)sender {
    AddressViewController *addressVC = [[AddressViewController alloc] initWithNibName:@"AddressViewController" bundle:nil];
    //detailProductVC.headerTitleMaster = [Language get:@"product_detail" alter:nil];
    //detailProductVC.headerIconMaster = icon(@"7");
    //detailProductVC.productModel = productModel;
    
    [self.navigationController pushViewController:addressVC animated:YES];
    addressVC = nil;
    [addressVC release];
}

- (void)performDetail:(id)sender {
    UIButton *button = (UIButton *)sender;
    ProductCell *cell = (ProductCell *)[[button superview] superview];
    int row = [_theTableView indexPathForCell:cell].row;
    
    ProductModel *productModel = [_content objectAtIndex:row];
    
    DetailProductCatalogueViewController *detailProductVC = [[DetailProductCatalogueViewController alloc] initWithNibName:@"DetailProductCatalogueViewController" bundle:nil];
    detailProductVC.headerTitleMaster = [Language get:@"product_detail" alter:nil];
    detailProductVC.headerIconMaster = icon(@"7");
    detailProductVC.productModel = productModel;
    
    [self.navigationController pushViewController:detailProductVC animated:YES];
    detailProductVC = nil;
    [detailProductVC release];
}

@end
