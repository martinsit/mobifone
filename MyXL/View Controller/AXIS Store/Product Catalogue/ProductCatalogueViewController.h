//
//  ProductCatalogueViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 8/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface ProductCatalogueViewController : BasicViewController {
    NSArray *_content;
}

@property (nonatomic, retain) NSArray *content;

@end
