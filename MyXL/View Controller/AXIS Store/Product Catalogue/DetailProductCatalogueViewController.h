//
//  DetailProductCatalogueViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 8/21/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "ProductModel.h"

@interface DetailProductCatalogueViewController : BasicViewController <UIWebViewDelegate> {
    ProductModel *productModel;
}

@property (nonatomic, retain) ProductModel *productModel;

@end
