//
//  ASMenuViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 7/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "MenuModel.h"

@interface ASMenuViewController : BasicViewController <AXISnetRequestDelegate, UISearchBarDelegate> {
    NSArray *_sortedMenu;
    NSDictionary *_theSourceMenu;
    
    NSString *_parentId;
    NSString *_groupDesc;
    
    NSString *_headerIcon;
    NSString *_headerTitle;
    
    MenuModel *_selectedMenuModel;
}

@property (nonatomic, retain) NSArray *sortedMenu;
@property (nonatomic, retain) NSDictionary *theSourceMenu;

@property (nonatomic, retain) NSString *parentId;
@property (nonatomic, retain) NSString *groupDesc;

@property (nonatomic, retain) NSString *headerIcon;
@property (nonatomic, retain) NSString *headerTitle;

@property (nonatomic, retain) MenuModel *selectedMenuModel;

@end
