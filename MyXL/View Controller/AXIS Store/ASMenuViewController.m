//
//  ASMenuViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 7/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ASMenuViewController.h"
#import "DataManager.h"
#import "GroupingAndSorting.h"
#import "CommonCell.h"
#import "AXISnetCellBackground.h"
#import "SectionHeaderView.h"
#import "SectionFooterView.h"
//#import "MenuModel.h"
#import "Constant.h"
#import "AXISnetCommon.h"

#import "ASTermViewController.h"
#import "CategoryViewController.h"

@interface ASMenuViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *theSearchBar;
@property (nonatomic, retain) NSString *searchTextInput;

@end

@implementation ASMenuViewController

@synthesize sortedMenu = _sortedMenu;
@synthesize theSourceMenu = _theSourceMenu;

@synthesize parentId = _parentId;
@synthesize groupDesc = _groupDesc;

@synthesize headerIcon = _headerIcon;
@synthesize headerTitle = _headerTitle;

@synthesize selectedMenuModel = _selectedMenuModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)viewWillAppear:(BOOL)animated {
    [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    
    DataManager *dataManager = [DataManager sharedInstance];
    NSArray *contentTmp = [dataManager.menuData valueForKey:_parentId];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    self.sortedMenu = [sortingResult allKeys];
    self.sortedMenu = [GroupingAndSorting doSortingByGroupId:self.sortedMenu];
    self.theSourceMenu = sortingResult;
    
    [self.theTableView reloadData];
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.sortedMenu = nil;;
    self.theSourceMenu = nil;
    
    self.parentId = nil;
    self.groupDesc = nil;
    
    self.headerIcon = nil;
    self.headerTitle = nil;
    
    self.selectedMenuModel = nil;
}

- (void)dealloc {
    [_sortedMenu release];
    _sortedMenu = nil;
    [_theSourceMenu release];
    _theSourceMenu = nil;
    
    [_parentId release];
    _parentId = nil;
    [_groupDesc release];
    _groupDesc = nil;
    
    [_headerIcon release];
    _headerIcon = nil;
    [_headerTitle release];
    _headerTitle = nil;
    
    [_selectedMenuModel release];
    _selectedMenuModel = nil;
    
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sortedMenu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section]] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierCommon = @"Common";
    
    CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierCommon];
    
    if (cell == nil) {
        cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierCommon];
        
        cell.backgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
        cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
        ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
    }
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
    
    cell.iconLabel.text = icon(menuModel.menuId);
    if ([cell.iconLabel.text length] <= 0) {
        cell.iconLabel.text = @"";
    }
    
    cell.titleLabel.text = [menuModel.menuName uppercaseString];
    
    if ([menuModel.desc length] > 0) {
        cell.arrowLabel.hidden = YES;
        cell.infoButton.hidden = NO;
    }
    else {
        cell.arrowLabel.hidden = NO;
        cell.infoButton.hidden = YES;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return _headerTitle;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kDefaultCellHeight;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
    
    _selectedMenuModel = menuModel;
    
    if ([menuModel.href isEqualToString:@"#toc"]) {
        [self performTermAndCondition];
    }
    
    if ([menuModel.href isEqualToString:@"#guide"]) {
        [self performUserGuide];
    }
    
    if ([menuModel.href isEqualToString:@"#history"]) {
        [self performTrxHistory];
    }
    
    if ([menuModel.href isEqualToString:@"#browse_category"]) {
        [self performBrowseCategory];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:_headerIcon
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:_headerIcon
                                                isFirstSection:NO] autorelease];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

#pragma mark - Selector

- (void)performTermAndCondition {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = AXIS_STORE_TERM_AND_CONDITION_REQ;
    [request termAndConditionAXISStore];
}

- (void)performUserGuide {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = AXIS_STORE_USER_GUIDE_REQ;
    [request userGuideAXISStore];
}

- (void)performTrxHistory {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = AXIS_STORE_TRX_HISTORY_REQ;
    [request trxHistoryAXISStore];
}

- (void)performBrowseCategory {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = AXIS_STORE_CATEGORY_REQ;
    [request categoryAXISStore:@""
              withCategoryName:@""
                     withLimit:@""
                      withPage:@""];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == AXIS_STORE_TERM_AND_CONDITION_REQ) {
            ASTermViewController *termViewController = [[ASTermViewController alloc] initWithNibName:@"ASTermViewController" bundle:nil];
            termViewController.headerTitleMaster = _selectedMenuModel.menuName;
            termViewController.headerIconMaster = icon(_selectedMenuModel.menuId);
            termViewController.htmlString = sharedData.termAndConditionData.label;
            
            [self.navigationController pushViewController:termViewController animated:YES];
            termViewController = nil;
            [termViewController release];
        }
        
        if (type == AXIS_STORE_USER_GUIDE_REQ) {
            ASTermViewController *termViewController = [[ASTermViewController alloc] initWithNibName:@"ASTermViewController" bundle:nil];
            termViewController.headerTitleMaster = _selectedMenuModel.menuName;
            termViewController.headerIconMaster = icon(_selectedMenuModel.menuId);
            termViewController.htmlString = sharedData.userGuideData.label;
            
            [self.navigationController pushViewController:termViewController animated:YES];
            termViewController = nil;
            [termViewController release];
        }
        
        if (type == AXIS_STORE_TRX_HISTORY_REQ) {
            if ([sharedData.trxHistoryData count] == 0) {
                NSString *reason = @"Kamu tidak memiliki transaksi saat ini";
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                                message:reason
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
        }
        
        if (type == AXIS_STORE_CATEGORY_REQ) {
            /*
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[Language get:@"general_error_message" alter:nil]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];*/
            
            CategoryViewController *termViewController = [[CategoryViewController alloc] initWithNibName:@"CategoryViewController" bundle:nil];
            termViewController.headerTitleMaster = _selectedMenuModel.menuName;
            termViewController.headerIconMaster = icon(_selectedMenuModel.menuId);
            termViewController.content = sharedData.categoryData;
            
            [self.navigationController pushViewController:termViewController animated:YES];
            termViewController = nil;
            [termViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
