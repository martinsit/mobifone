//
//  AddressViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 8/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface AddressViewController : BasicViewController <UITextFieldDelegate>

@end
