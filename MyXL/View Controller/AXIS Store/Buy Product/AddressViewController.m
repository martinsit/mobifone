//
//  AddressViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 8/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "AddressViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

#import "DataManager.h"

@interface AddressViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *billLabel;
@property (strong, nonatomic) IBOutlet UILabel *topNameTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *topNameValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *topEmailTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *topEmailValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *topAddressTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *topAddressValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *topCityTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *topCityValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *topPostCodeTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *topPostCodeValueLabel;
@property (strong, nonatomic) UIButton *updateProfileButton;

@property (strong, nonatomic) IBOutlet UILabel *deliveryLabel;
@property (strong, nonatomic) IBOutlet UIButton *sameAddressButton;
@property (strong, nonatomic) IBOutlet UILabel  *sameAddressLabel;
@property (readwrite) BOOL sameAddressSelected;
@property (strong, nonatomic) IBOutlet UILabel *nameTitleLabel;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UILabel *phoneTitleLabel;
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UITextField *address1TextField;
@property (strong, nonatomic) IBOutlet UITextField *address2TextField;
@property (strong, nonatomic) IBOutlet UITextField *address3TextField;
@property (strong, nonatomic) IBOutlet UILabel *cityLabel;
@property (strong, nonatomic) IBOutlet UITextField *cityTextField;
@property (strong, nonatomic) IBOutlet UILabel *zipCodeLabel;
@property (strong, nonatomic) IBOutlet UITextField *zipCodeTextField;
@property (strong, nonatomic) UIButton *otherProductButton;
@property (strong, nonatomic) UIButton *nextButton;

@end

@implementation AddressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    //-------------//
    // Setup Label //
    //-------------//
    _billLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _billLabel.textColor = [UIColor whiteColor];
    _billLabel.backgroundColor = kDefaultDarkGrayColor;
    _billLabel.text = [NSString stringWithFormat:@"  %@",[[Language get:@"bill_address" alter:nil] uppercaseString]];
    
    _topNameTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _topNameTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _topNameTitleLabel.text = [[Language get:@"full_name" alter:nil] uppercaseString];
    
    _topNameValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _topNameValueLabel.textColor = kDefaultPurpleColor;
    _topNameValueLabel.text = sharedData.profileData.fullName;
    
    _topEmailTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _topEmailTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _topEmailTitleLabel.text = [[Language get:@"email" alter:nil] uppercaseString];
    
    _topEmailValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _topEmailValueLabel.textColor = kDefaultPurpleColor;
    _topEmailValueLabel.text = sharedData.profileData.email;
    
    _topAddressTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _topAddressTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _topAddressTitleLabel.text = [[Language get:@"address" alter:nil] uppercaseString];
    
    _topAddressValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _topAddressValueLabel.textColor = kDefaultPurpleColor;
    _topAddressValueLabel.text = [NSString stringWithFormat:@"%@ %@ %@",sharedData.profileData.address,sharedData.profileData.address2,sharedData.profileData.address3];
    
    CGSize size = calculateExpectedSize(_topAddressValueLabel, _topAddressValueLabel.text);
    CGRect frame = _topAddressValueLabel.frame;
    frame.size.height = size.height;
    _topAddressValueLabel.frame = frame;
    
    _topCityTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _topCityTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _topCityTitleLabel.text = [[Language get:@"city" alter:nil] uppercaseString];
    frame = _topCityTitleLabel.frame;
    frame.origin.y = _topAddressValueLabel.frame.origin.y +_topAddressValueLabel.frame.size.height + kDefaultComponentPadding;
    _topCityTitleLabel.frame = frame;
    
    _topCityValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _topCityValueLabel.textColor = kDefaultPurpleColor;
    _topCityValueLabel.text = sharedData.profileData.city;
    frame = _topCityValueLabel.frame;
    frame.origin.y = _topCityTitleLabel.frame.origin.y +_topCityTitleLabel.frame.size.height - kDefaultPaddingDeduct;
    _topCityValueLabel.frame = frame;
    
    _topPostCodeTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _topPostCodeTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _topPostCodeTitleLabel.text = [[Language get:@"zip_code" alter:nil] uppercaseString];
    frame = _topPostCodeTitleLabel.frame;
    frame.origin.y = _topCityTitleLabel.frame.origin.y;
    _topPostCodeTitleLabel.frame = frame;
    
    _topPostCodeValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _topPostCodeValueLabel.textColor = kDefaultPurpleColor;
    _topPostCodeValueLabel.text = sharedData.profileData.postcode;
    frame = _topPostCodeValueLabel.frame;
    frame.origin.y = _topCityValueLabel.frame.origin.y;
    _topPostCodeValueLabel.frame = frame;
    
    // Change Profile Button
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    UIButton *button = createButtonWithFrame(CGRectMake(_topCityValueLabel.frame.origin.x,
                                                        _topCityValueLabel.frame.origin.y + _topCityValueLabel.frame.size.height + kDefaultComponentPadding,
                                                        120,
                                                        kDefaultButtonHeight),
                                             [Language get:@"change_profile" alter:nil],
                                             buttonFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
    
    [button addTarget:self action:@selector(performChangeProfile:) forControlEvents:UIControlEventTouchUpInside];
    
    _updateProfileButton = button;
    [_contentView addSubview:_updateProfileButton];
    
    //-----------//
    // Section 2 //
    //-----------//
    _deliveryLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _deliveryLabel.textColor = [UIColor whiteColor];
    _deliveryLabel.backgroundColor = kDefaultDarkGrayColor;
    _deliveryLabel.text = [NSString stringWithFormat:@"  %@",[[Language get:@"delivery_address" alter:nil] uppercaseString]];
    frame = _deliveryLabel.frame;
    frame.origin.y = _updateProfileButton.frame.origin.y + _updateProfileButton.frame.size.height + kDefaultComponentPadding;
    _deliveryLabel.frame = frame;
    
    frame = _sameAddressButton.frame;
    frame.origin.y = _deliveryLabel.frame.origin.y + _deliveryLabel.frame.size.height + kDefaultComponentPadding;
    _sameAddressButton.frame = frame;
    
    _sameAddressLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _sameAddressLabel.textColor = kDefaultPurpleColor;
    _sameAddressLabel.text = [Language get:@"same_address" alter:nil];
    frame = _sameAddressLabel.frame;
    frame.origin.y = _sameAddressButton.frame.origin.y;
    _sameAddressLabel.frame = frame;
    
    _nameTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _nameTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _nameTitleLabel.text = [[Language get:@"full_name" alter:nil] uppercaseString];
    frame = _nameTitleLabel.frame;
    frame.origin.y = _sameAddressButton.frame.origin.y + _sameAddressButton.frame.size.height + kDefaultComponentPadding;
    _nameTitleLabel.frame = frame;
    
    frame = _nameTextField.frame;
    frame.origin.y = _nameTitleLabel.frame.origin.y + _nameTitleLabel.frame.size.height + kDefaultComponentPadding - kDefaultPaddingDeduct;
    _nameTextField.frame = frame;
    
    _phoneTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _phoneTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _phoneTitleLabel.text = [[Language get:@"phone" alter:nil] uppercaseString];
    frame = _phoneTitleLabel.frame;
    frame.origin.y = _nameTextField.frame.origin.y + _nameTextField.frame.size.height + kDefaultComponentPadding;
    _phoneTitleLabel.frame = frame;
    
    frame = _phoneTextField.frame;
    frame.origin.y = _phoneTitleLabel.frame.origin.y + _phoneTitleLabel.frame.size.height + kDefaultComponentPadding - kDefaultPaddingDeduct;
    _phoneTextField.frame = frame;
    
    _addressLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _addressLabel.textColor = kDefaultTitleFontGrayColor;
    _addressLabel.text = [[Language get:@"address" alter:nil] uppercaseString];
    frame = _addressLabel.frame;
    frame.origin.y = _phoneTextField.frame.origin.y + _phoneTextField.frame.size.height + kDefaultComponentPadding;
    _addressLabel.frame = frame;
    
    frame = _address1TextField.frame;
    frame.origin.y = _addressLabel.frame.origin.y + _addressLabel.frame.size.height + kDefaultComponentPadding;
    _address1TextField.frame = frame;
    
    frame = _address2TextField.frame;
    frame.origin.y = _address1TextField.frame.origin.y + _address1TextField.frame.size.height + kDefaultComponentPadding;
    _address2TextField.frame = frame;
    
    frame = _address3TextField.frame;
    frame.origin.y = _address2TextField.frame.origin.y + _address2TextField.frame.size.height + kDefaultComponentPadding;
    _address3TextField.frame = frame;
    
    _cityLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _cityLabel.textColor = kDefaultTitleFontGrayColor;
    _cityLabel.text = [[Language get:@"city" alter:nil] uppercaseString];
    frame = _cityLabel.frame;
    frame.origin.y = _address3TextField.frame.origin.y + _address3TextField.frame.size.height + kDefaultComponentPadding;
    _cityLabel.frame = frame;
    
    frame = _cityTextField.frame;
    frame.origin.y = _cityLabel.frame.origin.y + _cityLabel.frame.size.height + kDefaultComponentPadding - kDefaultPaddingDeduct;
    _cityTextField.frame = frame;
    
    _zipCodeLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _zipCodeLabel.textColor = kDefaultTitleFontGrayColor;
    _zipCodeLabel.text = [[Language get:@"zip_code" alter:nil] uppercaseString];
    frame = _zipCodeLabel.frame;
    frame.origin.y = _cityTextField.frame.origin.y + _cityTextField.frame.size.height + kDefaultComponentPadding;
    _zipCodeLabel.frame = frame;
    
    frame = _zipCodeTextField.frame;
    frame.origin.y = _zipCodeLabel.frame.origin.y + _zipCodeLabel.frame.size.height + kDefaultComponentPadding - kDefaultPaddingDeduct;
    _zipCodeTextField.frame = frame;
    
    // Button Other
    button = createButtonWithFrame(CGRectMake(_zipCodeTextField.frame.origin.x,
                                              _zipCodeTextField.frame.origin.y + _zipCodeTextField.frame.size.height + kDefaultComponentPadding,
                                              120,
                                              kDefaultButtonHeight),
                                   [Language get:@"other_product" alter:nil],
                                   buttonFont,
                                   kDefaultBaseColor,
                                   kDefaultPinkColor,
                                   kDefaultPurpleColor);
    
    [button addTarget:self action:@selector(performOtherProduct:) forControlEvents:UIControlEventTouchUpInside];
    
    _otherProductButton = button;
    [_contentView addSubview:_otherProductButton];
    
    // Button Next
    button = createButtonWithFrame(CGRectMake(_zipCodeTextField.frame.origin.x + _zipCodeTextField.frame.size.width - 100,
                                              _otherProductButton.frame.origin.y,
                                              100,
                                              kDefaultButtonHeight),
                                   [Language get:@"next" alter:nil],
                                   buttonFont,
                                   kDefaultBaseColor,
                                   kDefaultPinkColor,
                                   kDefaultPurpleColor);
    
    [button addTarget:self action:@selector(performNext:) forControlEvents:UIControlEventTouchUpInside];
    
    _nextButton = button;
    [_contentView addSubview:_nextButton];
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultWhiteColor;
    
    frame = _contentView.frame;
    frame.size.height = _otherProductButton.frame.origin.y + _otherProductButton.frame.size.height + kDefaultPadding;
    _contentView.frame = frame;
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:[Language get:@"address" alter:nil]];
    headerView.icon = @"\ue006";
    [_contentView addSubview:headerView];
    [_contentView sendSubviewToBack:headerView];
    [headerView release];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = frame.origin.x - kDefaultLeftPadding;
    CGFloat y = frame.origin.y - kDefaultPadding;
    CGFloat width = frame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = frame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 50.0);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Selector

- (void)performChangeProfile:(id)sender {
}

- (void)performOtherProduct:(id)sender {
}

- (void)performNext:(id)sender {
}

@end
