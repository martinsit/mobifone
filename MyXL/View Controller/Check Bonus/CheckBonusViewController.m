//
//  CheckBonusViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/16/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CheckBonusViewController.h"

#import "FooterView.h"
#import "BonusCell.h"
#import "GeneralInfoCell.h"
#import "BorderCellBackground.h"

#import "SectionHeaderView.h"
#import "SectionFooterView.h"

#import "DataManager.h"
#import "BonusInfoModel.h"

#import "Constant.h"

@interface CheckBonusViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@end

@implementation CheckBonusViewController

@synthesize headerTitle = _headerTitle;
@synthesize headerIcon = _headerIcon;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
    FooterView *footer = [[[FooterView alloc] initWithFrame:CGRectMake(0.0, 0.0, _theTableView.bounds.size.width, 50.0)] autorelease];
    _theTableView.tableFooterView = footer;*/
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)viewDidUnload {
    self.headerTitle = nil;
    self.headerIcon = nil;
}

- (void)dealloc {
    [_headerTitle release];
    _headerTitle = nil;
    [_headerIcon release];
    _headerIcon = nil;
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.checkBalanceData.bonusInfo count] > 0) {
        return [sharedData.checkBalanceData.bonusInfo count];
    }
    else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierBalance = @"Bonus";
    DataManager *sharedData = [DataManager sharedInstance];
    
    if ([sharedData.checkBalanceData.bonusInfo count] > 0) {
        BonusCell *cell = (BonusCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierBalance];
        
        if (cell == nil) {
            cell = [[BonusCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierBalance];
            
            cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
            cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
        }
        
        BonusInfoModel *bonusInfoModel = [sharedData.checkBalanceData.bonusInfo objectAtIndex:indexPath.row];
        
        cell.titleLabel.text = [bonusInfoModel.desc uppercaseString];
        cell.valueLabel.text = bonusInfoModel.bonusVal;
        
        cell.validityLabel.text = [[Language get:@"validity" alter:nil] uppercaseString];
        cell.validityValueLabel.text = bonusInfoModel.accExpTime;
        
        return cell;
    }
    else {
        GeneralInfoCell *cell = (GeneralInfoCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierBalance];
        
        if (cell == nil) {
            cell = [[GeneralInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierBalance];
            
            cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
            cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
        }
        
        cell.infoLabel.text = [Language get:@"no_bonus" alter:nil];
        
        return cell;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return _headerTitle;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat padding = 10.0;
    CGFloat valueLabelHeight = 20.0;
    
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.checkBalanceData.bonusInfo count] > 0) {
        return 90.0;
    }
    else {
        NSString *value = [Language get:@"no_bonus" alter:nil];
        CGSize valueSize = [value sizeWithFont:[UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize]
                             constrainedToSize:CGSizeMake(260, 1000.0f)];
        if (valueSize.height < valueLabelHeight) {
            return padding + valueLabelHeight;
        }
        else {
            return padding + valueSize.height;
        }
    }
}

#pragma mark - Table view delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:_headerIcon
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:_headerIcon
                                                isFirstSection:NO] autorelease];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 80.0;
}

@end
