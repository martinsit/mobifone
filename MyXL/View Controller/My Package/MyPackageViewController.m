//
//  MyPackageViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/28/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "MyPackageViewController.h"
#import "FooterView.h"
#import "SectionHeaderView.h"
#import "SectionFooterView.h"

#import "MyPackageCell.h"
#import "MyPackageTitleiPadCell.h"
#import "MyPackageiPadCell.h"
#import "BorderCellBackground.h"
#import "DataManager.h"
#import "CheckUsageModel.h"
#import "HumanReadableDataSizeHelper.h"

#import "AXISnetCommon.h"
#import "Constant.h"
#import "ThankYouViewController.h"
#import "SubMenuViewController.h"

@interface MyPackageViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (nonatomic, retain) CheckUsageModel *selectedPackage;

- (void)stopButtonPressed:(id)sender;
- (void)performStopPackage;

@end

@implementation MyPackageViewController

@synthesize headerTitle = _headerTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
    FooterView *footer = [[[FooterView alloc] initWithFrame:CGRectMake(0.0, 0.0, _theTableView.bounds.size.width, 50.0)] autorelease];
    _theTableView.tableFooterView = footer;*/
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.headerTitle = nil;
}

- (void)dealloc {
    [_headerTitle release];
    _headerTitle = nil;
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DataManager *dataManager = [DataManager sharedInstance];
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        return [dataManager.checkUsageData count] + 1;
    }
    else {
        return [dataManager.checkUsageData count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD) {
        if (indexPath.section == 0 && indexPath.row == 0) {
            static NSString *CellIdentifierTitleiPad = @"MyPackageTitleiPad";
            MyPackageTitleiPadCell *cell = (MyPackageTitleiPadCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierTitleiPad];
            
            if (cell == nil) {
                cell = [[MyPackageTitleiPadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierTitleiPad];
                
                cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
            }
            
            cell.packageTitleLabel.text = [[Language get:@"package" alter:nil] uppercaseString];
            cell.typeTitleLabel.text = [[Language get:@"type" alter:nil] uppercaseString];
            cell.usageTitleLabel.text = [[Language get:@"usage" alter:nil] uppercaseString];
            cell.validityTitleLabel.text = [[Language get:@"validity" alter:nil] uppercaseString];
            
            return cell;
        }
        else {
            static NSString *CellIdentifieriPad = @"MyPackageiPad";
            MyPackageiPadCell *cell = (MyPackageiPadCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifieriPad];
            
            if (cell == nil) {
                cell = [[MyPackageiPadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifieriPad];
                
                cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                
                [cell.stopButton addTarget:self
                                    action:@selector(stopButtonPressed:)
                          forControlEvents:UIControlEventTouchUpInside];
                
                [cell.upgradeButton addTarget:self
                                       action:@selector(upgradeButtonPressed:)
                             forControlEvents:UIControlEventTouchUpInside];
                
                //cell.userInteractionEnabled = NO;
            }
            
            DataManager *dataManager = [DataManager sharedInstance];
            CheckUsageModel *checkUsageModel = [dataManager.checkUsageData objectAtIndex:indexPath.row - 1];
            
            cell.packageValueLabel.text = checkUsageModel.packageName;
            
            NSString *type = convertPackageType(checkUsageModel.packageType);
            cell.typeValueLabel.text = type;
            
            // Special case for FUP
            if ([checkUsageModel.packageType isEqualToString:@"3"]) {
                unsigned long long ullvalue = strtoull([checkUsageModel.usage UTF8String], NULL, 0);
                NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
                NSString *strUsage = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:usageNumber];
                cell.usageValueLabel.text = strUsage;
            }
            else {
                cell.usageValueLabel.text = checkUsageModel.usage;
            }
            
            cell.validityValueLabel.text = checkUsageModel.endDate;
            
            if ([checkUsageModel.unregCmd length] > 0 ||
                [checkUsageModel.unsubCmd length] > 0) {
                cell.stopButton.hidden = NO;
                [cell.stopButton setTag:indexPath.row - 1];
            }
            else {
                cell.stopButton.hidden = YES;
            }
            
            if ([checkUsageModel.flagUpgrade length] > 0) {
                cell.upgradeButton.hidden = NO;
                [cell.upgradeButton setTag:indexPath.row - 1];
            }
            else {
                cell.upgradeButton.hidden = YES;
            }
            
            return cell;
        }
    }
    else {
        static NSString *CellIdentifierBalance = @"MyPackage";
        MyPackageCell *cell = (MyPackageCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierBalance];
        
        if (cell == nil) {
            cell = [[MyPackageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierBalance];
            
            cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
            cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
            
            [cell.stopButton addTarget:self
                                action:@selector(stopButtonPressed:)
                      forControlEvents:UIControlEventTouchUpInside];
            
            [cell.upgradeButton addTarget:self
                                   action:@selector(upgradeButtonPressed:)
                         forControlEvents:UIControlEventTouchUpInside];
        }
        
        DataManager *dataManager = [DataManager sharedInstance];
        CheckUsageModel *checkUsageModel = [dataManager.checkUsageData objectAtIndex:indexPath.row];
        cell.packageTitleLabel.text = [[Language get:@"package" alter:nil] uppercaseString];
        cell.packageValueLabel.text = checkUsageModel.packageName;
        
        cell.usageTitleLabel.text = [[Language get:@"usage" alter:nil] uppercaseString];
        
        // Special case for FUP
        if ([checkUsageModel.packageType isEqualToString:@"3"]) {
            unsigned long long ullvalue = strtoull([checkUsageModel.usage UTF8String], NULL, 0);
            NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
            NSString *strUsage = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:usageNumber];
            cell.usageValueLabel.text = strUsage;
        }
        else {
            cell.usageValueLabel.text = checkUsageModel.usage;
        }
        
        cell.typeTitleLabel.text = [[Language get:@"type" alter:nil] uppercaseString];
        NSString *type = convertPackageType(checkUsageModel.packageType);
        cell.typeValueLabel.text = type;
        
        cell.validityTitleLabel.text = [[Language get:@"validity" alter:nil] uppercaseString];
        cell.validityValueLabel.text = checkUsageModel.endDate;
        
        /*
         unsigned long long ullvalue = strtoull([checkUsageModel.usage UTF8String], NULL, 0);
         NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
         NSString *strUsage = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:usageNumber];
         cell.typeValueLabel.text = strUsage;*/
        
        if ([checkUsageModel.unregCmd length] > 0 ||
            [checkUsageModel.unsubCmd length] > 0) {
            cell.stopButton.hidden = NO;
            [cell.stopButton setTag:indexPath.row];
        }
        else {
            cell.stopButton.hidden = YES;
        }
        
        if ([checkUsageModel.flagUpgrade length] > 0) {
            cell.upgradeButton.hidden = NO;
            [cell.upgradeButton setTag:indexPath.row];
        }
        else {
            cell.upgradeButton.hidden = YES;
        }
        
        return cell;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.headerTitleMaster;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        if (indexPath.section == 0 && indexPath.row == 0) {
            return 30.0;
        }
        else {
            return kDefaultButtonHeight + 10.0;
        }
    }
    else {
        
        
        DataManager *dataManager = [DataManager sharedInstance];
        CheckUsageModel *checkUsageModel = [dataManager.checkUsageData objectAtIndex:indexPath.row];
        
        NSString *text = checkUsageModel.packageName;
        CGFloat leftPadding = 20.0;
        //CGFloat topPadding = 5.0;
        CGFloat width = (self.view.frame.size.width - (10*2) - (leftPadding*2))/2.0;
        CGFloat height = 20.0;
        //CGFloat deduction = 5.0;
        
        CGFloat cellHeightDefault = 90.0 + kDefaultButtonHeight + 10.0;
        CGFloat labelHeightDefault = height;
        
        CGRect frame;
        frame = CGRectMake(leftPadding,
                           0,
                           width,
                           height);
        
        UILabel *label = [[UILabel alloc] initWithFrame:frame];
        label.textAlignment = UITextAlignmentLeft;
        label.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        label.textColor = kDefaultPurpleColor;
        label.backgroundColor = [UIColor blueColor];
        label.numberOfLines = 0;
        label.lineBreakMode = UILineBreakModeWordWrap;
        
        CGSize size = calculateExpectedSize(label, text);
        
        CGFloat extraHeightForTitle = 0.0;
        if (size.height > labelHeightDefault) {
            extraHeightForTitle = size.height - labelHeightDefault;
        }
        
        return cellHeightDefault + extraHeightForTitle;
    }
}

#pragma mark - Table view delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:NO] autorelease];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 80.0;
}

#pragma mark - Selector

- (void)stopButtonPressed:(id)sender {
    UIButton *button = (UIButton *)sender;
    DataManager *dataManager = [DataManager sharedInstance];
    _selectedPackage = [dataManager.checkUsageData objectAtIndex:button.tag];
    
    NSString *message = [NSString stringWithFormat:@"%@ %@ %@ %@\n%@",
                         [Language get:@"stop_package_confirmation_1" alter:nil],
                         _selectedPackage.packageName,
                         [Language get:@"stop_package_confirmation_2" alter:nil],
                         [Language get:@"stop_package_confirmation_3" alter:nil],
                         [Language get:@"ask_continue" alter:nil]];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                          otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
    alert.tag = 1;
    [alert show];
    [alert release];
}

- (void)performStopPackage {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = STOP_PACKAGE_REQ;
    [request stopPackage:_selectedPackage.unsubCmd withUnreg:_selectedPackage.unregCmd];
}

- (void)upgradeButtonPressed:(id)sender {
    UIButton *button = (UIButton *)sender;
    DataManager *dataManager = [DataManager sharedInstance];
    _selectedPackage = [dataManager.checkUsageData objectAtIndex:button.tag];
    
    [self performUpgradePackage:_selectedPackage.flagUpgrade];
}

- (void)performUpgradePackage:(NSString*)flagUpgrade {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = UPGRADE_PACKAGE_REQ;
    [request upgradePackage:flagUpgrade];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            [self performStopPackage];
        }
        else {
        }
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == STOP_PACKAGE_REQ) {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            
            thankYouViewController.info = [Language get:@"stop_package_thank_you" alter:nil];
            thankYouViewController.headerTitleMaster = self.headerTitleMaster;
            thankYouViewController.headerIconMaster = self.headerIconMaster;
            
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            thankYouViewController = nil;
            [thankYouViewController release];
        }
        
        if (type == UPGRADE_PACKAGE_REQ) {
            /*
            SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
            
            //        NSString *groupIdOfMenu = [self.sortedMenu objectAtIndex:indexPath.section];
            //        NSArray *objectsForMenu = [self.theSourceMenu objectForKey:groupIdOfMenu];
            //        MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
            //        NSLog(@"menuModel.parentId = %@",menuModel.menuId);
            
            subMenuVC.parentId = menuModel.menuId;
            subMenuVC.groupDesc = menuModel.desc;
            //(@"subMenuVC.groupDesc = %@",subMenuVC.groupDesc);
            
            subMenuVC.headerIconMaster = icon(_selectedMenu.menuId);
            if ([subMenuVC.headerIconMaster length] <= 0) {
                subMenuVC.headerIconMaster = icon(@"default_icon");
            }
            
            subMenuVC.headerTitleMaster = _selectedMenu.menuName;
            
            [self.navigationController pushViewController:subMenuVC animated:YES];
            subMenuVC = nil;
            [subMenuVC release];*/
            
            ///
            DataManager *dataManager = [DataManager sharedInstance];
            
            for (int i=0; i<[dataManager.upgradePackageData count]; i++) {
                MenuModel *upgradePackageModel = [dataManager.upgradePackageData objectAtIndex:i];
                upgradePackageModel.groupId        = @"500";
                upgradePackageModel.groupName      = [[Language get:@"upgrade_package" alter:nil] uppercaseString];
            }
            
            SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
            //subMenuVC.parentId = _selectedMenu.menuId;
            //subMenuVC.groupDesc = _selectedMenu.desc;
            
            //NSLog(@"dataManager.upgradePackageData = %i",[dataManager.upgradePackageData count]);
            
            subMenuVC.currentPackage = _selectedPackage.packageName;
            
            subMenuVC.upgradePackageModel = [dataManager.upgradePackageData objectAtIndex:0];
            subMenuVC.recommendedPackageModel = nil;
            
            subMenuVC.headerIconMaster = icon(@"default_icon");
            subMenuVC.headerTitleMaster = [Language get:@"upgrade_package" alter:nil];
            
            [self.navigationController pushViewController:subMenuVC animated:YES];
            subMenuVC = nil;
            [subMenuVC release];
        }
    }
    else {
        
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
