//
//  MyPackageViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/28/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface MyPackageViewController : BasicViewController <UIAlertViewDelegate> {
    NSString *_headerTitle;
}

@property (nonatomic, retain) NSString *headerTitle;

@end
