//
//  ChangePasswordViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/21/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "UnderLineLabel.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>
#import "DataManager.h"
#import "EncryptDecrypt.h"
#import "NotificationViewController.h"

@interface ChangePasswordViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *asteriskLabel;

@property (strong, nonatomic) IBOutlet UILabel *oldPasswordLabel;
@property (strong, nonatomic) IBOutlet UITextField *oldPasswordTextField;
@property (strong, nonatomic) IBOutlet UnderLineLabel *resetPasswordLabel;

@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;

@property (strong, nonatomic) IBOutlet UILabel *confirmPasswordLabel;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

@property (strong, nonatomic) UIButton *submitButton;

- (void)performResetPassword;
- (void)resetPassword;
- (void)performSubmit:(id)sender;
- (void)submit;
- (int)validateInput;

@end

@implementation ChangePasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    CGFloat screenWidth = 0.0;
    if (IS_IPAD) {
        screenWidth = 1024.0;
    }
    else {
        screenWidth = 320.0;
    }
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, kDefaultCellHeight*1.5)
                                                               withLabelForXL:[Language get:@"my_account" alter:nil]
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, screenWidth, kDefaultCellHeight)
                                                               withLabelForXL:[Language get:@"change_password" alter:nil]
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    
    /*
     * Asterisk
     */
    _asteriskLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _asteriskLabel.textColor = kDefaultNavyBlueColor;
    _asteriskLabel.text = [Language get:@"asterisk" alter:nil];
    
    _oldPasswordLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _oldPasswordLabel.textColor = kDefaultTitleFontGrayColor;
    _oldPasswordLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"old_password" alter:nil]];
    
    /*
    _resetPasswordLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontValueSize];
    _resetPasswordLabel.textColor = kDefaultPurpleColor;
    _resetPasswordLabel.text = [Language get:@"reset_password" alter:nil];
    [_resetPasswordLabel setShouldUnderline:YES];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(performResetPassword)];
    [_resetPasswordLabel setUserInteractionEnabled:YES];
    [_resetPasswordLabel addGestureRecognizer:tapGesture];
    [tapGesture release];
    // Hide Reset Password
    _resetPasswordLabel.hidden = YES;*/
    
    _passwordLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _passwordLabel.textColor = kDefaultTitleFontGrayColor;
    _passwordLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"new_password" alter:nil]];
    
    _confirmPasswordLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _confirmPasswordLabel.textColor = kDefaultTitleFontGrayColor;
    _confirmPasswordLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"confirm_password" alter:nil]];
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Setup Button //
    //--------------//
    DataManager *sharedData = [DataManager sharedInstance];
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    UIButton *button = createButtonWithFrame(CGRectMake(_confirmPasswordLabel.frame.origin.x,
                                                        _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             [[Language get:@"submit" alter:nil] uppercaseString],
                                             buttonFont,
                                             kDefaultWhiteColor,
                                             colorWithHexString(sharedData.profileData.themesModel.menuFeature),
                                             colorWithHexString(sharedData.profileData.themesModel.menuColor));
    [button addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    _submitButton = button;
    [_contentView addSubview:_submitButton];
    
    //--------------//
    // Content View //
    //--------------//
    newFrame = _contentView.frame;
    newFrame.size.height = _submitButton.frame.origin.y + _submitButton.frame.size.height + kDefaultComponentPadding; //kDefaultPadding
    _contentView.frame = newFrame;
    _contentView.backgroundColor = kDefaultWhiteColor;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 500.0);
    
    [headerView1 release];
    [headerView2 release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Selector

- (void)performResetPassword {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                    message:[Language get:@"change_password_confirmation" alter:nil]
                                                   delegate:self
                                          cancelButtonTitle:[[Language get:@"cancel" alter:nil] uppercaseString]
                                          otherButtonTitles:[Language get:@"ok" alter:nil],nil];
    alert.tag = 1;
    [alert show];
    [alert release];
}

- (void)resetPassword {
    [_oldPasswordTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_confirmPasswordTextField resignFirstResponder];
    
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = RESET_PASSWORD_REQ;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    //NSLog(@"msisdn API decrypt = %@",msisdn);
    
    [request resetPassword:msisdn
               withCaptcha:@""
                   withCid:@""
                 withToken:sharedData.profileData.token
              withLanguage:sharedData.profileData.language];
}

- (int)validateInput
{
    NSString *password = _passwordTextField.text;
    NSLog(@"password = %@",password);
    /*
    NSCharacterSet *alphaSet = [NSCharacterSet alphanumericCharacterSet];
    BOOL isValidCharacterSet = [[password stringByTrimmingCharactersInSet:alphaSet] isEqualToString:@""];*/
    
    BOOL containsLetter = NSNotFound != [password rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location;
    BOOL containsNumber = NSNotFound != [password rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location;
    NSLog(@"Contains letter: %d\n Contains number: %d", containsLetter, containsNumber);
    
    BOOL isValidCharacterSet = containsLetter && containsNumber;
    
    NSLog(@"isValidCharacterSet = %i",isValidCharacterSet);
    
    if ([_oldPasswordTextField.text length] <= 0 ||
        [_passwordTextField.text length] <= 0 ||
        [_confirmPasswordTextField.text length] <= 0) {
        return 2;
    }
    // TODO : V1.9
    else if ([_passwordTextField.text length] < MIN_PASSWORD_LENGTH) {
        return 1;
    }
    else if ([_passwordTextField.text length] > MAX_PASSWORD_LENGTH) {
        return 1;
    }
    else if ([_passwordTextField.text isEqualToString:_confirmPasswordTextField.text] == NO) {
        return 4;
    }
    // add condition for alphanumeric check
    else if(!isValidCharacterSet)
    {
        return 5;
    }
    else {
        return 0;
    }
}

- (void)performSubmit:(id)sender {
    int valid = [self validateInput];
    if (valid == 2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"empty_validation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 100;
        [alert show];
        [alert release];
    }
    else if (valid == 1)
    {
        
        // TODO : V1.9
        // Update error password length wording
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
//                                                        message:[Language get:@"password_length" alter:nil]
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"error_password_length" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 100;
        [alert show];
        [alert release];
    }
    else if (valid == 4) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"confirm_password_not_match" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 100;
        [alert show];
        [alert release];
    }
    else if (valid == 5) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[Language get:@"error_password_length" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 100;
        [alert show];
        [alert release];
    }
    else {
        [self submit];
    }
}

- (void)submit {
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    
    [_oldPasswordTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_confirmPasswordTextField resignFirstResponder];
    
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = CHANGE_PASSWORD_REQ;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    [request changePassword:_oldPasswordTextField.text
            withNewPassword:_passwordTextField.text
      withNewPasswordRetype:_confirmPasswordTextField.text
               withLanguage:sharedData.profileData.language];
}

- (void)performNotification:(id)sender {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = NOTIFICATION_REQ;
    [request notification];
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat targetY = -10 + textField.frame.origin.y;
    /*
    if (textField == _oldPasswordTextField) {
        targetY = -10 + textField.frame.origin.y;
    }*/
    
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    // TODO : V1.9
    // Update the maximum character length for password to be 64
    return !([newString length] > MAX_PASSWORD_LENGTH);
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        if (buttonIndex == 1) {
            [self resetPassword];
        }
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == RESET_PASSWORD_REQ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[Language get:@"reset_password_success" alter:nil]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        
        if (type == CHANGE_PASSWORD_REQ) {
            NSString *reason = [result valueForKey:REASON_KEY];
            /*
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[Language get:@"change_password_success" alter:nil]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];*/
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:reason
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else {
        
        NSString *reason = [result valueForKey:REASON_KEY];
        /*
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];*/
        
        // Changes by iNot 23 June 2015
        // TODO : V1.9
        // Change Wording, Replace "Maaf" on title Pop Up Message
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
        [alert release];
    }
}

@end
