//
//  BalanceSubmenuViewController.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 11/30/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BasicViewController.h"
#import "MenuModel.h"

@interface BalanceSubmenuViewController : BasicViewController {
    NSArray *_sortedMenu;
    NSDictionary *_theSourceMenu;
    
    MenuModel *_parentMenuModel;
}

@property (nonatomic, retain) NSArray *sortedMenu;
@property (nonatomic, retain) NSDictionary *theSourceMenu;

@property (nonatomic, retain) MenuModel *parentMenuModel;

@end
