//
//  BalanceSubmenuViewController.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 11/30/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BalanceSubmenuViewController.h"
#import "DataManager.h"
#import "GroupingAndSorting.h"
#import "Constant.h"
#import "SectionHeaderView.h"
#import "CommonCell.h"
#import "BalanceHomeCell.h"
#import "RectBackground.h"
#import "AXISnetCommon.h"

#import "PaymentChoiceViewController.h"
#import "SubMenuViewController.h"
#import "TransactionHistoryViewController.h"
#import "NotificationViewController.h"

@interface BalanceSubmenuViewController ()

@property (strong, nonatomic) MenuModel *selectedMenu;
@property (strong, nonatomic) IBOutlet UITableView *theTableView;

- (void)settingData;

@end

@implementation BalanceSubmenuViewController

@synthesize sortedMenu = _sortedMenu;
@synthesize theSourceMenu = _theSourceMenu;

@synthesize parentMenuModel = _parentMenuModel;

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)viewWillAppear:(BOOL)animated {
    [self settingData];
    [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Selector

- (void)settingData {
    DataManager *dataManager = [DataManager sharedInstance];
    NSArray *contentTmp = [NSArray arrayWithArray:dataManager.balanceMenu];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    self.sortedMenu = [sortingResult allKeys];
    self.sortedMenu = [GroupingAndSorting doSortingByGroupId:self.sortedMenu];
    self.theSourceMenu = sortingResult;
    
    [self.theTableView reloadData];
}

- (void)requestTransactionHistory:(NSString*)type {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = TRX_HISTORY_REQ;
    [request lastTransaction:type];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sortedMenu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
    
    if ([menuModel.href isEqualToString:@"#balance"]) {
        static NSString *CellIdentifier = @"BalanceCell";
        
        BalanceHomeCell *cell = (BalanceHomeCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[BalanceHomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            /*
            [cell.chevronButton addTarget:self
                                   action:@selector(performBalanceSubMenu:)
                         forControlEvents:UIControlEventTouchUpInside];*/
            
            cell.chevronButton.hidden = YES;
            
            cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                          withBasicColor:kDefaultWhiteSmokeColor
                                                       withSelectedColor:kDefaultWhiteSmokeColor] autorelease];
            
            cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                  withBasicColor:kDefaultWhiteSmokeColor
                                                               withSelectedColor:kDefaultWhiteSmokeColor] autorelease];
            ((RectBackground *)cell.selectedBackgroundView).selected = NO;
        }
        
        DataManager *sharedData = [DataManager sharedInstance];
        
        cell.balanceLabel.text = [NSString stringWithFormat:@"%@",addThousandsSeparator(sharedData.checkBalanceData.balance, sharedData.profileData.language)];
        cell.balanceLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
        
        cell.validityLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
        
        if (sharedData.profileData.msisdnType == PREPAID) {
            NSString *validity = @"";
            validity = sharedData.checkBalanceData.activeStopDate;
            if ([validity length] != 0) {
                validity = [NSString stringWithFormat:@"%@",changeDateFormat(validity)];
                cell.validityLabel.hidden = NO;
                cell.validityLabel.text = [NSString stringWithFormat:@"%@\n%@",[Language get:@"active_until" alter:nil],validity];
            }
            else {
                cell.validityLabel.hidden = YES;
            }
        }
        else {
            cell.validityLabel.hidden = YES;
        }
        
        return cell;
    }
    else {
        static NSString *CellIdentifier = @"Cell";
        CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.backgroundColor = [UIColor clearColor];
            
            [cell.infoButton addTarget:self
                                action:@selector(performInfoForCommonCell:)
                      forControlEvents:UIControlEventTouchUpInside];
            
            if (indexPath.section == 0) {
                if (indexPath.row == 0) {
                    cell.imageView.image = [UIImage imageNamed:@"buy_package_down_ico.png"];

                    cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                  withBasicColor:[UIColor colorWithRed:218.0/255.0 green:37.0/255.0 blue:29.0/255.0 alpha:1.0]
                                                               withSelectedColor:kDefaultGrayColor] autorelease];
                    cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                          withBasicColor:kDefaultWhiteColor
                                                                       withSelectedColor:kDefaultGrayColor] autorelease];
                    ((RectBackground *)cell.selectedBackgroundView).selected = YES;
                } else if (indexPath.row == 1)
                {
                    cell.imageView.image = [UIImage imageNamed:@"reload_balance_ico.png"];
                    
                    cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                  withBasicColor:[UIColor colorWithRed:193.0/255.0 green:33.0/255.0 blue:26.0/255.0 alpha:1.0]
                                                               withSelectedColor:kDefaultGrayColor] autorelease];
                    cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                          withBasicColor:[UIColor colorWithRed:193.0/255.0 green:33.0/255.0 blue:26.0/255.0 alpha:1.0]
                                                                       withSelectedColor:kDefaultGrayColor] autorelease];
                    ((RectBackground *)cell.selectedBackgroundView).selected = YES;
                }
            } else if (indexPath.section == 1)
            {
                if (indexPath.row == 0) {
                    cell.imageView.image = [UIImage imageNamed:@"utilization_down_ico.png"];
                    
                    cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                  withBasicColor:[UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:208.0/255.0 alpha:1.0]
                                                               withSelectedColor:kDefaultGrayColor] autorelease];
                    cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                          withBasicColor:[UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:208.0/255.0 alpha:1.0]
                                                                       withSelectedColor:kDefaultGrayColor] autorelease];
                    ((RectBackground *)cell.selectedBackgroundView).selected = YES;

                } else if (indexPath.row == 1)
                {
                    cell.imageView.image = [UIImage imageNamed:@"purchase_package_down_ico.png"];
                    
                    
                    cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                  withBasicColor:[UIColor colorWithRed:0.0/255.0 green:105.0/255.0 blue:189.0/255.0 alpha:1.0]
                                                               withSelectedColor:kDefaultGrayColor] autorelease];
                    cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                          withBasicColor:[UIColor colorWithRed:0.0/255.0 green:105.0/255.0 blue:189.0/255.0 alpha:1.0]
                                                                       withSelectedColor:kDefaultGrayColor] autorelease];
                    ((RectBackground *)cell.selectedBackgroundView).selected = YES;
                    

                } else if (indexPath.row == 2)
                {
                    cell.imageView.image = [UIImage imageNamed:@"reload_balance_ico.png"];
                    
                    
                    cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                  withBasicColor:[UIColor colorWithRed:0.0/255.0 green:93.0/255.0 blue:167.0/255.0 alpha:1.0]
                                                               withSelectedColor:kDefaultGrayColor] autorelease];
                    cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                          withBasicColor:[UIColor colorWithRed:0.0/255.0 green:93.0/255.0 blue:167.0/255.0 alpha:1.0]
                                                                       withSelectedColor:kDefaultGrayColor] autorelease];
                    ((RectBackground *)cell.selectedBackgroundView).selected = YES;
                    

                }
  
            }
        }
        
        cell.textLabel.text = [menuModel.menuName uppercaseString];
        cell.textLabel.textColor = kDefaultBlackColor;
        cell.arrowLabel.textColor = kDefaultSeparatorColor;
        cell.lineView.backgroundColor = kDefaultSeparatorColor;
        
        cell.infoButton.hidden = YES;
        cell.descLabel.hidden = YES;
        
        if (indexPath.row == objectsForMenu.count -1) {
            cell.lineView.hidden = YES;
        }
        else {
            cell.lineView.hidden = NO;
        }
        
        cell.refreshButton.hidden = YES;
        
        return cell;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    /*
    NSString *groupIdOfMenu = [self.sortedMenu objectAtIndex:section];
    NSArray *objectsForMenu = [self.theSourceMenu objectForKey:groupIdOfMenu];
    MenuModel *menuModel = [objectsForMenu objectAtIndex:0];
    if ([menuModel.href isEqualToString:@"#balance"]) {
        return menuModel.menuName;
    }
    else {
        return menuModel.groupName;
    }*/
    
    if (section == 0) {
        return @"Settings";
    } else if (section == 1)
    {
        return @"History";
    }
return @"Unknown";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
    if ([menuModel.href isEqualToString:@"#balance"]) {
        return 50.0;
    }
    else {
        return kDefaultCellHeight;
    }
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
    _selectedMenu = menuModel;
    
    if ([menuModel.href isEqualToString:@"#topup_hvrn"]) {
        PaymentChoiceViewController *subMenuVC = [[PaymentChoiceViewController alloc] initWithNibName:@"PaymentChoiceViewController" bundle:nil];
        
        //-- Setting Payment Menu --//
        DataManager *dataManager = [DataManager sharedInstance];
        
        NSArray *contentTmp = nil;
        
        // TODO : Fixed the wrong payment menu with menuID
//        contentTmp = [dataManager.menuData valueForKey:dataManager.paymentMethodMenu.menuId];
        contentTmp = [dataManager.menuData valueForKey:menuModel.menuId];
        
        // Grouping
        NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
        
        // Sorting
        NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
        
        NSArray *paymentMenu = [sortingResult allKeys];
        paymentMenu = [GroupingAndSorting doSortingByGroupId:paymentMenu];
        
        NSMutableArray *paymentMenuTmp = [[NSMutableArray alloc] init];
        for (int i=0; i<[paymentMenu count]; i++) {
            NSString *groupIdOfMenu = [paymentMenu objectAtIndex:i];
            NSArray *objectsForMenu = [sortingResult objectForKey:groupIdOfMenu];
            for (MenuModel *menu in objectsForMenu) {
                [paymentMenuTmp addObject:menu];
            }
        }
        
        subMenuVC.paymentMenu = paymentMenuTmp;
        subMenuVC.grayHeaderTitle = menuModel.groupName;
        subMenuVC.blueHeaderTitle = menuModel.menuName;
        subMenuVC.isLevel2 = NO;
        //---------------------------//
        
        [self.navigationController pushViewController:subMenuVC animated:YES];
        subMenuVC = nil;
        [subMenuVC release];
    }
    else if ([menuModel.href isEqualToString:@"#last5_usage"]) {
        [self requestTransactionHistory:@"usage"];
    }
    
    else if ([menuModel.href isEqualToString:@"#last5_reload"]) {
        [self requestTransactionHistory:@"reload"];
    }
    
    else if ([menuModel.href isEqualToString:@"#last5_package"]) {
        [self requestTransactionHistory:@"package"];
    }
    else {
        SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
        subMenuVC.parentMenuModel = menuModel;
        subMenuVC.grayHeaderTitle = menuModel.groupName;
        
        [self.navigationController pushViewController:subMenuVC animated:YES];
        
        subMenuVC = nil;
        [subMenuVC release];
    }
}
/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    
    headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                            withLabelForXL:sectionTitle
                                            isFirstSection:NO] autorelease];
    
    return headerView;
}
 */

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kDefaultCellHeight;
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        DataManager *sharedData = [DataManager sharedInstance];
        
        if (type == TRX_HISTORY_REQ) {
            
            TransactionHistoryViewController *transactionHistoryViewController = [[TransactionHistoryViewController alloc] initWithNibName:@"TransactionHistoryViewController" bundle:nil];
            
            transactionHistoryViewController.content = sharedData.lastTransactionData;
            transactionHistoryViewController.menuModel = _selectedMenu;
            
            transactionHistoryViewController.headerTitleMaster = _selectedMenu.menuName;
            transactionHistoryViewController.headerIconMaster = icon(_selectedMenu.menuId);
            
            [transactionHistoryViewController createBarButtonItem:BACK_NOTIF];
            
            [self.navigationController pushViewController:transactionHistoryViewController animated:YES];
            transactionHistoryViewController = nil;
            [transactionHistoryViewController release];
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else {
        
        //NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        //int type = [typeNumber intValue];
        
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
