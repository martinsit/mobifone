//
//  ResendPinViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/12/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AXISnetRequest.h"

@interface ResendPinViewController : BasicViewController <UITextFieldDelegate, AXISnetRequestDelegate>

@end
