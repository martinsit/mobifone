//
//  FourGPowerPackViewController.h
//  MyXL
//
//  Created by tyegah on 8/27/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

typedef enum {
    MATRIX_000 = 0,
    MATRIX_001,
    MATRIX_010,
    MATRIX_011,
    MATRIX_100,
    MATRIX_101,
    MATRIX_110,
    MATRIX_111
}MatrixType;

@interface FourGPowerPackViewController : BasicViewController {
    MatrixType matrixType;
}

@property (readwrite) MatrixType matrixType;

@end
