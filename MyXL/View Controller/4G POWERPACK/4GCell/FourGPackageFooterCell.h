//
//  FourGPackageFooterCell.h
//  MyXL
//
//  Created by tyegah on 9/2/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnderLineLabel.h"

@interface FourGPackageFooterCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UnderLineLabel *lblTitle;
//@property (retain, nonatomic) IBOutlet UIButton *btnLinkURL;

@end
