//
//  FourGPackageTableViewCell.m
//  MyXL
//
//  Created by tyegah on 8/27/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "FourGPackageTableViewCell.h"

@implementation FourGPackageTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_lblPackageName release];
    [_lblPeriod release];
    //[_lblSpeed release];
    [_lblPrice release];
    [_lblBuy release];
    [_buyView release];
    [_btnCurrentPackage release];
    [super dealloc];
}
@end
