//
//  FourGPackageFooterCell.m
//  MyXL
//
//  Created by tyegah on 9/2/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "FourGPackageFooterCell.h"

@implementation FourGPackageFooterCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_lblTitle release];
    //[_btnLinkURL release];
    [super dealloc];
}
@end
