//
//  4GPowerTableViewCell.m
//  MyXL
//
//  Created by tyegah on 8/25/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "4GPowerTableViewCell.h"

@implementation Power4GTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_lblTitle release];
    [_lblSubtitle release];
    [_buttonContainerView release];
    [_imgHandset release];
    [_imgSimcard release];
    [_imgPaket release];
    [_btn4GPower release];
    [_lblAwareness release];
    [super dealloc];
}
@end
