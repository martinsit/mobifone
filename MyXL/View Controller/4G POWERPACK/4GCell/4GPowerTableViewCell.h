//
//  4GPowerTableViewCell.h
//  MyXL
//
//  Created by tyegah on 8/25/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Power4GTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;
@property (retain, nonatomic) IBOutlet UILabel *lblSubtitle;
@property (retain, nonatomic) IBOutlet UIView *buttonContainerView;
@property (retain, nonatomic) IBOutlet UIImageView *imgHandset;
@property (retain, nonatomic) IBOutlet UIImageView *imgSimcard;
@property (retain, nonatomic) IBOutlet UIImageView *imgPaket;
@property (retain, nonatomic) IBOutlet UIButton *btn4GPower;
@property (retain, nonatomic) IBOutlet UILabel *lblAwareness;
@end
