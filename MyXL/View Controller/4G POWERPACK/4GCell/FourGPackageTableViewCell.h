//
//  FourGPackageTableViewCell.h
//  MyXL
//
//  Created by tyegah on 8/27/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FourGPackageTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *lblPackageName;
@property (retain, nonatomic) IBOutlet UILabel *lblPeriod;
//@property (retain, nonatomic) IBOutlet UILabel *lblSpeed;
@property (retain, nonatomic) IBOutlet UILabel *lblPrice;
@property (retain, nonatomic) IBOutlet UILabel *lblBuy;
@property (retain, nonatomic) IBOutlet UIView *buyView;
@property (retain, nonatomic) IBOutlet UIButton *btnCurrentPackage;

@end
