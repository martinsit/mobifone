//
//  FourGPowerPackViewController.m
//  MyXL
//
//  Created by tyegah on 8/27/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "FourGPowerPackViewController.h"
#import "FourGPackageTableViewCell.h"
#import "FourGPackageModel.h"
#import "KASlideShow2.h"
#import "ThankYouViewController.h"
#import "ShopLocatorViewController.h"
#import "Replace4GUsimViewController.h"
#import "FourGPackageFooterCell.h"
#import "UnderLineLabel.h"

@interface FourGPowerPackViewController ()<KASlideShowDelegate, UITableViewDataSource, UITableViewDelegate,UIAlertViewDelegate>
@property (retain, nonatomic) IBOutlet UIView *bannerWrapperView;
@property (retain, nonatomic) IBOutlet UIView *headerView;
@property (retain, nonatomic) IBOutlet UILabel *lbl4gStatus;
@property (retain, nonatomic) IBOutlet UIImageView *iconHandset;
@property (retain, nonatomic) IBOutlet UIImageView *iconSimCard;
@property (retain, nonatomic) IBOutlet UIImageView *iconPackage;
@property (retain, nonatomic) IBOutlet KASlideShow2 *bannerView;
@property (retain, nonatomic) IBOutlet UIView *middleView;
@property (retain, nonatomic) IBOutlet UITableView *tblView;
@property (retain, nonatomic) IBOutlet UIButton *btnBanner;
@property (retain, nonatomic) IBOutlet UnderLineLabel *lblBannerText;
@property (retain, nonatomic) IBOutlet UIImageView *iconMiddleBanner;
@property (retain, nonatomic) IBOutlet UILabel *lblTitleMiddleBanner;
@property (retain, nonatomic) IBOutlet UILabel *lblDescMiddleBanner;
@property (retain, nonatomic) IBOutlet UIPageControl *bannerPageControl;
@property (retain, nonatomic) IBOutlet UIButton *btnPreviousBanner;
@property (retain, nonatomic) IBOutlet UIButton *btnNextBanner;
@property (nonatomic, copy) NSString *trxId;
@property (nonatomic, retain) FourGPackageItemModel *pkgItem;
@property (retain, nonatomic) IBOutlet UIView *rewardsView;
@property (retain, nonatomic) IBOutlet UIImageView *imgRewards;
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;
@property (retain, nonatomic) IBOutlet UILabel *lblDesc;
@property (retain, nonatomic) IBOutlet UILabel *lblLinkDesc;

@property (retain, nonatomic) IBOutlet UIButton *btnRewards;
@property (retain, nonatomic) IBOutlet UnderLineLabel *lblRewardsLinkUrl;

@end

@implementation FourGPowerPackViewController

@synthesize matrixType;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_tblView registerNib:[UINib nibWithNibName:@"FourGPackageTableViewCell" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"FourGPackageCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"FourGPackageFooterCell" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"FourGPackageFooterCell"];
    _tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupMatrix];
    [self setupView];
    [self setupBanner];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_bannerView stop];
    [_bannerView setDelegate:nil];
}

#pragma mark - View Setup

-(void)setupMatrix
{
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *matrixResult = sharedData.fourGModel.matrix;
    matrixType = convertMatrix(matrixResult);
    
    switch(matrixType)
    {
        case MATRIX_000:
            sharedData.isFourGHandset   = NO;
            sharedData.isFourGSim       = NO;
            sharedData.isFourGPackage   = NO;
            break;
        case MATRIX_001:
            sharedData.isFourGHandset   = NO;
            sharedData.isFourGSim       = NO;
            sharedData.isFourGPackage   = YES;
            break;
        case MATRIX_010:
            sharedData.isFourGHandset   = NO;
            sharedData.isFourGSim       = YES;
            sharedData.isFourGPackage   = NO;
            break;
        case MATRIX_011:
            sharedData.isFourGHandset   = NO;
            sharedData.isFourGSim       = YES;
            sharedData.isFourGPackage   = YES;
            break;
        case MATRIX_100:
            sharedData.isFourGHandset   = YES;
            sharedData.isFourGSim       = NO;
            sharedData.isFourGPackage   = NO;
            break;
        case MATRIX_101:
            sharedData.isFourGHandset   = YES;
            sharedData.isFourGSim       = NO;
            sharedData.isFourGPackage   = YES;
            break;
        case MATRIX_110:
            sharedData.isFourGHandset   = YES;
            sharedData.isFourGSim       = YES;
            sharedData.isFourGPackage   = NO;
            break;
        default:
            sharedData.isFourGHandset   = YES;
            sharedData.isFourGSim       = YES;
            sharedData.isFourGPackage   = YES;
            break;
    }
}

- (void)setupView
{
    DataManager *sharedData = [DataManager sharedInstance];
    if(sharedData.fourGModel)
    {
        _lbl4gStatus.text = [[Language get:@"your_4g_status" alter:nil] uppercaseString];
        
        int currentLang = [Language getCurrentLanguage];
        // EN
        if (currentLang == 0) {
            _iconHandset.image = sharedData.isFourGHandset ? [UIImage imageNamed:@"handset-active.png"] : [UIImage imageNamed:@"handset-not-active.png"];
            _iconSimCard.image = sharedData.isFourGSim ? [UIImage imageNamed:@"sim-active.png"] : [UIImage imageNamed:@"sim-not-active.png"];
            _iconPackage.image = sharedData.isFourGPackage ? [UIImage imageNamed:@"package-active.png"] : [UIImage imageNamed:@"package-not-active.png"];
        }
        //ID
        else {
            _iconHandset.image = sharedData.isFourGHandset ? [UIImage imageNamed:@"id-handset-active.png"] : [UIImage imageNamed:@"id-handset-not-active.png"];
            _iconSimCard.image = sharedData.isFourGSim ? [UIImage imageNamed:@"id-sim-active.png"] : [UIImage imageNamed:@"id-sim-not-active.png"];
            _iconPackage.image = sharedData.isFourGPackage ? [UIImage imageNamed:@"id-package-active.png"] : [UIImage imageNamed:@"id-package-not-active.png"];
        }
        
        _lblTitleMiddleBanner.text = sharedData.fourGModel.headerTitle;
        _lblDescMiddleBanner.text = sharedData.fourGModel.headerDesc;
    }
    if(sharedData.isFourGPackage && sharedData.isFourGHandset && sharedData.isFourGSim)
    {
        _bannerWrapperView.hidden = YES;
        _middleView.hidden = YES;
        _tblView.hidden = YES;
        _rewardsView.hidden = NO;
        _rewardsView.backgroundColor = kDefaultHygieneBgColor;
        _imgRewards.backgroundColor = kDefaultHygieneBgColor;
        
        if(sharedData.fourGModel.rewardsModel)
        {
            // Title Label and Desc Label hide
            _lblTitle.text = sharedData.fourGModel.headerTitle;
            _lblDesc.text = sharedData.fourGModel.headerDesc;
            _lblTitle.hidden = YES;
            _lblDesc.hidden = YES;
            
            _imgRewards.imageURL = [NSURL URLWithString:sharedData.fourGModel.rewardsModel.backgroundReward];
            _imgRewards.contentMode = UIViewContentModeScaleAspectFit;
            [_btnRewards setTitle:sharedData.fourGModel.rewardsModel.btnDescReward forState:UIControlStateNormal];
            //_btnRewards.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
            _btnRewards.layer.cornerRadius = 5.0;
            _btnRewards.clipsToBounds = YES;
            _btnRewards.titleLabel.numberOfLines = 1;
            _btnRewards.titleLabel.adjustsFontSizeToFitWidth = YES;
            _btnRewards.titleLabel.minimumFontSize = 10.0;
            
            _lblRewardsLinkUrl.text = sharedData.fourGModel.rewardsModel.linkDescReward;
            _lblRewardsLinkUrl.shouldUnderline = YES;
            _lblRewardsLinkUrl.underLineOffset = 2;
            UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                          action:@selector(btnTextRewardsClicked)];
            [_lblRewardsLinkUrl addGestureRecognizer:tapGesture3];
            [_lblRewardsLinkUrl setUserInteractionEnabled:YES];
            
            //setup position
            CGRect btnFrame = _lblRewardsLinkUrl.frame;
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            //CGFloat screenWidth = screenRect.size.width;
            CGFloat screenHeight = screenRect.size.height;
            
            if (IS_IPAD) {
                screenHeight = 768.0;
                //_imgRewards.contentMode = UIViewContentModeScaleAspectFill;
                _imgRewards.contentMode = UIViewContentModeScaleToFill;
            }
            
            btnFrame.origin.y = screenHeight - kDefaultComponentPadding - btnFrame.size.height - 80.0;
            _lblRewardsLinkUrl.frame = btnFrame;
            
            btnFrame = _btnRewards.frame;
            btnFrame.origin.y = _lblRewardsLinkUrl.frame.origin.y - kDefaultComponentPadding - btnFrame.size.height;
            _btnRewards.frame = btnFrame;
        }
    }
    else if (sharedData.isFourGSim && sharedData.isFourGHandset)
    {
        
        _bannerWrapperView.hidden = YES;
        _middleView.frame = CGRectOffset(_middleView.frame, 0, -CGRectGetHeight(_bannerWrapperView.frame));
        _tblView.frame = CGRectOffset(_tblView.frame,0, -CGRectGetHeight(_bannerWrapperView.frame));
        CGRect tblFrame = _tblView.frame;
        tblFrame.size.height = tblFrame.size.height + CGRectGetHeight(_bannerWrapperView.frame);
        _tblView.frame = tblFrame;
        
        //Setup View if Image Banner empty
        if ([sharedData.fourGModel.images count] > 0) {
            _bannerWrapperView.hidden = NO;
            
            if (IS_IPAD) {
                CGRect ipadFrame = _bannerWrapperView.frame;
                ipadFrame.size.width = 1024.0;
                ipadFrame.size.height = 402.0;
                
                _bannerWrapperView.frame = ipadFrame;
                
                ipadFrame = _btnBanner.frame;
                ipadFrame.origin.x = (_bannerWrapperView.frame.size.width - _btnBanner.frame.size.width)/2;
                _btnBanner.frame = ipadFrame;
                
                ipadFrame = _lblBannerText.frame;
                ipadFrame.origin.x = _btnBanner.frame.origin.x;
                _lblBannerText.frame = ipadFrame;
            }
            
            NSLog(@"_bannerWrapperView.frame.origin.y = %f",_bannerWrapperView.frame.origin.y);
            NSLog(@"_bannerWrapperView.frame.size.height = %f",_bannerWrapperView.frame.size.height);
            
            CGRect frame = _middleView.frame;
            frame.origin.y = _bannerWrapperView.frame.origin.y + _bannerWrapperView.frame.size.height + 1;
            _middleView.frame = frame;
            
            frame = _tblView.frame;
            frame.origin.y = _middleView.frame.origin.y + _middleView.frame.size.height;
            frame.size.height = 227;
            
            if (IS_IPAD) {
                frame.size.height = 250;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 568) {
                NSLog(@"iPhone 5/5s");
                frame.size.height = 190;
            }
            
            _tblView.frame = frame;
        }
        else {
            _bannerWrapperView.hidden = YES;
            CGRect frame = _middleView.frame;
            frame.origin.y = _bannerWrapperView.frame.origin.y;
            _middleView.frame = frame;
            
            frame = _tblView.frame;
            frame.origin.y = _middleView.frame.origin.y + _middleView.frame.size.height;
            frame.size.height = frame.size.height + 200;
            _tblView.frame = frame;
        }
    }
    else {
        //Setup View if Image Banner empty
        if ([sharedData.fourGModel.images count] > 0) {
            _bannerWrapperView.hidden = NO;
            
            if (IS_IPAD) {
                //NSLog(@"--- iPad ---");
                CGRect ipadFrame = _bannerWrapperView.frame;
                ipadFrame.size.width = 1024.0;
                ipadFrame.size.height = 402.0;
                ipadFrame.origin.x = 0.0;
                
                _bannerWrapperView.frame = ipadFrame;
                
                _bannerView.contentMode = UIViewContentModeScaleAspectFit;
                
                ipadFrame = _btnBanner.frame;
                ipadFrame.origin.x = (_bannerWrapperView.frame.size.width - _btnBanner.frame.size.width)/2;
                _btnBanner.frame = ipadFrame;
                
                ipadFrame = _lblBannerText.frame;
                ipadFrame.origin.x = _btnBanner.frame.origin.x;
                _lblBannerText.frame = ipadFrame;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 480.0) {
                CGRect iphone4Frame = _bannerWrapperView.frame;
                iphone4Frame.origin.y = 71.0 - 50.0;
                
                _bannerWrapperView.frame = iphone4Frame;
                
                _bannerView.contentMode = UIViewContentModeScaleAspectFit;
            }
            
            //NSLog(@"ELSE _bannerWrapperView.frame.origin.y = %f",_bannerWrapperView.frame.origin.y);
            //NSLog(@"ELSE _bannerWrapperView.frame.size.height = %f",_bannerWrapperView.frame.size.height);
            
            CGRect frame = _middleView.frame;
            frame.origin.y = _bannerWrapperView.frame.origin.y + _bannerWrapperView.frame.size.height + 1;
            _middleView.frame = frame;
            
            frame = _tblView.frame;
            frame.origin.y = _middleView.frame.origin.y + _middleView.frame.size.height;
            frame.size.height = 227.0;
            
            if (IS_IPAD) {
                frame.size.height = 250.0;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 568) {
                //NSLog(@"iPhone 5/5s");
                frame.size.height = 190.0;
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 480) {
                //NSLog(@"iPhone 4/4s");
                frame.size.height = 100.0;
            }
            
            _tblView.frame = frame;
        }
        else {
            _bannerWrapperView.hidden = YES;
            CGRect frame = _middleView.frame;
            frame.origin.y = _bannerWrapperView.frame.origin.y;
            _middleView.frame = frame;
            
            frame = _tblView.frame;
            frame.origin.y = _middleView.frame.origin.y + _middleView.frame.size.height;
            frame.size.height = frame.size.height + 200;
            _tblView.frame = frame;
        }
    }
}

- (void)setupBanner
{
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.fourGModel.images count] > 0)
    {
        
        if([sharedData.fourGModel.images count] == 1)
        {
            _bannerPageControl.hidden = YES;
            _btnNextBanner.hidden = YES;
            _btnPreviousBanner.hidden = YES;
        }
        
        NSMutableArray *imageArray = [[NSMutableArray alloc] init];
        for (FourGPackageImagesModel *imageObj in sharedData.fourGModel.images) {
            [imageArray addObject:imageObj.images];
        }
        
        _bannerPageControl.numberOfPages = [sharedData.fourGModel.images count];
        _bannerView.delegate = self;
        [_bannerView setDelay:5]; // Delay between transitions
        [_bannerView setTransitionDuration:1]; // Transition duration
        [_bannerView setTransitionType:KASlideShowTransitionSlide]; // Choose a transition type (fade or slide)
        [_bannerView setImagesContentMode:UIViewContentModeScaleAspectFill];
        [_bannerView addImagesFromURLs:imageArray];
        _bannerView.isResourceFromURL = YES;
        [_bannerView addGesture:KASlideShowGestureSwipe];
        [_bannerView setClipsToBounds:YES]; //new
        
        // Setup wording on button slideshow
        FourGPackageImagesModel *imageObj2 = [sharedData.fourGModel.images objectAtIndex:0];
        
        [_btnBanner setTitle:imageObj2.wordingTopBtn forState:UIControlStateNormal];
        _btnBanner.layer.cornerRadius = 5.0;
        _btnBanner.clipsToBounds = YES;
        _btnBanner.titleLabel.numberOfLines = 1;
        _btnBanner.titleLabel.adjustsFontSizeToFitWidth = YES;
        _btnBanner.titleLabel.minimumFontSize = 8.0;
        _btnBanner.tag = 0; // current position
        
        _lblBannerText.text = imageObj2.wordingBtnTitle;
        _lblBannerText.shouldUnderline = YES;
        _lblBannerText.underLineOffset = 2;
        UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(btnTextBannerClicked:)];
        [_lblBannerText addGestureRecognizer:tapGesture3];
        [_lblBannerText setUserInteractionEnabled:YES];
        _lblBannerText.tag = 0; // current position
    }
}

#pragma mark - KASlideShow delegate

- (void)kaSlideShowWillShowNext:(KASlideShow2 *)slideShow
{
    
}

- (void)kaSlideShowWillShowPrevious:(KASlideShow2 *)slideShow
{
    
}

- (void)kaSlideShowDidShowNext:(KASlideShow2 *)slideShow
{
    _bannerPageControl.currentPage = slideShow.currentIndex;
    
    // Setup wording on button slideshow
    DataManager *sharedData = [DataManager sharedInstance];
    FourGPackageImagesModel *imageObj = [sharedData.fourGModel.images objectAtIndex:slideShow.currentIndex];
    
    [_btnBanner setTitle:imageObj.wordingTopBtn forState:UIControlStateNormal];
    _btnBanner.tag = slideShow.currentIndex; // current position
    
    _lblBannerText.text = imageObj.wordingBtnTitle;
    _lblBannerText.tag = slideShow.currentIndex; // current position
}

- (void)kaSlideShowDidShowPrevious:(KASlideShow2 *)slideShow
{
    _bannerPageControl.currentPage = slideShow.currentIndex;
    
    // Setup wording on button slideshow
    DataManager *sharedData = [DataManager sharedInstance];
    FourGPackageImagesModel *imageObj = [sharedData.fourGModel.images objectAtIndex:slideShow.currentIndex];
    
    [_btnBanner setTitle:imageObj.wordingTopBtn forState:UIControlStateNormal];
    _btnBanner.tag = slideShow.currentIndex; // current position
    
    _lblBannerText.text = imageObj.wordingBtnTitle;
    _lblBannerText.tag = slideShow.currentIndex; // current position
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DataManager *sharedData = [DataManager sharedInstance];
    if(sharedData.fourGModel)
        return [sharedData.fourGModel.packageList count] + 1;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataManager *sharedData = [DataManager sharedInstance];
    if(indexPath.row < [sharedData.fourGModel.packageList count])
    {

        FourGPackageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FourGPackageCell" forIndexPath:indexPath];
        
        FourGPackageItemModel *itemObj = [sharedData.fourGModel.packageList objectAtIndex:indexPath.row];
        if(itemObj)
        {
            cell.lblPrice.text = [NSString stringWithFormat:@"Rp %@",[AXISnetCommon formatStringWithCurrency:[itemObj.price doubleValue]]];
            cell.lblPeriod.text = [NSString stringWithFormat:@"%@ | %@", itemObj.volume, itemObj.duration];
            //cell.lblSpeed.text = @"";
            cell.lblPackageName.text = itemObj.packageName;
            cell.lblBuy.text = [[Language get:@"buy" alter:nil] uppercaseString];
            if(itemObj.isActive)
            {
                cell.buyView.hidden = YES;
                cell.btnCurrentPackage.hidden = NO;
                [cell.btnCurrentPackage setTitle:[Language get:@"your_plan" alter:nil] forState:UIControlStateNormal];
            }
            else
            {
                cell.btnCurrentPackage.hidden = YES;
                cell.buyView.hidden = NO;
                cell.buyView.layer.borderWidth = 1;
                cell.buyView.layer.borderColor = [UIColor colorWithRed:104.0/255.0 green:180.0/255.0 blue:44.0/255.0 alpha:1.0].CGColor;
                cell.buyView.layer.cornerRadius = 5;
                cell.buyView.layer.masksToBounds = YES;
            }
        }
        return cell;
    }
    else
    {
        FourGPackageFooterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FourGPackageFooterCell" forIndexPath:indexPath];
        
        cell.lblTitle.text = sharedData.fourGModel.footerTitle;
        cell.lblTitle.shouldUnderline = YES;
        cell.lblTitle.underLineOffset = 2;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(btnLinkFooterClicked:)];
        [cell.lblTitle addGestureRecognizer:tapGesture];
        [cell.lblTitle setUserInteractionEnabled:YES];
        cell.lblTitle.tag = 0; // current position
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataManager *sharedData = [DataManager sharedInstance];
    if(indexPath.row == [sharedData.fourGModel.packageList count])
        return 100;
    return 70;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DataManager *sharedData = [DataManager sharedInstance];
    if(indexPath.row < [sharedData.fourGModel.packageList count])
    {
        FourGPackageItemModel *itemModel = [sharedData.fourGModel.packageList objectAtIndex:indexPath.row];
        if(itemModel && !itemModel.isActive)
        {
            _pkgItem = itemModel;
            
            /*
             * Google Analytic
             */
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            // Screen tracking
            NSString *gaLabel = @"";
            if (matrixType == MATRIX_110) {
                // Package Offer
                gaLabel = [NSString stringWithFormat:@"Plan Selection - Buy %@",_pkgItem.pkgId];
            }
            else if (matrixType == MATRIX_100 || matrixType == MATRIX_101) {
                // USIM Offer
                gaLabel = [NSString stringWithFormat:@"Usim - Buy %@",_pkgItem.pkgId];
            }
            else if (matrixType == MATRIX_010 || matrixType == MATRIX_011) {
                // Merchant Offer
                gaLabel = [NSString stringWithFormat:@"Merchant - Buy %@",_pkgItem.pkgId];
            }
            else {
                // Bundling Offer
                gaLabel = [NSString stringWithFormat:@"Bundling - Buy %@",_pkgItem.pkgId];
            }
            
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"       // Event category (required)
                                                                  action:@"4G Power Pack" // Event action (required)
                                                                   label:gaLabel          // Event label
                                                                   value:nil] build]];
            [[GAI sharedInstance] dispatch];
            /******************************/
            
            if([itemModel.paymentMethod isEqualToString:@"cc"])
            {
                // TODO : V1.9
                // Add pay with credit card on buy package alert
                UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                          message:itemModel.confirmDesc
                                                                         delegate:self
                                                                cancelButtonTitle:[[Language get:@"pay_with_cc" alter:nil] uppercaseString]
                                                                otherButtonTitles:[[Language get:@"yes" alter:nil] uppercaseString],[[Language get:@"no" alter:nil] uppercaseString],nil];
                buyPackageAlert.tag = 4;
                
                [buyPackageAlert show];
                [buyPackageAlert release];
            }
            else
            {
                UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                          message:itemModel.confirmDesc
                                                                         delegate:self
                                                                cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                                otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
                
                buyPackageAlert.tag = 2;
                
                [buyPackageAlert show];
                [buyPackageAlert release];
            }
        }
    }
}

#pragma mark - IBActions

- (IBAction)btnBannerClicked:(id)sender
{
    NSLog(@"btnBannerClicked");
    
    /*
     * Google Analytic
     */
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    // Screen tracking
    NSString *gaLabel = @"";
    if (matrixType == MATRIX_100 || matrixType == MATRIX_101) {
        // Usim Offer
        gaLabel = @"Usim - Form 4G";
    }
    else if (matrixType == MATRIX_010 || matrixType == MATRIX_011) {
        // Merchant Offer
        gaLabel = @"Merchant - 4G Phone Bundling";
    }
    else {
        // Bundling Offer
        gaLabel = @"Bundling - Dapatkan Segera Banner";
    }
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"       // Event category (required)
                                                          action:@"4G Power Pack" // Event action (required)
                                                           label:gaLabel          // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    /******************************/
    
    UIButton *button = (UIButton *)sender;
    
    DataManager *sharedData = [DataManager sharedInstance];
    FourGPackageImagesModel *imageObj = [sharedData.fourGModel.images objectAtIndex:button.tag];
    
    [self redirectViewWithHref:imageObj.urlTopBtn];
}

- (void)btnTextBannerClicked:(UITapGestureRecognizer*)tapGesture
{
    
    /*
     * Google Analytic
     */
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    // Screen tracking
    NSString *gaLabel = @"";
    if (matrixType == MATRIX_100 || matrixType == MATRIX_101) {
        // Usim Offer
        gaLabel = @"Usim - XL Locator";
    }
    else if (matrixType == MATRIX_010 || matrixType == MATRIX_011) {
        // Merchant Offer
        gaLabel = @"Merchant - Dapatkan Micro Usim";
    }
    else {
        // Bundling Offer
        gaLabel = @"Bundling - Dapatkan Micro Usim";
    }
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"       // Event category (required)
                                                          action:@"4G Power Pack" // Event action (required)
                                                           label:gaLabel          // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    /******************************/
    
    UnderLineLabel *label = (UnderLineLabel *)tapGesture.view;
    NSLog(@"--> tag label %li",(long)label.tag);
    
    DataManager *sharedData = [DataManager sharedInstance];
    FourGPackageImagesModel *imageObj = [sharedData.fourGModel.images objectAtIndex:label.tag];
    
    NSLog(@"--> url %@",imageObj.urlBtnTitle);
    
    [self redirectViewWithHref:imageObj.urlBtnTitle];
}
- (IBAction)btnRewardsClicked:(id)sender {
    NSLog(@"btnRewardsClicked");
    
    /*
     * Google Analytic
     */
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // Screen tracking
    
    NSString *gaLabel = @"Complete";
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"       // Event category (required)
                                                          action:@"4G Power Pack" // Event action (required)
                                                           label:gaLabel          // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    /******************************/
    
    DataManager *sharedData = [DataManager sharedInstance];
    [self redirectViewWithHref:sharedData.fourGModel.rewardsModel.btnUrlReward];
}

- (void)btnTextRewardsClicked {
    NSLog(@"btnTextRewardsClicked");
    
    /*
     * Google Analytic
     */
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // Screen tracking
    
    NSString *gaLabel = @"Complete";
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"       // Event category (required)
                                                          action:@"4G Power Pack" // Event action (required)
                                                           label:gaLabel          // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    /******************************/
    
    DataManager *sharedData = [DataManager sharedInstance];
    [self redirectViewWithHref:sharedData.fourGModel.rewardsModel.linkUrlReward];
}

- (IBAction)btnNextBannerClicked:(id)sender
{
    [_bannerView next];
}

- (IBAction)btnPrevBannerClicked:(id)sender
{
    [_bannerView previous];
}


- (void)btnLinkFooterClicked:(UITapGestureRecognizer*)tapGesture
{
    DataManager *sharedData = [DataManager sharedInstance];
    [self redirectViewWithHref:sharedData.fourGModel.footerUrl];
}

- (void)redirectViewWithHref:(NSString *)href
{
    if ([href hasSuffix:@"#replacesim"]) {
        NSLog(@"#replacesim");
        [self redirectTo4GUSIM];
    }
    else if ([href hasSuffix:@"#store_locator"]) {
        NSLog(@"#store_locator");
        [self redirectToStoreLocator];
    }
    else {
        NSLog(@"Open Browser");
        [self showModalWebViewForURL:[NSURL URLWithString:href]];
    }
}

- (void)redirectToStoreLocator
{
    ShopLocatorViewController *shopLocatorViewController = [[ShopLocatorViewController alloc] initWithNibName:@"ShopLocatorViewController" bundle:nil];
    [self.navigationController pushViewController:shopLocatorViewController animated:YES];
    [shopLocatorViewController release];
    shopLocatorViewController = nil;
}

- (void)redirectTo4GUSIM
{
    Replace4GUsimViewController *replace4GUsimViewController = [[Replace4GUsimViewController alloc] initWithNibName:@"Replace4GUsimViewController" bundle:nil];
    [replace4GUsimViewController createBarButtonItem:BACK_NOTIF];
    
    MenuModel *menu = [[MenuModel alloc] init];
    menu.groupName = [Language get:@"form" alter:nil].uppercaseString;
    menu.menuName = [Language get:@"form_replace_simcard" alter:nil].uppercaseString;
    
    replace4GUsimViewController.menuModel = menu;
    [menu release];
    menu = nil;
    
    [self.navigationController pushViewController:replace4GUsimViewController animated:YES];
    [replace4GUsimViewController release];
    replace4GUsimViewController = nil;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    /*
    if(buttonIndex == 0)
    {
        [self performBuyPackage:_pkgItem.pkgId withAmount:_pkgItem.price];
    }*/
    
    // tag == 2 Buy Package
    if (alertView.tag == 2)
    {
        if (buttonIndex == 0) {
            //NSLog(@"YES");
            // TODO : V1.9
            // Loading time for buy package
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.dateToday = [[NSDate alloc] init];
            //NSLog(@"Buy Package Time %@", appDelegate.dateToday);
            [self performBuyPackage:_pkgItem.pkgId withAmount:_pkgItem.price];
        }
        else {
            //NSLog(@"NO");
        }
    }
    
    // Pay With Credit Card, tag == 4 Credit Card
    if (alertView.tag == 4)
    {
        // pay with credit card
        if(buttonIndex == 0)
        {
            NSURL *url = nil;
            DataManager *sharedData = [DataManager sharedInstance];
            
            NSString *saltKeyAPI = SALT_KEY;
            NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
            //            NSString *paymentId = @"1"; //hardcoded
            NSString *lang = sharedData.profileData.language;
            
            NSString *packageID = _pkgItem.pkgId;
            NSString *price = _pkgItem.price;
            
            NSString *baseUrlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API,BUY_PKG_M_KARTU];
            NSString *paramString = [NSString stringWithFormat:@"msisdn=%@&msisdnInitiator=%@&currency=IDR&productType=1&productId=%@&lang=%@&amount=%@&channelID=&desc=&returnUrl=&extraParam=&remarks=&signature=&password=&checkoutType=",msisdn,msisdn,packageID,lang,price];
            //NSLog(@"paramString = %@",paramString);
            paramString = [EncryptDecrypt doCipherForiOS7:paramString action:kCCEncrypt withKey:saltKeyAPI];
            
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlString,paramString]];
            //NSLog(@"URL M-Kartu = %@",url);
            
            SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
            webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
            [self presentViewController:webViewController animated:YES completion:NULL];
        }
        else if (buttonIndex == 1)
        {
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.dateToday = [[NSDate alloc] init];
            //NSLog(@"Buy Package Time %@", appDelegate.dateToday);
            [self performBuyPackage:_pkgItem.pkgId withAmount:_pkgItem.price];
        }
        //NSLog(@"button index %d", buttonIndex);
    }
}

- (void)performBuyPackage:(NSString*)packageId withAmount:(NSString*)amount
{
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = BUY_PACKAGE_REQ;
    self.trxId = generateUniqueString();
    [request buyPackage:packageId withAmount:amount withTrxId:self.trxId];
}

- (void)showModalWebViewForURL:(NSURL *)url
{
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
    webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:webViewController animated:YES completion:NULL];
}

#pragma mark - AXISNET Request Delegate

- (void)requestDoneWithInfo:(NSDictionary *)result
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success)
    {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        if(type == BUY_PACKAGE_REQ)
        {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            NSString *reason = [result valueForKey:REASON_KEY];
            thankYouViewController.info = reason;
            thankYouViewController.displayFBButton = NO;
            thankYouViewController.trxId = self.trxId;
            thankYouViewController.hideInboxBtn = NO;
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            [thankYouViewController release];
            thankYouViewController = nil;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_headerView release];
    [_lbl4gStatus release];
    [_iconHandset release];
    [_iconSimCard release];
    [_iconPackage release];
    [_bannerView release];
    [_middleView release];
    [_tblView release];
    [_btnBanner release];
    [_iconMiddleBanner release];
    [_lblTitleMiddleBanner release];
    [_lblDescMiddleBanner release];
    [_lblBannerText release];
    [_bannerPageControl release];
    [_btnPreviousBanner release];
    [_btnNextBanner release];
    [_bannerWrapperView release];
    [_rewardsView release];
    [_imgRewards release];
    [_lblTitle release];
    [_lblDesc release];
    [_lblLinkDesc release];
    [_lblRewardsLinkUrl release];
    [_btnRewards release];
    [super dealloc];
}

- (void)viewDidUnload
{
    [self setHeaderView:nil];
    [self setLbl4gStatus:nil];
    [self setIconHandset:nil];
    [self setIconSimCard:nil];
    [self setIconPackage:nil];
    [self setBannerView:nil];
    [self setMiddleView:nil];
    [self setTblView:nil];
    [self setBtnBanner:nil];
    [self setIconMiddleBanner:nil];
    [self setLblTitleMiddleBanner:nil];
    [self setLblDescMiddleBanner:nil];
    [self setLblBannerText:nil];
    [self setBannerPageControl:nil];
    [self setBtnPreviousBanner:nil];
    [self setBtnNextBanner:nil];
    [self setBannerWrapperView:nil];
    [self setRewardsView:nil];
    [self setImgRewards:nil];
    [self setLblTitle:nil];
    [self setLblDesc:nil];
    [self setLblLinkDesc:nil];
    [self setLblRewardsLinkUrl:nil];
    [self setBtnRewards:nil];
    [super viewDidUnload];
}

@end
