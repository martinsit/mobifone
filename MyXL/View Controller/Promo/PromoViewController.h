//
//  PromoViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "DataParser.h"

@interface PromoViewController : BasicViewController <DataParserDelegate> {
    NSArray *_content;
}

@property (nonatomic, retain) NSArray *content;

@end
