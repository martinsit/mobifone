//
//  PromoViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PromoViewController.h"
#import "SectionHeaderView.h"
#import "SectionFooterView.h"
#import "FooterView.h"
#import "PromoCell.h"
#import "AXISnetCellBackground.h"
#import "Constant.h"
#import "DataManager.h"
#import "PromoModel.h"
#import "DetailPromoViewController.h"
#import "AXISnetCommon.h"

@interface PromoViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

- (void)dummyDataPromo;

@end

@implementation PromoViewController

@synthesize content = _content;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
    FooterView *footer = [[[FooterView alloc] initWithFrame:CGRectMake(0.0, 0.0, _theTableView.bounds.size.width, 50.0)] autorelease];
    _theTableView.tableFooterView = footer;*/
    
    //[self dummyDataPromo];
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    DataManager *dataManager = [DataManager sharedInstance];
    self.content = dataManager.promoData;
    //NSLog(@"self.content count = %i",[self.content count]);
    [_theTableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.content = nil;
}

- (void)dealloc {
    [_content release];
    _content = nil;
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.content count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NotifCell";
	
    PromoCell *cell = (PromoCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		cell = [[PromoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.backgroundView = [[[AXISnetCellBackground alloc] initWithFrame:cell.frame
                                                           withDefaultColor:kDefaultButtonGrayColor
                                                          withSelectedColor:kDefaultYellowColor] autorelease];
        
        cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] initWithFrame:cell.frame
                                                                   withDefaultColor:kDefaultButtonGrayColor
                                                                  withSelectedColor:kDefaultYellowColor] autorelease];
        
        ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
	}
    
    PromoModel *promoModel = [self.content objectAtIndex:indexPath.row];
    cell.titleLabel.text = promoModel.promoTitle;
    cell.shortDescLabel.text = promoModel.promoShortDesc;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section
{
    DataManager *dataManager = [DataManager sharedInstance];
    return dataManager.menuModel.menuName;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForFooterInSection:(NSInteger)section
{
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView
viewForFooterInSection:(NSInteger)section
{
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat iPhoneWidth = 220;
    CGFloat iPadWidth = 800;
    
    PromoModel *promoModel = [self.content objectAtIndex:indexPath.row];
    
    NSString *title = promoModel.promoTitle;
    CGSize titleSize = CGSizeMake(0, 0);
    
    NSString *desc = promoModel.promoShortDesc;
    CGSize descSize = CGSizeMake(0, 0);
    
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        titleSize = [title sizeWithFont:[UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize]
                      constrainedToSize:CGSizeMake(iPadWidth, 1000.0f)];
        
        descSize = [desc sizeWithFont:[UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize]
                    constrainedToSize:CGSizeMake(iPadWidth, 1000.0f)];
    }
    else {
        titleSize = [title sizeWithFont:[UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize]
                      constrainedToSize:CGSizeMake(iPhoneWidth, 1000.0f)];
        
        descSize = [desc sizeWithFont:[UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize]
                    constrainedToSize:CGSizeMake(iPhoneWidth, 1000.0f)];
    }
    
    return titleSize.height + descSize.height + 30;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataManager *dataManager = [DataManager sharedInstance];
//    PromoViewController *promoVC = [[[PromoViewController alloc] initWithNibName:@"PromoViewController" bundle:nil] autorelease];
    DetailPromoViewController *detailPromoViewController = [[DetailPromoViewController alloc] initWithNibName:@"DetailPromoViewController" bundle:nil];
    detailPromoViewController.selectedPromo = indexPath.row;
    
    //detailPromoViewController.headerIconMaster = icon([NSString stringWithFormat:@"gid_%@",dataManager.menuModel.groupId]);
    
    detailPromoViewController.headerIconMaster = icon(@"gid_50");
    
    detailPromoViewController.headerTitleMaster = dataManager.menuModel.menuName;
    
    [self.navigationController pushViewController:detailPromoViewController animated:YES];
    detailPromoViewController = nil;
    [detailPromoViewController release];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    //DataManager *dataManager = [DataManager sharedInstance];
    
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        //headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 80.0) withLabel:sectionTitle isFirstSection:YES] autorelease];
        /*
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 80.0)
                                                     withLabel:sectionTitle
                                                      withIcon:icon([NSString stringWithFormat:@"gid_%@",dataManager.menuModel.groupId])
                                                isFirstSection:YES] autorelease];*/
        
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:icon(@"gid_50")
                                                isFirstSection:YES] autorelease];
    }
    else {
        //headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40.0) withLabel:sectionTitle isFirstSection:NO] autorelease];
        /*
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40.0)
                                                     withLabel:sectionTitle
                                                      withIcon:icon([NSString stringWithFormat:@"gid_%@",dataManager.menuModel.groupId])
                                                isFirstSection:NO] autorelease];*/
        
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:icon(@"gid_50")
                                                isFirstSection:NO] autorelease];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

#pragma mark - Selector

- (void)dummyDataPromo {
    NSString *data = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"msisdn\":\"JpCYsSi4ab3QVAep2McfuA==\",\"status\":\"1\",\"result\":[{\"promo_id\":\"73\",\"promo_title\":\"Smartphone Package Basic\",\"promo_body\":\"Enjoy the perfect partner for Smartphone. Unlimited internet with FUP 800MB, 200 minute call and SMS to all AXIS. Smartphone package Basic for IDR 34.900 Valid for 30 days.\",\"promo_datetime\":\"2012-10-29 07:52:53\",\"promo_url\":\"http://www.axisworld.co.id/SmartphonePackage\",\"promo_status\":\"1\",\"promo_shortdesc\":\"Enjoy the perfect partner for Smartphone. Unlimited internet with FUP 800MB, 200 minute call and SMS to all AXIS.\"}]}";
    
    //NSString *data = @"{\"username\":\"online\",\"password\":\"433169d5d9bcbb6d43f0d288e68f0cad\",\"msisdn\":\"JpCYsSi4ab3QVAep2McfuA==\",\"status\":\"1\",\"result\":[{\"promo_id\":\"73\",\"promo_title\":\"Smartphone Package Basic\",\"promo_body\":\"<p>Enjoy the perfect partner for Smartphone. Unlimited internet with FUP 800MB, 200 minute call and SMS to all AXIS.</p>\r\n<p>Smartphone package Basic for IDR 34.900<br>Valid for 30 days.</p>\",\"promo_datetime\":\"2012-10-29 07:52:53\",\"promo_url\":\"http://www.axisworld.co.id/SmartphonePackage\",\"promo_status\":\"1\",\"promo_shortdesc\":\"Enjoy the perfect partner for Smartphone. Unlimited internet with FUP 800MB, 200 minute call and SMS to all AXIS.\"},{\"promo_id\":\"75\",\"promo_title\":\"Smartphone Package Premium\",\"promo_body\":\"<p>Enjoy the perfect partner for Smartphone. Unlimited internet with FUP 1.5GB, Unlimited call and SMS to all AXIS, and 1000 SMS to OTHER operator.</p>\r\n<p>Smartphone package Premium for IDR 69.900<br>Valid for 30 days.</p>\",\"promo_datetime\":\"2012-10-29 07:51:53\",\"promo_url\":\"http://www.axisworld.co.id/SmartphonePackage\",\"promo_status\":\"1\",\"promo_shortdesc\":\"Enjoy the perfect partner for Smartphone. Unlimited internet with FUP 1.5GB, Unlimited call and SMS to all AXIS, and 1000 SMS to OTHER operator.\"},{\"promo_id\":\"71\",\"promo_title\":\"Night Package\",\"promo_body\":\"<p>No more overspending worries during night time.</p>\r\n<p>Enjoy unlimited internet, call, and SMS to all AXIS, and 10 SMS to other operators for only Rp1,000. Valid from 12 - 6 AM WIB in all AXIS network.</p>\r\n<p>Click <a href=\"http://www.axisworld.co.id/PaketBegadang\">here</a> for further information.</p>\",\"promo_datetime\":\"2012-09-17 01:31:04\",\"promo_url\":\"\",\"promo_status\":\"1\",\"promo_shortdesc\":\"No more overspending worries during night time.\"},{\"promo_id\":\"69\",\"promo_title\":\"AXIS Pro, 3X Better!\",\"promo_body\":\"<p>Now, AXIS Pro Unlimited 3x Better!</p>\r\n<p>Speed up to 7.2Mbps, FUP up to 6GB and the price remains the same starting from Rp 49,000/month.</p>\r\n<p>AXIS Pro Unlimited promo valid starting from July 10<sup>th</sup>&nbsp;- December 31<sup>st</sup> 2012.</p>\r\n<p>Click <a href=\"http://www.axisworld.co.id/Paket_Internetan#AXISPro\">here</a> for further information</p>\",\"promo_datetime\":\"2012-09-17 01:30:53\",\"promo_url\":\"\",\"promo_status\":\"1\",\"promo_shortdesc\":\"Now, AXIS Pro Unlimited 3x Better!\"},{\"promo_id\":\"63\",\"promo_title\":\"Internet Gaul, now 3x Better!\",\"promo_body\":\"<p>Now, the fun of Internet Gaul, <b>3X Better</b>!<br>Speed up to 7.2Mbps, FUP up to 800MB and the price remains the same starting from Rp 9,900/week.</p>\r\n<p>Internet Gaul promo valid starting from July 10<sup>th</sup> - August 31<sup>st</sup> 2012.</p>\r\n<p>Click <a href=\"http://www.axisworld.co.id/Paket_Internetan\">here</a> for further information</p>\",\"promo_datetime\":\"2012-09-17 01:30:27\",\"promo_url\":\"\",\"promo_status\":\"1\",\"promo_shortdesc\":\"Now, the fun of Internet Gaul, 3X Better!Speed up to 7.2Mbps, FUP up to 800MB and the price remains the same starting from Rp 9,900/week.\"},{\"promo_id\":\"65\",\"promo_title\":\"Catch reload blessing from AXIS!\",\"promo_body\":\"<p>Keep reloading your AXIS number with a minimum amount of Rp10,000, starting from <b>July 10<sup>th</sup> -September 10<sup>th</sup> 2012</b>, and collect the points!</p>\r\n<p>Win New Kijang Innova, Honda Motorcycle, LCD TV Samsung, Samsung Galaxy Y, Samsung Galaxy Tab, BlackBerry, hundreds Nexian handphone and thousands AXIS reload voucher.</p>\r\n<p>Click <a href=\"http://www.axisworld.co.id/TangkapBerkahIsiUlang\">here</a> for further information</p>\r\n\",\"promo_datetime\":\"2012-09-17 01:29:52\",\"promo_url\":\"\",\"promo_status\":\"1\",\"promo_shortdesc\":\"Keep reloading your AXIS number with a minimum amount of Rp10,000, starting from July 10th -September 10th 2012, and collect the points!\"}]}";
    
    DataParser *dataParser = [[DataParser alloc] init];
    dataParser.delegate = self;
    [dataParser parsePromoData:data];
    [dataParser release];
}

#pragma mark - DataParserDelegate

- (void)parsePromoDone:(BOOL)success withReason:(NSString*)reason {
    if (success) {
        DataManager *dataManager = [DataManager sharedInstance];
        self.content = dataManager.promoData;
        //NSLog(@"self.content count = %i",[self.content count]);
        [_theTableView reloadData];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
