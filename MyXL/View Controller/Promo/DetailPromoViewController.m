//
//  DetailPromoViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "DetailPromoViewController.h"
#import "DataManager.h"
#import "PromoModel.h"

#import "SectionHeaderView.h"
#import "Constant.h"
#import <QuartzCore/QuartzCore.h>
#import "ContentView.h"

@interface DetailPromoViewController ()

@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

@end

@implementation DetailPromoViewController

@synthesize selectedPromo;
@synthesize menuModel = _menuModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _contentView.backgroundColor = kDefaultWhiteColor;
}

- (void)viewWillAppear:(BOOL)animated
{
    //--------------//
    // Setup Header //
    //--------------//
    
//    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
//                                                               withLabelForXL:_menuModel.groupName
//                                                               isFirstSection:YES];
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:[Language get:@"hot_promo" alter:nil]
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:_menuModel.menuName
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //..:: added by iNot ::..
    DataManager *dataManager = [DataManager sharedInstance];
    
    PromoModel *promoModel = [dataManager.promoData objectAtIndex:selectedPromo];
    NSString *title = promoModel.promoTitle;
    NSString *body = promoModel.promoBody;
    id promoUrl = promoModel.promoUrl;
    
    NSString *href = [NSString string];
    NSString *hrefString = [NSString string];
    NSString *html = [NSString string];
    
    if (![promoUrl isEqualToString:@""]) {
        NSString *url = promoUrl;
        href = url;
        hrefString = [Language get:@"click_here" alter:nil];
        
        html = [NSString stringWithFormat:@"<html><style>h4{font-family:GothamRounded-Medium; font-size:16px; color:#e95225; margin:0} p {font-family:GothamRounded-Light; font-size:13px; margin:0} .button.orange {background: #e95225;} a.button {margin: 10px 5px 0px 0;} .button {text-align: center;-webkit-appearance: none;display: block;cursor: pointer;border: none;font-family: inherit;display: inline-block;font-size: 14px;font-family:GothamRounded-Medium;margin: 0 5px 0 0;padding: 5px 15px;background: #00b2db;color: #fff !important;border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px;-o-border-radius: 5px;}a, a:link, a:visited {text-decoration: none;color: inherit;outline: none;}</style><body><br><h4>%@</h4><br><p>%@</p><br><a target=\"_blank\" href=\"%@\" class=\"button orange\">%@</a></body></html>",title,body,href,hrefString];
    }
    else {
        html = [NSString stringWithFormat:@"<html><style>h4{font-family:GothamRounded-Medium; font-size:16px; color:#e95225; margin:0} p {font-family:GothamRounded-Light; font-size:13px; margin:0} .button.orange {background: #e95225;} a.button {margin: 10px 5px 0px 0;} .button {text-align: center;-webkit-appearance: none;display: block;cursor: pointer;border: none;font-family: inherit;display: inline-block;font-size: 14px;font-family:GothamRounded-Medium;margin: 0 5px 0 0;padding: 5px 15px;background: #00b2db;color: #fff !important;border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px;-o-border-radius: 5px;}a, a:link, a:visited {text-decoration: none;color: inherit;outline: none;}</style><body><br><h4>%@</h4><br><p>%@</p><br></body></html>",title,body];
    }
    
    
    [_webView loadHTMLString:[NSString stringWithFormat:@"%@",html] baseURL:nil];
    [_webView setBackgroundColor:[UIColor clearColor]];
    
    CGRect frame = _webView.frame;
    frame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height;
    _webView.frame = frame;
    
    for (UIView *wview in [[[_webView subviews] objectAtIndex:0] subviews]) {
        if([wview isKindOfClass:[UIImageView class]])
        {
            wview.hidden = YES;
        }
    }
    
    frame = _contentView.frame;
    frame.size.height = _webView.frame.origin.y + _webView.frame.size.height;
    _contentView.frame = frame;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)inWeb
shouldStartLoadWithRequest:(NSURLRequest *)inRequest
 navigationType:(UIWebViewNavigationType)inType
{
    if (inType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    return YES;
}

@end
