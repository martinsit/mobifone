//
//  DetailPromoViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "MenuModel.h"

@interface DetailPromoViewController : BasicViewController <UIWebViewDelegate> {
    int selectedPromo;
    MenuModel *_menuModel;
}

@property (readwrite) int selectedPromo;
@property (nonatomic, retain) MenuModel *menuModel;

@end
