//
//  TransactionHistoryViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/25/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "TransactionHistoryViewController.h"
#import "SectionHeaderView.h"
#import "SectionFooterView.h"
#import "FooterView.h"
#import "TransactionHistoryCell.h"
#import "AXISnetCellBackground.h"
#import "Constant.h"
#import "DataManager.h"
#import "TransactionHistoryModel.h"
#import "AXISnetCommon.h"

#import "RectBackground.h"

@interface TransactionHistoryViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@end

@implementation TransactionHistoryViewController

@synthesize content = _content;
@synthesize menuModel = _menuModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
    FooterView *footer = [[[FooterView alloc] initWithFrame:CGRectMake(0.0, 0.0, _theTableView.bounds.size.width, 50.0)] autorelease];
    _theTableView.tableFooterView = footer;*/
    
    //[_theTableView setBackgroundView:nil];
    //[_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

//- (void)viewWillAppear:(BOOL)animated {
//    [_theTableView reloadData];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.content = nil;
}

- (void)dealloc {
    [_content release];
    _content = nil;
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    else {
        return [self.content count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TransactionCell";
	
    TransactionHistoryCell *cell = (TransactionHistoryCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		cell = [[TransactionHistoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                      withBasicColor:kDefaultWhiteColor
                                                   withSelectedColor:kDefaultGrayColor] autorelease];
        
        cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                              withBasicColor:kDefaultWhiteColor
                                                           withSelectedColor:kDefaultGrayColor] autorelease];
        ((RectBackground *)cell.selectedBackgroundView).selected = YES;
	}
    
    TransactionHistoryModel *trxHistoryModel = [self.content objectAtIndex:indexPath.row];
    
    cell.timeLabel.text = trxHistoryModel.time;
    
    if ([trxHistoryModel.type isEqualToString:@"Reload"]) {
        cell.typeLabel.text = [Language get:@"reload" alter:nil].uppercaseString;
        cell.chargingLabel.text = [NSString stringWithFormat:@"Rp.%@",trxHistoryModel.charging];
    }
    else if ([trxHistoryModel.type isEqualToString:@"Package"]) {
        cell.typeLabel.text = [trxHistoryModel.typeLabel uppercaseString];
        cell.chargingLabel.text = [NSString stringWithFormat:@"Rp.%@",trxHistoryModel.charging];
    }
    else {
        cell.typeLabel.text = [trxHistoryModel.type uppercaseString];
        cell.chargingLabel.text = trxHistoryModel.charging;
    }
    
    // Separator View
    if (indexPath.row == self.content.count -1) {
        cell.lineView.hidden = YES;
    }
    else {
        cell.lineView.hidden = NO;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return _menuModel.groupName;
    }
    else {
        return _menuModel.menuName;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40.0;
}

#pragma mark - Table view delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight*1.5)
                                                withLabelForXL:sectionTitle
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                withLabelForXL:sectionTitle
                                                isFirstSection:NO] autorelease];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return kDefaultCellHeight*1.5;
    }
    else {
        return kDefaultCellHeight;
    }
}

@end
