//
//  TransactionHistoryViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/25/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "MenuModel.h"

@interface TransactionHistoryViewController : BasicViewController {
    NSArray *_content;
    MenuModel *_menuModel;
}

@property (nonatomic, retain) NSArray *content;
@property (nonatomic, retain) MenuModel *menuModel;

@end
