//
//  QuotaBarView.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 10/27/15.
//  Copyright © 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuotaBarView : UIView

@property (retain, nonatomic) IBOutlet UILabel *packageNameLabel;
@property (retain, nonatomic) IBOutlet UIView *bgDataView;
@property (retain, nonatomic) IBOutlet UIView *fgDataView;
@property (retain, nonatomic) IBOutlet UIView *bubbleView;
@property (retain, nonatomic) IBOutlet UIView *unlimitedBorderView;
@property (retain, nonatomic) IBOutlet UILabel *remainingLabel;
@property (retain, nonatomic) IBOutlet UILabel *totalLabel;

@end
