//
//  QuotaEnhanceViewController.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 10/26/15.
//  Copyright © 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "QuotaEnhanceViewController.h"
#import "PackageNameView.h"
#import "QuotaBarView.h"
#import "QuotaView.h"
#import "FlatButton.h"
#import "Language.h"
#import "AXISnetCommon.h"
#import "ThankYouViewController.h"
#import "SubMenuViewController.h"
#import "HeaderView.h"
#import "ASValuePopUpView.h"
#import "NotificationViewController.h"

@interface QuotaEnhanceViewController ()

//@property (nonatomic, retain) SectionHeaderView *headerView;
@property (nonatomic, retain) HeaderView *headerView;
@property (readwrite) NSInteger selectedPackage;

@end

@implementation QuotaEnhanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self setupAccordion];
    
    [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    
    [self createBadgeOnMessageIcon];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Selector

- (void)setupAccordion {
    
    /*
     * Clear View
     */
    for (UIView *oldViews in self.view.subviews)
    {
        [oldViews removeFromSuperview];
    }
    //***********
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    self.view.backgroundColor = kDefaultWhiteSmokeColor;
    
    CGFloat screenWidth = 0.0;
    if (IS_IPAD) {
        screenWidth = 1024.0;
    }
    else {
        screenWidth = 320.0;
    }
    
    // Instantiate the nib content without any reference to it.
    HeaderView *headerViewTmp = (HeaderView *)[[[NSBundle mainBundle] loadNibNamed:@"HeaderView" owner:nil options:nil] objectAtIndex:0];
    headerViewTmp.headerLabel.text = [Language get:@"my_package_detail" alter:nil];
    headerViewTmp.buyPackageButton.layer.cornerRadius = 5.0;
    headerViewTmp.buyPackageButton.clipsToBounds = YES;
    [headerViewTmp.buyPackageButton addTarget:self action:@selector(performPackageList) forControlEvents:UIControlEventTouchUpInside];
    [headerViewTmp.buyPackageButton setTitle:[NSString stringWithFormat:@"%@",[Language get:@"buy_package_detail" alter:nil].uppercaseString] forState:UIControlStateNormal];
    headerViewTmp.backgroundColor = [UIColor whiteColor];
    self.headerView = headerViewTmp;
    CGRect frameHeader = headerViewTmp.frame;
    frameHeader.origin.y = 30.0;
    frameHeader.size.width = screenWidth;
    self.headerView.frame = frameHeader;
    [self.view addSubview:self.headerView];
    
    accordion = [[AccordionView alloc] initWithFrame:CGRectMake(10.0, self.headerView.frame.origin.y + self.headerView.frame.size.height, screenWidth - 20.0, self.view.bounds.size.height)];
    accordion.showDetail = YES;
    accordion.delegate = self;
    [self.view addSubview:accordion];
    
    //-- looping to add how many package that we have
    for (int i = 0; i < [sharedData.quotaImprovementData.detail count]; i++) {
        
        NSLog(@"--> i = %i",i);
        
        QuotaDetailModel *detailModel = [sharedData.quotaImprovementData.detail objectAtIndex:i];
        
        //-------------------//
        // Package Name View //
        //-------------------//
        PackageNameView *packageNameView = (PackageNameView *)[[[NSBundle mainBundle] loadNibNamed:@"PackageNameView" owner:nil options:nil] objectAtIndex:0];
        packageNameView.packageNameLabel.text = [detailModel.name uppercaseString];
        packageNameView.packageNameView.layer.cornerRadius = 5.0;
        packageNameView.packageNameView.clipsToBounds = YES;
        
        //--------------//
        // Content View //
        //--------------//
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, screenWidth, 100.0)];
        
        // Instantiate the nib content without any reference to it.
        QuotaBarView *quotaBarView = (QuotaBarView *)[[[NSBundle mainBundle] loadNibNamed:@"QuotaBarView" owner:nil options:nil] objectAtIndex:0];
        CGRect frameQuotaBar = quotaBarView.frame;
        frameQuotaBar.size.width = screenWidth;
        quotaBarView.frame = frameQuotaBar;
        
        QuotaView *quotaView = (QuotaView *)[[[NSBundle mainBundle] loadNibNamed:@"QuotaView" owner:nil options:nil] objectAtIndex:0];
        CGRect frameQuotaView = quotaView.frame;
        frameQuotaView.size.width = screenWidth;
        quotaView.frame = frameQuotaView;
        
        float y = 0.0; // last position
        
        //--------------//
        // Benefit Data //
        //--------------//
        BenefitDataModel *dataModel = detailModel.benefitData;
        if (dataModel) {
            // data bar
            if ([dataModel.dataBar isKindOfClass:[NSArray class]]) {
                
                quotaBarView.hidden = NO;
                //quotaView.middleView.hidden = NO;
                quotaView.estimationView.hidden = NO;
                NSLog(@"Estimation View Muncul");
                
                for (DataBarModel *dataBarModel in dataModel.dataBar) {
                    
                    //NSLog(@"--> name = %@",dataBarModel.packageName);
                    
                    if (quotaBarView == nil) {
                        //NSLog(@"quotaBarView NIL");
                        quotaBarView = (QuotaBarView *)[[[NSBundle mainBundle] loadNibNamed:@"QuotaBarView" owner:nil options:nil] objectAtIndex:0];
                        CGRect frameQuotaBar = quotaBarView.frame;
                        frameQuotaBar.size.width = screenWidth;
                        quotaBarView.frame = frameQuotaBar;
                    }
                    
                    quotaBarView.packageNameLabel.text = dataBarModel.packageName;
                    quotaBarView.bgDataView.layer.cornerRadius = 11.0f;
                    quotaBarView.bgDataView.layer.masksToBounds = YES;
                    
                    // Foreground Gauge View
                    quotaBarView.fgDataView.layer.cornerRadius = 11.0f;
                    quotaBarView.fgDataView.layer.masksToBounds = YES;
                    CGRect fgViewRect = quotaBarView.fgDataView.frame;
                    
                    float barPercent = [dataBarModel.percentRemaining floatValue];
                    if (barPercent < 10.0) {
                        barPercent = 10;
                    }
                    fgViewRect.size.width = (barPercent/100) * quotaBarView.bgDataView.frame.size.width;
                    quotaBarView.fgDataView.frame = fgViewRect;
                    if ([dataBarModel.percentRemaining floatValue] <= 20) {
                        quotaBarView.fgDataView.backgroundColor = colorWithHexString(kDefaultRedQuotaColor);
                    }
                    else {
                        quotaBarView.fgDataView.backgroundColor = colorWithHexString(kDefaultGreenQuotaColor);
                    }
                    
                    CGRect quotaBarViewFrame = quotaBarView.frame;
                    quotaBarViewFrame.origin.y = y;
                    quotaBarView.frame = quotaBarViewFrame;
                    
                    // TODO: Unlimited
                    // Unlimited or not
                    
                    quotaBarView.unlimitedBorderView.layer.cornerRadius = quotaBarView.unlimitedBorderView.bounds.size.width/2;
                    quotaBarView.unlimitedBorderView.layer.masksToBounds = YES;
                    
                    CGRect unlimitedViewRect = quotaBarView.unlimitedBorderView.frame;
                    CGFloat position = (0.8*quotaBarView.bgDataView.frame.size.width) - (quotaBarView.unlimitedBorderView.frame.size.width/2);
                    unlimitedViewRect.origin.x = position;
                    //NSLog(@"position X = %f",position);
                    //NSLog(@"position Y = %f",unlimitedViewRect.origin.y);
                    quotaBarView.unlimitedBorderView.frame = unlimitedViewRect;
                    
                    // remove view from cell.bubbleView
                    for (ASValuePopUpView *oldViews in quotaBarView.bubbleView.subviews)
                    {
                        [oldViews removeFromSuperview];
                    }
                    
                    //bubble view
                    ASValuePopUpView *bubble = [[ASValuePopUpView alloc] initWithFrame:CGRectMake(0.0, 15.0, quotaBarView.bubbleView.frame.size.width, quotaBarView.bubbleView.frame.size.height)];
                    bubble.color = colorWithHexString(kDefaultRedQuotaColor);
                    bubble.cornerRadius = 4.0;
                    [bubble setTextColor:kDefaultWhiteColor];
                    [bubble setFont:[UIFont fontWithName:kDefaultFontBold size:6.0]];
                    [bubble setString:[NSString stringWithFormat:@"FUP %@",dataBarModel.total]];
                    [quotaBarView.bubbleView addSubview:bubble];
                    
                    CGRect bubbleViewRect = quotaBarView.bubbleView.frame;
                    CGFloat positionBubble = (0.8*quotaBarView.bgDataView.frame.size.width) - (quotaBarView.bubbleView.frame.size.width/2);
                    bubbleViewRect.origin.x = positionBubble;
                    quotaBarView.bubbleView.frame = bubbleViewRect;
                    NSLog(@"---> UNLIMITED = %@",dataModel.isUnlimited);
                    if ([dataModel.isUnlimited isEqualToString:@"1"]) {
                        quotaBarView.unlimitedBorderView.hidden = NO;
                        quotaBarView.bubbleView.hidden = NO;
                        //cell.lblPackageActive.hidden = YES;
                        //cell.lblTotalData.hidden = YES;
                        //cell.lblTotalPackage.text = [Language get:@"your_usage_data_plan" alter:nil];
                        //cell.lblRemainingData.text = [NSString stringWithFormat:@"%@ %@",[Language get:@"usage_data_plan" alter:nil],sharedData.quotaImprovementData.usage];
                        
                        // Change Foreground Logic for Unlimited
                        CGFloat unlimitedRange = quotaBarView.unlimitedBorderView.frame.origin.x + (quotaBarView.unlimitedBorderView.frame.size.width/2);
                        NSLog(@"iNot Dalem :%f",quotaBarView.unlimitedBorderView.frame.size.width);
                        NSLog(@"iNot Dalem BG VIEW:%f",quotaBarView.bgDataView.frame.size.width);
                        //NSLog(@"unlimitedRange = %f",unlimitedRange);
                        //NSLog(@"quotaBarView.fgDataView.frame.size.width = %f",quotaBarView.fgDataView.frame.size.width);
                        //NSLog(@"bgDataView = %f",quotaBarView.bgDataView.frame.size.width);
                        
                        CGRect fgViewRect = quotaBarView.fgDataView.frame;
                        
                        if (dataBarModel.percentUsage >= 100) {
                            fgViewRect.size.width = unlimitedRange + ((5/100)*quotaBarView.bgDataView.frame.size.width);
                        }
                        else {
                            fgViewRect.size.width = (dataBarModel.percentUsage/100) * unlimitedRange;
                        }
                        
                        quotaBarView.fgDataView.frame = fgViewRect;
                        
                        
                        quotaBarView.totalLabel.text = [NSString stringWithFormat:@"/FUP %@",dataBarModel.total];
                        [quotaBarView.totalLabel sizeToFit];
                        CGRect labelRect = quotaBarView.totalLabel.frame;
                        labelRect.origin.y = 48.0;
                        labelRect.size.height = 20.0;
                        labelRect.origin.x = 312.0 - quotaBarView.totalLabel.frame.size.width;
                        quotaBarView.totalLabel.frame = labelRect;
                        //quotaBarView.totalLabel.backgroundColor = [UIColor greenColor];
                        
                        quotaBarView.remainingLabel.text = [NSString stringWithFormat:@"%@ %@",[Language get:@"usage_data_plan" alter:nil],dataBarModel.usage];
                        //quotaBarView.remainingLabel.backgroundColor = [UIColor yellowColor];
                        CGRect labelRemainingRect = quotaBarView.remainingLabel.frame;
                        labelRemainingRect.origin.x = quotaBarView.totalLabel.frame.origin.x - quotaBarView.remainingLabel.frame.size.width;
                        quotaBarView.remainingLabel.frame = labelRemainingRect;
                    }
                    else {
                        quotaBarView.unlimitedBorderView.hidden = YES;
                        quotaBarView.bubbleView.hidden = YES;
                        //cell.lblPackageActive.hidden = NO;
                        //cell.lblTotalData.hidden = NO;
                        
                        quotaBarView.totalLabel.text = [NSString stringWithFormat:@"/%@ %@",[Language get:@"total" alter:nil],dataBarModel.total];
                        [quotaBarView.totalLabel sizeToFit];
                        CGRect labelRect = quotaBarView.totalLabel.frame;
                        labelRect.origin.y = 48.0;
                        labelRect.size.height = 20.0;
                        labelRect.origin.x = 312.0 - quotaBarView.totalLabel.frame.size.width;
                        quotaBarView.totalLabel.frame = labelRect;
                        //quotaBarView.totalLabel.backgroundColor = [UIColor greenColor];
                        
                        quotaBarView.remainingLabel.text = [NSString stringWithFormat:@"%@ %@",[Language get:@"remaining_data_plan" alter:nil],dataBarModel.remaining];
                        //quotaBarView.remainingLabel.backgroundColor = [UIColor yellowColor];
                        CGRect labelRemainingRect = quotaBarView.remainingLabel.frame;
                        labelRemainingRect.origin.x = quotaBarView.totalLabel.frame.origin.x - quotaBarView.remainingLabel.frame.size.width;
                        quotaBarView.remainingLabel.frame = labelRemainingRect;
                        
                        if ([dataBarModel.percentRemaining floatValue] <= 20) {
                            quotaBarView.remainingLabel.textColor = [UIColor redColor];
                        }
                        
                    }
                    //--- end of unlimited ---//
                    
                    // Add Quota Bar View to Content View //
                    [contentView addSubview:quotaBarView];
                    //------------------------------------//
                    y = quotaBarViewFrame.origin.y + quotaBarViewFrame.size.height;
                    
                    quotaBarView = nil;
                }
            }
            else {
                quotaBarView.hidden = YES;
                //quotaView.middleView.hidden = YES;
                quotaView.estimationView.hidden = YES;
                NSLog(@"Estimtion View Hide");
            }
            //
            
            // other properties
            CGRect fgDaysLeftViewRect = quotaView.daysLeftFgView.frame;
            fgDaysLeftViewRect.size.width = ([dataModel.percentSisaHari floatValue]/100.0) * quotaView.daysLeftBgView.frame.size.width;
            quotaView.daysLeftFgView.frame = fgDaysLeftViewRect;
            
            quotaView.circleCenterLabel.text = [NSString stringWithFormat:@"%@ %@",dataModel.sisaHari,[Language get:@"day" alter:nil]];
            CGRect centerDaysLeftViewRect = quotaView.circleCenterView.frame;
            centerDaysLeftViewRect.origin.x = quotaView.daysLeftFgView.frame.origin.x + quotaView.daysLeftFgView.frame.size.width - (quotaView.circleCenterView.frame.size.width/2) - quotaView.daysLeftFgView.frame.origin.x;
            
            NSString *zero = @"0";
            NSString *one = @"1";
            if ([dataModel.sisaHari isEqualToString:zero] || [dataModel.sisaHari isEqualToString:one]) {
                NSLog(@"Zero or One");
                //centerDaysLeftViewRect.origin.x = quotaView.circleLeftView.frame.origin.x;
                centerDaysLeftViewRect.origin.x = quotaView.daysLeftView.frame.origin.x - quotaView.daysLeftFgView.frame.origin.x - 2;
                NSLog(@"Circle Left View = %f",centerDaysLeftViewRect.origin.x);
            }
            quotaView.circleCenterView.frame = centerDaysLeftViewRect;
            
            
            quotaView.circleLeftView.layer.cornerRadius = quotaView.circleLeftView.bounds.size.width/2;
            quotaView.circleLeftView.layer.masksToBounds = YES;
            
            quotaView.circleRightView.layer.cornerRadius = quotaView.circleRightView.bounds.size.width/2;
            quotaView.circleRightView.layer.masksToBounds = YES;
            
            quotaView.circleCenterView.layer.cornerRadius = quotaView.circleCenterView.bounds.size.width/2;
            quotaView.circleCenterView.layer.masksToBounds = YES;
            
            
            
            //
            
            quotaView.startDateLabel.text = changeDateFormatForPackage(dataModel.regPackage);
            quotaView.endDateLabel.text = changeDateFormatForPackage(dataModel.activeUntil);
            quotaView.averageDailyUsageLabel.text = dataModel.rataRataHarian;
            quotaView.runoutEstimationLabel.text = changeDateFormatForPackage(dataModel.estimasiHabis);
            
        }
        else {
            quotaView.startDateLabel.text = @"-";
            quotaView.endDateLabel.text = @"-";
            quotaView.averageDailyUsageLabel.text = @"-";
            quotaView.runoutEstimationLabel.text = @"-";
            
        }
        
        // TODO: CR hide estimation view
        if ([detailModel.showRataHarian isEqualToString:@"1"] || [detailModel.showEstimasiHabis isEqualToString:@"1"]) {
            quotaView.estimationView.hidden = NO;
        }
        else {
            quotaView.estimationView.hidden = YES;
        }
        
        // Set label value
        quotaView.lblTitleStartDate.text = [Language get:@"start_date" alter:nil];
        quotaView.lblTitleEndDate.text = [Language get:@"end_date" alter:nil];
        quotaView.lblTitleAverageDailyUsage.text = [Language get:@"average_daily_data_usage" alter:nil];
        quotaView.lblTitleRunoutEstimation.text = [Language get:@"estimated_time_of_completion" alter:nil];
        quotaView.lblTitleVoice.text = [Language get:@"remaining_talk_time" alter:nil];
        quotaView.lblTitleSms.text = [Language get:@"remaining_sms" alter:nil];
        
        quotaView.orLabel.text = [Language get:@"or" alter:nil].uppercaseString;
        
        if ([dataModel.isUnlimited isEqualToString:@"1"]) {
            [quotaView.buyButton setTitle:[NSString stringWithFormat:@"%@",[Language get:@"add_fup" alter:nil].uppercaseString] forState:UIControlStateNormal];
        }
        else {
            [quotaView.buyButton setTitle:[NSString stringWithFormat:@"%@",[Language get:@"add_quota" alter:nil].uppercaseString] forState:UIControlStateNormal];
        }
        
        [quotaView.stopButton setTitle:[NSString stringWithFormat:@"%@",[Language get:@"stop_this_plan" alter:nil].uppercaseString] forState:UIControlStateNormal];
        
        quotaView.buyButton.layer.cornerRadius = 5.0;
        quotaView.buyButton.clipsToBounds = YES;
        quotaView.stopButton.layer.cornerRadius = 5.0;
        quotaView.stopButton.clipsToBounds = YES;
        //
        
        BenefitVoiceSmsModel *voiceModel = [detailModel.benefitVoice objectAtIndex:0];
        BenefitVoiceSmsModel *smsModel = [detailModel.benefitSms objectAtIndex:0];
        
        if (!!voiceModel.sisa && ![voiceModel.sisa isEqual:[NSNull null]]) {
            quotaView.voiceRemainingLabel.text = [NSString stringWithFormat:@"%@: %@",voiceModel.benefitName,voiceModel.sisa];
            quotaView.voiceTotalLabel.text = [NSString stringWithFormat:@"(Total %@)",voiceModel.total];
            if ([voiceModel.total isEqualToString:kNA] == YES) {
                quotaView.voiceTotalLabel.hidden = YES;
            }
            quotaView.voiceRemainingView.hidden = NO;
            quotaView.voiceRemainingView.layer.borderColor = [UIColor blackColor].CGColor;
            quotaView.voiceRemainingView.layer.borderWidth = 1.0f;
        }
        else {
            quotaView.voiceRemainingLabel.text = @"-";
            quotaView.voiceTotalLabel.text = [NSString stringWithFormat:@"(Total -)"];
            quotaView.voiceRemainingView.hidden = YES;
        }
        
        if (!!smsModel.sisa && ![smsModel.sisa isEqual:[NSNull null]]) {
            quotaView.smsRemainingLabel.text = [NSString stringWithFormat:@"%@: %@",smsModel.benefitName,smsModel.sisa];
            quotaView.smsTotalLabel.text = [NSString stringWithFormat:@"(Total %@)",smsModel.total];
            if ([smsModel.total isEqualToString:kNA] == YES) {
                quotaView.smsTotalLabel.hidden = YES;
            }
            quotaView.smsRemainingView.hidden = NO;
            quotaView.smsRemainingView.layer.borderColor = [UIColor blackColor].CGColor;
            quotaView.smsRemainingView.layer.borderWidth = 1.0f;
        }
        else {
            quotaView.smsRemainingLabel.text = @"-";
            quotaView.smsTotalLabel.text = [NSString stringWithFormat:@"(Total -)"];
            quotaView.smsRemainingView.hidden = YES;
        }
        
        // Additional CR Voice & SMS 2 : voice sms selain XL
        
        
        // Voice
        if ([detailModel.benefitVoice count] > 1) {
            BenefitVoiceSmsModel *voiceModel2 = [detailModel.benefitVoice objectAtIndex:1];
            quotaView.voiceRemainingLabel2.text = [NSString stringWithFormat:@"%@: %@",voiceModel2.benefitName,voiceModel2.sisa];
            quotaView.voiceTotalLabel2.text = [NSString stringWithFormat:@"(Total %@)",voiceModel2.total];
            if ([voiceModel2.total isEqualToString:kNA] == YES) {
                quotaView.voiceTotalLabel2.hidden = YES;
            }
            
            quotaView.voiceRemainingLabel2.hidden = NO;
        }
        else {
            quotaView.voiceRemainingLabel2.hidden = YES;
            quotaView.voiceTotalLabel2.hidden = YES;
        }
        
        //sms
        if ([detailModel.benefitSms count] > 1) {
            BenefitVoiceSmsModel *smsModel2 = [detailModel.benefitSms objectAtIndex:1];
            quotaView.smsRemainingLabel2.text = [NSString stringWithFormat:@"%@: %@",smsModel2.benefitName,smsModel2.sisa];
            quotaView.smsTotalLabel2.text = [NSString stringWithFormat:@"(Total %@)",smsModel2.total];
            if ([smsModel2.total isEqualToString:kNA] == YES) {
                quotaView.smsTotalLabel2.hidden = YES;
            }
            quotaView.smsRemainingLabel2.hidden = NO;
        }
        else {
            quotaView.smsRemainingLabel2.hidden = YES;
            quotaView.smsTotalLabel2.hidden = YES;
        }
        //--- end of Additional CR Voice & SMS 2
        
        quotaView.voiceRemainingView.layer.cornerRadius = 5.0;
        quotaView.voiceRemainingView.clipsToBounds = YES;
        quotaView.smsRemainingView.layer.cornerRadius = 5.0;
        quotaView.smsRemainingView.clipsToBounds = YES;
        
        // adjust Voice & SMS View position if no benefit data (only voice and sms)
        if (quotaView.estimationView.hidden == YES) {
            // voice hide, sms show
            if (quotaView.voiceRemainingView.hidden == YES && quotaView.smsRemainingView.hidden == NO) {
                
                quotaView.middleView.hidden = NO;
                
                CGRect smsViewFrame = quotaView.smsRemainingView.frame;
                smsViewFrame.origin.y = quotaView.middleView.frame.origin.y + quotaView.middleView.frame.size.height + kDefaultComponentPadding;
                quotaView.smsRemainingView.frame = smsViewFrame;
                
                CGRect buttonViewFrame = quotaView.buttonView.frame;
                buttonViewFrame.origin.y = quotaView.smsRemainingView.frame.origin.y + quotaView.smsRemainingView.frame.size.height + kDefaultComponentPadding;
                quotaView.buttonView.frame = buttonViewFrame;
            }
            // voice show, sms hide
            else if (quotaView.voiceRemainingView.hidden == NO && quotaView.smsRemainingView.hidden == YES) {
                
                quotaView.middleView.hidden = NO;
                
                CGRect voiceViewFrame = quotaView.voiceRemainingView.frame;
                voiceViewFrame.origin.y = quotaView.middleView.frame.origin.y + quotaView.middleView.frame.size.height + kDefaultComponentPadding;
                quotaView.voiceRemainingView.frame = voiceViewFrame;
                
                CGRect buttonViewFrame = quotaView.buttonView.frame;
                buttonViewFrame.origin.y = quotaView.voiceRemainingView.frame.origin.y + quotaView.voiceRemainingView.frame.size.height + kDefaultComponentPadding;
                quotaView.buttonView.frame = buttonViewFrame;
            }
            // voice hide, sms hide, or empty voice sms
            else if (quotaView.voiceRemainingView.hidden == YES && quotaView.smsRemainingView.hidden == YES) {
                
                quotaView.middleView.hidden = NO;
                
                CGRect buttonViewFrame = quotaView.buttonView.frame;
                buttonViewFrame.origin.y = quotaView.middleView.frame.origin.y + quotaView.middleView.frame.size.height + kDefaultComponentPadding;
                quotaView.buttonView.frame = buttonViewFrame;
            }
            // voice show, sms show
            else {
                quotaView.middleView.hidden = NO;
                
                CGRect voiceViewFrame = quotaView.voiceRemainingView.frame;
                voiceViewFrame.origin.y = quotaView.middleView.frame.origin.y + quotaView.middleView.frame.size.height + kDefaultComponentPadding;
                quotaView.voiceRemainingView.frame = voiceViewFrame;
                
                CGRect smsViewFrame = quotaView.smsRemainingView.frame;
                smsViewFrame.origin.y = quotaView.voiceRemainingView.frame.origin.y + quotaView.voiceRemainingView.frame.size.height + kDefaultComponentPadding;
                quotaView.smsRemainingView.frame = smsViewFrame;
                
                CGRect buttonViewFrame = quotaView.buttonView.frame;
                buttonViewFrame.origin.y = quotaView.smsRemainingView.frame.origin.y + quotaView.smsRemainingView.frame.size.height + kDefaultComponentPadding;
                quotaView.buttonView.frame = buttonViewFrame;
            }
        }
        else {
            // voice hide, sms show
            if (quotaView.voiceRemainingView.hidden == YES && quotaView.smsRemainingView.hidden == NO) {
                
                CGRect smsViewFrame = quotaView.smsRemainingView.frame;
                smsViewFrame.origin.y = quotaView.estimationView.frame.origin.y + quotaView.estimationView.frame.size.height + kDefaultComponentPadding;
                quotaView.smsRemainingView.frame = smsViewFrame;
                
                CGRect buttonViewFrame = quotaView.buttonView.frame;
                buttonViewFrame.origin.y = quotaView.smsRemainingView.frame.origin.y + quotaView.smsRemainingView.frame.size.height + kDefaultComponentPadding;
                quotaView.buttonView.frame = buttonViewFrame;
                
            }
            // voice show, sms hide
            else if (quotaView.voiceRemainingView.hidden == NO && quotaView.smsRemainingView.hidden == YES) {
                CGRect voiceViewFrame = quotaView.voiceRemainingView.frame;
                voiceViewFrame.origin.y = quotaView.estimationView.frame.origin.y + quotaView.estimationView.frame.size.height + kDefaultComponentPadding;
                quotaView.voiceRemainingView.frame = voiceViewFrame;
                
                CGRect buttonViewFrame = quotaView.buttonView.frame;
                buttonViewFrame.origin.y = quotaView.voiceRemainingView.frame.origin.y + quotaView.voiceRemainingView.frame.size.height + kDefaultComponentPadding;
                quotaView.buttonView.frame = buttonViewFrame;
            }
            // voice hide, sms hide
            else if (quotaView.voiceRemainingView.hidden == YES && quotaView.smsRemainingView.hidden == YES) {
                CGRect buttonViewFrame = quotaView.buttonView.frame;
                buttonViewFrame.origin.y = quotaView.estimationView.frame.origin.y + quotaView.estimationView.frame.size.height + kDefaultComponentPadding;
                quotaView.buttonView.frame = buttonViewFrame;
            }
            // voice show, sms show
            else {
                CGRect voiceViewFrame = quotaView.voiceRemainingView.frame;
                voiceViewFrame.origin.y = quotaView.estimationView.frame.origin.y + quotaView.estimationView.frame.size.height + kDefaultComponentPadding;
                quotaView.voiceRemainingView.frame = voiceViewFrame;
                
                CGRect smsViewFrame = quotaView.smsRemainingView.frame;
                smsViewFrame.origin.y = quotaView.voiceRemainingView.frame.origin.y + quotaView.voiceRemainingView.frame.size.height + kDefaultComponentPadding;
                quotaView.smsRemainingView.frame = smsViewFrame;
                
                CGRect buttonViewFrame = quotaView.buttonView.frame;
                buttonViewFrame.origin.y = quotaView.smsRemainingView.frame.origin.y + quotaView.smsRemainingView.frame.size.height + kDefaultComponentPadding;
                quotaView.buttonView.frame = buttonViewFrame;
            }
        }
        // end of adjust position
        
        // adjust Button View position if no benefit voice or sms
        /*
        if (quotaView.voiceRemainingView.hidden == YES || quotaView.smsRemainingView.hidden == YES) {
            CGRect buttonViewFrame = quotaView.buttonView.frame;
            buttonViewFrame.origin.y = quotaView.lastLineView.frame.origin.y + quotaView.lastLineView.frame.size.height + kDefaultComponentPadding;
            quotaView.buttonView.frame = buttonViewFrame;
        }*/
        
        CGRect quotaViewFrame = quotaView.frame;
        quotaViewFrame.origin.y = y;
        quotaViewFrame.size.height = quotaView.buttonView.frame.origin.y + quotaView.buttonView.frame.size.height + kDefaultPadding;
        quotaView.frame = quotaViewFrame;
        // end of adjustment
        
        // add action for button
        [quotaView.buyButton addTarget:self
                                action:@selector(clickBuyPackage:)
                      forControlEvents:UIControlEventTouchUpInside];
        quotaView.buyButton.tag = i;
        
        [quotaView.stopButton addTarget:self
                                 action:@selector(clickStopPackage:)
                       forControlEvents:UIControlEventTouchUpInside];
        quotaView.stopButton.tag = i;
        //
        
        // Add Quota Bar View to Content View //
        [contentView addSubview:quotaView];
        y = quotaView.frame.origin.y + quotaView.frame.size.height;
        //------------------------------------//
        
        // adjust content view height
        CGRect contentViewFrame = contentView.frame;
        contentViewFrame.size.height = y + kDefaultPadding;
        contentView.frame = contentViewFrame;
        
        // Controller's outlet has been bound during nib loading, so we can access view trough the outlet.
        [accordion addHeader:packageNameView withView:contentView];
    }
    //--
    
    [accordion setNeedsLayout];
    
    // Set this if you want to allow multiple selection
    [accordion setAllowsMultipleSelection:NO];
    
    
    // Layout UIView
    //[self.view bringSubviewToFront:self.headerView];
    //[self.view setUserInteractionEnabled:YES];
    
    /*
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [addButton setTitle:@"add button" forState:UIControlStateNormal];
    addButton.frame = CGRectMake(250, 50, 160, 50);
    [addButton setUserInteractionEnabled:YES];
    [addButton setEnabled:YES];
    [addButton setAlpha:1];
    [addButton addTarget:self action:@selector(performPackageList) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addButton];*/
}

- (void)clickBuyPackage:(id)sender {
    UIButton *button = (UIButton *)sender;
    _selectedPackage = button.tag;
    DataManager *sharedData = [DataManager sharedInstance];
    QuotaDetailModel *detailModel = [sharedData.quotaImprovementData.detail objectAtIndex:_selectedPackage];
    
    if ([detailModel.benefitData.isActiveInMyXL isEqualToString:@"1"]) {
        // Direct Buy
        NSString *confirmationMessage = detailModel.confirmDesc;
        
        if([detailModel.benefitData.paymentMethod isEqualToString:@"cc"])
        {
            // TODO : V1.9
            // Add pay with credit card on buy package alert
            UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                      message:confirmationMessage
                                                                     delegate:self
                                                            cancelButtonTitle:[[Language get:@"pay_with_cc" alter:nil] uppercaseString]
                                                            otherButtonTitles:[[Language get:@"yes" alter:nil] uppercaseString],[[Language get:@"no" alter:nil] uppercaseString],nil];
            buyPackageAlert.tag = 4;
            
            [buyPackageAlert show];
            [buyPackageAlert release];
        }
        else
        {
            UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                      message:confirmationMessage
                                                                     delegate:self
                                                            cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                            otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
            
            buyPackageAlert.tag = 2;
            
            [buyPackageAlert show];
            [buyPackageAlert release];
        }
    }
    else {
        // Package List
        [self performPackageList];
    }
}

- (void)clickStopPackage:(id)sender {
    UIButton *button = (UIButton *)sender;
    _selectedPackage = button.tag;
    DataManager *sharedData = [DataManager sharedInstance];
    QuotaDetailModel *detailModel = [sharedData.quotaImprovementData.detail objectAtIndex:_selectedPackage];
    
    NSString *message = [NSString stringWithFormat:@"%@ %@ %@ %@\n%@",
                         [Language get:@"stop_package_confirmation_1" alter:nil],
                         detailModel.name,
                         [Language get:@"stop_package_confirmation_2" alter:nil],
                         [Language get:@"stop_package_confirmation_3" alter:nil],
                         [Language get:@"ask_continue" alter:nil]];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                          otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
    alert.tag = 1;
    [alert show];
    [alert release];
}

- (void)performBuyPackage:(NSString*)packageId withAmount:(NSString*)amount {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = BUY_PACKAGE_REQ;
    NSString *trxId = generateUniqueString();
    [request buyPackage:packageId withAmount:amount withTrxId:trxId];
}

- (void)performStopPackage {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    DataManager *sharedData = [DataManager sharedInstance];
    QuotaDetailModel *detailModel = [sharedData.quotaImprovementData.detail objectAtIndex:_selectedPackage];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = STOP_PACKAGE_REQ;
    [request stopPackage:@"" withUnreg:detailModel.serviceId];
}

- (void)performPackageList {
    SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
    
    DataManager *dataManager = [DataManager sharedInstance];
    subMenuVC.parentMenuModel = dataManager.packageMenu;
    subMenuVC.isLevel2 = NO;
    subMenuVC.levelTwoType = COMMON;
    subMenuVC.grayHeaderTitle = dataManager.packageMenu.menuName;
    
    [self.navigationController pushViewController:subMenuVC animated:YES];
    subMenuVC = nil;
    [subMenuVC release];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    DataManager *sharedData = [DataManager sharedInstance];
    QuotaDetailModel *detailModel = [sharedData.quotaImprovementData.detail objectAtIndex:_selectedPackage];
    
    // tag == 1 Stop Package
    if (alertView.tag == 1)
    {
        if (buttonIndex == 0) {
            /*
             * Google Analytic
             */
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            // Screen tracking
            NSString *gaLabel = [NSString stringWithFormat:@"%@ -- yes",detailModel.serviceId];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                                  action:@"Stop"        // Event action (required)
                                                                   label:gaLabel        // Event label
                                                                   value:nil] build]];
            [[GAI sharedInstance] dispatch];
            /******************/
            
            [self performStopPackage];
        }
        else {
            /*
             * Google Analytic
             */
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            // Screen tracking
            NSString *gaLabel = [NSString stringWithFormat:@"%@ -- no",detailModel.serviceId];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                                  action:@"Stop"        // Event action (required)
                                                                   label:gaLabel        // Event label
                                                                   value:nil] build]];
            [[GAI sharedInstance] dispatch];
            /******************/
        }
    }
    
    // tag == 2 Buy Package
    if (alertView.tag == 2)
    {
        if (buttonIndex == 0) {
            /*
             * Google Analytic
             */
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            // Screen tracking
            NSString *gaLabel = [NSString stringWithFormat:@"%@ -- yes",detailModel.serviceId];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                                  action:@"Buy"         // Event action (required)
                                                                   label:gaLabel        // Event label
                                                                   value:nil] build]];
            [[GAI sharedInstance] dispatch];
            /******************/
            
            // TODO : V1.9
            // Loading time for buy package
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.dateToday = [[NSDate alloc] init];
            //NSLog(@"Buy Package Time %@", appDelegate.dateToday);
            [self performBuyPackage:detailModel.serviceId withAmount:detailModel.price];
        }
        else {
            /*
             * Google Analytic
             */
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            // Screen tracking
            NSString *gaLabel = [NSString stringWithFormat:@"%@ -- no",detailModel.serviceId];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                                  action:@"Buy"         // Event action (required)
                                                                   label:gaLabel        // Event label
                                                                   value:nil] build]];
            [[GAI sharedInstance] dispatch];
            /******************/
        }
    }
    
    // Pay With Credit Card
    if (alertView.tag == 4)
    {
        // pay with credit card
        if(buttonIndex == 0)
        {
            /*
             * Google Analytic
             */
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            // Screen tracking
            NSString *gaLabel = [NSString stringWithFormat:@"%@ -- cc",detailModel.serviceId];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                                  action:@"Buy"         // Event action (required)
                                                                   label:gaLabel        // Event label
                                                                   value:nil] build]];
            [[GAI sharedInstance] dispatch];
            /******************/
            
            NSURL *url = nil;
            DataManager *sharedData = [DataManager sharedInstance];
            
            NSString *saltKeyAPI = SALT_KEY;
            NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
            //            NSString *paymentId = @"1"; //hardcoded
            NSString *lang = sharedData.profileData.language;
            
            NSString *packageID = detailModel.serviceId;
            NSString *price = detailModel.price;
            
            NSString *baseUrlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API,BUY_PKG_M_KARTU];
            NSString *paramString = [NSString stringWithFormat:@"msisdn=%@&msisdnInitiator=%@&currency=IDR&productType=1&productId=%@&lang=%@&amount=%@&channelID=&desc=&returnUrl=&extraParam=&remarks=&signature=&password=&checkoutType=",msisdn,msisdn,packageID,lang,price];
            NSLog(@"paramString = %@",paramString);
            paramString = [EncryptDecrypt doCipherForiOS7:paramString action:kCCEncrypt withKey:saltKeyAPI];
            
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlString,paramString]];
            NSLog(@"URL M-Kartu = %@",url);
            
            SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
            webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
            [self presentViewController:webViewController animated:YES completion:NULL];
        }
        else if (buttonIndex == 1)
        {
            /*
             * Google Analytic
             */
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            // Screen tracking
            NSString *gaLabel = [NSString stringWithFormat:@"%@ -- yes",detailModel.serviceId];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                                  action:@"Buy"         // Event action (required)
                                                                   label:gaLabel        // Event label
                                                                   value:nil] build]];
            [[GAI sharedInstance] dispatch];
            /******************************/
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.dateToday = [[NSDate alloc] init];
            NSLog(@"Buy Package Time %@", appDelegate.dateToday);
            [self performBuyPackage:detailModel.serviceId withAmount:detailModel.price];
        }
        NSLog(@"button index %ld", (long)buttonIndex);
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == BUY_PACKAGE_REQ) {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            NSString *reason = [result valueForKey:REASON_KEY];
            thankYouViewController.info = reason;
            
            MenuModel *menu = [[MenuModel alloc] init];
            menu.groupName = [Language get:@"buy_package" alter:nil];
            thankYouViewController.menuModel = menu;
            
            thankYouViewController.displayFBButton = NO;
            thankYouViewController.hideInboxBtn = NO;
            
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            thankYouViewController = nil;
            [thankYouViewController release];
        }
        
        if (type == STOP_PACKAGE_REQ) {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            
            NSString *reason = [result valueForKey:REASON_KEY];
            thankYouViewController.info = reason;
            
            MenuModel *menu = [[MenuModel alloc] init];
            menu.groupName = [Language get:@"stop_package" alter:nil];
            thankYouViewController.menuModel = menu;
            
            //thankYouViewController.headerTitleMaster = self.headerTitleMaster;
            //thankYouViewController.headerIconMaster = self.headerIconMaster;
            
            thankYouViewController.displayFBButton = NO;
            thankYouViewController.hideInboxBtn = NO;
            
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            
            [menu release];
            
            thankYouViewController = nil;
            [thankYouViewController release];
            
        }
        
        if (type == NOTIFICATION_REQ) {
            
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            [notificationViewController createBarButtonItem:BACK];
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == BUY_PACKAGE_REQ) {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            thankYouViewController.info = [Language get:@"buy_package_thank_you" alter:nil];
            
            MenuModel *menu = [[MenuModel alloc] init];
            menu.groupName = [Language get:@"buy_package" alter:nil];
            thankYouViewController.menuModel = menu;
            
            thankYouViewController.displayFBButton = NO;
            thankYouViewController.hideInboxBtn = NO;
            
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            thankYouViewController = nil;
            [thankYouViewController release];
        }
        else {
            NSString *reason = [result valueForKey:REASON_KEY];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                            message:reason
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
}

#pragma mark - ACCORDION DELEGATE

- (void)accordion:(AccordionView *)accordionX didChangeSelection:(NSIndexSet *)selection {
    NSLog(@"index = %li",(long)accordionX.selectedIndex);
    
    for (int i=0; i<[accordionX.headers count]; i++) {
        if (i == accordionX.selectedIndex) {
            PackageNameView *view = [[accordionX headers] objectAtIndex:i];
            view.arrowLabel.text = @"h";
        }
        else {
            PackageNameView *view = [[accordionX headers] objectAtIndex:i];
            view.arrowLabel.text = @"j";
        }
    }
    
    
}

@end
