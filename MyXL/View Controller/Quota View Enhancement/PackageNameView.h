//
//  PackageNameView.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 10/28/15.
//  Copyright © 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlatButton.h"

@interface PackageNameView : UIView

@property (retain, nonatomic) IBOutlet UIView *packageNameView;
@property (retain, nonatomic) IBOutlet UILabel *packageNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *arrowLabel;

@end
