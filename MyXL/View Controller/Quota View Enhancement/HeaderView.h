//
//  HeaderView.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 11/13/15.
//  Copyright © 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderView : UIView

@property (retain, nonatomic) IBOutlet UILabel *headerLabel;
@property (retain, nonatomic) IBOutlet UIButton *buyPackageButton;

@end
