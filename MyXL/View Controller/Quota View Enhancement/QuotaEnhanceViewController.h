//
//  QuotaEnhanceViewController.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 10/26/15.
//  Copyright © 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BasicViewController.h"
#import "AccordionView.h"

@interface QuotaEnhanceViewController : BasicViewController <UIAlertViewDelegate, AccordionViewDelegate> {
    AccordionView *accordion;
}

@end
