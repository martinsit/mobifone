//
//  QuotaView.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 10/26/15.
//  Copyright © 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuotaView : UIView

@property (retain, nonatomic) IBOutlet UIView *daysLeftView;
@property (retain, nonatomic) IBOutlet UIView *daysLeftBgView;
@property (retain, nonatomic) IBOutlet UIView *daysLeftFgView;
@property (retain, nonatomic) IBOutlet UIView *circleLeftView;
@property (retain, nonatomic) IBOutlet UIView *circleRightView;
@property (retain, nonatomic) IBOutlet UIView *circleCenterView;
@property (retain, nonatomic) IBOutlet UILabel *circleCenterLabel;

@property (retain, nonatomic) IBOutlet UIView *middleView;

@property (retain, nonatomic) IBOutlet UIView *estimationView;

@property (retain, nonatomic) IBOutlet UILabel *lblTitleStartDate;
@property (retain, nonatomic) IBOutlet UILabel *lblTitleEndDate;

@property (retain, nonatomic) IBOutlet UILabel *startDateLabel;
@property (retain, nonatomic) IBOutlet UILabel *endDateLabel;

@property (retain, nonatomic) IBOutlet UILabel *lblTitleAverageDailyUsage;
@property (retain, nonatomic) IBOutlet UILabel *lblTitleRunoutEstimation;
@property (retain, nonatomic) IBOutlet UILabel *averageDailyUsageLabel;
@property (retain, nonatomic) IBOutlet UILabel *runoutEstimationLabel;

@property (retain, nonatomic) IBOutlet UIView *voiceRemainingView;
@property (retain, nonatomic) IBOutlet UILabel *voiceRemainingLabel;
@property (retain, nonatomic) IBOutlet UILabel *voiceTotalLabel;
@property (retain, nonatomic) IBOutlet UILabel *voiceRemainingLabel2;
@property (retain, nonatomic) IBOutlet UILabel *voiceTotalLabel2;
@property (retain, nonatomic) IBOutlet UILabel *lblTitleVoice;

@property (retain, nonatomic) IBOutlet UIView *lastLineView;

@property (retain, nonatomic) IBOutlet UIView *smsRemainingView;
@property (retain, nonatomic) IBOutlet UILabel *smsRemainingLabel;
@property (retain, nonatomic) IBOutlet UILabel *smsTotalLabel;
@property (retain, nonatomic) IBOutlet UILabel *smsRemainingLabel2;
@property (retain, nonatomic) IBOutlet UILabel *smsTotalLabel2;
@property (retain, nonatomic) IBOutlet UILabel *lblTitleSms;

@property (retain, nonatomic) IBOutlet UIView *buttonView;
@property (retain, nonatomic) IBOutlet UILabel *orLabel;
@property (retain, nonatomic) IBOutlet UIButton *buyButton;
@property (retain, nonatomic) IBOutlet UIButton *stopButton;

@end
