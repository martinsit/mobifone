//
//  BuyVoucherStepFourViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/20/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BuyVoucherStepFourViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

#import "DataManager.h"
#import "EncryptDecrypt.h"

@interface BuyVoucherStepFourViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *sentToLabel;
@property (strong, nonatomic) IBOutlet UILabel *msisdnLabel;

@property (strong, nonatomic) IBOutlet UILabel *nominalLabel;
@property (strong, nonatomic) IBOutlet UILabel *nominalValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *methodLabel;
@property (strong, nonatomic) IBOutlet UILabel *methodValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *bankLabel;
@property (strong, nonatomic) IBOutlet UILabel *bankValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *balanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *balanceValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *activeDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *activeDateValueLabel;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

@end

@implementation BuyVoucherStepFourViewController

@synthesize method;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

/*
- (void)viewWillAppear:(BOOL)animated {
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.paymentDebitData.transType isEqualToString:self.method.value]) {
        _methodValueLabel.text = self.method.label;
    }
    else {
        _methodValueLabel.text = sharedData.paymentDebitData.transType;
    }
}*/

- (void)viewWillAppear:(BOOL)animated {
    DataManager *sharedData = [DataManager sharedInstance];
    
    //-------------//
    // Setup Label //
    //-------------//
    _sentToLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _sentToLabel.textColor = kDefaultTitleFontGrayColor;
    _sentToLabel.text = [[Language get:@"xl_number" alter:nil] uppercaseString];
    
    _msisdnLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _msisdnLabel.textColor = kDefaultPurpleColor;
    _msisdnLabel.backgroundColor = [UIColor clearColor];
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.paymentDebitData.msisdnTo action:kCCDecrypt withKey:saltKeyAPI];
    _msisdnLabel.text = msisdn;
    
    _nominalLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _nominalLabel.textColor = kDefaultTitleFontGrayColor;
    _nominalLabel.text = [[Language get:@"nominal" alter:nil] uppercaseString];
    
    _nominalValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _nominalValueLabel.textColor = kDefaultPurpleColor;
    _nominalValueLabel.backgroundColor = [UIColor clearColor];
    _nominalValueLabel.text = addThousandsSeparator(sharedData.paymentDebitData.amount, sharedData.profileData.language);
    
    _methodLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _methodLabel.textColor = kDefaultTitleFontGrayColor;
    _methodLabel.text = [[Language get:@"payment_method" alter:nil] uppercaseString];
    
    _methodValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _methodValueLabel.textColor = kDefaultPurpleColor;
    _methodValueLabel.backgroundColor = [UIColor clearColor];
    if ([sharedData.paymentDebitData.transTypeLabel length] <= 0) {
        sharedData.paymentDebitData.transTypeLabel = self.method.label;
    }
    _methodValueLabel.text = sharedData.paymentDebitData.transTypeLabel;
    
    _bankLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _bankLabel.textColor = kDefaultTitleFontGrayColor;
    _bankLabel.text = [[Language get:@"bank" alter:nil] uppercaseString];
    
    _bankValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _bankValueLabel.textColor = kDefaultPurpleColor;
    _bankValueLabel.backgroundColor = [UIColor clearColor];
    _bankValueLabel.text = sharedData.paymentDebitData.bankName;
    
    _balanceLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _balanceLabel.textColor = kDefaultTitleFontGrayColor;
    _balanceLabel.text = [[Language get:@"new_balance" alter:nil] uppercaseString];
    
    _balanceValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _balanceValueLabel.textColor = kDefaultPurpleColor;
    _balanceValueLabel.backgroundColor = [UIColor clearColor];
    _balanceValueLabel.text = addThousandsSeparator(sharedData.paymentDebitData.balance, sharedData.profileData.language);
    
    _activeDateLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _activeDateLabel.textColor = kDefaultTitleFontGrayColor;
    _activeDateLabel.text = [[Language get:@"new_active_date" alter:nil] uppercaseString];
    
    _activeDateValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _activeDateValueLabel.textColor = kDefaultPurpleColor;
    _activeDateValueLabel.backgroundColor = [UIColor clearColor];
    NSString *date = [sharedData.paymentDebitData.activeDate substringToIndex:8];
    //NSLog(@"date = %@",date);
    _activeDateValueLabel.text = changeDateFormat(date);
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _activeDateValueLabel.frame.origin.y + _activeDateValueLabel.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:@"BUY VOUCHER"];
    _sectionHeaderView = headerView;
    
    _sectionHeaderView.icon = self.headerIconMaster;
    _sectionHeaderView.title = self.headerTitleMaster;
    
    [_contentView addSubview:_sectionHeaderView];
    [_contentView sendSubviewToBack:_sectionHeaderView];
    [headerView release];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [method release];
    [super dealloc];
}

@end
