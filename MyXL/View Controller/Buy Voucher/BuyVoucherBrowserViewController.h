//
//  BuyVoucherBrowserViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 5/3/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface BuyVoucherBrowserViewController : BasicViewController <UIWebViewDelegate, NSURLConnectionDataDelegate> {
    
}

@property (nonatomic, retain) NSString *msisdnFrom;
@property (nonatomic, retain) NSString *msisdnTo;
@property (nonatomic, retain) NSString *amount;
@property (nonatomic, retain) NSString *trxType;
@property (nonatomic, retain) NSString *device;
@property (nonatomic, retain) NSString *dateTime;
@property (nonatomic, retain) NSString *lang;

@end
