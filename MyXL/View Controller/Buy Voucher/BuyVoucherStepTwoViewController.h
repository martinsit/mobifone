//
//  BuyVoucherStepTwoViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "LabelValueModel.h"
#import "AXISnetRequest.h"

@interface BuyVoucherStepTwoViewController : BasicViewController <UITextFieldDelegate, UIActionSheetDelegate, UIPickerViewDelegate, AXISnetRequestDelegate> {
    NSString *msisdn;
    LabelValueModel *amount;
    
    NSArray *paymentMethods;
}

@property (nonatomic, retain) NSString *msisdn;
@property (nonatomic, retain) LabelValueModel *amount;
@property (nonatomic, retain) NSArray *paymentMethods;

@end
