//
//  BuyVoucherStepThreeViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/17/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "LabelValueModel.h"
#import "BankModel.h"
#import "AXISnetRequest.h"

@interface BuyVoucherStepThreeViewController : BasicViewController <UITextFieldDelegate, UIWebViewDelegate, AXISnetRequestDelegate, UIAlertViewDelegate> {
    NSString *msisdn;
    LabelValueModel *amount;
    LabelValueModel *method;
    BankModel *bank;
    NSString *cardNumber;
}

@property (nonatomic, retain) NSString *msisdn;
@property (nonatomic, retain) LabelValueModel *amount;
@property (nonatomic, retain) LabelValueModel *method;
@property (nonatomic, retain) BankModel *bank;
@property (nonatomic, retain) NSString *cardNumber;

@end
