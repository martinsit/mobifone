//
//  BuyVoucherStepFourViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/20/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "LabelValueModel.h"

@interface BuyVoucherStepFourViewController : BasicViewController {
    LabelValueModel *method;
}

@property (nonatomic, retain) LabelValueModel *method;

@end
