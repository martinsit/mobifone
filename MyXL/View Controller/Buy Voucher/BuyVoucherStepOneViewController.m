//
//  BuyVoucherStepOneViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BuyVoucherStepOneViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

#import "DataManager.h"
#import "EncryptDecrypt.h"
#import "LabelValueModel.h"

#import "BuyVoucherStepTwoViewController.h"

#import "ActionSheetStringPicker.h"

@interface BuyVoucherStepOneViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *sentToLabel;
@property (strong, nonatomic) IBOutlet UILabel *yourAXISLabel;
@property (strong, nonatomic) IBOutlet UILabel *myMSISDNLabel;
@property (strong, nonatomic) IBOutlet UILabel *otherAXISLabel;
@property (strong, nonatomic) IBOutlet UITextField *otherMSISDNTF;
@property (strong, nonatomic) IBOutlet UILabel *nominalLabel;
@property (strong, nonatomic) IBOutlet UIButton *nominalButton;

@property (strong, nonatomic) IBOutlet UIButton *myAXISButton;
@property (strong, nonatomic) IBOutlet UIButton *otherAXISButton;

@property (strong, nonatomic) UIButton *nextButton;

@property (readwrite) BOOL isMyAXIS;

@property (strong, nonatomic) UIActionSheet *nominalActionSheet;
@property (strong, nonatomic) UIPickerView *nominalPickerView;
@property (strong, nonatomic) NSArray *amounts;
@property (strong, nonatomic) NSArray *amountsLabel;
@property (readwrite) int rowIndex;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;
@property (strong, nonatomic) ContentView *containerView;

- (IBAction)performChangeToMyAXIS:(id)sender;
- (IBAction)performChangeToOtherAXIS:(id)sender;
- (void)changeSentTo:(BOOL)isMyAXISSelected;
- (void)didTapMyAXISWithGesture:(UITapGestureRecognizer *)tapGesture;
- (void)didTapOtherAXISWithGesture:(UITapGestureRecognizer *)tapGesture;

- (void)performNext:(id)sender;
- (BOOL)validateInput;

@end

@implementation BuyVoucherStepOneViewController

@synthesize nominalActionSheet;
@synthesize nominalPickerView;
@synthesize amounts;
@synthesize amountsLabel;
@synthesize rowIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    rowIndex = 0;
    
    // Set Default to My AXIS
    [self performChangeToMyAXIS:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    
    //-------------//
    // Setup Label //
    //-------------//
    _sentToLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _sentToLabel.textColor = kDefaultTitleFontGrayColor;
    _sentToLabel.text = [[Language get:@"send_to" alter:nil] uppercaseString];
    
    _yourAXISLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _yourAXISLabel.textColor = kDefaultPurpleColor;
    _yourAXISLabel.backgroundColor = [UIColor clearColor];
    _yourAXISLabel.text = [Language get:@"your_axis_number" alter:nil];
    
    _myMSISDNLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _myMSISDNLabel.textColor = kDefaultPurpleColor;
    _myMSISDNLabel.backgroundColor = [UIColor clearColor];
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.checkBalanceData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    _myMSISDNLabel.text = msisdn;
    
    _otherAXISLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _otherAXISLabel.textColor = kDefaultPurpleColor;
    _otherAXISLabel.backgroundColor = [UIColor clearColor];
    _otherAXISLabel.text = [Language get:@"other_axis_number" alter:nil];
    
    _otherMSISDNTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _otherMSISDNTF.textColor = kDefaultPurpleColor;
    
    _nominalLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _nominalLabel.textColor = kDefaultTitleFontGrayColor;
    _nominalLabel.backgroundColor = [UIColor clearColor];
    _nominalLabel.text = [[Language get:@"nominal" alter:nil] uppercaseString];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(didTapMyAXISWithGesture:)];
    [_yourAXISLabel setUserInteractionEnabled:YES];
    [_yourAXISLabel addGestureRecognizer:tapGesture];
    [tapGesture release];
    
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(didTapOtherAXISWithGesture:)];
    [_otherAXISLabel setUserInteractionEnabled:YES];
    [_otherAXISLabel addGestureRecognizer:tapGesture2];
    [tapGesture2 release];
    
    //--------------//
    // Setup Button //
    //--------------//
    // Submit Button
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    
    if (_nextButton) {
        [_nextButton removeFromSuperview];
    }
    
    UIButton *button = createButtonWithFrame(CGRectMake(_nominalButton.frame.origin.x,
                                                        _nominalButton.frame.origin.y + _nominalButton.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             
                                             [Language get:@"next" alter:nil],
                                             buttonFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
    
    [button addTarget:self action:@selector(performNext:) forControlEvents:UIControlEventTouchUpInside];
    
    _nextButton = button;
    [_contentView addSubview:_nextButton];
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _nextButton.frame.origin.y + _nextButton.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    /*
    if (_sectionHeaderView) {
        [_sectionHeaderView removeFromSuperview];
    }*/
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:@"BUY VOUCHER"];
    _sectionHeaderView = headerView;
    _sectionHeaderView.icon = self.headerIconMaster;
    _sectionHeaderView.title = self.headerTitleMaster;
    [_contentView addSubview:_sectionHeaderView];
    [_contentView sendSubviewToBack:_sectionHeaderView];
    [headerView release];
    /*
    if (_containerView) {
        [_containerView removeFromSuperview];
    }*/
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    _containerView = contentViewTmp;
    [_contentView addSubview:_containerView];
    [_contentView sendSubviewToBack:_containerView];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 200.0);
    
    self.amounts = [NSArray arrayWithArray:[sharedData.paymentParamData objectForKey:AMOUNT_KEY]];
    //rowIndex = 0;
    LabelValueModel *labelValueModel = [self.amounts objectAtIndex:rowIndex];
    [_nominalButton setTitle:labelValueModel.label forState:UIControlStateNormal];
    [_nominalButton setTitleColor:kDefaultPurpleColor forState:UIControlStateNormal];
    _nominalButton.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _nominalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _nominalButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    UIImage *backgroundImage = [[UIImage imageNamed:@"combo_bg_1.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0];
    [_nominalButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
    // Set Default to My AXIS
    //[self performChangeToMyAXIS:nil];
    
    NSMutableArray *labelArray = [[NSMutableArray alloc] init];
    for (LabelValueModel *labelValueModel in self.amounts) {
        [labelArray addObject:labelValueModel.label];
    }
    self.amountsLabel = [NSArray arrayWithArray:labelArray];
    [labelArray release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [nominalActionSheet release];
    [nominalPickerView release];
    [amounts release];
    [amountsLabel release];
    
    [super dealloc];
}

#pragma mark - Selector

- (BOOL)validateInput {
    BOOL result = NO;
    if ([_otherMSISDNTF.text length] > 0) {
        result = YES;
    }
    return result;
}

- (IBAction)performChangeToMyAXIS:(id)sender {
    [_otherMSISDNTF resignFirstResponder];
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    _isMyAXIS = YES;
    [self changeSentTo:_isMyAXIS];
}

- (void)didTapMyAXISWithGesture:(UITapGestureRecognizer *)tapGesture {
    [_otherMSISDNTF resignFirstResponder];
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    _isMyAXIS = YES;
    [self changeSentTo:_isMyAXIS];
}

- (IBAction)performChangeToOtherAXIS:(id)sender {
    _isMyAXIS = NO;
    [self changeSentTo:_isMyAXIS];
}

- (void)didTapOtherAXISWithGesture:(UITapGestureRecognizer *)tapGesture {
    _isMyAXIS = NO;
    [self changeSentTo:_isMyAXIS];
}

- (void)changeSentTo:(BOOL)isMyAXISSelected {
    if (isMyAXISSelected) {
        [_myAXISButton setSelected:YES];
        [_otherAXISButton setSelected:NO];
        
        _myMSISDNLabel.hidden = NO;
        _otherMSISDNTF.hidden = YES;
    }
    else {
        [_myAXISButton setSelected:NO];
        [_otherAXISButton setSelected:YES];
        
        _myMSISDNLabel.hidden = YES;
        _otherMSISDNTF.hidden = NO;
    }
}

- (void)performNext:(id)sender {
    
    BOOL valid = YES;
    
    if (!_isMyAXIS) {
        valid = [self validateInput];
    }
    
    if (valid) {
        
        BuyVoucherStepTwoViewController *buyVoucherStepTwoViewController = [[BuyVoucherStepTwoViewController alloc] initWithNibName:@"BuyVoucherStepTwoViewController" bundle:nil];
        if (_isMyAXIS) {
            buyVoucherStepTwoViewController.msisdn = _myMSISDNLabel.text;
            
            // edited by iNot 14 May 2013, 9:33 PM
            DataManager *sharedData = [DataManager sharedInstance];
            NSArray *allPaymentMethods = [NSArray arrayWithArray:[sharedData.paymentParamData objectForKey:METHOD_KEY]];
            buyVoucherStepTwoViewController.paymentMethods = [NSArray arrayWithArray:allPaymentMethods];
            //
        }
        else {
            [_otherMSISDNTF resignFirstResponder];
            buyVoucherStepTwoViewController.msisdn = _otherMSISDNTF.text;
            
            // edited by iNot 14 May 2013, 9:33 PM
            DataManager *sharedData = [DataManager sharedInstance];
            NSArray *allPaymentMethods = [NSArray arrayWithArray:[sharedData.paymentParamData objectForKey:METHOD_KEY]];
            
            NSMutableArray *paymentMethodsTmp = [[NSMutableArray alloc] init];
            for (LabelValueModel *labelValueModel in allPaymentMethods) {
                if ([labelValueModel.itemId isEqualToString:@"1"]) {
                    [paymentMethodsTmp addObject:labelValueModel];
                }
            }
            buyVoucherStepTwoViewController.paymentMethods = [NSArray arrayWithArray:paymentMethodsTmp];
            [paymentMethodsTmp release];
            //
        }
        
        //[_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
        
        buyVoucherStepTwoViewController.amount = [self.amounts objectAtIndex:rowIndex];
        
        buyVoucherStepTwoViewController.headerTitleMaster = self.headerTitleMaster;
        buyVoucherStepTwoViewController.headerIconMaster = self.headerIconMaster;
        
        [self.navigationController pushViewController:buyVoucherStepTwoViewController animated:YES];
        buyVoucherStepTwoViewController = nil;
        //[buyVoucherStepTwoViewController release];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"incomplete_input" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"ok" alter:nil]
                                              otherButtonTitles:nil];
        alert.tag = 100;
        [alert show];
        [alert release];
    }
}

- (IBAction)performShowNominal:(id)sender {
    [_otherMSISDNTF resignFirstResponder];
    
    [ActionSheetStringPicker showPickerWithTitle:@""
                                            rows:self.amountsLabel
                                initialSelection:rowIndex
                                          target:self
                                   successAction:@selector(langWasSelected:element:)
                                    cancelAction:@selector(actionPickerCancelled:)
                                          origin:sender];
    /*
    UIActionSheet *nominalActionSheetTmp = [[UIActionSheet alloc] initWithTitle:@""
                                                                       delegate:self
                                                              cancelButtonTitle:nil
                                                         destructiveButtonTitle:nil
                                                              otherButtonTitles:nil];
    
    UIPickerView *thePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
    thePickerView.showsSelectionIndicator = TRUE;
    self.nominalPickerView = thePickerView;
    nominalPickerView.delegate = self;
    [nominalPickerView selectRow:rowIndex inComponent:0 animated:YES];
    [thePickerView release];
    
    UIToolbar *pickerToolbar;
    pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                               target:self
                                                                               action:@selector(actionCancel:)];
    [barItems addObject:cancelBtn];
    [cancelBtn release];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                               target:self
                                                                               action:nil];
    [barItems addObject:flexSpace];
    [flexSpace release];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                             target:self
                                                                             action:@selector(actionDone:)];
    [barItems addObject:doneBtn];
    [doneBtn release];
    [pickerToolbar setItems:barItems animated:YES];
    [barItems release];
    [nominalActionSheetTmp addSubview:pickerToolbar];
    [nominalActionSheetTmp addSubview:nominalPickerView];
    [pickerToolbar release];
    
    self.nominalActionSheet = nominalActionSheetTmp;
    [nominalActionSheetTmp release];
    
    [nominalActionSheet showInView:self.view];
    [nominalActionSheet setBounds:CGRectMake(0, 0, 320, 464)];*/
}

- (void)actionCancel:(id)sender {
    [nominalActionSheet dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)actionDone:(id)sender {
	[nominalActionSheet dismissWithClickedButtonIndex:1 animated:YES];
}

#pragma mark - Implementation ActionSheetPicker.h

- (void)langWasSelected:(NSNumber *)selectedIndex element:(id)element {
    rowIndex = [selectedIndex intValue];
    LabelValueModel *labelValueModel = [self.amounts objectAtIndex:rowIndex];
    [_nominalButton setTitle:labelValueModel.label forState:UIControlStateNormal];
}

- (void)actionPickerCancelled:(id)sender {
    //NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	rowIndex = row;
    LabelValueModel *labelValueModel = [self.amounts objectAtIndex:rowIndex];
    [_nominalButton setTitle:labelValueModel.label forState:UIControlStateNormal];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    LabelValueModel *labelValueModel = [self.amounts objectAtIndex:row];
    return labelValueModel.label;
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return [self.amounts count];
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat targetY = -_otherAXISLabel.frame.size.height + textField.frame.origin.y;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 30);
}

@end
