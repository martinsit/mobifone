//
//  BuyVoucherStepThreeViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/17/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BuyVoucherStepThreeViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

#import "DataManager.h"
#import "BuyVoucherStepFourViewController.h"

#import "UnderLineLabel.h"

@interface BuyVoucherStepThreeViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *sentToLabel;
@property (strong, nonatomic) IBOutlet UILabel *msisdnLabel;

@property (strong, nonatomic) IBOutlet UILabel *nominalLabel;
@property (strong, nonatomic) IBOutlet UILabel *nominalValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *methodLabel;
@property (strong, nonatomic) IBOutlet UILabel *methodValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *bankLabel;
@property (strong, nonatomic) IBOutlet UILabel *bankValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *cardLabel;
@property (strong, nonatomic) IBOutlet UILabel *cardValueLabel;

@property (strong, nonatomic) IBOutlet UIWebView *helpView;

@property (strong, nonatomic) IBOutlet UILabel *tokenLabel;
@property (strong, nonatomic) IBOutlet UITextField *tokenTextField;

@property (strong, nonatomic) UIButton *nextButton;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;
@property (strong, nonatomic) ContentView *containerView;

@property (strong, nonatomic) IBOutlet UILabel *captchaLabel;
@property (strong, nonatomic) IBOutlet UIWebView *captchaWebView;
@property (strong, nonatomic) IBOutlet UnderLineLabel *recaptchaLabel;
@property (strong, nonatomic) IBOutlet UITextField *captchaTF;
// TODO : Hygiene
@property (nonatomic, retain) NSTimer *captchaTimer;

- (void)performPayment;
- (void)submitPayment;

@end

@implementation BuyVoucherStepThreeViewController

@synthesize msisdn;
@synthesize amount;
@synthesize method;
@synthesize bank;
@synthesize cardNumber;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    
    //-------------//
    // Setup Label //
    //-------------//
    _sentToLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _sentToLabel.textColor = kDefaultTitleFontGrayColor;
    _sentToLabel.text = [[Language get:@"xl_number" alter:nil] uppercaseString];
    
    _msisdnLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _msisdnLabel.textColor = kDefaultPurpleColor;
    _msisdnLabel.backgroundColor = [UIColor clearColor];
    
    _nominalLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _nominalLabel.textColor = kDefaultTitleFontGrayColor;
    _nominalLabel.text = [[Language get:@"nominal" alter:nil] uppercaseString];
    
    _nominalValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _nominalValueLabel.textColor = kDefaultPurpleColor;
    _nominalValueLabel.backgroundColor = [UIColor clearColor];
    
    _methodLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _methodLabel.textColor = kDefaultTitleFontGrayColor;
    _methodLabel.text = [[Language get:@"payment_method" alter:nil] uppercaseString];
    
    _methodValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _methodValueLabel.textColor = kDefaultPurpleColor;
    _methodValueLabel.backgroundColor = [UIColor clearColor];
    
    _bankLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _bankLabel.textColor = kDefaultTitleFontGrayColor;
    _bankLabel.text = [[Language get:@"bank" alter:nil] uppercaseString];
    
    _bankValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _bankValueLabel.textColor = kDefaultPurpleColor;
    _bankValueLabel.backgroundColor = [UIColor clearColor];
    
    _cardLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _cardLabel.textColor = kDefaultTitleFontGrayColor;
    _cardLabel.text = [[Language get:@"card_number" alter:nil] uppercaseString];
    
    _cardValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _cardValueLabel.textColor = kDefaultPurpleColor;
    _cardValueLabel.backgroundColor = [UIColor clearColor];
    
    _tokenLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _tokenLabel.textColor = kDefaultTitleFontGrayColor;
    _tokenLabel.text = [[Language get:@"token" alter:nil] uppercaseString];
    _tokenLabel.backgroundColor = [UIColor clearColor];
    
    _tokenTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _tokenTextField.textColor = kDefaultPurpleColor;
    
    // Add by iNot 22 July 2013
    // Captcha
    _captchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _captchaLabel.textColor = kDefaultTitleFontGrayColor;
    NSString *text = [NSString stringWithFormat:@"%@*",[[Language get:@"captcha_code" alter:nil] uppercaseString]];
    _captchaLabel.text = text;
    
    _recaptchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _recaptchaLabel.textColor = kDefaultPurpleColor;
    _recaptchaLabel.text = [Language get:@"recaptcha" alter:nil];
    [_recaptchaLabel setShouldUnderline:YES];
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(refreshCaptcha)];
    [_recaptchaLabel setUserInteractionEnabled:YES];
    [_recaptchaLabel addGestureRecognizer:tapGesture3];
    [tapGesture3 release];
    
    _captchaTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _captchaTF.textColor = kDefaultPurpleColor;
    
    //****** Request on 5 September 2013, Hide Captcha ******
    _captchaLabel.hidden = YES;
    _recaptchaLabel.hidden = YES;
    _captchaWebView.hidden = YES;
    _captchaTF.hidden = YES;
    //*******************************************************
    
    //
    
    //--------------//
    // Setup Button //
    //--------------//
    // Next Button
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    
    if (_nextButton) {
        [_nextButton removeFromSuperview];
    }
    
    UIButton *button = createButtonWithFrame(CGRectMake(_tokenTextField.frame.origin.x,
                                                        _tokenTextField.frame.origin.y + _tokenTextField.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             
                                             [Language get:@"next" alter:nil],
                                             buttonFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
    
    [button addTarget:self action:@selector(performPayment) forControlEvents:UIControlEventTouchUpInside];
    
    _nextButton = button;
    
    /*
    NSArray *viewsToRemove = [_contentView subviews];
    for (UIView *v in viewsToRemove) {
        if ([v isKindOfClass:[UIButton class]] ||
            [v isKindOfClass:[SectionHeaderView class]] ||
            [v isKindOfClass:[ContentView class]]) {
            [v removeFromSuperview];
        }
    }*/
    
    [_contentView addSubview:_nextButton];
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _nextButton.frame.origin.y + _nextButton.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    /*
    if (_sectionHeaderView) {
        [_sectionHeaderView removeFromSuperview];
    }*/
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:@"BUY VOUCHER"];
    _sectionHeaderView = headerView;
    _sectionHeaderView.icon = self.headerIconMaster;
    _sectionHeaderView.title = self.headerTitleMaster;
    [_contentView addSubview:_sectionHeaderView];
    [_contentView sendSubviewToBack:_sectionHeaderView];
    [headerView release];
    
    /*
    if (_containerView) {
        [_containerView removeFromSuperview];
    }*/
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    _containerView = contentViewTmp;
    [_contentView addSubview:_containerView];
    [_contentView sendSubviewToBack:_containerView];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 200.0);
    
    //---------------//
    // Setup WebView //
    //---------------//
    
    DataManager *sharedData = [DataManager sharedInstance];
    //NSString *html = [NSString stringWithFormat:@"<html><head><style>body {background:transparent; font-family:'Helvetica Neue', Helvetica; font-size:14px; color:#662584;} h2{margin:0 0 6px; font-size:16px;} p{line-height:18px; margin:0 0 10px} a{color:#B5005B; text-decoration:none; font-weight:bold;}</style></head><body><br/><h2>%@</h2><p>%@</p><p>%@</p><p>&nbsp;</p><p>&nbsp;</p></body></html>",title,body,href];
    
    NSString *html = [NSString stringWithFormat:@"<html><body>%@</body></html>",sharedData.paymentHelpData];
    //NSString *html = [NSString stringWithFormat:@"<html><body>Tes Aja</body></html>"];
    
    //NSString *body = sharedData.paymentHelpData;
    //NSString *html = [NSString stringWithFormat:@"<html><head><meta name=\"viewport\" content=\"initial-scale=1, user-scalable=no, width=device-width\" /></head><body>%@</body></html>",body];
    
    [_helpView loadHTMLString:[NSString stringWithFormat:@"%@",html] baseURL:nil];
    [_helpView setBackgroundColor:[UIColor clearColor]];
    
    for (UIView *wview in [[[_helpView subviews] objectAtIndex:0] subviews]) {
        if([wview isKindOfClass:[UIImageView class]])
        {
            wview.hidden = YES;
        }
    }
    
    _msisdnLabel.text = self.msisdn;
    _nominalValueLabel.text = self.amount.label;
    _methodValueLabel.text = self.method.label;
    _bankValueLabel.text = self.bank.value;
    _cardValueLabel.text = self.cardNumber;
    
    // Request Captcha
    [self refreshCaptcha];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [msisdn release];
    [amount release];
    [method release];
    [bank release];
    [cardNumber release];
    [super dealloc];
}

#pragma mark - Selector

- (void)performPayment {
    [_tokenTextField resignFirstResponder];
    [_captchaTF resignFirstResponder];
    
    //CGFloat targetY = _helpView.frame.origin.y + 60;
    //[_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
    
    UIAlertView *paymentAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                           message:[Language get:@"ask_continue" alter:nil]
                                                          delegate:self
                                                 cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                 otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
    paymentAlert.tag = 1;
    
    [paymentAlert show];
    [paymentAlert release];
}

// TODO : Hygiene
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_captchaTimer invalidate];
}

- (void)submitPayment {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = PAYMENT_DEBIT_REQ;
    /*
    DataManager *sharedData = [DataManager sharedInstance];
    
    [request topUpDebit:self.msisdn
             withAmount:self.amount.value
            withTrxType:self.method.value
               withBank:self.bank.value
             withCardNo:self.cardNumber
          withTokenResp:_tokenTextField.text
            withCaptcha:_captchaTF.text
                withCid:sharedData.cid];*/
    
    [request topUpDebit:self.msisdn
             withAmount:self.amount.value
            withTrxType:self.method.value
               withBank:self.bank.value
             withCardNo:self.cardNumber
          withTokenResp:_tokenTextField.text
            withCaptcha:@""
                withCid:@""];
}

- (void)refreshCaptcha {
    // TODO : Hygiene
    if(_captchaTimer != nil)
        [_captchaTimer invalidate];
    [self scheduleTimerForCaptcha];
    
    NSString *captchaURL = generateCaptchaURL();
    NSURL *captcha = [NSURL URLWithString:captchaURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:captcha];
    [_captchaWebView loadRequest:request];
    [request release];
}

// TODO : Hygiene
// Timer for captcha refresh
-(void)scheduleTimerForCaptcha
{
    NSDate *time = [NSDate dateWithTimeIntervalSinceNow:CAPTCHA_REFRESH_TIME];
    NSTimer *t = [[NSTimer alloc] initWithFireDate:time
                                          interval:CAPTCHA_REFRESH_TIME
                                            target:self
                                          selector:@selector(refreshCaptcha)
                                          userInfo:nil repeats:YES];
    
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer:t forMode: NSDefaultRunLoopMode];
    [t release];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == PAYMENT_DEBIT_REQ) {
            BuyVoucherStepFourViewController *buyVoucherStepFourViewController = [[BuyVoucherStepFourViewController alloc] initWithNibName:@"BuyVoucherStepFourViewController" bundle:nil];
            
            buyVoucherStepFourViewController.method = self.method;
            
            buyVoucherStepFourViewController.headerTitleMaster = self.headerTitleMaster;
            buyVoucherStepFourViewController.headerIconMaster = self.headerIconMaster;
            
            [self.navigationController pushViewController:buyVoucherStepFourViewController animated:YES];
            buyVoucherStepFourViewController = nil;
            //[buyVoucherStepFourViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (webView == _helpView) {
        /*
         [webView stringByEvaluatingJavaScriptFromString:
         [NSString stringWithFormat:
         @"document.querySelector('meta[name=viewport]').setAttribute('content', 'width=%d;', false); ",
         (int)webView.frame.size.width]];
         CGRect frame = webView.frame;
         frame.size.height = 1;        // Set the height to a small one.
         webView.frame = frame;       // Set webView's Frame, forcing the Layout of its embedded scrollView with current Frame's constraints (Width set above).
         frame.size.height = webView.scrollView.contentSize.height;  // Get the corresponding height from the webView's embedded scrollView.
         webView.frame = frame;*/
        
        
        //NSLog(@"contentSize.height = %f",webView.scrollView.contentSize.height);
        CGFloat webViewHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.height"] floatValue];
        //    NSString *string = [_helpView stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"body\").offsetHeight;"];
        //    NSLog(@"string = %@",string);
        //    CGFloat height = [string floatValue] + 8;
        
        //NSLog(@"webViewHeight = %f",webViewHeight);
        CGRect frame = [_helpView frame];
        frame.size.height = webViewHeight;
        [_helpView setFrame:frame];
        
        //----------------//
        // Relayout Label //
        //----------------//
        frame = [_tokenLabel frame];
        frame.origin.y = _helpView.frame.origin.y + _helpView.frame.size.height + kDefaultComponentPadding;
        [_tokenLabel setFrame:frame];
        
        frame = [_tokenTextField frame];
        frame.origin.y = _tokenLabel.frame.origin.y + _tokenLabel.frame.size.height + kDefaultComponentPadding - kDefaultPaddingDeduct;
        [_tokenTextField setFrame:frame];
        
        // Add by iNot 22 July 2013
        //------------------//
        // Relayout Captcha //
        //------------------//
        /*
        frame = [_captchaLabel frame];
        frame.origin.y = _tokenTextField.frame.origin.y + _tokenTextField.frame.size.height + kDefaultComponentPadding;
        [_captchaLabel setFrame:frame];
        
        frame = [_captchaWebView frame];
        frame.origin.y = _captchaLabel.frame.origin.y + _captchaLabel.frame.size.height + kDefaultComponentPadding - 5;
        [_captchaWebView setFrame:frame];
        
        frame = [_recaptchaLabel frame];
        frame.origin.y = _captchaWebView.frame.origin.y + _captchaWebView.frame.size.height + kDefaultComponentPadding;
        [_recaptchaLabel setFrame:frame];
        
        frame = [_captchaTF frame];
        frame.origin.y = _recaptchaLabel.frame.origin.y + _recaptchaLabel.frame.size.height + kDefaultComponentPadding;
        [_captchaTF setFrame:frame];*/
        //
        
        //-----------------//
        // Relayout Button //
        //-----------------//
        frame = [_nextButton frame];
        //frame.origin.y = _captchaTF.frame.origin.y + _captchaTF.frame.size.height + kDefaultComponentPadding;
        frame.origin.y = _tokenTextField.frame.origin.y + _tokenTextField.frame.size.height + kDefaultComponentPadding;
        [_nextButton setFrame:frame];
        
        //-----------------------//
        // Relayout Content View //
        //-----------------------//
        frame = [_contentView frame];
        frame.size.height = _nextButton.frame.origin.y + _nextButton.frame.size.height + kDefaultPadding;
        [_contentView setFrame:frame];
        
        ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
        
        NSArray *viewsToRemove = [_contentView subviews];
        for (UIView *v in viewsToRemove) {
            if ([v isKindOfClass:[ContentView class]]) {
                [v removeFromSuperview];
            }
        }
        [_contentView addSubview:contentViewTmp];
        [_contentView sendSubviewToBack:contentViewTmp];
        [contentViewTmp release];
        
        //-------------------------------//
        // Relayout Grey Background View //
        //-------------------------------//
        CGFloat x = _contentView.frame.origin.x - kDefaultLeftPadding;
        CGFloat y = _contentView.frame.origin.y - kDefaultPadding;
        CGFloat width = _contentView.frame.size.width + (kDefaultLeftPadding*2);
        CGFloat height = _contentView.frame.size.height + (kDefaultPadding*2);
        _greyBackgroundView.frame = CGRectMake(x, y, width, height);
        
        _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 200.0);
    }
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    //[_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    //return YES;
    /*
    if (textField == _captchaTF) {
        [textField resignFirstResponder];
        [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    }*/
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    /*
    CGFloat targetY = -_tokenTextField.frame.size.height + textField.frame.origin.y;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];*/
    
    //CGFloat targetY = -72 + textField.frame.origin.y;
    CGFloat targetY = -_tokenTextField.frame.size.height + textField.frame.origin.y;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
    
    /*
    if (textField == _tokenTextField) {
        _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 150.0);
    }*/
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 16);
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // tag == 1 Payment Confirmation
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            [self submitPayment];
        }
        else {
            // do nothing
        }
    }
}

@end
