//
//  BuyVoucherBrowserViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 5/3/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BuyVoucherBrowserViewController.h"
#import "Constant.h"
#import "EncryptDecrypt.h"

@interface BuyVoucherBrowserViewController ()

@property (strong, nonatomic) IBOutlet UIWebView *browserView;
@property (nonatomic) BOOL validatedRequest;
@property (nonatomic, strong) NSURL *originalUrl;

@end

@implementation BuyVoucherBrowserViewController

@synthesize msisdnFrom;
@synthesize msisdnTo;
@synthesize amount;
@synthesize trxType;
@synthesize device;
@synthesize dateTime;
@synthesize lang;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    NSString *saltKeyAPI = SALT_KEY;
    
    NSString *msisdnFromDecrypt = [EncryptDecrypt doCipherForiOS7:self.msisdnFrom action:kCCDecrypt withKey:saltKeyAPI];
    //NSLog(@"msisdnFromDecrypt = %@",msisdnFromDecrypt);
    
    NSString *param = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@",msisdnFromDecrypt,msisdnTo,amount,trxType,device,dateTime,lang];
    //NSLog(@"param = %@",param);
    
    NSString *saltKeyInteracct = @"43AX15net0n45";
    NSString *paramEncrypted = [EncryptDecrypt doCipherForiOS7:param action:kCCEncrypt withKey:saltKeyInteracct];
    
    NSString *urlAddress = [NSString stringWithFormat:@"https://axis-id.ezy-recharge.com/EZYrechargeAxis/LoginController/axisInit?data=%@",paramEncrypted];
    
    //NSLog(@"urlAddress = %@",urlAddress);
    
    //urlAddress = @"https://www.google.com";
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [_browserView loadRequest:requestObj];
    /*
    self.originalUrl = url;
    NSURLRequest *request = [NSURLRequest requestWithURL:self.originalUrl];
    [NSURLConnection connectionWithRequest:request delegate:self];*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [msisdnFrom release];
    [msisdnTo release];
    [amount release];
    [trxType release];
    [device release];
    [dateTime release];
    [lang release];
    [super dealloc];
}

#pragma mark - UIWebViewDelegate

// you will see this called for 404 errors

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.validatedRequest = NO; // reset this for the next link the user clicks on
}

// you will not see this called for 404 errors

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    //NSLog(@"%s error=%@", __FUNCTION__, error);
}

// this is where you could, intercept HTML requests and route them through
// NSURLConnection, to see if the server responds successfully.

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    // we're only validating links we click on; if we validated that successfully, though, let's just go open it
    // nb: we're only validating links we click on because some sites initiate additional html requests of
    // their own, and don't want to get involved in mediating each and every server request; we're only
    // going to concern ourselves with those links the user clicks on.
    
    if (self.validatedRequest || navigationType != UIWebViewNavigationTypeLinkClicked)
        return YES;
    
    // if user clicked on a link and we haven't validated it yet, let's do so
    
    self.originalUrl = request.URL;
    
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    // and if we're validating, don't bother to have the web view load it yet ...
    // the `didReceiveResponse` will do that for us once the connection has been validated
    
    return NO;
}

#pragma mark - NSURLConnectionDataDelegate method

// This code inspired by http://www.ardalahmet.com/2011/08/18/how-to-detect-and-handle-http-status-codes-in-uiwebviews/
// Given that some ISPs do redirects that one might otherwise prefer to see handled as errors, I'm also checking
// to see if the original URL's host matches the response's URL. This logic may be too restrictive (some valid redirects
// will be rejected, such as www.adobephotoshop.com which redirects you to www.adobe.com), but does capture the ISP
// redirect problem I am concerned about.

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    /*
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSString *origiOS4 = @"";
    if (version < 5) {
        origiOS4 = [[NSString alloc] initWithString:response.URL.absoluteString];
    }*/
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    
    NSString *originalUrlHostName = self.originalUrl.host;
    NSString *responseUrlHostName = response.URL.host;
    
    NSRange originalInResponse = [responseUrlHostName rangeOfString:originalUrlHostName]; // handle where we went to "apple.com" and got redirected to "www.apple.com"
    NSRange responseInOriginal = [originalUrlHostName rangeOfString:responseUrlHostName]; // handle where we went to "www.stackoverflow.com" and got redirected to "stackoverflow.com"
    
    if (originalInResponse.location == NSNotFound && responseInOriginal.location == NSNotFound)
    {
        NSLog(@"%s you were redirected from %@ to %@", __FUNCTION__, self.originalUrl.absoluteString, response.URL.absoluteString);
    }
    else if (httpResponse.statusCode < 200 || httpResponse.statusCode >= 300)
    {
        NSLog(@"%s request to %@ failed with statusCode=%ld", __FUNCTION__, response.URL.absoluteString, httpResponse.statusCode);
    }
    else
    {
        [connection cancel];
        
        self.validatedRequest = YES;
        
        [_browserView loadRequest:connection.originalRequest];
        
        return;
    }
    
    [connection cancel];
}

@end
