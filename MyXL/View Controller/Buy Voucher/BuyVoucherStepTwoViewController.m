//
//  BuyVoucherStepTwoViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BuyVoucherStepTwoViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>
#import "UnderLineLabel.h"

#import "DataManager.h"
#import "BankModel.h"

#import "BuyVoucherStepThreeViewController.h"

#import "ActionSheetStringPicker.h"

#import "BuyVoucherBrowserViewController.h"

#import "EncryptDecrypt.h"

@interface BuyVoucherStepTwoViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *sentToLabel;
@property (strong, nonatomic) IBOutlet UILabel *msisdnLabel;

@property (strong, nonatomic) IBOutlet UILabel *nominalLabel;
@property (strong, nonatomic) IBOutlet UILabel *nominalValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *methodLabel;
@property (strong, nonatomic) IBOutlet UIButton *methodButton;

@property (strong, nonatomic) IBOutlet UILabel *bankLabel;
@property (strong, nonatomic) IBOutlet UIButton *bankButton;

@property (strong, nonatomic) IBOutlet UILabel *cardLabel;
@property (strong, nonatomic) IBOutlet UITextField *cardTextField;

@property (strong, nonatomic) IBOutlet UnderLineLabel *helpLabel;

@property (strong, nonatomic) UIButton *nextButton;

@property (strong, nonatomic) UIActionSheet *theActionSheet;

@property (strong, nonatomic) UIPickerView *paymentMethodPickerView;
//@property (strong, nonatomic) NSArray *paymentMethods;
@property (strong, nonatomic) NSArray *paymentMethodsLabel;
@property (readwrite) int rowIndexPaymentMethod;

@property (strong, nonatomic) UIPickerView *bankPickerView;
@property (strong, nonatomic) NSArray *banks;
@property (strong, nonatomic) NSArray *banksLabel;
@property (readwrite) int rowIndexBank;

@property (readwrite) int selectedRowTmp;

@property (strong, nonatomic) NSDictionary *banksDict;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;
@property (strong, nonatomic) ContentView *containerView;

- (void)filterBanksBasedOnPaymentMethod;
- (void)performHelp;
- (IBAction)performShowPaymentMethod:(id)sender;
- (IBAction)performShowBanks:(id)sender;
- (void)performNext;
- (void)paymentHelp;
- (void)performInternalBrowser;

@end

@implementation BuyVoucherStepTwoViewController

@synthesize msisdn;
@synthesize amount;

@synthesize theActionSheet;

@synthesize paymentMethodPickerView;
@synthesize paymentMethods;
@synthesize rowIndexPaymentMethod;

@synthesize bankPickerView;
@synthesize banks;
@synthesize rowIndexBank;

@synthesize selectedRowTmp;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    rowIndexPaymentMethod = 0;
    rowIndexBank = 0;
}

- (void)viewWillAppear:(BOOL)animated {
    
    //-------------//
    // Setup Label //
    //-------------//
    _sentToLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _sentToLabel.textColor = kDefaultTitleFontGrayColor;
    _sentToLabel.text = [[Language get:@"xl_number" alter:nil] uppercaseString];
    
    _msisdnLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _msisdnLabel.textColor = kDefaultPurpleColor;
    _msisdnLabel.backgroundColor = [UIColor clearColor];
    
    _nominalLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _nominalLabel.textColor = kDefaultTitleFontGrayColor;
    _nominalLabel.text = [[Language get:@"nominal" alter:nil] uppercaseString];
    
    _nominalValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _nominalValueLabel.textColor = kDefaultPurpleColor;
    _nominalValueLabel.backgroundColor = [UIColor clearColor];
    
    _methodLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _methodLabel.textColor = kDefaultTitleFontGrayColor;
    _methodLabel.text = [[Language get:@"payment_method" alter:nil] uppercaseString];
    
    _bankLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _bankLabel.textColor = kDefaultTitleFontGrayColor;
    _bankLabel.text = [[Language get:@"bank" alter:nil] uppercaseString];
    
    _cardLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _cardLabel.textColor = kDefaultTitleFontGrayColor;
    _cardLabel.text = [[Language get:@"card_number" alter:nil] uppercaseString];
    
    _cardTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _cardTextField.textColor = kDefaultPurpleColor;
    
    _helpLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _helpLabel.textColor = kDefaultPurpleColor;
    _helpLabel.numberOfLines = 0;
    _helpLabel.lineBreakMode = UILineBreakModeWordWrap;
    //[_helpLabel setShouldUnderline:YES];
    //_helpLabel.backgroundColor = [UIColor yellowColor];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(performHelp)];
    [_helpLabel setUserInteractionEnabled:YES];
    [_helpLabel addGestureRecognizer:tapGesture];
    [tapGesture release];
    
    //--------------//
    // Setup Button //
    //--------------//
    // Payment Method Button
    DataManager *sharedData = [DataManager sharedInstance];
    //self.paymentMethods = [NSArray arrayWithArray:[sharedData.paymentParamData objectForKey:METHOD_KEY]];
    //rowIndexPaymentMethod = 0;
    LabelValueModel *labelValueModel = [self.paymentMethods objectAtIndex:rowIndexPaymentMethod];
    [_methodButton setTitle:labelValueModel.label forState:UIControlStateNormal];
    [_methodButton setTitleColor:kDefaultPurpleColor forState:UIControlStateNormal];
    _methodButton.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _methodButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _methodButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    UIImage *backgroundImage = [[UIImage imageNamed:@"combo_bg_1.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0];
    [_methodButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
    // New //
    NSMutableArray *labelArray = [[NSMutableArray alloc] init];
    for (LabelValueModel *labelValueModel in self.paymentMethods) {
        [labelArray addObject:labelValueModel.label];
    }
    self.paymentMethodsLabel = [NSArray arrayWithArray:labelArray];
    [labelArray release];
    //-----//
    
    // Bank Button
    self.banks = [NSArray arrayWithArray:[sharedData.paymentParamData objectForKey:BANK_KEY]];
    
    // Filter banks based on payment method
    [self filterBanksBasedOnPaymentMethod];
    
    //rowIndexBank = 0;
    
    self.banks = [self.banksDict valueForKey:labelValueModel.itemId];
    BankModel *bankModel = [self.banks objectAtIndex:rowIndexBank];
    
    [_bankButton setTitle:bankModel.label forState:UIControlStateNormal];
    [_bankButton setTitleColor:kDefaultPurpleColor forState:UIControlStateNormal];
    _bankButton.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _bankButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _bankButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [_bankButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
    if (IS_IPAD) {
        CGRect bankFrame = _bankButton.frame;
        bankFrame.size.width = 300.0;
        _bankButton.frame = bankFrame;
    }
    
    // New //
    labelArray = [[NSMutableArray alloc] init];
    for (BankModel *bankModel in self.banks) {
        [labelArray addObject:bankModel.label];
    }
    self.banksLabel = [NSArray arrayWithArray:labelArray];
    [labelArray release];
    //-----//
    
    if ([sharedData.profileData.language isEqualToString:@"id"]) {
        _helpLabel.text = bankModel.labelRefId;
    }
    else {
        _helpLabel.text = bankModel.labelRefEn;
    }
    CGSize size = calculateExpectedSize(_helpLabel, _helpLabel.text);
    CGRect labelFrame = _helpLabel.frame;
    labelFrame.size.height = size.height;
    _helpLabel.frame = labelFrame;
    
    // 3 May 2013
    if ([labelValueModel.value isEqualToString:@"topupCC"]) {
        _cardLabel.hidden = YES;
        _cardTextField.hidden = YES;
        labelFrame.origin.y = _bankButton.frame.origin.y + _bankButton.frame.size.height + kDefaultComponentPadding;
        _helpLabel.frame = labelFrame;
    }
    //-----------
    
    // Next Button
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    
    if (_nextButton) {
        [_nextButton removeFromSuperview];
    }
    
    UIButton *button = createButtonWithFrame(CGRectMake(_helpLabel.frame.origin.x,
                                                        _helpLabel.frame.origin.y + _helpLabel.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             
                                             [Language get:@"next" alter:nil],
                                             buttonFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
    
    [button addTarget:self action:@selector(performNext) forControlEvents:UIControlEventTouchUpInside];
    
    _nextButton = button;
    [_contentView addSubview:_nextButton];
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _nextButton.frame.origin.y + _nextButton.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    /*
    if (_sectionHeaderView) {
        NSLog(@"remove");
        [_sectionHeaderView removeFromSuperview];
    }*/
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:@"BUY VOUCHER"];
    _sectionHeaderView = headerView;
    _sectionHeaderView.icon = self.headerIconMaster;
    _sectionHeaderView.title = self.headerTitleMaster;
    [_contentView addSubview:_sectionHeaderView];
    [_contentView sendSubviewToBack:_sectionHeaderView];
    [headerView release];
    /*
    if (_containerView) {
        [_containerView removeFromSuperview];
    }*/
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    _containerView = contentViewTmp;
    [_contentView addSubview:_containerView];
    [_contentView sendSubviewToBack:_containerView];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 200.0);
    
    _msisdnLabel.text = self.msisdn;
    _nominalValueLabel.text = self.amount.label;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [msisdn release];
    [amount release];
    
    [theActionSheet release];
    
    [paymentMethodPickerView release];
    [paymentMethods release];
    
    [bankPickerView release];
    [banks release];
    
    [super dealloc];
}

#pragma mark - Selector

- (void)filterBanksBasedOnPaymentMethod {
    NSMutableDictionary *banksDictTmp = [NSMutableDictionary dictionary];
    
    for (int k=0; k<[self.paymentMethods count]; k++) {
        LabelValueModel *labelValueModel = [self.paymentMethods objectAtIndex:k];
        NSMutableArray *filteredArray = [NSMutableArray array];
        
        for (int i=0; i<[self.banks count]; i++) {
            BankModel *bankModel = [self.banks objectAtIndex:i];
            if ([bankModel.methodId isEqualToString:labelValueModel.itemId]) {
                [filteredArray addObject:bankModel];
            }
        }
        [banksDictTmp setValue:filteredArray forKey:labelValueModel.itemId];
    }
    self.banksDict = [NSDictionary dictionaryWithDictionary:banksDictTmp];
    //NSLog(@"self.banksDict = %@",self.banksDict);
}

- (IBAction)performShowPaymentMethod:(id)sender {
    [_cardTextField resignFirstResponder];
    
    [ActionSheetStringPicker showPickerWithTitle:@""
                                            rows:self.paymentMethodsLabel
                                initialSelection:rowIndexPaymentMethod
                                          target:self
                                   successAction:@selector(paymentMethodWasSelected:element:)
                                    cancelAction:@selector(actionPickerCancelled:)
                                          origin:sender];
    
    /*
    UIActionSheet *paymentMethodActionSheetTmp = [[UIActionSheet alloc] initWithTitle:@""
                                                                             delegate:self
                                                                    cancelButtonTitle:nil
                                                               destructiveButtonTitle:nil
                                                                    otherButtonTitles:nil];
    
    UIPickerView *thePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
    thePickerView.tag = 1;
    thePickerView.showsSelectionIndicator = TRUE;
    self.paymentMethodPickerView = thePickerView;
    paymentMethodPickerView.delegate = self;
    selectedRowTmp = rowIndexPaymentMethod;
    [paymentMethodPickerView selectRow:rowIndexPaymentMethod inComponent:0 animated:YES];
    [thePickerView release];
    
    UIToolbar *pickerToolbar;
    pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                               target:self
                                                                               action:@selector(actionCancel:)];
    [barItems addObject:cancelBtn];
    [cancelBtn release];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                               target:self
                                                                               action:nil];
    [barItems addObject:flexSpace];
    [flexSpace release];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                             target:self
                                                                             action:@selector(actionDone:)];
    doneBtn.tag = 1;
    [barItems addObject:doneBtn];
    [doneBtn release];
    [pickerToolbar setItems:barItems animated:YES];
    [barItems release];
    [paymentMethodActionSheetTmp addSubview:pickerToolbar];
    [paymentMethodActionSheetTmp addSubview:paymentMethodPickerView];
    [pickerToolbar release];
    
    self.theActionSheet = paymentMethodActionSheetTmp;
    [paymentMethodActionSheetTmp release];
    
    [theActionSheet showInView:self.view];
    [theActionSheet setBounds:CGRectMake(0, 0, 320, 464)];*/
}

- (void)actionCancel:(id)sender {
    [theActionSheet dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)actionDone:(id)sender {
    [theActionSheet dismissWithClickedButtonIndex:1 animated:YES];
    
    UIBarButtonItem *doneButton = (UIBarButtonItem*)sender;
    
    BankModel *bankModel = nil;
    
    if (doneButton.tag == 1) {
        // Payment Method
        rowIndexPaymentMethod = selectedRowTmp;
        LabelValueModel *labelValueModel = [self.paymentMethods objectAtIndex:rowIndexPaymentMethod];
        [_methodButton setTitle:labelValueModel.label forState:UIControlStateNormal];
        
        self.banks = [self.banksDict valueForKey:labelValueModel.itemId];
        rowIndexBank = 0;
        bankModel = [self.banks objectAtIndex:rowIndexBank];
    }
    else {
        // Bank
        rowIndexBank = selectedRowTmp;
        bankModel = [self.banks objectAtIndex:rowIndexBank];
    }
    
    [_bankButton setTitle:bankModel.label forState:UIControlStateNormal];
    
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.profileData.language isEqualToString:@"id"]) {
        _helpLabel.text = bankModel.labelRefId;
    }
    else {
        _helpLabel.text = bankModel.labelRefEn;
    }
    // setup label frame
    CGSize size = calculateExpectedSize(_helpLabel, _helpLabel.text);
    CGRect labelFrame = _helpLabel.frame;
    labelFrame.size.height = size.height;
    _helpLabel.frame = labelFrame;
    
    // setup button frame
    CGRect buttonFrame = _nextButton.frame;
    buttonFrame.origin.y = _helpLabel.frame.origin.y + _helpLabel.frame.size.height + kDefaultComponentPadding;
    _nextButton.frame = buttonFrame;
    
    // setup content view frame
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _nextButton.frame.origin.y + _nextButton.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    
    NSArray *viewsToRemove = [_contentView subviews];
    for (UIView *v in viewsToRemove) {
        if ([v isKindOfClass:[ContentView class]]) {
            [v removeFromSuperview];
        }
    }
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    // setup grey background frame
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
}

- (IBAction)performShowBanks:(id)sender {
    [_cardTextField resignFirstResponder];
    
    [ActionSheetStringPicker showPickerWithTitle:@""
                                            rows:self.banksLabel
                                initialSelection:rowIndexBank
                                          target:self
                                   successAction:@selector(bankWasSelected:element:)
                                    cancelAction:@selector(actionPickerCancelled:)
                                          origin:sender];
    
    /*
    UIActionSheet *bankActionSheetTmp = [[UIActionSheet alloc] initWithTitle:@""
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                      destructiveButtonTitle:nil
                                                           otherButtonTitles:nil];
    bankActionSheetTmp.tag = 2;
    
    UIPickerView *thePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
    thePickerView.tag = 2;
    thePickerView.showsSelectionIndicator = TRUE;
    self.bankPickerView = thePickerView;
    bankPickerView.delegate = self;
    selectedRowTmp = rowIndexBank;
    [bankPickerView selectRow:rowIndexBank inComponent:0 animated:YES];
    [thePickerView release];
    
    UIToolbar *pickerToolbar;
    pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                               target:self
                                                                               action:@selector(actionCancel:)];
    [barItems addObject:cancelBtn];
    [cancelBtn release];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                               target:self
                                                                               action:nil];
    [barItems addObject:flexSpace];
    [flexSpace release];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                             target:self
                                                                             action:@selector(actionDone:)];
    doneBtn.tag = 2;
    [barItems addObject:doneBtn];
    [doneBtn release];
    [pickerToolbar setItems:barItems animated:YES];
    [barItems release];
    [bankActionSheetTmp addSubview:pickerToolbar];
    [bankActionSheetTmp addSubview:bankPickerView];
    [pickerToolbar release];
    
    self.theActionSheet = bankActionSheetTmp;
    [bankActionSheetTmp release];
    
    [theActionSheet showInView:self.view];
    [theActionSheet setBounds:CGRectMake(0, 0, 320, 464)];*/
}

- (void)performHelp {
    BankModel *bankModel = [self.banks objectAtIndex:rowIndexBank];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",bankModel.urlRef]]];
}

- (void)performNext {
    [_cardTextField resignFirstResponder];
    //CGFloat targetY = _sentToLabel.frame.size.height + _sentToLabel.frame.origin.y + 20.0;
    //[_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
    
    LabelValueModel *labelValueModel = [self.paymentMethods objectAtIndex:rowIndexPaymentMethod];
    // 3 May 2013
    if ([labelValueModel.value isEqualToString:@"topupCC"]) {
        [self performInternalBrowser];
    }
    else {
        [self paymentHelp];
    }
    //-----------
}

- (void)paymentHelp {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = PAYMENT_HELP_REQ;
    
    LabelValueModel *labelValueModel = [self.paymentMethods objectAtIndex:rowIndexPaymentMethod];
    BankModel *bankModel = [self.banks objectAtIndex:rowIndexBank];
    
    NSString *bankFormatName = [NSString stringWithFormat:@"%@ %@",labelValueModel.value,bankModel.value];
    //NSLog(@"bankFormatName = %@",bankFormatName);
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdnDencrypted = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    
    //NSLog(@"from = %@",msisdnDencrypted);
    //NSLog(@"to = %@",self.msisdn);
    
    [request paymentHelp:msisdnDencrypted
              withCardNo:_cardTextField.text
                withBank:bankFormatName
              withAmount:self.amount.value];
}

- (void)performInternalBrowser {
    BuyVoucherBrowserViewController *buyVoucherBrowserViewController = [[BuyVoucherBrowserViewController alloc] initWithNibName:@"BuyVoucherBrowserViewController" bundle:nil];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    buyVoucherBrowserViewController.msisdnFrom = sharedData.profileData.msisdn;
    buyVoucherBrowserViewController.msisdnTo = self.msisdn;
    buyVoucherBrowserViewController.amount = self.amount.value;
    buyVoucherBrowserViewController.trxType = @"buyVoucher";
    buyVoucherBrowserViewController.device = @"mobile";
    
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    
    buyVoucherBrowserViewController.dateTime = dateString;
    
    buyVoucherBrowserViewController.lang = sharedData.profileData.language;
    
    [self.navigationController pushViewController:buyVoucherBrowserViewController animated:YES];
    buyVoucherBrowserViewController = nil;
    //[buyVoucherBrowserViewController release];
    
    [dateFormatter release];
}

#pragma mark - Implementation ActionSheetPicker.h

- (void)bankWasSelected:(NSNumber *)selectedIndex element:(id)element {
    rowIndexBank = [selectedIndex intValue];
    BankModel *bankModel = [self.banks objectAtIndex:rowIndexBank];
    [_bankButton setTitle:bankModel.label forState:UIControlStateNormal];
    
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.profileData.language isEqualToString:@"id"]) {
        _helpLabel.text = bankModel.labelRefId;
    }
    else {
        _helpLabel.text = bankModel.labelRefEn;
    }
    // setup label frame
    CGSize size = calculateExpectedSize(_helpLabel, _helpLabel.text);
    CGRect labelFrame = _helpLabel.frame;
    labelFrame.size.height = size.height;
    _helpLabel.frame = labelFrame;
    
    // setup button frame
    CGRect buttonFrame = _nextButton.frame;
    buttonFrame.origin.y = _helpLabel.frame.origin.y + _helpLabel.frame.size.height + kDefaultComponentPadding;
    _nextButton.frame = buttonFrame;
    
    // setup content view frame
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _nextButton.frame.origin.y + _nextButton.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    
    NSArray *viewsToRemove = [_contentView subviews];
    for (UIView *v in viewsToRemove) {
        if ([v isKindOfClass:[ContentView class]]) {
            [v removeFromSuperview];
        }
    }
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    // setup grey background frame
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
}

- (void)paymentMethodWasSelected:(NSNumber *)selectedIndex element:(id)element {
    rowIndexPaymentMethod = [selectedIndex intValue];
    LabelValueModel *labelValueModel = [self.paymentMethods objectAtIndex:rowIndexPaymentMethod];
    [_methodButton setTitle:labelValueModel.label forState:UIControlStateNormal];
    
    //NSLog(@"labelValueModel.itemId = %@",labelValueModel.itemId);
    
    // Filter banks based on payment method
    //[self filterBanksBasedOnPaymentMethod];
    
    self.banks = [self.banksDict valueForKey:labelValueModel.itemId];
    rowIndexBank = 0;
    BankModel *bankModel = [self.banks objectAtIndex:rowIndexBank];
    
    [_bankButton setTitle:bankModel.label forState:UIControlStateNormal];
    
    
    // New //
    NSMutableArray *labelArray = [[NSMutableArray alloc] init];
    for (BankModel *bankModel in self.banks) {
        [labelArray addObject:bankModel.label];
    }
    self.banksLabel = [NSArray arrayWithArray:labelArray];
    [labelArray release];
    //-----//
    
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.profileData.language isEqualToString:@"id"]) {
        _helpLabel.text = bankModel.labelRefId;
    }
    else {
        _helpLabel.text = bankModel.labelRefEn;
    }
    // setup label frame
    CGSize size = calculateExpectedSize(_helpLabel, _helpLabel.text);
    CGRect labelFrame = _helpLabel.frame;
    labelFrame.size.height = size.height;
    _helpLabel.frame = labelFrame;
    
    // 3 May 2013
    if ([labelValueModel.value isEqualToString:@"topupCC"]) {
        _cardLabel.hidden = YES;
        _cardTextField.hidden = YES;
        labelFrame.origin.y = _bankButton.frame.origin.y + _bankButton.frame.size.height + kDefaultComponentPadding;
        _helpLabel.frame = labelFrame;
    }
    else {
        _cardLabel.hidden = NO;
        _cardTextField.hidden = NO;
        labelFrame.origin.y = _cardTextField.frame.origin.y + _cardTextField.frame.size.height + kDefaultComponentPadding;
        _helpLabel.frame = labelFrame;
    }
    //-----------
    
    // setup button frame
    CGRect buttonFrame = _nextButton.frame;
    buttonFrame.origin.y = _helpLabel.frame.origin.y + _helpLabel.frame.size.height + kDefaultComponentPadding;
    _nextButton.frame = buttonFrame;
    
    // setup content view frame
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _nextButton.frame.origin.y + _nextButton.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    
    NSArray *viewsToRemove = [_contentView subviews];
    for (UIView *v in viewsToRemove) {
        if ([v isKindOfClass:[ContentView class]]) {
            [v removeFromSuperview];
        }
    }
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    // setup grey background frame
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
}

- (void)actionPickerCancelled:(id)sender {
    //NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == PAYMENT_HELP_REQ) {
            BuyVoucherStepThreeViewController *buyVoucherStepThreeViewController = [[BuyVoucherStepThreeViewController alloc] initWithNibName:@"BuyVoucherStepThreeViewController" bundle:nil];
            
            buyVoucherStepThreeViewController.msisdn = self.msisdn;
            buyVoucherStepThreeViewController.amount = self.amount;
            
            buyVoucherStepThreeViewController.headerTitleMaster = self.headerTitleMaster;
            buyVoucherStepThreeViewController.headerIconMaster = self.headerIconMaster;
            
            LabelValueModel *methodModel = [self.paymentMethods objectAtIndex:rowIndexPaymentMethod];
            buyVoucherStepThreeViewController.method = methodModel;
            
            BankModel *bankModel = [self.banks objectAtIndex:rowIndexBank];
            buyVoucherStepThreeViewController.bank = bankModel;
            
            buyVoucherStepThreeViewController.cardNumber = _cardTextField.text;
            
            [self.navigationController pushViewController:buyVoucherStepThreeViewController animated:YES];
            buyVoucherStepThreeViewController = nil;
            //[buyVoucherStepThreeViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    selectedRowTmp = row;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView.tag == 1) {
        LabelValueModel *labelValueModel = [self.paymentMethods objectAtIndex:row];
        return labelValueModel.label;
    }
    else {
        BankModel *bankModel = [self.banks objectAtIndex:row];
        return bankModel.label;
    }
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView.tag == 1) {
        return [self.paymentMethods count];
    }
    else {
        return [self.banks count];
    }
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat targetY = -_cardTextField.frame.size.height + textField.frame.origin.y;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 16);
}

@end
