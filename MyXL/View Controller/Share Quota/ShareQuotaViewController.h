//
//  ShareQuotaViewController.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 5/8/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BasicViewController.h"
#import "AXISnetRequest.h"
#import "CMPopTipView.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface ShareQuotaViewController : BasicViewController <UITextFieldDelegate, AXISnetRequestDelegate, CMPopTipViewDelegate,ABPeoplePickerNavigationControllerDelegate> {
    NSString *dataPackage;
    NSString *serviceIdA;
    //NSString *serviceIdB;
    
    NSArray *quotaList;
}

@property (nonatomic, retain) NSString *dataPackage;
@property (nonatomic, retain) NSString *serviceIdA;
//@property (nonatomic, retain) NSString *serviceIdB;

@property (nonatomic, retain) NSArray *quotaList;

@end
