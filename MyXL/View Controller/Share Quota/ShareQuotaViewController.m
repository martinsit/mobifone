//
//  ShareQuotaViewController.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 5/8/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ShareQuotaViewController.h"
#import "SectionHeaderView.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "DataManager.h"
#import "UnderLineLabel.h"
#import "ActionSheetStringPicker.h"
#import "EncryptDecrypt.h"
#import "ThankYouViewController.h"
#import "NotificationViewController.h"
#import "MenuModel.h"
#import "LabelValueModel.h"

@interface ShareQuotaViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, retain) IBOutlet UILabel *shareToLabel;
@property (nonatomic, retain) IBOutlet UITextField *shareToTF;
@property (nonatomic, retain) IBOutlet UIButton *contactButton;

@property (nonatomic, retain) IBOutlet UILabel *quotaLabel;
@property (nonatomic, retain) IBOutlet UIButton *quotaButton;
@property (nonatomic, retain) IBOutlet UIButton *hintButton;

@property (nonatomic, retain) IBOutlet UILabel *dataPackageLabel;
@property (nonatomic, retain) IBOutlet UILabel *dataPackageValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *captchaLabel;
@property (strong, nonatomic) IBOutlet UIWebView *captchaWebView;
@property (strong, nonatomic) IBOutlet UILabel *refreshLabel;
@property (strong, nonatomic) IBOutlet UnderLineLabel *recaptchaLabel;
@property (strong, nonatomic) IBOutlet UITextField *captchaTF;

@property (strong, nonatomic) UIButton *submitButton;

@property (nonatomic, strong)	id				currentPopTipViewTarget;
@property (nonatomic, strong)	NSMutableArray	*visiblePopTipViews;

@property (strong, nonatomic) NSArray *quotaToShare;
@property (readwrite) int rowQuota;
@property (strong, nonatomic) NSString *selectedQuota;

// TODO : Hygiene
@property (nonatomic, retain) NSTimer *captchaTimer;

@end

@implementation ShareQuotaViewController

@synthesize dataPackage;
@synthesize serviceIdA;
//@synthesize serviceIdB;
@synthesize quotaList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [self setupUILayout];
    [self setupValue];
    [self refreshCaptcha];
}

// TODO : Hygiene
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_captchaTimer invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Selector

- (void)setupUILayout {
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:[Language get:@"quota" alter:nil]
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:[Language get:@"share_quota" alter:nil]
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    _shareToLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _shareToLabel.textColor = kDefaultTitleFontGrayColor;
    
    _quotaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _quotaLabel.textColor = kDefaultTitleFontGrayColor;
    
    //---------------//
    // Quota Button //
    //---------------//
    NSMutableArray *quotaToShareTmp = [[NSMutableArray alloc] init];
    for (LabelValueModel *model in self.quotaList) {
        [quotaToShareTmp addObject:model.label];
    }
    
    self.quotaToShare = [NSArray arrayWithArray:quotaToShareTmp];
    _rowQuota = 0;
    self.selectedQuota = [self.quotaToShare objectAtIndex:_rowQuota];
    
    UIImage *backgroundImage = [[UIImage imageNamed:@"combo_bg_1.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0];
    [_quotaButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    [_quotaButton setTitle:[self.quotaToShare objectAtIndex:_rowQuota] forState:UIControlStateNormal];
    [_quotaButton setTitleColor:kDefaultNavyBlueColor forState:UIControlStateNormal];
    _quotaButton.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _quotaButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _quotaButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [_quotaButton addTarget:self action:@selector(showQuotaPicker:) forControlEvents:UIControlEventTouchUpInside];
    
    _dataPackageLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _dataPackageLabel.textColor = kDefaultTitleFontGrayColor;
    
    _dataPackageValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _dataPackageValueLabel.textColor = kDefaultNavyBlueColor;
    
    //------------------//
    //      Captcha     //
    //------------------//
    _captchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _captchaLabel.textColor = kDefaultTitleFontGrayColor;
    NSString *text = [NSString stringWithFormat:@"%@*",[Language get:@"captcha_code" alter:nil]];
    _captchaLabel.text = text;
    
    //------------------//
    //   Refresh Label  //
    //------------------//
    _refreshLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontTitleSize];
    _refreshLabel.textColor = kDefaultNavyBlueColor;
    
    _recaptchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _recaptchaLabel.textColor = kDefaultNavyBlueColor;
    _recaptchaLabel.text = [Language get:@"recaptcha" alter:nil];
    [_recaptchaLabel setShouldUnderline:NO];
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(refreshCaptcha)];
    [_recaptchaLabel setUserInteractionEnabled:YES];
    [_recaptchaLabel addGestureRecognizer:tapGesture3];
    [tapGesture3 release];
    
    _captchaTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _captchaTF.textColor = kDefaultNavyBlueColor;
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    newFrame.size.height = _captchaTF.frame.origin.y + _captchaTF.frame.size.height;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Setup Button //
    //--------------//
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    CGRect frame = _hintButton.frame;
    UIButton *button = createButtonWithFrame(frame,
                                             @"?",
                                             buttonFont,
                                             kDefaultWhiteColor,
                                             kDefaultAquaMarineColor,
                                             kDefaultBlueColor);
    [button addTarget:self action:@selector(performHint:) forControlEvents:UIControlEventTouchUpInside];
    [_fieldView addSubview:button];
    _hintButton.hidden = YES;
    
    frame = _contactButton.frame;
    UIButton *contactBtn = createButtonWithFrame(frame,
                                                 [Language get:@"phonebook" alter:nil],
                                                 buttonFont,
                                                 kDefaultWhiteColor,
                                                 kDefaultAquaMarineColor,
                                                 kDefaultBlueColor);
    [contactBtn addTarget:self action:@selector(performContact:) forControlEvents:UIControlEventTouchUpInside];
    [_fieldView addSubview:contactBtn];
    _contactButton.hidden = YES;
    
    UIButton *submitBtn = createButtonWithFrame(CGRectMake(_captchaTF.frame.origin.x,
                                                           _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
                                                           100,
                                                           kDefaultButtonHeight),
                                                [[Language get:@"submit" alter:nil] uppercaseString],
                                                buttonFont,
                                                kDefaultWhiteColor,
                                                kDefaultAquaMarineColor,
                                                kDefaultBlueColor);
    [submitBtn addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    _submitButton = submitBtn;
    [_contentView addSubview:_submitButton];
    
    //--------------//
    // Content View //
    //--------------//
    newFrame = _contentView.frame;
    newFrame.size.height = _submitButton.frame.origin.y + _submitButton.frame.size.height + kDefaultComponentPadding;
    _contentView.frame = newFrame;
    _contentView.backgroundColor = kDefaultWhiteColor;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 200.0);
    
    [headerView1 release];
    [headerView2 release];
}

- (void)setupValue {
    
    _shareToLabel.text = [Language get:@"xl_number_destination" alter:nil];
    [_contactButton setTitle:[Language get:@"phonebook" alter:nil] forState:UIControlStateNormal];
    
    _quotaLabel.text = [Language get:@"select_quota" alter:nil];
    
    _dataPackageLabel.text = [Language get:@"data_package" alter:nil];
    _dataPackageValueLabel.text = self.dataPackage;
    
    // Request Captcha
    [self refreshCaptcha];
}

- (void)performHint:(id)sender {
    
    NSString *title = [Language get:@"share_quota" alter:nil];
    NSString *contentMessage = [Language get:@"share_quota_hint" alter:nil];
    CMPopTipView *popTipView = [[CMPopTipView alloc] initWithTitle:title message:contentMessage];
    popTipView.delegate = self;
    popTipView.backgroundColor = [UIColor grayColor]; //kDefaultGrayColor
    popTipView.textColor = kDefaultWhiteColor;
    popTipView.animation = arc4random() % 2;
    //popTipView.has3DStyle = (BOOL)(arc4random() % 2);
    popTipView.has3DStyle = NO;
    
    /* Some options to try.
     */
    //popTipView.disableTapToDismiss = YES;
    //popTipView.preferredPointDirection = PointDirectionUp;
    popTipView.hasGradientBackground = NO;
    //popTipView.cornerRadius = 2.0;
    //popTipView.sidePadding = 30.0f;
    //popTipView.topMargin = 20.0f;
    //popTipView.pointerSize = 50.0f;
    popTipView.hasShadow = YES;
    
    popTipView.dismissTapAnywhere = YES;
    //[popTipView autoDismissAnimated:YES atTimeInterval:3.0];
    
    if ([sender isKindOfClass:[UIButton class]]) {
        UIButton *button = (UIButton *)sender;
        [popTipView presentPointingAtView:button inView:_fieldView animated:YES];
    }
    
    [self.visiblePopTipViews addObject:popTipView];
    self.currentPopTipViewTarget = sender;
}

- (void)performContact:(id)sender {
    
    //NSLog(@"Button clicked");
    
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    
    [self presentModalViewController:picker animated:YES];
    //[self presentViewController:picker animated:YES completion:nil];
}

- (void)refreshCaptcha {
    // TODO : Hygiene
    NSLog(@"Captcha Pressed");
    
    if(_captchaTimer != nil)
        [_captchaTimer invalidate];
    [self scheduleTimerForCaptcha];
    
    NSString *captchaURL = generateCaptchaURL();
    NSURL *captcha = [NSURL URLWithString:captchaURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:captcha];
    [_captchaWebView loadRequest:request];
    [request release];
}

// TODO : Hygiene
// Timer for captcha refresh
-(void)scheduleTimerForCaptcha
{
    NSDate *time = [NSDate dateWithTimeIntervalSinceNow:CAPTCHA_REFRESH_TIME];
    NSTimer *t = [[NSTimer alloc] initWithFireDate:time
                                          interval:CAPTCHA_REFRESH_TIME
                                            target:self
                                          selector:@selector(refreshCaptcha)
                                          userInfo:nil repeats:YES];
    
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer:t forMode: NSDefaultRunLoopMode];
    [t release];
}

- (void)performSubmit:(id)sender {
    
    [_shareToTF resignFirstResponder];
    [_captchaTF resignFirstResponder];
    
    int valid = [self validateInput];
    if (valid == 2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"empty_validation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 99;
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
    else {
        NSString *msisdnTo = _shareToTF.text;
        
        NSString *confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@?",
                                         [Language get:@"share_quota_confirmation_1" alter:nil],
                                         _quotaButton.titleLabel.text,
                                         [Language get:@"share_quota_confirmation_2" alter:nil],
                                         msisdnTo];
        
        UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                  message:confirmationMessage
                                                                 delegate:self
                                                        cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                        otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
        buyPackageAlert.tag = 2;
        [buyPackageAlert show];
    }
}

- (void)submit {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = SHARE_QUOTA_REQ;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdnToEncrypted = [EncryptDecrypt doCipherForiOS7:_shareToTF.text action:kCCEncrypt withKey:saltKeyAPI];
    //NSLog(@"msisdnToEncrypted = %@",msisdnToEncrypted);
    
    [request shareQuotaFromServiceIdA:self.serviceIdA
                         toServiceIdB:self.selectedQuota
                             toMsisdn:msisdnToEncrypted
                          withCaptcha:_captchaTF.text
                              withCid:sharedData.cid];
}

- (int)validateInput {
    /*
     * Validate empty field
     */
    if ([_shareToTF.text length] <= 0 ||
        [_captchaTF.text length] <= 0) {
        return 2;
    }
    else {
        return 0;
    }
}

#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
	[self.visiblePopTipViews removeObject:popTipView];
	self.currentPopTipViewTarget = nil;
}

#pragma mark - Implementation ActionSheetPicker.h

- (void)showQuotaPicker:(id)sender {
    [_shareToTF resignFirstResponder];
    [_captchaTF resignFirstResponder];
    
    [ActionSheetStringPicker showPickerWithTitle:@""
                                            rows:self.quotaToShare
                                initialSelection:_rowQuota
                                          target:self
                                   successAction:@selector(quotaWasSelected:element:)
                                    cancelAction:@selector(actionPickerCancelled:)
                                          origin:sender];
}

- (void)quotaWasSelected:(NSNumber *)selectedIndex element:(id)element {
    _rowQuota = [selectedIndex intValue];
    LabelValueModel *labelValueModel = [self.quotaList objectAtIndex:_rowQuota];
    self.selectedQuota = labelValueModel.value;
    
    //[_quotaButton setTitle:[self.quotaToShare objectAtIndex:_rowQuota] forState:UIControlStateNormal];
    [_quotaButton setTitle:labelValueModel.label forState:UIControlStateNormal];
    _quotaButton.titleLabel.textColor = kDefaultNavyBlueColor;
}

- (void)actionPickerCancelled:(id)sender {
    //NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _captchaTF) {
        [textField resignFirstResponder];
        [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat targetY = -10 + textField.frame.origin.y;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _captchaTF) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return !([newString length] > 6);
    }
    else {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return !([newString length] > 16);
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // tag == 2 Share Quota
    if (alertView.tag == 2) {
        if (buttonIndex == 0) {
            //NSLog(@"YES");
            [self submit];
        }
        else {
            //NSLog(@"NO");
        }
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == SHARE_QUOTA_REQ) {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            
            //thankYouViewController.info = [Language get:@"xl_tunai_reload_thank_you" alter:nil];
            NSString *reason = [result valueForKey:REASON_KEY];
            thankYouViewController.info = reason;
            //thankYouViewController.menuModel = _menuModel;
            thankYouViewController.displayFBButton = NO;
            thankYouViewController.hideInboxBtn = NO;
            
            MenuModel *model = [[MenuModel alloc] init];
            model.groupName = [Language get:@"share_quota" alter:nil];
            
            thankYouViewController.menuModel = model;
            
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            thankYouViewController = nil;
            [thankYouViewController release];
            
            [model release];
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
}

#pragma mark - ABPeoplePickerDelegate

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [self dismissModalViewControllerAnimated:YES];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    
    [self displayPerson:person];
    [self dismissModalViewControllerAnimated:YES];
    
    return NO;
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person
                                property:(ABPropertyID)property
                              identifier:(ABMultiValueIdentifier)identifier
{
    return NO;
}

- (void)displayPerson:(ABRecordRef)person
{
    NSString *phone = nil;
    ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
    if (ABMultiValueGetCount(phoneNumbers) > 0) {
        phone = (NSString*)ABMultiValueCopyValueAtIndex(phoneNumbers, 0);
    } else {
        phone = @"[None]";
    }
    _shareToTF.text = phone;
    CFRelease(phoneNumbers);
}

@end
