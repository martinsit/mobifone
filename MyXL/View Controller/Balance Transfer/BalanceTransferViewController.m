//
//  BalanceTransferViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/16/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BalanceTransferViewController.h"
#import "Constant.h"
#import <QuartzCore/QuartzCore.h>
#import "ContentView.h"
#import "SectionHeaderView.h"
#import "AXISnetCommon.h"
#import "DataManager.h"
#import "LabelValueModel.h"
#import "EncryptDecrypt.h"
#import "DetailReloadBalanceViewController.h"

#import "ActionSheetStringPicker.h"

#import "UnderLineLabel.h"

@interface BalanceTransferViewController ()

- (IBAction)performShowNominal:(id)sender;
- (void)transferBalance;
- (BOOL)validateInput;

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *otherAXISLabel;
@property (strong, nonatomic) IBOutlet UITextField *otherMSISDNTF;
@property (strong, nonatomic) IBOutlet UILabel *nominalLabel;
@property (strong, nonatomic) IBOutlet UIButton *nominalButton;
@property (strong, nonatomic) UIButton *submitButton;

@property (strong, nonatomic) UIActionSheet *nominalActionSheet;
@property (strong, nonatomic) UIPickerView *nominalPickerView;
@property (strong, nonatomic) NSArray *balanceTransferAmount;
@property (strong, nonatomic) NSArray *balanceTransferAmountLabel;
@property (readwrite) int rowIndex;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

@property (strong, nonatomic) IBOutlet UILabel *captchaLabel;
@property (strong, nonatomic) IBOutlet UIWebView *captchaWebView;
@property (strong, nonatomic) IBOutlet UnderLineLabel *recaptchaLabel;
@property (strong, nonatomic) IBOutlet UITextField *captchaTF;

// TODO : Hygiene
@property (nonatomic, retain) NSTimer *captchaTimer;

@end

@implementation BalanceTransferViewController

@synthesize nominalActionSheet;
@synthesize nominalPickerView;
@synthesize balanceTransferAmount;
@synthesize rowIndex;

@synthesize headerTitle = _headerTitle;
@synthesize headerIcon = _headerIcon;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

// TODO : Hygiene
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_captchaTimer invalidate];
}

- (void)viewWillAppear:(BOOL)animated {
    CGRect frame = _contentView.frame;
    
    //-------------//
    // Setup Label //
    //-------------//
    _otherAXISLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _otherAXISLabel.textColor = kDefaultTitleFontGrayColor;
    _otherAXISLabel.backgroundColor = [UIColor clearColor];
    _otherAXISLabel.text = [[Language get:@"other_axis_number" alter:nil] uppercaseString];
    
    _otherMSISDNTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _otherMSISDNTF.textColor = kDefaultPurpleColor;
    
    _nominalLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _nominalLabel.textColor = kDefaultTitleFontGrayColor;
    _nominalLabel.backgroundColor = [UIColor clearColor];
    _nominalLabel.text = [[Language get:@"nominal" alter:nil] uppercaseString];
    
    // Add by iNot 22 July 2013
    // Captcha
    _captchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _captchaLabel.textColor = kDefaultTitleFontGrayColor;
    NSString *text = [NSString stringWithFormat:@"%@*",[[Language get:@"captcha_code" alter:nil] uppercaseString]];
    _captchaLabel.text = text;
    
    _recaptchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _recaptchaLabel.textColor = kDefaultPurpleColor;
    _recaptchaLabel.text = [Language get:@"recaptcha" alter:nil];
    [_recaptchaLabel setShouldUnderline:YES];
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(refreshCaptcha)];
    [_recaptchaLabel setUserInteractionEnabled:YES];
    [_recaptchaLabel addGestureRecognizer:tapGesture3];
    [tapGesture3 release];
    
    _captchaTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _captchaTF.textColor = kDefaultPurpleColor;
    
    //****** Request on 5 September 2013, Hide Captcha ******
    _captchaLabel.hidden = YES;
    _recaptchaLabel.hidden = YES;
    _captchaWebView.hidden = YES;
    _captchaTF.hidden = YES;
    //*******************************************************
    
    //--------------//
    // Setup Button //
    //--------------//
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    UIButton *button = createButtonWithFrame(CGRectMake(_nominalButton.frame.origin.x,
                                                        _nominalButton.frame.origin.y + _nominalButton.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        30),
                                             [Language get:@"submit" alter:nil],
                                             buttonFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
    
    [button addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    _submitButton = button;
    [_contentView addSubview:_submitButton];
    
    CGRect newFrame = frame;
    newFrame.size.height = _submitButton.frame.origin.y + _submitButton.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:@"BALANCE TRANSFER"];
    
    _sectionHeaderView = headerView;
    _sectionHeaderView.icon = _headerIcon;
    _sectionHeaderView.title = _headerTitle;
    
    [_contentView addSubview:_sectionHeaderView];
    [_contentView sendSubviewToBack:_sectionHeaderView];
    [headerView release];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    DataManager *sharedData = [DataManager sharedInstance];
    self.balanceTransferAmount = [NSArray arrayWithArray:sharedData.balanceItemsData];
    
    rowIndex = 0;
    LabelValueModel *labelValueModel = [self.balanceTransferAmount objectAtIndex:rowIndex];
    [_nominalButton setTitle:labelValueModel.label forState:UIControlStateNormal];
    [_nominalButton setTitleColor:kDefaultPurpleColor forState:UIControlStateNormal];
    _nominalButton.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _nominalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _nominalButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    UIImage *backgroundImage = [[UIImage imageNamed:@"combo_bg_1.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0];
    [_nominalButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 200.0);
    
    NSMutableArray *labelArray = [[NSMutableArray alloc] init];
    for (LabelValueModel *labelValueModel in self.balanceTransferAmount) {
        [labelArray addObject:labelValueModel.label];
    }
    self.balanceTransferAmountLabel = [NSArray arrayWithArray:labelArray];
    [labelArray release];
    
    // Request Captcha
    [self refreshCaptcha];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.headerTitle = nil;
    self.headerIcon = nil;
}

- (void)dealloc
{
    [nominalActionSheet release];
    [nominalPickerView release];
    [balanceTransferAmount release];
    [_headerTitle release];
    _headerTitle = nil;
    [_headerIcon release];
    _headerIcon = nil;
    [super dealloc];
}

#pragma mark - Selector

- (BOOL)validateInput {
    BOOL result = NO;
    if ([_otherMSISDNTF.text length] > 0) {
        result = YES;
    }
    return result;
}

- (IBAction)performShowNominal:(id)sender {
    
    [_otherMSISDNTF resignFirstResponder];
    
    [ActionSheetStringPicker showPickerWithTitle:@""
                                            rows:self.balanceTransferAmountLabel
                                initialSelection:rowIndex
                                          target:self
                                   successAction:@selector(langWasSelected:element:)
                                    cancelAction:@selector(actionPickerCancelled:)
                                          origin:sender];
    /*
    UIActionSheet *nominalActionSheetTmp = [[UIActionSheet alloc] initWithTitle:@""
                                                                       delegate:self
                                                              cancelButtonTitle:nil
                                                         destructiveButtonTitle:nil
                                                              otherButtonTitles:nil];
    
    UIPickerView *thePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
    thePickerView.showsSelectionIndicator = TRUE;
    self.nominalPickerView = thePickerView;
    nominalPickerView.delegate = self;
    [nominalPickerView selectRow:rowIndex inComponent:0 animated:YES];
    [thePickerView release];
    
    UIToolbar *pickerToolbar;
    pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                               target:self
                                                                               action:@selector(actionCancel:)];
    [barItems addObject:cancelBtn];
    [cancelBtn release];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                               target:self
                                                                               action:nil];
    [barItems addObject:flexSpace];
    [flexSpace release];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                             target:self
                                                                             action:@selector(actionDone:)];
    [barItems addObject:doneBtn];
    [doneBtn release];
    [pickerToolbar setItems:barItems animated:YES];
    [barItems release];
    [nominalActionSheetTmp addSubview:pickerToolbar];
    [nominalActionSheetTmp addSubview:nominalPickerView];
    [pickerToolbar release];
    
    self.nominalActionSheet = nominalActionSheetTmp;
    [nominalActionSheetTmp release];
    
    [nominalActionSheet showInView:self.view];
    [nominalActionSheet setBounds:CGRectMake(0, 0, 320, 464)];*/
}

- (void)actionCancel:(id)sender {
    [nominalActionSheet dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)actionDone:(id)sender {
	[nominalActionSheet dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)performSubmit:(id)sender {
    
    [_otherMSISDNTF resignFirstResponder];
    [_captchaTF resignFirstResponder];
    
    BOOL validInput = [self validateInput];
    
    if (validInput) {
        LabelValueModel *labelValueModel = [self.balanceTransferAmount objectAtIndex:rowIndex];
        NSString *message = [NSString stringWithFormat:@"%@ %@ %@ %@ %@.\n%@",
                             [Language get:@"transfer_confirm_1" alter:nil],
                             labelValueModel.label,
                             [Language get:@"transfer_confirm_2" alter:nil],
                             _otherMSISDNTF.text,
                             [Language get:@"transfer_confirm_3" alter:nil],
                             [Language get:@"ask_continue" alter:nil]];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                              otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString], nil];
        alert.tag = 1;
        [alert show];
        [alert release];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"incomplete_input" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"ok" alter:nil]
                                              otherButtonTitles:nil];
        alert.tag = 100;
        [alert show];
        [alert release];
    }
}

- (void)transferBalance {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdnToEncrypted = [EncryptDecrypt doCipherForiOS7:_otherMSISDNTF.text action:kCCEncrypt withKey:saltKeyAPI];
    
    LabelValueModel *labelValueModel = [self.balanceTransferAmount objectAtIndex:rowIndex];
    
    //DataManager *sharedData = [DataManager sharedInstance];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = BALANCE_TRANSFER_REQ;
    /*
    [request balanceTransfer:msisdnToEncrypted
                  withAmount:labelValueModel.value
                 withCaptcha:_captchaTF.text
                     withCid:sharedData.cid];*/
    
    [request balanceTransfer:msisdnToEncrypted
                  withAmount:labelValueModel.value
                 withCaptcha:@""
                     withCid:@""];
}

- (void)refreshCaptcha {
    // TODO : Hygiene
    if(_captchaTimer != nil)
        [_captchaTimer invalidate];
    [self scheduleTimerForCaptcha];
    
    NSString *captchaURL = generateCaptchaURL();
    NSURL *captcha = [NSURL URLWithString:captchaURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:captcha];
    [_captchaWebView loadRequest:request];
    [request release];
}

// TODO : Hygiene
// Timer for captcha refresh
-(void)scheduleTimerForCaptcha
{
    _captchaTimer = nil;
    _captchaTimer = [NSTimer scheduledTimerWithTimeInterval:CAPTCHA_REFRESH_TIME
                                                     target: self
                                                   selector:@selector(refreshCaptcha)
                                                   userInfo: nil repeats:YES];
    
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer:_captchaTimer forMode: NSDefaultRunLoopMode];
//    [t release];
}

#pragma mark - Implementation ActionSheetPicker.h

- (void)langWasSelected:(NSNumber *)selectedIndex element:(id)element {
    rowIndex = [selectedIndex intValue];
    LabelValueModel *labelValueModel = [self.balanceTransferAmount objectAtIndex:rowIndex];
    [_nominalButton setTitle:labelValueModel.label forState:UIControlStateNormal];
}

- (void)actionPickerCancelled:(id)sender {
    //NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	rowIndex = row;
    LabelValueModel *labelValueModel = [self.balanceTransferAmount objectAtIndex:rowIndex];
    [_nominalButton setTitle:labelValueModel.label forState:UIControlStateNormal];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    LabelValueModel *labelValueModel = [self.balanceTransferAmount objectAtIndex:row];
    return labelValueModel.label;
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return [self.balanceTransferAmount count];
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    //[_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    //CGFloat targetY = -42 + textField.frame.origin.y;
    CGFloat targetY = -72 + textField.frame.origin.y;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 16);
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            [self transferBalance];
        }
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == BALANCE_TRANSFER_REQ) {
            DetailReloadBalanceViewController *detailReloadBalanceViewController = [[DetailReloadBalanceViewController alloc] initWithNibName:@"DetailReloadBalanceViewController" bundle:nil];
            detailReloadBalanceViewController.headerTitle = _headerTitle;
            detailReloadBalanceViewController.headerIcon = _headerIcon;
            
            DataManager *sharedData = [DataManager sharedInstance];
            detailReloadBalanceViewController.msisdn = sharedData.profileData.msisdn;
            detailReloadBalanceViewController.nominal = sharedData.balanceTransferData.msisdnTo;
            detailReloadBalanceViewController.balanceNew = sharedData.balanceTransferData.amount;
            detailReloadBalanceViewController.activeDateNew = sharedData.balanceTransferData.charge;
            
            detailReloadBalanceViewController.isBalanceTransfer = YES;
            
            [self.navigationController pushViewController:detailReloadBalanceViewController animated:YES];
            detailReloadBalanceViewController = nil;
            [detailReloadBalanceViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
