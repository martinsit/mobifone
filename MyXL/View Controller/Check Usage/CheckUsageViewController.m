//
//  CheckUsageViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CheckUsageViewController.h"

#import "Constant.h"
#import <QuartzCore/QuartzCore.h>
#import "ContentView.h"
#import "SectionHeaderView.h"
//#import "DataManager.h"
#import "AXISnetCommon.h"
#import "HumanReadableDataSizeHelper.h"

@interface CheckUsageViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UIView *barView;

@property (strong, nonatomic) IBOutlet UILabel *packageTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *packageValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *packageTypeTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *packageTypeValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *usageTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *usageValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *validityTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *validityValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *infoLabel;
@property (strong, nonatomic) IBOutlet UILabel *remainingLabel;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

@end

@implementation CheckUsageViewController

@synthesize packageType;
@synthesize checkUsageModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    
    //-------------//
    // Setup Label //
    //-------------//
    _packageTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _packageTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _packageTitleLabel.text = [[Language get:@"active_package" alter:nil] uppercaseString];
    
    _packageValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _packageValueLabel.textColor = kDefaultPurpleColor;
    
    _packageTypeTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _packageTypeTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _packageTypeTitleLabel.text = [[Language get:@"type" alter:nil] uppercaseString];
    
    _packageTypeValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _packageTypeValueLabel.textColor = kDefaultPurpleColor;
    
    _usageTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _usageTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _usageTitleLabel.text = [[Language get:@"usage" alter:nil] uppercaseString];
    
    _usageValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _usageValueLabel.textColor = kDefaultPurpleColor;
    
    _validityTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _validityTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _validityTitleLabel.text = [[Language get:@"validity" alter:nil] uppercaseString];
    
    _validityValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _validityValueLabel.textColor = kDefaultPurpleColor;
    
    _infoLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _infoLabel.textColor = kDefaultPurpleColor;
    
    _remainingLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _remainingLabel.textColor = kDefaultPurpleColor;
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _infoLabel.frame.origin.y + _infoLabel.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:@"RELOAD BALANCE"];
    _sectionHeaderView = headerView;
    [_contentView addSubview:_sectionHeaderView];
    [_contentView sendSubviewToBack:_sectionHeaderView];
    [headerView release];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    //--
    
    _sectionHeaderView.icon = self.headerIconMaster;
    _sectionHeaderView.title = self.headerTitleMaster;
    
    _packageValueLabel.text = checkUsageModel.packageName;
    
    NSString *type = convertPackageType(checkUsageModel.packageType);
    _packageTypeValueLabel.text = type;
    
    unsigned long long ullvalue = strtoull([checkUsageModel.usage UTF8String], NULL, 0);
    NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
    NSString *strUsage = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:usageNumber];
    _usageValueLabel.text = strUsage;
    
    _validityValueLabel.text = checkUsageModel.endDate;
    
    _infoLabel.text = checkUsageModel.info;
    
    _remainingLabel.text = checkUsageModel.remainingLabel;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [checkUsageModel release];
    [super dealloc];
}

@end
