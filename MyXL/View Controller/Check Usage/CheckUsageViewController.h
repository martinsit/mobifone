//
//  CheckUsageViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "CheckUsageModel.h"

typedef enum {
    PPU = 0,
    QUOTA,
    UNLIMITED,
    FUP
}PackageType;

@interface CheckUsageViewController : BasicViewController {
    PackageType packageType;
    CheckUsageModel *checkUsageModel;
}

@property (readwrite) PackageType packageType;
@property (nonatomic, retain) CheckUsageModel *checkUsageModel;

@end
