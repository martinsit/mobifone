//
//  RequestPasswordResultViewController.h
//  MyXL
//
//  Created by tyegah on 2/11/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface RequestPasswordResultViewController : BasicViewController
@property (nonatomic) BOOL isOffNet;
@end
