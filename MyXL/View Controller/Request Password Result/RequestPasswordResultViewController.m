//
//  RequestPasswordResultViewController.m
//  MyXL
//
//  Created by tyegah on 2/11/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "RequestPasswordResultViewController.h"
#import "AppDelegate.h"
#import "UIDeviceHardware.h"

@interface RequestPasswordResultViewController ()
@property (retain, nonatomic) IBOutlet UIView *contentView;
//@property (retain, nonatomic) IBOutlet UIImageView *bgImgView;
@property (retain, nonatomic) IBOutlet UILabel *lblDisclaimerMessage;
@end

@implementation RequestPasswordResultViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    /*
    NSString *platform = [UIDeviceHardware platformString];
    
    if ([platform isEqualToString:@"iPhone 5"] || [platform isEqualToString:@"iPhone 5 (GSM+CDMA)"] ||
        [platform isEqualToString:@"iPhone 5c"] || [platform isEqualToString:@"iPhone 5s"]) {
        _bgImgView.image = [UIImage imageNamed:@"old-xl-splash-iPhone5-640x1136.jpg"];
    }
    else {
        if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            _bgImgView.image = [UIImage imageNamed:@"old-xl-splash-iPad-1024x768.jpg"];
        }
        else {
            _bgImgView.image = [UIImage imageNamed:@"old-xl-splash-iPhone4-640x960.jpg"];
        }
    }*/
    
    self.isBackToRoot = YES;
    
    self.contentView.backgroundColor = colorWithHexString(@"#173358");
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNavigationBar];
    [self setupLabel];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    // Reset this variable to empty string again
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.tempMsisdn = @"";
}

-(void)setupNavigationBar
{
    //---------------------//
    // Show Navigation Bar //
    //---------------------//
    AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[self.navigationController navigationBar];
    navBar.hidden = NO;
    navBar.showDeviceLabel = YES;
    navBar.showMsisdnLabel = NO;
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *documentsDirectory = applicationDocumentsDirectory();
    NSString *logoName = @"";
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else if ([sharedData.profileData.telcoOperator isEqualToString:@"AXIS"]) {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    else {
        logoName = XL_LOGO_RETINA_NAME;
    }
    NSString *mediaPath = [documentsDirectory stringByAppendingPathComponent:logoName];
    [navBar updateLogo:mediaPath];
}

-(void)setupLabel
{
    if(_isOffNet)
    {
        if(IS_IPAD)
        {
            _lblDisclaimerMessage.hidden = YES;
//            _lblDisclaimerMessage.numberOfLines = 0;
//            _lblDisclaimerMessage.lineBreakMode = UILineBreakModeWordWrap;
//            _lblDisclaimerMessage.text = [Language get:@"disclaimer_message" alter:nil];
            [self setupIpadDisclaimerView];
        }
    }
    else
    {
        _lblDisclaimerMessage.numberOfLines = 0;
        _lblDisclaimerMessage.lineBreakMode = UILineBreakModeWordWrap;
        _lblDisclaimerMessage.text = [Language get:@"request_password_success" alter:nil];
    }
    
    _lblDisclaimerMessage.font = [UIFont fontWithName:kDefaultFont size:24];
    _lblDisclaimerMessage.textColor = kDefaultWhiteColor;
}

-(void)setupIpadDisclaimerView
{
    NSString *disclaimerMessage = [Language get:@"ipad_disclaimer_message" alter:nil];
    UILabel *lblDisclaimer = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, CGRectGetWidth(self.view.frame) - 40, 0)];
    lblDisclaimer.font = [UIFont fontWithName:kDefaultFont size:18];
    lblDisclaimer.textColor = kDefaultWhiteColor;
    
    CGSize labelSize = calculateLabelSize(CGRectGetWidth(self.view.frame) - 40, disclaimerMessage, lblDisclaimer.font);
    CGRect lblFrame = CGRectIntegral(CGRectMake(20, 20, labelSize.width, labelSize.height));
    
    lblDisclaimer.frame = lblFrame;
    lblDisclaimer.numberOfLines = 0;
    lblDisclaimer.text = disclaimerMessage;
    
    [_contentView addSubview:lblDisclaimer];
    [lblDisclaimer release];
    
    CGFloat viewWidth = CGRectGetWidth(self.view.frame) - 40;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat viewHeight = screenHeight - (20 *4) - labelSize.height - 50;
    UIView *stepView1 = [[UIView alloc] initWithFrame:CGRectIntegral(CGRectMake(20, CGRectGetMaxY(lblDisclaimer.frame) + 20, viewWidth, viewHeight))];
    stepView1.backgroundColor = [UIColor clearColor];
    UIImageView *imgStep1 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, viewWidth - (10+5), viewHeight - (10*2))];
    imgStep1.contentMode = UIViewContentModeScaleAspectFit;
    int lang = [Language getCurrentLanguage];
    if (lang == 0) {
        imgStep1.image = [UIImage imageNamed:@"iPad_EN.png"];
    }
    else {
        imgStep1.image = [UIImage imageNamed:@"iPad_ID.png"];
    }
    
    [stepView1 addSubview:imgStep1];
    
    /*
    CGFloat viewWidth = (CGRectGetWidth(self.view.frame)-40 - (20*2)) /3; // -> 20*2 (padding between images)
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat viewHeight = screenHeight - (20 *4) - labelSize.height - 50;
    UIView *stepView1 = [[UIView alloc] initWithFrame:CGRectIntegral(CGRectMake(20, CGRectGetMaxY(lblDisclaimer.frame) +20, viewWidth, viewHeight))];
    UIView *stepView2 = [[UIView alloc] initWithFrame:CGRectIntegral(CGRectMake(CGRectGetMaxX(stepView1.frame) + 20, CGRectGetMinY(stepView1.frame), viewWidth, viewHeight))];
    UIView *stepView3 = [[UIView alloc] initWithFrame:CGRectIntegral(CGRectMake(CGRectGetMaxX(stepView2.frame) + 20, CGRectGetMinY(stepView1.frame), viewWidth, viewHeight))];
    
    stepView1.backgroundColor = [UIColor clearColor];
    stepView2.backgroundColor = [UIColor clearColor];
    stepView3.backgroundColor = [UIColor clearColor];
    
    CGFloat numberViewSize = 30;
    CGFloat hintViewWidth = viewWidth - (25*2);
    CGFloat hintViewHeight = 50;
    UIFont *hintFont = [UIFont fontWithName:kDefaultFontLight size:14];
    
    UIView *hintView1 = [[UIView alloc] initWithFrame:CGRectMake(25, 25, hintViewWidth, hintViewHeight)];
    hintView1.backgroundColor = kDefaultWhiteColor;
    CGSize lblHintSize1 = calculateLabelSize(hintViewWidth -16, [Language get:@"ipad_disclaimer_step1" alter:nil], hintFont);
    UILabel *lblHint1 = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, lblHintSize1.width, lblHintSize1.height)];
    lblHint1.font = hintFont;
    lblHint1.numberOfLines = 0;
    lblHint1.text = [Language get:@"ipad_disclaimer_step1" alter:nil];
    lblHint1.textAlignment = NSTextAlignmentCenter;
    [hintView1 addSubview:lblHint1];
    [stepView1 addSubview:hintView1];
    
    UIView *numberView1 = [[UIView alloc] initWithFrame:CGRectMake(10, 10, numberViewSize, numberViewSize)];
    numberView1.backgroundColor = [UIColor yellowColor];
    UILabel *numberLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, numberViewSize, numberViewSize)];
    numberLbl1.text = @"1";
    numberLbl1.textAlignment = NSTextAlignmentCenter;
    [numberView1 addSubview:numberLbl1];
    [stepView1 addSubview:numberView1];
    
    UIImageView *imgStep1 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(hintView1.frame), CGRectGetMaxY(hintView1.frame) + 40, CGRectGetWidth(hintView1.frame), CGRectGetHeight(stepView1.frame) - CGRectGetMaxY(hintView1.frame) - 40 - 20)];
    imgStep1.contentMode = UIViewContentModeScaleToFill;
    imgStep1.image = [UIImage imageNamed:@"ipad_disclaimer1.jpg"];
    
    [stepView1 addSubview:imgStep1];
    
    UIView *hintView2 = [[UIView alloc] initWithFrame:CGRectMake(25, 25, hintViewWidth, hintViewHeight)];
    hintView2.backgroundColor = kDefaultWhiteColor;
    UILabel *lblHint2 = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, lblHintSize1.width, lblHintSize1.height)];
    lblHint2.font = hintFont;
    lblHint2.numberOfLines = 0;
    lblHint2.text = [Language get:@"ipad_disclaimer_step2" alter:nil];
    lblHint2.textAlignment = NSTextAlignmentCenter;
    [hintView2 addSubview:lblHint2];
    [stepView2 addSubview:hintView2];
    
    UIView *numberView2 = [[UIView alloc] initWithFrame:CGRectMake(10, 10, numberViewSize, numberViewSize)];
    numberView2.backgroundColor = [UIColor yellowColor];
    UILabel *numberLbl2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, numberViewSize, numberViewSize)];
    numberLbl2.text = @"2";
    numberLbl2.textAlignment = NSTextAlignmentCenter;
    [numberView2 addSubview:numberLbl2];
    [stepView2 addSubview:numberView2];
    
    UIImageView *imgStep2 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(hintView1.frame), CGRectGetMaxY(hintView1.frame) + 40, CGRectGetWidth(hintView1.frame), CGRectGetHeight(stepView1.frame) - CGRectGetMaxY(hintView1.frame) - 40 - 20)];
    imgStep2.contentMode = UIViewContentModeScaleToFill;
    imgStep2.image = [UIImage imageNamed:@"ipad_disclaimer2.jpg"];
    
    [stepView2 addSubview:imgStep2];
    
    UIView *hintView3 = [[UIView alloc] initWithFrame:CGRectMake(25, 25, hintViewWidth, hintViewHeight)];
    hintView3.backgroundColor = kDefaultWhiteColor;
    UILabel *lblHint3 = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, lblHintSize1.width, lblHintSize1.height)];
    lblHint3.font = hintFont;
    lblHint3.numberOfLines = 0;
    lblHint3.text = [Language get:@"ipad_disclaimer_step3" alter:nil];
    lblHint3.textAlignment = NSTextAlignmentCenter;
    [hintView3 addSubview:lblHint3];
    [stepView3 addSubview:hintView3];
    
    UIView *numberView3 = [[UIView alloc] initWithFrame:CGRectMake(10, 10, numberViewSize, numberViewSize)];
    numberView3.backgroundColor = [UIColor yellowColor];
    UILabel *numberLbl3 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, numberViewSize, numberViewSize)];
    numberLbl3.text = @"3";
    numberLbl3.textAlignment = NSTextAlignmentCenter;
    [numberView3 addSubview:numberLbl3];
    [stepView3 addSubview:numberView3];
    
    UIImageView *imgStep3 = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(hintView1.frame), CGRectGetMaxY(hintView1.frame) + 40, CGRectGetWidth(hintView1.frame), CGRectGetHeight(stepView1.frame) - CGRectGetMaxY(hintView1.frame) - 40 - 20)];
    imgStep3.contentMode = UIViewContentModeScaleToFill;
    imgStep3.image = [UIImage imageNamed:@"ipad_disclaimer1.jpg"];
    
    [stepView3 addSubview:imgStep3];*/
    
    [_contentView addSubview:stepView1];
    //[_contentView addSubview:stepView2];
    //[_contentView addSubview:stepView3];
    [stepView1 release];
    //[stepView2 release];
    //[stepView3 release];
    
    stepView1 = nil;
    //stepView2 = nil;
    //stepView3 = nil;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_contentView release];
    //[_bgImgView release];
    [_lblDisclaimerMessage release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setContentView:nil];
    //[self setBgImgView:nil];
    [self setLblDisclaimerMessage:nil];
    [super viewDidUnload];
}
@end
