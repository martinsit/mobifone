//
//  CheckBalanceViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface CheckBalanceViewController : BasicViewController {
    NSString *_headerTitle;
    NSString *_headerIcon;
}

@property (nonatomic, retain) NSString *headerTitle;
@property (nonatomic, retain) NSString *headerIcon;

@end
