//
//  CheckBalanceViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CheckBalanceViewController.h"

#import "FooterView.h"
#import "BalanceCell.h"
#import "BorderCellBackground.h"

#import "SectionHeaderView.h"
#import "SectionFooterView.h"

#import "DataManager.h"
#import "AXISnetCommon.h"
#import "Constant.h"
#import "EncryptDecrypt.h"

@interface CheckBalanceViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@end

@implementation CheckBalanceViewController

@synthesize headerTitle = _headerTitle;
@synthesize headerIcon = _headerIcon;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
    FooterView *footer = [[[FooterView alloc] initWithFrame:CGRectMake(0.0, 0.0, _theTableView.bounds.size.width, 50.0)] autorelease];
    _theTableView.tableFooterView = footer;*/
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.headerTitle = nil;
    self.headerIcon = nil;
}

- (void)dealloc {
    [_headerTitle release];
    _headerTitle = nil;
    [_headerIcon release];
    _headerIcon = nil;
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierBalance = @"Balance";
    BalanceCell *cell = (BalanceCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierBalance];
    
    if (cell == nil) {
        cell = [[BalanceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierBalance];
        
        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
    }
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    cell.msisdnTitleLabel.text = [[Language get:@"your_axis_number" alter:nil] uppercaseString];
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.checkBalanceData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    cell.msisdnValueLabel.text = msisdn;
    
    cell.statusTitleLabel.text = [[Language get:@"status" alter:nil] uppercaseString];
    if ([sharedData.checkBalanceData.accountStatus isEqualToString:@"1"]) {
        cell.statusValueLabel.text = [Language get:@"active" alter:nil];
        cell.validityValueLabel.text = changeDateFormat(sharedData.checkBalanceData.activeStopDate);
    }
    else {
        cell.statusValueLabel.text = [Language get:@"grace_period" alter:nil];
        cell.validityValueLabel.text = changeDateFormat(sharedData.checkBalanceData.suspendStopDate);
    }
    
    cell.balanceTitleLabel.text = [[Language get:@"your_balance" alter:nil] uppercaseString];
    NSString *balance = addThousandsSeparator(sharedData.checkBalanceData.balance, sharedData.profileData.language);
    cell.balanceValueLabel.text = balance;
    
    cell.validityTitleLabel.text = [[Language get:@"validity" alter:nil] uppercaseString];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return _headerTitle;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90.0;
}

#pragma mark - Table view delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:_headerIcon
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:_headerIcon
                                                isFirstSection:NO] autorelease];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 80.0;
}

@end
