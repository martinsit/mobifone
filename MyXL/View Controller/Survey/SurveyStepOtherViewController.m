//
//  SurveyStepOtherViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/30/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "SurveyStepOtherViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

#import "LabelValueModel.h"
#import "DataManager.h"
#import "MenuModel.h"
#import "SubMenuViewController.h"

@interface SurveyStepOtherViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *questionLabel;

@property (readwrite) int counter;

- (void)createRadioView;
- (void)createCheckBoxView;
- (void)performPrevious;
- (void)performNext;
- (void)performSubmit;

@end

@implementation SurveyStepOtherViewController

@synthesize menuModel = _menuModel;
@synthesize currentPage;
@synthesize surveyModel;
@synthesize radioButtonSetController;
@synthesize checkboxSetController;
@synthesize prevAnswer;
@synthesize currentAnswer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    
    _counter = 0;
    
    //-------------//
    // Setup Label //
    //-------------//
    _questionLabel.backgroundColor = [UIColor clearColor];
    _questionLabel.numberOfLines = 0;
    _questionLabel.lineBreakMode = UILineBreakModeWordWrap;
    _questionLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _questionLabel.textColor = kDefaultPurpleColor;
    _questionLabel.text = surveyModel.question;
    
    CGSize size = calculateExpectedSize(_questionLabel, surveyModel.question);
    CGRect labelFrame = _questionLabel.frame;
    labelFrame.size.height = size.height;
    _questionLabel.frame = labelFrame;
    
    if ([surveyModel.typeOption isEqualToString:@"radio"]) {
        [self createRadioView];
    }
    if ([surveyModel.typeOption isEqualToString:@"checkbox"]) {
        [self createCheckBoxView];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_menuModel release];
    _menuModel = nil;
    
    [surveyModel release];
    [prevAnswer release];
    [currentAnswer release];
    [super dealloc];
}

#pragma mark - Selector

- (void)createRadioView {
    NSMutableArray *buttons = [[NSMutableArray alloc] init];
    CGFloat startY = _questionLabel.frame.origin.y + _questionLabel.frame.size.height + kDefaultComponentPadding;
    CGFloat lastY = 0.0;
    for (int i = 0; i < [surveyModel.option count]; i++) {
        
        LabelValueModel *labelValue = [surveyModel.option objectAtIndex:i];
        
        // Button
        UIButton* aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [aButton setTag:i];
        CGRect frame = CGRectMake(_questionLabel.frame.origin.x, startY, kDefaultButtonHeight, kDefaultButtonHeight);
        aButton.frame = frame;
        
        [aButton setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
        [aButton setImage:[UIImage imageNamed:@"radio-on.png"] forState:UIControlStateSelected];
        
        //aButton.backgroundColor = [UIColor clearColor];
        
        [_contentView addSubview:aButton];
        [buttons addObject:aButton];
        
        // Label
        CGFloat xLabel = aButton.frame.origin.x + aButton.frame.size.width + kDefaultComponentPadding;
        CGFloat width = _contentView.frame.size.width - (kDefaultPadding*2) - aButton.frame.size.width;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xLabel, startY, width, kDefaultButtonHeight)];
        
        label.backgroundColor = [UIColor clearColor];
        label.numberOfLines = 0;
        label.lineBreakMode = UILineBreakModeWordWrap;
        label.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        label.textColor = kDefaultPurpleColor;
        label.text = labelValue.label;
        
        CGSize size = calculateExpectedSize(label, labelValue.label);
        if (size.height < kDefaultButtonHeight) {
            frame = label.frame;
            frame.size.height = kDefaultButtonHeight;
        }
        else {
            frame = label.frame;
            frame.size.height = size.height;
        }
        
        label.frame = frame;
        
        [_contentView addSubview:label];
        
        startY = label.frame.origin.y + label.frame.size.height + kDefaultComponentPadding;
        lastY = label.frame.origin.y + label.frame.size.height;
    }
    
    // Instantiate your GSRadioButtonSetController object
    self.radioButtonSetController = [[GSRadioButtonSetController alloc] init];
    
    // Set its delegate to your view controller
    self.radioButtonSetController.delegate = self;
    
    // Set its buttons property to an array of buttons that you've
    // created previously.
    NSArray *buttonArray = [buttons copy];
    self.radioButtonSetController.buttons = buttonArray;
    
    [buttons release];
    
    //-----------------//
    // Previous Button //
    //-----------------//
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize];
    
    UIButton *prevButton = createButtonWithFrame(CGRectMake(_questionLabel.frame.origin.x,
                                                        startY,
                                                        100,
                                                        kDefaultButtonHeight),
                                             @"f",
                                             buttonFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
    
    [prevButton addTarget:self action:@selector(performPrevious) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:prevButton];
    
    //-------------//
    // Next Button //
    //-------------//
    UIButton *nextButton = createButtonWithFrame(CGRectMake(_questionLabel.frame.origin.x + _questionLabel.frame.size.width - 100,
                                                            startY,
                                                            100,
                                                            kDefaultButtonHeight),
                                                 @"d",
                                                 buttonFont,
                                                 kDefaultBaseColor,
                                                 kDefaultPinkColor,
                                                 kDefaultPurpleColor);
    
    [nextButton addTarget:self action:@selector(performNext) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:nextButton];
    
    startY = nextButton.frame.origin.y + nextButton.frame.size.height + kDefaultComponentPadding;
    lastY = nextButton.frame.origin.y + nextButton.frame.size.height;
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    
    newFrame.size.height = lastY + kDefaultPadding;
    _contentView.frame = newFrame;
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:_menuModel.menuName];
    headerView.icon = icon(_menuModel.menuId);
    headerView.titleLabel.font = [UIFont fontWithName:@"OpenSans-Extrabold" size:18.0];
    [_contentView addSubview:headerView];
    [_contentView sendSubviewToBack:headerView];
    [headerView release];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 50.0);
}

- (void)createCheckBoxView {
    NSMutableArray *buttons = [[NSMutableArray alloc] init];
    CGFloat startY = _questionLabel.frame.origin.y + _questionLabel.frame.size.height + kDefaultComponentPadding;
    CGFloat lastY = 0.0;
    for (int i = 0; i < [surveyModel.option count]; i++) {
        
        LabelValueModel *labelValue = [surveyModel.option objectAtIndex:i];
        
        // Button
        UIButton* aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [aButton setTag:i];
        CGRect frame = CGRectMake(_questionLabel.frame.origin.x, startY, kDefaultButtonHeight, kDefaultButtonHeight);
        aButton.frame = frame;
        
        [aButton setImage:[UIImage imageNamed:@"check-off.png"] forState:UIControlStateNormal];
        [aButton setImage:[UIImage imageNamed:@"check-on.png"] forState:UIControlStateSelected];
        
        //aButton.backgroundColor = [UIColor clearColor];
        
        [_contentView addSubview:aButton];
        [buttons addObject:aButton];
        
        // Label
        CGFloat xLabel = aButton.frame.origin.x + aButton.frame.size.width + kDefaultComponentPadding;
        CGFloat width = _contentView.frame.size.width - (kDefaultPadding*2) - aButton.frame.size.width;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xLabel, startY, width, kDefaultButtonHeight)];
        
        label.backgroundColor = [UIColor clearColor];
        label.numberOfLines = 0;
        label.lineBreakMode = UILineBreakModeWordWrap;
        label.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        label.textColor = kDefaultPurpleColor;
        label.text = labelValue.label;
        
        CGSize size = calculateExpectedSize(label, labelValue.label);
        if (size.height < kDefaultButtonHeight) {
            frame = label.frame;
            frame.size.height = kDefaultButtonHeight;
        }
        else {
            frame = label.frame;
            frame.size.height = size.height;
        }
        
        label.frame = frame;
        
        [_contentView addSubview:label];
        
        startY = label.frame.origin.y + label.frame.size.height + kDefaultComponentPadding;
        lastY = label.frame.origin.y + label.frame.size.height;
    }
    
    // Instantiate your GSRadioButtonSetController object
    self.checkboxSetController = [[CheckboxSetController alloc] init];
    
    // Set its delegate to your view controller
    self.checkboxSetController.delegate = self;
    
    // Set its buttons property to an array of buttons that you've
    // created previously.
    NSArray *buttonArray = [buttons copy];
    self.checkboxSetController.buttons = buttonArray;
    
    [buttons release];
    
    //-----------------//
    // Previous Button //
    //-----------------//
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize];
    
    UIButton *prevButton = createButtonWithFrame(CGRectMake(_questionLabel.frame.origin.x,
                                                            startY,
                                                            100,
                                                            kDefaultButtonHeight),
                                                 @"f",
                                                 buttonFont,
                                                 kDefaultBaseColor,
                                                 kDefaultPinkColor,
                                                 kDefaultPurpleColor);
    
    [prevButton addTarget:self action:@selector(performPrevious) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:prevButton];
    
    //-------------//
    // Next Button //
    //-------------//
    UIButton *nextButton = createButtonWithFrame(CGRectMake(_questionLabel.frame.origin.x + _questionLabel.frame.size.width - 100,
                                                            startY,
                                                            100,
                                                            kDefaultButtonHeight),
                                                 @"d",
                                                 buttonFont,
                                                 kDefaultBaseColor,
                                                 kDefaultPinkColor,
                                                 kDefaultPurpleColor);
    
    [nextButton addTarget:self action:@selector(performNext) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:nextButton];
    
    startY = nextButton.frame.origin.y + nextButton.frame.size.height + kDefaultComponentPadding;
    lastY = nextButton.frame.origin.y + nextButton.frame.size.height;
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    
    newFrame.size.height = lastY + kDefaultPadding;
    _contentView.frame = newFrame;
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:_menuModel.menuName];
    headerView.icon = icon(_menuModel.menuId);
    headerView.titleLabel.font = [UIFont fontWithName:@"OpenSans-Extrabold" size:18.0];
    [_contentView addSubview:headerView];
    [_contentView sendSubviewToBack:headerView];
    [headerView release];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 50.0);
}

- (void)performPrevious {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)performNext {
    
    BOOL valid = validateChoice(surveyModel, _counter);
    
    if (valid) {
        DataManager *sharedData = [DataManager sharedInstance];
        
        if ((self.currentPage + 1) < [sharedData.surveyData count]) {
            SurveyStepOtherViewController *surveyStepOtherViewController = [[SurveyStepOtherViewController alloc] initWithNibName:@"SurveyStepOtherViewController" bundle:nil];
            
            surveyStepOtherViewController.currentPage = self.currentPage + 1;
            surveyStepOtherViewController.prevAnswer = self.currentAnswer;
            surveyStepOtherViewController.menuModel = _menuModel;
            
            //NSLog(@"answer = %@",surveyStepOtherViewController.prevAnswer);
            
            SurveyModel *surveyModelTmp = [sharedData.surveyData objectAtIndex:surveyStepOtherViewController.currentPage];
            
            surveyStepOtherViewController.surveyModel = surveyModelTmp;
            
            [self.navigationController pushViewController:surveyStepOtherViewController animated:YES];
            surveyStepOtherViewController = nil;
            [surveyStepOtherViewController release];
        }
        else {
            //NSLog(@"Submit");
            [self performSubmit];
        }
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:surveyModel.validationMsg
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"ok" alter:nil]
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)performSubmit {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = SURVEY_SUBMIT_REQ;
    [request submitSurvey:self.currentAnswer];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == SURVEY_SUBMIT_REQ) {
            
            DataManager *dataManager = [DataManager sharedInstance];
            
            for (int i=0; i<[dataManager.recommendedData count]; i++) {
                MenuModel *recommendedPackageModel = [dataManager.recommendedData objectAtIndex:i];
                recommendedPackageModel.groupId        = _menuModel.menuId;
                recommendedPackageModel.groupName      = _menuModel.menuName;
            }
            
            SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
            subMenuVC.parentId = _menuModel.menuId;
            subMenuVC.groupDesc = _menuModel.desc;
            //NSLog(@"subMenuVC.groupDesc = %@",subMenuVC.groupDesc);
            subMenuVC.recommendedPackageModel = _menuModel;
            
            [self.navigationController pushViewController:subMenuVC animated:YES];
            subMenuVC = nil;
            [subMenuVC release];
        }
    }
    else {
        
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - GSRadioButtonSetController delegate methods

- (void)radioButtonSetController:(GSRadioButtonSetController *)controller
          didSelectButtonAtIndex:(NSUInteger)selectedIndex
{
    // Handle button selection here
    //NSLog(@"Someone just selected button %u!", selectedIndex);
    
    if (_counter == 0) {
        _counter++;
    }
    
    LabelValueModel *labelValue = [surveyModel.option objectAtIndex:selectedIndex];
    
    self.currentAnswer = labelValue.value;
    
    if ([self.prevAnswer length] > 0) {
        self.currentAnswer = [NSString stringWithFormat:@"%@|%@",self.prevAnswer,self.currentAnswer];
    }
    
    //NSLog(@"self.currentAnswer = %@",self.currentAnswer);
}

#pragma mark - CheckboxSetControllerDelegate delegate methods

- (void)checkboxSetController:(CheckboxSetController *)controller
             didSelectButtons:(NSArray*)selectedButtons {
    
    _counter = [selectedButtons count];
    
    NSMutableArray *answers = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [selectedButtons count]; i++) {
        //NSLog(@"index = %@",[selectedButtons objectAtIndex:i]);
        NSNumber *answerItem = [selectedButtons objectAtIndex:i];
        LabelValueModel *labelValue = [surveyModel.option objectAtIndex:[answerItem intValue]];
        [answers addObject:labelValue.value];
    }
    
    self.currentAnswer = [answers componentsJoinedByString:@","];
    
    if ([self.prevAnswer length] > 0) {
        self.currentAnswer = [NSString stringWithFormat:@"%@|%@",self.prevAnswer,self.currentAnswer];
    }
    
    //NSLog(@"self.currentAnswer = %@",self.currentAnswer);
}

@end
