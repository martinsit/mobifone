//
//  SurveyStepOneViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AXISnetRequest.h"
#import "MenuModel.h"

@interface SurveyStepOneViewController : BasicViewController <AXISnetRequestDelegate> {
    MenuModel *_menuModel;
}

@property (nonatomic, retain) MenuModel *menuModel;

@end
