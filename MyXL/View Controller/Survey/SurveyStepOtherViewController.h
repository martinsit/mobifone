//
//  SurveyStepOtherViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/30/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "SurveyModel.h"
#import "GSRadioButtonSetController.h"
#import "CheckboxSetController.h"
#import "AXISnetRequest.h"
#import "MenuModel.h"

@interface SurveyStepOtherViewController : BasicViewController <GSRadioButtonSetControllerDelegate, CheckboxSetControllerDelegate, AXISnetRequestDelegate> {
    MenuModel *_menuModel;
    int currentPage;
    SurveyModel *surveyModel;
    NSString *prevAnswer;
    NSString *currentAnswer;
}

@property (nonatomic, retain) MenuModel *menuModel;
@property (readwrite) int currentPage;
@property (nonatomic, retain) SurveyModel *surveyModel;
@property (nonatomic, strong) GSRadioButtonSetController *radioButtonSetController;
@property (nonatomic, strong) CheckboxSetController *checkboxSetController;
@property (nonatomic, retain) NSString *prevAnswer;
@property (nonatomic, retain) NSString *currentAnswer;

@end
