//
//  SurveyStepOneViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/29/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "SurveyStepOneViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>
#import "DataManager.h"
#import "SurveyStepOtherViewController.h"
#import "SurveyModel.h"

@interface SurveyStepOneViewController ()

@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *descLabel;
@property (strong, nonatomic) UIButton *startButton;

- (void)performStart;

@end

@implementation SurveyStepOneViewController

@synthesize menuModel = _menuModel;

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    //-------------//
    // Setup Label //
    //-------------//
    _descLabel.backgroundColor = [UIColor clearColor];
    _descLabel.numberOfLines = 0;
    _descLabel.lineBreakMode = UILineBreakModeWordWrap;
    _descLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _descLabel.textColor = kDefaultPurpleColor;
    _descLabel.text = _menuModel.topDesc;
    
    CGSize size = calculateExpectedSize(_descLabel, _menuModel.topDesc);
    CGRect labelFrame = _descLabel.frame;
    labelFrame.size.height = size.height;
    _descLabel.frame = labelFrame;
    
    //--------------//
    // Setup Button //
    //--------------//
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    
    UIButton *button = createButtonWithFrame(CGRectMake(_descLabel.frame.origin.x,
                                                        _descLabel.frame.origin.y + _descLabel.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             [Language get:@"start" alter:nil],
                                             buttonFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
    
    [button addTarget:self action:@selector(performStart) forControlEvents:UIControlEventTouchUpInside];
    
    _startButton = button;
    [_contentView addSubview:_startButton];
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _startButton.frame.origin.y + _startButton.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:_menuModel.menuName];
    headerView.icon = icon(_menuModel.menuId);
    headerView.titleLabel.font = [UIFont fontWithName:@"OpenSans-Extrabold" size:18.0];
    [_contentView addSubview:headerView];
    [_contentView sendSubviewToBack:headerView];
    [headerView release];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_menuModel release];
    _menuModel = nil;
    [super dealloc];
}

#pragma mark - Selector

- (void)performStart {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = SURVEY_REQ;
    [request survey];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == SURVEY_REQ) {
            
            SurveyStepOtherViewController *surveyStepOtherViewController = [[SurveyStepOtherViewController alloc] initWithNibName:@"SurveyStepOtherViewController" bundle:nil];
            
            surveyStepOtherViewController.currentPage = 0;
            surveyStepOtherViewController.menuModel = _menuModel;
            
            DataManager *sharedData = [DataManager sharedInstance];
            
            SurveyModel *surveyModel = [sharedData.surveyData objectAtIndex:0];
            
            surveyStepOtherViewController.surveyModel = surveyModel;
            
            [self.navigationController pushViewController:surveyStepOtherViewController animated:YES];
            surveyStepOtherViewController = nil;
            [surveyStepOtherViewController release];
            
            /*
            DataManager *dataManager = [DataManager sharedInstance];
            
            for (int i=0; i<[dataManager.recommendedData count]; i++) {
                MenuModel *recommendedPackageModel = [dataManager.recommendedData objectAtIndex:i];
                recommendedPackageModel.groupId        = _selectedMenu.groupId;
                recommendedPackageModel.groupName      = _selectedMenu.menuName;
            }
            
            SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
            subMenuVC.parentId = _selectedMenu.menuId;
            subMenuVC.groupDesc = _selectedMenu.desc;
            NSLog(@"subMenuVC.groupDesc = %@",subMenuVC.groupDesc);
            subMenuVC.recommendedPackageModel = _selectedMenu;
            
            [self.navigationController pushViewController:subMenuVC animated:YES];
            subMenuVC = nil;
            [subMenuVC release];
             */
        }
    }
    else {
        
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
