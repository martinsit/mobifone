//
//  DeviceSettingViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "DeviceSettingViewController.h"

#import "FooterView.h"
#import "ContactUsCell.h"
#import "BorderCellBackground.h"
#import "Constant.h"

//#import "ContactUsModel.h"

#import "DataManager.h"
#import "SectionFooterView.h"
#import "SectionHeaderView.h"

#import "DeviceSettingModel.h"
#import "LabelValueModel.h"
#import "AXISnetCommon.h"

@interface DeviceSettingViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@end

@implementation DeviceSettingViewController

@synthesize data = _data;
@synthesize content = _content;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
    FooterView *footer = [[[FooterView alloc] initWithFrame:CGRectMake(0.0, 0.0, _theTableView.bounds.size.width, 50.0)] autorelease];
    _theTableView.tableFooterView = footer;*/
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)viewWillAppear:(BOOL)animated {
    _content = [_data objectForKey:@"list"];
    [self.theTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.data = nil;
    self.content = nil;
}

- (void)dealloc {
    [_data release];
    _data = nil;
    [_content release];
    _content = nil;
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_content count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DeviceSettingModel *deviceSettingModel = [_content objectAtIndex:section];
    return [deviceSettingModel.data count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TransactionCell";
	
    ContactUsCell *cell = (ContactUsCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		cell = [[ContactUsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
	}
    
    DeviceSettingModel *deviceSettingModel = [_content objectAtIndex:indexPath.section];
    LabelValueModel *labelValueModel = [deviceSettingModel.data objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = labelValueModel.label;
    cell.valueLabel.text = labelValueModel.value;
    
//    CGSize size = calculateExpectedSize(cell.valueLabel, cell.valueLabel.text);
//    CGRect frame = [cell.valueLabel frame];
//    frame.size.height = size.height;
//    cell.valueLabel.frame = frame;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    DeviceSettingModel *deviceSettingModel = [_content objectAtIndex:section];
    NSString *title = deviceSettingModel.desc;
    if ([title length] > 0) {
        return title;
    }
    else {
        return [_data objectForKey:@"name"];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 45.0;
    CGFloat valueLabelHeight = 20.0;
    CGFloat margin = height - valueLabelHeight;
    CGFloat padding = 10.0;
    
    DeviceSettingModel *deviceSettingModel = [_content objectAtIndex:indexPath.section];
    LabelValueModel *labelValueModel = [deviceSettingModel.data objectAtIndex:indexPath.row];
    
    NSString *value = labelValueModel.value;
    CGSize valueSize = [value sizeWithFont:[UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize]
                       constrainedToSize:CGSizeMake(260, 1000.0f)];
    
    // value pasti ada
    if (valueSize.height > valueLabelHeight) {
        if ([labelValueModel.label length] > 0) {
            //NSLog(@"cell = %f",valueSize.height + margin);
            return valueSize.height + margin;
        }
        else {
            //NSLog(@"cell = %f",valueSize.height + padding);
            return valueSize.height + padding;
        }
    }
    
    else {
        if ([labelValueModel.label length] > 0) {
            if ([labelValueModel.value length] > 0) {
                return height;
            }
            else {
                return valueLabelHeight + padding;
            }
            //NSLog(@"cell = %f",height);
            
        }
        else {
            //NSLog(@"cell = %f",valueLabelHeight + padding);
            return valueLabelHeight + padding;
        }
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    ContactUsModel *contactUsModel = [self.content objectAtIndex:indexPath.row];
    if ([contactUsModel.href isEqualToString:@"#call"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",contactUsModel.actionValue]]];
    }
    else if ([contactUsModel.href isEqualToString:@"#url"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:contactUsModel.actionValue]];
    }
    else if ([contactUsModel.href isEqualToString:@"#email"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@",contactUsModel.actionValue]]];
    }
    else {
    }*/
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        /*
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 80.0)
                                                     withLabel:sectionTitle
                                                isFirstSection:YES] autorelease];*/
        
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:YES] autorelease];
        
    }
    else {
        /*
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40.0)
                                                     withLabel:sectionTitle
                                                isFirstSection:NO] autorelease];*/
        
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:NO] autorelease];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

@end
