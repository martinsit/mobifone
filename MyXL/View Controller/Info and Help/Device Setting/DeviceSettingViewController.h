//
//  DeviceSettingViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface DeviceSettingViewController : BasicViewController {
    NSDictionary *_data;
    NSArray *_content;
}

@property (nonatomic, retain) NSDictionary *data;
@property (nonatomic, retain) NSArray *content;

@end
