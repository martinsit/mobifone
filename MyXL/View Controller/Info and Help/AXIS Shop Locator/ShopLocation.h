//
//  ShopLocation.h
//  Aconnect
//
//  Created by Tony Hadisiswanto on 11/7/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface ShopLocation : NSObject <MKAnnotation>

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSArray *time;
@property (nonatomic, assign) CLLocationCoordinate2D theCoordinate;

- (id)initWithName:(NSString*)name
           address:(NSString*)address
       workingTime:(NSArray*)time
        coordinate:(CLLocationCoordinate2D)coordinate;
- (MKMapItem*)mapItem;

@end
