//
//  DetailShopViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/13/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "ShopLocation.h"
#import "MenuModel.h"

@interface DetailShopViewController : BasicViewController {
    ShopLocation *shopLocation;
    MenuModel *_menuModel;
}

@property (nonatomic, retain) ShopLocation *shopLocation;
@property (nonatomic, retain) MenuModel *menuModel;

@end
