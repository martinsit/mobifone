//
//  DetailShopViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/13/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "DetailShopViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

@interface DetailShopViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *shopTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UILabel *workingHourTitleLabel;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

@end

@implementation DetailShopViewController

@synthesize shopLocation;
@synthesize menuModel = _menuModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:_menuModel.groupName
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:_menuModel.menuName
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    _shopTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _shopTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _shopTitleLabel.numberOfLines = 0;
    _shopTitleLabel.lineBreakMode = UILineBreakModeWordWrap;
    _shopTitleLabel.text = [shopLocation.name uppercaseString];
    
    _addressLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _addressLabel.textColor = kDefaultPurpleColor;
    _addressLabel.numberOfLines = 0;
    _addressLabel.lineBreakMode = UILineBreakModeWordWrap;
    _addressLabel.text = shopLocation.address;
    [_addressLabel sizeToFit];
    
    _workingHourTitleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _workingHourTitleLabel.textColor = kDefaultPurpleColor;
    _workingHourTitleLabel.numberOfLines = 0;
    _workingHourTitleLabel.lineBreakMode = UILineBreakModeWordWrap;
    _workingHourTitleLabel.text = [Language get:@"working_time" alter:nil];
    //[_workingHourTitleLabel sizeToFit];
    
    CGRect frameTmp = [_workingHourTitleLabel frame];
    frameTmp.origin.y = _addressLabel.frame.origin.y + _addressLabel.frame.size.height + kDefaultComponentPadding;
    [_workingHourTitleLabel setFrame:frameTmp];
    
    CGRect frameLabel = frameTmp;
    
    for (NSString *workingHour in shopLocation.time) {
        
        UILabel *workingHourValueLabel = [[UILabel alloc] initWithFrame:frameTmp];
        workingHourValueLabel.backgroundColor = [UIColor clearColor];
        workingHourValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        workingHourValueLabel.textColor = kDefaultPurpleColor;
        workingHourValueLabel.numberOfLines = 0;
        workingHourValueLabel.lineBreakMode = UILineBreakModeWordWrap;
        workingHourValueLabel.text = workingHour;
        [workingHourValueLabel sizeToFit];
        
        frameTmp = workingHourValueLabel.frame;
        frameTmp.origin.y = frameLabel.origin.y + frameLabel.size.height;
        [workingHourValueLabel setFrame:frameTmp];
        
        frameLabel = frameTmp;
        
        [_fieldView addSubview:workingHourValueLabel];
        [workingHourValueLabel release];
    }
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    newFrame.size.height = frameTmp.origin.y + frameTmp.size.height;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Content View //
    //--------------//
    newFrame = _contentView.frame;
    newFrame.size.height = _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding + 50;
    _contentView.frame = newFrame;
    _contentView.backgroundColor = kDefaultWhiteColor;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [shopLocation release];
    [super dealloc];
}

@end
