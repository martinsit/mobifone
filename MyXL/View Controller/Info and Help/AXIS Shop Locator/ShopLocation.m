//
//  ShopLocation.m
//  Aconnect
//
//  Created by Tony Hadisiswanto on 11/7/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import "ShopLocation.h"
#import <AddressBook/AddressBook.h>

@interface ShopLocation ()

//@property (nonatomic, copy) NSString *name;
//@property (nonatomic, copy) NSString *address;
//@property (nonatomic, copy) NSArray *time;
//@property (nonatomic, assign) CLLocationCoordinate2D theCoordinate;

@end

@implementation ShopLocation

- (id)initWithName:(NSString*)name
           address:(NSString*)address
       workingTime:(NSArray*)time
        coordinate:(CLLocationCoordinate2D)coordinate {
    
    if ((self = [super init])) {
        if ([name isKindOfClass:[NSString class]]) {
            self.name = name;
        } else {
            self.name = @"Unknown charge";
        }
        self.time = time;
        /*
        NSString *stringTmp = @"";
        for (int i=0; i<[self.time count]; i++) {
            NSString *workingTime = [self.time objectAtIndex:i];
            stringTmp = [stringTmp stringByAppendingString:workingTime];
            stringTmp = [stringTmp stringByAppendingString:@"\n"];
        }*/
        
        self.address = address;
        self.theCoordinate = coordinate;
        
    }
    return self;
}

- (NSString *)title {
    return _name;
}

- (NSString *)subtitle {
    return _address;
}

- (NSArray *)time {
    return _time;
}

- (CLLocationCoordinate2D)coordinate {
    return _theCoordinate;
}

- (MKMapItem*)mapItem {
    NSDictionary *addressDict = @{(NSString*)kABPersonAddressStreetKey : _address};
    
    MKPlacemark *placemark = [[MKPlacemark alloc]
                              initWithCoordinate:self.coordinate
                              addressDictionary:addressDict];
    
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    mapItem.name = self.title;
    
    return mapItem;
}

@end
