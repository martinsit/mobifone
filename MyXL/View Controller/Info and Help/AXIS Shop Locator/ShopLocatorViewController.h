//
//  ShopLocatorViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/13/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>
#import <MapKit/MKReverseGeocoder.h>
#import <CoreLocation/CoreLocation.h>

#import "MenuModel.h"

@interface ShopLocatorViewController : BasicViewController <MKMapViewDelegate, MKReverseGeocoderDelegate,CLLocationManagerDelegate, UISearchBarDelegate> {
    MenuModel *_menuModel;
}

@property (nonatomic, retain) MenuModel *menuModel;

@end
