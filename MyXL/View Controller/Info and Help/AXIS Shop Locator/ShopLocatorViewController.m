//
//  ShopLocatorViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/13/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ShopLocatorViewController.h"
#import "Constant.h"
#import "ShopLocationModel.h"
#import "DataManager.h"
#import "ShopLocation.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

#import "DetailShopViewController.h"

#import "NotificationViewController.h"

@interface ShopLocatorViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UISearchBar *theSearchBar;
@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

@property (readwrite) BOOL needRefresh;
@property (readwrite) CLLocationCoordinate2D location;
@property (nonatomic, retain) MKReverseGeocoder *geoCoder;
@property (nonatomic, retain) MKPlacemark *mPlacemark;

@property (nonatomic, retain) NSString *searchTextInput;

- (void)performShopLocation:(NSString*)city;
- (void)refreshMap;

@end

#define METERS_PER_MILE 1609.344

@implementation ShopLocatorViewController

@synthesize menuModel = _menuModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    /*
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _mapView.frame.origin.y + _mapView.frame.size.height + 10.0;
    _contentView.frame = newFrame;
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:@""];
    _sectionHeaderView = headerView;
    [_contentView addSubview:_sectionHeaderView];
    [_contentView sendSubviewToBack:_sectionHeaderView];
    [headerView release];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 200.0);
     */
    
    _theSearchBar.tintColor = kDefaultNavyBlueColor;
    
    // 1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = -6.224085;
    zoomLocation.longitude= 106.827324;
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 50*METERS_PER_MILE, 50*METERS_PER_MILE);
    
    // 3
    [_mapView setRegion:viewRegion animated:YES];
    
    [self performShopLocation:@""];
}

- (void)viewWillAppear:(BOOL)animated {
    //_sectionHeaderView.icon = self.headerIconMaster;
    //_sectionHeaderView.title = self.headerTitleMaster;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Selector

- (void)performShopLocation:(NSString*)city {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = SHOP_LOCATION_REQ;
    [request shopLocation:city];
}

- (void)refreshMap {
    for (id<MKAnnotation> annotation in _mapView.annotations) {
        [_mapView removeAnnotation:annotation];
    }
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    for (ShopLocationModel *shopLocationModel in sharedData.shopLocationData) {
        NSNumber *latitude = shopLocationModel.latitude;
        //NSLog(@"latitude = %@",latitude);
        NSNumber *longitude = shopLocationModel.longitude;
        //NSLog(@"longitude = %@",longitude);
        NSString *name = shopLocationModel.title;
        NSString *address = shopLocationModel.address;
        NSArray *workingTime = shopLocationModel.time;
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = latitude.doubleValue;
        //NSLog(@"coordinate.latitude = %f",coordinate.latitude);
        coordinate.longitude = longitude.doubleValue;
        //NSLog(@"coordinate.longitude = %f",coordinate.longitude);
        
        ShopLocation *annotation = [[ShopLocation alloc] initWithName:name
                                                              address:address
                                                          workingTime:workingTime
                                                           coordinate:coordinate];
        [_mapView addAnnotation:annotation];
	}
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == SHOP_LOCATION_REQ) {
            [self refreshMap];
            
            if (_needRefresh) {
                _needRefresh = NO;
                
                DataManager *sharedData = [DataManager sharedInstance];
                ShopLocationModel *shopLocationModel = [sharedData.shopLocationData objectAtIndex:0];
                // 1
                CLLocationCoordinate2D zoomLocation;
                zoomLocation.latitude = shopLocationModel.latitude.doubleValue;
                zoomLocation.longitude= shopLocationModel.longitude.doubleValue;
                
                // 2
                MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 5*METERS_PER_MILE, 5*METERS_PER_MILE);
                
                // 3
                [_mapView setRegion:viewRegion animated:YES];
            }
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - MKMapViewDelegate
/*
 - (MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id <MKAnnotation>)annotation {
 NSLog(@"MKMapViewDelegate");
 static NSString *identifier = @"ShopLocation";
 if ([annotation isKindOfClass:[ShopLocation class]]) {
 
 MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
 if (annotationView == nil) {
 annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
 annotationView.enabled = YES;
 annotationView.canShowCallout = YES;
 annotationView.image = [UIImage imageNamed:@"4.png"];//here we use a nice image instead of the default pins
 } else {
 annotationView.annotation = annotation;
 }
 
 return annotationView;
 }
 
 return nil;
 }*/

- (MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id<MKAnnotation>)annotation {
    
    static NSString *AnnotationViewID = @"annotationViewID";
    
    MKAnnotationView *annotationView = (MKAnnotationView *)[mapView1 dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    
    if (annotationView == nil)
    {
        annotationView = [[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID] autorelease];
    }
    
    annotationView.image = [UIImage imageNamed:@"map-marker.png"];
    //annotationView.animatesDrop = TRUE;
    annotationView.enabled = YES;
    annotationView.canShowCallout = YES;
    annotationView.annotation = annotation;
    
    //return annotationView;
    
//	MKPinAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ShopLocation"];
//	annotationView.animatesDrop = TRUE;
//    annotationView.enabled = YES;
//    annotationView.canShowCallout = YES;
//    [annotationView setPinColor:MKPinAnnotationColorPurple];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    annotationView.rightCalloutAccessoryView = rightButton;
    
    UIImageView *axisIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"map-marker.png"]];
    annotationView.leftCalloutAccessoryView = axisIconView;
    [axisIconView release];
    
	return annotationView;
}

- (void)mapView:(MKMapView *)_mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    DetailShopViewController *detailViewController = [[DetailShopViewController alloc] initWithNibName:@"DetailShopViewController" bundle:nil];
    
    detailViewController.shopLocation = view.annotation;
    detailViewController.headerTitleMaster = self.headerTitleMaster;
    detailViewController.headerIconMaster = self.headerIconMaster;
    detailViewController.menuModel = _menuModel;
    
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
}

- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error{
	//NSLog(@"Reverse Geocoder Errored");
}

- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark{
	//NSLog(@"Reverse Geocoder completed");
	_mPlacemark = placemark;
	[_mapView addAnnotation:placemark];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
	
    //mStoreLocationButton.hidden=FALSE;
	_location = newLocation.coordinate;
	//One location is obtained.. just zoom to that location
    
	MKCoordinateRegion region;
	region.center = _location;
	//Set Zoom level using Span
	MKCoordinateSpan span;
	span.latitudeDelta =.005;
	span.longitudeDelta =.005;
	region.span = span;
    
	[_mapView setRegion:region animated:TRUE];
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	NSString *escapedUrl = [searchText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	self.searchTextInput = escapedUrl;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    _needRefresh = YES;
    [self performShopLocation:self.searchTextInput];
	
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
	searchBar.showsCancelButton = YES;
	return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
	searchBar.showsCancelButton = NO;
	return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
}

@end
