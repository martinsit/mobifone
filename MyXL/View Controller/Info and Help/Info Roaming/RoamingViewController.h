//
//  RoamingViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "MenuModel.h"

@interface RoamingViewController : BasicViewController {
    MenuModel *menuModel;
}

@property (nonatomic, retain) MenuModel *menuModel;

@end
