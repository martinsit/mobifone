//
//  RoamingViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 12/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "RoamingViewController.h"
#import "Constant.h"
#import "DataManager.h"
#import "RoamingModel.h"
#import "RoamingCell.h"
#import "SectionHeaderView.h"
#import "AXISnetCommon.h"

@interface RoamingViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (readwrite) CGFloat extraHeight;

@end

@implementation RoamingViewController

@synthesize menuModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [_theTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    else {
        DataManager *sharedData = [DataManager sharedInstance];
        return [sharedData.roamingData count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    RoamingCell *cell = (RoamingCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[RoamingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = kDefaultWhiteColor;
    }
    
    DataManager *sharedData = [DataManager sharedInstance];
    RoamingModel *model = [sharedData.roamingData objectAtIndex:indexPath.row];
    cell.titleLabel.text = [model.country uppercaseString];
    cell.descLabel.text = model.partner;
    
    CGSize sizeLabelDesc = calculateExpectedSize(cell.descLabel, cell.descLabel.text);
    CGRect descFrame = cell.descLabel.frame;
    descFrame.size.height = sizeLabelDesc.height;
    cell.descLabel.frame = descFrame;
    
    if ([sharedData.profileData.telcoOperator isEqualToString:@"AXIS"]) {
        cell.descLabel.textAlignment = UITextAlignmentLeft;
    }
    else {
        cell.descLabel.textAlignment = UITextAlignmentRight;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return menuModel.groupName;
    }
    else {
        return menuModel.menuName;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    DataManager *sharedData = [DataManager sharedInstance];
    if (indexPath.section == 0) {
        return 0;
    }
    else {
        if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
            return 65;
        }
        else {
            CGFloat height = 20.0;
            CGFloat labelHeightDefault = height * 1.5;
            CGFloat descLabelWidth = 0.0;
            if (IS_IPAD) {
                descLabelWidth = 949.0;
            }
            else {
                descLabelWidth = 160.0;
            }
            
            RoamingModel *model = [sharedData.roamingData objectAtIndex:indexPath.row];
            
            UILabel *labelDesc = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, descLabelWidth, labelHeightDefault)];
            labelDesc.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontTitleSize];
            labelDesc.numberOfLines = 0;
            labelDesc.lineBreakMode = UILineBreakModeWordWrap;
            labelDesc.text = model.partner;
            CGSize sizeLabelDesc = calculateExpectedSize(labelDesc, labelDesc.text);
            
            _extraHeight = 0.0;
            if (sizeLabelDesc.height > labelHeightDefault) {
                _extraHeight = sizeLabelDesc.height - labelHeightDefault;
            }
            else {
                _extraHeight = labelHeightDefault;
            }
            
            return _extraHeight;
        }
    }
}

#pragma mark - Table view delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight*1.5)
                                                withLabelForXL:sectionTitle
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                withLabelForXL:sectionTitle
                                                isFirstSection:NO] autorelease];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return kDefaultCellHeight*1.5;
    }
    else {
        return kDefaultCellHeight;
    }
}

@end
