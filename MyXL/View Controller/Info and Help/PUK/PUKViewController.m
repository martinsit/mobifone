//
//  PUKViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PUKViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

#import "DataManager.h"

#import "EncryptDecrypt.h"

#import "NotificationViewController.h"

@interface PUKViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *msisdnLabel;
@property (strong, nonatomic) IBOutlet UITextField *msisdnTextField;

@property (strong, nonatomic) IBOutlet UILabel *iccidLabel;
@property (strong, nonatomic) IBOutlet UILabel *iccidInfoLabel;
//@property (strong, nonatomic) UIButton *infoButton;
@property (strong, nonatomic) IBOutlet UITextField *iccidTextField;
@property (strong, nonatomic) UIButton *submitButton;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

- (void)performInfo;
- (void)performSubmit;
- (void)submitPUK;

@end

@implementation PUKViewController

@synthesize menuModel = _menuModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    DataManager *sharedData = [DataManager sharedInstance];
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:_menuModel.groupName
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:_menuModel.menuName
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    _msisdnLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _msisdnLabel.textColor = kDefaultTitleFontGrayColor;
    
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        _msisdnLabel.text = [Language get:@"xl_number" alter:nil];
    }
    else {
        _msisdnLabel.text = [Language get:@"axis_number" alter:nil];
    }
    
    _msisdnTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    _msisdnTextField.textColor = kDefaultBlackColor;
    NSString *saltKeyAPI = SALT_KEY;
    
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    if ([msisdn length] > 0) {
        msisdn = [msisdn stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"0"];
    }
    _msisdnTextField.text = msisdn;
    
    _iccidLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _iccidLabel.textColor = kDefaultTitleFontGrayColor;
    _iccidLabel.text = @"ICCID";
    
    _iccidInfoLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    _iccidInfoLabel.textColor = kDefaultBlackColor;
    _iccidInfoLabel.numberOfLines = 0;
    _iccidInfoLabel.lineBreakMode = UILineBreakModeWordWrap;
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        _iccidInfoLabel.text = [Language get:@"iccid_info" alter:nil];
    }
    else {
        _iccidInfoLabel.text = [Language get:@"iccid_info_axis" alter:nil];
    }
    
    _iccidTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    _iccidTextField.textColor = kDefaultBlackColor;
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Setup Button //
    //--------------//
    UIFont *titleFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    UIButton *button = createButtonWithFrame(CGRectMake(_iccidTextField.frame.origin.x,
                                                        _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             [[Language get:@"submit" alter:nil] uppercaseString],
                                             titleFont,
                                             kDefaultWhiteColor,
                                             colorWithHexString(sharedData.profileData.themesModel.menuFeature),
                                             colorWithHexString(sharedData.profileData.themesModel.menuColor));
    [button addTarget:self action:@selector(performSubmit) forControlEvents:UIControlEventTouchUpInside];
    _submitButton = button;
    [_contentView addSubview:_submitButton];
    
    //--------------//
    // Content View //
    //--------------//
    newFrame = _contentView.frame;
    newFrame.size.height = _submitButton.frame.origin.y + _submitButton.frame.size.height + kDefaultComponentPadding + 50.0;
    _contentView.frame = newFrame;
    _contentView.backgroundColor = kDefaultWhiteColor;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 200.0);
    
    [headerView1 release];
    [headerView2 release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.menuModel = nil;
}

- (void)dealloc {
    [_menuModel release];
    _menuModel = nil;
    [super dealloc];
}

#pragma mark - Selector

- (void)performInfo {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:_menuModel.tooltips
                                                   delegate:self
                                          cancelButtonTitle:[Language get:@"ok" alter:nil]
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}

- (void)performSubmit {
    [_msisdnTextField resignFirstResponder];
    [_iccidTextField resignFirstResponder];
    
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    
    [self submitPUK];
}

- (void)submitPUK {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = PUK_REQ;
    
    [request puk:_msisdnTextField.text
       withIccid:_iccidTextField.text];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == PUK_REQ) {
            DataManager *sharedData = [DataManager sharedInstance];
            NSString *puk1 = sharedData.pukData.puk1;
            //NSString *puk2 = sharedData.pukData.puk2;
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[NSString stringWithFormat:@"%@",puk1]
                                                           delegate:self
                                                  cancelButtonTitle:[Language get:@"ok" alter:nil]
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"ok" alter:nil]
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat targetY = -textField.frame.size.height + textField.frame.origin.y;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 19);
}

@end
