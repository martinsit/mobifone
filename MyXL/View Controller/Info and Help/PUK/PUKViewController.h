//
//  PUKViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "MenuModel.h"
#import "AXISnetRequest.h"

@interface PUKViewController : BasicViewController <UITextFieldDelegate, AXISnetRequestDelegate> {
    MenuModel *_menuModel;
}

@property (nonatomic, retain) MenuModel *menuModel;

@end
