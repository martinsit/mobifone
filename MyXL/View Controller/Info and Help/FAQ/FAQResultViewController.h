//
//  FAQResultViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AXISnetRequest.h"

@interface FAQResultViewController : BasicViewController <AXISnetRequestDelegate, UISearchBarDelegate> {
    NSArray *_content;
}

@property (nonatomic, retain) NSArray *content;

@end
