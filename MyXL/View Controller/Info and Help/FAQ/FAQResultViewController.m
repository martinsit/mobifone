//
//  FAQResultViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "FAQResultViewController.h"

#import "CommonCell.h"
#import "AXISnetCellBackground.h"
#import "SectionHeaderView.h"
#import "SectionFooterView.h"
#import "Constant.h"
#import "MTPopupWindow.h"
#import "AXISnetCommon.h"

#import "FAQResultModel.h"
#import "DataManager.h"

@interface FAQResultViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *theSearchBar;
@property (nonatomic, retain) NSString *searchTextInput;

@property (strong, nonatomic) IBOutlet UIView *blockBgView;
@property (strong, nonatomic) IBOutlet UIView *popUpView;
@property (strong, nonatomic) IBOutlet UIWebView *theWebView;
@property (nonatomic, retain) NSIndexPath *checkedIndexPath;

@property (readwrite) CGFloat extraHeight;

- (IBAction)closePopUp:(id)sender;

@end

@implementation FAQResultViewController

@synthesize content = _content;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.checkedIndexPath = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.content = nil;
}

- (void)dealloc {
    [_content release];
    _content = nil;
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_content count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierCommon = @"Common";
    
    CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierCommon];
    
    if (cell == nil) {
        cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierCommon];
        
        [cell.infoButton addTarget:self
                            action:@selector(performInfo:)
                  forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.backgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
    cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
    ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
    
    FAQResultModel *faqResultModel = [_content objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = [faqResultModel.question uppercaseString];
    CGSize size = calculateExpectedSize(cell.titleLabel, cell.titleLabel.text);
    CGRect newFrame = cell.titleLabel.frame;
    newFrame.size.height = size.height;
    cell.titleLabel.frame = newFrame;
    cell.needResize = YES;
    
    if (!faqResultModel.isWebView) {
        cell.descLabel.text = faqResultModel.answer;
    }
    
    //cell.arrowLabel.text = @"d";
    if ([faqResultModel.answer length] > 0) {
        cell.arrowLabel.hidden = YES;
        cell.infoButton.hidden = NO;
    }
    else {
        cell.arrowLabel.hidden = NO;
        cell.infoButton.hidden = YES;
    }
    
    if (faqResultModel.open) {
        if (faqResultModel.isWebView) {
            cell.descLabel.hidden = YES;
        }
        else {
            cell.descLabel.hidden = NO;
            
            newFrame = cell.descLabel.frame;
            newFrame.size.height = _extraHeight;
            cell.descLabel.frame = newFrame;
        }
    }
    else {
        cell.descLabel.hidden = YES;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.headerTitleMaster;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //return kDefaultCellHeight;
    
    CGFloat labelHeightDefault = 28.0;
    
    FAQResultModel *faqResultModel = [self.content objectAtIndex:indexPath.row];
    
    NSString *text = [faqResultModel.question uppercaseString];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 240.0, 28.0)];
    CGSize size = calculateExpectedSize(label, text);
    
    CGFloat extraHeightForTitle = 0.0;
    if (size.height > labelHeightDefault) {
        extraHeightForTitle = size.height - labelHeightDefault;
    }
    
    if (faqResultModel.open) {
        if (faqResultModel.isWebView) {
            return kDefaultCellHeight + extraHeightForTitle;
        }
        else {
            return kDefaultCellHeight + _extraHeight + extraHeightForTitle;
        }
    }
    else {
        return kDefaultCellHeight + extraHeightForTitle;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *selectedIndexPath = indexPath;
    
    FAQResultModel *faqResultModel = [_content objectAtIndex:indexPath.row];
    
    // Uncheck the previous checked row
    if (self.checkedIndexPath)
    {
        FAQResultModel *faqResultModel = [_content objectAtIndex:self.checkedIndexPath.row];
        faqResultModel.open = NO;
    }
    
    if ([self.checkedIndexPath isEqual:selectedIndexPath])
    {
        self.checkedIndexPath = nil;
    }
    else
    {
        faqResultModel.open = YES;
        self.checkedIndexPath = indexPath;
        
        if (faqResultModel.isWebView) {
            //popUp.delegate = self;
            //[MTPopupWindow showWindowWithHTMLFile:faqResultModel.answer insideView:popUpView];
            //[self.view bringSubviewToFront:popUpView];
            
            //[MTPopupWindow showWindowWithHTMLFile:faqResultModel.answer];
            
            //AconnectAppDelegate *appDelegate = (AconnectAppDelegate *)[[UIApplication sharedApplication] delegate];
            //[MTPopupWindow showWindowWithHTMLFile:faqResultModel.answer insideView:appDelegate.navcontroller.view];
            
            NSString *body = [NSString stringWithFormat:@"<html><head><style> body, label,table, td {font-family:Helvetica; font-size:12px} p {padding:0;margin:0} label {font-weight:bold} p.Q {font-size:16px; color:#B5005B;font-weight:bold} .left-pad {margin-top:0;padding-left:7px;} .left-pad li {margin-left:10px !important;overflow:visible !important;} .tbl-faq {text-align:center;font-size:12px;color:#333333;border:1px solid #666666;border-collapse: collapse;} .tbl-faq.head {background-color: #dedede;} .tbl-faq.body {background-color:#ffffff; font-weight:normal;} </style> </head> <body> <p class=\"Q\">%@</p><p class=\"A\"><br/>%@</p> </body> </html>",faqResultModel.question,faqResultModel.answer];
            
            NSString *reqSysVer = @"6.0";
            NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
            if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending) {
                [MTPopupWindow showWindowWithHTMLFile:body insideView:self.view];
            }
            else {
                _blockBgView.hidden = NO;
                _popUpView.hidden = NO;
                [_theWebView loadHTMLString:body baseURL:nil];
            }
            
            faqResultModel.open = NO;
            self.checkedIndexPath = nil;
        }
        else {
            // Calculate extra height
            
            CommonCell *cell = (CommonCell*)[_theTableView cellForRowAtIndexPath:indexPath];
            
            //CGRect testFrame = cell.descLabel.frame;
            //NSLog(@"frame = %f",testFrame.size.height);
            
            CGSize size = calculateExpectedSize(cell.descLabel, faqResultModel.answer);
            _extraHeight = size.height;
            
            /*
            NSString *fontType = FONT_DESC;
            CGFloat fontSize = FONT_DESC_SIZE;
            UIFont *font = [UIFont fontWithName:fontType size:fontSize];
            NSString *descString = faqResultModel.answer;
            CGSize maximumLabelSize;
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                maximumLabelSize = CGSizeMake(290,9999);
            }
            else {
                maximumLabelSize = CGSizeMake(744,9999);
            }
            CGSize size = [descString sizeWithFont:font
                                 constrainedToSize:maximumLabelSize
                                     lineBreakMode:UILineBreakModeWordWrap];
            extraHeight = size.height;*/
        }
    }
    
    [_theTableView reloadData];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:NO] autorelease];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

#pragma mark - Selector

- (IBAction)closePopUp:(id)sender {
    _blockBgView.hidden = YES;
    _popUpView.hidden = YES;
}

- (void)performFAQ:(NSString*)menuId withKeyword:(NSString*)keyword {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = FAQ_REQ;
    [request faq:menuId withKeyword:keyword];
}

- (void)performInfo:(id)sender {
    UIButton *button = (UIButton *)sender;
    CommonCell *cell = (CommonCell *)[[button superview] superview];
    int section = [_theTableView indexPathForCell:cell].section;
    int row = [_theTableView indexPathForCell:cell].row;
    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
    
    FAQResultModel *faqResultModel = [_content objectAtIndex:selectedIndexPath.row];
    
    // Uncheck the previous checked row
    if (self.checkedIndexPath)
    {
        FAQResultModel *faqResultModel = [_content objectAtIndex:self.checkedIndexPath.row];
        faqResultModel.open = NO;
    }
    
    if ([self.checkedIndexPath isEqual:selectedIndexPath])
    {
        self.checkedIndexPath = nil;
    }
    else
    {
        faqResultModel.open = YES;
        self.checkedIndexPath = selectedIndexPath;
        
        if (faqResultModel.isWebView) {
            
            NSString *body = [NSString stringWithFormat:@"<html><head><style> body, label,table, td {font-family:Helvetica; font-size:12px} p {padding:0;margin:0} label {font-weight:bold} p.Q {font-size:16px; color:#B5005B;font-weight:bold} .left-pad {margin-top:0;padding-left:7px;} .left-pad li {margin-left:10px !important;overflow:visible !important;} .tbl-faq {text-align:center;font-size:12px;color:#333333;border:1px solid #666666;border-collapse: collapse;} .tbl-faq.head {background-color: #dedede;} .tbl-faq.body {background-color:#ffffff; font-weight:normal;} </style> </head> <body> <p class=\"Q\">%@</p><p class=\"A\"><br/>%@</p> </body> </html>",faqResultModel.question,faqResultModel.answer];
            
            NSString *reqSysVer = @"6.0";
            NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
            if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending) {
                [MTPopupWindow showWindowWithHTMLFile:body insideView:self.view];
            }
            else {
                _blockBgView.hidden = NO;
                _popUpView.hidden = NO;
                [_theWebView loadHTMLString:body baseURL:nil];
            }
            
            faqResultModel.open = NO;
            self.checkedIndexPath = nil;
        }
        else {
            // Calculate extra height
            CommonCell *cell = (CommonCell*)[_theTableView cellForRowAtIndexPath:selectedIndexPath];
            CGSize size = calculateExpectedSize(cell.descLabel, faqResultModel.answer);
            _extraHeight = size.height;
        }
    }
    
    [_theTableView reloadData];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == FAQ_REQ) {
            
            DataManager *sharedData = [DataManager sharedInstance];
            _content = sharedData.faqData;
            self.headerTitleMaster = self.searchTextInput;
            
            [_theTableView reloadData];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	NSString *escapedUrl = [searchText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	self.searchTextInput = escapedUrl;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    _theTableView.allowsSelection = NO;
    _theTableView.scrollEnabled = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self performFAQ:@"0" withKeyword:self.searchTextInput];
	
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    _theTableView.allowsSelection = YES;
    _theTableView.scrollEnabled = YES;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
	searchBar.showsCancelButton = YES;
	return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
	searchBar.showsCancelButton = NO;
	return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
    _theTableView.allowsSelection = YES;
    _theTableView.scrollEnabled = YES;
}

@end
