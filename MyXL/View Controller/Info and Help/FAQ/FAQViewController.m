//
//  FAQViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "FAQViewController.h"

#import "DataManager.h"
#import "GroupingAndSorting.h"
#import "CommonCell.h"
#import "AXISnetCellBackground.h"
#import "SectionHeaderView.h"
#import "SectionFooterView.h"
#import "MenuModel.h"

#import "Constant.h"
#import "AXISnetCommon.h"

#import "FAQResultViewController.h"

@interface FAQViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *theSearchBar;
@property (nonatomic, retain) NSString *searchTextInput;
@property (nonatomic, retain) NSString *nextTitle;

@end

@implementation FAQViewController

@synthesize sortedMenu = _sortedMenu;
@synthesize theSourceMenu = _theSourceMenu;

@synthesize parentId = _parentId;
@synthesize groupDesc = _groupDesc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)viewWillAppear:(BOOL)animated {
    DataManager *dataManager = [DataManager sharedInstance];
    NSArray *contentTmp = [dataManager.menuData valueForKey:_parentId];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    self.sortedMenu = [sortingResult allKeys];
    self.sortedMenu = [GroupingAndSorting doSortingByGroupId:self.sortedMenu];
    self.theSourceMenu = sortingResult;
    
    [self.theTableView reloadData];
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.sortedMenu = nil;;
    self.theSourceMenu = nil;
    
    self.parentId = nil;
    self.groupDesc = nil;
}

- (void)dealloc {
    [_sortedMenu release];
    _sortedMenu = nil;
    [_theSourceMenu release];
    _theSourceMenu = nil;
    
    [_parentId release];
    _parentId = nil;
    [_groupDesc release];
    _groupDesc = nil;
    
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sortedMenu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section]] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierCommon = @"Common";
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierCommon];
    
    if (cell == nil) {
        cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierCommon];
        //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.backgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
        cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
        ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
    }
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
    
    cell.iconLabel.text = icon(menuModel.menuId);
    if ([cell.iconLabel.text length] <= 0) {
        cell.iconLabel.text = @"";
    }
    
    cell.titleLabel.text = [menuModel.menuName uppercaseString];
    
    if ([menuModel.desc length] > 0) {
        cell.arrowLabel.hidden = YES;
        cell.infoButton.hidden = NO;
    }
    else {
        cell.arrowLabel.hidden = NO;
        cell.infoButton.hidden = YES;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.headerTitleMaster;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kDefaultCellHeight;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
    self.nextTitle = menuModel.menuName;
    [self performFAQ:menuModel.menuId withKeyword:@""];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:NO] autorelease];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

#pragma mark - Selector

- (void)performFAQ:(NSString*)menuId withKeyword:(NSString*)keyword {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = FAQ_REQ;
    [request faq:menuId withKeyword:keyword];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == FAQ_REQ) {
            FAQResultViewController *faqResultViewController = [[FAQResultViewController alloc] initWithNibName:@"FAQResultViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            faqResultViewController.content = sharedData.faqData;
            
            faqResultViewController.headerTitleMaster = self.nextTitle;
            faqResultViewController.headerIconMaster = self.headerIconMaster;
            
            [self.navigationController pushViewController:faqResultViewController animated:YES];
            faqResultViewController = nil;
            [faqResultViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	NSString *escapedUrl = [searchText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	self.searchTextInput = escapedUrl;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    _theTableView.allowsSelection = NO;
    _theTableView.scrollEnabled = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.nextTitle = self.searchTextInput;
    [self performFAQ:@"0" withKeyword:self.searchTextInput];
	
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    _theTableView.allowsSelection = YES;
    _theTableView.scrollEnabled = YES;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
	searchBar.showsCancelButton = YES;
	return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
	searchBar.showsCancelButton = NO;
	return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
    _theTableView.allowsSelection = YES;
    _theTableView.scrollEnabled = YES;
}

@end
