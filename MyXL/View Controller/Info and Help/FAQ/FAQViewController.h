//
//  FAQViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AXISnetRequest.h"

@interface FAQViewController : BasicViewController <AXISnetRequestDelegate, UISearchBarDelegate> {
    NSArray *_sortedMenu;
    NSDictionary *_theSourceMenu;
    
    NSString *_parentId;
    NSString *_groupDesc;
}

@property (nonatomic, retain) NSArray *sortedMenu;
@property (nonatomic, retain) NSDictionary *theSourceMenu;

@property (nonatomic, retain) NSString *parentId;
@property (nonatomic, retain) NSString *groupDesc;

@end
