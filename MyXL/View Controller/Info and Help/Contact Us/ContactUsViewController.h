//
//  ContactUsViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AccordionView.h"
#import "MenuModel.h"
#import "DYRateView.h"

typedef enum {
    CONTACT_US_ACC = 0,
    PROMO_ACC,
    ABOUT_ACC,
    FAQ_ACC,
    INFO4G_ACC
}AccordionType;

@interface ContactUsViewController : BasicViewController <DYRateViewDelegate, UITextViewDelegate, UIAlertViewDelegate> {
    NSArray *_content;
    AccordionView *accordion;
    AccordionType accordionType;
    MenuModel *menuModel;
    
    BOOL _isLevel2;
}

@property (nonatomic, retain) NSArray *content;
@property (readwrite) AccordionType accordionType;
@property (nonatomic, retain) MenuModel *menuModel;

@property (readwrite) BOOL isLevel2;

@end
