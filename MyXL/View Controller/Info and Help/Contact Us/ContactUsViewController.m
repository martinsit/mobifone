//
//  ContactUsViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ContactUsViewController.h"
#import "FooterView.h"
#import "ContactUsCell.h"
#import "BorderCellBackground.h"
#import "Constant.h"
#import "ContactUsModel.h"
#import "DataManager.h"
#import "SectionFooterView.h"
#import "SectionHeaderView.h"

#import "FlatButton.h"
#import "PromoModel.h"
#import "AXISnetCommon.h"
#import "DetailPromoViewController.h"

#import "GroupingAndSorting.h"

#import "SubMenuViewController.h"

#import "SVModalWebViewController.h"
#import "UpdateProfileViewController.h"
#import "MenuModel.h"
#import "Replace4GUsimViewController.h"
#import "KTTextView.h"
#import "NotificationViewController.h"

@interface ContactUsViewController ()

//@property (readwrite) int ratingValue;
@property (strong, nonatomic) DYRateView *rateView;
@property (strong, nonatomic) KTTextView *textView;

@end

@implementation ContactUsViewController

@synthesize content = _content;
@synthesize accordionType;
@synthesize menuModel;

@synthesize isLevel2 = _isLevel2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //DataManager *sharedData = [DataManager sharedInstance];
    //_ratingValue = sharedData.ratingValue;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self setupAccordion];
    
    if (_isLevel2) {
        [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    }
    else {
        [self createBarButtonItem:BACK_NOTIF];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.content = nil;
}

- (void)dealloc {
    [_content release];
    _content = nil;
    [super dealloc];
}

#pragma mark - Selector

- (int)validateRateComment {
    DataManager *sharedData = [DataManager sharedInstance];
    if (sharedData.ratingValue == 0) {
        return 1; // rating
    }
    else if ([_textView.text length] <= 1) {
        return 2; // comment
    }
    else {
        return 0;
    }
}

- (void)performSubmitRating:(id)sender {
    /*
    int valid = [self validateRateComment];
    if (valid == 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"empty_validation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    else if (valid ==2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"empty_validation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    else {
        [self submitRateComment];
    }*/
    
    [self submitRateComment];
}

- (void)submitRateComment {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.commentValue = _textView.text;
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = RATE_COMMENT_4G_USIM_REQ;
    
    //[request rate4g:[NSString stringWithFormat:@"%i",sharedData.ratingValue] withComment:sharedData.commentValue];
    int ratingInt = (int)[self checkRatingValue];
    [request rate4g:[NSString stringWithFormat:@"%i",ratingInt] withComment:sharedData.commentValue];
}

- (void)performPackage:(id)sender {
    SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
    
    DataManager *dataManager = [DataManager sharedInstance];
    subMenuVC.parentMenuModel = dataManager.package4gMenu;
    subMenuVC.isLevel2 = NO;
    subMenuVC.levelTwoType = COMMON;
    subMenuVC.grayHeaderTitle = dataManager.packageMenu.menuName;
    
    [self.navigationController pushViewController:subMenuVC animated:YES];
    subMenuVC = nil;
    [subMenuVC release];
}

- (void)performShow:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    PromoModel *promoModel = [self.content objectAtIndex:button.tag];
    
    NSURL *url = nil;
    
    url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",promoModel.promoUrl]];
    
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
    webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:webViewController animated:YES completion:NULL];
}

- (void)performReplace:(id)sender
{
    Replace4GUsimViewController *replace4GUsimViewController = [[Replace4GUsimViewController alloc] initWithNibName:@"Replace4GUsimViewController" bundle:nil];
    [replace4GUsimViewController createBarButtonItem:BACK_NOTIF];
    
    MenuModel *menu = [[MenuModel alloc] init];
    menu.groupName = [Language get:@"form" alter:nil].uppercaseString;
    menu.menuName = [Language get:@"form_replace_simcard" alter:nil].uppercaseString;
    
    replace4GUsimViewController.menuModel = menu;
    [menu release];
    
    [self.navigationController pushViewController:replace4GUsimViewController animated:YES];
    replace4GUsimViewController = nil;
    [replace4GUsimViewController release];
}

- (void)performDetail:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    PromoModel *promoModel = [self.content objectAtIndex:button.tag];
    
    if (![promoModel.promoMenuId isEqualToString:@""]) {
        SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
        NSString *menuId = promoModel.promoMenuId;
        menuId = [menuId stringByReplacingOccurrencesOfString:@"." withString:@"_"];
        MenuModel *nextMenu = [[MenuModel alloc] init];
        nextMenu.menuId = menuId;
        subMenuVC.parentMenuModel = nextMenu;
        subMenuVC.grayHeaderTitle = @"PROMO";
        [self.navigationController pushViewController:subMenuVC animated:YES];
        
        subMenuVC = nil;
        [subMenuVC release];
    }
    else {
        DetailPromoViewController *detailPromoViewController = [[DetailPromoViewController alloc] initWithNibName:@"DetailPromoViewController" bundle:nil];
        detailPromoViewController.selectedPromo = button.tag;
        detailPromoViewController.menuModel = self.menuModel;
        
        //detailPromoViewController.headerIconMaster = icon(@"gid_50");
        //detailPromoViewController.headerTitleMaster = dataManager.menuModel.menuName;
        
        [self.navigationController pushViewController:detailPromoViewController animated:YES];
        detailPromoViewController = nil;
        [detailPromoViewController release];
    }
}

- (void)setupAccordion {
    
    /*
     * Clear View
     */
    
    for (UIView *oldViews in self.view.subviews)
    {
        [oldViews removeFromSuperview];
    }
    
    //***********
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    self.view.backgroundColor = kDefaultWhiteSmokeColor;
    
    CGFloat screenWidth = 0.0;
    if (IS_IPAD) {
        screenWidth = 1024.0;
    }
    else {
        screenWidth = 320.0;
    }
    
    SectionHeaderView *titleView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0.0, 31.0, screenWidth, kDefaultCellHeight*1.5)
                                                             withLabelForXL:menuModel.menuName
                                                             isFirstSection:YES];
    [self.view addSubview:titleView];
    
    accordion = [[AccordionView alloc] initWithFrame:CGRectMake(0, titleView.frame.origin.y + titleView.frame.size.height, screenWidth, self.view.bounds.size.height - 30.0)];
    // TODO : V1.9
    accordion.showDetail = YES;
    
    [self.view addSubview:accordion];
    
    UIFont *titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontHeaderSize];
    
    for (int i = 0; i < [_content count]; i++) {
        
        if (accordionType == CONTACT_US_ACC) {
            ContactUsModel *contactUsModel = [self.content objectAtIndex:i];
            FlatButton *button;
            if (i==0) {
                button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kDefaultCellHeight)
                                                     title:contactUsModel.label
                                                 titleFont:titleFont
                                                titleColor:kDefaultWhiteColor
                                                   bgColor:colorWithHexString(sharedData.profileData.themesModel.accordionCurrentBg)
                                             selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionCurrentBg)];
            }
            else {
                button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kDefaultCellHeight)
                                                     title:contactUsModel.label
                                                 titleFont:titleFont
                                                titleColor:kDefaultWhiteColor
                                                   bgColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)
                                             selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)];
            }
            
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, button.frame.size.height - 1, self.view.bounds.size.width, 1)];
            lineView.backgroundColor = kDefaultWhiteColor;
            [button addSubview:lineView];
            
            UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
            NSString *html = [NSString stringWithFormat:@"<html><style>body {font-family:GothamRounded-Light; font-size:13px;}</style><body>%@</body></html>",contactUsModel.value];
            [webView loadHTMLString:[NSString stringWithFormat:@"%@",html] baseURL:nil];
            [webView setBackgroundColor:kDefaultWhiteColor];
            
            [accordion addHeader:button withView:webView];
        }
        else if (accordionType == PROMO_ACC) {
            PromoModel *promoModel = [self.content objectAtIndex:i];
            FlatButton *button;
            if (i==0) {
                button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kDefaultCellHeight + 10.0)
                                                     title:promoModel.promoTitle
                                                 titleFont:titleFont
                                                titleColor:kDefaultWhiteColor
                                                   bgColor:colorWithHexString(sharedData.profileData.themesModel.accordionCurrentBg)
                                             selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionCurrentBg)];
            }
            else {
                button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kDefaultCellHeight + 10.0)
                                                     title:promoModel.promoTitle
                                                 titleFont:titleFont
                                                titleColor:kDefaultWhiteColor
                                                   bgColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)
                                             selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)];
            }
            // Adjust Title Label height
            CGSize labelSize = calculateExpectedSize(button.titleLbl, promoModel.promoTitle);
            CGRect labelFrame = button.titleLbl.frame;
            labelFrame.size.height = labelSize.height;
            labelFrame.origin.y = (button.frame.size.height - labelFrame.size.height)/2;
            button.titleLbl.frame = labelFrame;
            
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, button.frame.size.height - 1, self.view.bounds.size.width, 1)];
            lineView.backgroundColor = kDefaultWhiteColor;
            [button addSubview:lineView];
            
            UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
            contentView.backgroundColor = kDefaultWhiteColor;
            
            UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, self.view.bounds.size.width - (15*2), 200)];
            descLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
            descLabel.textColor = kDefaultBlackColor;
            descLabel.backgroundColor = [UIColor clearColor];
            descLabel.numberOfLines = 0;
            descLabel.lineBreakMode = UILineBreakModeWordWrap;
            descLabel.text = promoModel.promoShortDesc;
            
            CGSize size = calculateExpectedSize(descLabel, descLabel.text);
            CGRect frame = [descLabel frame];
            frame.size.height = size.height;
            descLabel.frame = frame;
            
            /*
             * Add Button
             */
            UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            UIButton *buttonDetail = createButtonWithFrame(CGRectMake(descLabel.frame.origin.x,
                                                                      descLabel.frame.origin.y + descLabel.frame.size.height + kDefaultComponentPadding,
                                                                      100,
                                                                      kDefaultButtonHeight),
                                                           [[Language get:@"detail" alter:nil] uppercaseString],
                                                           buttonFont,
                                                           kDefaultWhiteColor,
                                                           colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                           colorWithHexString(sharedData.profileData.themesModel.menuFeature));
            buttonDetail.tag = i;
            [buttonDetail addTarget:self action:@selector(performDetail:) forControlEvents:UIControlEventTouchUpInside];
            [contentView addSubview:buttonDetail];
            
            /*
             * Adjust Height Content
             */
            frame = contentView.frame;
            //frame.size.height = descLabel.frame.size.height + (15*2);
            if (i == [_content count] - 1) {
                frame.size.height = buttonDetail.frame.origin.y + buttonDetail.frame.size.height + 30;
            }
            else {
                frame.size.height = buttonDetail.frame.origin.y + buttonDetail.frame.size.height + 15;
            }
            
            contentView.frame = frame;
            
            [contentView addSubview:descLabel];
            
            [accordion addHeader:button withView:contentView];
        }
        else if (accordionType == INFO4G_ACC) {
            PromoModel *promoModel = [self.content objectAtIndex:i];
            FlatButton *button;
            if (i==0) {
                button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kDefaultCellHeight + 10.0)
                                                     title:promoModel.promoTitle
                                                 titleFont:titleFont
                                                titleColor:kDefaultWhiteColor
                                                   bgColor:colorWithHexString(sharedData.profileData.themesModel.accordionCurrentBg)
                                             selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionCurrentBg)];
            }
            else {
                button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kDefaultCellHeight + 10.0)
                                                     title:promoModel.promoTitle
                                                 titleFont:titleFont
                                                titleColor:kDefaultWhiteColor
                                                   bgColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)
                                             selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)];
            }
            // Adjust Title Label height
            CGSize labelSize = calculateExpectedSize(button.titleLbl, promoModel.promoTitle);
            CGRect labelFrame = button.titleLbl.frame;
            labelFrame.size.height = labelSize.height;
            labelFrame.origin.y = (button.frame.size.height - labelFrame.size.height)/2;
            button.titleLbl.frame = labelFrame;
            
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, button.frame.size.height - 1, self.view.bounds.size.width, 1)];
            lineView.backgroundColor = kDefaultWhiteColor;
            [button addSubview:lineView];
            
            UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 0.0, 200.0)];
            contentView.backgroundColor = kDefaultWhiteColor;
            
            UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 15.0, self.view.bounds.size.width - (15.0*2.0), 200.0)];
            descLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
            descLabel.textColor = kDefaultBlackColor;
            descLabel.backgroundColor = [UIColor clearColor];
            descLabel.numberOfLines = 0;
            descLabel.lineBreakMode = UILineBreakModeWordWrap;
            NSLog(@"--> desc height origin = %f",descLabel.frame.size.height);
            
            NSString *descStr = promoModel.promoShortDesc;
            
            if (!!descStr && ![descStr isEqual:[NSNull null]]) {
                descStr = [descStr stringByReplacingOccurrencesOfString:@"\r\n"
                                                             withString:@"<br>"];
                descStr = [descStr stringByReplacingOccurrencesOfString:@"<br>"
                                                             withString:@"\n"];
            }
            else {
                descStr = @"";
            }
            
            descLabel.text = descStr;
            
            CGSize size = calculateExpectedSize(descLabel, descLabel.text);
            CGRect frame = [descLabel frame];
            frame.size.height = size.height;
            descLabel.frame = frame;
            NSLog(@"--> desc height = %f",frame.size.height);
            
            /*
            NSError *err = nil;
            descLabel.attributedText = [[NSAttributedString alloc] initWithData:[descLabel.text dataUsingEncoding:NSUTF8StringEncoding]
                                                                        options:@{ NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType}
                                                             documentAttributes:nil
                                                                          error:&err];
            if (err)
                NSLog(@"Unable to parse label text: %@", err);*/
            
            
            
            /*
             * Add Button
             */
            UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            
            UIButton *buttonDetail;
            
            if ([promoModel.href isEqualToString:@"#replacesim"]) {
                buttonDetail = createButtonWithFrame(CGRectMake(descLabel.frame.origin.x,
                                                                          descLabel.frame.origin.y + descLabel.frame.size.height + kDefaultComponentPadding,
                                                                          100,
                                                                          kDefaultButtonHeight),
                                                               [[Language get:@"replace" alter:nil] uppercaseString],
                                                               buttonFont,
                                                               kDefaultWhiteColor,
                                                               colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                               colorWithHexString(sharedData.profileData.themesModel.menuFeature));
                buttonDetail.tag = i;
                [buttonDetail addTarget:self action:@selector(performReplace:) forControlEvents:UIControlEventTouchUpInside];
            }
            else if ([promoModel.href isEqualToString:@"#package"] || [promoModel.href isEqualToString:@"#showing"]) {
                buttonDetail = createButtonWithFrame(CGRectMake(descLabel.frame.origin.x,
                                                                descLabel.frame.origin.y + descLabel.frame.size.height + kDefaultComponentPadding,
                                                                100,
                                                                kDefaultButtonHeight),
                                                     [[Language get:@"detail" alter:nil] uppercaseString],
                                                     buttonFont,
                                                     kDefaultWhiteColor,
                                                     colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                     colorWithHexString(sharedData.profileData.themesModel.menuFeature));
                buttonDetail.tag = i;
                [buttonDetail addTarget:self action:@selector(performPackage:) forControlEvents:UIControlEventTouchUpInside];
            }
            else if ([promoModel.href isEqualToString:@"#rating4G"]) {
                /*
                 * Setting UI
                 */
                descLabel.hidden = YES;
                
                // Rate Label
                UILabel *rateLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, self.view.bounds.size.width - (15*2), 20)];
                rateLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
                rateLabel.textColor = kDefaultBlackColor;
                rateLabel.backgroundColor = [UIColor clearColor];
                rateLabel.numberOfLines = 0;
                rateLabel.lineBreakMode = UILineBreakModeWordWrap;
                rateLabel.text = [Language get:@"ratenow" alter:nil];
                [contentView addSubview:rateLabel];
                
                // Rate View
                DYRateView *rateViewTmp = [[DYRateView alloc] initWithFrame:CGRectMake(rateLabel.frame.origin.x, rateLabel.frame.origin.y + rateLabel.frame.size.height + kDefaultComponentPadding, 120, 20) fullStar:[UIImage imageNamed:@"StarFullLarge.png"] emptyStar:[UIImage imageNamed:@"StarEmptyLarge.png"]];
                rateViewTmp.padding = 5;
                rateViewTmp.alignment = RateViewAlignmentCenter;
                rateViewTmp.editable = YES;
                rateViewTmp.delegate = self;
                rateViewTmp.rate = [self checkRatingValue];
                _rateView = rateViewTmp;
                [contentView addSubview:_rateView];
                [rateViewTmp release];
                
                // Comment Label
                UILabel *commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(_rateView.frame.origin.x, _rateView.frame.origin.y + _rateView.frame.size.height + kDefaultComponentPadding, self.view.bounds.size.width - (15*2), 20)];
                commentLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
                commentLabel.textColor = kDefaultBlackColor;
                commentLabel.backgroundColor = [UIColor clearColor];
                commentLabel.numberOfLines = 0;
                commentLabel.lineBreakMode = UILineBreakModeWordWrap;
                commentLabel.text = [Language get:@"comment" alter:nil];
                [contentView addSubview:commentLabel];
                
                // Comment View
                KTTextView *textViewTmp = [[KTTextView alloc] initWithFrame:CGRectMake(commentLabel.frame.origin.x, commentLabel.frame.origin.y + commentLabel.frame.size.height + kDefaultComponentPadding, self.view.bounds.size.width - (15*2), 100)];
                textViewTmp.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
                textViewTmp.delegate = self;
                textViewTmp.text = sharedData.commentValue;
                _textView = textViewTmp;
                [contentView addSubview:_textView];
                [textViewTmp release];
                
                // Max Char Label
                UILabel *maxCharLabel = [[UILabel alloc] initWithFrame:CGRectMake(_textView.frame.origin.x, _textView.frame.origin.y + _textView.frame.size.height, self.view.bounds.size.width - (15*2), 20)];
                maxCharLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSmallSize];
                maxCharLabel.textColor = kDefaultBlackColor;
                maxCharLabel.backgroundColor = [UIColor clearColor];
                maxCharLabel.numberOfLines = 0;
                maxCharLabel.lineBreakMode = UILineBreakModeWordWrap;
                maxCharLabel.text = [Language get:@"max_char_comment" alter:nil];
                [contentView addSubview:maxCharLabel];
                
                buttonDetail = createButtonWithFrame(CGRectMake(maxCharLabel.frame.origin.x,
                                                                maxCharLabel.frame.origin.y + maxCharLabel.frame.size.height + kDefaultComponentPadding,
                                                                100,
                                                                kDefaultButtonHeight),
                                                     [[Language get:@"submit" alter:nil] uppercaseString],
                                                     buttonFont,
                                                     kDefaultWhiteColor,
                                                     colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                     colorWithHexString(sharedData.profileData.themesModel.menuFeature));
                buttonDetail.tag = i;
                [buttonDetail addTarget:self action:@selector(performSubmitRating:) forControlEvents:UIControlEventTouchUpInside];
            }
            else {
                buttonDetail = createButtonWithFrame(CGRectMake(descLabel.frame.origin.x,
                                                                          descLabel.frame.origin.y + descLabel.frame.size.height + kDefaultComponentPadding,
                                                                          100,
                                                                          kDefaultButtonHeight),
                                                               [[Language get:@"detail" alter:nil] uppercaseString],
                                                               buttonFont,
                                                               kDefaultWhiteColor,
                                                               colorWithHexString(sharedData.profileData.themesModel.menuColor),
                                                               colorWithHexString(sharedData.profileData.themesModel.menuFeature));
                buttonDetail.tag = i;
                [buttonDetail addTarget:self action:@selector(performShow:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            [contentView addSubview:buttonDetail];
            
            /*
             * Adjust Height Content
             */
            frame = contentView.frame;
            
            if ([promoModel.href isEqualToString:@"#rating4G"]) {
                frame.size.height = buttonDetail.frame.origin.y + buttonDetail.frame.size.height + 180;
            }
            else if (i == [_content count] - 1) {
                frame.size.height = buttonDetail.frame.origin.y + buttonDetail.frame.size.height + 30;
            }
            else {
                frame.size.height = buttonDetail.frame.origin.y + buttonDetail.frame.size.height + 15;
            }
            
            contentView.frame = frame;
            
            [contentView addSubview:descLabel];
            
            [accordion addHeader:button withView:contentView];
        }
        else if (accordionType == ABOUT_ACC) {
            ContactUsModel *contactUsModel = [self.content objectAtIndex:i];
            FlatButton *button;
            if (i==0) {
                button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, screenWidth, kDefaultCellHeight)
                                                     title:contactUsModel.label
                                                 titleFont:titleFont
                                                titleColor:kDefaultWhiteColor
                                                   bgColor:colorWithHexString(sharedData.profileData.themesModel.accordionCurrentBg)
                                             selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionCurrentBg)];
            }
            else {
                button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, screenWidth, kDefaultCellHeight)
                                                     title:contactUsModel.label
                                                 titleFont:titleFont
                                                titleColor:kDefaultWhiteColor
                                                   bgColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)
                                             selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)];
            }
            
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, button.frame.size.height - 1, screenWidth, 1)];
            lineView.backgroundColor = kDefaultWhiteColor;
            [button addSubview:lineView];
            
            CGFloat height = 250.0;
            if (IS_IPAD) {
                if (i == 0) {
                    height = 180.0;
                }
                else {
                    height = 500.0;
                }
            }
            else {
                height = height;
            }
            
            UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 0, height)]; //320
            NSString *html = [NSString stringWithFormat:@"%@",contactUsModel.value];
            [webView loadHTMLString:[NSString stringWithFormat:@"%@",html] baseURL:nil];
            [webView setBackgroundColor:kDefaultWhiteColor];
            
            [accordion addHeader:button withView:webView];
        }
        else if (accordionType == FAQ_ACC) {
            
            // FAQ Content : get from Menu
            
            MenuModel *faqModel = [self.content objectAtIndex:i];
            FlatButton *button;
            if (i==0) {
                button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kDefaultCellHeight + 10.0)
                                                     title:faqModel.menuName
                                                 titleFont:titleFont
                                                titleColor:kDefaultWhiteColor
                                                   bgColor:colorWithHexString(sharedData.profileData.themesModel.accordionCurrentBg)
                                             selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionCurrentBg)];
            }
            else {
                button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kDefaultCellHeight + 10.0)
                                                     title:faqModel.menuName
                                                 titleFont:titleFont
                                                titleColor:kDefaultWhiteColor
                                                   bgColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)
                                             selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)];
            }
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, button.frame.size.height - 1, screenWidth, 1)];
            lineView.backgroundColor = kDefaultWhiteColor;
            [button addSubview:lineView];
            
            CGFloat height = 250.0;
            if (IS_IPAD) {
                if (i == 0) {
                    height = 180.0;
                }
                else {
                    height = 450.0;
                }
            }
            else {
                height = height;
            }
            
            UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 0, height)];
            NSString *styleFont = [NSString stringWithFormat:@"<div style=\"font-family:%@;font-size:12;\">",kDefaultFontLight];
            NSString *content = faqModel.desc;
            if (!!content && ![content isEqual:[NSNull null]]) {
                if ([content rangeOfString:@"<div>"].location == NSNotFound) {
                    content = [NSString stringWithFormat:@"<div>%@</div>",content];
                }
                
                content = [content stringByReplacingOccurrencesOfString:@"<div>"
                                                             withString:styleFont];
                content = [content stringByReplacingOccurrencesOfString:@"rn"
                                                             withString:@"<br>"];
                content = [content stringByReplacingOccurrencesOfString:@"\r\n"
                                                             withString:@"<br>"];
                content = [content stringByReplacingOccurrencesOfString:@"\""
                                                             withString:@""];
            }
            else {
                content = @"";
            }
            
            NSString *html = [NSString stringWithFormat:@"%@",content];
            [webView loadHTMLString:[NSString stringWithFormat:@"%@",html] baseURL:nil];
            [webView setBackgroundColor:kDefaultWhiteColor];
            
            [accordion addHeader:button withView:webView];
        }
        else {
            
            MenuModel *faqModel = [self.content objectAtIndex:i];
            FlatButton *button;
            if (i==0) {
                button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kDefaultCellHeight + 10.0)
                                                     title:faqModel.menuName
                                                 titleFont:titleFont
                                                titleColor:kDefaultWhiteColor
                                                   bgColor:colorWithHexString(sharedData.profileData.themesModel.accordionCurrentBg)
                                             selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionCurrentBg)];
            }
            else {
                button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kDefaultCellHeight + 10.0)
                                                     title:faqModel.menuName
                                                 titleFont:titleFont
                                                titleColor:kDefaultWhiteColor
                                                   bgColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)
                                             selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)];
            }
            // Adjust Title Label height
            CGSize labelSize = calculateExpectedSize(button.titleLbl, faqModel.menuName);
            CGRect labelFrame = button.titleLbl.frame;
            labelFrame.size.height = labelSize.height;
            labelFrame.origin.y = (button.frame.size.height - labelFrame.size.height)/2;
            button.titleLbl.frame = labelFrame;
            
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, button.frame.size.height - 1, self.view.bounds.size.width, 1)];
            lineView.backgroundColor = kDefaultWhiteColor;
            [button addSubview:lineView];
            
            UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
            contentView.backgroundColor = kDefaultWhiteColor;
            
            UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, self.view.bounds.size.width - (15*2), 200)];
            descLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
            descLabel.textColor = kDefaultBlackColor;
            descLabel.backgroundColor = [UIColor clearColor];
            descLabel.numberOfLines = 0;
            descLabel.lineBreakMode = UILineBreakModeWordWrap;
            descLabel.text = faqModel.desc;
            
            CGSize size = calculateExpectedSize(descLabel, descLabel.text);
            CGRect frame = [descLabel frame];
            frame.size.height = size.height;
            descLabel.frame = frame;
            
            /*
             * Adjust Height Content
             */
            frame = contentView.frame;
            //frame.size.height = descLabel.frame.size.height + (15*2);
            if (i == [_content count] - 1) {
                frame.size.height = descLabel.frame.origin.y + descLabel.frame.size.height + 30;
            }
            else {
                frame.size.height = descLabel.frame.origin.y + descLabel.frame.size.height + 15;
            }
            
            contentView.frame = frame;
            
            [contentView addSubview:descLabel];
            
            [accordion addHeader:button withView:contentView];
        }
    }
    
    [accordion setNeedsLayout];
    
    // Set this if you want to allow multiple selection
    [accordion setAllowsMultipleSelection:NO];
}

- (float)checkRatingValue {
    DataManager *sharedData = [DataManager sharedInstance];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([sharedData.profileData.autologin isEqualToString:@""] == YES) {
        // Manual Login
        [prefs setFloat:0.0 forKey:RATING_CACHE_KEY];
        return 0.0;
    }
    else {
        // Autologin
        float ratingValue = [prefs integerForKey:RATING_CACHE_KEY];
        if (ratingValue < 1) {
            NSLog(@"VALUE KOSONG");
            return 0.0;
        }
        else {
            NSLog(@"VALUE NOT NULL");
            return ratingValue;
        }
    }
}

#pragma mark - Rate View Delegate

- (void)rateView:(DYRateView *)rateView changedToNewRate:(NSNumber *)rate {
    //DataManager *sharedData = [DataManager sharedInstance];
    //sharedData.ratingValue = [rate intValue];
    
    [[NSUserDefaults standardUserDefaults] setFloat:[rate floatValue] forKey:RATING_CACHE_KEY];
}

#pragma mark - UITextView Delegate
/*
- (void)textViewDidBeginEditing:(UITextView *)textView {
    CGFloat targetY = textView.frame.origin.y - textView.frame.size.height;
    [accordion.scrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}*/

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }
    else {
        return textView.text.length + (text.length - range.length) <= 160;
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success)
    {
        // TODO : V1.9
        // Bug fixing -> App not redirecting to notification page after requesting inbox.
        // Only happens in this page
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            [notificationViewController createBarButtonItem:BACK];
            [self.navigationController pushViewController:notificationViewController animated:YES];
            [notificationViewController release];
            notificationViewController = nil;
        }
        else
        {
            NSString *reason = [result valueForKey:REASON_KEY];
            NSLog(@"reason = %@",reason);
            // TODO : V1.9
            // Prevent empty pop up notification
            if([reason length] > 0)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:reason
                                                               delegate:self
                                                      cancelButtonTitle:[Language get:@"close" alter:nil]
                                                      otherButtonTitles:nil];
                alert.tag = 10;
                alert.delegate = self;
                [alert show];
                [alert release];
            }
            //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
            //                                                        message:reason
            //                                                       delegate:self
            //                                              cancelButtonTitle:[Language get:@"close" alter:nil]
            //                                              otherButtonTitles:nil];
            //        alert.tag = 10;
            //        alert.delegate = self;
            //        [alert show];
            //        [alert release];
            
            // Clear Comment
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.commentValue = @"";
            _textView.text = sharedData.commentValue;
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        NSLog(@"reason = %@",reason);
        if([reason length] > 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:reason
                                                           delegate:self
                                                  cancelButtonTitle:[Language get:@"close" alter:nil]
                                                  otherButtonTitles:nil];
            alert.tag = 10;
            alert.delegate = self;
            [alert show];
            [alert release];
        }
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
//                                                        message:reason
//                                                       delegate:self
//                                              cancelButtonTitle:[Language get:@"close" alter:nil]
//                                              otherButtonTitles:nil];
//        alert.tag = 10;
//        alert.delegate = self;
//        [alert show];
//        [alert release];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10) {
        if (buttonIndex == 0) {
            [accordion setSelectedIndex:0];
        }
    }
}

@end
