//
//  CheckQuotaViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 11/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CheckQuotaViewController.h"
#import "DataManager.h"
#import "CheckUsageModel.h"
#import "PPUCell.h"
#import "QuotaCell.h"
#import "QuotaiPadCell.h"
#import "UnlimitedCell.h"
#import "FUPCell.h"
#import "FUPiPadCell.h"
#import "BorderCellBackground.h"
#import "AXISnetCommon.h"
#import "HumanReadableDataSizeHelper.h"
#import "ProgressBar.h"
#import "Constant.h"

#import "CommonCell.h"
#import "AXISnetCellBackground.h"
#import "SectionFooterView.h"
#import "SectionHeaderView.h"

#import "CheckUsageXLModel.h"
#import "PackageAllowanceModel.h"
#import "XLPackageBarCell.h"
#import "XLPackageAllowanceCell.h"
#import "XLPackageButtonCell.h"
#import "SubMenuViewController.h"
#import "ThankYouViewController.h"

#import "LoadMoreCell.h"
#import "RectBackground.h"

#import "NotificationViewController.h"
#import "ShareQuotaViewController.h"
#import "QuotaMeterViewController.h"

#import "QuotaCalculatorViewController.h"

@interface CheckQuotaViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (readwrite) CGFloat checkUsageCellHeight;
@property (nonatomic, retain) CheckUsageXLModel *selectedPackage;
@property (readwrite) BOOL showAll;

@end

@implementation CheckQuotaViewController

@synthesize isLevel2 = _isLevel2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        self.screenName = @"CheckQuota";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _checkUsageCellHeight = 0;
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)viewWillAppear:(BOOL)animated
{
//    // Google Analytics
//    [self activePage:@"Check Quota Page"];
    [super viewWillAppear:animated];
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.checkUsageXLData count] == 1) {
        _showAll = YES;
    }
    else {
        _showAll = NO;
    }
    
    if (_isLevel2) {
        [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    }
    else {
        [self createBarButtonItem:BACK_NOTIF];
    }
    
    [_theTableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    self.screenName = @"CheckQuotaiOS";
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"CheckQuotaiOS"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [[GAI sharedInstance] dispatch];
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    DataManager *sharedData = [DataManager sharedInstance];
    if (_showAll) {
        return [sharedData.checkUsageXLData count] + 1;
    }
    else {
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    else {
        DataManager *sharedData = [DataManager sharedInstance];
        CheckUsageXLModel *model = [sharedData.checkUsageXLData objectAtIndex:section - 1];
        if (_showAll) {
            if (model.showbar) {
                // TODO : V1.9
                if([model.fup length] > 0)
                {
                    return 3;
                }
                else
                {
                    return [model.packageAllowance count] + 2;
                }
//                return [model.packageAllowance count] + 2;
            }
            else {
                return [model.packageAllowance count] + 1;
            }
        }
        else {
            if (model.showbar) {
                return [model.packageAllowance count] + 3;
            }
            else {
                return [model.packageAllowance count] + 2;
            }
        }
    }
    return 0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return nil;
    }
    else {
        DataManager *sharedData = [DataManager sharedInstance];
        CheckUsageXLModel *model = [sharedData.checkUsageXLData objectAtIndex:indexPath.section - 1];
        
        if (_showAll) {
            if (model.showbar) {
                if (indexPath.row == 0) {
                    static NSString *CellIdentifierBar = @"BarView";
                    XLPackageBarCell *cell = (XLPackageBarCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierBar];
                    
                    if (cell == nil) {
                        cell = [[XLPackageBarCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierBar];
                        cell.userInteractionEnabled = NO;
                        cell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
                        
                        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    }
                    
                    // -- Usage Bar --
                    for (UIView *view in [cell.barView subviews])
                    {
                        [view removeFromSuperview];
                    }
                    
                    CGRect parentViewFrame = cell.barView.frame;
                    CGFloat padding = 0.0;
                    CGFloat width = 0.0;
                    if (IS_IPAD) {
                        padding = 15.0;
                        width = 1024 - (padding*2);
                    }
                    else {
                        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
                            padding = 15.0;
                            width = cell.frame.size.width - (padding*2);
                        }
                        else {
                            padding = 5.0;
                            width = cell.frame.size.width - (padding*6);
                        }
                    }
                    
                    parentViewFrame = CGRectMake(padding, 0, width, 55.0);
                    
                    ProgressBar *bar = [[ProgressBar alloc] initWithFrame:parentViewFrame withData:model];
                    [cell.barView addSubview:bar];
                    
                    return cell;
                }
                // TODO : V1.9
//                else if (indexPath.row == ([model.packageAllowance count] + 2) - 1)
                else if ((indexPath.row == ([model.packageAllowance count] + 2) - 1 && [model.fup length] == 0) || ([model.fup length] > 0 && indexPath.row == 2))
                {
                    static NSString *CellIdentifierButton = @"ButtonView";
                    XLPackageButtonCell *cell = (XLPackageButtonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierButton];
                    
                    if (cell == nil) {
                        cell = [[XLPackageButtonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierButton];
                        cell.userInteractionEnabled = YES;
                        
                        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        
                        [cell.stopButton addTarget:self
                                            action:@selector(stopButtonPressed:)
                                  forControlEvents:UIControlEventTouchUpInside];
                        
                        [cell.shareQuotaButton addTarget:self
                                                  action:@selector(shareQuotaButtonPressed:)
                                        forControlEvents:UIControlEventTouchUpInside];
                        
                        [cell.quotaMeterButton addTarget:self
                                                  action:@selector(quotaMeterButtonPressed:)
                                        forControlEvents:UIControlEventTouchUpInside];
                    }
                    cell.stopButton.tag = indexPath.section - 1;
                    cell.shareQuotaButton.tag = indexPath.section - 1;
                    cell.quotaMeterButton.tag = indexPath.section - 1;
                    
                    if ([model.shareQuota isEqualToString:@"0"]) {
                        cell.shareQuotaButton.hidden = YES;
                    }
                    else {
                        cell.shareQuotaButton.hidden = NO;
                    }
                    
                    if ([model.quotaMeter isEqualToString:@"0"]) {
                        cell.quotaMeterButton.hidden = YES;
                    }
                    else {
                        cell.quotaMeterButton.hidden = NO;
                    }
                    
                    return cell;
                }
                else
                {
                    static NSString *CellIdentifierAllowance = @"AllowanceView";
                    XLPackageAllowanceCell *cell = (XLPackageAllowanceCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierAllowance];
                    
                    if (cell == nil) {
                        cell = [[XLPackageAllowanceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierAllowance];
                        cell.userInteractionEnabled = NO;
                        
                        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    }
                    
                    // TODO : V1.9
                    //                    PackageAllowanceModel *allowanceModel;
                    //                    if([model.fup length] == 0)
                    //                    allowanceModel = [model.packageAllowance objectAtIndex:indexPath.row - 1];
                    @try
                    {
                        PackageAllowanceModel *allowanceModel = [model.packageAllowance objectAtIndex:indexPath.row - 1];
                        CGFloat remainingPercent = [[[allowanceModel.remainingPercent componentsSeparatedByString:@"%"] objectAtIndex:0] floatValue];
                        
                        // FUP wording for unlimited package that reached FUP
                        if([model.fup length] > 0 && remainingPercent <= 0 && model.isUnlimited)
                        {
                            cell.allowanceName.text = model.fup;
                            cell.remaining.text = @"";
                            cell.legendView.backgroundColor = [UIColor clearColor];
                        }
                        else
                        {
                            cell.allowanceName.text = allowanceModel.name;
                            cell.legendView.backgroundColor = legendColorForBar(indexPath.row - 1);
                            if(model.isUnlimited)
                            {
                                cell.remaining.text = allowanceModel.usage;
                                // green color
                                cell.legendView.backgroundColor = legendColorForBar(0);
                            }
                            else
                            {
                                cell.remaining.text = allowanceModel.remaining;
                            }
                        }
                    }
                    @catch (NSException *exception) {
                        NSLog(@"exception %@", [exception reason]);
                    }
//                    cell.allowanceName.text = allowanceModel.name;
//                    cell.remaining.text = allowanceModel.remaining;
//                    cell.legendView.backgroundColor = legendColorForBar(indexPath.row - 1);
                    
                    return cell;
                }

            }
            else {
                
                if (indexPath.row == ([model.packageAllowance count] + 1) - 1) {
                    static NSString *CellIdentifierButton = @"ButtonView";
                    XLPackageButtonCell *cell = (XLPackageButtonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierButton];
                    
                    if (cell == nil) {
                        cell = [[XLPackageButtonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierButton];
                        cell.userInteractionEnabled = YES;
                        
                        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        
                        [cell.stopButton addTarget:self
                                            action:@selector(stopButtonPressed:)
                                  forControlEvents:UIControlEventTouchUpInside];
                        
                        [cell.shareQuotaButton addTarget:self
                                                  action:@selector(shareQuotaButtonPressed:)
                                        forControlEvents:UIControlEventTouchUpInside];
                        
                        [cell.quotaMeterButton addTarget:self
                                                  action:@selector(quotaMeterButtonPressed:)
                                        forControlEvents:UIControlEventTouchUpInside];
                    }
                    cell.stopButton.tag = indexPath.section - 1;
                    [cell.stopButton setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuFeature))
                                               forState:UIControlStateNormal];
                    [cell.stopButton setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuColor))
                     
                                               forState:UIControlStateHighlighted];
                    
                    cell.shareQuotaButton.tag = indexPath.section - 1;
                    [cell.shareQuotaButton setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuFeature))
                                               forState:UIControlStateNormal];
                    [cell.shareQuotaButton setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuColor))
                     
                                               forState:UIControlStateHighlighted];
                    
                    cell.quotaMeterButton.tag = indexPath.section - 1;
                    [cell.quotaMeterButton setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuFeature))
                                                     forState:UIControlStateNormal];
                    [cell.quotaMeterButton setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuColor))
                     
                                                     forState:UIControlStateHighlighted];
                    
                    if ([model.shareQuota isEqualToString:@"0"]) {
                        cell.shareQuotaButton.hidden = YES;
                    }
                    else {
                        cell.shareQuotaButton.hidden = NO;
                    }
                    
                    if ([model.quotaMeter isEqualToString:@"0"]) {
                        cell.quotaMeterButton.hidden = YES;
                    }
                    else {
                        cell.quotaMeterButton.hidden = NO;
                    }
                    
                    return cell;
                }
                else {
                    static NSString *CellIdentifierAllowance = @"AllowanceView";
                    XLPackageAllowanceCell *cell = (XLPackageAllowanceCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierAllowance];
                    
                    if (cell == nil) {
                        cell = [[XLPackageAllowanceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierAllowance];
                        cell.userInteractionEnabled = NO;
                        
                        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    }
                    
                    PackageAllowanceModel *allowanceModel = [model.packageAllowance objectAtIndex:indexPath.row];
                    cell.allowanceName.text = allowanceModel.name;
                    cell.remaining.text = allowanceModel.remaining;
                    cell.legendView.backgroundColor = legendColorForBar(indexPath.row);
                    
                    return cell;
                }

            }
        }
        else {
            if (model.showbar) {
                if (indexPath.row == 0) {
                    static NSString *CellIdentifierBar = @"BarView";
                    XLPackageBarCell *cell = (XLPackageBarCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierBar];
                    
                    if (cell == nil) {
                        cell = [[XLPackageBarCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierBar];
                        cell.userInteractionEnabled = NO;
                        cell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
                        
                        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    }
                    
                    // -- Usage Bar --
                    for (UIView *view in [cell.barView subviews])
                    {
                        [view removeFromSuperview];
                    }
                    
                    CGRect parentViewFrame = cell.barView.frame;
                    CGFloat padding = 0.0;
                    CGFloat width = 0.0;
                    if (IS_IPAD) {
                        padding = 15.0;
                        width = 1024 - (padding*2);
                    }
                    else {
                        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
                            padding = 15.0;
                            width = cell.frame.size.width - (padding*2);
                        }
                        else {
                            padding = 5.0;
                            width = cell.frame.size.width - (padding*6);
                        }
                    }
                    
                    parentViewFrame = CGRectMake(padding, 0, width, 65.0); //55
                    
                    ProgressBar *bar = [[ProgressBar alloc] initWithFrame:parentViewFrame withData:model];
                    [cell.barView addSubview:bar];
                    
                    return cell;
                }
                
                else if (indexPath.row == ([model.packageAllowance count] + 2) - 1) {
                    static NSString *CellIdentifierButton = @"ButtonView";
                    XLPackageButtonCell *cell = (XLPackageButtonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierButton];
                    
                    if (cell == nil) {
                        cell = [[XLPackageButtonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierButton];
                        cell.userInteractionEnabled = YES;
                        
                        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        
                        [cell.stopButton addTarget:self
                                            action:@selector(stopButtonPressed:)
                                  forControlEvents:UIControlEventTouchUpInside];
                        
                        [cell.shareQuotaButton addTarget:self
                                                  action:@selector(shareQuotaButtonPressed:)
                                        forControlEvents:UIControlEventTouchUpInside];
                        
                        [cell.quotaMeterButton addTarget:self
                                                  action:@selector(quotaMeterButtonPressed:)
                                        forControlEvents:UIControlEventTouchUpInside];
                    }
                    cell.stopButton.tag = indexPath.section - 1;
                    cell.shareQuotaButton.tag = indexPath.section - 1;
                    cell.quotaMeterButton.tag = indexPath.section - 1;
                    
                    if ([model.shareQuota isEqualToString:@"0"]) {
                        cell.shareQuotaButton.hidden = YES;
                    }
                    else {
                        cell.shareQuotaButton.hidden = NO;
                    }
                    
                    if ([model.quotaMeter isEqualToString:@"0"]) {
                        cell.quotaMeterButton.hidden = YES;
                    }
                    else {
                        cell.quotaMeterButton.hidden = NO;
                    }
                    
                    return cell;
                }
                
                // Load More
                else if (indexPath.row == ([model.packageAllowance count] + 3) - 1) {
                    static NSString *CellIdentifierButton = @"LoadMoreView";
                    LoadMoreCell *cell = (LoadMoreCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierButton];
                    
                    if (cell == nil) {
                        cell = [[LoadMoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierButton];
                        cell.userInteractionEnabled = YES;
                        
                        cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                      withBasicColor:kDefaultDarkGrayColor
                                                                   withSelectedColor:kDefaultDarkGrayColor] autorelease];
                        
                        cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                              withBasicColor:kDefaultDarkGrayColor
                                                                           withSelectedColor:kDefaultDarkGrayColor] autorelease];
                    }
                    
                    cell.titleLabel.text = [Language get:@"other_package" alter:nil];
                    cell.arrowLabel.text = @"e";
                    
                    return cell;
                }
                
                else {
                    static NSString *CellIdentifierAllowance = @"AllowanceView";
                    XLPackageAllowanceCell *cell = (XLPackageAllowanceCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierAllowance];
                    
                    if (cell == nil) {
                        cell = [[XLPackageAllowanceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierAllowance];
                        cell.userInteractionEnabled = NO;
                        
                        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    }
                    
                    PackageAllowanceModel *allowanceModel = [model.packageAllowance objectAtIndex:indexPath.row - 1];
                    cell.allowanceName.text = allowanceModel.name;
                    cell.remaining.text = allowanceModel.remaining;
                    cell.legendView.backgroundColor = legendColorForBar(indexPath.row - 1);
                    
                    return cell;
                }
            }
            else {
                
                
                if (indexPath.row == ([model.packageAllowance count] + 1) - 1) {
                    static NSString *CellIdentifierButton = @"ButtonView";
                    XLPackageButtonCell *cell = (XLPackageButtonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierButton];
                    
                    if (cell == nil) {
                        cell = [[XLPackageButtonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierButton];
                        cell.userInteractionEnabled = YES;
                        
                        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        
                        [cell.stopButton addTarget:self
                                            action:@selector(stopButtonPressed:)
                                  forControlEvents:UIControlEventTouchUpInside];
                        
                        [cell.shareQuotaButton addTarget:self
                                                  action:@selector(shareQuotaButtonPressed:)
                                        forControlEvents:UIControlEventTouchUpInside];
                        
                        [cell.quotaMeterButton addTarget:self
                                                  action:@selector(quotaMeterButtonPressed:)
                                        forControlEvents:UIControlEventTouchUpInside];
                    }
                    cell.stopButton.tag = indexPath.section - 1;
                    cell.shareQuotaButton.tag = indexPath.section - 1;
                    cell.quotaMeterButton.tag = indexPath.section - 1;
                    
                    if ([model.shareQuota isEqualToString:@"0"]) {
                        cell.shareQuotaButton.hidden = YES;
                    }
                    else {
                        cell.shareQuotaButton.hidden = NO;
                    }
                    
                    if ([model.quotaMeter isEqualToString:@"0"]) {
                        cell.quotaMeterButton.hidden = YES;
                    }
                    else {
                        cell.quotaMeterButton.hidden = NO;
                    }
                    
                    return cell;
                }
                
                // Load More
                else if (indexPath.row == ([model.packageAllowance count] + 2) - 1) {
                    static NSString *CellIdentifierButton = @"LoadMoreView";
                    LoadMoreCell *cell = (LoadMoreCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierButton];
                    
                    if (cell == nil) {
                        cell = [[LoadMoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierButton];
                        cell.userInteractionEnabled = YES;
                        
                        cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                      withBasicColor:kDefaultDarkGrayColor
                                                                   withSelectedColor:kDefaultDarkGrayColor] autorelease];
                        
                        cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                              withBasicColor:kDefaultDarkGrayColor
                                                                           withSelectedColor:kDefaultDarkGrayColor] autorelease];
                    }
                    
                    cell.titleLabel.text = [Language get:@"other_package" alter:nil];
                    cell.arrowLabel.text = @"e";
                    
                    return cell;
                }
                
                else {
                    static NSString *CellIdentifierAllowance = @"AllowanceView";
                    XLPackageAllowanceCell *cell = (XLPackageAllowanceCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierAllowance];
                    
                    if (cell == nil) {
                        cell = [[XLPackageAllowanceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierAllowance];
                        cell.userInteractionEnabled = NO;
                        
                        cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                        cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    }
                    
                    PackageAllowanceModel *allowanceModel = [model.packageAllowance objectAtIndex:indexPath.row];
                    cell.allowanceName.text = allowanceModel.name;
                    cell.remaining.text = allowanceModel.remaining;
                    cell.legendView.backgroundColor = legendColorForBar(indexPath.row);
                    
                    return cell;
                }
            }
        }
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return [Language get:@"my_package" alter:nil];
    }
    else {
        return @"";
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        DataManager *sharedData = [DataManager sharedInstance];
        CheckUsageXLModel *model = [sharedData.checkUsageXLData objectAtIndex:indexPath.section - 1];
        if (model.showbar) {
            return 65.0; //55
        }
        else {
            return 30.0;
        }
    }
    
    else {
        DataManager *sharedData = [DataManager sharedInstance];
        CheckUsageXLModel *model = [sharedData.checkUsageXLData objectAtIndex:indexPath.section - 1];
        if (model.showbar)
        {
            // TODO : V1.9
            // Adjust cell height when FUP reached
//            if (indexPath.row == ([model.packageAllowance count] + 2) - 1) {
            if((indexPath.row == ([model.packageAllowance count] + 2) - 1 && [model.fup length] == 0) || ([model.fup length] > 0 && indexPath.row == 2))
            {
                return kDefaultCellHeight;
            }
            else {
                return 30.0;
            }
        }
        else {
            if (indexPath.row == ([model.packageAllowance count] + 1) - 1) {
                return kDefaultCellHeight;
            }
            else {
                return 30.0;
            }
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    //NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    
    SectionHeaderView *headerView;
    if (section == 0) {
        /*
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                withLabelForXL:sectionTitle
                                                isFirstSection:YES] autorelease];*/
        
        NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                     withTitle:sectionTitle
                                                  withSubtitle:[Language get:@"buy_package" alter:nil]
                                                isFirstSection:YES
                                              isZeroTopPadding:NO] autorelease];
        
        [headerView.packageButton addTarget:self action:@selector(performPackageList) forControlEvents:UIControlEventTouchUpInside];
        
        // TODO : BUG FIX
        // Quota calculator button doesnt work
        DataManager *sharedData = [DataManager sharedInstance];
        if (sharedData.haveQuotaCalculator) {
            [headerView.quotaCalculatorButton addTarget:self action:@selector(performQuotaCalculator) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    else {
        /*
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                withLabelForXL:sectionTitle
                                                isFirstSection:NO] autorelease];*/
        
        DataManager *sharedData = [DataManager sharedInstance];
        CheckUsageXLModel *model = [sharedData.checkUsageXLData objectAtIndex:section - 1];
        /*
        NSArray *expDate = [model.expDate componentsSeparatedByString:@"-"];
        NSString *expString = @"";
        for (int i = 0; i < [expDate count]; i++) {
            NSString *string = [expDate objectAtIndex:i];
            [expString stringByAppendingString:string];
        }
        expString = changeDateFormat(expString);*/
        
        NSString *expString = changeDateFormatForPackage(model.expDate);
        
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                     withTitle:model.name
                                                  withSubtitle:expString
                                                isFirstSection:NO
                                              isZeroTopPadding:NO] autorelease];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return kDefaultCellHeight*2;
    }
    else {
        return kDefaultCellHeight;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_showAll == NO) {
        DataManager *sharedData = [DataManager sharedInstance];
        CheckUsageXLModel *model = [sharedData.checkUsageXLData objectAtIndex:indexPath.section - 1];
        if (model.showbar) {
            if (indexPath.row == ([model.packageAllowance count] + 3) - 1) {
                _showAll = YES;
                [_theTableView reloadData];
            }
        }
        else {
            if (indexPath.row == ([model.packageAllowance count] + 2) - 1) {
                _showAll = YES;
                [_theTableView reloadData];
            }
        }
        
    }
}

#pragma mark - Selector

// TODO : BUG FIX
// Quota calculator button doesnt work
- (void)performQuotaCalculator
{
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = QUOTA_CALCULATOR_REQ;
    [request quotaCalculator];
}

- (void)performPackageList {
    SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
    
    DataManager *dataManager = [DataManager sharedInstance];
//    MenuModel *selectedMenu = dataManager.packageMenu;
//    dataManager.parentId = selectedMenu.menuId;
//    dataManager.menuModel = selectedMenu;
//    
//    subMenuVC.parentId = selectedMenu.menuId;
//    subMenuVC.groupDesc = selectedMenu.desc;
    
    subMenuVC.parentMenuModel = dataManager.packageMenu;
    
    subMenuVC.isLevel2 = NO;
    subMenuVC.levelTwoType = COMMON;
    subMenuVC.grayHeaderTitle = dataManager.packageMenu.menuName;
    
    [self.navigationController pushViewController:subMenuVC animated:YES];
    subMenuVC = nil;
    [subMenuVC release];
}

- (void)stopButtonPressed:(id)sender {
    UIButton *button = (UIButton *)sender;
    DataManager *dataManager = [DataManager sharedInstance];
    _selectedPackage = [dataManager.checkUsageXLData objectAtIndex:button.tag];
    
    NSString *message = [NSString stringWithFormat:@"%@ %@ %@ %@\n%@",
                         [Language get:@"stop_package_confirmation_1" alter:nil],
                         _selectedPackage.name,
                         [Language get:@"stop_package_confirmation_2" alter:nil],
                         [Language get:@"stop_package_confirmation_3" alter:nil],
                         [Language get:@"ask_continue" alter:nil]];
    //NSLog(@"%@",message);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                          otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
    alert.tag = 1;
    [alert show];
    [alert release];
}

- (CGFloat)calculateCellHeight {
    DataManager *sharedData = [DataManager sharedInstance];
    CheckUsageModel *checkUsageModel = [sharedData.checkUsageData objectAtIndex:0];
    // PPU
    if ([checkUsageModel.packageType isEqualToString:@"0"] ||
        [checkUsageModel.packageType isEqualToString:@"2"]) {
        
        CGFloat leftPadding = 20.0;
        CGFloat topPadding = 5.0;
        CGFloat width = (self.view.frame.size.width - (leftPadding*4)) / 2; //150
        CGFloat height = 20.0;
        CGFloat deduction = 5.0;
        
        CGRect frame = CGRectMake(leftPadding,
                                  topPadding,
                                  width,
                                  height);
        
        UILabel *leftTitleLabel = [[UILabel alloc] initWithFrame:frame];
        frame = CGRectMake(leftPadding,
                           leftTitleLabel.frame.origin.y + leftTitleLabel.frame.size.height - deduction,
                           width,
                           height);
        UILabel *leftValueLabel = [[UILabel alloc] initWithFrame:frame];
        leftValueLabel.textAlignment = UITextAlignmentLeft;
        leftValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        leftValueLabel.numberOfLines = 0;
        leftValueLabel.lineBreakMode = UILineBreakModeWordWrap;
        NSString *packageNameValue = checkUsageModel.packageName;
        CGSize packageNameValueSize = calculateExpectedSize(leftValueLabel,packageNameValue);
        frame = leftValueLabel.frame;
        frame.size.height = packageNameValueSize.height;
        leftValueLabel.frame = frame;
        
        frame = CGRectMake(leftPadding,
                           leftValueLabel.frame.origin.y + leftValueLabel.frame.size.height + topPadding,
                           width,
                           height);
        leftTitleLabel.frame = frame;
        
        frame = CGRectMake(leftPadding,
                           leftTitleLabel.frame.origin.y + leftTitleLabel.frame.size.height - deduction,
                           width,
                           height);
        leftValueLabel.frame = frame;
        
        unsigned long long ullvalue = strtoull([checkUsageModel.usage UTF8String], NULL, 0);
        NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
        NSString *strUsage = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:usageNumber];
        NSString *usageValue = strUsage;
        CGSize usageValueSize = calculateExpectedSize(leftValueLabel, usageValue);
        frame = leftValueLabel.frame;
        frame.size.height = usageValueSize.height;
        leftValueLabel.frame = frame;
        
        if ([checkUsageModel.info length] > 0) {
            frame = CGRectMake(leftPadding,
                               leftValueLabel.frame.origin.y + leftValueLabel.frame.size.height + topPadding,
                               width,
                               height);
            leftTitleLabel.frame = frame;
            
            frame = CGRectMake(leftPadding,
                               leftTitleLabel.frame.origin.y + leftTitleLabel.frame.size.height - deduction,
                               (width*2) - (leftPadding*2),
                               height);
            leftValueLabel.frame = frame;
            NSString *infoValue = checkUsageModel.info;
            CGSize infoValueSize = calculateExpectedSize(leftValueLabel, infoValue);
            frame = leftValueLabel.frame;
            frame.size.height = infoValueSize.height;
            leftValueLabel.frame = frame;
        }
        
        _checkUsageCellHeight = leftValueLabel.frame.origin.y + leftValueLabel.frame.size.height + (topPadding*2);
        return _checkUsageCellHeight;
    }
    else if ([checkUsageModel.packageType isEqualToString:@"1"]) {
        if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            return 131;
        }
        else {
            return 180;
        }
    }
    else if ([checkUsageModel.packageType isEqualToString:@"3"]) {
        if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            return 131;
        }
        else {
            return 190; //180
        }
    }
    else {
        return 90; // 70
    }
}

- (void)performStopPackage {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = STOP_PACKAGE_REQ;
    [request stopPackage:@"" withUnreg:_selectedPackage.idService];
}

- (void)shareQuotaButtonPressed:(id)sender {
    UIButton *button = (UIButton *)sender;
    DataManager *dataManager = [DataManager sharedInstance];
    _selectedPackage = [dataManager.checkUsageXLData objectAtIndex:button.tag];
    
    [self performShareQuota];
}

- (void)quotaMeterButtonPressed:(id)sender {
    UIButton *button = (UIButton *)sender;
    DataManager *dataManager = [DataManager sharedInstance];
    _selectedPackage = [dataManager.checkUsageXLData objectAtIndex:button.tag];
    
    [self performQuotaMeter];
}

- (void)performShareQuota {
    ShareQuotaViewController *shareQuotaViewController = [[ShareQuotaViewController alloc] initWithNibName:@"ShareQuotaViewController" bundle:nil];
    shareQuotaViewController.dataPackage = _selectedPackage.name;
    shareQuotaViewController.serviceIdA = _selectedPackage.idService;
    shareQuotaViewController.quotaList = _selectedPackage.shareQuotaList;
    [self.navigationController pushViewController:shareQuotaViewController animated:YES];
    shareQuotaViewController = nil;
    [shareQuotaViewController release];
}

- (void)performQuotaMeter {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = QUOTA_METER_REQ;
    [request quotaMeter];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            [self performStopPackage];
        }
        else {
        }
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == QUOTA_CALCULATOR_REQ) {
            QuotaCalculatorViewController *packageViewController = [[QuotaCalculatorViewController alloc] initWithNibName:@"QuotaCalculatorViewController" bundle:nil];
            
            [self.navigationController pushViewController:packageViewController animated:YES];
            packageViewController = nil;
            [packageViewController release];
        }
        
        if (type == STOP_PACKAGE_REQ) {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            
            NSString *reason = [result valueForKey:REASON_KEY];
            thankYouViewController.info = reason;
            
            MenuModel *menu = [[MenuModel alloc] init];
            menu.groupName = [Language get:@"stop_package" alter:nil];
            thankYouViewController.menuModel = menu;
            
            //thankYouViewController.info = [Language get:@"stop_package_thank_you" alter:nil];
            
            thankYouViewController.headerTitleMaster = self.headerTitleMaster;
            thankYouViewController.headerIconMaster = self.headerIconMaster;
            
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            
            [menu release];
            
            thankYouViewController = nil;
            [thankYouViewController release];
            
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
        
        if (type == QUOTA_METER_REQ) {
            // Action for Quota Meter --> Show Quota Meter UI
            QuotaMeterViewController *quotaMeterViewController = [[QuotaMeterViewController alloc] initWithNibName:@"QuotaMeterViewController" bundle:nil];
            [self.navigationController pushViewController:quotaMeterViewController animated:YES];
            quotaMeterViewController = nil;
            [quotaMeterViewController release];
        }
    }
    else {
        
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
