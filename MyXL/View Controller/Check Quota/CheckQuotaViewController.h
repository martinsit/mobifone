//
//  CheckQuotaViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 11/5/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface CheckQuotaViewController : BasicViewController {
    BOOL _isLevel2;
}

@property (readwrite) BOOL isLevel2;

@end
