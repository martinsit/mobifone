//
//  XtraPromoWinnerListViewController.m
//  MyXL
//
//  Created by tyegah on 7/31/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XtraPromoWinnerListViewController.h"
#import "XtraPromoWinnerListTableCell.h"
#import "XtraPromoWinnerFirstTableCell.h"
#import "PaymentChoiceViewController.h"
#import "WinnerListModel.h"

@interface XtraPromoWinnerListViewController ()
@property (nonatomic, retain) UIButton *btnWinnerInfo;
@property (nonatomic, retain) UIButton *btnReloadBalance;
@property (nonatomic, retain) UIButton *btnTermsAndCons;
@end

@implementation XtraPromoWinnerListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib
    _tblView.dataSource = self;
    _tblView.delegate = self;
    _tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tblView.allowsSelection = NO;
    [_tblView registerNib:[UINib nibWithNibName:@"XtraPromoWinnerListTableCell" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"WinnerListCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"XtraPromoWinnerFirstTableCell" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"WinnerFirstCell"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DataManager *sharedData = [DataManager sharedInstance];
    if([sharedData.winnerListData count] > 0)
        return [sharedData.winnerListData count];
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataManager *sharedData = [DataManager sharedInstance];
    if([sharedData.winnerListData count] > 0)
    {
        WinnerListModel *winnerObj = [[WinnerListModel alloc] init];
        winnerObj = [sharedData.winnerListData objectAtIndex:indexPath.row];
        if(indexPath.row == 0)
        {
            XtraPromoWinnerFirstTableCell *cell = (XtraPromoWinnerFirstTableCell*)[tableView dequeueReusableCellWithIdentifier:@"WinnerFirstCell"];
            
            if (cell == nil) {
                cell = [[[XtraPromoWinnerFirstTableCell alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 200)] autorelease];
            }
            cell.lblWinnerName.text = winnerObj.name;
            cell.lblPhoneNo.text = winnerObj.msisdn;
            cell.lblDate.text = [AXISnetCommon formatDateFromString:winnerObj.eventdate];
            if([winnerObj.urlImage length] > 0)
                cell.imgView.imageURL = [NSURL URLWithString:winnerObj.urlImage];
            return cell;
        }
        else
        {
            XtraPromoWinnerListTableCell *cell = (XtraPromoWinnerListTableCell*)[tableView dequeueReusableCellWithIdentifier:@"WinnerListCell"];
            if (cell == nil)
            {
                cell = [[[XtraPromoWinnerListTableCell alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 100)] autorelease];
            }
            cell.lblWinnerName.text = winnerObj.name;
            cell.lblWinnerPhoneNo.text = winnerObj.msisdn;
            cell.lblWinnerDate.text = [AXISnetCommon formatDateFromString:winnerObj.eventdate];
            if([winnerObj.urlImage length] > 0)
                cell.winnerImgView.imageURL = [NSURL URLWithString:winnerObj.urlImage];
            return cell;
        }
    }
    else
    {
        NSString *cellID = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID] autorelease];
        }
        
        UILabel *lblMessage = [[[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetHeight(cell.contentView.frame)/2, CGRectGetWidth(cell.contentView.frame) - 16, 40)] autorelease];
        lblMessage.font = [UIFont boldSystemFontOfSize:30];
        lblMessage.textAlignment = NSTextAlignmentCenter;
        lblMessage.text = sharedData.winnerMessage;
        [cell.contentView addSubview:lblMessage];
        return cell;
    }
    return nil;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 40)];
    headerView.backgroundColor = kDefaultBaseColor;
    UILabel *lblWinner = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, CGRectGetWidth(headerView.frame) - 30 - 8, 21)];
    lblWinner.font = [UIFont fontWithName:kDefaultFontLight size:15];
    lblWinner.textColor = kDefaultBlueColor;
    lblWinner.text = @"Daftar Pemenang";
    [headerView addSubview:lblWinner];
    [lblWinner release];
    lblWinner = nil;
    return headerView;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 160)];
    footerView.backgroundColor = [UIColor whiteColor];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(footerView.frame), 21)];
    lblTitle.font = [UIFont fontWithName:kDefaultFontLight size:12];
    lblTitle.textColor = [UIColor blackColor];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.text = @"Daftar lengkap pemenang di";
    [footerView addSubview:lblTitle];
    [lblTitle release];
    lblTitle = nil;
    
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    _btnWinnerInfo = createButtonWithFrame(CGRectMake(20,
                                                        30,
                                                        CGRectGetWidth(footerView.frame) - 40,
                                                        30),
                                             @"60MAZDA2.XL.CO.ID",
                                             buttonFont,
                                             kDefaultBlueColor,
                                             footerView.backgroundColor,
                                             footerView.backgroundColor);
    
    [_btnWinnerInfo addTarget:self action:@selector(btnWinnerInfoClicked:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_btnWinnerInfo];

    UIButton *button = createButtonWithFrame(CGRectMake(20,
                                                        CGRectGetMaxY(_btnWinnerInfo.frame) + 10,
                                                        CGRectGetWidth(footerView.frame) - 40,
                                                        40),
                                             @"ISI PULSA & MENANGKAN UNDIANNYA",
                                             buttonFont,
                                             kDefaultBlackColor,
                                             kDefaultYellowColor,
                                             kDefaultYellowColor);
    
    [button addTarget:self action:@selector(btnReloadBalanceClicked:) forControlEvents:UIControlEventTouchUpInside];
    _btnReloadBalance = button;
    [footerView addSubview:_btnReloadBalance];
    
    UIView *termsAndConView = [[[UIView alloc] initWithFrame:CGRectMake(20,
                                                                       CGRectGetMaxY(_btnReloadBalance.frame) + 10,
                                                                       CGRectGetWidth(footerView.frame) - 40,
                                                                       25)] autorelease];
    
    UILabel *lblTermsAndCons = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(termsAndConView.frame), CGRectGetHeight(termsAndConView.frame))] autorelease];
    lblTermsAndCons.font = [UIFont fontWithName:kDefaultFont size:15];
    lblTermsAndCons.textAlignment = NSTextAlignmentCenter;
    lblTermsAndCons.text = @"Syarat & Ketentuan";
    lblTermsAndCons.textColor = kDefaultFontGrayColor;
    [termsAndConView addSubview:lblTermsAndCons];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(termsAndConsClicked)];
    [termsAndConView addGestureRecognizer:tapGesture];
    
    [footerView addSubview:termsAndConView];
    
    return footerView;
}

-(void)termsAndConsClicked
{
    [self showModalWebViewForURL:[NSURL URLWithString:XTRAPROMO_TERMSCONS_URL]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
        return 200;
    return 100;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 160;
}

-(void)btnWinnerInfoClicked:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // Screen tracking
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                          action:@"Cek"  // Event action (required)
                                                           label:@"Pemenang"          // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    
    [self showModalWebViewForURL:[NSURL URLWithString:@"http://60mazda2.xl.co.id"]];
}

-(void)showModalWebViewForURL:(NSURL *)url
{
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
    webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:webViewController animated:YES completion:NULL];
}

-(void)btnReloadBalanceClicked:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // Screen tracking
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                          action:@"Tertarik"  // Event action (required)
                                                           label:@"isi pulsa"          // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSArray *contentTmp = nil;
    NSDictionary *dict = [GroupingAndSorting doGrouping:sharedData.homeMenu];
    NSArray *objectsForMenu = [dict objectForKey:@"20"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"href==%@",@"#reload_pg"];
    NSArray *filteredArray = [objectsForMenu filteredArrayUsingPredicate:predicate];
    if([filteredArray count] > 0)
    {
        MenuModel *menuModel = [filteredArray objectAtIndex:0];
        contentTmp = [sharedData.menuData valueForKey:menuModel.menuId]; //-> 5 is menuID for reloadBalance
        // Grouping
        NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
        
        // Sorting
        NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
        
        NSArray *paymentMenu = [sortingResult allKeys];
        paymentMenu = [GroupingAndSorting doSortingByGroupId:paymentMenu];
        
        NSMutableArray *paymentMenuTmp = [[NSMutableArray alloc] init];
        for (int i=0; i<[paymentMenu count]; i++) {
            NSString *groupIdOfMenu = [paymentMenu objectAtIndex:i];
            NSArray *objectsForMenu = [sortingResult objectForKey:groupIdOfMenu];
            for (MenuModel *menu in objectsForMenu) {
                [paymentMenuTmp addObject:menu];
            }
        }
        
        PaymentChoiceViewController *subMenuVC = [[PaymentChoiceViewController alloc] initWithNibName:@"PaymentChoiceViewController" bundle:nil];
        subMenuVC.paymentMenu = paymentMenuTmp;
        subMenuVC.grayHeaderTitle = [Language get:@"balance_management" alter:nil];
        subMenuVC.blueHeaderTitle = menuModel.menuName;
        [subMenuVC createBarButtonItem:BACK];
        [self.navigationController pushViewController:subMenuVC animated:YES];
        [subMenuVC release];
        subMenuVC = nil;
    }
}

- (void)dealloc {
    [_tblView release];
    [_btnWinnerInfo release];
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.winnerListData = nil;
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTblView:nil];
    [self setBtnWinnerInfo:nil];
    [super viewDidUnload];
}
@end
