//
//  XtraPromoWinnerFirstTableCell.h
//  MyXL
//
//  Created by tyegah on 8/6/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface XtraPromoWinnerFirstTableCell : UITableViewCell
@property (retain, nonatomic) IBOutlet AsyncImageView *imgView;
@property (retain, nonatomic) IBOutlet UILabel *lblWinnerName;
@property (retain, nonatomic) IBOutlet UILabel *lblPhoneNo;
@property (retain, nonatomic) IBOutlet UILabel *lblDate;
@property (retain, nonatomic) IBOutlet UIView *wrapperView;

@end
