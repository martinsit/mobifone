//
//  XtraPromoWinnerFirstTableCell.m
//  MyXL
//
//  Created by tyegah on 8/6/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XtraPromoWinnerFirstTableCell.h"

@implementation XtraPromoWinnerFirstTableCell

- (void)awakeFromNib {
    // Initialization code
    self.wrapperView.layer.cornerRadius = 5;
    self.imgView.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_imgView release];
    [_lblWinnerName release];
    [_lblPhoneNo release];
    [_lblDate release];
    [_wrapperView release];
    [super dealloc];
}
@end
