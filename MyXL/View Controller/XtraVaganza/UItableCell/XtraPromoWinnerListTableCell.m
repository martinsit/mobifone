//
//  XtraPromoWinnerListTableCell.m
//  MyXL
//
//  Created by tyegah on 8/3/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XtraPromoWinnerListTableCell.h"

@implementation XtraPromoWinnerListTableCell

- (id)init {
    if(self = [super init]) {
        NSLog(@"Init called");
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupXIBWithFrame:frame];
    }
    return self;
}

-(void)setupXIBWithFrame:(CGRect)frame
{
    self = [[[NSBundle bundleForClass:[self class]]loadNibNamed:@"XtraPromoWinnerListTableCell" owner:self options:nil]firstObject];
    
    self.frame =  frame;
}

- (void)awakeFromNib {
    // Initialization code
    self.winnerImgView.layer.cornerRadius = self.winnerImgView.frame.size.width / 2;
    self.winnerImgView.clipsToBounds = YES;
    self.winnerImgView.layer.borderColor = kDefaultBlueColor.CGColor;
    self.winnerImgView.layer.borderWidth = 3;
    self.wrapperView.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_winnerImgView release];
    [_lblWinnerName release];
    [_lblWinnerPhoneNo release];
    [_lblWinnerDate release];
    [_wrapperView release];
    [super dealloc];
}
@end
