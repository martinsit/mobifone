//
//  XtraPromoWinnerListTableCell.h
//  MyXL
//
//  Created by tyegah on 8/3/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface XtraPromoWinnerListTableCell : UITableViewCell
@property (retain, nonatomic) UIView *displayView;
@property (retain, nonatomic) IBOutlet UIView *wrapperView;
@property (retain, nonatomic) IBOutlet AsyncImageView *winnerImgView;
@property (retain, nonatomic) IBOutlet UILabel *lblWinnerName;
@property (retain, nonatomic) IBOutlet UILabel *lblWinnerPhoneNo;
@property (retain, nonatomic) IBOutlet UILabel *lblWinnerDate;
@end
