//
//  XtraPromoWinnerListViewController.h
//  MyXL
//
//  Created by tyegah on 7/31/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BasicViewController.h"

@interface XtraPromoWinnerListViewController : BasicViewController<UITableViewDelegate, UITableViewDataSource>
@property (retain, nonatomic) IBOutlet UITableView *tblView;

@end
