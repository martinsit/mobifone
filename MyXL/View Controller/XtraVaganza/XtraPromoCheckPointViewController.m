//
//  XtraPromoCheckPointViewController.m
//  MyXL
//
//  Created by tyegah on 7/31/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XtraPromoCheckPointViewController.h"
#import "PaymentChoiceViewController.h"

@interface XtraPromoCheckPointViewController ()
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIView *wrapperView;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;
@property (retain, nonatomic) IBOutlet UILabel *lblTotalPoint;
@property (retain, nonatomic) IBOutlet UILabel *lblTxtPoint;
@property (retain, nonatomic) IBOutlet UILabel *lblSubtitle;
@property (retain, nonatomic) IBOutlet UILabel *lblDate;
@property (retain, nonatomic) IBOutlet UILabel *lblFooterTitle;
@property (retain, nonatomic) IBOutlet UIView *lineViewLeft;
@property (retain, nonatomic) IBOutlet UIView *lineViewRight;
@property (retain, nonatomic) IBOutlet UIView *timerHourView;
@property (retain, nonatomic) IBOutlet UILabel *lblTimerHour;
@property (retain, nonatomic) IBOutlet UIView *timerMinuteView;
@property (retain, nonatomic) IBOutlet UILabel *lblTimerMinute;
@property (retain, nonatomic) IBOutlet UIView *timerSecondView;
@property (retain, nonatomic) IBOutlet UILabel *lblTimerSecond;
@property (retain, nonatomic) IBOutlet UILabel *lblHour;
@property (retain, nonatomic) IBOutlet UILabel *lblMinute;
@property (retain, nonatomic) IBOutlet UILabel *lblDetik;
@property (nonatomic, retain) UIButton *btnReloadBalance;
@property (nonatomic, retain) UIButton *btnTermsAndCons;

@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) NSTimer *timer;
@end

@implementation XtraPromoCheckPointViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupLayout];
    [self startCountDown];
    [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_timer invalidate];
}

-(void)startCountDown
{
    DataManager *sharedData = [DataManager sharedInstance];
    if(sharedData.checkPointData)
    {
        NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        self.date = [dateFormatter dateFromString:sharedData.checkPointData.date];
    }
    
    //Set up a timer that calls the updateTime method every second to update the label
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self
                                           selector:@selector(updateTime)
                                           userInfo:nil
                                            repeats:YES];
}

-(void)updateTime
{
//    NSLog(@"update time runs");
    //Get the time left until the specified date
    NSInteger ti = ((NSInteger)[_date timeIntervalSinceNow]);
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600) % 24;
//    NSInteger days = (ti / 86400);
    
    //Update the lable with the remaining time
    _lblTimerHour.text = [NSString stringWithFormat:@"%02li", (long)hours];
    _lblTimerMinute.text = [NSString stringWithFormat:@"%02li", (long)minutes];
    _lblTimerSecond.text = [NSString stringWithFormat:@"%02li", (long)seconds];
}

-(void)setupLayout
{
    /*
     * Clear View
     */
    
//    for (UIView *oldViews in self.view.subviews)
//    {
//        [oldViews removeFromSuperview];
//    }
    
    //***********
    DataManager *sharedData = [DataManager sharedInstance];
    _lblTotalPoint.text = sharedData.checkPointData.point;
    _lblDate.text = [AXISnetCommon formatDateFromString:sharedData.checkPointData.date];
    
    NSDictionary *boldAttrs = @{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSForegroundColorAttributeName :[UIColor blackColor]};
    
    NSDictionary *defaultAttrs = @{NSFontAttributeName : [UIFont systemFontOfSize:24], NSForegroundColorAttributeName :[UIColor blackColor]};
    
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] init];
    
    NSString *openingText = @"";
    NSString *closingText = @"";
    openingText = @"Masih ada ";
    closingText = @" lagi untuk dimenangkan";
    
    NSAttributedString *open = [[NSAttributedString alloc] initWithString:openingText
                                                               attributes:defaultAttrs];
    
    NSAttributedString *content = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ Mobil",sharedData.checkPointData.stock] attributes:boldAttrs];
    
    NSAttributedString *close = [[NSAttributedString alloc] initWithString:closingText attributes:defaultAttrs];
    
    [labelText appendAttributedString:open];
    [labelText appendAttributedString:content];
    [labelText appendAttributedString:close];
    
    _lblFooterTitle.attributedText = labelText;

    self.view.backgroundColor = kDefaultWhiteSmokeColor;
    UIView *v = [self.wrapperView viewWithTag:999];
    if(!v)
    {
        SectionHeaderView *titleView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0,  CGRectGetWidth([UIScreen mainScreen].bounds), kDefaultCellHeight*1.5)
                                                                 withLabelForXL:@"Cek Poin Hari Ini"
                                                                 isFirstSection:YES];
        titleView.tag = 999;
        [self.wrapperView addSubview:titleView];
    }
    
    UIView *vi = [self.wrapperView viewWithTag:888];
    if(!vi)
    {
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
        _btnReloadBalance = createButtonWithFrame(CGRectMake(20,
                                                             CGRectGetMaxY(_lblSubtitle.frame) + 70,
                                                             CGRectGetWidth(_wrapperView.frame) - 40,
                                                             40),
                                                  @"ISI PULSA & REBUT KESEMPATANNYA",
                                                  buttonFont,
                                                  kDefaultBlackColor,
                                                  kDefaultYellowColor,
                                                  kDefaultYellowColor);
        _btnReloadBalance.tag = 888;
        
        [_btnReloadBalance addTarget:self action:@selector(btnReloadBalanceClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [_wrapperView addSubview:_btnReloadBalance];
    }
    
    CGRect frame = _wrapperView.frame;
    UIView *vi2 = [self.wrapperView viewWithTag:777];
    if(!vi2)
    {
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
        _btnTermsAndCons = createButtonWithFrame(CGRectMake(20,
                                                            CGRectGetMaxY(_btnReloadBalance.frame) + 10,
                                                            CGRectGetWidth(_btnReloadBalance.frame),
                                                            25),
                                                 @"Syarat & Ketentuan",
                                                 buttonFont,
                                                 [UIColor darkGrayColor],
                                                 kDefaultWhiteColor,
                                                 kDefaultWhiteColor);
        _btnTermsAndCons.tag = 777;
        [_btnTermsAndCons addTarget:self action:@selector(termsAndConsClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.wrapperView addSubview:_btnTermsAndCons];
        frame.size.height = CGRectGetMaxY(_btnTermsAndCons.frame);
        _wrapperView.frame = frame;
    }
    
    frame = _contentView.frame;
    frame.size.height = CGRectGetMaxY(_wrapperView.frame);
    _contentView.frame = frame;
    
    _scrollView.contentSize = CGSizeMake(CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetMaxY(_contentView.frame));
    
    _timerHourView.layer.cornerRadius = 5;
    _timerHourView.layer.borderWidth = 3;
    _timerHourView.layer.borderColor = [UIColor grayColor].CGColor;
    
    _timerMinuteView.layer.cornerRadius = 5;
    _timerMinuteView.layer.borderWidth = 3;
    _timerMinuteView.layer.borderColor = [UIColor grayColor].CGColor;
    
    _timerSecondView.layer.cornerRadius = 5;
    _timerSecondView.layer.borderWidth = 3;
    _timerSecondView.layer.borderColor = [UIColor grayColor].CGColor;
}

-(void)termsAndConsClicked
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // Screen tracking
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                          action:@"Lihat"  // Event action (required)
                                                           label:@"Info"          // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    [self showModalWebViewForURL:[NSURL URLWithString:XTRAPROMO_TERMSCONS_URL]];
}

-(void)showModalWebViewForURL:(NSURL *)url
{
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
    webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:webViewController animated:YES completion:NULL];
}

-(void)btnReloadBalanceClicked:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // Screen tracking
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                          action:@"Tertarik"  // Event action (required)
                                                           label:@"isi pulsa"          // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSArray *contentTmp = nil;
    NSDictionary *dict = [GroupingAndSorting doGrouping:sharedData.homeMenu];
    NSArray *objectsForMenu = [dict objectForKey:@"20"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"href==%@",@"#reload_pg"];
    NSArray *filteredArray = [objectsForMenu filteredArrayUsingPredicate:predicate];
    if([filteredArray count] > 0)
    {
        MenuModel *menuModel = [filteredArray objectAtIndex:0];
        contentTmp = [sharedData.menuData valueForKey:menuModel.menuId]; //-> 5 is menuID for reloadBalance
        // Grouping
        NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
        
        // Sorting
        NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
        
        NSArray *paymentMenu = [sortingResult allKeys];
        paymentMenu = [GroupingAndSorting doSortingByGroupId:paymentMenu];
        
        NSMutableArray *paymentMenuTmp = [[NSMutableArray alloc] init];
        for (int i=0; i<[paymentMenu count]; i++) {
            NSString *groupIdOfMenu = [paymentMenu objectAtIndex:i];
            NSArray *objectsForMenu = [sortingResult objectForKey:groupIdOfMenu];
            for (MenuModel *menu in objectsForMenu) {
                [paymentMenuTmp addObject:menu];
            }
        }
        
        PaymentChoiceViewController *subMenuVC = [[PaymentChoiceViewController alloc] initWithNibName:@"PaymentChoiceViewController" bundle:nil];
        subMenuVC.paymentMenu = paymentMenuTmp;
        subMenuVC.grayHeaderTitle = [Language get:@"balance_management" alter:nil];
        subMenuVC.blueHeaderTitle = menuModel.menuName;
        [subMenuVC createBarButtonItem:BACK];
        [self.navigationController pushViewController:subMenuVC animated:YES];
        [subMenuVC release];
        subMenuVC = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_scrollView release];
    [_wrapperView release];
    [_contentView release];
    [_lblTitle release];
    [_lblTotalPoint release];
    [_lblTxtPoint release];
    [_lblSubtitle release];
    [_lblDate release];
    [_lblFooterTitle release];
    [_lineViewLeft release];
    [_lineViewRight release];
    [_timerHourView release];
    [_lblTimerHour release];
    [_timerMinuteView release];
    [_lblTimerMinute release];
    [_timerSecondView release];
    [_lblTimerSecond release];
    [_lblHour release];
    [_lblMinute release];
    [_lblDetik release];
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.checkPointData = nil;
    [super dealloc];
}
- (void)viewDidUnload {
    [self setScrollView:nil];
    [self setWrapperView:nil];
    [self setContentView:nil];
    [self setLblTitle:nil];
    [self setLblTotalPoint:nil];
    [self setLblTxtPoint:nil];
    [self setLblSubtitle:nil];
    [self setLblDate:nil];
    [self setLblFooterTitle:nil];
    [self setLineViewLeft:nil];
    [self setLineViewRight:nil];
    [self setTimerHourView:nil];
    [self setLblTimerHour:nil];
    [self setTimerMinuteView:nil];
    [self setLblTimerMinute:nil];
    [self setTimerSecondView:nil];
    [self setLblTimerSecond:nil];
    [self setLblHour:nil];
    [self setLblMinute:nil];
    [self setLblDetik:nil];
    [super viewDidUnload];
}
@end
