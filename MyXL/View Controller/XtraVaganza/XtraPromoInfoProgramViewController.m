//
//  XtraPromoInfoProgramViewController.m
//  MyXL
//
//  Created by tyegah on 7/31/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XtraPromoInfoProgramViewController.h"
#import "PaymentChoiceViewController.h"

@interface XtraPromoInfoProgramViewController ()<UIWebViewDelegate>
@property (retain, nonatomic) IBOutlet UIWebView *webView;
@end

NSString *const kInfoProgramURL = @"http://webdev.my.xl.co.id/myxl19/xtravaganza/info_program";

@implementation XtraPromoInfoProgramViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupLayout];
}

-(void)setupLayout
{
    self.view.backgroundColor = kDefaultWhiteSmokeColor;
    NSURL *url = [NSURL URLWithString:kInfoProgramURL];
    NSLog(@"URL = %@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSLog(@"request = %@",request);
    [_webView loadRequest:request];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
}

#pragma mark - UIWebView Delegate
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
     NSLog(@"did fail load");
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"did start load");
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"did finish load");
    [self.hud hide:YES];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestDesc = [[request URL] absoluteString];
    NSLog(@"request DESC %@", requestDesc);
    if(![requestDesc isEqualToString:kInfoProgramURL])
    {
        if([requestDesc rangeOfString:@"#reload_pg"].location != NSNotFound)
        {
            [self reloadBalance];
        }
        else
            [self showModalWebViewForURL:[request URL]];
        return NO;
    }
    return YES;
}

-(void)reloadBalance
{
    DataManager *sharedData = [DataManager sharedInstance];
    NSArray *contentTmp = nil;
    NSDictionary *dict = [GroupingAndSorting doGrouping:sharedData.homeMenu];
    NSArray *objectsForMenu = [dict objectForKey:@"20"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"href==%@",@"#reload_pg"];
    NSArray *filteredArray = [objectsForMenu filteredArrayUsingPredicate:predicate];
    if([filteredArray count] > 0)
    {
        MenuModel *menuModel = [filteredArray objectAtIndex:0];
        contentTmp = [sharedData.menuData valueForKey:menuModel.menuId]; //-> 5 is menuID for reloadBalance
        // Grouping
        NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
        
        // Sorting
        NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
        
        NSArray *paymentMenu = [sortingResult allKeys];
        paymentMenu = [GroupingAndSorting doSortingByGroupId:paymentMenu];
        
        NSMutableArray *paymentMenuTmp = [[NSMutableArray alloc] init];
        for (int i=0; i<[paymentMenu count]; i++) {
            NSString *groupIdOfMenu = [paymentMenu objectAtIndex:i];
            NSArray *objectsForMenu = [sortingResult objectForKey:groupIdOfMenu];
            for (MenuModel *menu in objectsForMenu) {
                [paymentMenuTmp addObject:menu];
            }
        }
        
        PaymentChoiceViewController *subMenuVC = [[PaymentChoiceViewController alloc] initWithNibName:@"PaymentChoiceViewController" bundle:nil];
        subMenuVC.paymentMenu = paymentMenuTmp;
        subMenuVC.grayHeaderTitle = [Language get:@"balance_management" alter:nil];
        subMenuVC.blueHeaderTitle = menuModel.menuName;
        [subMenuVC createBarButtonItem:BACK];
        [self.navigationController pushViewController:subMenuVC animated:YES];
        [subMenuVC release];
        subMenuVC = nil;
    }
}

-(void)showModalWebViewForURL:(NSURL *)url
{
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
    webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:webViewController animated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [_webView release];
    [super dealloc];
}



- (void)viewDidUnload {
    [self setWebView:nil];
    [super viewDidUnload];
}
@end
