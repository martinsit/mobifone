//
//  XtraPromoGetBonusViewController.m
//  MyXL
//
//  Created by tyegah on 8/31/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XtraPromoGetBonusViewController.h"
#import "ThankYouViewController.h"
#import "PaymentChoiceViewController.h"

@interface XtraPromoGetBonusViewController ()
@property (retain, nonatomic) IBOutlet UILabel *lblHeader;
@property (retain, nonatomic) IBOutlet UILabel *lblFooter;
@property (retain, nonatomic) IBOutlet UIButton *btnGetBonus;
@property (retain, nonatomic) IBOutlet UIButton *btnFooterToDo;
@property (retain, nonatomic) IBOutlet UIButton *btnSK;
@property (retain, nonatomic) IBOutlet UILabel *lblDesc;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@end

@implementation XtraPromoGetBonusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupView];
}

-(void)setupView
{
    DataManager *sharedData = [DataManager sharedInstance];
    if(sharedData.checkBonusModel)
    {
        CGFloat screenWidth = _contentView.frame.size.width;
        if (IS_IPAD) {
            screenWidth = 1024.0;
        }
        SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, screenWidth,kDefaultCellHeight)
                                                                   withLabelForXL:sharedData.checkBonusModel.headerTopLabel
                                                                   isFirstSection:NO];
        [_scrollView addSubview:headerView];
        _contentView.frame = CGRectOffset(_contentView.frame, 0, CGRectGetHeight(headerView.frame));
        _lblDesc.text = sharedData.checkBonusModel.deskripsi;
        _lblFooter.text = sharedData.checkBonusModel.footerLabel;
        _lblHeader.text = sharedData.checkBonusModel.headerLabel;
        [_btnFooterToDo setTitle:sharedData.checkBonusModel.footerTodoLabel forState:UIControlStateNormal];
        [_btnGetBonus setTitle:sharedData.checkBonusModel.tombolLabel forState:UIControlStateNormal];
        _btnGetBonus.hidden = !sharedData.checkBonusModel.tombol;
        [_btnSK setTitle:sharedData.checkBonusModel.skLabel forState:UIControlStateNormal];
        if(!sharedData.checkBonusModel.tombol)
        {
            CGRect lblFooterFrame = _lblFooter.frame;
            _lblFooter.frame = CGRectOffset(_lblFooter.frame, 0, -(CGRectGetMinY(lblFooterFrame)-CGRectGetMinY(_btnGetBonus.frame)));
            _btnFooterToDo.frame = CGRectOffset(_btnFooterToDo.frame, 0, -(CGRectGetMinY(lblFooterFrame)-CGRectGetMinY(_btnGetBonus.frame)));
            _btnSK.frame = CGRectOffset(_btnSK.frame, 0, -(CGRectGetMinY(lblFooterFrame)-CGRectGetMinY(_btnGetBonus.frame)));
        }
    }
    CGRect contentFrame = _contentView.frame;
    contentFrame.size.height = CGRectGetMaxY(_btnSK.frame) + 20;
    _scrollView.contentSize = CGSizeMake(CGRectGetWidth(_scrollView.frame), CGRectGetMaxY(_contentView.frame));
}

#pragma mark - AXISNET Request Delegate

- (void)requestDoneWithInfo:(NSDictionary *)result
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success)
    {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        if(type == GETBONUS_XTRAPROMO_REQ)
        {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            NSString *reason = [result valueForKey:REASON_KEY];
            thankYouViewController.info = reason;
            thankYouViewController.displayFBButton = NO;
            thankYouViewController.hideInboxBtn = NO;
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            [thankYouViewController release];
            thankYouViewController = nil;
        }
    }
}

- (IBAction)getBonusClicked:(id)sender
{
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    DataManager *sharedData = [DataManager sharedInstance];
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = GETBONUS_XTRAPROMO_REQ;
    [request getBonusXtraPromoWithLayanan:sharedData.checkBonusModel.layanan];
}

- (IBAction)reloadButtonClicked:(id)sender {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // Screen tracking
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                          action:@"Tertarik"  // Event action (required)
                                                           label:@"isi pulsa"          // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSArray *contentTmp = nil;
    NSDictionary *dict = [GroupingAndSorting doGrouping:sharedData.homeMenu];
    NSArray *objectsForMenu = [dict objectForKey:@"20"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"href==%@",@"#reload_pg"];
    NSArray *filteredArray = [objectsForMenu filteredArrayUsingPredicate:predicate];
    if([filteredArray count] > 0)
    {
        MenuModel *menuModel = [filteredArray objectAtIndex:0];
        contentTmp = [sharedData.menuData valueForKey:menuModel.menuId]; //-> 5 is menuID for reloadBalance
        // Grouping
        NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
        
        // Sorting
        NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
        
        NSArray *paymentMenu = [sortingResult allKeys];
        paymentMenu = [GroupingAndSorting doSortingByGroupId:paymentMenu];
        
        NSMutableArray *paymentMenuTmp = [[NSMutableArray alloc] init];
        for (int i=0; i<[paymentMenu count]; i++) {
            NSString *groupIdOfMenu = [paymentMenu objectAtIndex:i];
            NSArray *objectsForMenu = [sortingResult objectForKey:groupIdOfMenu];
            for (MenuModel *menu in objectsForMenu) {
                [paymentMenuTmp addObject:menu];
            }
        }
        
        PaymentChoiceViewController *subMenuVC = [[PaymentChoiceViewController alloc] initWithNibName:@"PaymentChoiceViewController" bundle:nil];
        subMenuVC.paymentMenu = paymentMenuTmp;
        subMenuVC.grayHeaderTitle = [Language get:@"balance_management" alter:nil];
        subMenuVC.blueHeaderTitle = menuModel.menuName;
        [subMenuVC createBarButtonItem:BACK];
        [self.navigationController pushViewController:subMenuVC animated:YES];
        [subMenuVC release];
        subMenuVC = nil;
    }
}

- (IBAction)skButtonClicked:(id)sender {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // Screen tracking
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                          action:@"Lihat"  // Event action (required)
                                                           label:@"Info"          // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    [self showModalWebViewForURL:[NSURL URLWithString:XTRAPROMO_TERMSCONS_URL]];
}

- (void)showModalWebViewForURL:(NSURL *)url
{
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
    webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:webViewController animated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_btnGetBonus release];
    [_lblFooter release];
    [_btnFooterToDo release];
    [_btnSK release];
    [_lblDesc release];
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.checkBonusModel = nil;
    [_contentView release];
    [_scrollView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setBtnGetBonus:nil];
    [self setLblFooter:nil];
    [self setBtnFooterToDo:nil];
    [self setBtnSK:nil];
    [self setLblDesc:nil];
    [self setContentView:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
}
@end
