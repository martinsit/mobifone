//
//  QuotaImprovementViewCell.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 10/19/15.
//  Copyright © 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "QuotaImprovementViewCell.h"

@implementation QuotaImprovementViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_lblTotalPackage release];
    [_lblPackageActive release];
    [_lblRemainingData release];
    [_lblTotalData release];
    [_lblStartDate release];
    [_lblEndDate release];
    [_bgDataView release];
    [_fgDataView release];
    
    [_lblTitleStartDate release];
    [_lblTitleEndDate release];
    [_bubbleView release];
    [_unlimitedBorderView release];
    
    [_daysLeftView release];
    [_daysLeftBgView release];
    [_daysLeftFgView release];
    [_circleLeftView release];
    [_circleRightView release];
    [_circleCenterView release];
    [_circleCenterLabel release];
    
    [_detailButton release];
    [_voiceSmsButton release];
    
    [super dealloc];
}

@end
