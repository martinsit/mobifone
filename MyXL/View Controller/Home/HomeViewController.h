//
//  HomeViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/7/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "WEPopoverController.h"
#import "AXISnetRequest.h"
#import "KASlideShow.h"
#import "EGORefreshTableHeaderView.h"

@class MSNavigationPaneViewController;

@interface HomeViewController : BasicViewController <PopoverControllerDelegate, AXISnetRequestDelegate, KASlideShowDelegate, EGORefreshTableHeaderDelegate, DataParserDelegate> {
    
    NSArray *content;
    NSArray *_sortedMenu;
    NSDictionary *_theSourceMenu;
    //WEPopoverController *navPopover;
    
    //int adsImageCounter;
    
    EGORefreshTableHeaderView *_refreshHeaderView;
	
	//  Reloading var should really be your tableviews datasource
	//  Putting it here for demo purposes
	BOOL _reloading;
    
    //BOOL _showBalance;
    
    //NSArray *adsContent;
}


@property (nonatomic, retain) NSArray *content;
@property (nonatomic, retain) NSArray *sortedMenu;
@property (nonatomic, retain) NSDictionary *theSourceMenu;

@property (strong, nonatomic) MSNavigationPaneViewController *navigationPaneViewController;

@property (nonatomic, strong) WEPopoverController *popoverController;

@property (retain, nonatomic) UIViewController *centerController;

//@property (nonatomic, retain) NSArray *adsContent;

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

- (void)updateView;

@end
