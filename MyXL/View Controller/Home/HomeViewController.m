//
//  HomeViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/7/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "HomeViewController.h"
#import "CommonCell.h"
#import "FooterView.h"
#import "DataManager.h"
#import "MenuModel.h"
#import "GroupingAndSorting.h"

#import "MSNavigationPaneViewController.h"
#import "MasterViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "HomeCellBackground.h"

#import "AXISnetCommon.h"
#import "Constant.h"
#import "AXISnetNavigationBar.h"

#import "NotificationViewController.h"
#import "PromoViewController.h"
#import "UpdateProfileViewController.h"
#import "ChangePasswordViewController.h"

#import "BalanceViewController.h"
#import "PackageViewController.h"
#import "PromoViewController.h"

//mobifone
#import "PagedImageTableViewCell.h"
#import "MobifoneCallCenterTableViewCell.h"

#import "ASMenuViewController.h"

//new
#import "LeftViewController.h"
#import "IIViewDeckController.h"
#import "IIWrapController.h"

#import "AdsModel.h"

#import "AppDelegate.h"

#import "SubMenuViewController.h"

#import "AdsRequest.h"

#import "CheckUsageModel.h"
#import "HumanReadableDataSizeHelper.h"

#import "CheckQuotaViewController.h"
#import "BalanceHomeCell.h"
#import "DTCustomColoredAccessory.h"
#import "RectBackground.h"
#import "FeatureHomeCell.h"

#import "QuotaMeterViewController.h"
#import "PaymentChoiceViewController.h"

#import "QuotaCalculatorViewController.h"

#import "ContactUsViewController.h"

#import "BalanceSubmenuViewController.h"

// TODO : NEPTUNE
#import "PaketSukaViewController.h"
#import "PaketSukaMenuViewController.h"

#import "InfoAndPromoViewController.h"
#import "SVModalWebViewController.h"

#import "XtraPromoWinnerListViewController.h"
#import "XtraPromoCheckPointViewController.h"
#import "XtraPromoInfoProgramViewController.h"
#import "XtraPromoGetBonusViewController.h"
#import "WebviewViewController.h"

#import "4GPowerTableViewCell.h"
#import "FeatureMenuSmallView.h"
#import "FeatureMenuMediumView.h"
#import "BalanceTableViewCell.h"

#import "FourGPowerPackViewController.h"

#import "QuotaImprovementViewCell.h"
#import "QuotaEnhanceViewController.h"

@interface HomeViewController ()<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet KASlideShow *slideshow;

@property (strong, nonatomic) LeftViewController *sideMenuViewController;

@property (strong, nonatomic) AdsRequest *adsRequest;

@property (strong, nonatomic) IBOutlet UILabel *balanceIcon;
@property (strong, nonatomic) IBOutlet UILabel *balanceTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *balanceValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *balanceValidityLabel;

@property (strong, nonatomic) IBOutlet UILabel *packageIcon;
@property (strong, nonatomic) IBOutlet UILabel *packageTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *packageValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *packageValidityLabel;

@property (strong, nonatomic) IBOutlet UIView *packageView;

@property (strong, nonatomic) MenuModel *selectedMenu;

@property (strong, nonatomic) MenuModel *reloadMenu;

@property (nonatomic, strong) UIWebView *xtraWebView;
@property (nonatomic, strong) UIButton *btnRefresh;
@property (nonatomic, assign) BOOL isWebViewLoaded;

// TODO : 4G POWERPACK
@property (nonatomic, assign) NSInteger totalRows;
//- (void)createAccountAndNotificationButton;
//- (void)performAccount:(id)sender;
//- (void)performNotification:(id)sender;
//- (void)performLogout;

- (void)performBalance;
//- (void)performPackage;
//- (void)performPromo;

//- (void)setupAds;

//- (void)performAds;

- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;

- (void)performInfo:(MenuModel*)menuModel withLevelTwoType:(LevelTwoType)type;

- (void)requestMenu;

@end

@implementation HomeViewController

#if IS_STAGING == 1
    //NSString * const kXtraPromoURL          = @"http://web123.xl.co.id/xtravaganza/home";
    NSString * const kXtraPromoURL          = @"http://web123.xl.co.id/xtravaganza/home_native";
    NSString * const kXtraInfoProgramURL    = @"http://web123.xl.co.id/xtravaganza/info_program";
    NSString * const kXtraGetBonusURL       = @"http://web123.xl.co.id/xtravaganza/ambil_bonus";
#elif IS_PRODUCTION == 1
    //NSString * const kXtraPromoURL          = @"http://my.xl.co.id/xtravaganza/home";
    NSString * const kXtraPromoURL          = @"http://my.xl.co.id/xtravaganza/home_native";
    NSString * const kXtraInfoProgramURL    = @"http://my.xl.co.id/xtravaganza/info_program";
    NSString * const kXtraGetBonusURL       = @"http://my.xl.co.id/xtravaganza/ambil_bonus";
#else
    //NSString * const kXtraPromoURL          = @"http://webdev.my.xl.co.id/myxl19/xtravaganza/home";
    NSString * const kXtraPromoURL          = @"http://webdev.my.xl.co.id/myxl19/xtravaganza/home_native";
    NSString * const kXtraInfoProgramURL    = @"http://webdev.my.xl.co.id/myxl19/xtravaganza/info_program";
    NSString * const kXtraGetBonusURL       = @"http://webdev.my.xl.co.id/myxl19/xtravaganza/ambil_bonus";
#endif


@synthesize content;
@synthesize sortedMenu = _sortedMenu;
@synthesize theSourceMenu = _theSourceMenu;

@synthesize centerController = _viewController;

@synthesize popoverController = _myPopoverController;

@synthesize adsContent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.backgroundColor = kDefaultWhiteSmokeColor;
//        self.screenName = @"Home";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _slideshow.backgroundColor = kDefaultWhiteSmokeColor;
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    adsImageCounter = 0;
    
    [self createBarButtonItem:PROFILE_NOTIF];
    
    /*
    FooterView *footer = [[[FooterView alloc] initWithFrame:CGRectMake(0.0, 0.0, _theTableView.bounds.size.width, 50.0)] autorelease];
    _theTableView.tableFooterView = footer;*/
    
    if (_refreshHeaderView == nil) {
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _theTableView.bounds.size.height, self.view.frame.size.width, _theTableView.bounds.size.height)];
		view.delegate = self;
		[_theTableView addSubview:view];
		_refreshHeaderView = view;
		[view release];
		
	}
	
    [_theTableView registerNib:[UINib nibWithNibName:@"4GPowerTableViewCell" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"4gpowercell"];
    [_theTableView registerNib:[UINib nibWithNibName:@"BalanceTableViewCell" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"BalanceTableViewCell"];
    [_theTableView registerNib:[UINib nibWithNibName:@"PagedImageTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"PagedImageTableViewCell"];
    
    [_theTableView registerNib:[UINib nibWithNibName:@"QuotaImprovementViewCell" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"QuotaImprovementViewCell"];
    [_theTableView registerNib:[UINib nibWithNibName:@"MobifoneCallCenterTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"MobifoneCallCenterTableViewCell"];
    
	/* 
     * Update the last update date
     */
	[_refreshHeaderView refreshLastUpdatedDate];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    //---------------------//
    // Show Navigation Bar //
    //---------------------//
    AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[self.navigationController navigationBar];
    navBar.hidden = NO;
    [navBar updateGreeting];
    
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if (appDelegate.loggedIn) {
        DataManager *dataManager = [DataManager sharedInstance];
        
        // TODO: v1.9.5
        // Load Cache Quota Info
        NSString *quotaCache = (NSString*)getDataFromPreferenceWithKey(USAGE_CACHE_KEY);
        NSLog(@"quotaCache = %@",quotaCache);
        DataParser *dataParser = [[DataParser alloc] init];
        dataParser.delegate = self;
        [dataParser parseFourGPackageStatus:quotaCache];
        [dataParser release];
        
        // TODO : Update Hygiene Defect
        [appDelegate registerDeviceToken];
        
        [self updateView];
        
        // 4G POWERPACK
        [self check4GPackageStatus];
        
        self.badgeNotif.value = dataManager.unread;
        
        if (!dataManager.showBalance) {
            [self performBalance];
        }
        
        /*
         * Request Hot Offer Banner
         */
        //[self setupAds];
        //[self performAds:HOME_PAGE withSize:ADS_SIZE_HOME_IPHONE];
        
        /*
         * The method is in BasicViewController
         */
        if (!dataManager.showBanner) {
            if (IS_IPAD) {
                [self performHotOfferBanner:ADS_SIZE_HOME_IPAD];
            }
            else {
                [self performHotOfferBanner:ADS_SIZE_HOME_IPHONE];
            }
        }
    }
    
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    [sharedCache release];
    sharedCache = nil;
    
    [_theTableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    // Google Analytics
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if (appDelegate.loggedIn)
    {
        // TODO : V1.9
        // Loading Time
        NSTimeInterval interval = -[appDelegate.dateToday timeIntervalSinceNow];
        NSLog(@"Login END %@", appDelegate.dateToday);
        NSLog(@"Login Interval %f", interval);
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"MyXL IPA" interval:@((int)(interval * 1000)) name:@"Login Time" label:nil] build]];
        appDelegate.dateToday = nil;
        
        // Screen tracking
        self.screenName = @"HomePageiOS";
        [tracker set:kGAIScreenName value:@"HomePageiOS"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        [[GAI sharedInstance] dispatch];
    }
    
    [super viewDidAppear:animated];
}

// TODO : Hygiene
-(void)viewDidDisappear:(BOOL)animated
{
    // Hide the hud when moving to another view even when there are unfinished processes
    [self.hud hide:YES];
    //self.theTableView = nil;
}

- (void)viewDidUnload {
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [self.popoverController dismissPopoverAnimated:NO];
    self.popoverController = nil;
    
    _refreshHeaderView = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    _refreshHeaderView = nil;
    [super dealloc];
}

// TODO : HYGIENE - PUSH NOTIF
-(void)checkPushNotificationAvailability
{
    NSLog(@"check push notification availability");
    DataManager *sharedData = [DataManager sharedInstance];
    if(sharedData.isRemoteNotificationAvailable)
    {
        NSString *saltKeyAPI = SALT_KEY;
        NSString *deviceMsisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
//        NSString *notifType = [sharedData.remoteNotificationInfo valueForKey:@"type"];
        
        if([deviceMsisdn hasPrefix:@"0"])
            deviceMsisdn = [deviceMsisdn stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"62"];
        //NSLog(@"device msisdn %@", deviceMsisdn);
        if(!sharedData.isRemoteNotificationShown)
        {
            // show notification in pop up
            [self showPopupNotificationWithInfo:sharedData.remoteNotificationInfo];
            sharedData.isRemoteNotificationShown = YES;
        }
    }
}

// TODO : HYGIENE - PUSH NOTIF
-(void)showPopupNotificationWithInfo:(NSDictionary *)userInfo
{
    NSLog(@"Show pop up notif");
    NSDictionary *apsDict = [userInfo valueForKey:@"aps"];
    if(apsDict)
    {
        NSString *message = [apsDict valueForKey:@"alert"];
        UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@",message] delegate:self cancelButtonTitle:[[Language get:@"close" alter:nil] uppercaseString] otherButtonTitles:[[Language get:@"view" alter:nil] uppercaseString], nil] autorelease];
        alertView.tag = 999;
        [alertView show];
    }
}

// TODO : HYGIENE - PUSH NOTIF
-(void)processNotification
{
//    AXISnetRequest *request = [AXISnetRequest sharedInstance];
//    [request cancelAllRequest];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate)
    {
        [appDelegate requestNotification];
    }
}

// TODO : HYGIENE - PUSH NOTIF
#pragma mark - UIAlertView Delegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)index {
    // TODO : HYGIENE
    if(alertView.tag == 999)
    {
        if(index != 0)
        {
            DataManager *sharedData = [DataManager sharedInstance];
            NSString *pageID = [sharedData.remoteNotificationInfo valueForKey:@"page_id"];
            // Inbox
            if([pageID isEqual:@"1"])
            {
                [self processNotification];
            }
            // TODO : V1.9
            // View Quota
            else if([pageID isEqualToString:@"2"])
            {
                [self requestCheckUsageXL];
            }
            else if([pageID isEqualToString:@"3"])
            {
                DataManager *sharedData = [DataManager sharedInstance];
                LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
                leftViewController.content = sharedData.leftMenuData;
                _sideMenuViewController = leftViewController;
                
                NSDictionary *dict = [GroupingAndSorting doGrouping:sharedData.homeMenu];
                NSArray *objectsForMenu = [dict objectForKey:@"20"];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"href==%@",@"#reload_pg"];
                NSArray *filteredArray = [objectsForMenu filteredArrayUsingPredicate:predicate];
                if([filteredArray count] > 0)
                {
                    MenuModel *menuModel = [filteredArray objectAtIndex:0];
                    [self performPaymentChoice:menuModel];
                }
            }
            else if([pageID isEqualToString:@"4"])
            {
                [self performPromo];
            }
        }
    }
}

-(void)featureMenuTapped:(UIGestureRecognizer *)recognizer
{
    DataManager *sharedData = [DataManager sharedInstance];
    NSInteger index = recognizer.view.tag;
    //NSArray *objectsForMenu = [_theSourceMenu objectForKey:@"1"];
    //-NSArray *objectsForMenu = [_theSourceMenu objectForKey:@"126"];
    NSArray *objectsForMenu = [NSArray arrayWithArray:sharedData.featureMenu];
    MenuModel *menuObj = [objectsForMenu objectAtIndex:index];
    NSLog(@"href = %@",menuObj.href);
    
    if ([menuObj.href isEqualToString:@"#package"]) {
        menuObj.menuId = @"6";
    }
    
    [self redirectToPageWithMenuModel:menuObj orHref:@""];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.loggedIn)
    {
        NSInteger row = 7;
        /*
        DataManager *sharedData = [DataManager sharedInstance];
        if (sharedData.profileData.flagXvaganza) {
            row++;
        }
        if (sharedData.profileData.flag4Gpowermeter) {
            row++;
        }
         */
        _totalRows = row;
        
    }
    NSLog(@"_totalRows = %li",(long)_totalRows);
    return _totalRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataManager *dataManager = [DataManager sharedInstance];
    NSLog(@" current rows %ld",(long)indexPath.row);
    // TODO: Row 0 for Balance Info
    if (indexPath.row == 0)
    {
        static NSString *cellIdentifier = @"PagedImageTableViewCell";
        PagedImageTableViewCell *cell = (PagedImageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        cell.bnrImgView.image = [UIImage imageNamed:@"banner_home.png"];
        cell.bnrImgView.contentMode = UIViewContentModeScaleAspectFill;
        
        return cell;
        
        
       
    }
    // TODO: Row 1 for Featured Menu V2
    else if (indexPath.row == 1)
    {
        static NSString *kCustomCellID = @"TableCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCustomCellID];
        if (cell == nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCustomCellID] autorelease];
        }
        
        if ([cell.contentView subviews]) {
            for (UIView *subview in [cell.contentView subviews]) {
                [subview removeFromSuperview];
            }
        }
        
        NSArray *objectsForMenu = [NSArray arrayWithArray:dataManager.featureMenu];
        
        // Filter Array != row 0
        NSMutableArray *menu = [[NSMutableArray alloc] init];
        for (int i=0; i<[objectsForMenu count]; i++) {
            MenuModel *menuObj = [objectsForMenu objectAtIndex:i];
            if (i>0) {
                [menu addObject:menuObj];
            }
        }
        objectsForMenu = [NSArray arrayWithArray:menu];
                NSLog(@"%lu",(unsigned long)objectsForMenu.count);
        //
        
        NSInteger menuCount = [objectsForMenu count];
        
        //---- Menu New
        int contentWidth = CGRectGetWidth(cell.contentView.frame);
        int menuWidth = contentWidth/menuCount;
        int posX = 0;
        for (int i = 1; i<=menuCount; i++)
        {
            MenuModel *menuObj = [objectsForMenu objectAtIndex:i-1];
            FeatureMenuMediumView *menuView = (FeatureMenuMediumView *)[[[NSBundle mainBundle] loadNibNamed:@"FeatureMenuMediumView" owner:nil options: nil] objectAtIndex:0];
            if(menuView && menuObj)
            {
                menuView.lblMenuName.text = [menuObj.menuName capitalizedString];
                menuView.imgIcon.text = menuObj.icon;
            }
            
            /*
             * Reskin
             */
            if ([dataManager.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
                [dataManager.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
                // AXIS
                menuView.backgroundColor = colorWithHexString(dataManager.profileData.themesModel.menuColor);
                menuView.backgroundColor = colorWithHexString(dataManager.profileData.themesModel.menuColor);
            }
            else if ([dataManager.profileData.themesModel.header isEqualToString:@""]) {
                // XL
                menuView.backgroundColor = kDefaultDarkBlueColor;
                menuView.backgroundColor = kDefaultDarkBlueColor;
            }
            else {
                menuView.backgroundColor = kDefaultDarkBlueColor;
                menuView.backgroundColor = kDefaultDarkBlueColor;
            }
            /*----------*/
            
            if(i==menuCount)
                menuWidth = contentWidth;
            CGRect viewFrame = CGRectMake(posX, 1, menuWidth-1, 48);
            menuView.frame = viewFrame;
            
            menuView.tag = i;
            UITapGestureRecognizer *tapGesture = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(featureMenuTapped:)] autorelease];
            [menuView addGestureRecognizer:tapGesture];
            [cell.contentView addSubview:menuView];
            posX = posX + menuWidth;
            contentWidth = contentWidth - menuWidth+1;
        }
        //---- End of Menu New
        return cell;
    }
    // TODO: XTRAVAGANZA
    else if (indexPath.row == 2)
    {
        
        static NSString *cellIdentifier = @"BalanceTableViewCell";
        BalanceTableViewCell *cell = (BalanceTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (dataManager.showBalance)
        {
            cell.lblBalanceTitle.hidden = NO;
            cell.lblBalanceValue.hidden = YES;
            cell.lblValidityTitle.hidden = NO;
            cell.lblValidityDate.hidden = YES;
        }
        else
        {
            cell.lblBalanceTitle.hidden = NO;
            cell.lblBalanceValue.hidden = NO;
            cell.lblValidityTitle.hidden = NO;
            cell.lblValidityDate.hidden = NO;
        }
        
        NSArray *objectsForMenu = [NSArray arrayWithArray:dataManager.featureMenu];
        MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
        cell.lblBalanceTitle.text = [menuModel.menuName capitalizedString];
        
        cell.lblBalanceValue.text = [NSString stringWithFormat:@"%@",addThousandsSeparator(dataManager.checkBalanceData.balance, dataManager.profileData.language)];
        
        if (dataManager.profileData.msisdnType == PREPAID) {
            NSString *validity = @"";
            validity = dataManager.checkBalanceData.activeStopDate;
            if ([validity length] != 0) {
                validity = [NSString stringWithFormat:@"%@",changeDateFormat(validity)];
                cell.lblValidityTitle.hidden = NO;
                cell.lblValidityTitle.text = [NSString stringWithFormat:@"%@ : %@",[Language get:@"active_until" alter:nil],[NSString stringWithFormat:@"%@",validity]];
            }
            else {
                cell.lblValidityTitle.hidden = YES;
            }
        }
        else {
            cell.lblValidityTitle.hidden = YES;
        }
        
        /*
         * Reskin
         *
         */
        if ([dataManager.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
            [dataManager.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
            // AXIS
            cell.balanceView.backgroundColor = colorWithHexString(dataManager.profileData.themesModel.menuFeature);
            cell.validityView.backgroundColor = colorWithHexString(dataManager.profileData.themesModel.menuFeature);
            
            cell.lblBalanceTitle.textColor = kDefaultWhiteColor;
            cell.lblBalanceValue.textColor = kDefaultWhiteColor;
            cell.lblValidityTitle.textColor = kDefaultWhiteColor;
            cell.lblValidityDate.textColor = kDefaultWhiteColor;
        }
        else if ([dataManager.profileData.themesModel.header isEqualToString:@""]) {
            // XL
            cell.balanceView.backgroundColor = kDefaultOceanBlueColor;
            cell.validityView.backgroundColor = kDefaultOceanBlueColor;
            
            cell.lblBalanceTitle.textColor = kDefaultBlackColor;
            cell.lblBalanceValue.textColor = kDefaultBlackColor;
            cell.lblValidityTitle.textColor = kDefaultBlackColor;
            cell.lblValidityDate.textColor = kDefaultBlackColor;
        }
        else {
            cell.balanceView.backgroundColor = kDefaultOceanBlueColor;
            cell.validityView.backgroundColor = kDefaultOceanBlueColor;
            
            cell.lblBalanceTitle.textColor = kDefaultBlackColor;
            cell.lblBalanceValue.textColor = kDefaultBlackColor;
            cell.lblValidityTitle.textColor = kDefaultBlackColor;
            cell.lblValidityDate.textColor = kDefaultBlackColor;
        }
        /*----------*/
        
        // add action Refresh Balance
        UITapGestureRecognizer *tapGesture1 = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(performBalanceSubMenu)] autorelease];
        [cell.balanceView setUserInteractionEnabled:YES];
        [cell.balanceView addGestureRecognizer:tapGesture1];
        
        return cell;
        /*
        NSLog(@"row %ld, webview cell", (long)indexPath.row);
        static NSString *kCustomCellID = @"TableCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCustomCellID];
        if (cell == nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCustomCellID] autorelease];
        }
        CGFloat webviewHeight = 250;
        CGFloat webviewWidth = CGRectGetWidth(cell.contentView.frame);
        if(IS_IPAD) {
            webviewHeight = 350;
            webviewWidth = 1024;
        }
        cell.contentView.frame = CGRectMake(0, 0, webviewWidth, webviewHeight);
        if(!_xtraWebView)
        {
            _xtraWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, webviewWidth, webviewHeight)];
            _xtraWebView.delegate = self;
            _xtraWebView.scalesPageToFit = YES;
            NSLog(@"webview frame %@", NSStringFromCGRect(cell.contentView.frame));
            NSURL *url = [NSURL URLWithString:kXtraPromoURL];
            NSLog(@"URL = %@",url);
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:60.0];
            NSLog(@"request = %@",request);
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            if(appDelegate.loggedIn)
            {
                [_xtraWebView loadRequest:request];
            }
            _xtraWebView.scrollView.scrollEnabled = NO;
            _xtraWebView.scrollView.bounces = NO;
            _btnRefresh = createButtonWithFrame(CGRectMake(CGRectGetWidth(_xtraWebView.frame)/2 - (kDefaultButtonHeight/2), CGRectGetHeight(_xtraWebView.frame)/2 - (kDefaultButtonHeight/2), kDefaultButtonHeight, kDefaultButtonHeight),
                                                @"}",
                                                [UIFont fontWithName:kDefaultFontKSAN size:17.0],
                                                kDefaultNavyBlueColor,
                                                [UIColor whiteColor],
                                                [UIColor lightGrayColor]);
            [_btnRefresh addTarget:self action:@selector(refreshXtraWebView) forControlEvents:UIControlEventTouchUpInside];
            [_xtraWebView addSubview:_btnRefresh];
            _btnRefresh.hidden = YES;
        }
        
        [cell.contentView addSubview:_xtraWebView];
        cell.contentView.backgroundColor = kDefaultWhiteColor;
        return cell;
         */
    }
    
    else if (indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5)
    {
        static NSString *simpleTextName = @"Simple_Text_Cell_Style";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTextName];
        NSArray *offerArray = [[NSArray alloc]initWithObjects:@"Buy Package",@"Reload Balance",@"Info Promo", nil];
        
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTextName];
        }
        
        NSString *colorName = [offerArray objectAtIndex:indexPath.row - 3];
        NSLog(@"%@",colorName);
        NSString *title = [colorName capitalizedString];
        cell.textLabel.text = title;
        return cell;
    }
    
    // TODO: v1.9.5
    /*
    else if ((indexPath.row == 3 && dataManager.profileData.flagXvaganza) || (indexPath.row == 2 && !dataManager.profileData.flagXvaganza)) {
        DataManager *sharedData = [DataManager sharedInstance];
        static NSString *CellIdentifier = @"QuotaImprovementViewCell";
        QuotaImprovementViewCell *cell = (QuotaImprovementViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        // Remove target
        [cell.detailButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        
        if ([sharedData.quotaImprovementData.activePackage intValue] == 0) {
            cell.lblTotalPackage.text = [Language get:@"no_data_plan" alter:nil];
            cell.lblPackageActive.hidden = YES;
            
            cell.detailButton.layer.cornerRadius = 5.0;
            cell.detailButton.clipsToBounds = YES;
            [cell.detailButton addTarget:self action:@selector(performPackageList:) forControlEvents:UIControlEventTouchUpInside];
            [cell.detailButton setTitle:[NSString stringWithFormat:@"%@",[Language get:@"buy_data_plan" alter:nil].uppercaseString] forState:UIControlStateNormal];
            
            cell.voiceSmsButton.hidden = YES;
            if ([sharedData.quotaImprovementData.detail count] > 0) {
                QuotaDetailModel *detailModel = [sharedData.quotaImprovementData.detail objectAtIndex:0];
                
                if ([detailModel.benefitSms count] > 0 || [detailModel.benefitVoice count] > 0) {
                    cell.voiceSmsButton.hidden = NO;
                    cell.voiceSmsButton.shouldUnderline = YES;
                    cell.voiceSmsButton.underLineOffset = 2;
                    cell.voiceSmsButton.text = [NSString stringWithFormat:@"%@",[Language get:@"voice_sms_btn" alter:nil]];
                    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                                  action:@selector(detailQuotaButtonTap)];
                    [cell.voiceSmsButton addGestureRecognizer:tapGesture3];
                    [cell.voiceSmsButton setUserInteractionEnabled:YES];
                }
                else {
                    cell.voiceSmsButton.hidden = YES;
                }
            }
        }
        else {
            cell.lblTotalPackage.text = [Language get:@"total_data_plan" alter:nil];
            NSString *totalPackageStr = [NSString stringWithFormat:@"%@ %@ %@",[Language get:@"active_data_plan_1" alter:nil],sharedData.quotaImprovementData.activePackage,[Language get:@"active_data_plan_2" alter:nil]];
            cell.lblPackageActive.text = totalPackageStr;
            cell.lblPackageActive.hidden = NO;
            
            cell.detailButton.layer.cornerRadius = 5.0;
            cell.detailButton.clipsToBounds = YES;
            [cell.detailButton addTarget:self action:@selector(detailQuotaButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.detailButton setTitle:[NSString stringWithFormat:@"%@",[Language get:@"see_detail_quota" alter:nil].uppercaseString] forState:UIControlStateNormal];
            cell.voiceSmsButton.hidden = YES;
        }
        
        NSString *remainingStr = @"";
        if (!!sharedData.quotaImprovementData.remaining && ![sharedData.quotaImprovementData.remaining isEqual:[NSNull null]]) {
            remainingStr = sharedData.quotaImprovementData.remaining;
        }
        
        NSString *totalStr = @"";
        if (!!sharedData.quotaImprovementData.total && ![sharedData.quotaImprovementData.total isEqual:[NSNull null]]) {
            totalStr = sharedData.quotaImprovementData.total;
        }
        
        cell.lblRemainingData.text = [NSString stringWithFormat:@"%@ %@",[Language get:@"remaining_data_plan" alter:nil],remainingStr];
        cell.lblTotalData.text = [NSString stringWithFormat:@"%@ %@",[Language get:@"total" alter:nil],totalStr];
        
        cell.lblTitleEndDate.text = [Language get:@"end_date" alter:nil];
        cell.lblEndDate.text = changeDateFormatForPackage(sharedData.quotaImprovementData.activeUntil);
        
        cell.lblTitleStartDate.text = [Language get:@"start_date" alter:nil];
        cell.lblStartDate.text = changeDateFormatForPackage(sharedData.quotaImprovementData.regPackage);
        
        cell.bgDataView.layer.cornerRadius = 11.0f;
        cell.bgDataView.layer.masksToBounds = YES;
        
        // Foreground Gauge View
        cell.fgDataView.layer.cornerRadius = 11.0f;
        cell.fgDataView.layer.masksToBounds = YES;
        CGRect fgViewRect = cell.fgDataView.frame;
        fgViewRect.size.width = (sharedData.quotaImprovementData.percentRemaining/100) * cell.bgDataView.frame.size.width;
        cell.fgDataView.frame = fgViewRect;
        if (sharedData.quotaImprovementData.percentRemaining <= 20) {
            cell.fgDataView.backgroundColor = colorWithHexString(kDefaultRedQuotaColor);
        }
        else {
            cell.fgDataView.backgroundColor = colorWithHexString(kDefaultGreenQuotaColor);
        }
        
        // Unlimited or not
        cell.unlimitedBorderView.layer.cornerRadius = cell.unlimitedBorderView.bounds.size.width/2;
        cell.unlimitedBorderView.layer.masksToBounds = YES;
        
        CGRect unlimitedViewRect = cell.unlimitedBorderView.frame;
        CGFloat position = (0.8*cell.bgDataView.frame.size.width) - (cell.unlimitedBorderView.frame.size.width/2);
        unlimitedViewRect.origin.x = position;
        cell.unlimitedBorderView.frame = unlimitedViewRect;
        
        // remove view from cell.bubbleView
        for (ASValuePopUpView *oldViews in cell.bubbleView.subviews)
        {
            [oldViews removeFromSuperview];
        }
        
        //bubble view
        ASValuePopUpView *bubble = [[ASValuePopUpView alloc] initWithFrame:CGRectMake(0.0, 15.0, cell.bubbleView.frame.size.width, cell.bubbleView.frame.size.height)];
        bubble.color = colorWithHexString(@"EF3A13");
        bubble.cornerRadius = 4.0;
        [bubble setTextColor:kDefaultWhiteColor];
        [bubble setFont:[UIFont fontWithName:kDefaultFontBold size:6.0]];
        [bubble setString:[NSString stringWithFormat:@"FUP %@",sharedData.quotaImprovementData.total]];
        [cell.bubbleView addSubview:bubble];
        
        CGRect bubbleViewRect = cell.bubbleView.frame;
        CGFloat positionBubble = (0.8*cell.bgDataView.frame.size.width) - (cell.bubbleView.frame.size.width/2);
        bubbleViewRect.origin.x = positionBubble;
        cell.bubbleView.frame = bubbleViewRect;
        
        if ([sharedData.quotaImprovementData.isUnlimited isEqualToString:@"1"]) {
            cell.unlimitedBorderView.hidden = NO;
            cell.bubbleView.hidden = NO;
            cell.lblPackageActive.hidden = YES;
            cell.lblTotalData.hidden = YES;
            cell.lblTotalPackage.text = [Language get:@"your_usage_data_plan" alter:nil];
            cell.lblRemainingData.text = [NSString stringWithFormat:@"%@ %@",[Language get:@"usage_data_plan" alter:nil],sharedData.quotaImprovementData.usage];
            
            // Change Foreground Logic for Unlimited
            CGFloat unlimitedRange = cell.unlimitedBorderView.frame.origin.x + (cell.unlimitedBorderView.frame.size.width/2);
            NSLog(@"iNot :%f",cell.unlimitedBorderView.frame.size.width);
            NSLog(@"iNot BG VIEW:%f",cell.bgDataView.frame.size.width);
            
            CGRect fgViewRect = cell.fgDataView.frame;
            
            if (sharedData.quotaImprovementData.percentUsage >= 100) {
                fgViewRect.size.width = unlimitedRange + ((5/100)*cell.bgDataView.frame.size.width);
            }
            else {
                fgViewRect.size.width = (sharedData.quotaImprovementData.percentUsage/100) * unlimitedRange;
            }
            
            cell.fgDataView.frame = fgViewRect;
        }
        else {
            cell.unlimitedBorderView.hidden = YES;
            cell.bubbleView.hidden = YES;
        }
        
        CGRect fgDaysLeftViewRect = cell.daysLeftFgView.frame;
        fgDaysLeftViewRect.size.width = (sharedData.quotaImprovementData.percentSisaHari/100) * cell.daysLeftBgView.frame.size.width;
        cell.daysLeftFgView.frame = fgDaysLeftViewRect;
        
        NSString *daysLeftStr = @"0";
        if (!!sharedData.quotaImprovementData.sisaHari && ![sharedData.quotaImprovementData.sisaHari isEqual:[NSNull null]]) {
            daysLeftStr = sharedData.quotaImprovementData.sisaHari;
        }
        cell.circleCenterLabel.text = [NSString stringWithFormat:@"%@ %@",daysLeftStr,[Language get:@"day" alter:nil]];
        CGRect centerDaysLeftViewRect = cell.circleCenterView.frame;
        centerDaysLeftViewRect.origin.x = cell.daysLeftFgView.frame.origin.x + cell.daysLeftFgView.frame.size.width - (cell.circleCenterView.frame.size.width/2) - cell.daysLeftFgView.frame.origin.x;
        if ([daysLeftStr isEqualToString:@"0"]) {
            centerDaysLeftViewRect.origin.x = cell.circleLeftView.frame.origin.x;
        }
        cell.circleCenterView.frame = centerDaysLeftViewRect;
        
        cell.circleLeftView.layer.cornerRadius = cell.circleLeftView.bounds.size.width/2;
        cell.circleLeftView.layer.masksToBounds = YES;
        
        cell.circleRightView.layer.cornerRadius = cell.circleRightView.bounds.size.width/2;
        cell.circleRightView.layer.masksToBounds = YES;
        
        cell.circleCenterView.layer.cornerRadius = cell.circleCenterView.bounds.size.width/2;
        cell.circleCenterView.layer.masksToBounds = YES;
        
        return cell;
    }
     */
    // TODO: 4G POWERPACK
//    mobifone for last banner
    else if(indexPath.row == _totalRows-1)
    {
        static NSString *cellIdentifier = @"MobifoneCallCenterTableViewCell";
        MobifoneCallCenterTableViewCell *cell = (MobifoneCallCenterTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        return cell;
        /*
        DataManager *sharedData = [DataManager sharedInstance];
        static NSString *CellIdentifier = @"4gpowercell";
        
        Power4GTableViewCell *cell = (Power4GTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        [cell.btn4GPower addTarget:self action:@selector(fourGPowerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lblTitle.text = [[Language get:@"title_4gpowerpack_x" alter:nil] uppercaseString];
        if (sharedData.isFourGHandset && sharedData.isFourGSim && sharedData.isFourGPackage) {
            [cell.btn4GPower setTitle:[[Language get:@"button_4gpowerpack_complete" alter:nil] uppercaseString] forState:UIControlStateNormal];
        }
        else {
            [cell.btn4GPower setTitle:[[Language get:@"button_4gpowerpack" alter:nil] uppercaseString] forState:UIControlStateNormal];
        }
        
        cell.btn4GPower.layer.cornerRadius = 5.0;
        cell.btn4GPower.clipsToBounds = YES;
        cell.btn4GPower.titleLabel.numberOfLines = 1;
        cell.btn4GPower.titleLabel.adjustsFontSizeToFitWidth = YES;
        cell.btn4GPower.titleLabel.minimumFontSize = 10.0;
        
        cell.lblAwareness.text = [Language get:@"awareness_4g_usim" alter:nil];
        
        int currentLang = [Language getCurrentLanguage];
        
        // EN
        if (currentLang == 0) {
            cell.imgPaket.image = sharedData.isFourGPackage ? [UIImage imageNamed:@"pm-package-ready.png"] : [UIImage imageNamed:@"pm-package-not-ready.png"];
            cell.imgHandset.image = sharedData.isFourGHandset ? [UIImage imageNamed:@"pm-handset-ready.png"] : [UIImage imageNamed:@"pm-handset-not-ready.png"];
            cell.imgSimcard.image = sharedData.isFourGSim ? [UIImage imageNamed:@"pm-simcard-ready.png"] : [UIImage imageNamed:@"pm-simcard-not-ready.png"];
        }
        // ID
        else {
            cell.imgPaket.image = sharedData.isFourGPackage ? [UIImage imageNamed:@"id-pm-package-ready.png"] : [UIImage imageNamed:@"id-pm-package-not-ready.png"];
            cell.imgHandset.image = sharedData.isFourGHandset ? [UIImage imageNamed:@"id-pm-handset-ready.png"] : [UIImage imageNamed:@"id-pm-handset-not-ready.png"];
            cell.imgSimcard.image = sharedData.isFourGSim ? [UIImage imageNamed:@"id-pm-simcard-ready.png"] : [UIImage imageNamed:@"id-pm-simcard-not-ready.png"];
        }
        
        return cell;
         */
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 145.0;
    }
    else if(indexPath.row == 1) {
        return 50.0;
    }
    else if(indexPath.row == 2) {
        return 50.0;
    }
    else if (indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5){
        return 44.0;
    }
    else if (indexPath.row == _totalRows-1){
        return 44.0;
    }
    return 0.0;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // simplify the conditions
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    if(indexPath.row == 0) {
        NSLog(@"do nothing");
    }
    else if ((indexPath.row == 3 && sharedData.profileData.flagXvaganza) || (indexPath.row == 2 && !sharedData.profileData.flagXvaganza)) {
        NSLog(@"do nothing");
    }
    else {
        if (indexPath.row != _totalRows-1)
        {
            NSArray *objectsForMenu = [NSArray arrayWithArray:sharedData.featureMenu];
            NSInteger menuCount = [objectsForMenu count];
            NSInteger menuRetracted = _totalRows - menuCount-1;
            if(indexPath.row >= menuRetracted)
            {
                NSUInteger index = indexPath.row - menuRetracted;
                if(index < menuCount)
                {
                    MenuModel *menuModel = [objectsForMenu objectAtIndex:index];
                    [self redirectToPageWithMenuModel:menuModel orHref:@""];
                }
            }
        }
        else if(indexPath.row == _totalRows-1) {
            [self fourGPowerButtonClicked:nil];
        }
    }

}

#pragma mark - 4G POWERMETER

- (void)check4GPackageStatus
{
    NSLog(@"-------- Request API 4G Package Status ---------");
    DataManager *sharedData = [DataManager sharedInstance];
    NSNumber *number = [[NSUserDefaults standardUserDefaults] objectForKey:kIsPackage4G];
    
    if (number)
        sharedData.isFourGPackage = [number boolValue];
    
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = FOUR_G_PACKAGE_STATUS_REQ;
    [request getFourGPackageStatus];
}

- (void)fourGPowerButtonClicked:(id)sender
{
    /*
     * Google Analytic
     */
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // Screen tracking
    NSString *gaLabel = @"4G Power Pack";
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                          action:@"Home"        // Event action (required)
                                                           label:gaLabel        // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    /******************************/
    
    DataManager *sharedData = [DataManager sharedInstance];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = FOUR_G_POWERPACK_REQ;
    [request getFourGPowerWithMatrix:[NSString stringWithFormat:@"%ld%ld%ld",(long)@(sharedData.isFourGHandset).integerValue,(long)@(sharedData.isFourGSim).integerValue, (long)@(sharedData.isFourGPackage).integerValue]];
}

- (void)redirectToFourGPowerPack
{
    FourGPowerPackViewController *vcon = [[FourGPowerPackViewController alloc] initWithNibName:@"FourGPowerPackViewController" bundle:nil];
    [self.navigationController pushViewController:vcon animated:YES];
    [vcon release];
    vcon = nil;
}

#pragma mark - HREF SELECTION

-(void)redirectToPageWithMenuModel:(MenuModel *)menuModel orHref:(NSString *)href
{
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:@"20"];
    DataManager *dataManager = [DataManager sharedInstance];
    NSString *hrefWeb = @"";
    
    if(menuModel)
    {
        hrefWeb = menuModel.hrefweb;
        href = menuModel.href;
    }
    else
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"href == %@",href];
        NSArray *arrayObj = [objectsForMenu filteredArrayUsingPredicate:predicate];
        if([arrayObj count] > 0)
        {
            menuModel = [arrayObj objectAtIndex:0];
        }
    }
    
    if ([href isEqualToString:@"#xltunai"] || [hrefWeb isEqualToString:@"#xltunai"] || [hrefWeb isEqualToString:@"#xltunai/id/7"])
    {
        
        _selectedMenu = menuModel;
        [self performXLTunaiBalance];
    }
    else if ([href isEqualToString:@"#reload_pg"]) {
        NSLog(@"reload Balance");
        LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
        leftViewController.content = dataManager.leftMenuData;
        _sideMenuViewController = leftViewController;
        menuModel.menuId = @"5"; // tricky
        [self performPaymentChoice:menuModel];
    }
    else if ([href isEqualToString:@"#quotacalculator"]) {
        NSLog(@"quota Calculator");
        _selectedMenu = menuModel;
        [self performQuotaCalculator];
    }
    else if ([href isEqualToString:@"#promo"]) {
        /*
         * Google Analytic
         */
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        // Screen tracking
        NSString *gaLabel = @"Info Program Promo";
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                              action:@"Home"        // Event action (required)
                                                               label:gaLabel        // Event label
                                                               value:nil] build]];
        [[GAI sharedInstance] dispatch];
        /******************************/
        
        _selectedMenu = menuModel;
        [self performPromo];
    }
    else if ([href isEqualToString:@"#xtra_info_program"]) {
        [self openWebViewWithURLString:kXtraInfoProgramURL];
    }
    else if ([href isEqualToString:@"#xtra_list_pemenang"]) {
        [self performXtraWinnerList];
    }
    else if ([href isEqualToString:@"#xtra_cek_poin"]) {
        [self performXtraCheckPoint];
    }
    else if ([href isEqualToString:@"#xtra_ambil_bonus"])
    {
        [self performXtraCheckBonus];
    }
    // 4G Readiness Check
    else if([href isEqualToString:@"#openwebview"])
    {
        /*
         * Google Analytic
         */
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        // Screen tracking
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                              action:@"Cek"         // Event action (required)
                                                               label:@"4G Ready"    // Event label
                                                               value:nil] build]];
        [[GAI sharedInstance] dispatch];
        /******************************/
        
        [self openWebViewWithURLString:menuModel.desc];
    }
    // Check Package and Quota
    else if ([href isEqualToString:@"#my_package"]) {
        /*
         * Google Analytic
         */
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        // Screen tracking
        NSString *gaLabel = @"Cek Paket Kuota";
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                              action:@"Home"        // Event action (required)
                                                               label:gaLabel        // Event label
                                                               value:nil] build]];
        [[GAI sharedInstance] dispatch];
        /******************************/
        
        //[self requestCheckUsageXL];
        [self detailQuotaButtonTap];
    }
    // Buy Package
    else if ([href isEqualToString:@"#package"]) {
        /*
         * Google Analytic
         */
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        // Screen tracking
        NSString *gaLabel = @"Beli Paket";
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                              action:@"Home"        // Event action (required)
                                                               label:gaLabel        // Event label
                                                               value:nil] build]];
        [[GAI sharedInstance] dispatch];
        /******************************/
        
        LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
        leftViewController.content = dataManager.leftMenuData;
        _sideMenuViewController = leftViewController;
        
        [self performInfo:menuModel withLevelTwoType:COMMON];
    }
    else {
        LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
        leftViewController.content = dataManager.leftMenuData;
        _sideMenuViewController = leftViewController;
        
        [self performInfo:menuModel withLevelTwoType:COMMON];
    }
}

-(void)openWebViewWithURLString:(NSString *)strURL
{
    WebviewViewController *webviewVC = [[WebviewViewController alloc] initWithNibName:@"WebviewViewController" bundle:nil];
    webviewVC.strURL = strURL;
    [self.navigationController pushViewController:webviewVC animated:YES];
    [webviewVC release];
    webviewVC = nil;
}

#pragma mark - XTRA PROMO

-(void)redirectToXtraInfoProgram
{
    XtraPromoInfoProgramViewController *xtraInfoProgramVC = [[XtraPromoInfoProgramViewController alloc] initWithNibName:@"XtraPromoInfoProgramViewController" bundle:nil];
    [self.navigationController pushViewController:xtraInfoProgramVC animated:YES];
    [xtraInfoProgramVC release];
    xtraInfoProgramVC = nil;
}

-(void)redirectToXtraWinnerList
{
    XtraPromoWinnerListViewController *xtraWinnerListVC = [[XtraPromoWinnerListViewController alloc] initWithNibName:@"XtraPromoWinnerListViewController" bundle:nil];
    [self.navigationController pushViewController:xtraWinnerListVC animated:YES];
    [xtraWinnerListVC release];
    xtraWinnerListVC = nil;
}

-(void)redirectToXtraCheckPoint
{
    XtraPromoCheckPointViewController *xtraCheckPointVC = [[XtraPromoCheckPointViewController alloc] initWithNibName:@"XtraPromoCheckPointViewController" bundle:nil];
    [self.navigationController pushViewController:xtraCheckPointVC animated:YES];
    [xtraCheckPointVC release];
    xtraCheckPointVC = nil;
}

-(void)redirectToXtraGetBonus
{
    XtraPromoGetBonusViewController *xtraBonusVC = [[XtraPromoGetBonusViewController alloc] initWithNibName:@"XtraPromoGetBonusViewController" bundle:nil];
    [self.navigationController pushViewController:xtraBonusVC animated:YES];
    [xtraBonusVC release];
    xtraBonusVC = nil;
}

- (void)performXtraCheckPoint
{
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = CHECK_POINT_XTRAPROMO_REQ;
    [request checkPointXtraPromo];
}

- (void)performXtraWinnerList
{
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = WINNER_LIST_XTRAPROMO_REQ;
    [request getWinnerListXtraPromo];
}

- (void)performXtraCheckBonus
{
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = CHECKBONUS_XTRAPROMO_REQ;
    [request checkGetBonusXtraPromo];
}

#pragma mark - QUOTA IMPROVEMENT

- (void)checkQuotaImprovement {
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = QUOTA_IMPROVEMENT_REQ;
    [request checkQuotaImprovement];
}

- (void)detailQuotaButtonClicked:(id)sender {
    // TODO: Bug fixing tribar menu that doesn't work
    // Reinstantiate the Left Controller
    LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    leftViewController.content = self.content;
    _sideMenuViewController = leftViewController;
    
    QuotaEnhanceViewController *packageViewControllerTmp = [[QuotaEnhanceViewController alloc] initWithNibName:@"QuotaEnhanceViewController" bundle:nil];
    
    UIViewController *packageViewController = packageViewControllerTmp;
    
    IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:packageViewController leftViewController:_sideMenuViewController];
    deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
    deckController.leftSize = self.view.frame.size.width - 270;
    packageViewController = deckController;
    
    UINavigationController *navController = customizedNavigationController(self.view.bounds,NO);
    [navController setViewControllers:[NSArray arrayWithObject:packageViewController]];
    
    packageViewController = [[IIWrapController alloc] initWithViewController:navController];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController pushViewController:packageViewController animated:YES];
}

- (void)detailQuotaButtonTap {
    // TODO: Bug fixing tribar menu that doesn't work
    // Reinstantiate the Left Controller
    LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    leftViewController.content = self.content;
    _sideMenuViewController = leftViewController;
    
    QuotaEnhanceViewController *packageViewControllerTmp = [[QuotaEnhanceViewController alloc] initWithNibName:@"QuotaEnhanceViewController" bundle:nil];
    
    UIViewController *packageViewController = packageViewControllerTmp;
    
    IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:packageViewController leftViewController:_sideMenuViewController];
    deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
    deckController.leftSize = self.view.frame.size.width - 270;
    packageViewController = deckController;
    
    UINavigationController *navController = customizedNavigationController(self.view.bounds,NO);
    [navController setViewControllers:[NSArray arrayWithObject:packageViewController]];
    
    packageViewController = [[IIWrapController alloc] initWithViewController:navController];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController pushViewController:packageViewController animated:YES];
}

#pragma mark - Selector

- (void)setupAds
{
    if([self.adsContent count] > 0)
    {
        if (IS_IPAD) {
            CGRect frame = _slideshow.frame;
            frame.size.width = self.view.bounds.size.width;
            frame.size.height = 66.0;
            frame.origin.y = self.view.bounds.size.height - 66.0;
            _slideshow.frame = frame;
            
            frame = _slideshow.topImageView.frame;
            frame.size.width = self.view.bounds.size.width;
            frame.size.height = 66.0;
            frame.origin.y = 0.0;
            _slideshow.topImageView.frame = frame;
            
            frame = _slideshow.bottomImageView.frame;
            frame.size.width = self.view.bounds.size.width;
            frame.size.height = 66.0;
            frame.origin.y = 0.0;
            _slideshow.bottomImageView.frame = frame;
        }
        
        _slideshow.delegate = self;
        [_slideshow setDelay:3]; // Delay between transitions
        [_slideshow setTransitionDuration:1]; // Transition duration
        [_slideshow setTransitionType:KASlideShowTransitionSlide]; // Choose a transition type (fade or slide)
        [_slideshow setImagesContentMode:UIViewContentModeScaleAspectFit]; // Choose a content mode for images to display
        
        // TODO: BUG FIX
        // Fixed crash on axis number
        if([self.adsContent count] > 0)
        {
            for (int i=0; i<[self.adsContent count]; i++) {
                MenuModel *adsModel = [self.adsContent objectAtIndex:i];
                if(adsModel.image)
                    [_slideshow addImage:adsModel.image];
            }
            
            [_slideshow start];
            _slideshow.hidden = NO;
        }
        
        DataManager *dataManager = [DataManager sharedInstance];
        dataManager.showBanner = YES;
    }
    else
    {
        //hide banner
        self.slideshow.hidden = YES;
        CGRect tableRect = self.theTableView.frame;
        if(tableRect.size.height < CGRectGetHeight([UIScreen mainScreen].bounds)-20)
            tableRect.size.height = tableRect.size.height + CGRectGetHeight(self.slideshow.frame);
        NSLog(@"---> table rect %@", NSStringFromCGRect(tableRect));
        self.theTableView.frame = tableRect;
    }
}

- (void)updateView {
    
    if (_reloading) {
        [self doneLoadingTableViewData];
    }
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    //------------------//
    // Reskinning       //
    //------------------//
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        
        // AXIS
        
        [self.settingBtn setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuColor)) forState:UIControlStateNormal];
        [self.settingBtn setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuFeature))
                                      forState:UIControlStateHighlighted];
        UILabel *btnLabel = (UILabel*)[self.settingBtn viewWithTag:99];
        btnLabel.textColor = kDefaultWhiteColor;
        
        
        [self.inboxBtn setBackgroundImage:imageFromColor([UIColor clearColor]) forState:UIControlStateNormal];
        [self.inboxBtn setBackgroundImage:imageFromColor([UIColor clearColor]) forState:UIControlStateHighlighted];
        UILabel *inboxBtnLabel = (UILabel*)[self.inboxBtn viewWithTag:99];
        inboxBtnLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
    }
    else {
        
        // XL
        
        //[self.settingBtn setBackgroundImage:imageFromColor(kDefaultWhiteColor) forState:UIControlStateNormal];
        [self.settingBtn setBackgroundImage:imageFromColor([UIColor clearColor]) forState:UIControlStateNormal];
        [self.settingBtn setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuFeature))
                                   forState:UIControlStateHighlighted];
        UILabel *btnLabel = (UILabel*)[self.settingBtn viewWithTag:99];
        //btnLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
        btnLabel.textColor = kDefaultWhiteColor;
        
        [self.inboxBtn setBackgroundImage:imageFromColor([UIColor clearColor]) forState:UIControlStateNormal];
        [self.inboxBtn setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuFeature))
                                 forState:UIControlStateHighlighted];
        UILabel *inboxBtnLabel = (UILabel*)[self.inboxBtn viewWithTag:99];
        inboxBtnLabel.textColor = kDefaultWhiteColor;
    }
    
    //------------------//
    // Restructure Menu //
    //------------------//
    self.content = [NSArray arrayWithArray:sharedData.homeMenu];
    
    // Save menu for left menu
    sharedData.leftMenuData = [NSArray arrayWithArray:sharedData.sideMenu];
    
    //-------------------------//
    // End of Restructure Menu //
    //-------------------------//
    
    NSMutableArray *menuTmp = [[NSMutableArray alloc] init];
    for (MenuModel *menuModelTmp in self.content) {
        
        if ([menuModelTmp.href isEqualToString:@"#quotacalculator"] == NO) {
            [menuTmp addObject:menuModelTmp];
        }
    }
    self.content = [NSArray arrayWithArray:menuTmp];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:self.content];
    // TODO: V1.9
    // Skip the sorting to keep the menu as is
    self.theSourceMenu = groupingResult;
    
    [self.theTableView reloadData];
    
    // TODO: HYGIENE
    // Moved this method here to make sure all the other basic tasks are almost finished before the pop up shows
    [self checkPushNotificationAvailability];
}

- (void)performBalance
{
    /*
     * Google Analytic
     */
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // Screen tracking
    NSString *gaLabel = @"Refresh Pulsa";
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                          action:@"Home"        // Event action (required)
                                                           label:gaLabel        // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    /******************************/
    
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = CHECK_BALANCE_REQ;
    [request checkBalance];
}

-(void)refreshXtraWebView
{
    NSLog(@"webview refreshed");
    _isWebViewLoaded = NO;
    if(_xtraWebView)
    {
        NSURL *url = [NSURL URLWithString:kXtraPromoURL];
        NSLog(@"URL = %@",url);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:60.0];
        NSLog(@"request = %@",request);
        [_xtraWebView loadRequest:request];
    }
}

- (void)performInfo:(MenuModel*)menuModel withLevelTwoType:(LevelTwoType)type
{
    SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
    subMenuVC.parentMenuModel = menuModel;
    subMenuVC.isLevel2 = YES;
    subMenuVC.levelTwoType = type;
    subMenuVC.grayHeaderTitle = menuModel.menuName;
    
    UIViewController *infoViewController = subMenuVC;
    
    IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:infoViewController leftViewController:_sideMenuViewController];
    deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
    deckController.leftSize = self.view.frame.size.width - 260; //270
    infoViewController = deckController;
    
    UINavigationController *navController = customizedNavigationController(self.view.bounds,NO);
    // Change NavBar BgColor
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[navController navigationBar];
    changeNavbarBg(navBar, app.window.bounds);
    //----------------------
    [navController setViewControllers:[NSArray arrayWithObject:infoViewController]];
    
    infoViewController = [[IIWrapController alloc] initWithViewController:navController];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController pushViewController:infoViewController animated:YES];
}

- (void)requestMenu {
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = MENU_REQ;
    [request menu];
}

- (void)myPackageButtonPressed:(id)sender {
    // TODO: V1.9
    [self requestCheckUsageXL];
}

-(void)requestCheckUsageXL
{
    LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    leftViewController.content = self.content;
    _sideMenuViewController = leftViewController;
    
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = CHECK_USAGE_XL_REQ;
    [request checkInternetUsageXL];
}

- (void)xlStarButtonPressed:(id)sender {
    DataManager *dataManager = [DataManager sharedInstance];
    MenuModel *selectedMenu = dataManager.xlstarMenu;
    dataManager.parentId = selectedMenu.menuId;
    dataManager.menuModel = selectedMenu;
    
    LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    leftViewController.content = self.content;
    _sideMenuViewController = leftViewController;
    
    [self performInfo:selectedMenu withLevelTwoType:COMMON];
}

- (void)performXLTunaiBalance {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = XL_TUNAI_BALANCE_REQ;
    [request xlTunaiBalance];
}

- (void)quotaMeterButtonPressed:(id)sender {
    
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = QUOTA_METER_REQ;
    [request quotaMeter];
}

- (void)performPaymentChoice:(MenuModel*)menuModel {
    PaymentChoiceViewController *subMenuVC = [[PaymentChoiceViewController alloc] initWithNibName:@"PaymentChoiceViewController" bundle:nil];
    
    //-- Setting Payment Menu --//
    DataManager *dataManager = [DataManager sharedInstance];
    
    NSArray *contentTmp = nil;
    contentTmp = [dataManager.menuData valueForKey:menuModel.menuId];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    NSArray *paymentMenu = [sortingResult allKeys];
    paymentMenu = [GroupingAndSorting doSortingByGroupId:paymentMenu];
    
    NSMutableArray *paymentMenuTmp = [[NSMutableArray alloc] init];
    for (int i=0; i<[paymentMenu count]; i++) {
        NSString *groupIdOfMenu = [paymentMenu objectAtIndex:i];
        NSArray *objectsForMenu = [sortingResult objectForKey:groupIdOfMenu];
        for (MenuModel *menu in objectsForMenu) {
            [paymentMenuTmp addObject:menu];
        }
    }
    
    subMenuVC.paymentMenu = paymentMenuTmp;
    subMenuVC.grayHeaderTitle = [Language get:@"balance_management" alter:nil];
    subMenuVC.blueHeaderTitle = menuModel.menuName;
    subMenuVC.isLevel2 = YES;
    
    //---------------------------//
    
    UIViewController *infoViewController = subMenuVC;
    
    IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:infoViewController leftViewController:_sideMenuViewController];
    deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
    deckController.leftSize = self.view.frame.size.width - 260; //270
    infoViewController = deckController;
    
    UINavigationController *navController = customizedNavigationController(self.view.bounds,NO);
    [navController setViewControllers:[NSArray arrayWithObject:infoViewController]];
    
    infoViewController = [[IIWrapController alloc] initWithViewController:navController];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController pushViewController:infoViewController animated:YES];
    
    [paymentMenuTmp release];
}

- (void)performReloadBalance:(id)sender {
    
    // TODO: V1.9
    // Redirect to isipulsa.xl.co.id on reload balance
    NSURL *url = nil;
    //url = [NSURL URLWithString:@"https://isipulsa.xl.co.id/myxl"];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    NSString *paymentId = @"1"; //hardcoded
    NSString *lang = sharedData.profileData.language;
    
    NSString *baseUrlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API,M_KARTU];
    NSString *paramString = [NSString stringWithFormat:@"msisdn=%@&paymentid=%@&lang=%@",msisdn,paymentId,lang];
    //NSLog(@"paramString = %@",paramString);
    paramString = [EncryptDecrypt doCipherForiOS7:paramString action:kCCEncrypt withKey:saltKeyAPI];
    //NSLog(@"Encrypt paramString = %@",paramString);
    
    url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlString,paramString]];
    //NSLog(@"URL M-Kartu = %@",url);
    
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
    webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:webViewController animated:YES completion:NULL];
}

-(void)getPaketSuka
{
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = USAGE_SIMULATOR_SUKASUKA_REQ;
    [request usageSimulatorSukaSuka];
}

-(void)performPaketSukaSuka:(id)sender
{
    [self getPageSukaSuka];
}

- (void)performQuotaCalculator {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = QUOTA_CALCULATOR_REQ;
    [request quotaCalculator];
}

- (void)performPromo
{
    // TODO: Get rid of the promo cache
    NSLog(@"Perform promo");
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = PROMO_REQ;
    [request promo];
}

- (void)performBalanceSubMenu
{
    /*
     * Google Analytic
     */
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // Screen tracking
    NSString *gaLabel = @"Masa Aktif";
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                          action:@"Home"        // Event action (required)
                                                           label:gaLabel        // Event label
                                                           value:nil] build]];
    [[GAI sharedInstance] dispatch];
    /******************************/
    
    // TODO: Bug fixing tribar menu that doesn't work
    // Reinstantiate the Left Controller
    LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    leftViewController.content = self.content;
    _sideMenuViewController = leftViewController;
    
    BalanceSubmenuViewController *subMenuVC = [[BalanceSubmenuViewController alloc] initWithNibName:@"BalanceSubmenuViewController" bundle:nil];
    
    UIViewController *infoViewController = subMenuVC;
    
    IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:infoViewController leftViewController:_sideMenuViewController];
    deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
    deckController.leftSize = self.view.frame.size.width - 260;
    infoViewController = deckController;
    
    UINavigationController *navController = customizedNavigationController(self.view.bounds,NO);
    [navController setViewControllers:[NSArray arrayWithObject:infoViewController]];
    
    infoViewController = [[IIWrapController alloc] initWithViewController:navController];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController pushViewController:infoViewController animated:YES];
    
}

- (void)performPackageList:(id)sender {
    NSLog(@"Perform Pkg List");
    DataManager *dataManager = [DataManager sharedInstance];
    LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    leftViewController.content = dataManager.leftMenuData;
    _sideMenuViewController = leftViewController;
    NSLog(@"%@ : %@",dataManager.packageMenu.menuName,dataManager.packageMenu.menuId);
    [self performInfo:dataManager.packageMenu withLevelTwoType:COMMON];
}

#pragma mark - PopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController {
    //(@"Did dismiss");
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    //NSLog(@"Should dismiss");
    return YES;
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *repeatResult = [result valueForKey:REPEAT_KEY];
        BOOL repeat = [repeatResult boolValue];
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == CHECK_BALANCE_REQ) {
            DataManager *dataManager = [DataManager sharedInstance];
            dataManager.showBalance = TRUE;
            [self updateView];
        }
        
        if (type == CHECK_USAGE_XL_REQ) {
            
            CheckQuotaViewController *packageViewControllerTmp = [[CheckQuotaViewController alloc] initWithNibName:@"CheckQuotaViewController" bundle:nil];
            packageViewControllerTmp.isLevel2 = YES;
            
            UIViewController *packageViewController = packageViewControllerTmp;
            
            IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:packageViewController leftViewController:_sideMenuViewController];
            deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
            deckController.leftSize = self.view.frame.size.width - 270;
            packageViewController = deckController;
            
            UINavigationController *navController = customizedNavigationController(self.view.bounds,NO);
            [navController setViewControllers:[NSArray arrayWithObject:packageViewController]];
            
            packageViewController = [[IIWrapController alloc] initWithViewController:navController];
            
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            [self.navigationController pushViewController:packageViewController animated:YES];
        }
        
        // TODO : NEPTUNE
        if (type == NOTIFICATION_REQ) {
            
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            [notificationViewController createBarButtonItem:BACK];
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
        
        if (type == MENU_REQ) {
            [self updateView];
        }
        
        if (type == SIGN_OUT_REQ) {
            
            resetAllData();
            
            // Reset Image Ads Counter
            adsImageCounter = 0;
            
            // Reset SlideShow Images
            [_slideshow stop];
            [_slideshow.images removeAllObjects];
            _slideshow.hidden = YES;
            
            AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            appDelegate.loggedIn = NO;
            
            // TODO: V1.9
            // set update device token to be YES again.
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.shouldUpdateDeviceToken = YES;
            
            // TODO: Hygiene
            // Reset this autologin variable on the login view instead
//            appDelegate.isAutoLogin = NO;
            [appDelegate showLoginView];
        }
        
        if (type == XL_TUNAI_BALANCE_REQ) {
            if (repeat) {
                [self performXLTunaiBalance];
            }
            else {
                DataManager *dataManager = [DataManager sharedInstance];
                dataManager.parentId = _selectedMenu.menuId;
                dataManager.menuModel = _selectedMenu;
                
                LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
                leftViewController.content = self.content;
                _sideMenuViewController = leftViewController;
                
                [self performInfo:_selectedMenu withLevelTwoType:XL_TUNAI];
            }
        }
        
        if (type == CHANGE_LANGUAGE_REQ) {
            // TODO: V1.9.5
            // Refresh Navigation Bar
            AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[self.navigationController navigationBar];
            [navBar updateGreeting];
            //-----------------------
            [self requestMenu];
        }
        
        if (type == QUOTA_METER_REQ) {
            // Action for Quota Meter --> Show Quota Meter UI
            QuotaMeterViewController *quotaMeterViewController = [[QuotaMeterViewController alloc] initWithNibName:@"QuotaMeterViewController" bundle:nil];
            [self.navigationController pushViewController:quotaMeterViewController animated:YES];
            quotaMeterViewController = nil;
            [quotaMeterViewController release];
        }
        
        if (type == QUOTA_CALCULATOR_REQ) {
            QuotaCalculatorViewController *packageViewController = [[QuotaCalculatorViewController alloc] initWithNibName:@"QuotaCalculatorViewController" bundle:nil];
            
            [self.navigationController pushViewController:packageViewController animated:YES];
            packageViewController = nil;
            [packageViewController release];
        }
        
        if (type == PROMO_REQ) {
            
            // Changes by iNot at 24 May 2013
            // Set Current Datetime to Preference for cache
            NSDate *current = [NSDate date];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setObject:current
                      forKey:PROMO_CACHE_KEY];
            [prefs synchronize];
            //
            
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.parentId = _selectedMenu.menuId;
            sharedData.menuModel = _selectedMenu;
            
            LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
            leftViewController.content = self.content;
            _sideMenuViewController = leftViewController;
            
            // TODO: V1.9
            // DROP 2
            InfoAndPromoViewController *infoAndPromoVC = [[InfoAndPromoViewController alloc] initWithNibName:@"InfoAndPromoViewController" bundle:nil];
            infoAndPromoVC.contentData = sharedData.promoData;
            infoAndPromoVC.menuModel = _selectedMenu;
            infoAndPromoVC.isParentPromo = YES;
            infoAndPromoVC.pageTitle = _selectedMenu.menuName;
            
            UIViewController *infoViewController = infoAndPromoVC;
            
            IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:infoViewController leftViewController:_sideMenuViewController];
            deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
            deckController.leftSize = self.view.frame.size.width - 260; //270
            infoViewController = deckController;
            
            UINavigationController *navController = customizedNavigationController(self.view.bounds,NO);
            [navController setViewControllers:[NSArray arrayWithObject:infoViewController]];
            
            infoViewController = [[IIWrapController alloc] initWithViewController:navController];
            
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            [self.navigationController pushViewController:infoViewController animated:YES];
        }
    
        // TODO: NEPTUNE
        if (type == PAGE_SUKASUKA_REQ)
        {
            [self navigateToPaketSukaSuka];
        }
        
        // XTRA PROMO CHECK POINT
        if (type == CHECK_POINT_XTRAPROMO_REQ)
        {
            [self redirectToXtraCheckPoint];
        }
        
        // XTRA PROMO WINNER LIST
        if (type == WINNER_LIST_XTRAPROMO_REQ)
        {
            [self redirectToXtraWinnerList];
        }
        
        if (type == CHECKBONUS_XTRAPROMO_REQ)
        {
            [self redirectToXtraGetBonus];
        }
        
        if (type == FOUR_G_POWERPACK_REQ)
            [self redirectToFourGPowerPack];
        
        if (type == FOUR_G_PACKAGE_STATUS_REQ) {
            //[_theTableView reloadData];
            [self updateView];
        }
        
        
        // TODO: v1.9.5
        if (type == QUOTA_IMPROVEMENT_REQ) {
            [_theTableView reloadData];
        }
    }
    else {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == CHECK_BALANCE_REQ) {
            DataManager *dataManager = [DataManager sharedInstance];
            dataManager.showBalance = FALSE;
            [self updateView];
        }
        else if (type == FOUR_G_PACKAGE_STATUS_REQ) {
            [self updateView];
        }
        else {
            NSString *reason = [result valueForKey:REASON_KEY];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                            message:reason
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
}

// TODO: NEPTUNE
-(void)getPageSukaSuka
{
    @try
    {
        [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = PAGE_SUKASUKA_REQ;
        [request pageSukasuka];
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

-(void)navigateToPaketSukaSuka
{
    LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    leftViewController.content = self.content;
    _sideMenuViewController = leftViewController;
    
    PaketSukaMenuViewController *packageViewController = [[PaketSukaMenuViewController alloc] initWithNibName:@"PaketSukaMenuViewController" bundle:nil];
    
    UIViewController *infoViewController = packageViewController;
    
    IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:packageViewController leftViewController:_sideMenuViewController];
    deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
    deckController.leftSize = self.view.frame.size.width - 260; //270
    infoViewController = deckController;
    
    UINavigationController *navController = customizedNavigationController(self.view.bounds,NO);
    [navController setViewControllers:[NSArray arrayWithObject:infoViewController]];
    
    infoViewController = [[IIWrapController alloc] initWithViewController:navController];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController pushViewController:infoViewController animated:YES];
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource {
	
	//  should be calling your tableviews data source model to reload
	
	_reloading = YES;
    
    [self requestMenu];
    [self performBalance];
    [self check4GPackageStatus];
}

- (void)doneLoadingTableViewData {
	
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_theTableView];
	
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	[self reloadTableViewDataSource];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	return _reloading; // should return if data source model is reloading
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	return [NSDate date]; // should return date data source was last changed
}

#pragma mark -
#pragma mark KASlideShowDelegate

- (void) kaSlideShowDidSelectBanner:(int)selectedPackage {
    DataManager *sharedData = [DataManager sharedInstance];
    MenuModel *menuModel = [sharedData.hotOfferBannerData objectAtIndex:selectedPackage];
    
    LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    leftViewController.content = sharedData.leftMenuData;
    _sideMenuViewController = leftViewController;
    
    SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
    subMenuVC.isLevel2 = YES;
    subMenuVC.subMenuContentType = HOT_OFFER_PACKAGE;
    
    //
    NSString *menuId = menuModel.menuId;
    NSArray *newStringArray = [menuId componentsSeparatedByString:@"."];
    NSString *parentId = [NSString stringWithFormat:@"%@",[newStringArray objectAtIndex:0]];
    NSArray *menuArray = [sharedData.menuData objectForKey:parentId];
    MenuModel *parentMenu = [menuArray objectAtIndex:0];
    subMenuVC.grayHeaderTitle = parentMenu.groupName;
    //
    
    UIViewController *infoViewController = subMenuVC;
    IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:infoViewController leftViewController:_sideMenuViewController];
    deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
    deckController.leftSize = self.view.frame.size.width - 260; //270
    infoViewController = deckController;
    UINavigationController *navController = customizedNavigationController(self.view.bounds,NO);
    [navController setViewControllers:[NSArray arrayWithObject:infoViewController]];
    infoViewController = [[IIWrapController alloc] initWithViewController:navController];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController pushViewController:infoViewController animated:YES];
}

#pragma mark - UIWebView Delegate

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.hud hide:YES];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"error %@", [error localizedDescription]);
    [self.hud hide:YES];
    if(_btnRefresh)
        _btnRefresh.hidden = NO;
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestDesc = [[request URL] absoluteString];
    NSString *href = @"#";
    NSRange range = [requestDesc rangeOfString:href options:NSCaseInsensitiveSearch];
    if(![requestDesc isEqualToString:kXtraPromoURL])
    {
        if(range.location != NSNotFound)
        {
            NSString *hrefString = [requestDesc substringFromIndex:range.location];
            [self redirectToPageWithMenuModel:nil orHref:hrefString];
        }
        return NO;
    }
    
    return YES;
}

#pragma mark - Parse Data Delegate

- (void)parseDataDone:(BOOL)success withReason:(NSString*)reason {
    NSLog(@"----Parse Done----");
    [self.theTableView reloadData];
}

@end
