//
//  QuotaImprovementViewCell.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 10/19/15.
//  Copyright © 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnderLineLabel.h"

@interface QuotaImprovementViewCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblTotalPackage;
@property (retain, nonatomic) IBOutlet UILabel *lblPackageActive;
@property (retain, nonatomic) IBOutlet UILabel *lblRemainingData;
@property (retain, nonatomic) IBOutlet UILabel *lblTotalData;
@property (retain, nonatomic) IBOutlet UILabel *lblStartDate;
@property (retain, nonatomic) IBOutlet UILabel *lblEndDate;

@property (retain, nonatomic) IBOutlet UILabel *lblTitleStartDate;
@property (retain, nonatomic) IBOutlet UILabel *lblTitleEndDate;

@property (retain, nonatomic) IBOutlet UIView *bgDataView;
@property (retain, nonatomic) IBOutlet UIView *fgDataView;
@property (retain, nonatomic) IBOutlet UIView *bubbleView;
@property (retain, nonatomic) IBOutlet UIView *unlimitedBorderView;

@property (retain, nonatomic) IBOutlet UIView *daysLeftView;
@property (retain, nonatomic) IBOutlet UIView *daysLeftBgView;
@property (retain, nonatomic) IBOutlet UIView *daysLeftFgView;
@property (retain, nonatomic) IBOutlet UIView *circleLeftView;
@property (retain, nonatomic) IBOutlet UIView *circleRightView;
@property (retain, nonatomic) IBOutlet UIView *circleCenterView;
@property (retain, nonatomic) IBOutlet UILabel *circleCenterLabel;

@property (retain, nonatomic) IBOutlet UIButton *detailButton;
@property (retain, nonatomic) IBOutlet UnderLineLabel *voiceSmsButton;

@end
