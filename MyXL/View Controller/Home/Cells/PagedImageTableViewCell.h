//
//  PagedImageTableViewCell.h
//  MyXL
//
//  Created by Martin Partahi Sitorus on 1/5/16.
//  Copyright (c) 2016 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PagedImageTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIScrollView *scrollViewes;
@property (retain, nonatomic) IBOutlet UIImageView *bnrImgView;
@property (retain, nonatomic) IBOutlet UIPageControl *bnrPageControl;

@end
