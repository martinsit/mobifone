//
//  FeatureMenuMediumView.h
//  MyXL
//
//  Created by tyegah on 8/26/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeatureMenuMediumView : UIView
@property (retain, nonatomic) IBOutlet UILabel *imgIcon;
@property (retain, nonatomic) IBOutlet UILabel *lblMenuName;
@end
