//
//  PagedImageTableViewCell.m
//  MyXL
//
//  Created by Martin Partahi Sitorus on 1/5/16.
//  Copyright (c) 2016 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PagedImageTableViewCell.h"

@implementation PagedImageTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_scrollViewes release];
    [_bnrImgView release];
    [_bnrPageControl release];
    [super dealloc];
}
@end
