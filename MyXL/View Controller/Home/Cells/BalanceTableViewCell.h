//
//  BalanceTableViewCell.h
//  MyXL
//
//  Created by tyegah on 8/26/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BalanceTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIView *balanceView;
@property (retain, nonatomic) IBOutlet UIView *validityView;
@property (retain, nonatomic) IBOutlet UILabel *lblBalanceTitle;
@property (retain, nonatomic) IBOutlet UILabel *lblBalanceValue;
@property (retain, nonatomic) IBOutlet UIImageView *imgBalanceIcon;
@property (retain, nonatomic) IBOutlet UILabel *lblValidityTitle;
@property (retain, nonatomic) IBOutlet UILabel *lblValidityDate;
@property (retain, nonatomic) IBOutlet UIImageView *imgValidityIcon;

@end
