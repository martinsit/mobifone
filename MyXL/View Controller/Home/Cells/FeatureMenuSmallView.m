//
//  FeatureMenuSmallView.m
//  MyXL
//
//  Created by tyegah on 8/26/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "FeatureMenuSmallView.h"

@implementation FeatureMenuSmallView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
    [_imgIcon release];
    [_lblMenuName release];
    [super dealloc];
}
@end
