//
//  BalanceTableViewCell.m
//  MyXL
//
//  Created by tyegah on 8/26/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BalanceTableViewCell.h"

@implementation BalanceTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_balanceView release];
    [_validityView release];
    [_lblBalanceTitle release];
    [_lblBalanceValue release];
    [_imgBalanceIcon release];
    [_lblValidityTitle release];
    [_lblValidityDate release];
    [_imgValidityIcon release];
    [super dealloc];
}
@end
