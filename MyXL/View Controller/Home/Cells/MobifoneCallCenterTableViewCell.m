//
//  MobifoneCallCenterTableViewCell.m
//  MyXL
//
//  Created by Martin Partahi Sitorus on 1/6/16.
//  Copyright (c) 2016 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "MobifoneCallCenterTableViewCell.h"

@implementation MobifoneCallCenterTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
