//
//  NotificationViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/18/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "NotifModel.h"
#import "NotifParamModel.h"
#import "AXISnetRequest.h"

@interface NotificationViewController : BasicViewController <AXISnetRequestDelegate, UITableViewDelegate, UITableViewDataSource> {
    NSArray *_content;
    NotifParamModel *selectedNotifParam;
    UIAlertView *paramAlert;
}

@property (nonatomic, retain) NSArray *content;
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (nonatomic, assign) BOOL shouldProvideBackButton;

@end
