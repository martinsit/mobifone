//
//  NotificationViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/18/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "NotificationViewController.h"
#import "SectionHeaderView.h"
#import "SectionFooterView.h"
#import "FooterView.h"
#import "NotificationCell.h"
#import "AXISnetCellBackground.h"

#import "Constant.h"
#import "AXISnetCommon.h"
#import "DataManager.h"

@interface NotificationViewController ()


@property (strong, nonatomic) UIAlertView *detailAlert;
@property (strong, nonatomic) NotifModel *selectedNotif;
@property (strong, nonatomic) UILabel *unreadLabel;
@property (readwrite) int selectedRow;

- (void)showAlertView:(NotifModel*)notifModel;
- (void)requestNotifParam:(NSDictionary*)paramDict;

@end

@implementation NotificationViewController

@synthesize content = _content;
@synthesize theTableView = _theTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // TODO : HYGIENE
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTableView) name:kNotificationRefreshTable object:nil];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = kDefaultWhiteColor;
    
    DataManager *dataManager = [DataManager sharedInstance];
    
    // TODO : HYGIENE - PUSH NOTIF
    if(dataManager.isRemoteNotificationAvailable)
    {
        //NSLog(@"REMOTE NOTIFICATION AVAILABLE");
        NSString *mailboxID = [dataManager.remoteNotificationInfo valueForKey:@"mailboxID"];
        if([mailboxID length]> 0)
        {
            [self requestNotificationDetailFromPushNotif:mailboxID];
        }
       
        dataManager.isRemoteNotificationAvailable = NO;
        dataManager.remoteNotificationInfo = nil;
    }
    
    dataManager.unread = 0;
    self.badgeNotif.value = dataManager.unread;
    
    [self.theTableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    if(_shouldProvideBackButton)
    {
        [self createBarButtonItem:BACK];
    }
    [self.theTableView reloadData];
}

// TODO : HYGIENE
-(void)viewDidDisappear:(BOOL)animated
{
    [self.hud hide:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:kNotificationRefreshTable];
}

// TODO : HYGIENE
-(void)refreshTableView
{
    [[NSNotificationCenter defaultCenter] removeObserver:kNotificationRefreshTable];
    [_theTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.content = nil;
}

- (void)dealloc {
    [_content release];
    _content = nil;
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    else {
        DataManager *sharedData = [DataManager sharedInstance];
        //return [self.content count];
        return [sharedData.notificationData count];
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NotifCell";
	
    NotificationCell *cell = (NotificationCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		cell = [[NotificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        /*
        cell.backgroundView = [[[AXISnetCellBackground alloc] initWithFrame:cell.frame
                                                           withDefaultColor:kDefaultButtonGrayColor
                                                          withSelectedColor:kDefaultYellowColor] autorelease];
        
        cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] initWithFrame:cell.frame
                                                                   withDefaultColor:kDefaultButtonGrayColor
                                                                  withSelectedColor:kDefaultYellowColor] autorelease];
        
        ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;*/
	}
    
    DataManager *sharedData = [DataManager sharedInstance];
    //NotifModel *notifModel = [self.content objectAtIndex:indexPath.row];
    NotifModel *notifModel = [sharedData.notificationData objectAtIndex:indexPath.row];
    cell.notifLabel.text = notifModel.message;
    
    cell.fromLabel.text = notifModel.from;
    NSArray *dateArray = [notifModel.dateTime componentsSeparatedByString:@"T"];
    NSString *dateString = [dateArray objectAtIndex:0];
    //NSString *yearString = changeDateFormatForNotification(dateString, 3);
    NSString *monthString = notifModel.month;
    //cell.monthLabel.text = [NSString stringWithFormat:@"%@ %@",monthString, yearString];
    cell.monthLabel.text = [NSString stringWithFormat:@"%@",monthString];
    cell.dateLabel.text = changeDateFormatForNotification(dateString, 0);
    
    if ([notifModel.readFlag isEqualToString:@"READ"]) {
        cell.fromLabel.font  = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
        cell.notifLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
        //cell.monthLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        //cell.dateLabel.font  = [UIFont fontWithName:kDefaultFontLight size:40.0];
    }
    else {
        // unread notif
        cell.fromLabel.font  = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
        cell.notifLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
        //cell.monthLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontValueSize];
        //cell.dateLabel.font  = [UIFont fontWithName:kDefaultFont size:40.0];
    }

    
    UIImage *backgroundImage;
    if ([notifModel.readFlag isEqualToString:@"READ"]) {
        backgroundImage = imageFromColor(kDefaultSeparatorColor);
    }
    else {
        backgroundImage = imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuFeature));
    }
    /*
    if ([notifModel.notifType isEqualToString:@"-1"]) {
        backgroundImage = imageFromColor(kDefaultRedColor);
    }
    else if ([notifModel.notifType isEqualToString:@"0"]) {
        backgroundImage = imageFromColor(kDefaultYellowColor);
    }
    else if ([notifModel.notifType isEqualToString:@"1"]) {
        backgroundImage = imageFromColor(kDefaultGreenColor);
    }
    else {
        backgroundImage = imageFromColor(kDefaultPurpleColor);
    }*/
    cell.notifTypeView.image = backgroundImage;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return [[Language get:@"inbox" alter:nil] uppercaseString];
    }
    else {
        return [[Language get:@"message" alter:nil] uppercaseString];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.0;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    DataManager *sharedData = [DataManager sharedInstance];
    //NotifModel *notifModel = [self.content objectAtIndex:indexPath.row];
    NotifModel *notifModel = [sharedData.notificationData objectAtIndex:indexPath.row];
    _selectedNotif = notifModel;
    _selectedRow = indexPath.row;
    [self requestNotifDetail:_selectedNotif];
    
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    /*
    NotifModel *notifModel = [self.content objectAtIndex:indexPath.row];
    NSArray *dateArray = [notifModel.dateTime componentsSeparatedByString:@"T"];
    NSString *dateString = [dateArray objectAtIndex:0];
    NSLog(@"--> %@",dateString);
    dateString = changeDateFormatForNotification(dateString, 2);
    
    if ([notifModel.notifType isEqualToString:@"2"]) {
        if ([notifModel.replyStatus isEqualToString:@"1"]) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",[Language get:@"notification" alter:nil]]
                                                            message:[NSString stringWithFormat:@"%@\n%@\n\n%@: %@",
                                                                     dateString,
                                                                     notifModel.message,
                                                                     [Language get:@"your_answer" alter:nil],
                                                                     notifModel.replyValue]
                                                           delegate:self
                                                  cancelButtonTitle:[Language get:@"ok" alter:nil]
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        else {
            [self showAlertView:notifModel];
            selectedNotif = notifModel;
        }
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",[Language get:@"inbox" alter:nil]]
                                                        message:[NSString stringWithFormat:@"%@\n%@",dateString,notifModel.message]
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"ok" alter:nil]
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }*/
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight*1.5)
                                                withLabelForXL:sectionTitle
                                                isFirstSection:YES] autorelease];
    }
    else {
        /*
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                withLabelForXL:sectionTitle
                                                isFirstSection:NO] autorelease];*/
        NSString *unreadString = [NSString stringWithFormat:@"%i",[[DataManager sharedInstance] unreadMsg]];
        headerView = [[[SectionHeaderView alloc] initForInboxWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight) withTitle:sectionTitle withUnread:unreadString] autorelease];
        _unreadLabel = headerView.unreadLabel;
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return kDefaultCellHeight*1.5;
    }
    else {
        return kDefaultCellHeight;
    }
}

#pragma mark - Selector

- (void)showAlertView:(NotifModel*)notifModel {
    UIAlertView* av = [UIAlertView new];
    
    NSArray *dateArray = [notifModel.dateTime componentsSeparatedByString:@"T"];
    NSString *dateString = [dateArray objectAtIndex:0];
    dateString = changeDateFormatForNotification(dateString, 2);
    
    av.title = [NSString stringWithFormat:@"%@",[Language get:@"notification" alter:nil]];
    av.message = [NSString stringWithFormat:@"%@\n%@",dateString,notifModel.message];
    
    // Add Buttons
    for (int i=0; i < [notifModel.param count]; i++) {
        NotifParamModel *aNotifParam = [notifModel.param objectAtIndex:i];
        [av addButtonWithTitle:aNotifParam.label];
    }
    paramAlert = av;
    [paramAlert show];
    paramAlert.delegate = self;
}

- (void)requestNotifParam:(NSString*)mailboxId {
    /*
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = REPLY_NOTIFICATION_REQ;
    
    [request postNotifParam:[paramDict valueForKey:@"notifId"]
                buttonIndex:[paramDict valueForKey:@"buttonIndex"]
                    element:[paramDict valueForKey:@"element"]];*/
}

// TODO : HYGIENE
// Display the notification message sent from push notification
-(void)requestNotificationDetailFromPushNotif:(NSString *)mailboxID
{
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = NOTIFICATION_DETAIL_REQ;
    
    [request notificationDetail:mailboxID];
}

- (void)requestNotifDetail:(NotifModel*)model {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = NOTIFICATION_DETAIL_REQ;
    
    [request notificationDetail:model.idNotif];
}

- (void)requestNotifDelete:(NotifModel*)model {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = NOTIFICATION_DELETE_REQ;
    
    [request notificationDelete:model.idNotif];
}

- (void)requestNotif {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = NOTIFICATION_REQ;
    [request notification];
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView == paramAlert) {
        NotifParamModel *aNotifParamModel = [_selectedNotif.param objectAtIndex:buttonIndex];
        selectedNotifParam = aNotifParamModel;
        NSDictionary *paramDict = [[NSDictionary alloc] initWithObjectsAndKeys:_selectedNotif.idNotif,@"notifId",aNotifParamModel.idx,@"buttonIndex",@"option",@"element",nil];
        
        [self requestNotifParam:paramDict];
        [paramDict release];
    }
    
    if (alertView == _detailAlert) {
        if (buttonIndex == 0) {
            
            
            if ([[[DataManager sharedInstance] detailNotificationData].readFlag isEqualToString:@"UNREAD"]) {
                NSString *unreadString = [NSString stringWithFormat:@"%i",[[DataManager sharedInstance] unreadMsg]];
                _unreadLabel.text = unreadString;
                
                NotifModel *notifModel = [[[DataManager sharedInstance] notificationData] objectAtIndex:_selectedRow];
                notifModel.readFlag = @"READ";
                
                NSIndexPath *rowToReload = [NSIndexPath indexPathForRow:_selectedRow inSection:1];
                NSArray *rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
                [_theTableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationRight];
            }
        }
        else {
            
            [self requestNotifDelete:_selectedNotif];
        }
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == REPLY_NOTIFICATION_REQ) {
            if ([selectedNotifParam.type isEqualToString:@"DISPLAY"]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:selectedNotifParam.content
                                                               delegate:self
                                                      cancelButtonTitle:[Language get:@"ok" alter:nil]
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
            else {
                DataManager *sharedData = [DataManager sharedInstance];
                NSString *desc = sharedData.replyNotifDesc;
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:desc
                                                               delegate:self
                                                      cancelButtonTitle:[Language get:@"ok" alter:nil]
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
        }
        
        if (type == NOTIFICATION_DETAIL_REQ) {
            DataManager *sharedData = [DataManager sharedInstance];
            NSString *desc = sharedData.detailNotificationData.detailMessage;
            
            NSArray *dateArray = [sharedData.detailNotificationData.dateTime componentsSeparatedByString:@"T"];
            NSString *dateString = [dateArray objectAtIndex:0];
            // Setup Date Format
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            // 2013-11-27
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [dateFormatter dateFromString:dateString];
            
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            dateString = [dateFormatter stringFromDate:date];
            //------------------
            
            NSString *timeString = [dateArray objectAtIndex:1];
            NSArray *timeArray = [timeString componentsSeparatedByString:@"."];
            timeString = [timeArray objectAtIndex:0];
            
            dateString = [NSString stringWithFormat:@"%@ %@",dateString,timeString];
            
            //NSString *dateString = changeDateTimeFormatForNotification(sharedData.detailNotificationData.dateTime);
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"message" alter:nil]
                                                            message:[NSString stringWithFormat:@"%@\n\n%@",dateString,desc]
                                                           delegate:self
                                                  cancelButtonTitle:[Language get:@"close" alter:nil]
                                                  otherButtonTitles:[Language get:@"delete" alter:nil], nil];
            _detailAlert = alert;
            [_detailAlert show];
            [_detailAlert release];
        }
        
        if (type == NOTIFICATION_DELETE_REQ) {
            [self requestNotif];
        }
        
        if (type == NOTIFICATION_REQ) {
            [_theTableView reloadData];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"ok" alter:nil]
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
