//
//  SubMenuViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "SubMenuViewController.h"
#import "FooterView.h"
#import "DataManager.h"
#import "GroupingAndSorting.h"

#import "CommonCell.h"
#import "BuyPackageCell.h"
#import "GeneralInfoCell.h"
#import "BorderCellBackground.h"
#import "AXISnetCellBackground.h"
#import "SectionHeaderView.h"
#import "SectionFooterView.h"

#import "GiftPackageViewController.h"

#import "Constant.h"
#import "ContactUsViewController.h"
#import "PUKViewController.h"
#import "DeviceSettingViewController.h"
#import "ShopLocatorViewController.h"
#import "FAQViewController.h"
#import "ThankYouViewController.h"
#import "NotificationViewController.h"

#import "AXISnetCommon.h"

#import "AppDelegate.h"

#import "FBAlertView.h"

#import "UpdateProfileViewController.h"

#import "ReloadBalanceViewController.h"

#import "TransactionHistoryViewController.h"

#import "PromoViewController.h"

#import "RoamingViewController.h"

#import "ContactUsModel.h"

#import "XLTunaiReloadViewController.h"
#import "XLTunaiPLNViewController.h"

#import "RectBackground.h"

#import "XLStarCell.h"

#import "CheckQuotaViewController.h"

#import "IIViewDeckController.h"

#import "PaymentChoiceViewController.h"

#import "EncryptDecrypt.h"

#import "SVWebViewController.h"

#import "QuotaCalculatorViewController.h"

#import "PaketSukaViewController.h"
#import "PaketSukaMenuViewController.h"

#import "PromoModel.h"

#import "QuotaEnhanceViewController.h"

#import "InfoAndPromoViewController.h"

@interface SubMenuViewController ()

@property (strong, nonatomic) MenuModel *selectedMenu;
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (nonatomic, retain) NSIndexPath *checkedIndexPath;
@property (readwrite) CGFloat extraHeight;

- (void)settingData;
- (void)performExtendValidity:(NSString*)amount;
- (void)performBuyPackage:(NSString*)packageId withAmount:(NSString*)amount;
- (void)performContactUs;
- (void)performPUK:(MenuModel*)selectedMenu;
- (void)performDeviceSetting:(NSString*)type;
- (void)performInfo:(id)sender;
- (void)performActivate:(id)sender;
- (void)performInfoForCommonCell:(id)sender;

@end

@implementation SubMenuViewController

@synthesize sortedMenu = _sortedMenu;
@synthesize theSourceMenu = _theSourceMenu;

@synthesize parentMenuModel = _parentMenuModel;

@synthesize parentId = _parentId;
@synthesize groupDesc = _groupDesc;

@synthesize recommendedPackageModel = _recommendedPackageModel;
@synthesize upgradePackageModel = _upgradePackageModel;

@synthesize isLevel2 = _isLevel2;

@synthesize isHotOffer = _isHotOffer;

@synthesize currentPackage = _currentPackage;

@synthesize trxId = _trxId;

@synthesize levelTwoType = _levelTwoType;

@synthesize star = _star;

@synthesize grayHeaderTitle = _grayHeaderTitle;

@synthesize subMenuViewController = _subMenuViewController;

@synthesize subMenuContentType = _subMenuContentType;

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        self.screenName = @"BuyPackage";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
    FooterView *footer = [[[FooterView alloc] initWithFrame:CGRectMake(0.0, 0.0, _theTableView.bounds.size.width, 50.0)] autorelease];
    _theTableView.tableFooterView = footer;*/
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)viewWillAppear:(BOOL)animated {
    
    //_groupDesc = @"Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.";
    
    self.screenName = @"BuyPackageiOS";
    [super viewWillAppear:animated];
    
    [self settingData];
    
    if (_isLevel2) {
        [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    }
    else {
        [self createBarButtonItem:BACK_NOTIF];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"BuyPackageiOS"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [[GAI sharedInstance] dispatch];
    [super viewDidAppear:animated];
}

#pragma mark - Selector

- (void)settingData {
    DataManager *dataManager = [DataManager sharedInstance];
    NSArray *contentTmp = nil;
    if (_recommendedPackageModel) {
        contentTmp = [NSArray arrayWithArray:dataManager.recommendedData];
    }
    else if (_upgradePackageModel) {
        contentTmp = [NSArray arrayWithArray:dataManager.upgradePackageData];
    }
    else {
        /*
        if (_isHotOffer) {
            contentTmp = dataManager.hotOfferBannerData;
        }
        else {
            //contentTmp = [dataManager.menuData valueForKey:_parentId];
            contentTmp = [dataManager.menuData valueForKey:_parentMenuModel.menuId];
        }*/
        
        if (_subMenuContentType == HOT_OFFER_PACKAGE) {
            contentTmp = dataManager.hotOfferBannerData;
        }
        else if (_subMenuContentType == RECOMMENDED_XL_PACKAGE) {
            contentTmp = dataManager.recommendedPackageXLData;
        }
        else if ([_parentId length] > 0)
        {
            contentTmp = [dataManager.menuData valueForKey:_parentId];
        }
        else
        {
            contentTmp = [dataManager.menuData valueForKey:_parentMenuModel.menuId];
        }
    }
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    self.sortedMenu = [sortingResult allKeys];
    self.sortedMenu = [GroupingAndSorting doSortingByGroupId:self.sortedMenu];
    self.theSourceMenu = sortingResult;
    
    // Set Parent
    //self.currentSortedMenu = self.sortedMenu;
    //self.currentSourceMenu = self.theSourceMenu;
    //
    
    for (int i=0; i<[self.sortedMenu count]; i++) {
        NSString *groupIdOfMenu = [self.sortedMenu objectAtIndex:i];
        NSArray *objectsForMenu = [self.theSourceMenu objectForKey:groupIdOfMenu];
        for (MenuModel *menuModel in objectsForMenu) {
            menuModel.open = NO;
        }
    }
    self.checkedIndexPath = nil;
    [self.theTableView reloadData];
}

- (void)performExtendValidity:(NSString*)amount {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = EXTEND_VALIDITY_REQ;
    [request extendValidity:amount];
}

- (void)performBuyPackage:(NSString*)packageId withAmount:(NSString*)amount {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = BUY_PACKAGE_REQ;
    self.trxId = generateUniqueString();
    [request buyPackage:packageId withAmount:amount withTrxId:self.trxId];
}

- (void)performContactUs {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = CONTACT_US_REQ;
    [request contactUs:@"Prepaid"];
}

- (void)performInfoRoaming {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = ROAMING_REQ;
    [request infoRoaming:@"Prepaid"];
}

- (void)performPUK:(MenuModel*)selectedMenu {
    PUKViewController *pukViewController = [[PUKViewController alloc] initWithNibName:@"PUKViewController" bundle:nil];
    pukViewController.menuModel = selectedMenu;
    
    [pukViewController createBarButtonItem:BACK_NOTIF];
    
    [self.navigationController pushViewController:pukViewController animated:YES];
    pukViewController = nil;
    [pukViewController release];
}

- (void)performDeviceSetting:(NSString*)type {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = DEVICE_SETTING_REQ;
    [request deviceSetting:type];
}

- (void)performInfo:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_theTableView];
    NSIndexPath *selectedIndexPath = [_theTableView indexPathForRowAtPoint:buttonPosition];
    if (selectedIndexPath != nil)
    {
        NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:selectedIndexPath.section];
        NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
        
        MenuModel *menuModel = nil;
        
        if ([_groupDesc length] > 0) {
            if (selectedIndexPath.row != 0) {
                menuModel = [objectsForMenu objectAtIndex:selectedIndexPath.row - 1];
            }
        }
        else {
            menuModel = [objectsForMenu objectAtIndex:selectedIndexPath.row];
        }
        
        // Uncheck the previous checked row
        if (self.checkedIndexPath)
        {
            NSString *groupIdOfMenuLast = [_sortedMenu objectAtIndex:self.checkedIndexPath.section];
            NSArray *objectsForMenuLast = [_theSourceMenu objectForKey:groupIdOfMenuLast];
            
            MenuModel *uncheckSection;
            if ([_groupDesc length] > 0) {
                uncheckSection = [objectsForMenuLast objectAtIndex:self.checkedIndexPath.row - 1];
            }
            else {
                uncheckSection = [objectsForMenuLast objectAtIndex:self.checkedIndexPath.row];
            }
            
            uncheckSection.open = NO;
        }
        
        if ([self.checkedIndexPath isEqual:selectedIndexPath])
        {
            self.checkedIndexPath = nil;
        }
        else
        {
            menuModel.open = YES;
            self.checkedIndexPath = selectedIndexPath;
            
            BuyPackageCell *selectedCell = (BuyPackageCell*)[_theTableView cellForRowAtIndexPath:selectedIndexPath];
            
            CGSize size = calculateExpectedSize(selectedCell.descLabel, menuModel.desc);
            
            _extraHeight = size.height;
        }
        
        [_theTableView reloadData];
    }
}

- (void)performActivate:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_theTableView];
    NSIndexPath *selectedIndexPath = [_theTableView indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"selectedIndexPath.section = %i",selectedIndexPath.section);
    //NSLog(@"selectedIndexPath.row = %i",selectedIndexPath.row);
    if (selectedIndexPath != nil)
    {
        NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:selectedIndexPath.section-1];
        NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
        
        MenuModel *menuModel = nil;
        
        if ([_groupDesc length] > 0) {
            if (selectedIndexPath.row != 0) {
                menuModel = [objectsForMenu objectAtIndex:selectedIndexPath.row - 1];
            }
        }
        else {
            menuModel = [objectsForMenu objectAtIndex:selectedIndexPath.row];
        }
        
        _selectedMenu = menuModel;
        
        if ([menuModel.href isEqualToString:@"#extend_validity"]) {
            DataManager *sharedData = [DataManager sharedInstance];
            NSString *price = [NSString stringWithFormat:@"%@",addThousandsSeparator(menuModel.price, sharedData.profileData.language)];
            NSString *confirmationMessage = @"";
            if ([sharedData.profileData.language isEqualToString:@"id"]) {
                confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@. \n %@",
                                       [Language get:@"extend_confirmation_1" alter:nil],
                                       price,
                                       [Language get:@"extend_confirmation_2" alter:nil],
                                       menuModel.duration,
                                       [Language get:@"ask_continue" alter:nil]];
            }
            else {
                confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ \n %@",
                                       price,
                                       [Language get:@"extend_confirmation_1" alter:nil],
                                       menuModel.duration,
                                       [Language get:@"extend_confirmation_2" alter:nil],
                                       [Language get:@"ask_continue" alter:nil]];
            }
            
            UIAlertView *extendValidityAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                          message:confirmationMessage
                                                                         delegate:self
                                                                cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                                otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
            extendValidityAlert.tag = 1;
            
            [extendValidityAlert show];
            [extendValidityAlert release];
        }
        else if ([menuModel.href isEqualToString:@"#buy_package"] || [menuModel.href isEqualToString:@"buy_package"]) {
            /*
            DataManager *sharedData = [DataManager sharedInstance];
            NSString *price = [NSString stringWithFormat:@"%@",addThousandsSeparator(menuModel.price, sharedData.profileData.language)];
            NSString *confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@. \n %@",
                                             [Language get:@"buy_package_confirmation_1" alter:nil],
                                             price,
                                             [Language get:@"buy_package_confirmation_2" alter:nil],
                                             menuModel.groupName,
                                             menuModel.menuName,
                                             [Language get:@"buy_package_confirmation_3" alter:nil],
                                             menuModel.duration,
                                             [Language get:@"ask_continue" alter:nil]];*/
            
            NSString *confirmationMessage = _selectedMenu.confirmDesc;
            
            // DROP 2
            if([_selectedMenu.paymentMethod isEqualToString:@"cc"])
            {
                // TODO : V1.9
                // Add pay with credit card on buy package alert
                UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                          message:confirmationMessage
                                                                         delegate:self
                                                                cancelButtonTitle:[[Language get:@"pay_with_cc" alter:nil] uppercaseString]
                                                                otherButtonTitles:[[Language get:@"yes" alter:nil] uppercaseString],[[Language get:@"no" alter:nil] uppercaseString],nil];
                buyPackageAlert.tag = 4;
                
                [buyPackageAlert show];
                [buyPackageAlert release];
            }
            else
            {
                UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                          message:confirmationMessage
                                                                         delegate:self
                                                                cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                                otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
                
                buyPackageAlert.tag = 2;
                
                [buyPackageAlert show];
                [buyPackageAlert release];
            }
        }
        else if ([menuModel.href isEqualToString:@"#gift_package"]) {
            GiftPackageViewController *giftPackageViewController = [[GiftPackageViewController alloc] initWithNibName:@"GiftPackageViewController" bundle:nil];
            
            giftPackageViewController.isThankYouPage = NO;
            giftPackageViewController.giftPackageModel = menuModel;
            giftPackageViewController.headerTitleMaster = menuModel.groupName;
            giftPackageViewController.headerIconMaster = icon(menuModel.parentId);
            
            [self.navigationController pushViewController:giftPackageViewController animated:YES];
            giftPackageViewController = nil;
            [giftPackageViewController release];
        }
        else if ([menuModel.href isEqualToString:@"#up_package"]) {
            DataManager *sharedData = [DataManager sharedInstance];
            NSString *price = [NSString stringWithFormat:@"%@",addThousandsSeparator(menuModel.price, sharedData.profileData.language)];
            
            NSString *confirmationMessage = @"";
            if ([sharedData.profileData.language isEqualToString:@"id"]) {
                confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@. \n %@",
                                       [Language get:@"buy_package_confirmation_1" alter:nil],
                                       price,
                                       [Language get:@"upgrade_package_confirmation_2" alter:nil],
                                       menuModel.menuName,
                                       [Language get:@"buy_package_confirmation_3" alter:nil],
                                       menuModel.duration,
                                       [Language get:@"upgrade_package_confirmation_4" alter:nil],
                                       _currentPackage,
                                       [Language get:@"ask_continue" alter:nil]];
            }
            else {
                confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@. \n %@",
                                       [Language get:@"buy_package_confirmation_1" alter:nil],
                                       price,
                                       [Language get:@"upgrade_package_confirmation_2" alter:nil],
                                       menuModel.menuName,
                                       [Language get:@"buy_package_confirmation_3" alter:nil],
                                       menuModel.duration,
                                       [Language get:@"upgrade_package_confirmation_4" alter:nil],
                                       _currentPackage,
                                       [[Language get:@"package" alter:nil] lowercaseString],
                                       [Language get:@"ask_continue" alter:nil]];
            }
            
            UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                      message:confirmationMessage
                                                                     delegate:self
                                                            cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                            otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
            
            buyPackageAlert.tag = 2;
            
            [buyPackageAlert show];
            [buyPackageAlert release];
        }
        else {
        }
    }
}

- (void)performInfoForCommonCell:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_theTableView];
    NSIndexPath *selectedIndexPath = [_theTableView indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"--> select section = %i",selectedIndexPath.section);
    //NSLog(@"--> select row = %i",selectedIndexPath.row);
    if (selectedIndexPath != nil)
    {
        NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:selectedIndexPath.section - 1];
        NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
        MenuModel *menuModel = [objectsForMenu objectAtIndex:selectedIndexPath.row];
        
        // Uncheck the previous checked row
        if (self.checkedIndexPath)
        {
            NSString *groupIdOfMenuLast = [_sortedMenu objectAtIndex:self.checkedIndexPath.section - 1];
            NSArray *objectsForMenuLast = [_theSourceMenu objectForKey:groupIdOfMenuLast];
            
            MenuModel *uncheckSection = [objectsForMenuLast objectAtIndex:self.checkedIndexPath.row];
            uncheckSection.open = NO;
        }
        
        if ([self.checkedIndexPath isEqual:selectedIndexPath])
        {
            self.checkedIndexPath = nil;
        }
        else
        {
            menuModel.open = YES;
            self.checkedIndexPath = selectedIndexPath;
            
//            CommonCell *selectedCell = (CommonCell*)[_theTableView cellForRowAtIndexPath:selectedIndexPath];
//            
//            CGSize size = calculateExpectedSize(selectedCell.descLabel, menuModel.desc);
//            
//            _extraHeight = size.height;
//            NSLog(@"&&&&&&&&&&&&&& PERFORM desc _extraHeight = %f",_extraHeight);
//            
//            CGRect frame = selectedCell.descLabel.frame;
//            frame.size.height = _extraHeight;
//            selectedCell.descLabel.frame = frame;
        }
        
        
    }
    
    [_theTableView reloadData];
}

- (void)performUpdateProfile {
    DataManager *sharedData = [DataManager sharedInstance];
    int value = [sharedData.profileData.status intValue];
    if (value == 2) {
        [self requestProfileInfo];
    }
    else {
        UpdateProfileViewController *updateProfileViewController = [[UpdateProfileViewController alloc] initWithNibName:@"UpdateProfileViewController" bundle:nil];
        [updateProfileViewController createBarButtonItem:BACK_NOTIF];
        updateProfileViewController.menuModel = _selectedMenu;
        [self.navigationController pushViewController:updateProfileViewController animated:YES];
        updateProfileViewController = nil;
        [updateProfileViewController release];
    }
}

- (void)requestProfileInfo {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = PROFILE_INFO_REQ;
    [request profileInfo];
}

- (void)requestTransactionHistory:(NSString*)type {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = TRX_HISTORY_REQ;
    [request lastTransaction:type];
}

- (void)performPromo {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDate *promoCacheDate = [prefs objectForKey:PROMO_CACHE_KEY];
    NSDate *currentDate = [NSDate date];
    NSTimeInterval secondsBetween = [currentDate timeIntervalSinceDate:promoCacheDate];
    float minutes = secondsBetween / 60;
    
    if (promoCacheDate == nil || minutes > kDefaultCache) {
//        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        self.hud.labelText = [Language get:@"loading" alter:nil];
        [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = PROMO_REQ;
        [request promo];
    }
    else {
        /*
        PromoViewController *promoViewController = [[PromoViewController alloc] initWithNibName:@"PromoViewController" bundle:nil];
        [promoViewController createBarButtonItem:BACK];
        [self.navigationController pushViewController:promoViewController animated:YES];
        promoViewController = nil;
        [promoViewController release];*/
        
        DataManager *sharedData = [DataManager sharedInstance];
        
//        ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
//        contactUsViewController.content = sharedData.promoData;
//        contactUsViewController.accordionType = PROMO_ACC;
//        contactUsViewController.menuModel = _selectedMenu;
        
        InfoAndPromoViewController *infoAndPromoVC = [[InfoAndPromoViewController alloc] initWithNibName:@"InfoAndPromoViewController" bundle:nil];
        infoAndPromoVC.contentData = sharedData.promoData;
        infoAndPromoVC.menuModel = _selectedMenu;
        infoAndPromoVC.isParentPromo = YES;
        infoAndPromoVC.pageTitle = _selectedMenu.menuName;
        
        [self.navigationController pushViewController:infoAndPromoVC animated:YES];
        infoAndPromoVC = nil;
        [infoAndPromoVC release];
    }
}

- (void)performXLStar {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = XL_STAR_REQ;
    [request xlStar];
}

- (void)performCheckQuota {
    /*
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = CHECK_USAGE_XL_REQ;
    [request checkInternetUsageXL];*/
    
    // TODO: V1.9.5
    QuotaEnhanceViewController *packageViewControllerTmp = [[QuotaEnhanceViewController alloc] initWithNibName:@"QuotaEnhanceViewController" bundle:nil];
    [self.navigationController pushViewController:packageViewControllerTmp animated:YES];
    packageViewControllerTmp = nil;
    [packageViewControllerTmp release];
}

- (void)performQuotaCalculator {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = QUOTA_CALCULATOR_REQ;
    [request quotaCalculator];
}

- (void)performMKartu {
    NSURL *url = nil;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    NSString *paymentId = @"1"; //hardcoded
    NSString *lang = sharedData.profileData.language;
    
    NSString *baseUrlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API,M_KARTU];
    NSString *paramString = [NSString stringWithFormat:@"msisdn=%@&paymentid=%@&lang=%@",msisdn,paymentId,lang];
    //NSLog(@"paramString = %@",paramString);
    paramString = [EncryptDecrypt doCipherForiOS7:paramString action:kCCEncrypt withKey:saltKeyAPI];
    //NSLog(@"Encrypt paramString = %@",paramString);
    
    url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlString,paramString]];
    //NSLog(@"URL M-Kartu = %@",url);
    
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
	webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:webViewController animated:YES completion:NULL];
}

- (void)performAutoReload {
    NSURL *url = nil;
    url = [NSURL URLWithString:@"https://isipulsa.xl.co.id/login"];
    
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
    webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:webViewController animated:YES completion:NULL];
}

- (void)perform4gusim {
    
    /*
     * Request to API
     */
    
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = INFO_4G_USIM_REQ;
    [request info4GUsim];
    
    /*
     * Dummy Data
     */
    //[self dummyDataInfo4G];
    //---------
    
}

- (void)dummyDataInfo4G {
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
    
    PromoModel *model = [[PromoModel alloc] init];
    model.promoId = @"1";
    model.promoTitle = @"General Information";
    model.promoBody = @"";
    model.promoDateTime = @"";
    model.promoUrl = @"http://www.xl.co.id/4G";
    model.promoStatus = @"";
    model.promoShortDesc = @"4G is the most fastest mobile technology network to browse, streaming, gaming and/or download/upload content.";
    model.promoMenuId = @"";
    model.href = @"#show";
    [contentTmp addObject:model];
    model = nil;
    
    model = [[PromoModel alloc] init];
    model.promoId = @"1";
    model.promoTitle = @"XL 4G Coverage Area";
    model.promoBody = @"";
    model.promoDateTime = @"";
    model.promoUrl = @"http://www.xl.co.id/4G";
    model.promoStatus = @"";
    model.promoShortDesc = @"Currently XL 4G already available on Jakarta, Bogor, Yogyakarta, Medan, Bandung and Surabaya. More information please click www.xl.co.id/4G";
    model.promoMenuId = @"";
    model.href = @"#show";
    [contentTmp addObject:model];
    model = nil;
    
    model = [[PromoModel alloc] init];
    model.promoId = @"1";
    model.promoTitle = @"Device 4G";
    model.promoBody = @"";
    model.promoDateTime = @"";
    model.promoUrl = @"http://www.xl.co.id/4G";
    model.promoStatus = @"";
    model.promoShortDesc = @"Currently 4G services only for 4G smartphone device, ensure your device has 4G network capability";
    model.promoMenuId = @"";
    model.href = @"#show";
    [contentTmp addObject:model];
    model = nil;
    
    model = [[PromoModel alloc] init];
    model.promoId = @"1";
    model.promoTitle = @"4G USIM Card";
    model.promoBody = @"";
    model.promoDateTime = @"";
    model.promoUrl = @"http://www.xl.co.id/4G";
    model.promoStatus = @"";
    model.promoShortDesc = @"To get best experience of XL 4G network, you have to upgrade your SIM Card with 4G SIM on XL Center near you";
    model.promoMenuId = @"";
    model.href = @"#show";
    [contentTmp addObject:model];
    model = nil;
    
    model = [[PromoModel alloc] init];
    model.promoId = @"1";
    model.promoTitle = @"Replace Your SIM Card";
    model.promoBody = @"";
    model.promoDateTime = @"";
    model.promoUrl = @"";
    model.promoStatus = @"";
    model.promoShortDesc = @"To enable 4G internet service you must replace your old SIM Card with the 4G USIM card. You can do following actions for replacement :\nCome to nearest XL Center.\nComplete your My Profile and press Ganti Kartu 4G button here";
    model.promoMenuId = @"";
    model.href = @"#replacesim";
    [contentTmp addObject:model];
    model = nil;
    
    model = [[PromoModel alloc] init];
    model.promoId = @"1";
    model.promoTitle = @"4G Internet Package";
    model.promoBody = @"";
    model.promoDateTime = @"";
    model.promoUrl = @"";
    model.promoStatus = @"";
    model.promoShortDesc = @"To enable 4G internet service you must replace your old SIM Card with the 4G USIM card. You can do following actions for replacement :\nCome to nearest XL Center.\nComplete your My Profile and press Ganti Kartu 4G button here";
    model.promoMenuId = @"";
    model.href = @"#showing";
    [contentTmp addObject:model];
    model = nil;
    
    model = [[PromoModel alloc] init];
    model.promoId = @"1";
    model.promoTitle = @"Comment & Rating";
    model.promoBody = @"";
    model.promoDateTime = @"";
    model.promoUrl = @"";
    model.promoStatus = @"";
    model.promoShortDesc = @"";
    model.promoMenuId = @"";
    model.href = @"#comment";
    [contentTmp addObject:model];
    model = nil;
    
    sharedData.info4gData = [NSArray arrayWithArray:contentTmp];
    
    ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
    contactUsViewController.content = sharedData.info4gData;
    contactUsViewController.accordionType = INFO4G_ACC;
    contactUsViewController.menuModel = _selectedMenu;
    
    [self.navigationController pushViewController:contactUsViewController animated:YES];
    contactUsViewController = nil;
    [contactUsViewController release];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        DataManager *sharedData = [DataManager sharedInstance];
        
        if (type == EXTEND_VALIDITY_REQ) {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            
            thankYouViewController.info = sharedData.extendValidityData.statusText;
            //thankYouViewController.headerTitleMaster = _selectedMenu.groupName;
            //thankYouViewController.headerIconMaster = icon(_selectedMenu.parentId);
            thankYouViewController.menuModel = _selectedMenu;
            thankYouViewController.displayFBButton = NO;
            thankYouViewController.hideInboxBtn = NO;
            
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            thankYouViewController = nil;
            [thankYouViewController release];
        }
        
        if (type == BUY_PACKAGE_REQ) {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            
            //thankYouViewController.info = [Language get:@"buy_package_thank_you" alter:nil];
            NSString *reason = [result valueForKey:REASON_KEY];
            thankYouViewController.info = reason;
            //thankYouViewController.headerTitleMaster = _selectedMenu.groupName;
            //thankYouViewController.headerIconMaster = icon(_selectedMenu.parentId);
            thankYouViewController.displayFBButton = NO;
            thankYouViewController.trxId = self.trxId;
            thankYouViewController.menuModel = _selectedMenu;
            thankYouViewController.hideInboxBtn = NO;
            
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            thankYouViewController = nil;
            [thankYouViewController release];
        }
        
        if (type == CONTACT_US_REQ) {
            ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
            
            contactUsViewController.content = sharedData.contactUsData;
            contactUsViewController.accordionType = CONTACT_US_ACC;
            contactUsViewController.menuModel = _selectedMenu;
            
            [contactUsViewController createBarButtonItem:BACK_NOTIF];
            
            //contactUsViewController.headerTitleMaster = _selectedMenu.menuName;
            //contactUsViewController.headerIconMaster = icon(_selectedMenu.menuId);
            
            [self.navigationController pushViewController:contactUsViewController animated:YES];
            contactUsViewController = nil;
            [contactUsViewController release];
        }
        
        if (type == ROAMING_REQ) {
            RoamingViewController *roamingViewController = [[RoamingViewController alloc] initWithNibName:@"RoamingViewController" bundle:nil];
            roamingViewController.menuModel = _selectedMenu;
            
            [roamingViewController createBarButtonItem:BACK_NOTIF];
            
            [self.navigationController pushViewController:roamingViewController animated:YES];
            roamingViewController = nil;
            [roamingViewController release];
        }
        
        if (type == DEVICE_SETTING_REQ) {
            DeviceSettingViewController *deviceSettingViewController = [[DeviceSettingViewController alloc] initWithNibName:@"DeviceSettingViewController" bundle:nil];
            
            deviceSettingViewController.data = sharedData.deviceSettingData;
            
            deviceSettingViewController.headerTitleMaster = _selectedMenu.menuName;
            deviceSettingViewController.headerIconMaster = icon(_selectedMenu.menuId);
            
            [deviceSettingViewController createBarButtonItem:BACK_NOTIF];
            
            [self.navigationController pushViewController:deviceSettingViewController animated:YES];
            deviceSettingViewController = nil;
            [deviceSettingViewController release];
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
        
        if (type == SIGN_OUT_REQ) {
            // TODO : Hygiene
            resetAllData();
            
            AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            appDelegate.loggedIn = NO;
            
            // TODO : V1.9
            // set update device token to be YES again.
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.shouldUpdateDeviceToken = YES;
            
            // TODO : Hygiene
            // Reset this autologin variable on the login view instead
//            appDelegate.isAutoLogin = NO;
            [appDelegate showLoginView];
        }
        
        if (type == TRX_HISTORY_REQ) {
            
            TransactionHistoryViewController *transactionHistoryViewController = [[TransactionHistoryViewController alloc] initWithNibName:@"TransactionHistoryViewController" bundle:nil];
            
            transactionHistoryViewController.content = sharedData.lastTransactionData;
            transactionHistoryViewController.menuModel = _selectedMenu;
            
            transactionHistoryViewController.headerTitleMaster = _selectedMenu.menuName;
            transactionHistoryViewController.headerIconMaster = icon(_selectedMenu.menuId);
            
            [transactionHistoryViewController createBarButtonItem:BACK_NOTIF];
            
            [self.navigationController pushViewController:transactionHistoryViewController animated:YES];
            transactionHistoryViewController = nil;
            [transactionHistoryViewController release];
        }
        
        if (type == PROMO_REQ) {
            
            // Changes by iNot at 24 May 2013
            // Set Current Datetime to Preference
            NSDate *current = [NSDate date];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setObject:current
                      forKey:PROMO_CACHE_KEY];
            [prefs synchronize];
            //
            /*
            PromoViewController *promoViewController = [[PromoViewController alloc] initWithNibName:@"PromoViewController" bundle:nil];
            promoViewController.headerTitleMaster = _selectedMenu.menuName;
            promoViewController.headerIconMaster = icon(_selectedMenu.menuId);
            
            [self.navigationController pushViewController:promoViewController animated:YES];
            promoViewController = nil;
            [promoViewController release];*/
            
            /*
            ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
            
            contactUsViewController.content = sharedData.promoData;
            contactUsViewController.accordionType = PROMO_ACC;
            contactUsViewController.menuModel = _selectedMenu;*/
            
            InfoAndPromoViewController *infoAndPromoVC = [[InfoAndPromoViewController alloc] initWithNibName:@"InfoAndPromoViewController" bundle:nil];
            infoAndPromoVC.contentData = sharedData.promoData;
            infoAndPromoVC.menuModel = _selectedMenu;
            infoAndPromoVC.isParentPromo = YES;
            infoAndPromoVC.pageTitle = _selectedMenu.menuName;
            
            //[infoAndPromoVC createBarButtonItem:BACK_NOTIF];
            
            //contactUsViewController.headerTitleMaster = _selectedMenu.menuName;
            //contactUsViewController.headerIconMaster = icon(_selectedMenu.menuId);
            
            [self.navigationController pushViewController:infoAndPromoVC animated:YES];
            infoAndPromoVC = nil;
            [infoAndPromoVC release];
        }
        
        if (type == XL_STAR_REQ) {
            SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
            
//            DataManager *dataManager = [DataManager sharedInstance];
//            dataManager.parentId = _selectedMenu.menuId;
//            dataManager.menuModel = _selectedMenu;
//            
//            subMenuVC.parentId = _selectedMenu.menuId;
//            subMenuVC.groupDesc = _selectedMenu.desc;
            
            subMenuVC.parentMenuModel = _selectedMenu;
            
            subMenuVC.levelTwoType = XL_STAR_VIEW;
            
            subMenuVC.grayHeaderTitle = _selectedMenu.groupName;
            
            LabelValueModel *model = sharedData.xlStarData;
            subMenuVC.star = model.value;
            
            [self.navigationController pushViewController:subMenuVC animated:YES];
            subMenuVC = nil;
            [subMenuVC release];
        }
        
        if (type == PROFILE_INFO_REQ) {
            
            //sharedData.isPostpaid = YES; --> delete donk
            
            UpdateProfileViewController *updateProfileViewController = [[UpdateProfileViewController alloc] initWithNibName:@"UpdateProfileViewController" bundle:nil];
            [updateProfileViewController createBarButtonItem:BACK_NOTIF];
            updateProfileViewController.menuModel = _selectedMenu;
            
            [self.navigationController pushViewController:updateProfileViewController animated:YES];
            updateProfileViewController = nil;
            [updateProfileViewController release];
        }
        
        if (type == CHECK_USAGE_XL_REQ) {
            CheckQuotaViewController *packageViewController = [[CheckQuotaViewController alloc] initWithNibName:@"CheckQuotaViewController" bundle:nil];
            packageViewController.isLevel2 = NO;
            
            [self.navigationController pushViewController:packageViewController animated:YES];
            packageViewController = nil;
            [packageViewController release];
        }
        
        if (type == QUOTA_CALCULATOR_REQ) {
            QuotaCalculatorViewController *packageViewController = [[QuotaCalculatorViewController alloc] initWithNibName:@"QuotaCalculatorViewController" bundle:nil];
            
            [self.navigationController pushViewController:packageViewController animated:YES];
            packageViewController = nil;
            [packageViewController release];
        }
        
        // TODO : NEPTUNE
        if (type == PAGE_SUKASUKA_REQ)
        {
//            PaketSukaViewController *packageViewController = [[PaketSukaViewController alloc] initWithNibName:@"PaketSukaViewController" bundle:nil];
            PaketSukaMenuViewController *packageViewController = [[PaketSukaMenuViewController alloc] initWithNibName:@"PaketSukaMenuViewController" bundle:nil];
            
            [self.navigationController pushViewController:packageViewController animated:YES];
            packageViewController = nil;
            [packageViewController release];
        }
        
        // CR 4G USIM
        if (type == INFO_4G_USIM_REQ) {
            
            ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
            contactUsViewController.content = sharedData.info4gData;
            contactUsViewController.accordionType = INFO4G_ACC;
            contactUsViewController.menuModel = _selectedMenu;
            
            [self.navigationController pushViewController:contactUsViewController animated:YES];
            contactUsViewController = nil;
            [contactUsViewController release];
        }
    }
    else {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == BUY_PACKAGE_REQ) {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            
            thankYouViewController.info = [Language get:@"buy_package_thank_you" alter:nil];
            //thankYouViewController.headerTitleMaster = _selectedMenu.groupName;
            //thankYouViewController.headerIconMaster = icon(_selectedMenu.parentId);
            thankYouViewController.menuModel = _selectedMenu;
            thankYouViewController.displayFBButton = NO;
            thankYouViewController.trxId = self.trxId;
            thankYouViewController.hideInboxBtn = NO;
            
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            thankYouViewController = nil;
            [thankYouViewController release];
        }
        else {
            NSString *reason = [result valueForKey:REASON_KEY];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                            message:reason
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.sortedMenu = nil;;
    self.theSourceMenu = nil;
    self.parentId = nil;
    self.groupDesc = nil;
    self.recommendedPackageModel = nil;
    self.upgradePackageModel = nil;
    self.currentPackage = nil;
}

- (void)dealloc {
    [_sortedMenu release];
    _sortedMenu = nil;
    
    [_theSourceMenu release];
    _theSourceMenu = nil;
    
    [_parentId release];
    _parentId = nil;
    
    [_groupDesc release];
    _groupDesc = nil;
    
    [_recommendedPackageModel release];
    _recommendedPackageModel = nil;
    
    [_upgradePackageModel release];
    _upgradePackageModel = nil;
    
    [_currentPackage release];
    _currentPackage = nil;
    
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sortedMenu count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    else {
        if ([_groupDesc length] > 0) {
            return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section-1]] count] + 1;
        }
        else {
            if (_levelTwoType == XL_STAR_VIEW) {
                return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section-1]] count] + 1;
            }
            else {
                return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section-1]] count];
            }
            
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    static NSString *cellIdentifierBuy = @"CellBuy";
    static NSString *cellIdentifierInfo = @"CellInfo";
    
    if ([_groupDesc length] > 0) {
        NSLog(@"^^^^^^^^^^^^^^^^^^ Group Desc");
        if (indexPath.row == 0) {
            GeneralInfoCell *cell = (GeneralInfoCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierInfo];
            
            if (cell == nil) {
                cell = [[GeneralInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifierInfo];
                
                cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                
                cell.userInteractionEnabled = NO;
            }
            
            cell.infoLabel.text = _groupDesc;
            
            return cell;
        }
        else {
            NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section-1];
            NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
            MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
            
            if ([menuModel.price length] > 0) {
                BuyPackageCell *cell = (BuyPackageCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierBuy];
                
                if (cell == nil) {
                    cell = [[BuyPackageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifierBuy];
                    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    
                    [cell.infoButton addTarget:self
                                        action:@selector(performInfo:)
                              forControlEvents:UIControlEventTouchUpInside];
                    
                    [cell.activateButton addTarget:self
                                            action:@selector(performActivate:)
                                  forControlEvents:UIControlEventTouchUpInside];
                    
                    cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                  withBasicColor:kDefaultWhiteColor
                                                               withSelectedColor:kDefaultGrayColor] autorelease];
                    cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                          withBasicColor:kDefaultWhiteColor
                                                                       withSelectedColor:kDefaultGrayColor] autorelease];
                    ((RectBackground *)cell.selectedBackgroundView).selected = YES;
                }
                
//                cell.backgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
//                cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
//                ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
                
                /*
                 if (_recommendedPackageModel) {
                 cell.iconLabel.text = icon(@"default_icon");
                 }
                 else {
                 cell.iconLabel.text = icon(menuModel.menuId);
                 if ([cell.iconLabel.text length] <= 0) {
                 cell.iconLabel.text = icon(@"default_icon");
                 }
                 }*/
                
                cell.titleLabel.text = [menuModel.menuName uppercaseString];
                CGSize size = calculateExpectedSize(cell.titleLabel, cell.titleLabel.text);
                CGRect newFrame = cell.titleLabel.frame;
                newFrame.size.height = size.height;
                cell.titleLabel.frame = newFrame;
                cell.needResize = YES;
                
                DataManager *sharedData = [DataManager sharedInstance];
                cell.priceLabel.text = addThousandsSeparator(menuModel.price, sharedData.profileData.language);
                //NSLog(@"--> cell.priceLabel.text = %@",cell.priceLabel.text);
                
                if ([menuModel.desc length] > 0) {
                    cell.infoButton.hidden = NO;
                }
                else {
                    cell.infoButton.hidden = YES;
                }
                
                // Accordion
                if (menuModel.open) {
                    cell.descLabel.hidden = NO;
                    
                    CGRect newFrame = cell.descLabel.frame;
                    newFrame.size.height = _extraHeight;
                    cell.descLabel.frame = newFrame;
                }
                else {
                    cell.descLabel.hidden = YES;
                }
                cell.descLabel.text = menuModel.desc;
                
                return cell;
            }
            else {
                //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                if (cell == nil) {
                    cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    
                    [cell.infoButton addTarget:self
                                        action:@selector(performInfoForCommonCell:)
                              forControlEvents:UIControlEventTouchUpInside];
                    
                    cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                  withBasicColor:kDefaultWhiteColor
                                                               withSelectedColor:kDefaultGrayColor] autorelease];
                    cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                          withBasicColor:kDefaultWhiteColor
                                                                       withSelectedColor:kDefaultGrayColor] autorelease];
                    ((RectBackground *)cell.selectedBackgroundView).selected = YES;
                }
                
//                cell.backgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
//                cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
//                ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
                /*
                if (_recommendedPackageModel) {
                    cell.iconLabel.text = icon(@"default_icon");
                }
                else {
                    cell.iconLabel.text = icon(menuModel.menuId);
                    if ([cell.iconLabel.text length] <= 0) {
                        cell.iconLabel.text = icon(@"default_icon");
                    }
                }*/
                
                /**
                 * Without Icon
                 */
                cell.iconLabel.text = @"";
                
                cell.titleLabel.text = [menuModel.menuName uppercaseString];
                if ([menuModel.desc length] > 0) {
                    cell.arrowLabel.hidden = YES;
                    cell.infoButton.hidden = NO;
                }
                else {
                    cell.arrowLabel.hidden = NO;
                    cell.infoButton.hidden = YES;
                }
                
                // Accordion
                if (menuModel.open) {
                    cell.descLabel.hidden = NO;
                    
                    CGRect newFrame = cell.descLabel.frame;
                    newFrame.size.height = _extraHeight;
                    cell.descLabel.frame = newFrame;
                }
                else {
                    cell.descLabel.hidden = YES;
                }
                cell.descLabel.text = menuModel.desc;
                
                cell.refreshButton.hidden = YES;
                
                return cell;
            }
        }
    }
    else {
        NSLog(@"################### No Group Desc");
        NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section-1];
        NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
        MenuModel *menuModel;
        if (_levelTwoType == XL_STAR_VIEW) {
            if (indexPath.row == 0) {
                menuModel = nil;
            }
            else {
                menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
            }
        }
        else {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row];
        }
        
        if ([menuModel.price length] > 0) {
            BuyPackageCell *cell = (BuyPackageCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierBuy];
            
            if (cell == nil) {
                cell = [[BuyPackageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifierBuy];
                cell.backgroundColor = kDefaultWhiteColor;
                
                cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                              withBasicColor:kDefaultWhiteColor
                                                           withSelectedColor:kDefaultWhiteColor] autorelease];
                
                cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                      withBasicColor:kDefaultWhiteColor
                                                                   withSelectedColor:kDefaultWhiteColor] autorelease];
                ((RectBackground *)cell.selectedBackgroundView).selected = YES;
                
                [cell.infoButton addTarget:self
                                    action:@selector(performInfo:)
                          forControlEvents:UIControlEventTouchUpInside];
                
                [cell.activateButton addTarget:self
                                        action:@selector(performActivate:)
                              forControlEvents:UIControlEventTouchUpInside];
            }
            
            // CR Reskinning
            DataManager *sharedData = [DataManager sharedInstance];
            [cell.activateButton setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.packageButtonBg)) forState:UIControlStateNormal];
            [cell.activateButton setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuColor))
                                          forState:UIControlStateHighlighted];
            UILabel *btnLabel = (UILabel*)[cell.activateButton viewWithTag:99];
            btnLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.packageButtonTxt);
            //
            
            cell.titleLabel.text = [menuModel.menuName uppercaseString];
            CGSize size = calculateExpectedSize(cell.titleLabel, cell.titleLabel.text);
            CGRect newFrame = cell.titleLabel.frame;
            newFrame.size.height = size.height;
            cell.titleLabel.frame = newFrame;
            
            
            cell.priceLabel.text = addThousandsSeparator(menuModel.price, sharedData.profileData.language);
            
            //---------------------------------------------------//
            // cell.descLabel is not required on Extend Validity //
            //---------------------------------------------------//
            if ([menuModel.href isEqualToString:@"#extend_validity"]) {
                cell.descLabel.hidden = YES;
            }
            else {
                cell.descLabel.hidden = NO;
                NSLog(@"desc label frame %@", NSStringFromCGRect(cell.descLabel.frame));
                // TODO : V1.9
                // Change the description with short_desc
                if([menuModel.shortDesc length] > 0)
                    cell.descLabel.text = [NSString stringWithFormat:@"%@", menuModel.shortDesc];
                else if([menuModel.desc length] > 0)
                    cell.descLabel.text = [NSString stringWithFormat:@"%@", menuModel.desc];
                else if([menuModel.volume length] > 0 && [menuModel.duration length]> 0)
                    cell.descLabel.text = [NSString stringWithFormat:@"%@, %@",menuModel.volume, menuModel.duration];
//                cell.descLabel.text = [NSString stringWithFormat:@"%@, %@",menuModel.volume, menuModel.duration];
            }
            
            cell.infoButton.hidden = YES;
            
            cell.lineView.backgroundColor = kDefaultSeparatorColor;
            if (indexPath.row == objectsForMenu.count -1) {
                cell.lineView.hidden = YES;
            }
            else {
                cell.lineView.hidden = NO;
            }
            
            return cell;
        }
        else {
            
            // XL Star index 0
            if ((_levelTwoType == XL_STAR_VIEW) && (indexPath.row == 0)) {
                
                XLStarCell *cell = (XLStarCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                if (cell == nil) {
                    cell = [[XLStarCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    cell.backgroundColor = kDefaultWhiteColor;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    
                    cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                  withBasicColor:kDefaultWhiteColor
                                                               withSelectedColor:kDefaultWhiteColor] autorelease];
                    
                    cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                          withBasicColor:kDefaultWhiteColor
                                                                       withSelectedColor:kDefaultWhiteColor] autorelease];
                    ((RectBackground *)cell.selectedBackgroundView).selected = YES;
                }
                
                DataManager *sharedData = [DataManager sharedInstance];
                cell.descLabel.text = sharedData.xlStarData.label;
                
                return cell;
            }
            else {
                
                CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                if (cell == nil) {
                    cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    cell.backgroundColor = kDefaultWhiteColor;
                    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    
                    
                    [cell.infoButton addTarget:self
                                        action:@selector(performInfoForCommonCell:)
                              forControlEvents:UIControlEventTouchUpInside];
                    
                    cell.backgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                  withBasicColor:kDefaultWhiteColor
                                                               withSelectedColor:kDefaultGrayColor] autorelease];
                    cell.selectedBackgroundView = [[[RectBackground alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)
                                                                          withBasicColor:kDefaultWhiteColor
                                                                       withSelectedColor:kDefaultGrayColor] autorelease];
                    ((RectBackground *)cell.selectedBackgroundView).selected = YES;
                }
                
                
                
                
                //            cell.backgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
                //            cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
                //            ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
                
                /*
                 if (_recommendedPackageModel) {
                 cell.iconLabel.text = icon(@"default_icon");
                 }
                 else {
                 cell.iconLabel.text = icon(menuModel.menuId);
                 if ([cell.iconLabel.text length] <= 0) {
                 cell.iconLabel.text = icon(@"default_icon");
                 }
                 }*/
                
                /**
                 * Without Icon
                 */
                //cell.iconLabel.text = @"";
                
                cell.titleLabel.text = [menuModel.menuName uppercaseString];
                cell.titleLabel.textColor = kDefaultBlackColor;
//                CGSize sizeLabelTitle = calculateExpectedSize(cell.titleLabel, cell.titleLabel.text);
//                CGRect titleFrame = cell.titleLabel.frame;
//                titleFrame.size.height = sizeLabelTitle.height;
//                cell.titleLabel.frame = titleFrame;
                
                
                cell.arrowLabel.textColor = kDefaultSeparatorColor;
                cell.lineView.backgroundColor = kDefaultSeparatorColor;
                
                //cell.arrowLabel.hidden = NO;
                //cell.infoButton.hidden = YES;
                
                /*
                 * Show Info Button or not
                 */
                if ([menuModel.desc length] > 0) {
                    if ([menuModel.href isEqualToString:@"#xlstar_detail"]) {
                        cell.infoButton.hidden = YES;
                    }
                    else {
                        cell.infoButton.hidden = NO;
                    }
                }
                else {
                    cell.infoButton.hidden = YES;
                }
                
                /*
                 * Accordion
                 */
                if (menuModel.open) {
                    cell.descLabel.hidden = NO;
                    
                    cell.descLabel.text = menuModel.desc;
                    //UILabel *labelDesc = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260.0, 30.0)];
                    //labelDesc.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
                    CGSize sizeLabelDesc = calculateExpectedSize(cell.descLabel, cell.descLabel.text);
                    //NSLog(@"++++++-------- sizeLabelDesc = %f",sizeLabelDesc.height);
                    
                    CGRect descFrame = cell.descLabel.frame;
                    descFrame.size.height = sizeLabelDesc.height;
                    cell.descLabel.frame = descFrame;
                    
                    //NSLog(@"+++++++++ DESC SHOW");
                    //NSLog(@"+++++++++ tinggi desc label = %f",cell.descLabel.frame.size.height);
                }
                else {
                    cell.descLabel.hidden = YES;
                    //NSLog(@"--------- DESC HIDE");
                    //NSLog(@"--------- tinggi desc label = %f",cell.descLabel.frame.size.height);
                }
                
                
                /*
                 if ([menuModel.desc length] > 0) {
                 
                 //                cell.descLabel.hidden = NO;
                 //                CGRect newFrame = cell.descLabel.frame;
                 //                newFrame.size.height = _extraHeight;
                 //                cell.descLabel.frame = newFrame;
                 //                cell.descLabel.text = menuModel.desc;
                 }
                 else {
                 //cell.descLabel.hidden = YES;
                 }*/
                
                if (_levelTwoType == XL_STAR_VIEW) {
                    if (indexPath.row == objectsForMenu.count) {
                        cell.lineView.hidden = YES;
                    }
                    else {
                        cell.lineView.hidden = NO;
                    }
                }
                else {
                    if (indexPath.row == objectsForMenu.count -1) {
                        cell.lineView.hidden = YES;
                    }
                    else {
                        cell.lineView.hidden = NO;
                    }
                }
                
                cell.refreshButton.hidden = YES;
                
                return cell;
            }
            
        }
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        
        return _grayHeaderTitle;
        
        /*
        //DataManager *sharedData = [DataManager sharedInstance];
        if (_isHotOffer) {
            //return [Language get:@"package" alter:nil];
            return _grayHeaderTitle;
        }
        else if ([_parentMenuModel.href isEqualToString:@"#package"]) {
            //return [Language get:@"package" alter:nil];
            return _grayHeaderTitle;
        }
        else {
            //return _parentMenuModel.menuName;
            //return _parentMenuModel.groupName;
            
            return _grayHeaderTitle;
        }*/
    }
    else {
        if (_levelTwoType == XL_STAR_VIEW) {
            return @"XL Star";
        }
        else {
            NSString *groupIdOfMenu = [self.sortedMenu objectAtIndex:section-1];
            NSArray *objectsForMenu = [self.theSourceMenu objectForKey:groupIdOfMenu];
            MenuModel *menuModel = [objectsForMenu objectAtIndex:0];
            NSLog(@"--> %@",menuModel.groupName);
            return menuModel.groupName;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 0;
    }
    else {
        if ([_groupDesc length] > 0) {
            if (indexPath.row == 0) {
                CGFloat padding = 10.0;
                CGFloat valueLabelHeight = 20.0;
                CGFloat additionalPadding = 5.0;
                
                NSString *value = _groupDesc;
                CGSize valueSize = CGSizeZero;
                
                CGFloat tableWidth = 0.0;
                CGFloat leftPadding = 20.0;
                if (IS_IPAD) {
                    tableWidth = self.view.frame.size.width - (45*2);
                    valueSize = [value sizeWithFont:[UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize]
                                  constrainedToSize:CGSizeMake(tableWidth - (leftPadding*2), 1000.0f)];
                }
                else {
                    tableWidth = self.view.frame.size.width - (10*2);
                    valueSize = [value sizeWithFont:[UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize]
                                  constrainedToSize:CGSizeMake(tableWidth - (leftPadding*2), 1000.0f)];
                }
                
                if (valueSize.height < valueLabelHeight) {
                    return padding + valueLabelHeight + additionalPadding;
                }
                else {
                    return padding + valueSize.height + additionalPadding;
                }
            }
            else {
                NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section-1];
                NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
                MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
                
                if ([menuModel.price length] > 0) {
                    //return 50;
                    
                    //return kDefaultButtonHeight + 20 + (8*2);
                    return kDefaultButtonHeight + 20;
                    
                    /*
                     CGFloat labelHeightDefault = 20.0;
                     CGFloat cellHeightDefault = 52.0;
                     
                     NSString *text = [menuModel.menuName uppercaseString];
                     
                     UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160.0, 20.0)];
                     label.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
                     CGSize size = calculateExpectedSize(label, text);
                     
                     CGFloat extraHeightForTitle = 0.0;
                     if (size.height > labelHeightDefault) {
                     extraHeightForTitle = size.height - labelHeightDefault;
                     }
                     
                     if (menuModel.open) {
                     return cellHeightDefault + _extraHeight + extraHeightForTitle;
                     }
                     else {
                     return cellHeightDefault + extraHeightForTitle;
                     }*/
                }
                else {
                    //return kDefaultCellHeight;
                    //NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
                    //NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
                    
                    //MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
                    
                    CGFloat labelHeightDefault = 28.0;
                    
                    NSString *text = [menuModel.menuName uppercaseString];
                    
                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 240.0, 28.0)];
                    CGSize size = calculateExpectedSize(label, text);
                    
                    CGFloat extraHeightForTitle = 0.0;
                    if (size.height > labelHeightDefault) {
                        extraHeightForTitle = size.height - labelHeightDefault;
                    }
                    
                    if (menuModel.open) {
                        return kDefaultCellHeight + _extraHeight + extraHeightForTitle;
                    }
                    else {
                        return kDefaultCellHeight + extraHeightForTitle;
                    }
                }
            }
        }
        else {
            NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section-1];
            NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
            
            MenuModel *menuModel;
            if (_levelTwoType == XL_STAR_VIEW) {
                if (indexPath.row == 0) {
                    menuModel = nil;
                }
                else {
                    menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
                }
            }
            else {
                menuModel = [objectsForMenu objectAtIndex:indexPath.row];
            }
            
            if ((_levelTwoType == XL_STAR_VIEW) && (indexPath.row == 0)) {
                return 170;
            }
            else if ([menuModel.price length] > 0) {
                NSLog(@"---> Desc Length = %lu",(unsigned long)[menuModel.desc length]);
                NSLog(@"---> Menu Name = %@",menuModel.menuName);
                NSLog(@"######################");
                if ([menuModel.href isEqualToString:@"#extend_validity"]) {
                    return 50.0;
                }
                else {
                    // TODO : V1.9
                    // Adjust the cell height for long desc texts
                    _extraHeight = 10;
                    if ([menuModel.menuName length] > 29)
                    {
                        if ([menuModel.desc length] > 55) {
                            _extraHeight = 30;
                            return 75.0 + _extraHeight; // desc buy package list 3 row
                        }
                        else {
                            return 75.0 + _extraHeight; // buy package list 2 row
                        }
                        
                    }
                    else {
                        if ([menuModel.desc length] > 55) {
                            _extraHeight = 30;
                            return 70.0 + _extraHeight; // desc buy package list 3 row
                        }
                        else {
                            return 70.0 + _extraHeight; // buy package list 1 row
                        }
                        
                    }
                    
                }
            }
            else {
                
                CGFloat labelHeightDefault = 30.0;
                
                NSString *text = [menuModel.menuName uppercaseString];
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260.0, labelHeightDefault)];
                label.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
                CGSize size = calculateExpectedSize(label, text);
                
                CGFloat extraHeightForTitle = 0.0;
                if (size.height > labelHeightDefault) {
                    extraHeightForTitle = size.height - labelHeightDefault;
                }
                
                /*
                if ([menuModel.desc length] > 0) {
                    return kDefaultCellHeight + _extraHeight + extraHeightForTitle;
                }
                else {
                    return kDefaultCellHeight + extraHeightForTitle;
                }*/
                
                if (menuModel.open) {
                    CGFloat descLabelWidth = 0.0;
                    if (IS_IPAD) {
                        descLabelWidth = 949.0;
                    }
                    else {
                        descLabelWidth = 160.0;
                    }
                    UILabel *labelDesc = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, descLabelWidth, labelHeightDefault)];
                    labelDesc.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
                    labelDesc.numberOfLines = 0;
                    labelDesc.lineBreakMode = UILineBreakModeWordWrap;
                    labelDesc.text = menuModel.desc;
                    CGSize sizeLabelDesc = calculateExpectedSize(labelDesc, labelDesc.text);
                    //NSLog(@"--> ******* EXPECTED HEIGHT = %f",sizeLabelDesc.height);
                    //NSLog(@"--> ******* Desc = %@",menuModel.desc);
                    
                    _extraHeight = 0.0;
                    if (sizeLabelDesc.height > labelHeightDefault) {
                        _extraHeight = sizeLabelDesc.height - labelHeightDefault;
                    }
                    else {
                        _extraHeight = labelHeightDefault;
                    }
                    
                    //NSLog(@"--> ******* _extraHeight di ROW CELL = %f",_extraHeight);
                    //NSLog(@"extraHeightForTitle = %f",extraHeightForTitle);
                    //NSLog(@"kDefaultCellHeight = %f",kDefaultCellHeight);
                    //NSLog(@"total height = %f",kDefaultCellHeight + _extraHeight + extraHeightForTitle);
                    
                    //NSLog(@"--> ******* ROW OPEN height = %f",kDefaultCellHeight + _extraHeight + extraHeightForTitle);
                    
                    // TODO : UPDATE HYGIENE
                    // Update extraheight if it's only one/two lines of desc
                    if(_extraHeight <= 10)
                        _extraHeight = 25;
                    
                    return kDefaultCellHeight + _extraHeight + extraHeightForTitle + 6.0;
                }
                else {
                    //NSLog(@"-->******* ROW -CLOSE- height = %f",kDefaultCellHeight + extraHeightForTitle);
                    return kDefaultCellHeight + extraHeightForTitle;
                }
            }
        }
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section != 0) {
        if ([_groupDesc length] > 0) {
            //NSLog(@"Ada Group Desc");
            if (indexPath.row != 0) {
                //[tableView deselectRowAtIndexPath:indexPath animated:YES];
                //NSLog(@"SELECTED");
                NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section-1];
                NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
                MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
                //NSLog(@"menuModel.href = %@",menuModel.href);
                
                _selectedMenu = menuModel;
                
                if ([menuModel.href isEqualToString:@"#extend_validity"]) {
                    /*
                    DataManager *sharedData = [DataManager sharedInstance];
                    NSString *price = [NSString stringWithFormat:@"%@",addThousandsSeparator(menuModel.price, sharedData.profileData.language)];
                    NSString *confirmationMessage = @"";
                    if ([sharedData.profileData.language isEqualToString:@"id"]) {
                        confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@. \n %@",
                                               [Language get:@"extend_confirmation_1" alter:nil],
                                               price,
                                               [Language get:@"extend_confirmation_2" alter:nil],
                                               menuModel.duration,
                                               [Language get:@"ask_continue" alter:nil]];
                    }
                    else {
                        confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ \n %@",
                                               price,
                                               [Language get:@"extend_confirmation_1" alter:nil],
                                               menuModel.duration,
                                               [Language get:@"extend_confirmation_2" alter:nil],
                                               [Language get:@"ask_continue" alter:nil]];
                    }*/
                    
                    NSString *confirmationMessage = _selectedMenu.confirmDesc;
                    
                    UIAlertView *extendValidityAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                                  message:confirmationMessage
                                                                                 delegate:self
                                                                        cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                                        otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
                    extendValidityAlert.tag = 1;
                    
                    [extendValidityAlert show];
                    [extendValidityAlert release];
                }
                
                else if ([menuModel.href isEqualToString:@"#buy_package"] || [menuModel.href isEqualToString:@"buy_package"]) {
                    //NSLog(@"Buy Package");
                    /*
                    DataManager *sharedData = [DataManager sharedInstance];
                    NSString *price = [NSString stringWithFormat:@"%@",addThousandsSeparator(menuModel.price, sharedData.profileData.language)];
                    NSString *confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@. \n %@",
                                                     [Language get:@"buy_package_confirmation_1" alter:nil],
                                                     price,
                                                     [Language get:@"buy_package_confirmation_2" alter:nil],
                                                     menuModel.groupName,
                                                     menuModel.menuName,
                                                     [Language get:@"buy_package_confirmation_3" alter:nil],
                                                     menuModel.duration,
                                                     [Language get:@"ask_continue" alter:nil]];*/
                    
                    NSString *confirmationMessage = _selectedMenu.confirmDesc;
                    
                    if([_selectedMenu.paymentMethod isEqualToString:@"cc"])
                    {
                        // TODO : V1.9
                        // Add pay with credit card on buy package alert
                        UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                                  message:confirmationMessage
                                                                                 delegate:self
                                                                        cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                                        otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],@"Pay With Credit Card",nil];
                        buyPackageAlert.tag = 2;
                        
                        [buyPackageAlert show];
                        [buyPackageAlert release];
                    }
                    else
                    {
                        UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                                  message:confirmationMessage
                                                                                 delegate:self
                                                                        cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                                        otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
                        
                        buyPackageAlert.tag = 2;
                        
                        [buyPackageAlert show];
                        [buyPackageAlert release];
                    }
                    
//                    UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
//                                                                              message:confirmationMessage
//                                                                             delegate:self
//                                                                    cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
//                                                                    otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
//                    
//                    buyPackageAlert.tag = 2;
//                    
//                    [buyPackageAlert show];
//                    [buyPackageAlert release];
                }
                
                else if ([menuModel.href isEqualToString:@"#up_package"]) {
                    DataManager *sharedData = [DataManager sharedInstance];
                    NSString *price = [NSString stringWithFormat:@"%@",addThousandsSeparator(menuModel.price, sharedData.profileData.language)];
                    
                    NSString *confirmationMessage = @"";
                    if ([sharedData.profileData.language isEqualToString:@"id"]) {
                        confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@. \n %@",
                                               [Language get:@"buy_package_confirmation_1" alter:nil],
                                               price,
                                               [Language get:@"upgrade_package_confirmation_2" alter:nil],
                                               menuModel.menuName,
                                               [Language get:@"buy_package_confirmation_3" alter:nil],
                                               menuModel.duration,
                                               [Language get:@"upgrade_package_confirmation_4" alter:nil],
                                               _currentPackage,
                                               [Language get:@"ask_continue" alter:nil]];
                    }
                    else {
                        confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@. \n %@",
                                               [Language get:@"buy_package_confirmation_1" alter:nil],
                                               price,
                                               [Language get:@"upgrade_package_confirmation_2" alter:nil],
                                               menuModel.menuName,
                                               [Language get:@"buy_package_confirmation_3" alter:nil],
                                               menuModel.duration,
                                               [Language get:@"upgrade_package_confirmation_4" alter:nil],
                                               _currentPackage,
                                               [[Language get:@"package" alter:nil] lowercaseString],
                                               [Language get:@"ask_continue" alter:nil]];
                    }
                    
                    UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                              message:confirmationMessage
                                                                             delegate:self
                                                                    cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                                    otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
                    
                    buyPackageAlert.tag = 2;
                    
                    [buyPackageAlert show];
                    [buyPackageAlert release];
                }
                
                else if ([menuModel.href isEqualToString:@"#gift_package"]) {
                    GiftPackageViewController *giftPackageViewController = [[GiftPackageViewController alloc] initWithNibName:@"GiftPackageViewController" bundle:nil];
                    
                    giftPackageViewController.isThankYouPage = NO;
                    giftPackageViewController.giftPackageModel = menuModel;
                    giftPackageViewController.headerTitleMaster = _selectedMenu.groupName;
                    giftPackageViewController.headerIconMaster = icon(_selectedMenu.parentId);
                    
                    [self.navigationController pushViewController:giftPackageViewController animated:YES];
                    giftPackageViewController = nil;
                    [giftPackageViewController release];
                }
                
                else if ([menuModel.href isEqualToString:@"#contactus"]) {
                    [self performContactUs];
                }
                
                else if ([menuModel.href isEqualToString:@"#puk"]) {
                    [self performPUK:menuModel];
                }
                
                else if ([menuModel.href isEqualToString:@"#sms"]) {
                    [self performDeviceSetting:menuModel.href];
                }
                
                else if ([menuModel.href isEqualToString:@"#manual"]) {
                    [self performDeviceSetting:menuModel.href];
                }
                
                else if ([menuModel.href isEqualToString:@"#store_locator"]) {
                    ShopLocatorViewController *shopLocatorViewController = [[ShopLocatorViewController alloc] initWithNibName:@"ShopLocatorViewController" bundle:nil];
                    
                    shopLocatorViewController.headerTitleMaster = _selectedMenu.menuName;
                    shopLocatorViewController.headerIconMaster = icon(_selectedMenu.menuId);
                    
                    /*
                     giftPackageViewController.isThankYouPage = NO;
                     giftPackageViewController.giftPackageModel = menuModel;
                     giftPackageViewController.headerTitleMaster = _selectedMenu.groupName;
                     giftPackageViewController.headerIconMaster = icon(_selectedMenu.parentId);*/
                    
                    [self.navigationController pushViewController:shopLocatorViewController animated:YES];
                    shopLocatorViewController = nil;
                    [shopLocatorViewController release];
                }
                
                else if ([menuModel.href isEqualToString:@"#faqs"]) {
                    /*
                    FAQViewController *faqViewController = [[FAQViewController alloc] initWithNibName:@"FAQViewController" bundle:nil];
                    faqViewController.parentId = menuModel.menuId;
                    faqViewController.groupDesc = menuModel.desc;
                    
                    faqViewController.headerTitleMaster = _selectedMenu.menuName;
                    faqViewController.headerIconMaster = icon(_selectedMenu.menuId);
                    
                    [self.navigationController pushViewController:faqViewController animated:YES];
                    faqViewController = nil;
                    [faqViewController release];*/
                    
                    
                    
                }
                
                else if ([menuModel.href isEqualToString:@"#xltunai_reload"]) {
                    
                }
                else {
                    SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
                    subMenuVC.parentId = menuModel.menuId;
                    subMenuVC.groupDesc = menuModel.desc;
                    [self.navigationController pushViewController:subMenuVC animated:YES];
                    subMenuVC = nil;
                    [subMenuVC release];
                }
            }
            else {
            }
        }
        else {
            //NSLog(@"--> No Group Desc");
            
            NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section-1];
            NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
            
            MenuModel *menuModel;
            if (_levelTwoType == XL_STAR_VIEW) {
                menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
            }
            else {
                menuModel = [objectsForMenu objectAtIndex:indexPath.row];
            }
            
            _selectedMenu = menuModel;
            
            if ([menuModel.href isEqualToString:@"#extend_validity"]) {
                DataManager *sharedData = [DataManager sharedInstance];
                NSString *price = [NSString stringWithFormat:@"%@",addThousandsSeparator(menuModel.price, sharedData.profileData.language)];
                NSString *confirmationMessage = @"";
                if ([sharedData.profileData.language isEqualToString:@"id"]) {
                    confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@. \n %@",
                                           [Language get:@"extend_confirmation_1" alter:nil],
                                           price,
                                           [Language get:@"extend_confirmation_2" alter:nil],
                                           menuModel.duration,
                                           [Language get:@"ask_continue" alter:nil]];
                }
                else {
                    confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ \n %@",
                                           price,
                                           [Language get:@"extend_confirmation_1" alter:nil],
                                           menuModel.duration,
                                           [Language get:@"extend_confirmation_2" alter:nil],
                                           [Language get:@"ask_continue" alter:nil]];
                }
                
                UIAlertView *extendValidityAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                              message:confirmationMessage
                                                                             delegate:self
                                                                    cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                                    otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
                extendValidityAlert.tag = 1;
                
                [extendValidityAlert show];
                [extendValidityAlert release];
            }
            
            else if ([menuModel.href isEqualToString:@"#buy_package"]) {
                //NSLog(@"Buy Package");
                /*
                DataManager *sharedData = [DataManager sharedInstance];
                NSString *price = [NSString stringWithFormat:@"%@",addThousandsSeparator(menuModel.price, sharedData.profileData.language)];
                NSString *confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@. \n %@",
                                                 [Language get:@"buy_package_confirmation_1" alter:nil],
                                                 price,
                                                 [Language get:@"buy_package_confirmation_2" alter:nil],
                                                 menuModel.groupName,
                                                 menuModel.menuName,
                                                 [Language get:@"buy_package_confirmation_3" alter:nil],
                                                 menuModel.duration,
                                                 [Language get:@"ask_continue" alter:nil]];*/
                
                /*
                 * Google Analytic
                 */
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                // Screen tracking
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                                      action:@"Buy"         // Event action (required)
                                                                       label:_selectedMenu.packageId // Event label
                                                                       value:nil] build]];
                [[GAI sharedInstance] dispatch];
                /******************/
                
                NSString *confirmationMessage = _selectedMenu.confirmDesc;
                
                // DROP 2
                if([_selectedMenu.paymentMethod isEqualToString:@"cc"])
                {
                    // TODO : V1.9
                    // Add pay with credit card on buy package alert
                    UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                              message:confirmationMessage
                                                                             delegate:self
                                                                    cancelButtonTitle:[[Language get:@"pay_with_cc" alter:nil] uppercaseString]
                                                                    otherButtonTitles:[[Language get:@"yes" alter:nil] uppercaseString],[[Language get:@"no" alter:nil] uppercaseString],nil];
                    buyPackageAlert.tag = 4;
                    
                    [buyPackageAlert show];
                    [buyPackageAlert release];
                }
                else
                {
                    UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                              message:confirmationMessage
                                                                             delegate:self
                                                                    cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                                    otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
                    
                    buyPackageAlert.tag = 2;
                    
                    [buyPackageAlert show];
                    [buyPackageAlert release];
                }
            }
            
            else if ([menuModel.href isEqualToString:@"#up_package"]) {
                DataManager *sharedData = [DataManager sharedInstance];
                NSString *price = [NSString stringWithFormat:@"%@",addThousandsSeparator(menuModel.price, sharedData.profileData.language)];
                
                NSString *confirmationMessage = @"";
                if ([sharedData.profileData.language isEqualToString:@"id"]) {
                    confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@. \n %@",
                                           [Language get:@"buy_package_confirmation_1" alter:nil],
                                           price,
                                           [Language get:@"upgrade_package_confirmation_2" alter:nil],
                                           menuModel.menuName,
                                           [Language get:@"buy_package_confirmation_3" alter:nil],
                                           menuModel.duration,
                                           [Language get:@"upgrade_package_confirmation_4" alter:nil],
                                           _currentPackage,
                                           [Language get:@"ask_continue" alter:nil]];
                }
                else {
                    confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@. \n %@",
                                           [Language get:@"buy_package_confirmation_1" alter:nil],
                                           price,
                                           [Language get:@"upgrade_package_confirmation_2" alter:nil],
                                           menuModel.menuName,
                                           [Language get:@"buy_package_confirmation_3" alter:nil],
                                           menuModel.duration,
                                           [Language get:@"upgrade_package_confirmation_4" alter:nil],
                                           _currentPackage,
                                           [[Language get:@"package" alter:nil] lowercaseString],
                                           [Language get:@"ask_continue" alter:nil]];
                }
                
                UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                          message:confirmationMessage
                                                                         delegate:self
                                                                cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                                otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
                
                buyPackageAlert.tag = 2;
                
                [buyPackageAlert show];
                [buyPackageAlert release];
            }
            
            else if ([menuModel.href isEqualToString:@"#gift_package"]) {
                GiftPackageViewController *giftPackageViewController = [[GiftPackageViewController alloc] initWithNibName:@"GiftPackageViewController" bundle:nil];
                
                giftPackageViewController.isThankYouPage = NO;
                giftPackageViewController.giftPackageModel = menuModel;
                giftPackageViewController.headerTitleMaster = _selectedMenu.groupName;
                giftPackageViewController.headerIconMaster = icon(_selectedMenu.parentId);
                
                [self.navigationController pushViewController:giftPackageViewController animated:YES];
                giftPackageViewController = nil;
                [giftPackageViewController release];
            }
            
            else if ([menuModel.href isEqualToString:@"#account"]) {
                [self performUpdateProfile];
            }
            
            else if ([menuModel.href isEqualToString:@"#contactus"]) {
                [self performContactUs];
            }
            
            else if ([menuModel.href isEqualToString:@"#puk"]) {
                [self performPUK:menuModel];
            }
            
            else if ([menuModel.href isEqualToString:@"#sms"]) {
                [self performDeviceSetting:menuModel.href];
            }
            
            else if ([menuModel.href isEqualToString:@"#manual"]) {
                [self performDeviceSetting:menuModel.href];
            }
            
            else if ([menuModel.href isEqualToString:@"#store_locator"]) {
                ShopLocatorViewController *shopLocatorViewController = [[ShopLocatorViewController alloc] initWithNibName:@"ShopLocatorViewController" bundle:nil];
                shopLocatorViewController.menuModel = _selectedMenu;
                shopLocatorViewController.headerTitleMaster = _selectedMenu.menuName;
                shopLocatorViewController.headerIconMaster = icon(_selectedMenu.menuId);
                
                [shopLocatorViewController createBarButtonItem:BACK_NOTIF];
                
                /*
                 giftPackageViewController.isThankYouPage = NO;
                 giftPackageViewController.giftPackageModel = menuModel;
                 giftPackageViewController.headerTitleMaster = _selectedMenu.groupName;
                 giftPackageViewController.headerIconMaster = icon(_selectedMenu.parentId);*/
                
                [self.navigationController pushViewController:shopLocatorViewController animated:YES];
                shopLocatorViewController = nil;
                [shopLocatorViewController release];
            }
            
            else if ([menuModel.href isEqualToString:@"#faqs"]) {
                /*
                FAQViewController *faqViewController = [[FAQViewController alloc] initWithNibName:@"FAQViewController" bundle:nil];
                faqViewController.parentId = menuModel.menuId;
                faqViewController.groupDesc = menuModel.desc;
                
                faqViewController.headerTitleMaster = _selectedMenu.menuName;
                faqViewController.headerIconMaster = icon(_selectedMenu.menuId);
                
                [self.navigationController pushViewController:faqViewController animated:YES];
                faqViewController = nil;
                [faqViewController release];*/
                
                //
                // Set Data
                //
                DataManager *dataManager = [DataManager sharedInstance];
                NSArray *contentTmp = [dataManager.menuData valueForKey:menuModel.menuId];
                NSArray *sortedMenu = [GroupingAndSorting doSortingByMenuIdForHome:contentTmp];
                //---------
                
                ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
                contactUsViewController.content = sortedMenu;
                contactUsViewController.accordionType = FAQ_ACC;
                contactUsViewController.menuModel = _selectedMenu;
                
                [self.navigationController pushViewController:contactUsViewController animated:YES];
                contactUsViewController = nil;
                [contactUsViewController release];
                
            }
            
            else if ([menuModel.href isEqualToString:@"#topup_hvrn"]) {
                /*
                ReloadBalanceViewController *reloadBalanceVC = [[ReloadBalanceViewController alloc] initWithNibName:@"ReloadBalanceViewController" bundle:nil];
                reloadBalanceVC.menuModel = menuModel;
                reloadBalanceVC.headerTitle = menuModel.menuName;
                reloadBalanceVC.headerIcon = icon(menuModel.menuId);
                
                [reloadBalanceVC createBarButtonItem:BACK_NOTIF];
                
                [self.navigationController pushViewController:reloadBalanceVC animated:YES];
                reloadBalanceVC = nil;
                [reloadBalanceVC release];*/
                
                PaymentChoiceViewController *subMenuVC = [[PaymentChoiceViewController alloc] initWithNibName:@"PaymentChoiceViewController" bundle:nil];
                //reloadBalanceVC.menuModel = menuModel;
                //reloadBalanceVC.headerTitle = menuModel.menuName;
                //reloadBalanceVC.headerIcon = icon(menuModel.menuId);
                //[reloadBalanceVC createBarButtonItem:BACK_NOTIF];
                
                //-- Setting Payment Menu --//
                DataManager *dataManager = [DataManager sharedInstance];
                
                NSArray *contentTmp = nil;
                // TODO : NEED TO CHECK MENUID FOR PAYMENT MENU
//                contentTmp = [dataManager.menuData valueForKey:dataManager.paymentMethodMenu.menuId];
                contentTmp = [dataManager.menuData valueForKey:menuModel.menuId];
                
                // Grouping
                NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
                
                // Sorting
                NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
                
                NSArray *paymentMenu = [sortingResult allKeys];
                paymentMenu = [GroupingAndSorting doSortingByGroupId:paymentMenu];
                
                NSMutableArray *paymentMenuTmp = [[NSMutableArray alloc] init];
                for (int i=0; i<[paymentMenu count]; i++) {
                    NSString *groupIdOfMenu = [paymentMenu objectAtIndex:i];
                    NSArray *objectsForMenu = [sortingResult objectForKey:groupIdOfMenu];
                    for (MenuModel *menu in objectsForMenu) {
                        [paymentMenuTmp addObject:menu];
                    }
                }
                
                subMenuVC.paymentMenu = paymentMenuTmp;
                subMenuVC.grayHeaderTitle = menuModel.groupName;
                subMenuVC.blueHeaderTitle = menuModel.menuName;
                subMenuVC.isLevel2 = NO;
                
                //---------------------------//
                
                [self.navigationController pushViewController:subMenuVC animated:YES];
                subMenuVC = nil;
                [subMenuVC release];
            }
            
            else if ([menuModel.href isEqualToString:@"#xltunai_reload"]) {
                
                DataManager *sharedData = [DataManager sharedInstance];
                
                NSArray *contentTmp = [sharedData.menuData valueForKey:menuModel.menuId];
                
                if ([contentTmp count] > 0)
                {
                    SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
                    
                    //                DataManager *dataManager = [DataManager sharedInstance];
                    //                dataManager.parentId = menuModel.menuId;
                    //                dataManager.menuModel = menuModel;
                    //
                    //                subMenuVC.parentId = menuModel.menuId;
                    //                subMenuVC.groupDesc = menuModel.desc;
                    
                    subMenuVC.parentMenuModel = menuModel;
                    subMenuVC.grayHeaderTitle = menuModel.groupName;
                    
                    //subMenuVC.subMenuViewController = self; // matiin karna gak dipake
                    
                    //[self.viewDeckController setCenterController:subMenuVC];
                    
                    [self.navigationController pushViewController:subMenuVC animated:YES];
                    
                    subMenuVC = nil;
                    [subMenuVC release];
                }
                else {
                    if ([sharedData.xlTunaiBalanceData.code isEqualToString:@"01"]) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                                        message:[Language get:@"xl_tunai_not_eligible" alter:nil]
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                        [alert release];
                    }
                    else {
                        XLTunaiReloadViewController *reloadBalanceVC = [[XLTunaiReloadViewController alloc] initWithNibName:@"XLTunaiReloadViewController" bundle:nil];
                        reloadBalanceVC.menuModel = menuModel;
                        reloadBalanceVC.headerTitle = menuModel.menuName;
                        reloadBalanceVC.headerIcon = icon(menuModel.menuId);
                        
                        [reloadBalanceVC createBarButtonItem:BACK_NOTIF];
                        
                        [self.navigationController pushViewController:reloadBalanceVC animated:YES];
                        reloadBalanceVC = nil;
                        [reloadBalanceVC release];
                    }
                    /*
                     PaymentChoiceViewController *reloadBalanceVC = [[PaymentChoiceViewController alloc] initWithNibName:@"PaymentChoiceViewController" bundle:nil];
                     reloadBalanceVC.menuModel = menuModel;
                     //reloadBalanceVC.headerTitle = menuModel.menuName;
                     //reloadBalanceVC.headerIcon = icon(menuModel.menuId);
                     
                     [reloadBalanceVC createBarButtonItem:BACK_NOTIF];
                     
                     [self.navigationController pushViewController:reloadBalanceVC animated:YES];
                     reloadBalanceVC = nil;
                     [reloadBalanceVC release];*/
                }
                
                
            }
            
            else if ([menuModel.href isEqualToString:@"#xltunai_pln"]) {
                DataManager *sharedData = [DataManager sharedInstance];
                if ([sharedData.xlTunaiBalanceData.code isEqualToString:@"01"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                                    message:[Language get:@"xl_tunai_not_eligible" alter:nil]
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    [alert release];
                }
                else {
                    XLTunaiPLNViewController *reloadBalanceVC = [[XLTunaiPLNViewController alloc] initWithNibName:@"XLTunaiPLNViewController" bundle:nil];
                    reloadBalanceVC.menuModel = menuModel;
                    reloadBalanceVC.headerTitle = menuModel.menuName;
                    reloadBalanceVC.headerIcon = icon(menuModel.menuId);
                    
                    [reloadBalanceVC createBarButtonItem:BACK_NOTIF];
                    
                    [self.navigationController pushViewController:reloadBalanceVC animated:YES];
                    reloadBalanceVC = nil;
                    [reloadBalanceVC release];
                }
            }
            
            else if ([menuModel.href isEqualToString:@"#mkartu_kredit"] || [menuModel.href isEqualToString:@"#mkredit"]) {
                [self performMKartu];
            }
            
            else if ([menuModel.href isEqualToString:@"#auto_reload"]) {
                [self performAutoReload];
            }
            
            else if ([menuModel.href isEqualToString:@"#last5_usage"]) {
                [self requestTransactionHistory:@"usage"];
            }
            
            else if ([menuModel.href isEqualToString:@"#last5_reload"]) {
                [self requestTransactionHistory:@"reload"];
            }
            
            else if ([menuModel.href isEqualToString:@"#last5_package"]) {
                [self requestTransactionHistory:@"package"];
            }
            
            else if ([menuModel.href isEqualToString:@"#promo"]) {
                [self performPromo];
            }
            
            // 4gUSIM
            else if ([menuModel.href isEqualToString:@"#fourgusim"]) {
                [self perform4gusim];
            }
            
            else if ([menuModel.href isEqualToString:@"#axisshop"]) {
                ShopLocatorViewController *shopLocatorViewController = [[ShopLocatorViewController alloc] initWithNibName:@"ShopLocatorViewController" bundle:nil];
                
                shopLocatorViewController.menuModel = _selectedMenu;
                
                shopLocatorViewController.headerTitleMaster = _selectedMenu.menuName;
                shopLocatorViewController.headerIconMaster = icon(_selectedMenu.menuId);
                
                [shopLocatorViewController createBarButtonItem:BACK_NOTIF];
                
                [self.navigationController pushViewController:shopLocatorViewController animated:YES];
                shopLocatorViewController = nil;
                [shopLocatorViewController release];
            }
            
            else if ([menuModel.href isEqualToString:@"#roaming"]) {
                [self performInfoRoaming];
            }
            
            else if ([menuModel.href isEqualToString:@"#xlstar"]) {
                [self performXLStar];
            }
            
            else if ([menuModel.href isEqualToString:@"#xlstar_detail"]) {
                ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
                thankYouViewController.isXLStar = YES;
                thankYouViewController.info = menuModel.desc;
                thankYouViewController.menuModel = menuModel;
                //thankYouViewController.headerTitleMaster = _selectedMenu.groupName;
                //thankYouViewController.headerIconMaster = icon(_selectedMenu.parentId);
                thankYouViewController.displayFBButton = NO;
                thankYouViewController.hideInboxBtn = YES;
               
                //thankYouViewController.trxId = self.trxId;
                
                [self.navigationController pushViewController:thankYouViewController animated:YES];
                thankYouViewController = nil;
                [thankYouViewController release];
            }
            
            // TODO : NEPTUNE
            else if([menuModel.href isEqualToString:@"#sukasuka"])
            {
//                [self getPaketSuka];
                [self getPageSukaSuka];
            }
            else {
                SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
                
//                DataManager *dataManager = [DataManager sharedInstance];
//                dataManager.parentId = menuModel.menuId;
//                dataManager.menuModel = menuModel;
//                
//                subMenuVC.parentId = menuModel.menuId;
//                subMenuVC.groupDesc = menuModel.desc;
                
                subMenuVC.parentMenuModel = menuModel;
                subMenuVC.grayHeaderTitle = menuModel.groupName;
                
                //subMenuVC.subMenuViewController = self; // matiin karna gak dipake
                
                //[self.viewDeckController setCenterController:subMenuVC];
                
                [self.navigationController pushViewController:subMenuVC animated:YES];
                
                subMenuVC = nil;
                [subMenuVC release];
            }
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        if ([_parentMenuModel.href isEqualToString:@"#package"] || [_parentMenuModel.hrefweb isEqualToString:@"#recommended"] || _isHotOffer) {
            headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight*1.5)
                                                         withTitle:sectionTitle
                                                      withSubtitle:[Language get:@"my_package" alter:nil]
                                                    isFirstSection:YES
                                                  isZeroTopPadding:YES] autorelease];
            [headerView.packageButton addTarget:self action:@selector(performCheckQuota) forControlEvents:UIControlEventTouchUpInside];
            
            DataManager *sharedData = [DataManager sharedInstance];
            if (sharedData.haveQuotaCalculator) {
                [headerView.quotaCalculatorButton addTarget:self action:@selector(performQuotaCalculator) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        else {
            headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight*1.5)
                                                    withLabelForXL:sectionTitle
                                                    isFirstSection:YES] autorelease];
        }
    }
    else {
        if (_levelTwoType == XL_TUNAI) {
            DataManager *sharedData = [DataManager sharedInstance];
            NSString *balance = addThousandsSeparator(sharedData.xlTunaiBalanceData.balance, sharedData.profileData.language);
            //NSLog(@"--> width = %f",tableView.frame.size.width);
            headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                         withTitle:sectionTitle
                                                      withSubtitle:balance
                                                    isFirstSection:NO
                                                  isZeroTopPadding:YES] autorelease];
        }
        else if (_levelTwoType == XL_STAR_VIEW) {
            
            NSString *starString = @"";
            int starNumber = [self.star intValue];
            //NSLog(@"star = %i",starNumber);
            if (starNumber == 0) {
                starString = @"JJJ";
            }
            else if (starNumber == 1) {
                starString = @"KJJ";
            }
            else if (starNumber == 2) {
                starString = @"KKJ";
            }
            else {
                starString = @"KKK";
            }
            
            headerView = [[[SectionHeaderView alloc] initForXLStarWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                                  withTitle:sectionTitle
                                                                   withStar:starString] autorelease];
            /*
            headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                    withLabelForXL:sectionTitle
                                                    isFirstSection:NO] autorelease];*/
        }
        // Common
        else {
            headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                    withLabelForXL:sectionTitle
                                                    isFirstSection:NO] autorelease];
        }
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return kDefaultCellHeight*1.5;
    }
    else {
        return kDefaultCellHeight;
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // tag == 1 Extend Validity
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            //NSLog(@"YES");
            [self performExtendValidity:_selectedMenu.price];
        }
        else {
            //NSLog(@"NO");
        }
    }
    
    // tag == 2 Buy Package
    if (alertView.tag == 2)
    {
        if (buttonIndex == 0) {
            //NSLog(@"YES");
            
            /*
             * Google Analytic
             */
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            // Screen tracking
            NSString *gaLabel = [NSString stringWithFormat:@"%@ -- yes",_selectedMenu.packageId];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                                  action:@"Buy"         // Event action (required)
                                                                   label:gaLabel        // Event label
                                                                   value:nil] build]];
            [[GAI sharedInstance] dispatch];
            /******************/
            
            // TODO : V1.9
            // Loading time for buy package
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.dateToday = [[NSDate alloc] init];
            NSLog(@"Buy Package Time %@", appDelegate.dateToday);
            [self performBuyPackage:_selectedMenu.packageId withAmount:_selectedMenu.price];
        }
        else {
            //NSLog(@"NO");
            /*
             * Google Analytic
             */
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            // Screen tracking
            NSString *gaLabel = [NSString stringWithFormat:@"%@ -- no",_selectedMenu.packageId];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                                  action:@"Buy"         // Event action (required)
                                                                   label:gaLabel        // Event label
                                                                   value:nil] build]];
            [[GAI sharedInstance] dispatch];
            /******************/
        }
    }
    // Pay With Credit Card
    if (alertView.tag == 4)
    {
        // pay with credit card
        if(buttonIndex == 0)
        {
            /*
             * Google Analytic
             */
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            // Screen tracking
            NSString *gaLabel = [NSString stringWithFormat:@"%@ -- cc",_selectedMenu.packageId];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                                  action:@"Buy"         // Event action (required)
                                                                   label:gaLabel        // Event label
                                                                   value:nil] build]];
            [[GAI sharedInstance] dispatch];
            /******************/
            
            NSURL *url = nil;
            DataManager *sharedData = [DataManager sharedInstance];
            
            NSString *saltKeyAPI = SALT_KEY;
            NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
            //            NSString *paymentId = @"1"; //hardcoded
            NSString *lang = sharedData.profileData.language;
            
            NSString *packageID = _selectedMenu.packageId;
            NSString *price = _selectedMenu.price;
            
            NSString *baseUrlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API,BUY_PKG_M_KARTU];
            NSString *paramString = [NSString stringWithFormat:@"msisdn=%@&msisdnInitiator=%@&currency=IDR&productType=1&productId=%@&lang=%@&amount=%@&channelID=&desc=&returnUrl=&extraParam=&remarks=&signature=&password=&checkoutType=",msisdn,msisdn,packageID,lang,price];
            NSLog(@"paramString = %@",paramString);
            paramString = [EncryptDecrypt doCipherForiOS7:paramString action:kCCEncrypt withKey:saltKeyAPI];
            
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlString,paramString]];
            NSLog(@"URL M-Kartu = %@",url);
            
            SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
            webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
            [self presentViewController:webViewController animated:YES completion:NULL];
        }
        else if (buttonIndex == 1)
        {
            /*
             * Google Analytic
             */
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            // Screen tracking
            NSString *gaLabel = [NSString stringWithFormat:@"%@ -- yes",_selectedMenu.packageId];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                                  action:@"Buy"         // Event action (required)
                                                                   label:gaLabel        // Event label
                                                                   value:nil] build]];
            [[GAI sharedInstance] dispatch];
            /******************************/
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.dateToday = [[NSDate alloc] init];
            NSLog(@"Buy Package Time %@", appDelegate.dateToday);
            [self performBuyPackage:_selectedMenu.packageId withAmount:_selectedMenu.price];
        }
        NSLog(@"button index %ld", (long)buttonIndex);
    }
}

// TODO : NEPTUNE
-(void)getPaketSuka
{
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = PAKET_SUKA_SUKA_REQ;
    [request paketSukaSuka];
}

// TODO : NEPTUNE
-(void)getPageSukaSuka
{
    @try
    {
//        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        self.hud.labelText = [Language get:@"loading" alter:nil];
        [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = PAGE_SUKASUKA_REQ;
        [request pageSukasuka];
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

#pragma mark - Method overriding from Basic View Controller

@end
