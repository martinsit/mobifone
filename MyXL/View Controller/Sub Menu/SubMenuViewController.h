//
//  SubMenuViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "MenuModel.h"
#import "AXISnetRequest.h"

typedef enum {
    COMMON = 0,
    XL_TUNAI,
    XL_STAR_VIEW
}LevelTwoType;

typedef enum {
    COMMON_PACKAGE = 0,
    HOT_OFFER_PACKAGE,
    RECOMMENDED_XL_PACKAGE
}SubMenuContentType;

@interface SubMenuViewController : BasicViewController <UIAlertViewDelegate, AXISnetRequestDelegate> {
    NSArray *_sortedMenu;
    NSDictionary *_theSourceMenu;
    
    MenuModel *_parentMenuModel;
    NSString *_parentId;
    NSString *_groupDesc;
    
    MenuModel *_recommendedPackageModel;
    MenuModel *_upgradePackageModel;
    
    BOOL _isHotOffer;
    
    BOOL _isLevel2;
    
    NSString *_currentPackage;
    
    NSString *_trxId;
    
    LevelTwoType _levelTwoType;
    
    NSString *_star;
    
    NSString *_grayHeaderTitle;
    
    SubMenuContentType _subMenuContentType;
}

@property (nonatomic, retain) NSArray *sortedMenu;
@property (nonatomic, retain) NSDictionary *theSourceMenu;

@property (nonatomic, retain) MenuModel *parentMenuModel;

@property (nonatomic, retain) NSString *parentId;
@property (nonatomic, retain) NSString *groupDesc;

@property (nonatomic, retain) MenuModel *recommendedPackageModel;
@property (nonatomic, retain) MenuModel *upgradePackageModel;

@property (readwrite) BOOL isHotOffer;

@property (readwrite) BOOL isLevel2;

@property (nonatomic, retain) NSString *currentPackage;

@property (nonatomic, retain) NSString *trxId;

@property (readwrite) LevelTwoType levelTwoType;

@property (nonatomic, retain) NSString *star;

@property (nonatomic, retain) NSString *grayHeaderTitle;

@property (nonatomic, retain) SubMenuViewController *subMenuViewController;

@property (readwrite) SubMenuContentType subMenuContentType;

@end
