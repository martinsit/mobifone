//
//  QuotaMeterViewController.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 5/7/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BasicViewController.h"
#import "KASlideShow.h"
#import "CMPopTipView.h"
#import "ViewFactory.h"

@interface QuotaMeterViewController : BasicViewController <KASlideShowDelegate, CMPopTipViewDelegate> {
    ViewFactory *viewFactory;
}

@property (nonatomic, retain) ViewFactory *viewFactory;

@end
