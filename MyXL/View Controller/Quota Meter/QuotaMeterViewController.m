//
//  QuotaMeterViewController.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 5/7/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "QuotaMeterViewController.h"
#import "AXISnetNavigationBar.h"
#import "SectionHeaderView.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "DataManager.h"
#import "QuotaMeterModel.h"
#import "SubMenuViewController.h"
#import "AsyncImageView.h"

#import "SVWebViewController.h"
#import "AdsModel.h"
#import "AXISnetCommon.h"

#import "ViewFactory.h"
#import "FlatButton.h"

@interface QuotaMeterViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, retain) IBOutlet UILabel *packageTitleLabel;
@property (nonatomic, retain) IBOutlet UILabel *packageValueLabel;
@property (nonatomic, retain) IBOutlet UIButton *hintButton;
@property (nonatomic, retain) IBOutlet UILabel *remainingLabel;

@property (nonatomic, retain) IBOutlet UILabel *quotaTypeLabel;
@property (nonatomic, retain) IBOutlet UILabel *dataUsageLabel;
@property (nonatomic, retain) IBOutlet UILabel *quotaFromLabel;
@property (nonatomic, retain) IBOutlet UILabel *quotaToLabel;

@property (nonatomic, retain) IBOutlet UILabel *descLabel;
@property (nonatomic, retain) IBOutlet UIButton *detailButton;
@property (nonatomic, retain) IBOutlet UIButton *recommendationButton;
@property (strong, nonatomic) IBOutlet KASlideShow *slideshow;
@property (nonatomic, retain) IBOutlet UIImageView *bannerView;
@property (nonatomic, retain) IBOutlet UIView *iconView;
@property (nonatomic, retain) IBOutlet UIWebView *theWebView;

@property (nonatomic, strong)	id				currentPopTipViewTarget;
@property (nonatomic, strong)	NSMutableArray	*visiblePopTipViews;

@property (nonatomic, retain) NSString *greyHeaderText;



@end

@implementation QuotaMeterViewController

@synthesize viewFactory;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self copyFolder];
        
        _theScrollView.hidden = YES;
        
        //self.view.backgroundColor = kDefaultWhiteSmokeColor;
        _theTableView.backgroundColor = kDefaultWhiteSmokeColor;
        
        //[_theTableView setBackgroundView:nil];
        //[_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.viewFactory = [[ViewFactory alloc] initWithNib:@"ViewFactoryTemplates"];
    self.viewFactory = [ViewFactory sharedInstanceVF];
}

- (void)viewWillAppear:(BOOL)animated {
    
    self.visiblePopTipViews = [NSMutableArray array];
    
    //[self setupUILayout];
    
    //[self copyFolder];
    
    //[self readAndEditHTMLFile];
    
    //[self displayContent];
    
    //[self setupValue];
    
    DataManager *sharedData = [DataManager sharedInstance];
    for (QuotaMeterModel *model in sharedData.quotaMeterData) {
        if (model.quotaType == 3) {
            self.greyHeaderText = model.buttonLabel;
        }
    }
    
    [_theTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    DataManager *sharedData = [DataManager sharedInstance];
    return [sharedData.quotaMeterData count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"++++++++ section = %i",indexPath.section);
    if (indexPath.section == 0) {
        return nil;
    }
    else {
        DataManager *sharedData = [DataManager sharedInstance];
        
        QuotaMeterModel *model = [sharedData.quotaMeterData objectAtIndex:indexPath.section - 1];
        
        if (model.quotaType == 1) {
            UITableViewCell *cell = [self.viewFactory cellOfKind:@"quotameterlow" forTable:tableView];
            
            cell.backgroundColor = kDefaultWhiteColor;
            
            // Quota Meter
            UIWebView *webView = (UIWebView *)[cell viewWithTag:201];
            [self readAndEditHTMLFile:model];
            NSString *fileName = [self displayContent:model];
            NSURL *url = [NSURL fileURLWithPath:fileName];
            [webView loadRequest:[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0]];
            
            // Variant Title Label
            UILabel *packageTitleLabel = (UILabel *)[cell viewWithTag:202];
            packageTitleLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            packageTitleLabel.textColor = kDefaultBlackColor;
            packageTitleLabel.text = [Language get:@"variant" alter:nil].uppercaseString;
            
            // Variant Value Label
            UILabel *packageValueLabel = (UILabel *)[cell viewWithTag:203];
            packageValueLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            packageValueLabel.textColor = kDefaultAquaMarineColor;
            packageValueLabel.text = model.variantName.uppercaseString;
            
            // Hint Button
            FlatButton *hintButton = (FlatButton *)[cell viewWithTag:204];
            UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            hintButton.buttonBackgroundColor = kDefaultAquaMarineColor;
            hintButton.buttonSelectedColor = kDefaultBlueColor;
            hintButton.buttonTitle = @"?";
            hintButton.buttonTitleColor = kDefaultWhiteColor;
            hintButton.buttonTitleFont = buttonFont;
            [hintButton refreshButtonAppearance];
            hintButton.tag = indexPath.section - 1;
            [hintButton addTarget:self action:@selector(performHint:) forControlEvents:UIControlEventTouchUpInside];
            
            // Remaining Days Label
            UILabel *remainingLabel = (UILabel *)[cell viewWithTag:205];
            remainingLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            remainingLabel.textColor = kDefaultNavyBlueColor;
            remainingLabel.text = model.remainingDaysLabel;
            CGSize size = calculateExpectedSize(packageValueLabel, packageValueLabel.text);
            CGRect frame = packageValueLabel.frame;
            frame.size.height = size.height;
            
            if (frame.size.height > 20.0) {
                //NSLog(@"--> Varian Value height = %f",frame.size.height);
                
                packageValueLabel.frame = frame;
                
                frame = remainingLabel.frame;
                frame.origin.y = packageValueLabel.frame.origin.y + packageValueLabel.frame.size.height;
                remainingLabel.frame = frame;
            }
            
            // Quota Type Label
            UILabel *quotaTypeLabel = (UILabel *)[cell viewWithTag:206];
            quotaTypeLabel.font = [UIFont fontWithName:kDefaultFont size:25.0];
            quotaTypeLabel.textColor = kDefaultAquaMarineColor;
            quotaTypeLabel.text = model.quotaTypeLabel;
            
            // Data Usage Label
            UILabel *dataUsageLabel = (UILabel *)[cell viewWithTag:207];
            dataUsageLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            dataUsageLabel.textColor = kDefaultBlackColor;
            dataUsageLabel.text = [Language get:@"data_usage" alter:nil].uppercaseString;
            
            // Quota Min Label
            UILabel *quotaFromLabel = (UILabel *)[cell viewWithTag:208];
            quotaFromLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            quotaFromLabel.textColor = kDefaultBlackColor;
            quotaFromLabel.text = model.quotaNormalUsageFrom;
            //quotaFromLabel.backgroundColor = [UIColor redColor];
            
            // Quota Max Label
            UILabel *quotaToLabel = (UILabel *)[cell viewWithTag:209];
            quotaToLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            quotaToLabel.textColor = kDefaultBlackColor;
            quotaToLabel.text = model.quotaNormalUsageTo;
            //quotaToLabel.backgroundColor = [UIColor redColor];
            
            // Description Label Bottom
            UILabel *descLabel = (UILabel *)[cell viewWithTag:210];
            descLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            descLabel.textColor = kDefaultBlackColor;
            descLabel.text = model.descLabel;
            size = calculateExpectedSize(descLabel, descLabel.text);
            frame = descLabel.frame;
            frame.size.height = size.height;
            descLabel.frame = frame;
            if (frame.size.height > 20.0) {
                CGFloat diff = frame.size.height - 20.0;
                frame.origin.y = frame.origin.y - diff;
                descLabel.frame = frame;
            }
            
            // Banner View
            AsyncImageView *bannerView = (AsyncImageView *)[cell viewWithTag:211];
            bannerView.tag = indexPath.section - 1;
            UITapGestureRecognizer *tapImageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                              action:@selector(performBannerViewHref:)];
            [bannerView setUserInteractionEnabled:YES];
            [bannerView addGestureRecognizer:tapImageGesture];
            // load the image
            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:bannerView];
            
            if ([model.banner length] > 0) {
                bannerView.showActivityIndicator = YES;
                bannerView.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%i",model.banner,rand()]];
                bannerView.hidden = NO;
            }
            else {
                bannerView.showActivityIndicator = NO;
                bannerView.hidden = YES;
            }
            
            // Detail Button
            FlatButton *detailButton = (FlatButton *)[cell viewWithTag:212];
            detailButton.buttonBackgroundColor = kDefaultAquaMarineColor;
            detailButton.buttonSelectedColor = kDefaultBlueColor;
            detailButton.buttonTitle = [model.detailButton uppercaseString];
            detailButton.buttonTitleColor = kDefaultWhiteColor;
            detailButton.buttonTitleFont = buttonFont;
            [detailButton refreshButtonAppearance];
            detailButton.tag = indexPath.section - 1;
            [detailButton addTarget:self action:@selector(performCheckQuota:) forControlEvents:UIControlEventTouchUpInside];
            
            // Recommendation Button
            //FlatButton *recommendationButton = (FlatButton *)[cell viewWithTag:213];
            
            // Icon View
            UIView *iconView = (UIView *)[cell viewWithTag:214];
            int section = indexPath.section;
            iconView.tag = section - 1;
            
            // LOW
            //recommendationButton.hidden = YES;
            //bannerView.hidden = NO;
            //iconView.hidden = NO;
            
            // load icon image
            CGFloat widthHeight = 50.0; //32
            CGFloat x = 0.0;
            UIView *iconFrame = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, (widthHeight * [model.icon count]), widthHeight)];
            for (int i = 0; i<[model.icon count]; i++) {
                AdsModel *adsModel = [model.icon objectAtIndex:i];
                AsyncImageView *icon = [[AsyncImageView alloc] initWithFrame:CGRectMake(x, 0.0, widthHeight, widthHeight)];
                //icon.tag = (iconView.tag * 10) + i;
                icon.tag = ((indexPath.section - 1) * 10) + i;
                //NSLog(@"--> icon tag = %i",icon.tag);
                [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:icon];
                icon.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%i",adsModel.imageURL,rand()]];
                icon.showActivityIndicator = YES;
                
                [iconFrame addSubview:icon];
                
                //
                UITapGestureRecognizer *tapImageGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                                   action:@selector(performIconHref:)];
                [icon setUserInteractionEnabled:YES];
                [icon addGestureRecognizer:tapImageGesture2];
                //
                
                x = icon.frame.origin.x + icon.frame.size.width;
                
                [icon release];
                icon = nil;
                
                if (i == [model.icon count] - 1) {
                    CGRect frame = iconFrame.frame;
                    frame.origin.x = (self.view.bounds.size.width - frame.size.width) / 2;
                    iconFrame.frame = frame;
                    
                    [iconView addSubview:iconFrame];
                }
            }
            
            return cell;
        }
        
        else if (model.quotaType == 2) {
            UITableViewCell *cell = [self.viewFactory cellOfKind:@"quotameternormal" forTable:tableView];
            
            cell.backgroundColor = kDefaultWhiteColor;
            
            // Quota Meter
            UIWebView *webView = (UIWebView *)[cell viewWithTag:201];
            [self readAndEditHTMLFile:model];
            NSString *fileName = [self displayContent:model];
            NSURL *url = [NSURL fileURLWithPath:fileName];
            [webView loadRequest:[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0]];
            
            // Variant Title Label
            UILabel *packageTitleLabel = (UILabel *)[cell viewWithTag:202];
            packageTitleLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            packageTitleLabel.textColor = kDefaultBlackColor;
            packageTitleLabel.text = [Language get:@"variant" alter:nil].uppercaseString;
            
            // Variant Value Label
            UILabel *packageValueLabel = (UILabel *)[cell viewWithTag:203];
            packageValueLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            packageValueLabel.textColor = kDefaultAquaMarineColor;
            packageValueLabel.text = model.variantName.uppercaseString;
            
            // Hint Button
            FlatButton *hintButton = (FlatButton *)[cell viewWithTag:204];
            UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            hintButton.buttonBackgroundColor = kDefaultAquaMarineColor;
            hintButton.buttonSelectedColor = kDefaultBlueColor;
            hintButton.buttonTitle = @"?";
            hintButton.buttonTitleColor = kDefaultWhiteColor;
            hintButton.buttonTitleFont = buttonFont;
            [hintButton refreshButtonAppearance];
            hintButton.tag = indexPath.section - 1;
            [hintButton addTarget:self action:@selector(performHint:) forControlEvents:UIControlEventTouchUpInside];
            
            // Remaining Days Label
            UILabel *remainingLabel = (UILabel *)[cell viewWithTag:205];
            remainingLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            remainingLabel.textColor = kDefaultNavyBlueColor;
            remainingLabel.text = model.remainingDaysLabel;
            CGSize size = calculateExpectedSize(packageValueLabel, packageValueLabel.text);
            CGRect frame = packageValueLabel.frame;
            frame.size.height = size.height;
            
            if (frame.size.height > 20.0) {
                //NSLog(@"--> Varian Value height = %f",frame.size.height);
                
                packageValueLabel.frame = frame;
                
                frame = remainingLabel.frame;
                frame.origin.y = packageValueLabel.frame.origin.y + packageValueLabel.frame.size.height;
                remainingLabel.frame = frame;
            }
            
            // Quota Type Label
            UILabel *quotaTypeLabel = (UILabel *)[cell viewWithTag:206];
            quotaTypeLabel.font = [UIFont fontWithName:kDefaultFont size:25.0];
            quotaTypeLabel.textColor = kDefaultAquaMarineColor;
            quotaTypeLabel.text = model.quotaTypeLabel;
            
            // Data Usage Label
            UILabel *dataUsageLabel = (UILabel *)[cell viewWithTag:207];
            dataUsageLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            dataUsageLabel.textColor = kDefaultBlackColor;
            dataUsageLabel.text = [Language get:@"data_usage" alter:nil].uppercaseString;
            
            // Quota Min Label
            UILabel *quotaFromLabel = (UILabel *)[cell viewWithTag:208];
            quotaFromLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            quotaFromLabel.textColor = kDefaultBlackColor;
            quotaFromLabel.text = model.quotaNormalUsageFrom;
            //quotaFromLabel.backgroundColor = [UIColor redColor];
            
            // Quota Max Label
            UILabel *quotaToLabel = (UILabel *)[cell viewWithTag:209];
            quotaToLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            quotaToLabel.textColor = kDefaultBlackColor;
            quotaToLabel.text = model.quotaNormalUsageTo;
            //quotaToLabel.backgroundColor = [UIColor redColor];
            
            // Description Label Bottom
            UILabel *descLabel = (UILabel *)[cell viewWithTag:210];
            descLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            descLabel.textColor = kDefaultBlackColor;
            descLabel.text = model.descLabel;
            size = calculateExpectedSize(descLabel, descLabel.text);
            frame = descLabel.frame;
            frame.size.height = size.height;
            descLabel.frame = frame;
            /*
            if (frame.size.height > 20.0) {
                CGFloat diff = frame.size.height - 20.0;
                frame.origin.y = frame.origin.y - diff;
                descLabel.frame = frame;
            }*/
            
            // Banner View
            /*
            AsyncImageView2 *bannerView = (AsyncImageView2 *)[cell viewWithTag:211];
            bannerView.tag = indexPath.section - 1;
            UITapGestureRecognizer *tapImageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                              action:@selector(performBannerViewHref:)];
            [bannerView setUserInteractionEnabled:YES];
            [bannerView addGestureRecognizer:tapImageGesture];*/
            
            // Detail Button
            FlatButton *detailButton = (FlatButton *)[cell viewWithTag:212];
            detailButton.buttonBackgroundColor = kDefaultAquaMarineColor;
            detailButton.buttonSelectedColor = kDefaultBlueColor;
            detailButton.buttonTitle = [model.detailButton uppercaseString];
            detailButton.buttonTitleColor = kDefaultWhiteColor;
            detailButton.buttonTitleFont = buttonFont;
            [detailButton refreshButtonAppearance];
            detailButton.tag = indexPath.section - 1;
            [detailButton addTarget:self action:@selector(performCheckQuota:) forControlEvents:UIControlEventTouchUpInside];
            
            // Recommendation Button
            FlatButton *recommendationButton = (FlatButton *)[cell viewWithTag:213];
            
            // Icon View
            /*
            UIView *iconView = (UIView *)[cell viewWithTag:214];
            NSLog(@"****** section %i",indexPath.section);
            int section = indexPath.section;
            iconView.tag = section - 1;
            NSLog(@"--> iconView tag = %i",iconView.tag);
            iconView.backgroundColor = [UIColor redColor];
            
            NSLog(@"--> section %i = NORMAL",indexPath.section);*/
            
            // NORMAL
            if ([model.buttonLabel length] > 0) {
                recommendationButton.buttonBackgroundColor = kDefaultAquaMarineColor;
                recommendationButton.buttonSelectedColor = kDefaultBlueColor;
                recommendationButton.buttonTitle = [model.buttonLabel uppercaseString];
                recommendationButton.buttonTitleColor = kDefaultWhiteColor;
                recommendationButton.buttonTitleFont = buttonFont;
                [recommendationButton refreshButtonAppearance];
                recommendationButton.tag = indexPath.section - 1;
                [recommendationButton addTarget:self action:@selector(performPackageList:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            //bannerView.hidden = YES;
            //iconView.hidden = YES;
            return cell;
        }
        
        else {
            UITableViewCell *cell = [self.viewFactory cellOfKind:@"quotameterhigh" forTable:tableView];
            
            cell.backgroundColor = kDefaultWhiteColor;
            
            // Quota Meter
            UIWebView *webView = (UIWebView *)[cell viewWithTag:201];
            [self readAndEditHTMLFile:model];
            NSString *fileName = [self displayContent:model];
            NSURL *url = [NSURL fileURLWithPath:fileName];
            [webView loadRequest:[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0]];
            
            // Variant Title Label
            UILabel *packageTitleLabel = (UILabel *)[cell viewWithTag:202];
            packageTitleLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            packageTitleLabel.textColor = kDefaultBlackColor;
            packageTitleLabel.text = [Language get:@"variant" alter:nil].uppercaseString;
            
            // Variant Value Label
            UILabel *packageValueLabel = (UILabel *)[cell viewWithTag:203];
            packageValueLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            packageValueLabel.textColor = kDefaultAquaMarineColor;
            packageValueLabel.text = model.variantName.uppercaseString;
            
            // Hint Button
            FlatButton *hintButton = (FlatButton *)[cell viewWithTag:204];
            UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            hintButton.buttonBackgroundColor = kDefaultAquaMarineColor;
            hintButton.buttonSelectedColor = kDefaultBlueColor;
            hintButton.buttonTitle = @"?";
            hintButton.buttonTitleColor = kDefaultWhiteColor;
            hintButton.buttonTitleFont = buttonFont;
            [hintButton refreshButtonAppearance];
            hintButton.tag = indexPath.section - 1;
            [hintButton addTarget:self action:@selector(performHint:) forControlEvents:UIControlEventTouchUpInside];
            
            // Remaining Days Label
            UILabel *remainingLabel = (UILabel *)[cell viewWithTag:205];
            remainingLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            remainingLabel.textColor = kDefaultNavyBlueColor;
            remainingLabel.text = model.remainingDaysLabel;
            CGSize size = calculateExpectedSize(packageValueLabel, packageValueLabel.text);
            CGRect frame = packageValueLabel.frame;
            frame.size.height = size.height;
            
            if (frame.size.height > 20.0) {
                //NSLog(@"--> Varian Value height = %f",frame.size.height);
                
                packageValueLabel.frame = frame;
                
                frame = remainingLabel.frame;
                frame.origin.y = packageValueLabel.frame.origin.y + packageValueLabel.frame.size.height;
                remainingLabel.frame = frame;
            }
            
            // Quota Type Label
            UILabel *quotaTypeLabel = (UILabel *)[cell viewWithTag:206];
            quotaTypeLabel.font = [UIFont fontWithName:kDefaultFont size:25.0];
            quotaTypeLabel.textColor = kDefaultAquaMarineColor;
            quotaTypeLabel.text = model.quotaTypeLabel;
            
            // Data Usage Label
            UILabel *dataUsageLabel = (UILabel *)[cell viewWithTag:207];
            dataUsageLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            dataUsageLabel.textColor = kDefaultBlackColor;
            dataUsageLabel.text = [Language get:@"data_usage" alter:nil].uppercaseString;
            
            // Quota Min Label
            UILabel *quotaFromLabel = (UILabel *)[cell viewWithTag:208];
            quotaFromLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            quotaFromLabel.textColor = kDefaultBlackColor;
            quotaFromLabel.text = model.quotaNormalUsageFrom;
            //quotaFromLabel.backgroundColor = [UIColor redColor];
            
            // Quota Max Label
            UILabel *quotaToLabel = (UILabel *)[cell viewWithTag:209];
            quotaToLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            quotaToLabel.textColor = kDefaultBlackColor;
            quotaToLabel.text = model.quotaNormalUsageTo;
            //quotaToLabel.backgroundColor = [UIColor redColor];
            
            // Description Label Bottom
            UILabel *descLabel = (UILabel *)[cell viewWithTag:210];
            descLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
            descLabel.textColor = kDefaultBlackColor;
            descLabel.text = model.descLabel;
            size = calculateExpectedSize(descLabel, descLabel.text);
            frame = descLabel.frame;
            frame.size.height = size.height;
            descLabel.frame = frame;
            if (frame.size.height > 20.0) {
                CGFloat diff = frame.size.height - 20.0;
                frame.origin.y = frame.origin.y - diff;
                descLabel.frame = frame;
            }
            
            // Banner View
            /*
            AsyncImageView2 *bannerView = (AsyncImageView2 *)[cell viewWithTag:211];
            bannerView.tag = indexPath.section - 1;
            UITapGestureRecognizer *tapImageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                              action:@selector(performBannerViewHref:)];
            [bannerView setUserInteractionEnabled:YES];
            [bannerView addGestureRecognizer:tapImageGesture];*/
            
            // Detail Button
            FlatButton *detailButton = (FlatButton *)[cell viewWithTag:212];
            detailButton.buttonBackgroundColor = kDefaultAquaMarineColor;
            detailButton.buttonSelectedColor = kDefaultBlueColor;
            detailButton.buttonTitle = [model.detailButton uppercaseString];
            detailButton.buttonTitleColor = kDefaultWhiteColor;
            detailButton.buttonTitleFont = buttonFont;
            [detailButton refreshButtonAppearance];
            detailButton.tag = indexPath.section - 1;
            [detailButton addTarget:self action:@selector(performCheckQuota:) forControlEvents:UIControlEventTouchUpInside];
            
            // Recommendation Button
            FlatButton *recommendationButton = (FlatButton *)[cell viewWithTag:213];
            
            // Icon View
            /*
            UIView *iconView = (UIView *)[cell viewWithTag:214];
            NSLog(@"****** section %i",indexPath.section);
            int section = indexPath.section;
            iconView.tag = section - 1;
            NSLog(@"--> iconView tag = %i",iconView.tag);
            iconView.backgroundColor = [UIColor redColor];
            
            NSLog(@"--> section %i = HIGH",indexPath.section);*/
            
            // HIGH
            recommendationButton.buttonBackgroundColor = kDefaultAquaMarineColor;
            recommendationButton.buttonSelectedColor = kDefaultBlueColor;
            recommendationButton.buttonTitle = [model.buttonLabel uppercaseString];
            recommendationButton.buttonTitleColor = kDefaultWhiteColor;
            recommendationButton.buttonTitleFont = buttonFont;
            [recommendationButton refreshButtonAppearance];
            recommendationButton.tag = indexPath.section - 1;
            [recommendationButton addTarget:self action:@selector(performRecommendationPackage:) forControlEvents:UIControlEventTouchUpInside];
            
            //bannerView.hidden = YES;
            
            /*
            for (UIView *subView in [iconView subviews])
            {
                [subView removeFromSuperview];
            }
            iconView.hidden = YES;*/
            
            return cell;
            
        }
        
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return [Language get:@"quota_meter" alter:nil];
    }
    else {
        return [Language get:@"data_usage" alter:nil];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 0;
    }
    else {
        return 510;
    }
}

#pragma mark - Table view delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                withLabelForXL:sectionTitle
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kDefaultCellHeight)
                                                withLabelForXL:sectionTitle
                                                isFirstSection:NO] autorelease];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return kDefaultCellHeight;
    }
    else {
        return kDefaultCellHeight;
    }
}

#pragma mark - Selector

- (void)setupUILayout {
    /*
    self.visiblePopTipViews = [NSMutableArray array];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:[Language get:@"quota_meter" alter:nil]
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:[Language get:@"data_usage" alter:nil]
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    _packageTitleLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _packageTitleLabel.textColor = kDefaultBlackColor;
    
    _packageValueLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _packageValueLabel.textColor = kDefaultAquaMarineColor;
    
    _remainingLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _remainingLabel.textColor = kDefaultNavyBlueColor;
    
    _descLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _descLabel.textColor = kDefaultBlackColor;
    
    _quotaTypeLabel.font = [UIFont fontWithName:kDefaultFont size:25.0];
    _quotaTypeLabel.textColor = kDefaultAquaMarineColor;
    
    _dataUsageLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _dataUsageLabel.textColor = kDefaultBlackColor;
    
    _quotaFromLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _quotaFromLabel.textColor = kDefaultBlackColor;
    
    _quotaToLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _quotaToLabel.textColor = kDefaultBlackColor;
    
    //-------------//
    // Banner View //
    //-------------//
    UITapGestureRecognizer *tapImageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(performBannerViewHref)];
    [_bannerView setUserInteractionEnabled:YES];
    [_bannerView addGestureRecognizer:tapImageGesture];
    
    //--------------//
    // Setup Button //
    //--------------//
    UIFont *titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    CGRect frame = _hintButton.frame;
    UIButton *button = createButtonWithFrame(frame,
                                             @"?",
                                             titleFont,
                                             kDefaultWhiteColor,
                                             kDefaultAquaMarineColor,
                                             kDefaultBlueColor);
    [button addTarget:self action:@selector(performHint:) forControlEvents:UIControlEventTouchUpInside];
    //_hintButton = button;
    [_fieldView addSubview:button];
    _hintButton.hidden = YES;
    
    
    // Detail Button
    frame = _detailButton.frame;
    button = createButtonWithFrame(frame,
                                   [sharedData.quotaMeterData.detailButton uppercaseString],
                                   titleFont,
                                   kDefaultWhiteColor,
                                   kDefaultAquaMarineColor,
                                   kDefaultBlueColor);
    [button addTarget:self action:@selector(performCheckQuota:) forControlEvents:UIControlEventTouchUpInside];
    [_fieldView addSubview:button];
    _detailButton.hidden = YES;
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    newFrame.size.height = _iconView.frame.origin.y + _iconView.frame.size.height;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Content View //
    //--------------//
    newFrame = _contentView.frame;
    newFrame.size.height = _fieldView.frame.origin.y + _fieldView.frame.size.height;
    _contentView.frame = newFrame;
    _contentView.backgroundColor = kDefaultWhiteColor;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 200.0);
    
    if (sharedData.quotaMeterData.quotaType == 1) {
        // LOW
        _recommendationButton.hidden = YES;
        
        // load the image
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:_bannerView];
        _bannerView.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%i",sharedData.quotaMeterData.banner,rand()]];
        
        // load icon image
        CGFloat widthHeight = 50.0; //32
        CGFloat x = 0.0;
        UIView *iconFrame = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, (widthHeight * [sharedData.quotaMeterData.icon count]), widthHeight)];
        for (int i = 0; i<[sharedData.quotaMeterData.icon count]; i++) {
            AdsModel *adsModel = [sharedData.quotaMeterData.icon objectAtIndex:i];
            UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(x, 0.0, widthHeight, widthHeight)];
            icon.tag = i;
            icon.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%i",adsModel.imageURL,rand()]];
            
            [iconFrame addSubview:icon];
            
            //
            UITapGestureRecognizer *tapImageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                              action:@selector(performIconHref:)];
            [icon setUserInteractionEnabled:YES];
            [icon addGestureRecognizer:tapImageGesture];
            //
            
            x = icon.frame.origin.x + icon.frame.size.width;
            
            [icon release];
            icon = nil;
            
            if (i == [sharedData.quotaMeterData.icon count] - 1) {
                CGRect frame = iconFrame.frame;
                frame.origin.x = (self.view.bounds.size.width - frame.size.width) / 2;
                iconFrame.frame = frame;
                
                [_iconView addSubview:iconFrame];
            }
        }
    }
    else if (sharedData.quotaMeterData.quotaType == 2) {
        // NORMAL
        UIFont *titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
        CGRect frame = _recommendationButton.frame;
        
        if ([sharedData.quotaMeterData.buttonLabel length] > 0) {
            UIButton *button = createButtonWithFrame(frame,
                                                     sharedData.quotaMeterData.buttonLabel,
                                                     titleFont,
                                                     kDefaultWhiteColor,
                                                     kDefaultAquaMarineColor,
                                                     kDefaultBlueColor);
            [button addTarget:self action:@selector(performPackageList:) forControlEvents:UIControlEventTouchUpInside];
            [_fieldView addSubview:button];
        }
        _recommendationButton.hidden = YES;
        
        _bannerView.hidden = YES;
        _iconView.hidden = YES;
    }
    else {
        // HIGH
        UIFont *titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
        CGRect frame = _recommendationButton.frame;
        
        UIButton *button = createButtonWithFrame(frame,
                                                 sharedData.quotaMeterData.buttonLabel,
                                                 titleFont,
                                                 kDefaultWhiteColor,
                                                 kDefaultAquaMarineColor,
                                                 kDefaultBlueColor);
        [button addTarget:self action:@selector(performRecommendationPackage:) forControlEvents:UIControlEventTouchUpInside];
        [_fieldView addSubview:button];
        _recommendationButton.hidden = YES;
        
        _bannerView.hidden = YES;
        _iconView.hidden = YES;
    }*/
}

- (void)setupValue {
    /*
    DataManager *sharedData = [DataManager sharedInstance];
    
    _packageTitleLabel.text = [Language get:@"package" alter:nil].uppercaseString;
    
    _packageValueLabel.text = sharedData.quotaMeterData.quotaPackageLabel.uppercaseString;
    CGSize size = calculateExpectedSize(_packageValueLabel, _packageValueLabel.text);
    CGRect frame = _packageValueLabel.frame;
    frame.size.height = size.height;
    _packageValueLabel.frame = frame;
    
    if (frame.size.height > 21.0) {
        frame = _remainingLabel.frame;
        frame.origin.y = _packageValueLabel.frame.origin.y + _packageValueLabel.frame.size.height;
        _remainingLabel.frame = frame;
    }
    
    _remainingLabel.text    = sharedData.quotaMeterData.remainingDaysLabel;
    _descLabel.text         = sharedData.quotaMeterData.descLabel;
    size = calculateExpectedSize(_descLabel, _descLabel.text);
    frame = _descLabel.frame;
    frame.size.height = size.height;
    _descLabel.frame = frame;
    
    if (frame.size.height > 21.0) {
        CGFloat diff = frame.size.height - 21.0;
        frame.origin.y = frame.origin.y - diff;
        _descLabel.frame = frame;
    }
    
    _quotaTypeLabel.text = sharedData.quotaMeterData.quotaTypeLabel;
    _quotaFromLabel.text = sharedData.quotaMeterData.quotaNormalUsageFrom;
    _quotaToLabel.text   = sharedData.quotaMeterData.quotaNormalUsageTo;
    _dataUsageLabel.text = [Language get:@"data_usage" alter:nil].uppercaseString;*/
}

- (void)performHint:(id)sender {

    UIButton *button = (UIButton*)sender;
    int tag = button.tag;
    
    DataManager *sharedData = [DataManager sharedInstance];
    QuotaMeterModel *model = [sharedData.quotaMeterData objectAtIndex:tag];
    
    //NSString *title = sharedData.quotaMeterData.hintLabel;
    NSString *contentMessage = model.hintDescLabel;
    //NSLog(@"--> contentMessage %@",contentMessage);
    ///CMPopTipView *popTipView = [[CMPopTipView alloc] initWithTitle:title message:contentMessage];
    CMPopTipView *popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
    popTipView.delegate = self;
    popTipView.backgroundColor = kDefaultOrangeColor; //kDefaultGrayColor //[UIColor grayColor]
    popTipView.borderColor = [UIColor clearColor];
    popTipView.textColor = kDefaultWhiteColor;
    popTipView.animation = arc4random() % 2;
    //popTipView.has3DStyle = (BOOL)(arc4random() % 2);
    popTipView.has3DStyle = NO;
    
    // Some options to try.
    
    //popTipView.disableTapToDismiss = YES;
    //popTipView.preferredPointDirection = PointDirectionUp;
    popTipView.hasGradientBackground = NO;
    //popTipView.cornerRadius = 2.0;
    //popTipView.sidePadding = 30.0f;
    //popTipView.topMargin = 20.0f;
    //popTipView.pointerSize = 50.0f;
    popTipView.hasShadow = YES;
    
    popTipView.dismissTapAnywhere = YES;
    //[popTipView autoDismissAnimated:YES atTimeInterval:3.0];
    
    if ([sender isKindOfClass:[UIButton class]]) {
        UIButton *button = (UIButton *)sender;
        [popTipView presentPointingAtView:button inView:_theTableView animated:YES];
    }
    
    [self.visiblePopTipViews addObject:popTipView];
    self.currentPopTipViewTarget = sender;
}

- (void)dismissAllPopTipViews
{
	while ([self.visiblePopTipViews count] > 0) {
		CMPopTipView *popTipView = [self.visiblePopTipViews objectAtIndex:0];
		[popTipView dismissAnimated:YES];
		[self.visiblePopTipViews removeObjectAtIndex:0];
	}
}

- (void)performCheckQuota:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)performPackageList:(id)sender {
    
    NSLog(@"perform pkg list");
    
    SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    subMenuVC.parentMenuModel = sharedData.packageMenu;
    
    subMenuVC.isLevel2 = NO;
    subMenuVC.levelTwoType = COMMON;
    subMenuVC.grayHeaderTitle = sharedData.packageMenu.menuName;
    
    [self.navigationController pushViewController:subMenuVC animated:YES];
    subMenuVC = nil;
    [subMenuVC release];
}

- (void)performRecommendationPackage:(id)sender {
    
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    FlatButton *button = (FlatButton*)sender;
    int tag = button.tag;
    
    DataManager *sharedData = [DataManager sharedInstance];
    QuotaMeterModel *model = [sharedData.quotaMeterData objectAtIndex:tag];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = RECOMMENDED_PACKAGE_XL_REQ;
    [request recommendedPackageXL:model.serviceId];
}

- (void)performBannerViewHref:(id)sender {
    
    //UIImageView *imgView = (UIImageView*)sender;
    //int tag = imgView.tag;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
    
    QuotaMeterModel *model = [sharedData.quotaMeterData objectAtIndex:[tapRecognizer.view tag]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@", model.hrefBanner];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    SVWebViewController *webViewController = [[SVWebViewController alloc] initWithURL:url];
	[self.navigationController pushViewController:webViewController animated:YES];
}

- (void)performIconHref:(id)sender {
    
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
    //NSLog(@"--> icon tag = %i",[tapRecognizer.view tag]);
    
    int tag = [tapRecognizer.view tag];
    int section = 0;
    int iconIndex = 0;
    if (tag < 10) {
        section = 0;
        iconIndex = tag;
    }
    else {
        section = tag/10;
        iconIndex = tag - (section*10);
    }
    
    DataManager *sharedData = [DataManager sharedInstance];
    QuotaMeterModel *model = [sharedData.quotaMeterData objectAtIndex:section];
    AdsModel *adsModel = [model.icon objectAtIndex:iconIndex];
    
    NSString *urlString = [NSString stringWithFormat:@"%@",adsModel.actionURL];
    NSURL *url = [NSURL URLWithString:urlString];
    
    SVWebViewController *webViewController = [[SVWebViewController alloc] initWithURL:url];
	[self.navigationController pushViewController:webViewController animated:YES];
}

- (void)readAndEditHTMLFile:(QuotaMeterModel*)model {
    //NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"HTML"]];
    /*
    NSError* error = nil;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"HTML"];
    NSString *res = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error: &error];*/
    
    
    //
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"/HTML/index.html"];
    NSString *res = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    //
    
    //NSLog(@"res string = %@",res);
    
    //DataManager *sharedData = [DataManager sharedInstance];
    
    res = [res stringByReplacingOccurrencesOfString:@"value_data_usage"
                                         withString:model.quotaCurrentUsagePercent];
    
    res = [res stringByReplacingOccurrencesOfString:@"value_data_nusage"
                                         withString:model.quotaNormalUsagePercent];
    
    res = [res stringByReplacingOccurrencesOfString:@"value_nusage_text"
                                         withString:model.quotaNormalUsageDesc];
    
    res = [res stringByReplacingOccurrencesOfString:@"value_usage_text"
                                         withString:model.quotaCurrentUsageDesc];
    
    //NSLog(@"edit string = %@",res);
    
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName = [NSString stringWithFormat:@"%@/HTML/index%i.html",documentsDirectory,model.modelIndex];
    
    //NSString *path2 = [documentsDirectory stringByAppendingPathComponent:@"myfile.txt"];
    
    [res writeToFile:fileName
          atomically:NO
            encoding:NSStringEncodingConversionAllowLossy
               error:nil];
}

- (NSString*)displayContent:(QuotaMeterModel*)model {
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/HTML/index%i.html",documentsDirectory,model.modelIndex];
    
    /*
    NSString *content = [[NSString alloc] initWithContentsOfFile:fileName
                                                    usedEncoding:nil
                                                           error:nil];
    NSLog(@"content = %@",content);*/
    
    return fileName;
    
    //NSURL *url = [NSURL fileURLWithPath:fileName];
    //[_theWebView loadRequest:[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0]];
}

- (void)copyFolder {
    BOOL success1;
    NSFileManager *fileManager1 = [NSFileManager defaultManager];
    fileManager1.delegate = self;
    NSError *error1;
    NSArray *paths1 = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory1 = [paths1 objectAtIndex:0];
    NSString *writableDBPath1 = [documentsDirectory1 stringByAppendingPathComponent:@"/HTML"];
    //NSLog(@"writableDBPath1 = %@",writableDBPath1);
    success1 = [fileManager1 fileExistsAtPath:writableDBPath1];
    if (success1 )
    {
        NSString *defaultDBPath1 = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"HTML"];
        //NSLog(@"default path %@",defaultDBPath1);
        success1 = [fileManager1 copyItemAtPath:defaultDBPath1 toPath:writableDBPath1 error:&error1];
    } else {
        if (![[NSFileManager defaultManager] fileExistsAtPath:writableDBPath1]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:writableDBPath1 withIntermediateDirectories:NO attributes:nil error:&error1];
            
            NSString *defaultDBPath1 = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"HTML"];
            //NSLog(@"default path %@",defaultDBPath1);
            success1 = [fileManager1 copyItemAtPath:defaultDBPath1 toPath:writableDBPath1 error:&error1];
        }
        
    }
}

- (BOOL)fileManager:(NSFileManager *)fileManager shouldProceedAfterError:(NSError *)error copyingItemAtPath:(NSString *)srcPath toPath:(NSString *)dstPath{
    if ([error code] == 516) //error code for: The operation couldn’t be completed. File exists
        return YES;
    else
        return NO;
}

#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
	[self.visiblePopTipViews removeObject:popTipView];
	self.currentPopTipViewTarget = nil;
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == RECOMMENDED_PACKAGE_XL_REQ) {
            
            SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
            subMenuVC.isLevel2 = NO;
            subMenuVC.subMenuContentType = RECOMMENDED_XL_PACKAGE;
            subMenuVC.grayHeaderTitle = self.greyHeaderText;
            
            [self.navigationController pushViewController:subMenuVC animated:YES];
            subMenuVC = nil;
            [subMenuVC release];
        }
    }
    else {
        
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
