//
//  GiftPackageViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "GiftPackageViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

#import "DataManager.h"
#import "UnderLineLabel.h"

@interface GiftPackageViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *packageLabel;
@property (strong, nonatomic) IBOutlet UILabel *packageValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *transferToLabel;
@property (strong, nonatomic) IBOutlet UITextField *transferToTextField;
@property (strong, nonatomic) IBOutlet UILabel *transferToValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *thankYouLabel;

@property (strong, nonatomic) UIButton *transferButton;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

@property (strong, nonatomic) IBOutlet UILabel *captchaLabel;
@property (strong, nonatomic) IBOutlet UIWebView *captchaWebView;
@property (strong, nonatomic) IBOutlet UnderLineLabel *recaptchaLabel;
@property (strong, nonatomic) IBOutlet UITextField *captchaTF;

- (void)performTransfer;
- (void)submitTransfer;

@end

@implementation GiftPackageViewController

@synthesize isThankYouPage;
@synthesize toNumber;

@synthesize giftPackageModel = _giftPackageModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    
    //-------------//
    // Setup Label //
    //-------------//
    _packageLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _packageLabel.textColor = kDefaultTitleFontGrayColor;
    _packageLabel.text = [[Language get:@"package" alter:nil] uppercaseString];
    
    _packageValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _packageValueLabel.textColor = kDefaultPurpleColor;
    _packageValueLabel.backgroundColor = [UIColor clearColor];
    
    _priceLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _priceLabel.textColor = kDefaultTitleFontGrayColor;
    _priceLabel.text = [[Language get:@"price" alter:nil] uppercaseString];
    
    _priceValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _priceValueLabel.textColor = kDefaultPurpleColor;
    _priceValueLabel.backgroundColor = [UIColor clearColor];
    
    _transferToLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _transferToLabel.textColor = kDefaultTitleFontGrayColor;
    _transferToLabel.text = [[Language get:@"transfer_to" alter:nil] uppercaseString];
    
    _transferToTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _transferToTextField.textColor = kDefaultPurpleColor;
    
    _transferToValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _transferToValueLabel.textColor = kDefaultPurpleColor;
    _transferToValueLabel.backgroundColor = [UIColor clearColor];
    
    _thankYouLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _thankYouLabel.textColor = kDefaultPurpleColor;
    _thankYouLabel.backgroundColor = [UIColor clearColor];
    _thankYouLabel.numberOfLines = 0;
    _thankYouLabel.lineBreakMode = UILineBreakModeWordWrap;
    
    // Add by iNot 22 July 2013
    // Captcha
    _captchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _captchaLabel.textColor = kDefaultTitleFontGrayColor;
    NSString *text = [NSString stringWithFormat:@"%@*",[[Language get:@"captcha_code" alter:nil] uppercaseString]];
    _captchaLabel.text = text;
    
    _recaptchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _recaptchaLabel.textColor = kDefaultPurpleColor;
    _recaptchaLabel.text = [Language get:@"recaptcha" alter:nil];
    [_recaptchaLabel setShouldUnderline:YES];
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(refreshCaptcha)];
    [_recaptchaLabel setUserInteractionEnabled:YES];
    [_recaptchaLabel addGestureRecognizer:tapGesture3];
    [tapGesture3 release];
    
    _captchaTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _captchaTF.textColor = kDefaultPurpleColor;
    //
    
    //--------------//
    // Setup Button //
    //--------------//
    // Submit Button
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    
    UIButton *button = createButtonWithFrame(CGRectMake(_captchaTF.frame.origin.x,
                                                        _captchaTF.frame.origin.y + _captchaTF.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             
                                             [Language get:@"submit" alter:nil],
                                             buttonFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
    
    [button addTarget:self action:@selector(performTransfer) forControlEvents:UIControlEventTouchUpInside];
    
    _transferButton = button;
    [_contentView addSubview:_transferButton];
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _transferButton.frame.origin.y + _transferButton.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40) withLabel:@"TRANSFER PAKET"];
    _sectionHeaderView = headerView;
    [_contentView addSubview:_sectionHeaderView];
    [_contentView sendSubviewToBack:_sectionHeaderView];
    [headerView release];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 200.0);
    
    _sectionHeaderView.icon = self.headerIconMaster;
    _sectionHeaderView.title = self.headerTitleMaster;
    
    _packageValueLabel.text = _giftPackageModel.menuName;
    BOOL isNumber = isAllDigits(_giftPackageModel.price);
    if (isNumber) {
        DataManager *sharedData = [DataManager sharedInstance];
        _priceValueLabel.text = addThousandsSeparator(_giftPackageModel.price, sharedData.profileData.language);
    }
    else {
        _priceValueLabel.text = _giftPackageModel.price;
    }
    
    if (isThankYouPage) {
        _transferToValueLabel.hidden = NO;
        _transferToValueLabel.text = self.toNumber;
        
        _captchaLabel.hidden = YES;
        _captchaWebView.hidden = YES;
        _recaptchaLabel.hidden = YES;
        _captchaTF.hidden = YES;
        
        _thankYouLabel.hidden = NO;
        
        _thankYouLabel.text = [Language get:@"buy_package_thank_you" alter:nil];
        
        /*
        _thankYouLabel.text = [NSString stringWithFormat:@"%@ %@ %@",
                              [Language get:@"gift_thank_you_1" alter:nil],
                              self.toNumber,
                              [Language get:@"gift_thank_you_2" alter:nil]];*/
        
        CGSize size = calculateExpectedSize(_thankYouLabel, _thankYouLabel.text);
        CGRect frame = [_thankYouLabel frame];
        frame.size.height = size.height;
        _thankYouLabel.frame = frame;
        
        _transferToTextField.hidden = YES;
        _transferButton.hidden = YES;
        
        //-----------------------//
        // Relayout Content View //
        //-----------------------//
        frame = [_contentView frame];
        frame.size.height = _thankYouLabel.frame.origin.y + _thankYouLabel.frame.size.height + kDefaultPadding;
        [_contentView setFrame:frame];
        
        NSArray *viewsToRemove = [_contentView subviews];
        for (UIView *v in viewsToRemove) {
            if ([v isKindOfClass:[ContentView class]]) {
                [v removeFromSuperview];
            }
        }
        
        ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
        [_contentView addSubview:contentViewTmp];
        [_contentView sendSubviewToBack:contentViewTmp];
        [contentViewTmp release];
        
        //-------------------------------//
        // Relayout Grey Background View //
        //-------------------------------//
        CGFloat x = _contentView.frame.origin.x - kDefaultLeftPadding;
        CGFloat y = _contentView.frame.origin.y - kDefaultPadding;
        CGFloat width = _contentView.frame.size.width + (kDefaultLeftPadding*2);
        CGFloat height = _contentView.frame.size.height + (kDefaultPadding*2);
        _greyBackgroundView.frame = CGRectMake(x, y, width, height);
        
        _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 200.0);
    }
    else {
        _transferToValueLabel.hidden = YES;
        _thankYouLabel.hidden = YES;
        _transferToTextField.hidden = NO;
        _transferButton.hidden = NO;
        
        _captchaLabel.hidden = NO;
        _captchaWebView.hidden = NO;
        _recaptchaLabel.hidden = NO;
        _captchaTF.hidden = NO;
        
        // Request Captcha
        [self refreshCaptcha];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.giftPackageModel = nil;
}

- (void)dealloc
{
    [toNumber release];
    
    [_giftPackageModel release];
    _giftPackageModel = nil;
    
    [super dealloc];
}

#pragma mark - Selector

- (void)performTransfer {
    [_transferToTextField resignFirstResponder];
    [_captchaTF resignFirstResponder];
    
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *price = [NSString stringWithFormat:@"%@",addThousandsSeparator(_giftPackageModel.price, sharedData.profileData.language)];
    NSString *confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@. \n %@",
                                     [Language get:@"buy_package_confirmation_1" alter:nil],
                                     price,
                                     [Language get:@"buy_package_confirmation_2" alter:nil],
                                     _giftPackageModel.groupName,
                                     _giftPackageModel.menuName,
                                     [Language get:@"buy_package_confirmation_3" alter:nil],
                                     _giftPackageModel.duration,
                                     [Language get:@"ask_continue" alter:nil]];
    
    UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                              message:confirmationMessage
                                                             delegate:self
                                                    cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                    otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
    
    buyPackageAlert.tag = 2;
    
    [buyPackageAlert show];
    [buyPackageAlert release];
}

- (void)submitTransfer {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = GIFT_PACKAGE_REQ;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    [request giftPackage:_transferToTextField.text
             withPackage:_giftPackageModel.packageId
             withCaptcha:_captchaTF.text
                 withCid:sharedData.cid];
}

- (void)refreshCaptcha {
    NSString *captchaURL = generateCaptchaURL();
    NSURL *captcha = [NSURL URLWithString:captchaURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:captcha];
    [_captchaWebView loadRequest:request];
    [request release];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == GIFT_PACKAGE_REQ) {
            GiftPackageViewController *giftPackageVC = [[GiftPackageViewController alloc] initWithNibName:@"GiftPackageViewController" bundle:nil];
            giftPackageVC.giftPackageModel = self.giftPackageModel;
            giftPackageVC.toNumber = _transferToTextField.text;
            giftPackageVC.isThankYouPage = YES;
            
            giftPackageVC.headerTitleMaster = self.headerTitleMaster;
            giftPackageVC.headerIconMaster = self.headerIconMaster;
            
            [self.navigationController pushViewController:giftPackageVC animated:YES];
            giftPackageVC = nil;
            [giftPackageVC release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:[[Language get:@"ok" alter:nil] uppercaseString]
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    //[_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    //CGFloat targetY = -_transferToTextField.frame.size.height + textField.frame.origin.y;
    CGFloat targetY = -72 + textField.frame.origin.y;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 16);
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // tag == 2 Gift Package
    if (alertView.tag == 2) {
        if (buttonIndex == 0) {
            //NSLog(@"YES");
            [self submitTransfer];
        }
        else {
            //NSLog(@"NO");
        }
    }
    
}

@end
