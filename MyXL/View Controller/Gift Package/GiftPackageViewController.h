//
//  GiftPackageViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 2/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "MenuModel.h"
#import "AXISnetRequest.h"

@interface GiftPackageViewController : BasicViewController <UITextFieldDelegate, AXISnetRequestDelegate> {
    BOOL isThankYouPage;
    NSString *toNumber;
    
    MenuModel *_giftPackageModel;
}

@property (readwrite) BOOL isThankYouPage;
@property (nonatomic, retain) NSString *toNumber;

@property (nonatomic, retain) MenuModel *giftPackageModel;

@end
