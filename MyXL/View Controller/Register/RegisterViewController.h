//
//  RegisterViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/23/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AXISnetRequest.h"

@interface RegisterViewController : BasicViewController <UITextFieldDelegate, AXISnetRequestDelegate>

@end
