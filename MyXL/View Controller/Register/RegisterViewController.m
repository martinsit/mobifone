//
//  RegisterViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/23/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "RegisterViewController.h"

#import "Constant.h"
#import "AXISnetRequest.h"
//#import "ContentView.h"
#import "AXISnetCommon.h"
#import <QuartzCore/QuartzCore.h>
#import "UnderLineLabel.h"
#import "DataManager.h"

#import "ForgotPasswordViewController.h"
#import "AccountActivationViewController.h"

#import "ActionSheetStringPicker.h"

#import "AXISnetNavigationBar.h"
#import "SectionHeaderView.h"

#import "AppDelegate.h"

@interface RegisterViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) IBOutlet UILabel *asteriskLabel;

@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;

@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;

@property (strong, nonatomic) IBOutlet UILabel *confirmPasswordLabel;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;

@property (strong, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (strong, nonatomic) IBOutlet UITextField *nickNameTextField;

@property (strong, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;

@property (strong, nonatomic) IBOutlet UILabel *languageLabel;
@property (strong, nonatomic) IBOutlet UIButton *languageButton;

@property (strong, nonatomic) IBOutlet UILabel *captchaLabel;
@property (strong, nonatomic) IBOutlet UIWebView *captchaWebView;

@property (strong, nonatomic) IBOutlet UILabel *refreshLabel;
@property (strong, nonatomic) IBOutlet UnderLineLabel *recaptchaLabel;
@property (strong, nonatomic) IBOutlet UITextField *captchaTextField;

@property (strong, nonatomic) UIButton *submitButton;
@property (strong, nonatomic) UIButton *loginButton;
@property (strong, nonatomic) UIButton *forgotPasswordButton;
@property (strong, nonatomic) UIButton *accountActivationButton;

@property (strong, nonatomic) UIActionSheet *langActionSheet;
@property (strong, nonatomic) UIPickerView *langPickerView;
@property (strong, nonatomic) NSArray *lang;
@property (readwrite) int rowIndex;
@property (strong, nonatomic) NSString *selectedLang;

- (void)performSubmit:(id)sender;
- (void)performLogin:(id)sender;
- (void)performForgotPassword:(id)sender;
- (void)performAccountActivation:(id)sender;
- (void)submit;
- (IBAction)performLanguage:(id)sender;
- (void)refreshCaptcha;

- (void)langWasSelected:(NSNumber *)selectedIndex element:(id)element;

@end

@implementation RegisterViewController

@synthesize langActionSheet;
@synthesize langPickerView;
@synthesize lang;
@synthesize rowIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    //---------------------//
    // Show Navigation Bar //
    //---------------------//
    
    AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[self.navigationController navigationBar];
    navBar.hidden = NO;
    
    // Change NavBar BgColor
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    changeNavbarBg(navBar, appDelegate.window.bounds);
    //----------------------
    
    NSString *documentsDirectory = applicationDocumentsDirectory();
    NSString *logoName = @"";
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else if ([sharedData.profileData.telcoOperator isEqualToString:@"AXIS"]) {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    else {
        logoName = XL_LOGO_RETINA_NAME;
    }
    NSString *mediaPath = [documentsDirectory stringByAppendingPathComponent:logoName];
    [navBar updateLogo:mediaPath];
    //
    
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:[[Language get:@"my_account" alter:nil] uppercaseString]
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:[[Language get:@"registration" alter:nil] uppercaseString]
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
//    _titleLabel.font = [UIFont fontWithName:kDefaultFontBold size:24.0];
//    _titleLabel.textColor = kDefaultFontLightGrayColor;
//    _titleLabel.text = [[Language get:@"registration" alter:nil] uppercaseString];
    
    _asteriskLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _asteriskLabel.textColor = kDefaultNavyBlueColor;
    _asteriskLabel.text = [Language get:@"asterisk" alter:nil];
    
    _usernameLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _usernameLabel.textColor = kDefaultTitleFontGrayColor;
    NSString *text = [NSString stringWithFormat:@"%@*",[Language get:@"your_axis_number" alter:nil]];
    _usernameLabel.text = text;
    
    _passwordLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _passwordLabel.textColor = kDefaultTitleFontGrayColor;
    text = [NSString stringWithFormat:@"%@*",[Language get:@"password" alter:nil]];
    _passwordLabel.text = text;
    
    _confirmPasswordLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _confirmPasswordLabel.textColor = kDefaultTitleFontGrayColor;
    text = [NSString stringWithFormat:@"%@*",[Language get:@"confirm_password" alter:nil]];
    _confirmPasswordLabel.text = text;
    
    _nameLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _nameLabel.textColor = kDefaultTitleFontGrayColor;
    _nameLabel.text = [Language get:@"full_name" alter:nil];
    
    _nickNameLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _nickNameLabel.textColor = kDefaultTitleFontGrayColor;
    _nickNameLabel.text = [Language get:@"nickname" alter:nil];
    
    _emailLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _emailLabel.textColor = kDefaultTitleFontGrayColor;
    _emailLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"email" alter:nil]];
    
    _languageLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _languageLabel.textColor = kDefaultTitleFontGrayColor;
    text = [NSString stringWithFormat:@"%@*",[Language get:@"language_selection" alter:nil]];
    _languageLabel.text = text;
    
    _captchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _captchaLabel.textColor = kDefaultTitleFontGrayColor;
    text = [NSString stringWithFormat:@"%@*",[Language get:@"captcha_code" alter:nil]];
    _captchaLabel.text = text;
    
    // Refresh Label
    _refreshLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize];
    _refreshLabel.textColor = kDefaultNavyBlueColor;
    
    _recaptchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _recaptchaLabel.textColor = kDefaultNavyBlueColor;
    _recaptchaLabel.text = [Language get:@"recaptcha" alter:nil];
    [_recaptchaLabel setShouldUnderline:NO];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(refreshCaptcha)];
    [_recaptchaLabel setUserInteractionEnabled:YES];
    [_recaptchaLabel addGestureRecognizer:tapGesture];
    [tapGesture release];
    
    //------------------//
    // Setup Text Field //
    //------------------//
    _usernameTextField.delegate = self;
    
    _passwordTextField.delegate = self;
    
    _confirmPasswordTextField.delegate = self;
    
    _nameTextField.delegate = self;
    
    _nickNameTextField.delegate = self;
    
    _emailTextField.delegate = self;
    
    _captchaTextField.delegate = self;
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    newFrame.size.height = _captchaTextField.frame.origin.y + _captchaTextField.frame.size.height;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Setup Button //
    //--------------//
    
    // Submit Button
    UIFont *titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    UIButton *button = createButtonWithFrame(CGRectMake(_captchaTextField.frame.origin.x,
                                                        _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             
                                             [[Language get:@"register" alter:nil] uppercaseString],
                                             titleFont,
                                             kDefaultWhiteColor,
                                             kDefaultAquaMarineColor,
                                             kDefaultBlueColor);
    
    [button addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    _submitButton = button;
    [_contentView addSubview:_submitButton];
    
    // Login Button
    CGFloat padding = 10.0;
    CGFloat contentWidth = _contentView.frame.size.width - (padding*2);
    
    button = createButtonWithIconAndArrow(CGRectMake(padding, _submitButton.frame.origin.y + _submitButton.frame.size.height + 20.0, contentWidth, kDefaultButtonHeight),
                                          @"F",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          [[Language get:@"login" alter:nil] uppercaseString],
                                          [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          @"d",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          kDefaultAquaMarineColor,
                                          kDefaultBlueColor);
    
    [button addTarget:self
               action:@selector(performLogin:)
     forControlEvents:UIControlEventTouchDown];
    
    _loginButton = button;
    [_contentView addSubview:_loginButton];
    
    // Forgot Password
    button = createButtonWithIconAndArrow(CGRectMake(padding, _loginButton.frame.origin.y + _loginButton.frame.size.height + 4.0, contentWidth, kDefaultButtonHeight),
                                          @"G",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          [[Language get:@"forget_password" alter:nil] uppercaseString],
                                          [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          @"d",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          kDefaultAquaMarineColor,
                                          kDefaultBlueColor);
    
    [button addTarget:self
               action:@selector(performForgotPassword:)
     forControlEvents:UIControlEventTouchDown];
    
    _forgotPasswordButton = button;
    [_contentView addSubview:_forgotPasswordButton];
    
    // Account Activation
    button = createButtonWithIconAndArrow(CGRectMake(padding, _forgotPasswordButton.frame.origin.y + _forgotPasswordButton.frame.size.height + 4.0, contentWidth, kDefaultButtonHeight),
                                          @"C",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          [[Language get:@"account_activation" alter:nil] uppercaseString],
                                          [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          @"d",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          kDefaultAquaMarineColor,
                                          kDefaultBlueColor);
    
    [button addTarget:self
               action:@selector(performAccountActivation:)
     forControlEvents:UIControlEventTouchDown];
    
    _accountActivationButton = button;
    [_contentView addSubview:_accountActivationButton];
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultWhiteColor;
    newFrame = _contentView.frame;
    newFrame.size.height = _accountActivationButton.frame.origin.y + _accountActivationButton.frame.size.height + kDefaultComponentPadding + 44 + 10;
    _contentView.frame = newFrame;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 50.0);
    
    [headerView1 release];
    [headerView2 release];
    
    // Button Lang
    self.lang = [NSArray arrayWithObjects:@"Bahasa Indonesia",
                 @"English",
                 nil];
    rowIndex = 0;
    if (rowIndex == 0) {
        self.selectedLang = @"id";
    }
    else {
        self.selectedLang = @"en";
    }
    //
    UIImage *backgroundImage = [[UIImage imageNamed:@"combo_bg_1.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0];
    [_languageButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    //
    
    [_languageButton setTitle:[self.lang objectAtIndex:rowIndex] forState:UIControlStateNormal];
    [_languageButton setTitleColor:kDefaultPurpleColor forState:UIControlStateNormal];
    _languageButton.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _languageButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _languageButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    // Captcha
    [self refreshCaptcha];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [langActionSheet release];
    [langPickerView release];
    [lang release];
    [super dealloc];
}

#pragma mark - Selector

- (int)validateInput {
    if ([_usernameTextField.text length] <= 0 ||
        [_passwordTextField.text length] <= 0 ||
        [_confirmPasswordTextField.text length] <= 0 ||
        [_emailTextField.text length] <= 0 ||
        [_captchaTextField.text length] <= 0) {
        return 2;
    }
    else if ([_passwordTextField.text length] < 5) {
        return 1;
    }
    else if ([_passwordTextField.text length] > 8) {
        return 1;
    }
    else if ([_confirmPasswordTextField.text length] < 5) {
        return 1;
    }
    else if ([_confirmPasswordTextField.text length] > 8) {
        return 1;
    }
    else if ([_passwordTextField.text isEqualToString:_confirmPasswordTextField.text] == NO) {
        return 4;
    }
    else if (isValidEmail(_emailTextField.text) == NO) {
        return 3;
    }
    else {
        return 0;
    }
}

- (void)performSubmit:(id)sender {
    int valid = [self validateInput];
    if (valid == 2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"empty_validation_registration" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
    else if (valid == 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"password_length" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
    else if (valid == 4) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"confirm_password_not_match" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
    else if (valid == 3) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"email_validation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
    else {
        [self submit];
    }
}

- (void)performLogin:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)performForgotPassword:(id)sender {
    ForgotPasswordViewController *forgotPasswordViewController = [[ForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:forgotPasswordViewController animated:YES];
    forgotPasswordViewController = nil;
    [forgotPasswordViewController release];
}

- (void)performAccountActivation:(id)sender {
    AccountActivationViewController *accountActivationViewController = [[AccountActivationViewController alloc] initWithNibName:@"AccountActivationViewController" bundle:nil];
    [self.navigationController pushViewController:accountActivationViewController animated:YES];
    accountActivationViewController = nil;
    [accountActivationViewController release];
}

- (void)submit {
    
    [_usernameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_confirmPasswordTextField resignFirstResponder];
    [_nameTextField resignFirstResponder];
    [_nickNameTextField resignFirstResponder];
    [_emailTextField resignFirstResponder];
    [_captchaTextField resignFirstResponder];
    
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = REGISTER_REQ;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    int currentLang = [Language getCurrentLanguage];
    NSString *activeLang = @"";
    if (currentLang == 0) {
        activeLang = @"en";
    }
    else {
        activeLang = @"id";
    }
    
    [request registerWithUsername:_nickNameTextField.text
                     withPassword:_passwordTextField.text
                       withMsisdn:_usernameTextField.text
                      withCaptcha:_captchaTextField.text
                          withCid:sharedData.cid
                     withFullname:_nameTextField.text
                        withEmail:_emailTextField.text
                     withLanguage:self.selectedLang
                  withCurrentLang:activeLang];
}

- (IBAction)performLanguage:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:@""
                                            rows:self.lang
                                initialSelection:rowIndex
                                          target:self
                                   successAction:@selector(langWasSelected:element:)
                                    cancelAction:@selector(actionPickerCancelled:)
                                          origin:sender];
    
    /*
    UIActionSheet *langActionSheetTmp = [[UIActionSheet alloc] initWithTitle:@""
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                      destructiveButtonTitle:nil
                                                           otherButtonTitles:nil];
    
    UIPickerView *thePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
    thePickerView.showsSelectionIndicator = TRUE;
    self.langPickerView = thePickerView;
    langPickerView.delegate = self;
    [langPickerView selectRow:rowIndex inComponent:0 animated:YES];
    [thePickerView release];
    
    UIToolbar *pickerToolbar;
    pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                               target:self
                                                                               action:@selector(actionCancel:)];
    [barItems addObject:cancelBtn];
    [cancelBtn release];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                               target:self
                                                                               action:nil];
    [barItems addObject:flexSpace];
    [flexSpace release];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                             target:self
                                                                             action:@selector(actionDone:)];
    [barItems addObject:doneBtn];
    [doneBtn release];
    [pickerToolbar setItems:barItems animated:YES];
    [barItems release];
    [langActionSheetTmp addSubview:pickerToolbar];
    [langActionSheetTmp addSubview:langPickerView];
    [pickerToolbar release];
    
    self.langActionSheet = langActionSheetTmp;
    [langActionSheetTmp release];
    
    [langActionSheet showInView:self.view];
    [langActionSheet setBounds:CGRectMake(0, 0, 320, 464)];*/
    
    
}

- (void)actionCancel:(id)sender {
    [langActionSheet dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)actionDone:(id)sender {
	[langActionSheet dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)refreshCaptcha {
    NSString *captchaURL = generateCaptchaURL();
    NSURL *captcha = [NSURL URLWithString:captchaURL];
    //NSLog(@"--> captcha = %@",captcha);
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:captcha];
    [_captchaWebView loadRequest:request];
    [request release];
}

#pragma mark - Implementation ActionSheetPicker.h

- (void)langWasSelected:(NSNumber *)selectedIndex element:(id)element {
    rowIndex = [selectedIndex intValue];
    if (rowIndex == 0) {
        self.selectedLang = @"id";
    }
    else {
        self.selectedLang = @"en";
    }
    
    [_languageButton setTitle:[self.lang objectAtIndex:rowIndex] forState:UIControlStateNormal];
}

- (void)actionPickerCancelled:(id)sender {
    //NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}
/*
#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	rowIndex = row;
    [_languageButton setTitle:[self.lang objectAtIndex:rowIndex] forState:UIControlStateNormal];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.lang objectAtIndex:row];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return [self.lang count];
}*/

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat targetY = -42 + textField.frame.origin.y;
    
    if (textField == _emailTextField) {
        targetY = textField.frame.origin.y + 80.0;
    }
    if (textField == _captchaTextField) {
        //targetY = -80 + textField.frame.origin.y;
        targetY = textField.frame.origin.y + 20.0;
    }
    
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == _passwordTextField ||
        textField == _confirmPasswordTextField) {
        return !([newString length] > 8);
    }
    else {
        return !([newString length] > 30);
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    
    NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
    int type = [typeNumber intValue];
    
    BOOL success = [reqResult boolValue];
    
    if (success) {
        if (type == REGISTER_REQ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"registration" alter:nil]
                                                            message:[Language get:@"register_success" alter:nil]
                                                           delegate:self
                                                  cancelButtonTitle:[[Language get:@"ok" alter:nil] uppercaseString]
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
            [self performAccountActivation:nil];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
}

@end
