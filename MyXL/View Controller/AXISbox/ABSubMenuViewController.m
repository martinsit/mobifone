//
//  ABSubMenuViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ABSubMenuViewController.h"
#import "Constant.h"
#import "SectionFooterView.h"
#import "SectionHeaderView.h"
//#import "MyPackageCell.h"
#import "CommonCell.h"
#import "BorderCellBackground.h"
#import "AXISnetCellBackground.h"
#import "AXISnetCommon.h"

#import "MenuModel.h"
#import "DataManager.h"
#import "GroupingAndSorting.h"

#import "ABContactsViewController.h"
#import "ABImportPhotosViewController.h"
#import "ABUploadPhotoOperation.h"
#import "ABUploadContactOperation.h"

#import "ABFileModel.h"
#import "ABExportPhotosViewController.h"

#import "ELCAlbumPickerController.h"

@interface ABSubMenuViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (nonatomic, retain) NSArray *sortedMenu;
@property (nonatomic, retain) NSDictionary *theSourceMenu;

@property (readwrite) BOOL cameraOrGalleryIsActive;
@property (nonatomic, retain) NSOperationQueue *operationQueue;

@property (nonatomic, retain) CLLocationManager *locationManager;

- (void)performExportContacts;
- (void)performImportContacts;

- (void)performExportPhotos;
- (void)performImportPhotos;

- (void)launchCamera;

@end

@implementation ABSubMenuViewController

@synthesize parentId = _parentId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)viewWillAppear:(BOOL)animated {
    DataManager *dataManager = [DataManager sharedInstance];
    NSArray *contentTmp = [dataManager.menuData valueForKey:_parentId];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    self.sortedMenu = [sortingResult allKeys];
    self.sortedMenu = [GroupingAndSorting doSortingByGroupId:self.sortedMenu];
    self.theSourceMenu = sortingResult;
    
    [_theTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_parentId release];
    _parentId = nil;
    
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sortedMenu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section]] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifierCommon = @"Common";
    
    CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierCommon];
    
    if (cell == nil) {
        cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifierCommon];
        
        cell.backgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
        cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
        ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
    }
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
    
    cell.iconLabel.text = icon(menuModel.menuId);
    if ([cell.iconLabel.text length] <= 0) {
        cell.iconLabel.text = icon(@"default_icon");
    }
    
    cell.titleLabel.text = [menuModel.menuName uppercaseString];
    if ([menuModel.desc length] > 0) {
        cell.arrowLabel.hidden = YES;
        cell.infoButton.hidden = NO;
    }
    else {
        cell.arrowLabel.hidden = NO;
        cell.infoButton.hidden = YES;
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    MenuModel *menuModel = [objectsForMenu objectAtIndex:0];
    return menuModel.groupName;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kDefaultCellHeight;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
    
    if ([menuModel.href isEqualToString:@"#backup_contacts"]) {
        [self performExportContacts];
    }
    else if ([menuModel.href isEqualToString:@"#restore_contacts"]) {
        [self performImportContacts];
    }
    else if ([menuModel.href isEqualToString:@"#export_photos"]) {
        [self performExportPhotos];
    }
    else if ([menuModel.href isEqualToString:@"#import_photos"]) {
        
        /*
        NSString *firstTitle = [[self.sectionMenu objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        NSString *lastTitle = [self.groups objectAtIndex:indexPath.row];
        self.nextViewSectionTitle = [NSString stringWithFormat:@"%@ %@",firstTitle,lastTitle];*/
        
        [self performImportPhotos];
    }
    else {
        //NSLog(@"Do Nothing");
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    /*
     SectionHeaderView *headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40.0)] autorelease];
     return headerView;*/
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu objectAtIndex:0];
    
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:icon([NSString stringWithFormat:@"gid_%@",menuModel.groupId])
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:icon([NSString stringWithFormat:@"gid_%@",menuModel.groupId])
                                                isFirstSection:NO] autorelease];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

#pragma mark - Selector

- (void)performExportContacts {
    ABContactsViewController *detailViewController = [[ABContactsViewController alloc] initWithNibName:@"ABContactsViewController" bundle:nil];
    detailViewController.isImport = NO;
    [self.navigationController pushViewController:detailViewController animated:YES];
    detailViewController = nil;
    [detailViewController release];
}

- (void)performImportContacts {
    ABContactsViewController *detailViewController = [[ABContactsViewController alloc] initWithNibName:@"ABContactsViewController" bundle:nil];
    detailViewController.isImport = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    detailViewController = nil;
    [detailViewController release];
}

- (void)performExportPhotos {
    //NSLog(@"export photo");
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:[Language get:@"export_photos" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"cancel" alter:nil]
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:[Language get:@"camera" alter:nil], [Language get:@"photo_gallery" alter:nil], nil];
    // Show the sheet
    [sheet showInView:self.view];
    [sheet release];
}

- (void)performImportPhotos {
    //NSLog(@"import photo");
    ABImportPhotosViewController *importPhotosViewController = [[ABImportPhotosViewController alloc] initWithNibName:@"ABImportPhotosViewController" bundle:nil];
    
    //importPhotosViewController.fromShowPhoto = NO;
    //importPhotosViewController.aSectionTitle = self.nextViewSectionTitle;
    
    [self.navigationController pushViewController:importPhotosViewController animated:YES];
    importPhotosViewController = nil;
    [importPhotosViewController release];
}

- (void)launchCamera {
    _cameraOrGalleryIsActive = YES;
    
    // Hide Pad
    //appDelegate.pad.hidden = YES;
    
    // Create image picker controller
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    // Set source to the camera
    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    
    // Delegate is self
    imagePicker.delegate = self;
    
    // Allow editing of image ?
    //imagePicker.allowsImageEditing = NO;
    
    // Show image picker
    [self presentModalViewController:imagePicker animated:YES];
}

- (void)launchPhotoLibrary {
    _cameraOrGalleryIsActive = YES;
    
    ELCAlbumPickerController *albumController = [[ELCAlbumPickerController alloc] initWithNibName:@"ELCAlbumPickerController" bundle:[NSBundle mainBundle]];
	ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initWithRootViewController:albumController];
    [albumController setParent:elcPicker];
	[elcPicker setDelegate:self];
    
    [self presentModalViewController:elcPicker animated:YES];
    
    [elcPicker release];
    [albumController release];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    //NSLog(@"ini pilihan export photo");
    if (buttonIndex == 0) {
        //NSLog(@"Camera");
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == YES) {
            //NSLog(@"Camera is available and ready");
            [self launchCamera];
        }
        else {
            //NSLog(@"Camera is not available");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera"
                                                            message:@"Camera is not available"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
    else if (buttonIndex == 1) {
        
        
        //NSLog(@"Photo Lib");
        if ([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)
        {
            //Do ALAssetLibrary work here...
            [self launchPhotoLibrary];
        }
        else
        {
            
//             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:
//             NSLocalizedString( @"Please Enable Location Service's", @"Please Enable Location Service's" )
//             message: NSLocalizedString( @"Message", @"Message" )
//             delegate: self
//             cancelButtonTitle: NSLocalizedString( @"Close", @"Close" )
//             otherButtonTitles: nil];
//             
//             [alert show];
//             [alert release];
            
            // Start the location manager.
            [[self locationManager] startUpdatingLocation];
        }
    }
    else {
        //NSLog(@"nothing");
    }
}

#pragma mark - UIImagePickerCallback

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // Access the uncropped image from info dictionary
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    //NSLog(@"Original image width: %f and height: %f", image.size.width, image.size.height);
    
    //UIImage* editedImage = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    //NSLog(@"Edited image width: %f and height: %f", editedImage.size.width, editedImage.size.height);
    
    // Save image
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
    [picker release];
    
    [self dismissModalViewControllerAnimated:YES];
    // Show Pad
    //appDelegate.pad.hidden = NO;
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    UIAlertView *alert;
    
    // Unable to save the image
    if (error) {
        alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                           message:@"Unable to save image to Photo Album."
                                          delegate:self cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    else {
        /*
         alert = [[UIAlertView alloc] initWithTitle:@"Success"
         message:@"Image saved to Photo Album."
         delegate:self cancelButtonTitle:@"Ok"
         otherButtonTitles:nil];*/
        // Save to Doc App
        //NSString *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/axisboxcam.jpg"];
        //[UIImageJPEGRepresentation(image, 1.0) writeToFile:jpgPath atomically:YES];
        
        //NSArray *photoCam = [NSArray arrayWithObject:image];
        
        self.hud.mode = MBProgressHUDModeDeterminate;
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        self.hud.progress = 0.0f;
        
        [UIApplication sharedApplication].idleTimerDisabled = YES;
        
        //NSLog(@"Original image width: %f and height: %f", image.size.width, image.size.height);
        /*
         NSData *imgDataOri = UIImageJPEGRepresentation(image, 1);
         NSUInteger imgOriLength = [imgDataOri length];
         NSNumber *oriFileSize = [NSNumber numberWithUnsignedInteger:imgOriLength];
         NSString *oriFileSizeStr = [HumanReadableDataSizeHelper humanReadableSizeFromBytes:oriFileSize
         useSiPrefixes:YES
         useSiMultiplier:NO];
         NSLog(@"oriFileSizeStr = %@",oriFileSizeStr);*/
        
        // resize image
        CGFloat imgWidth = image.size.width;
        CGFloat imgHeight = image.size.height;
        CGFloat targetWidth;
        CGFloat targetHeight;
        if (imgWidth > imgHeight) {
            targetWidth = 1024;
            targetHeight = 768;
        }
        else {
            targetWidth = 768;
            targetHeight = 1024;
        }
        CGSize itemSize = CGSizeMake(targetWidth, targetHeight);
        UIImage *imageNewSize = [self imageWithImage:image scaledToSize:itemSize];
        
        //UIImage *imageNewSize = [UIImage imageWithCGImage:image.CGImage scale:0.5 orientation:image.imageOrientation];
        /*
         NSLog(@"New image width: %f and height: %f", imageNewSize.size.width, imageNewSize.size.height);
         NSData *imgDataKw = UIImageJPEGRepresentation(imageNewSize, 1);
         NSUInteger imgKwLength = [imgDataKw length];
         NSNumber *kwFileSize = [NSNumber numberWithUnsignedInteger:imgKwLength];
         NSString *kwFileSizeStr = [HumanReadableDataSizeHelper humanReadableSizeFromBytes:kwFileSize
         useSiPrefixes:YES
         useSiMultiplier:NO];
         NSLog(@"kwFileSizeStr = %@",kwFileSizeStr);*/
        
        /*
         ABPhotoController *aPhotoController = [[ABPhotoController alloc] init];
         self.photoController = aPhotoController;
         [self.photoController exportPhotos:photoCam
         isFromCamera:YES
         withDelegate:self
         withSelector:@selector(exportPhotoCallback:withInfo:)];
         [aPhotoController release];*/
        
        // ---------------
        // using operation
        // ---------------
        
        // Create operation queue
        _operationQueue = [NSOperationQueue new];
        // set maximum operations possible
        //[operationQueue setMaxConcurrentOperationCount:1];
        
        NSString *photoFileName = [NSString stringWithFormat:@"Camera_%@.jpg",generateUniqueString()];
        ABUploadPhotoOperation *uploadOperation = [[ABUploadPhotoOperation alloc] initWithPhoto:imageNewSize
                                                                                   withFileName:photoFileName];
        [uploadOperation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
        [uploadOperation addObserver:self forKeyPath:@"isExecuting" options:NSKeyValueObservingOptionNew context:NULL];
        [_operationQueue addOperation:uploadOperation]; // operation starts as soon as its added
        [uploadOperation release];
    }
}

- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark - ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info {
	
	[self dismissModalViewControllerAnimated:YES];
    // Show Pad
    //appDelegate.pad.hidden = NO;
    
    /*
     for(NSDictionary *dict in info) {
     
     //         UIImage *viewImage = [dict objectForKey:UIImagePickerControllerOriginalImage];
     //         ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
     //         // Request to save the image to camera roll
     //         [library writeImageToSavedPhotosAlbum:[viewImage CGImage]
     //         orientation:(ALAssetOrientation)[viewImage imageOrientation]
     //         completionBlock:^(NSURL *assetURL, NSError *error){
     //         if (error) {
     //         NSLog(@"error");
     //         } else {
     //         NSLog(@"url %@", assetURL);
     //         }
     //         }];
     //         [library release];
     
     //-NSLog(@"UIImagePickerControllerReferenceURL = %@",[dict objectForKey:UIImagePickerControllerReferenceURL]);
     //-NSLog(@"originalFileName = %@",[dict objectForKey:@"originalFileName"]);
     
     }*/
    
    //-- tutup sementara
    ABExportPhotosViewController *exportPhotoViewController = [[ABExportPhotosViewController alloc] initWithNibName:@"ABExportPhotosViewController" bundle:nil];
    exportPhotoViewController.exportPhotoData = info;
    
    NSMutableArray *tempData = [[NSMutableArray alloc] init];
    for (int i=0; i<[info count]; i++) {
        ABFileModel *aFileData = [[ABFileModel alloc] init];
        aFileData.exportIndex = i;
        aFileData.isExecuting = NO;
        aFileData.isFinished = NO;
        aFileData.isSuccess = NO;
        
        [tempData addObject:aFileData];
        
        [aFileData release];
        aFileData = nil;
    }
    
    exportPhotoViewController.progressData = [NSArray arrayWithArray:tempData];
    [tempData release];
    
    //exportPhotoViewController.aSubMenuVC = self;
    [self.navigationController pushViewController:exportPhotoViewController animated:YES];
    //[appDelegate.navcontroller pushViewController:exportPhotoViewController animated:YES];
    [exportPhotoViewController release];
	
    /*
     for (UIView *v in [scrollview subviews]) {
     [v removeFromSuperview];
     }
     
     CGRect workingFrame = scrollview.frame;
     workingFrame.origin.x = 0;
     
     for(NSDictionary *dict in info) {
     
     UIImageView *imageview = [[UIImageView alloc] initWithImage:[dict objectForKey:UIImagePickerControllerOriginalImage]];
     [imageview setContentMode:UIViewContentModeScaleAspectFit];
     imageview.frame = workingFrame;
     
     [scrollview addSubview:imageview];
     [imageview release];
     
     workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
     }
     
     [scrollview setPagingEnabled:YES];
     [scrollview setContentSize:CGSizeMake(workingFrame.origin.x, workingFrame.size.height)];*/
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker {
	[self dismissModalViewControllerAnimated:YES];
    // Show Pad
    //appDelegate.pad.hidden = NO;
}

#pragma mark - KVO Observing

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)operation
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if ([operation isKindOfClass:[ABUploadContactOperation class]]) {
        //NSLog(@"Haha berhasil");
    }
    else {
        /*
         [self.aHUD hide:YES];
         [self.aHUD removeFromSuperview];
         //[HUD release];
         
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self.lu translate:LN_DB_EXPORT_PHOTO_TITLE]
         message:[self.lu translate:LN_SUCCESS]
         delegate:self
         cancelButtonTitle:@"OK"
         otherButtonTitles:nil];
         [alert show];
         [alert release];*/
        
        ABUploadPhotoOperation *uploadOperation = (ABUploadPhotoOperation *)operation;
        if ([keyPath isEqualToString:@"isExecuting"]) {
            CGFloat currentProgress = [uploadOperation theProgress];
            self.hud.progress = currentProgress;
        }
        else {
            [UIApplication sharedApplication].idleTimerDisabled = NO;
            
            //[self.aHUD hide:YES];
            //[self.aHUD removeFromSuperview];
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            BOOL success = [uploadOperation success];
            if (success) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"EXPORT PHOTO"
                                                                message:@"Success"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"EXPORT PHOTO"
                                                                message:@"Failed"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
        }
    }
}

#pragma mark -
#pragma mark Location manager

/**
 Return a location manager -- create one if necessary.
 */
- (CLLocationManager *)locationManager {
	
    if (_locationManager != nil) {
		return _locationManager;
	}
	
	_locationManager = [[CLLocationManager alloc] init];
	[_locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
	[_locationManager setDelegate:self];
	
	return _locationManager;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    [_locationManager stopUpdatingLocation];
    
    if ([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)
    {
        [self launchPhotoLibrary];
    }
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ENABLE LOCATION"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"CLOSE"
                                          otherButtonTitles:nil];
    
    [alert show];
    [alert release];
}

@end
