//
//  ABUploadContactOperation.m
//  Aconnect
//
//  Created by linkit on 5/14/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import "ABUploadContactOperation.h"
#import <DropboxSDK/DropboxSDK.h>
#import <stdlib.h>
#import <AddressBook/AddressBook.h>
#import "VCard.h"
#import "ABFileModel.h"

@interface ABUploadContactOperation () <DBRestClientDelegate>

// export contacts
- (NSString*)getFilename:(ABRecordRef)person withIndex:(int)theIndex;
- (void)writeStringToFile:(NSString*)aString withFileName:(NSString*)aFileName;
- (NSString *)getDocumentPath:(NSString*)aFileName;
- (void)uploadFile:(NSString*)aFileName to:(NSString*)toPath from:(NSString*)fromPath;
- (void)deleteFile:(NSString*)aFileName;

//- (void)getListOfFiles:(BOOL)willImport;
//- (void)startExportContacts;

@property (nonatomic, readonly) DBRestClient* restClient;

@end

@implementation ABUploadContactOperation

@synthesize allPeople = allPeople_;
@synthesize index = index_;
@synthesize contactPaths = contactPaths_; // hapus

@synthesize theProgress = theProgress_;

#pragma mark -
#pragma mark Initialization & Memory Management

- (DBRestClient*)restClient {
    if (restClient == nil) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

- (id)initWithPeople:(CFArrayRef)thePeople withIndex:(int)theIndex withData:(NSArray*)data {
    if( (self = [super init]) ) {
        allPeople_ = thePeople;
        index_ = theIndex;
        
        //[contactPaths_ release];
        //contactPaths_ = data;
        
        contactPaths_ = [data copy];
    }
    return self;
}

- (void)dealloc {
    [contactPaths_ release]; // hapus
    contactPaths_ = nil; // hapus
    [super dealloc];
}

#pragma mark -
#pragma mark Start & Utility Methods

- (void)done {
    // Alert anyone that we are finished
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    executing_ = NO;
    finished_  = YES;
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)canceled {
	// Code for being cancelled
    [self done];
}

- (void)start {
    // Ensure that this operation starts on the main thread
    if (![NSThread isMainThread])
    {
        [self performSelectorOnMainThread:@selector(start)
                               withObject:nil waitUntilDone:NO];
        return;
    }
    
    // Ensure that the operation should exute
    if ( finished_ || [self isCancelled] ) { 
        [self done]; 
        return; 
    }
    
    // From this point on, the operation is officially executing--remember, isExecuting
    // needs to be KVO compliant!
    [self willChangeValueForKey:@"isExecuting"];
    executing_ = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    // Create the NSURLConnection--this could have been done in init, but we delayed
    // until no in case the operation was never enqueued or was cancelled before starting
    ABRecordRef person = CFArrayGetValueAtIndex(allPeople_, index_);
    
    //NSString *vCard = [VCard generateVCardStringWithRec:person];
    NSString *vCard = [VCard generateVCardStringWithRecID:index_];
    
    // generate file name here
    NSString *aFileName = [NSString stringWithFormat:@"%@.vcf",[self getFilename:person withIndex:index_]];
    
    // write file
    [self writeStringToFile:vCard withFileName:aFileName];
    
    // get Doc Path
    NSString *docPath = [self getDocumentPath:aFileName];
    
    // upload file
    [self uploadFile:aFileName to:@"/Contacts/" from:docPath];
}

- (void)onProgress {
    [self willChangeValueForKey:@"isExecuting"];
    executing_ = YES;
    [self didChangeValueForKey:@"isExecuting"];
}

#pragma mark -
#pragma mark Overrides

- (BOOL)isConcurrent {
    return YES;
}

- (BOOL)isExecuting {
    return executing_;
}

- (BOOL)isFinished {
    return finished_;
}

#pragma mark - Upload Callback

- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath
              from:(NSString*)srcPath metadata:(DBMetadata*)metadata {
    
    //counter++;
    //NSLog(@"counter = %i",counter);
    
    NSString *aFileName = [destPath lastPathComponent];
    [self deleteFile:aFileName];
    
    //if (counter == totalContact) {
    //}
    
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
		[self done];
	}
}

- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error {
    //NSLog(@"File upload failed with error - %@", error);
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
		[self done];
	}
}

- (void)restClient:(DBRestClient*)client uploadProgress:(CGFloat)progress 
           forFile:(NSString*)destPath from:(NSString*)srcPath {
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
        theProgress_ = progress;
        //NSLog(@"theProgress_ di OPERATION = %f",theProgress_);
		[self onProgress];
	}
}

#pragma mark - Private Method

- (NSString*)getFilename:(ABRecordRef)person withIndex:(int)theIndex {
    NSString *firstName = (NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString *lastName = (NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    NSString *company = (NSString *)ABRecordCopyValue(person, kABPersonOrganizationProperty);
    
    NSString *fileName;
    
    if (firstName == nil && lastName != nil) {
        fileName = lastName;
    }
    else if (lastName == nil && firstName != nil) {
        fileName = firstName;
    }
    else if (firstName == nil && lastName == nil) {
        if (company != nil) {
            fileName = company;
        }
        else {
            fileName = [NSString stringWithFormat:@"Unnamed_%i",theIndex];
        }
    }
    else {
        fileName = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
    }
    return fileName;
}

- (void)writeStringToFile:(NSString*)aString withFileName:(NSString*)aFileName {
    // Build the path, and create if needed.
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = aFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    // The main act.
    [[aString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

- (NSString *)getDocumentPath:(NSString*)aFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:aFileName];
    return path;
}

- (void)uploadFile:(NSString*)aFileName to:(NSString*)toPath from:(NSString*)fromPath {
    //[self.restClient uploadFile:aFileName toPath:toPath fromPath:fromPath];
    
    //NSLog(@"File akan diupload = %@",aFileName);
    
    if ([contactPaths_ count] == 0) {
        //NSLog(@"contactPaths_ kosong");
        [[self restClient] uploadFile:aFileName toPath:toPath withParentRev:nil fromPath:fromPath];
    }
    else {
        //NSLog(@"contactPaths_ banyak");
        BOOL same;
        NSString *revFile;
        
        for (int i=0; i < [contactPaths_ count]; i++) {
            ABFileModel *aFileData = [contactPaths_ objectAtIndex:i];
            NSString *contactPath = aFileData.path;
            
            NSString *aFileNameInDropbox = [contactPath lastPathComponent];
            //NSLog(@"aFileNameInDropbox = %@",aFileNameInDropbox);
            
            if ([aFileNameInDropbox isEqualToString:aFileName]) {
                //NSLog(@"+++Sama+++");
                same = YES;
                revFile = aFileData.rev;
                break;
            }
            else {
                //NSLog(@"---TIDAK Sama---");
                same = NO;
            }
        }
        
        if (same) {
            [[self restClient] uploadFile:aFileName toPath:toPath withParentRev:revFile fromPath:fromPath];
        }
        else {
            [[self restClient] uploadFile:aFileName toPath:toPath withParentRev:nil fromPath:fromPath];
        }
    }
}

- (void)deleteFile:(NSString*)aFileName {
    // Build the path, and create if needed.
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = aFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    NSError *error;
    
    if ([[NSFileManager defaultManager] removeItemAtPath:fileAtPath error:&error] != YES)
        NSLog(@"Unable to delete file: %@", [error localizedDescription]);
//    else
//        NSLog(@"Delete File Success");
}

@end
