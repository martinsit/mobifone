//
//  ABContactController.m
//  Aconnect
//
//  Created by linkit on 4/26/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import "ABContactController.h"
#import "VCard.h"
#import "VCardImporter.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <DropboxSDK/DropboxSDK.h>
#import <stdlib.h>
#import "ABFileModel.h"

@interface ABContactController () <DBRestClientDelegate>

// export contacts
- (NSString*)getFilename:(ABRecordRef)person withIndex:(int)index;
- (void)writeStringToFile:(NSString*)aString withFileName:(NSString*)aFileName;
- (NSString *)getDocumentPath:(NSString*)aFileName;
- (void)uploadFile:(NSString*)aFileName to:(NSString*)toPath from:(NSString*)fromPath;
- (void)deleteFile:(NSString*)aFileName;
- (void)getListOfFiles:(BOOL)willImport;
- (void)startExportContacts;

// import contacts
- (void)loadVCard:(NSArray*)selectedContacts;
- (NSString*)createTempVCardPath:(NSString*)fileName;

@property (nonatomic, readonly) DBRestClient* restClient;

@end

@implementation ABContactController

@synthesize delegate;
@synthesize callback;
@synthesize errorCallback;

- (void)dealloc {
    [restClient release];
    [contactsHash release];
    [contactPaths release];
    [currentContactPath release];
	[super dealloc];
}

- (DBRestClient*)restClient {
    if (restClient == nil) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

#pragma mark - Export Contacts

- (void)exportContacts:(id)theDelegate
          withSelector:(SEL)theSelector {
    
    self.delegate = theDelegate;
    self.callback = theSelector;
    
    // First get list of files from dropbox
    [self getListOfFiles:NO];
}

- (void)getListOfFiles:(BOOL)willImport {
    willImportContacts = willImport;
    
    NSString *contactsRoot = nil;
    if ([DBSession sharedSession].root == kDBRootDropbox) {
        contactsRoot = @"/Contacts";
        //NSLog(@"/Contacts");
    } else {
        contactsRoot = @"/";
        //NSLog(@"/");
    }
    
    [self.restClient loadMetadata:contactsRoot withHash:contactsHash];
}

- (void)startExportContacts {
    ABAddressBookRef addressBookRef = ABAddressBookCreate();
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBookRef);
    totalContact = nPeople;
    counter = 0;
    
    for (int i=0; i < nPeople; i++) {
        ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
        
        //NSLog(@"Contacts");
        NSString *vCard = [VCard generateVCardStringWithRec:person];
        //NSString* vCard = [VCard generateVCardStringWithRecID:1];
        //NSLog(@"isi VCard = %@",vCard);
        
        // generate file name here
        NSString *aFileName = [NSString stringWithFormat:@"%@.vcf",[self getFilename:person withIndex:i]];
        
        // create temporary file <-- ternyata gak bisa pake temp file
        //NSString *tempDocPath = [self createTempVCardPath:aFileName];
        
        // write file
        [self writeStringToFile:vCard withFileName:aFileName];
        
        // get Doc Path
        NSString *docPath = [self getDocumentPath:aFileName];
        
        // upload file
        [self uploadFile:aFileName to:@"/Contacts/" from:docPath];
    }
    
    CFRelease(addressBookRef);
}

- (NSString*)getFilename:(ABRecordRef)person withIndex:(int)index {
    NSString *firstName = (NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString *lastName = (NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    NSString *company = (NSString *)ABRecordCopyValue(person, kABPersonOrganizationProperty);
    
    NSString *fileName;
    
    if (firstName == nil && lastName != nil) {
        fileName = lastName;
    }
    else if (lastName == nil && firstName != nil) {
        fileName = firstName;
    }
    else if (firstName == nil && lastName == nil) {
        if (company != nil) {
            fileName = company;
        }
        else {
            fileName = [NSString stringWithFormat:@"Unnamed_%i",index];
        }
    }
    else {
        fileName = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
    }
    //NSLog(@"fileName = %@",fileName);
    return fileName;
}

- (void)writeStringToFile:(NSString*)aString withFileName:(NSString*)aFileName {
    // Build the path, and create if needed.
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = aFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    // The main act.
    [[aString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

- (NSString *)getDocumentPath:(NSString*)aFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:aFileName];
    return path;
}

- (void)uploadFile:(NSString*)aFileName to:(NSString*)toPath from:(NSString*)fromPath {
    //[self.restClient uploadFile:aFileName toPath:toPath fromPath:fromPath];
    
    //NSLog(@"File akan diupload = %@",aFileName);
    
    if ([contactPaths count] == 0) {
        [[self restClient] uploadFile:aFileName toPath:toPath withParentRev:nil fromPath:fromPath];
    }
    else {
        BOOL same;
        NSString *revFile;
        
        for (int i=0; i < [contactPaths count]; i++) {
            ABFileModel *aFileData = [contactPaths objectAtIndex:i];
            //NSString *contactPath = [contactPaths objectAtIndex:i];
            NSString *contactPath = aFileData.path;
            
            NSString *aFileNameInDropbox = [contactPath lastPathComponent];
            //NSLog(@"aFileNameInDropbox = %@",aFileNameInDropbox);
            
            if ([aFileNameInDropbox isEqualToString:aFileName]) {
                same = YES;
                revFile = aFileData.rev;
                break;
            }
            else {
                same = NO;
            }
        }
        
        if (same) {
            [[self restClient] uploadFile:aFileName toPath:toPath withParentRev:revFile fromPath:fromPath];
        }
        else {
            [[self restClient] uploadFile:aFileName toPath:toPath withParentRev:nil fromPath:fromPath];
        }
    }
}

- (void)deleteFile:(NSString*)aFileName {
    // Build the path, and create if needed.
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = aFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    NSError *error;
    
    if ([[NSFileManager defaultManager] removeItemAtPath:fileAtPath error:&error] != YES)
        NSLog(@"Unable to delete file: %@", [error localizedDescription]);
//    else
//        NSLog(@"Delete File Success");
}

#pragma mark - Import Contacts

- (void)getListFile:(id)theDelegate 
       withSelector:(SEL)theSelector {
    self.delegate = theDelegate;
    self.callback = theSelector;
    
    [self getListOfFiles:YES];
}

- (void)importContacts:(id)theDelegate
          withSelector:(SEL)theSelector {
    
    self.delegate = theDelegate;
    self.callback = theSelector;
    
    [self getListOfFiles:YES];
    
    /*
    willImportContacts = YES;
    
    NSString *contactsRoot = nil;
    if ([DBSession sharedSession].root == kDBRootDropbox) {
        contactsRoot = @"/Contacts";
        NSLog(@"/Contacts");
    } else {
        contactsRoot = @"/";
        NSLog(@"/");
    }
    
    [self.restClient loadMetadata:contactsRoot withHash:contactsHash];*/
}

- (void)restoreContacts:(NSArray*)selectedContacts
           withDelegate:(id)theDelegate
           withSelector:(SEL)theSelector {
    self.delegate = theDelegate;
    self.callback = theSelector;
    
    [self loadVCard:selectedContacts];
}

- (void)loadVCard:(NSArray*)selectedContacts {
    if ([selectedContacts count] == 0) {
        NSString *msg = nil;
        if ([DBSession sharedSession].root == kDBRootDropbox) {
            msg = @"Put .vcf files in your Contacts folder to use AXIS box!";
        } else {
            msg = @"Put .vcf files in your app's App folder to use AXIS box!";
        }
        
        [[[[UIAlertView alloc] initWithTitle:@"No VCard Files!" 
                                     message:msg 
                                    delegate:nil 
                           cancelButtonTitle:@"OK" 
                           otherButtonTitles:nil] autorelease] show];
        
        //[self setWorking:NO];
    }
    else {
        
        NSString* contactPath;
        
        //NSLog(@"jumlah contacts = %i",[selectedContacts count]);
        
        totalContact = [selectedContacts count];
        counter = 0;
        
        for (int i=0; i < [selectedContacts count]; i++) {
            ABFileModel *aFileData = [selectedContacts objectAtIndex:i];
            //contactPath = [contactPaths objectAtIndex:i];
            contactPath = aFileData.path;
            
            [currentContactPath release];
            currentContactPath = [contactPath retain];
            
            //NSLog(@"currentContactPath ke-%i = %@",i,currentContactPath);
            NSString *aFileName = [currentContactPath lastPathComponent];
            //NSLog(@"aFileName = %@",aFileName);
            //NSLog(@"[self createTempVCardPath:aFileName] = %@",[self createTempVCardPath:aFileName]);
            [self.restClient loadFile:currentContactPath intoPath:[self createTempVCardPath:aFileName]];
        }
        
        /*
         [currentContactPath release];
         currentContactPath = [contactPath retain];
         
         NSLog(@"currentContactPath = %@",currentContactPath);        
         [self.restClient loadFile:currentContactPath intoPath:[self contactPath]];*/
    }
}

- (NSString*)createTempVCardPath:(NSString*)fileName {
    return [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
}

#pragma mark - DBRestClientDelegate methods

- (void)restClient:(DBRestClient*)client loadedMetadata:(DBMetadata*)metadata {
    [contactsHash release];
    contactsHash = [metadata.hash retain];
    
    NSArray* validExtensions = [NSArray arrayWithObjects:@"vcf", nil];
    
    //NSMutableArray* newContactPaths = [NSMutableArray new];
    NSMutableArray *fileDataTmp = [NSMutableArray new];
    
    for (DBMetadata* child in metadata.contents) {
        NSString* extension = [[child.path pathExtension] lowercaseString];
        if (!child.isDirectory && [validExtensions indexOfObject:extension] != NSNotFound) {
            //[newContactPaths addObject:child.path];
            
            ABFileModel *aFileData = [[ABFileModel alloc] init];
            aFileData.filename          = child.filename;
            //NSLog(@"aFileData.filename = %@",aFileData.filename);
            aFileData.thumbnailExists   = child.thumbnailExists;
            aFileData.totalBytes        = child.totalBytes;
            aFileData.lastModifiedDate  = child.lastModifiedDate;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            NSString *dateString = [dateFormatter stringFromDate:child.lastModifiedDate];
            aFileData.dateString = dateString;
            [dateFormatter release];
            
            aFileData.humanReadableSize = child.humanReadableSize;
            aFileData.icon              = child.icon;
            aFileData.path              = child.path;
            //NSLog(@"aFileData.path = %@",aFileData.path);
            aFileData.rev               = child.rev;
            //NSLog(@"aFileData.rev = %@",aFileData.rev);
            
            //
            NSUInteger length = [child.filename length];
            NSString *newString = [child.filename substringToIndex:length-4];
            aFileData.name = newString;
            //
            
            [fileDataTmp addObject:aFileData];
            
            [aFileData release];
            aFileData = nil;
        }
    }
    [contactPaths release];
    //contactPaths = newContactPaths;
    contactPaths = fileDataTmp;
    
    if (willImportContacts) {
        //[self loadVCard];
        
        if (delegate && callback) {
            if ([delegate respondsToSelector:self.callback]) {
                BOOL respond = YES;
                NSNumber *passedValue = [NSNumber numberWithBool:respond];
                [delegate performSelector:self.callback withObject:passedValue withObject:contactPaths];
            }
            else {
                //NSLog(@"No response from delegate");
            }
        }
    }
    else {
        //NSLog(@"**Start export Contacts **");
        [self startExportContacts];
    }
}

- (void)restClient:(DBRestClient*)client metadataUnchangedAtPath:(NSString*)path {
    //[self loadVCard];
}

- (void)restClient:(DBRestClient*)client loadMetadataFailedWithError:(NSError*)error {
    //NSLog(@"restClient:loadMetadataFailedWithError: %@", [error localizedDescription]);
    if (willImportContacts) {
        if (delegate && callback) {
            if ([delegate respondsToSelector:self.callback]) {
                BOOL respond = NO;
                NSNumber *passedValue = [NSNumber numberWithBool:respond];
                [delegate performSelector:self.callback withObject:passedValue withObject:nil];
            }
            else {
                //NSLog(@"No response from delegate");
            }
        }
    }
}

- (void)restClient:(DBRestClient*)client loadedThumbnail:(NSString*)destPath {
    //[self setWorking:NO];
    /*
    NSError *error;
    NSString *tempTextOut = [NSString stringWithContentsOfFile:destPath
                                                      encoding:NSUTF8StringEncoding
                                                         error:&error];
    NSLog(@"isi VCard yang di-download = %@",tempTextOut);*/
}

- (void)restClient:(DBRestClient*)client loadThumbnailFailedWithError:(NSError*)error {
    //[self setWorking:NO];
    //[self displayError];
}

#pragma mark - Upload Callback

- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath
              from:(NSString*)srcPath metadata:(DBMetadata*)metadata {
    
    /*
    //[self setWorking:NO];
    NSLog(@"file diupload dari : %@",srcPath);
    NSLog(@"destPath : %@",destPath);
    NSLog(@"metadata.filename : %@",metadata.filename);
    NSLog(@"File uploaded successfully to path: %@", metadata.path);
    NSString *msg = [NSString stringWithFormat:@"File uploaded successfully to path: %@",metadata.path];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Success"  
                                                    message:msg  
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];*/
    
    counter++;
    //NSLog(@"counter = %i",counter);
    
    NSString *aFileName = [destPath lastPathComponent];
    [self deleteFile:aFileName];
    
    if (counter == totalContact) {
        if (delegate && callback) {
            /*
            if ([delegate respondsToSelector:self.callback]) {
                NSString *msg = [NSString stringWithFormat:@"File uploaded successfully to Contacts directory"];
                [delegate performSelector:self.callback withObject:msg];
            }
            else {
                //NSLog(@"No response from delegate");
            }*/
            
            if ([delegate respondsToSelector:self.callback]) {
                BOOL respond = YES;
                NSNumber *passedValue = [NSNumber numberWithBool:respond];
                [delegate performSelector:self.callback withObject:passedValue withObject:nil];
            }
            else {
                //NSLog(@"No response from delegate");
            }
        }
    }
}

- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error {
    //[self setWorking:NO];
    //NSLog(@"File upload failed with error - %@", error);
    /*
    NSString *msg = [NSString stringWithFormat:@"File upload failed with error - %@",error];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Failed"  
                                                    message:msg  
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];*/
    
    if (delegate && callback) {
        if ([delegate respondsToSelector:self.callback]) {
            BOOL respond = NO;
            NSNumber *passedValue = [NSNumber numberWithBool:respond];
            NSString *msg = [NSString stringWithFormat:@"%@",[error localizedDescription]];
            [delegate performSelector:self.callback withObject:passedValue withObject:msg];
        }
        else {
            //NSLog(@"No response from delegate");
        }
    }
}

#pragma mark - Download Callback

- (void)restClient:(DBRestClient*)client loadedFile:(NSString*)destPath contentType:(NSString*)contentType metadata:(DBMetadata*)metadata {
    //[self setWorking:NO];
    
    counter++;
    
    NSError *error;
    NSString *tempTextOut = [NSString stringWithContentsOfFile:destPath
                                                      encoding:NSUTF8StringEncoding
                                                         error:&error];
    //NSLog(@"isi VCard yang di-download = %@",tempTextOut);
    
    VCardImporter *importer = [[VCardImporter alloc] init];
    [importer parse:tempTextOut];
    [importer release];
    
    if (counter == totalContact) {
        if (delegate && callback) {
            if ([delegate respondsToSelector:self.callback]) {
                BOOL respond = YES;
                NSNumber *passedValue = [NSNumber numberWithBool:respond];
                //NSString *msg = [NSString stringWithFormat:@"Please check your Address Book"];
                [delegate performSelector:self.callback withObject:passedValue withObject:nil];
            }
            else {
                //NSLog(@"No response from delegate");
            }
        }
    }
}

/*
 - (void)restClient:(DBRestClient*)client loadProgress:(CGFloat)progress forFile:(NSString*)destPath {
 
 }*/

- (void)restClient:(DBRestClient*)client loadFileFailedWithError:(NSError*)error {
    //NSLog(@"restClient:loadFileFailedWithError: %@", [error localizedDescription]);
    //[self displayError];
    //[self setWorking:NO];
    
    if (delegate && callback) {
        if ([delegate respondsToSelector:self.callback]) {
            BOOL respond = NO;
            NSNumber *passedValue = [NSNumber numberWithBool:respond];
            NSString *msg = [NSString stringWithFormat:@"%@",[error localizedDescription]];
            [delegate performSelector:self.callback withObject:passedValue withObject:msg];
        }
        else {
            //NSLog(@"No response from delegate");
        }
    }
}

@end
