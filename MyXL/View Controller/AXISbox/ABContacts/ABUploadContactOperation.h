//
//  ABUploadContactOperation.h
//  Aconnect
//
//  Created by linkit on 5/14/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DBRestClient;

@interface ABUploadContactOperation : NSOperation {
    // In concurrent operations, we have to manage the operation's state
    BOOL executing_;
    BOOL finished_;
    
    DBRestClient* restClient;
    CFArrayRef allPeople_;
    int index_;
    NSArray *contactPaths_;
    
    CGFloat theProgress_;
}

@property (nonatomic, readonly) CFArrayRef allPeople;
@property (nonatomic, readonly) int index;
@property (nonatomic, readonly) NSArray *contactPaths; // hapus

@property (nonatomic, readonly) CGFloat theProgress;

- (id)initWithPeople:(CFArrayRef)thePeople withIndex:(int)theIndex withData:(NSArray*)data;

@end
