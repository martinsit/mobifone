//
//  ABContactsViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ABContactsViewController.h"
#import "ABContactController.h"
#import "ABFileModel.h"
#import "ABContactEngine.h"
#import "ABUploadContactOperation.h"
#import "ABDownloadContactOperation.h"

#import <AddressBook/AddressBook.h>

@interface ABContactsViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;
//@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *submitBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *selectAllBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *clearBtn;

@property (nonatomic, retain) ABContactController *contactController;
@property (nonatomic, retain) NSArray *vcfArray;
@property (nonatomic, retain) NSMutableArray *filteredListContent;
@property (nonatomic, retain, readonly) NSArray *sectionedListContent;
@property (nonatomic, retain) NSArray *selectedVcf;

@property (nonatomic, retain) ABContactEngine *contactEngine;
@property (nonatomic, retain) NSOperationQueue *operationQueue;
@property (readwrite) int totalContact;
@property (readwrite) int counter;
@property (readwrite) float progress;

// The saved state of the search UI if a memory warning removed the view.
@property (nonatomic, retain) NSString *savedSearchTerm;
@property (readwrite) NSInteger savedScopeButtonIndex;
@property (readwrite) BOOL searchWasActive;

- (void)loadAddressBook;
- (NSString*)modifyPhoneNumber:(NSString*)phoneNumber;
- (void)updateSelectionCount;

- (IBAction)performClearAll:(id)sender;
- (void)clearSelectionForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath;

- (IBAction)performSelectAll:(id)sender;
- (void)markSelectionForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath;

- (IBAction)performSubmit:(id)sender;

- (void)backupContacts;
- (void)restoreContacts;

@end

@interface NSArray (SSArrayOfArrays)
- (id)objectAtIndexPath:(NSIndexPath *)indexPath;
@end

@implementation NSArray (SSArrayOfArrays)

- (id)objectAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
}

@end

@interface NSMutableArray (SSArrayOfArrays)
// If idx is beyond the bounds of the reciever, this method automatically extends the reciever to fit with empty subarrays.
- (void)addObject:(id)anObject toSubarrayAtIndex:(NSUInteger)idx;
@end

@implementation NSMutableArray (SSArrayOfArrays)

- (void)addObject:(id)anObject toSubarrayAtIndex:(NSUInteger)idx
{
    while ([self count] <= idx) {
        [self addObject:[NSMutableArray array]];
    }
    
    [[self objectAtIndex:idx] addObject:anObject];
}

@end

@implementation ABContactsViewController

@synthesize isImport;

- (void)setVcfArray:(NSArray *)inListContent
{
    if (_vcfArray == inListContent) {
        return;
    }
    [_vcfArray release];
    _vcfArray = [inListContent retain];
    
    NSMutableArray *sections = [NSMutableArray array];
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    
    for (ABFileModel *aFileData in _vcfArray) {
        NSInteger section = [collation sectionForObject:aFileData collationStringSelector:@selector(name)];
        [sections addObject:aFileData toSubarrayAtIndex:section];
    }
    
    NSInteger section = 0;
    for (section = 0; section < [sections count]; section++) {
        NSArray *sortedSubarray = [collation sortedArrayFromArray:[sections objectAtIndex:section]
                                          collationStringSelector:@selector(name)];
        [sections replaceObjectAtIndex:section withObject:sortedSubarray];
    }
    [_sectionedListContent release];
    _sectionedListContent = [sections retain];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    if (isImport) {
        //---------//
        // Restore //
        //---------//
        
        _selectAllBtn.title = [Language get:@"select_all" alter:nil];
        _submitBtn.title = [Language get:@"restore" alter:nil];
        _clearBtn.title = [Language get:@"clear" alter:nil];
        
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        ABContactController *aContactController = [[ABContactController alloc] init];
        self.contactController = aContactController;
        [self.contactController getListFile:self withSelector:@selector(getListCallback:withData:)];
        [aContactController release];
    }
    else {
        //--------//
        // Backup //
        //--------//
        
        _selectAllBtn.title = [Language get:@"select_all" alter:nil];
        _submitBtn.title = [Language get:@"backup" alter:nil];
        _clearBtn.title = [Language get:@"clear" alter:nil];
        
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        [self loadAddressBook];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Selector

- (void)loadAddressBook {
    ABAddressBookRef addressBook = ABAddressBookCreate();
	CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
	CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    
    NSMutableArray *people = [[NSMutableArray alloc] init];
    
	for (int i = 0; i < nPeople; i++) {
		ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
        
        NSString *firstName = (NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
		NSString *lastName = (NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
        NSString *company = (NSString *)ABRecordCopyValue(person, kABPersonOrganizationProperty);
        
        ABFileModel *aFileData = [[ABFileModel alloc] init];
        aFileData.peopleIndex = i;
        
        if (firstName == nil && lastName != nil) {
            aFileData.name = lastName;
        }
        else if (lastName == nil && firstName != nil) {
            aFileData.name = firstName;
        }
        else if (firstName == nil && lastName == nil) {
            if (company != nil) {
                aFileData.name = company;
            }
            else {
                ABMultiValueRef phones = (NSString*)ABRecordCopyValue(person, kABPersonPhoneProperty);
                NSString* phoneNumber = @"";
                NSString* phoneNumberLabel;
                for(CFIndex i = 0; i < ABMultiValueGetCount(phones); i++) {
                    phoneNumberLabel = (NSString*)ABMultiValueCopyLabelAtIndex(phones, i);
                    if([phoneNumberLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel])
                    {
                        [phoneNumber release];
                        phoneNumber = (NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                    }
                    else if ([phoneNumberLabel isEqualToString:(NSString*)kABPersonPhoneIPhoneLabel])
                    {
                        [phoneNumber release];
                        phoneNumber = (NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                    }
                    else if ([phoneNumberLabel isEqualToString:(NSString*)kABPersonPhoneMainLabel])
                    {
                        [phoneNumber release];
                        phoneNumber = (NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                    }
                    else if ([phoneNumberLabel isEqualToString:(NSString*)kABPersonPhoneHomeFAXLabel])
                    {
                        [phoneNumber release];
                        phoneNumber = (NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                    }
                    else if ([phoneNumberLabel isEqualToString:(NSString*)kABPersonPhoneWorkFAXLabel])
                    {
                        [phoneNumber release];
                        phoneNumber = (NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                    }
                    else if ([phoneNumberLabel isEqualToString:(NSString*)kABPersonPhonePagerLabel])
                    {
                        [phoneNumber release];
                        phoneNumber = (NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                    }
                    else {
                        [phoneNumber release];
                        phoneNumber = (NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                    }
                    phoneNumber = [self modifyPhoneNumber:phoneNumber];
                    aFileData.name = phoneNumber;
                    break;
                }
            }
        }
        else {
            aFileData.name = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
        }
        
        ////////////////////// get person image
        UIImage *img;
        if (ABPersonHasImageData(person)) {
            img = [UIImage imageWithData:(NSData *)ABPersonCopyImageData(person)];
        }
        else {
            img = nil;
        }
        aFileData.photo = img;
        //////////////////////
        
        [people addObject:aFileData];
		[aFileData release];
	}
    
    self.vcfArray = [people copy];
    [people release];
    
    // create a filtered list that will contain products for the search results table.
    self.filteredListContent = [NSMutableArray arrayWithCapacity:[self.vcfArray count]];
    
    // restore search settings if they were saved in didReceiveMemoryWarning.
    if (self.savedSearchTerm)
    {
        [self.searchDisplayController setActive:self.searchWasActive];
        [self.searchDisplayController.searchBar setSelectedScopeButtonIndex:self.savedScopeButtonIndex];
        [self.searchDisplayController.searchBar setText:self.savedSearchTerm];
        
        self.savedSearchTerm = nil;
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [_theTableView reloadData];
    _theTableView.scrollEnabled = YES;
    
    if ([self.vcfArray count] > 0) {
        
        _submitBtn.enabled = NO;
        _selectAllBtn.enabled = YES;
        _clearBtn.enabled = YES;
    }
    else {
        _submitBtn.enabled = NO;
        _selectAllBtn.enabled = NO;
        _clearBtn.enabled = NO;
    }
}

- (NSString*)modifyPhoneNumber:(NSString*)phoneNumber {
    if ([phoneNumber hasPrefix:@"+"]) {
        phoneNumber = [phoneNumber substringFromIndex:1];
        phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    else if ([phoneNumber hasPrefix:@"0"]) {
        phoneNumber = [phoneNumber substringFromIndex:1];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *countryPhCode = [prefs stringForKey:@"Account.CountryPhoneCode"];
        phoneNumber = [NSString stringWithFormat:@"%@%@",countryPhCode,phoneNumber];
    }
    else {
        phoneNumber = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]]componentsJoinedByString:@""];
    }
    
    return phoneNumber;
}

- (void)updateSelectionCount {
    NSInteger count = 0;
	
    NSMutableArray *selectedFileTmp = [NSMutableArray new];
	for (ABFileModel *aFileData in self.vcfArray)
	{
		if (aFileData.selected)
		{
			count++;
            [selectedFileTmp addObject:aFileData];
		}
	}
    [_selectedVcf release];
    _selectedVcf = selectedFileTmp;
    
    if ([_selectedVcf count] > 0) {
        _submitBtn.enabled = YES;
    }
    else {
        _submitBtn.enabled = NO;
    }
}

- (IBAction)performClearAll:(id)sender {
    NSInteger numberOfSection = [self numberOfSectionsInTableView:_theTableView];
    for (NSInteger section=0; section<numberOfSection; section++) {
        NSInteger numberOfRow = [self tableView:_theTableView numberOfRowsInSection:section];
        for (NSInteger row=0; row<numberOfRow; row++) {
            [self clearSelectionForTableView:_theTableView indexPath:[NSIndexPath indexPathForRow:row inSection:section]];
        }
    }
}

- (void)clearSelectionForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    ABFileModel *aFileData = [self.sectionedListContent objectAtIndexPath:indexPath];
    if (aFileData.selected)
	{
		[self tableView:tableView didSelectRowAtIndexPath:indexPath];
		aFileData.selected = NO;
	}
}

- (IBAction)performSelectAll:(id)sender {
    NSInteger numberOfSection = [self numberOfSectionsInTableView:_theTableView];
    for (NSInteger section=0; section<numberOfSection; section++) {
        NSInteger numberOfRow = [self tableView:_theTableView numberOfRowsInSection:section];
        for (NSInteger row=0; row<numberOfRow; row++) {
            [self markSelectionForTableView:_theTableView indexPath:[NSIndexPath indexPathForRow:row inSection:section]];
        }
    }
}

- (void)markSelectionForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    ABFileModel *aFileData = [self.sectionedListContent objectAtIndexPath:indexPath];
    if (!aFileData.selected)
	{
		[self tableView:tableView didSelectRowAtIndexPath:indexPath];
		aFileData.selected = YES;
	}
}

- (IBAction)performSubmit:(id)sender {
    if (isImport) {
        NSString *message = [NSString stringWithFormat:@"%@\n%@",[Language get:@"restore_contacts_confirmation" alter:nil],[Language get:@"ask_continue" alter:nil]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[Language get:@"restore_contacts" alter:nil]uppercaseString]
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:[[Language get:@"yes" alter:nil]uppercaseString]
                                              otherButtonTitles:[[Language get:@"no" alter:nil]uppercaseString],nil];
        alert.tag = 1;
        [alert show];
        [alert release];
    }
    else {
        NSString *message = [NSString stringWithFormat:@"%@\n%@",[Language get:@"backup_contacts_confirmation" alter:nil],[Language get:@"ask_continue" alter:nil]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[Language get:@"backup_contacts" alter:nil]uppercaseString]
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:[[Language get:@"yes" alter:nil]uppercaseString]
                                              otherButtonTitles:[[Language get:@"no" alter:nil]uppercaseString],nil];
        alert.tag = 2;
        [alert show];
        [alert release];
    }
}

- (void)backupContacts {
    /*
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.labelText = [self.lu translate:LN_DB_BACKINGUP_CONTACTS];
    [HUD show:YES];*/
    
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    ABContactEngine *abContactEngine = [[ABContactEngine alloc] init];
    self.contactEngine = abContactEngine;
    [self.contactEngine listAllFiles:NO
                    withDelegate:self
                    withSelector:@selector(listFilesCallback:withData:)];
    [abContactEngine release];
}

- (void)threadStartAnimating {
    _progress = 0.0f;
    
    self.hud.mode = MBProgressHUDModeDeterminate;
    self.hud.progress = _progress;
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //self.hud.labelText = [Language get:@"loading" alter:nil];
    
    /*
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    HUD.mode = MBProgressHUDModeDeterminate;
    [self.navigationController.view addSubview:HUD];
    HUD.labelText = [self.lu translate:LN_DB_BACKINGUP_CONTACTS];
    [HUD show:YES];
    HUD.progress = progress;*/
}

- (void)startBackup:(NSArray*)data {
    // Create operation queue
    _operationQueue = [NSOperationQueue new];
    // set maximum operations possible
    [_operationQueue setMaxConcurrentOperationCount:10];
    
    ABAddressBookRef addressBookRef = ABAddressBookCreate();
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
    
    _totalContact = [_selectedVcf count];
    _counter = 0;
    
    for (int i=0; i < [_selectedVcf count]; i++) {
        ABFileModel *aFileData = [_selectedVcf objectAtIndex:i];
        ABUploadContactOperation *uploadOperation = [[ABUploadContactOperation alloc] initWithPeople:allPeople
                                                                                           withIndex:aFileData.peopleIndex
                                                                                            withData:data];
        
        [uploadOperation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
        // percobaan
        [uploadOperation addObserver:self forKeyPath:@"isExecuting" options:NSKeyValueObservingOptionNew context:NULL];
        //--
        [_operationQueue addOperation:uploadOperation]; // operation starts as soon as its added
        [uploadOperation release];
    }
    
    CFRelease(addressBookRef);
}

- (void)restoreContacts {
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    _progress = 0.0f;
    self.hud.mode = MBProgressHUDModeDeterminate;
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.progress = _progress;
    
    // Create operation queue
    _operationQueue = [NSOperationQueue new];
    // set maximum operations possible
    [_operationQueue setMaxConcurrentOperationCount:10];
    
    _totalContact = [_selectedVcf count];
    _counter = 0;
    
    for (int i=0; i < [_selectedVcf count]; i++) {
        ABFileModel *aFileData = [_selectedVcf objectAtIndex:i];
        
        // download operation goes here
        ABDownloadContactOperation *downloadOperation = [[ABDownloadContactOperation alloc] initWithPath:aFileData.path];
        [downloadOperation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
        [_operationQueue addOperation:downloadOperation]; // operation starts as soon as its added
        [downloadOperation release];
    }
}

#pragma mark - KVO Observing

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)operation
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if ([operation isKindOfClass:[ABUploadContactOperation class]]) {
        if ([keyPath isEqualToString:@"isExecuting"]) {
            /*
             ABUploadContactOperation *uploadOperation = (ABUploadContactOperation *)operation;
             CGFloat currentprogress = [uploadOperation theProgress];
             NSLog(@"currentprogress = %f",currentprogress);*/
        }
        else {
            _counter++;
            if (_counter == _totalContact) {
                [UIApplication sharedApplication].idleTimerDisabled = NO;
                _progress = 1.0f;
                
                self.hud.progress = _progress;
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
//                HUD.progress = progress;
//                [HUD hide:YES];
//                [HUD removeFromSuperview];
//                [HUD release];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[Language get:@"backup_contacts" alter:nil]uppercaseString]
                                                                message:[Language get:@"success" alter:nil]
                                                               delegate:self
                                                      cancelButtonTitle:[Language get:@"ok" alter:nil]
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                if (_progress < 1.0f) {
                    _progress = ((float)_counter/(float)_totalContact)*1.0f;
                    
                    self.hud.progress = _progress;
                    //HUD.progress = progress;
                }
            }
        }
    }
    else {
        _counter++;
        if (_counter == _totalContact) {
            [UIApplication sharedApplication].idleTimerDisabled = NO;
            _progress = 1.0f;
            
            self.hud.progress = _progress;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
//            HUD.progress = progress;
//            [HUD hide:YES];
//            [HUD removeFromSuperview];
//            [HUD release];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[Language get:@"restore_contacts" alter:nil]uppercaseString]
                                                            message:[Language get:@"success" alter:nil]
                                                           delegate:self
                                                  cancelButtonTitle:[Language get:@"ok" alter:nil]
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        else {
            if (_progress < 1.0f) {
                _progress = ((float)_counter/(float)_totalContact)*1.0f;
                self.hud.progress = _progress;
                //HUD.progress = progress;
            }
        }
    }
}

#pragma mark - Callback

- (void)listFilesCallback:(NSNumber*)respond withData:(NSArray*)data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL success = [respond boolValue];
    if (success) {
        [UIApplication sharedApplication].idleTimerDisabled = YES;
        [NSThread detachNewThreadSelector:@selector(threadStartAnimating) toTarget:self withObject:nil];
        [self performSelector:@selector(startBackup:)
                   withObject:data afterDelay:0.5];
    }
    else {
        // langsung upload
        [UIApplication sharedApplication].idleTimerDisabled = YES;
        [NSThread detachNewThreadSelector:@selector(threadStartAnimating) toTarget:self withObject:nil];
        [self performSelector:@selector(startBackup:)
                   withObject:data afterDelay:0.5];
    }
}

- (void)getListCallback:(NSNumber*)respond withData:(NSArray*)data {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if ([data count] > 0) {
        _submitBtn.enabled = NO;
        _selectAllBtn.enabled = YES;
        _clearBtn.enabled = YES;
    }
    else {
        _submitBtn.enabled = NO;
        _selectAllBtn.enabled = NO;
        _clearBtn.enabled = NO;
    }
    
    BOOL success = [respond boolValue];
    if (success) {
        self.vcfArray = data;
        
        // create a filtered list that will contain products for the search results table.
        self.filteredListContent = [NSMutableArray arrayWithCapacity:[self.vcfArray count]];
        
        // restore search settings if they were saved in didReceiveMemoryWarning.
        if (self.savedSearchTerm)
        {
            [self.searchDisplayController setActive:self.searchWasActive];
            [self.searchDisplayController.searchBar setSelectedScopeButtonIndex:self.savedScopeButtonIndex];
            [self.searchDisplayController.searchBar setText:self.savedSearchTerm];
            
            self.savedSearchTerm = nil;
        }
        
        [_theTableView reloadData];
        _theTableView.scrollEnabled = YES;
    }
    else {
        [_theTableView reloadData];
        _theTableView.scrollEnabled = YES;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        return 1;
    }
	else
	{
        return [self.sectionedListContent count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        return [self.filteredListContent count];
    }
	else
	{
        return [[self.sectionedListContent objectAtIndex:section] count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    ABContactsCell *cell = (ABContactsCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"ABContactsCell" owner:self options:nil];
		cell = [_cell autorelease];
		_cell = nil;
	}
    
    ABFileModel *aFileData = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        aFileData = [self.filteredListContent objectAtIndex:indexPath.row];
    }
    else {
        aFileData = [self.sectionedListContent objectAtIndexPath:indexPath];
    }
    
    cell.filenameLabel.text = aFileData.name;
    //cell.sizeLabel.text = aFileData.humanReadableSize;
    if (isImport) {
        cell.sizeLabel.hidden = NO;
        cell.sizeLabel.text = [NSString stringWithFormat:@"%@: %@",@"Last Modified",aFileData.dateString];
        cell.imageView.image = [UIImage imageNamed:@"contact.png"];
    }
    else {
        cell.sizeLabel.hidden = YES;
        if (aFileData.photo == nil) {
            cell.imageView.image = [UIImage imageNamed:@"contact.png"];
        }
        else {
            cell.imageView.image = aFileData.photo;
        }
    }
    
    
    if (aFileData.selected) {
        cell.checkMark.image = [UIImage imageNamed:@"check_green.png"];
    }
    else {
        cell.checkMark.image = [UIImage imageNamed:@"notselected.png"];
    }
    
    /*
     TableViewProcessor *tvp = [[TableViewProcessor alloc] init];
     UITableViewCell *cell_ = [tvp processTableCells:(UITableViewCell*)cell
     withIndexPath:indexPath
     withTotalCell:10
     withAdditionalHeight:23.0
     andLineStatus:LA_WITH_LINE
     andArrowStatus:AR_WITH_ARROW];
     
     
     [tvp release];
     cell = (ABImportContactsCell*) cell_;*/
    return cell;
}
/*
 - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
 NSString *title = @"";
 if (section == 0) {
 title = @"Import Contacts";
 }
 else {
 title = nil;
 }
 return title;
 }
 
 - (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
 NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
 if (sectionTitle == nil) {
 return nil;
 }
 TableViewProcessor *tvp = [[TableViewProcessor alloc] init];
 UIView *view = [tvp viewForHeaderWithTitle:sectionTitle];
 [tvp release];
 return view;
 }*/

/*
 Section-related methods: Retrieve the section titles and section index titles from the collation.
 */

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    }
    else {
        return [[self.sectionedListContent objectAtIndex:section] count] ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section] : nil;
    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    }
    else {
        return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:
                [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
    }
    //return [NSArray arrayWithArray:[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 0;
    }
    else {
        if (title == UITableViewIndexSearch) {
            [tableView scrollRectToVisible:self.searchDisplayController.searchBar.frame animated:NO];
            return -1;
        }
        else {
            return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index-1];
        }
    }
    //return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index-1];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ABFileModel *aFileData = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        aFileData = [self.filteredListContent objectAtIndex:indexPath.row];
    }
	else
	{
        aFileData = [self.sectionedListContent objectAtIndexPath:indexPath];
    }
    
    aFileData.selected = !aFileData.selected;
    
    [self updateSelectionCount];
    
    ABContactsCell *cell = (ABContactsCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (!cell) {
        return;
    }
    
    if (aFileData.selected) {
        cell.checkMark.image = [UIImage imageNamed:@"check_green.png"];
    }
    else {
        cell.checkMark.image = [UIImage imageNamed:@"notselected.png"];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // Import
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            //NSLog(@"YES");
            [self restoreContacts];
        }
        else {
            //NSLog(@"NO");
        }
    }
    else if (alertView.tag == 2) {
        if (buttonIndex == 0) {
            //NSLog(@"B YES");
            [self backupContacts];
        }
        else {
            //NSLog(@"B NO");
        }
    }
    else {
    }
}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.filteredListContent removeAllObjects]; // First clear the filtered array.
	
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
    for (NSArray *section in self.sectionedListContent) {
        for (ABFileModel *aFileData in section)
        {
            NSComparisonResult result = [aFileData.name compare:searchText
                                                        options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)
                                                          range:NSMakeRange(0, [searchText length])];
            if (result == NSOrderedSame)
            {
                [self.filteredListContent addObject:aFileData];
            }
        }
    }
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}


- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    [controller.searchBar setShowsCancelButton:YES animated:NO];
    
    for (UIView *subView in controller.searchBar.subviews){
        if([subView isKindOfClass:[UIButton class]]){
            [(UIButton*)subView setTitle:@"Done" forState:UIControlStateNormal];
        }
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [_theTableView reloadData];
}

@end
