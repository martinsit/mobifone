//
//  ABDownloadContactOperation.m
//  Aconnect
//
//  Created by linkit on 5/17/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import "ABDownloadContactOperation.h"
#import <DropboxSDK/DropboxSDK.h>
#import <stdlib.h>
#import "VCardImporter.h"

@interface ABDownloadContactOperation () <DBRestClientDelegate>

// export contacts
//- (NSString*)getFilename:(ABRecordRef)person withIndex:(int)theIndex;
//- (void)writeStringToFile:(NSString*)aString withFileName:(NSString*)aFileName;
//- (NSString *)getDocumentPath:(NSString*)aFileName;
//- (void)uploadFile:(NSString*)aFileName to:(NSString*)toPath from:(NSString*)fromPath;
//- (void)deleteFile:(NSString*)aFileName;

//- (void)getListOfFiles:(BOOL)willImport;
//- (void)startExportContacts;

- (NSString*)createTempVCardPath:(NSString*)fileName;

@property (nonatomic, readonly) DBRestClient* restClient;

@end

@implementation ABDownloadContactOperation

@synthesize contactPath = contactPath_;

#pragma mark -
#pragma mark Initialization & Memory Management

- (DBRestClient*)restClient {
    if (restClient == nil) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

- (id)initWithPath:(NSString*)path {
    if( (self = [super init]) ) {
        contactPath_ = path;
    }
    return self;
}

- (void)dealloc {
    [contactPath_ release];
    contactPath_ = nil;
    [super dealloc];
}

#pragma mark -
#pragma mark Start & Utility Methods

- (void)done {
    // Alert anyone that we are finished
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    executing_ = NO;
    finished_  = YES;
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)canceled {
	// Code for being cancelled
    [self done];
}

- (void)start {
    // Ensure that this operation starts on the main thread
    if (![NSThread isMainThread])
    {
        [self performSelectorOnMainThread:@selector(start)
                               withObject:nil waitUntilDone:NO];
        return;
    }
    
    // Ensure that the operation should exute
    if ( finished_ || [self isCancelled] ) { 
        [self done]; 
        return; 
    }
    
    // From this point on, the operation is officially executing--remember, isExecuting
    // needs to be KVO compliant!
    [self willChangeValueForKey:@"isExecuting"];
    executing_ = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    // download file
    NSString *aFileName = [contactPath_ lastPathComponent];
    [self.restClient loadFile:contactPath_ intoPath:[self createTempVCardPath:aFileName]];
}

#pragma mark -
#pragma mark Overrides

- (BOOL)isConcurrent {
    return YES;
}

- (BOOL)isExecuting {
    return executing_;
}

- (BOOL)isFinished {
    return finished_;
}

#pragma mark - Download Callback

- (void)restClient:(DBRestClient*)client loadedFile:(NSString*)destPath contentType:(NSString*)contentType metadata:(DBMetadata*)metadata {
    
    NSError *error;
    NSString *tempTextOut = [NSString stringWithContentsOfFile:destPath
                                                      encoding:NSUTF8StringEncoding
                                                         error:&error];
    
    VCardImporter *importer = [[VCardImporter alloc] init];
    [importer parse:tempTextOut];
    [importer release];
    
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
		[self done];
	}
}

/*
 - (void)restClient:(DBRestClient*)client loadProgress:(CGFloat)progress forFile:(NSString*)destPath {
 
 }*/

- (void)restClient:(DBRestClient*)client loadFileFailedWithError:(NSError*)error {
    //NSLog(@"restClient:loadFileFailedWithError: %@", [error localizedDescription]);
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
		[self done];
	}
}

#pragma mark - Private Method

- (NSString*)createTempVCardPath:(NSString*)fileName {
    return [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
}

@end
