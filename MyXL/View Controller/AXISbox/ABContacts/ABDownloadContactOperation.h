//
//  ABDownloadContactOperation.h
//  Aconnect
//
//  Created by linkit on 5/17/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DBRestClient;

@interface ABDownloadContactOperation : NSOperation {
    // In concurrent operations, we have to manage the operation's state
    BOOL executing_;
    BOOL finished_;
    
    DBRestClient* restClient;
    NSString *contactPath_;
}

@property (nonatomic, readonly) NSString *contactPath; 

- (id)initWithPath:(NSString*)path;

@end
