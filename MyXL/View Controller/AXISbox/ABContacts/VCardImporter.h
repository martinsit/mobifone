//
//  VcardImporter.h
//  AddressBookVcardImport
//
//  Created by Alan Harper on 20/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

@interface VCardImporter : NSObject {
    ABAddressBookRef addressBook;
    ABRecordRef personRecord;
    NSString *base64image;
}

- (void)parse:(NSString *)inputString;
- (void)parseLine:(NSString *)line;
- (void)parseName:(NSString *)line;
- (void)parseEmail:(NSString *)line;
- (void)parseTel:(NSString *)line;
- (void)parseImage;
- (void)emptyAddressBook;

- (void)parseLineX:(NSString *)line;

//- (IBAction)getVcardInfoButtonPressed:(id)sender;

@end

