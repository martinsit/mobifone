//
//  VcardImporter.m
//  AddressBookVcardImport
//
//  Created by Alan Harper on 20/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "VCardImporter.h"
#import "Base64.h"

@implementation VCardImporter

- (id)init {
    if (self = [super init]) {
        //addressBook = ABAddressBookCreate();
    }
    
    return self;
}

- (void)dealloc {
    //CFRelease(addressBook);
    [super dealloc];
}

- (void)parse:(NSString *)inputString {
    //[self emptyAddressBook];
    
    //NSString *filename = [[NSBundle mainBundle] pathForResource:@"vCards" ofType:@"vcf"];
    //NSLog(@"openning file %@", filename);
    //NSData *stringData = [NSData dataWithContentsOfFile:filename];
    //NSString *vcardString = [[NSString alloc] initWithData:stringData encoding:NSUTF8StringEncoding];
    
    //--ABAddressBookRef addressBookRef = ABAddressBookCreate(); // create address book record
    
    NSString *vcardString = [[NSString alloc] initWithString:inputString];
    
    NSArray *lines = [vcardString componentsSeparatedByString:@"\n"];
    //NSLog(@"lines = %i",[lines count]);
    
    for(NSString* line in lines) {
        //NSLog(@"line = %@",line);
        [self parseLineX:line];
    }
    
    //--ABAddressBookSave(addressBookRef, nil); //NULL
    
    [vcardString release];
}

- (void)parseLineX:(NSString *)line {
    
    if ([line hasPrefix:@"BEGIN"]) {
        CFErrorRef anError = NULL;
        personRecord = ABPersonCreate();
        if (anError != NULL) { 
            //NSLog(@"error while creating..");
        }
        else {
            //NSLog(@"ABPersonCreate berhasil");
        }
    }
    else if ([line hasPrefix:@"END"]) {
        ABAddressBookRef addressBookRef; 
        CFErrorRef error = NULL; 
        addressBookRef = ABAddressBookCreate();
        BOOL isAdded = ABAddressBookAddRecord (addressBookRef,personRecord,&error);
        if (isAdded) {
            //NSLog(@"added..");
        }
        if (error != NULL) {
            //NSLog(@"ABAddressBookAddRecord %@", error);
        }
        error = NULL;
        
        BOOL isSaved = ABAddressBookSave (addressBookRef,&error);
        if(isSaved) {
            //NSLog(@"saved..");
        }
        if (error != NULL) {
            //NSLog(@"ABAddressBookSave %@", error);
        }
        CFRelease(addressBookRef);
        
        //ABAddressBookAddRecord(myAddressBook, personRecord, nil); //NULL
    } 
    else if ([line hasPrefix:@"N:"]) {
        [self parseName:line];
    } 
    else if ([line hasPrefix:@"EMAIL;"]) {
        [self parseEmail:line];
    } 
    else if ([line hasPrefix:@"PHOTO;BASE64:"]) {
        base64image = [NSString string];
        base64image = [line substringFromIndex:13];
        //NSLog(@"base64image = %@",base64image);
        if (base64image && [line hasPrefix:@"  "]) {
            NSString *trimmedLine = [line stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            base64image = [base64image stringByAppendingString:trimmedLine];
        }
        
        if (base64image) {
            // finished contatenating image string
            [self parseImage];
        } 
    } 
    else if ([line hasPrefix:@"TEL;"]) {
        [self parseTel:line];
    }
}

- (void)parseLine:(NSString *)line {
    if (base64image && [line hasPrefix:@"  "]) {
        NSString *trimmedLine = [line stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        base64image = [base64image stringByAppendingString:trimmedLine];
    } else if (base64image) {
        // finished contatenating image string
        [self parseImage];
    } else if ([line hasPrefix:@"BEGIN"]) {
        personRecord = ABPersonCreate();
    } else if ([line hasPrefix:@"END"]) {
        ABAddressBookAddRecord(addressBook,personRecord, nil); //NULL
    } else if ([line hasPrefix:@"N:"]) {
        [self parseName:line];
    } else if ([line hasPrefix:@"EMAIL;"]) {
        [self parseEmail:line];
    } else if ([line hasPrefix:@"PHOTO;BASE64:"]) {
        base64image = [NSString string];
    } else if ([line hasPrefix:@"TEL;"]) {
        [self parseTel:line];
    }
}

- (void)parseName:(NSString *)line {
    NSArray *upperComponents = [line componentsSeparatedByString:@":"];
    NSArray *components = [[upperComponents objectAtIndex:1] componentsSeparatedByString:@";"];
    ABRecordSetValue (personRecord, kABPersonLastNameProperty,[components objectAtIndex:0], NULL);
    ABRecordSetValue (personRecord, kABPersonFirstNameProperty,[components objectAtIndex:1], NULL);
    //ABRecordSetValue (personRecord, kABPersonPrefixProperty,[components objectAtIndex:3], NULL);
}

- (void)parseEmail:(NSString *)line {
    NSArray *mainComponents = [line componentsSeparatedByString:@":"];
    NSString *emailAddress = [mainComponents objectAtIndex:1];
    CFStringRef label;
    ABMutableMultiValueRef multiEmail;
    
    if ([line rangeOfString:@"WORK"].location != NSNotFound) {
        label = kABWorkLabel;
    } else if ([line rangeOfString:@"HOME"].location != NSNotFound) {
        label = kABHomeLabel;
    } else {
        label = kABOtherLabel;
    }
    
    ABMultiValueRef immutableMultiEmail = ABRecordCopyValue(personRecord, kABPersonEmailProperty);
    if (immutableMultiEmail) {
        multiEmail = ABMultiValueCreateMutableCopy(immutableMultiEmail);
    } else {
        multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    }
    ABMultiValueAddValueAndLabel(multiEmail, emailAddress, label, NULL);
    ABRecordSetValue(personRecord, kABPersonEmailProperty, multiEmail,nil);
    
    CFRelease(multiEmail);
    if (immutableMultiEmail) {
        CFRelease(immutableMultiEmail);
    }
}

- (void)parseTel:(NSString *)line {
    NSArray *mainComponents = [line componentsSeparatedByString:@":"];
    NSString *phoneNumber = [mainComponents objectAtIndex:1];
    CFStringRef label;
    ABMutableMultiValueRef multiPhone;
    
    if ([line rangeOfString:@"CELL"].location != NSNotFound) {
        label = kABPersonPhoneMobileLabel;
    } 
    else if ([line rangeOfString:@"IPHONE"].location != NSNotFound) {
        label = kABPersonPhoneIPhoneLabel;
    } 
    else if ([line rangeOfString:@"HOME"].location != NSNotFound) {
        label = kABHomeLabel;
    }
    else if ([line rangeOfString:@"WORK"].location != NSNotFound) {
        label = kABWorkLabel;
    }
    else if ([line rangeOfString:@"MAIN"].location != NSNotFound) {
        label = kABPersonPhoneMainLabel;
    }
    else if (([line rangeOfString:@"HOME"].location != NSNotFound) && ([line rangeOfString:@"FAX"].location != NSNotFound)) {
        label = kABPersonPhoneHomeFAXLabel;
    }
    else if (([line rangeOfString:@"WORK"].location != NSNotFound) && ([line rangeOfString:@"FAX"].location != NSNotFound)) {
        label = kABPersonPhoneWorkFAXLabel;
    }
    else if ([line rangeOfString:@"PAGER"].location != NSNotFound) {
        label = kABPersonPhonePagerLabel;
    }
    else {
        label = kABOtherLabel;
    }
    
    ABMultiValueRef immutableMultiPhone = ABRecordCopyValue(personRecord, kABPersonPhoneProperty);
    if (immutableMultiPhone) {
        multiPhone = ABMultiValueCreateMutableCopy(immutableMultiPhone);
    } else {
        multiPhone = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    }
    ABMultiValueAddValueAndLabel(multiPhone, phoneNumber, label, NULL);
    ABRecordSetValue(personRecord, kABPersonPhoneProperty, multiPhone,nil);
    
    CFRelease(multiPhone);
    if (immutableMultiPhone) {
        CFRelease(immutableMultiPhone);
    }
}

/*
//--
- (IBAction)getVcardInfoButtonPressed:(id)sender
{
    
    NSString *str=[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@”b” ofType:@”txt”]];
    
    NSArray *subStrings = [str componentsSeparatedByCharactersInSet:
                           
                           [NSCharacterSet characterSetWithCharactersInString:@"\n"]];
    
    NSArray *getiingFNData=[[NSArray alloc]init];
    
    NSArray *arr=[[NSArray alloc]init];
    
    for (int i=0;i<[subStrings count];i++)
        
    {
        
        arr=[subStrings objectAtIndex:i];
        
        if(i==4)
            
        {
            
            NSArray *abc=[arr componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
            
            telePhone.text=[abc objectAtIndex:1];
            
        }
        
        else
            
        {
            
            NSArray *abc=[arr componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@";"]];
            
            if([[abc objectAtIndex:0] isEqualToString:@”FN”])
                
            {
                
                getiingFNData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@":"] ];
                
                fullName.text=[getiingFNData objectAtIndex:1];
                
            }
            
            else if([[abc objectAtIndex:0] isEqualToString:@”N”])
                
            {
                
                getiingFNData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet:  [NSCharacterSet characterSetWithCharactersInString:@":"]]; 
                
                lastName.text=[getiingFNData objectAtIndex:1];
                
                firstName.text=[abc objectAtIndex:2];
                
            }
            
            else if([[abc objectAtIndex:0] isEqualToString:@”TITLE”])
                
            {
                
                getiingFNData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet:  [NSCharacterSet characterSetWithCharactersInString:@":"]]; 
                
                jobTitle.text=[getiingFNData objectAtIndex:1];
                
            }
            
            else if([[abc objectAtIndex:0] isEqualToString:@”TEL”])
                
            {
                
                if([abc count]==3)
                    
                {
                    
                    getiingFNData=[[abc objectAtIndex:2] componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@":"]]; 
                    
                    if([[abc objectAtIndex:1] isEqualToString:@”WORK”] && [[getiingFNData objectAtIndex:0] isEqualToString:@”FAX”])
                        
                    {
                        
                        workFaxNumber.text=[getiingFNData objectAtIndex:1];
                        
                    }
                    
                }
                
                else
                    
                {
                    
                    getiingFNData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet:  [NSCharacterSet characterSetWithCharactersInString:@":"]]; 
                    
                    if([[getiingFNData objectAtIndex:0] isEqualToString:@”WORK”])
                        
                    {
                        
                        workTel.text = [getiingFNData objectAtIndex:1];
                        
                    }
                    
                    else if([[getiingFNData objectAtIndex:0] isEqualToString:@”FAX”])
                        
                    {
                        
                        faxNumber.text = [getiingFNData objectAtIndex:1];
                        
                    }
                    
                }
                
            }
            
            else if([[abc objectAtIndex:0] isEqualToString:@”EMAIL”])
                
            {
                
                getiingFNData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@":"]]; 
                
                emailAddress.text=[getiingFNData objectAtIndex:1];
                
            }   
            
            else if([[abc objectAtIndex:0] isEqualToString:@”ORG”])
                
            {
                
                getiingFNData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet:  [NSCharacterSet characterSetWithCharactersInString:@":"]]; 
                
                orgName.text=[getiingFNData objectAtIndex:1];
                
            }  
            
            else if([[abc objectAtIndex:0] isEqualToString:@”ADR”])
                
            {
                
                if([abc count]==9)
                    
                {
                    
                    adres.text=[abc objectAtIndex:3];
                    
                    //cuntry.text=[[abc objectAtIndex:5] stringByAppendingString:[abc objectAtIndex:6]]; 
                    
                    cuntry.text=[abc objectAtIndex:7];
                    
                    zipCode.text=[abc objectAtIndex:6];
                    
                    cityName.text=[abc objectAtIndex:4];
                    
                }
                
            }
            
            else if([[abc objectAtIndex:0] isEqualToString:@”URL”])
                
            {
                
                getiingFNData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@":"]]; 
                
                webURL.text=[getiingFNData objectAtIndex:1];
                
            } 
            
            else
                
            {
                
            }
            
        }
        
        //NSLog(@”Index=%d , Value=%@”,i,[subStrings objectAtIndex:i]);
        
    }
    
}
//--
 */

- (void)parseImage {
    [Base64 initialize];
    NSData *imageData = [Base64 decode:base64image];
    base64image = nil;
    ABPersonSetImageData(personRecord, (CFDataRef)imageData, NULL);
}

- (void)emptyAddressBook {
    CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBook);
    int arrayCount = CFArrayGetCount(people);
    ABRecordRef abrecord;
    
    for (int i = 0; i < arrayCount; i++) {
        abrecord = CFArrayGetValueAtIndex(people, i);
        ABAddressBookRemoveRecord(addressBook,abrecord, NULL);
    }
    CFRelease(people);
}
@end
