//
//  ABContactController.h
//  Aconnect
//
//  Created by linkit on 4/26/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DBRestClient;

@interface ABContactController : NSObject {
    DBRestClient* restClient;
    NSString* contactsHash;
    NSArray* contactPaths;
    NSString* currentContactPath;
    
    id delegate;
	SEL callback;
	SEL errorCallback;
    
    int totalContact;
    int counter;
    
    BOOL willImportContacts;
}

@property (nonatomic, retain) id delegate;
@property (nonatomic) SEL callback;
@property (nonatomic) SEL errorCallback;

- (void)exportContacts:(id)theDelegate
          withSelector:(SEL)theSelector;

- (void)getListFile:(id)theDelegate 
       withSelector:(SEL)theSelector;

- (void)importContacts:(id)theDelegate
          withSelector:(SEL)theSelector;

- (void)restoreContacts:(NSArray*)selectedContacts
           withDelegate:(id)theDelegate
           withSelector:(SEL)theSelector;

@end
