//
//  ABContactsCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/27/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ABContactsCell.h"

@implementation ABContactsCell

@synthesize filenameLabel;
@synthesize sizeLabel;
@synthesize imageView;
@synthesize checkMark;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

@end
