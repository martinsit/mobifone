//
//  ABContactsCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/27/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABContactsCell : UITableViewCell {
    UILabel *filenameLabel;
    UILabel *sizeLabel;
    UIImageView *imageView;
    UIImageView *checkMark;
}

@property (nonatomic, retain) IBOutlet UILabel *filenameLabel;
@property (nonatomic, retain) IBOutlet UILabel *sizeLabel;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UIImageView *checkMark;

@end
