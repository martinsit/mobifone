//
//  ABContactsViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "ABContactsCell.h"

@interface ABContactsViewController : BasicViewController <UIAlertViewDelegate, UISearchDisplayDelegate,UISearchBarDelegate> {
    BOOL isImport;
    IBOutlet ABContactsCell *_cell;
}

@property (readwrite) BOOL isImport;

@end
