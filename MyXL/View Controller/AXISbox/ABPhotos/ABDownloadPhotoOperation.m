//
//  ABDownloadPhotoOperation.m
//  Aconnect
//
//  Created by linkit on 5/22/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import "ABDownloadPhotoOperation.h"
#import <DropboxSDK/DropboxSDK.h>
#import <stdlib.h>

@interface ABDownloadPhotoOperation () <DBRestClientDelegate>

- (NSString*)createTempVCardPath:(NSString*)fileName;

@property (nonatomic, readonly) DBRestClient* restClient;

@end

@implementation ABDownloadPhotoOperation

@synthesize contactPath = contactPath_;
@synthesize theProgress = theProgress_;
@synthesize success = success_;
@synthesize aImage = aImage_;

#pragma mark -
#pragma mark Initialization & Memory Management

- (DBRestClient*)restClient {
    if (restClient == nil) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

- (id)initWithPath:(NSString*)path {
    if( (self = [super init]) ) {
        contactPath_ = path;
    }
    return self;
}

- (void)dealloc {
    [contactPath_ release];
    contactPath_ = nil;
    [super dealloc];
}

#pragma mark -
#pragma mark Start & Utility Methods

- (void)done {
    // Alert anyone that we are finished
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    executing_ = NO;
    finished_  = YES;
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)canceled {
	// Code for being cancelled
    [self done];
}

- (void)start {
    // Ensure that this operation starts on the main thread
    if (![NSThread isMainThread])
    {
        [self performSelectorOnMainThread:@selector(start)
                               withObject:nil waitUntilDone:NO];
        return;
    }
    
    // Ensure that the operation should exute
    if ( finished_ || [self isCancelled] ) { 
        [self done]; 
        return; 
    }
    
    // From this point on, the operation is officially executing--remember, isExecuting
    // needs to be KVO compliant!
    [self willChangeValueForKey:@"isExecuting"];
    executing_ = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    // download file
    NSString *aFileName = [contactPath_ lastPathComponent];
    [self.restClient loadFile:contactPath_ intoPath:[self createTempVCardPath:aFileName]];
}

- (void)onProgress {
    [self willChangeValueForKey:@"isExecuting"];
    executing_ = YES;
    [self didChangeValueForKey:@"isExecuting"];
}

#pragma mark -
#pragma mark Overrides

- (BOOL)isConcurrent {
    return YES;
}

- (BOOL)isExecuting {
    return executing_;
}

- (BOOL)isFinished {
    return finished_;
}

#pragma mark - Download Callback

- (void)restClient:(DBRestClient*)client loadedFile:(NSString*)destPath contentType:(NSString*)contentType metadata:(DBMetadata*)metadata {
    
    aImage_ = [UIImage imageWithContentsOfFile:destPath];
    
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
        success_ = YES;
		[self done];
	}
}

- (void)restClient:(DBRestClient*)client loadFileFailedWithError:(NSError*)error {
    //NSLog(@"restClient:loadFileFailedWithError: %@", [error localizedDescription]);
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
        success_ = NO;
		[self done];
	}
}

- (void)restClient:(DBRestClient*)client loadProgress:(CGFloat)progress forFile:(NSString*)destPath {
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
        theProgress_ = progress;
		[self onProgress];
	}
}

#pragma mark - Private Method

- (NSString*)createTempVCardPath:(NSString*)fileName {
    return [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
}

@end
