//
//  ABExportPhotosCell.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ABExportPhotosCell.h"
#import "Constant.h"

@implementation ABExportPhotosCell

@synthesize filenameLabel;
@synthesize sizeLabel;
@synthesize imageView;
@synthesize progressView;
@synthesize checkmark;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        filenameLabel = [[UILabel alloc] init];
        filenameLabel.textAlignment = UITextAlignmentLeft;
        filenameLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        filenameLabel.textColor = kDefaultTitleFontGrayColor;
        filenameLabel.backgroundColor = [UIColor clearColor];
        
        sizeLabel = [[UILabel alloc] init];
        sizeLabel.textAlignment = UITextAlignmentLeft;
        sizeLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        sizeLabel.textColor = kDefaultPurpleColor;
        sizeLabel.backgroundColor = [UIColor clearColor];
        
        imageView = [[UIImageView alloc] init];
        imageView.backgroundColor = [UIColor clearColor];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        //progressView.trackTintColor = [UIColor purpleColor];
        
        checkmark = [[UIImageView alloc] init];
        checkmark.backgroundColor = [UIColor clearColor];
        checkmark.contentMode = UIViewContentModeScaleAspectFit;
        
        [self.contentView addSubview:filenameLabel];
        [self.contentView addSubview:sizeLabel];
        [self.contentView addSubview:imageView];
        [self.contentView addSubview:progressView];
        [self.contentView addSubview:checkmark];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect contentRect = self.contentView.bounds;
    
    CGFloat leftPadding = 30.0;
    CGFloat topPadding = 10.0;
    
    //CGFloat height = contentRect.size.height - (topPadding*2);
    
    CGFloat titleLabelHeight = 20.0;
    
    CGRect frame;
    
    frame = CGRectMake(leftPadding,
                       topPadding,
                       40.0,
                       40.0);
    imageView.frame = frame;
    
    frame = CGRectMake(leftPadding + imageView.frame.size.width + 10,
                       topPadding,
                       contentRect.size.width - (leftPadding + imageView.frame.size.width + 10) - leftPadding,
                       titleLabelHeight);
    filenameLabel.frame = frame;
    //[filenameLabel sizeToFit];
    
    progressView.frame = frame;
    
    frame = CGRectMake(leftPadding + imageView.frame.size.width + 10,
                       topPadding + filenameLabel.frame.size.height,
                       contentRect.size.width - (leftPadding + imageView.frame.size.width + 10) - leftPadding,
                       titleLabelHeight);
    sizeLabel.frame = frame;
    [sizeLabel sizeToFit];
    
    frame = CGRectMake(filenameLabel.frame.origin.x + filenameLabel.frame.size.width - 20,
                       topPadding + filenameLabel.frame.size.height,
                       20,
                       20);
    checkmark.frame = frame;
}

- (void)dealloc
{
    [filenameLabel release];
    [sizeLabel release];
    [imageView release];
    [progressView release];
    [checkmark release];
    [super dealloc];
}

@end
