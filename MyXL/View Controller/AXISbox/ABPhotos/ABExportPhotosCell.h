//
//  ABExportPhotosCell.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/1/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABExportPhotosCell : UITableViewCell {
    UILabel *filenameLabel;
    UILabel *sizeLabel;
    UIImageView *imageView;
    UIProgressView *progressView;
    UIImageView *checkmark;
}

@property (nonatomic, retain) UILabel *filenameLabel;
@property (nonatomic, retain) UILabel *sizeLabel;
@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) UIProgressView *progressView;
@property (nonatomic, retain) UIImageView *checkmark;

@end
