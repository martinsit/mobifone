//
//  ABPhotoController.h
//  Aconnect
//
//  Created by linkit on 4/26/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DBRestClient;
@class ABFileModel;

@protocol ABPhotoControllerDelegate;

@interface ABPhotoController : NSObject {
    DBRestClient* restClient;
    NSString* photosHash;
    NSArray* photoPaths;
    NSString* currentPhotoPath;
    BOOL isGetList;
    
    id delegate;
	SEL callback;
	SEL errorCallback;
    
    int totalPhoto;
    int counter;
    
    ABFileModel *abFileData;
    NSIndexPath *indexPathInTableView;
    id <ABPhotoControllerDelegate> delegatePhoto;
    BOOL isDownloadPhotoForList;
}

@property (nonatomic, retain) id delegate;
@property (nonatomic) SEL callback;
@property (nonatomic) SEL errorCallback;

@property (nonatomic, retain) ABFileModel *abFileData;
@property (nonatomic, retain) NSIndexPath *indexPathInTableView;
@property (nonatomic, assign) id <ABPhotoControllerDelegate> delegatePhoto;

- (void)getListFile:(id)theDelegate 
       withSelector:(SEL)theSelector;

- (void)loadPhoto:(NSUInteger)index
     withDelegate:(id)theDelegate 
     withSelector:(SEL)theSelector;

- (void)exportPhotos:(NSArray*)photos
        isFromCamera:(BOOL)fromCamera
        withDelegate:(id)theDelegate
        withSelector:(SEL)theSelector;

- (void)downloadPhoto:(NSString*)size;

@end

@protocol ABPhotoControllerDelegate

- (void)photoDidLoad:(NSIndexPath *)indexPath;

@end
