//
//  ABImportPhotosViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/28/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ABImportPhotosViewController.h"

#import "ABImportPhotosCell.h"
#import "AXISnetCellBackground.h"
#import "SectionHeaderView.h"
#import "SectionFooterView.h"
#import "Constant.h"

#import "DataManager.h"
#import "ABFileModel.h"
#import "ABPhotoController.h"
#import "ABShowPhotoViewController.h"

@interface ABImportPhotosViewController () <ABPhotoControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@property (nonatomic, retain) NSMutableDictionary *imageDownloadsInProgress;
@property (readwrite) BOOL fromShowPhoto;
@property (nonatomic, retain) ABPhotoController *photoController;

- (void)startDownloadImage:(ABFileModel*)abFileData forIndexPath:(NSIndexPath *)indexPath;
- (void)loadImagesForOnscreenRows;

@end

@implementation ABImportPhotosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)viewWillAppear:(BOOL)animated {
    
    if (!_fromShowPhoto) {
        self.imageDownloadsInProgress = [NSMutableDictionary dictionary];
        
        DataManager *sharedData = [DataManager sharedInstance];
        
        if ([sharedData.dropboxFileData count] > 0) {
            NSMutableArray *tempArray = [NSMutableArray arrayWithArray:sharedData.dropboxFileData];
            [tempArray removeAllObjects];
            sharedData.dropboxFileData = tempArray;
            [_theTableView reloadData];
        }
        
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        ABPhotoController *aPhotoController = [[ABPhotoController alloc] init];
        self.photoController = aPhotoController;
        [self.photoController getListFile:self withSelector:@selector(getListCallback:)];
        [aPhotoController release];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    // Release any cached data, images, etc that aren't in use.
    //NSArray *allDownloads = [self.imageDownloadsInProgress allValues];
    //[allDownloads makeObjectsPerformSelector:@selector(cancelDownload)];
}

#pragma mark - Callback

- (void)getListCallback:(NSNumber*)respond {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    BOOL success = [respond boolValue];
    if (success) {
        [_theTableView reloadData];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Photos"
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
        [alert release];
        [_theTableView reloadData];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DataManager *sharedData = [DataManager sharedInstance];
    return [sharedData.dropboxFileData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    ABImportPhotosCell *cell = (ABImportPhotosCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
        cell = [[ABImportPhotosCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.backgroundView = [[[AXISnetCellBackground alloc] initWithFrame:cell.frame
                                                           withDefaultColor:kDefaultLightPinkColor
                                                          withSelectedColor:kDefaultDarkPinkColor] autorelease];
        
        cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] initWithFrame:cell.frame
                                                                   withDefaultColor:kDefaultLightPinkColor
                                                                  withSelectedColor:kDefaultDarkPinkColor] autorelease];
        
        ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
	}
    
    DataManager *sharedData = [DataManager sharedInstance];
    ABFileModel *aFileData = [sharedData.dropboxFileData objectAtIndex:indexPath.row];
    cell.filenameLabel.text = aFileData.filename;
    cell.sizeLabel.text = aFileData.humanReadableSize;
    
    if (!aFileData.photo) {
		if (_theTableView.dragging == NO && _theTableView.decelerating == NO) {
			[self startDownloadImage:aFileData forIndexPath:indexPath];
		}
		// if a download is deferred or in progress, return a placeholder image
		cell.imageView.image = [UIImage imageNamed:@"photo.png"];
	}
	else {
		cell.imageView.image = aFileData.photo;
	}
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[Language get:@"import_photos" alter:nil] uppercaseString];
    //return self.headerTitleMaster;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //return kDefaultCellHeight;
    return 60.0;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _fromShowPhoto = YES;
    ABShowPhotoViewController *detailViewController = [[ABShowPhotoViewController alloc] initWithNibName:@"ABShowPhotoViewController" bundle:nil];
    detailViewController.photoIndex = indexPath.row;
    [self.navigationController pushViewController:detailViewController animated:YES];
    detailViewController = nil;
    [detailViewController release];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:NO] autorelease];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

#pragma mark - Selector

- (void)startDownloadImage:(ABFileModel*)abFileData forIndexPath:(NSIndexPath *)indexPath {
    ABPhotoController *imageDownloader = [_imageDownloadsInProgress objectForKey:indexPath];
    if (imageDownloader == nil)
    {
        imageDownloader = [[ABPhotoController alloc] init];
        imageDownloader.abFileData = abFileData;
        imageDownloader.indexPathInTableView = indexPath;
        imageDownloader.delegatePhoto = self;
        [_imageDownloadsInProgress setObject:imageDownloader forKey:indexPath];
        [imageDownloader downloadPhoto:@"medium"];
        [imageDownloader release];
    }
}

// this method is used in case the user scrolled into a set of cells that don't have their app icons yet
- (void)loadImagesForOnscreenRows
{
    DataManager *sharedData = [DataManager sharedInstance];
    if ([sharedData.dropboxFileData count] > 0)
    {
        NSArray *visiblePaths = [_theTableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            DataManager *sharedData = [DataManager sharedInstance];
            ABFileModel *aFileData = [sharedData.dropboxFileData objectAtIndex:indexPath.row];
            if (!aFileData.photo) // avoid the app icon download if the app already has an icon
            {
                [self startDownloadImage:aFileData forIndexPath:indexPath];
            }
        }
    }
}

#pragma mark -
#pragma mark Deferred image loading (UIScrollViewDelegate)

// Load images for all onscreen rows when scrolling is finished
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}

#pragma mark - Download Photo Callback

// called by our ImageDownloader when an icon is ready to be displayed
- (void)photoDidLoad:(NSIndexPath *)indexPath {
    ABPhotoController *imageDownloader = [_imageDownloadsInProgress objectForKey:indexPath];
    if (imageDownloader != nil)
    {
        ABImportPhotosCell *cell = (ABImportPhotosCell*)[_theTableView cellForRowAtIndexPath:imageDownloader.indexPathInTableView];
        
        // Display the newly loaded image
        cell.imageView.image = imageDownloader.abFileData.photo;
    }
}

@end
