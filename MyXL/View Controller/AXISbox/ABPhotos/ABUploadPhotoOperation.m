//
//  ABUploadPhotoOperation.m
//  Aconnect
//
//  Created by linkit on 5/18/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import "ABUploadPhotoOperation.h"
#import <DropboxSDK/DropboxSDK.h>
#import <stdlib.h>

@interface ABUploadPhotoOperation () <DBRestClientDelegate>

- (void)writeImageToFile:(UIImage*)image withFileName:(NSString*)aFileName;
- (NSString *)getDocumentPath:(NSString*)aFileName;
- (void)deleteFile:(NSString*)aFileName;

@property (nonatomic, readonly) DBRestClient* restClient;

@end

@implementation ABUploadPhotoOperation

@synthesize photo = photo_;
@synthesize fileName = fileName_;
@synthesize theProgress = theProgress_;
@synthesize theIndexPath = theIndexPath_;
@synthesize success = success_;

#pragma mark -
#pragma mark Initialization & Memory Management

- (DBRestClient*)restClient {
    if (restClient == nil) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

- (id)initWithPhoto:(UIImage*)thePhoto withFileName:(NSString*)aFileName {
    if( (self = [super init]) ) {
        photo_ = [thePhoto copy];
        fileName_ = [aFileName copy];
    }
    return self;
}

- (id)initWithPhoto:(UIImage*)thePhoto withFileName:(NSString*)aFileName forIndexPath:(NSIndexPath*)indexPath {
    if( (self = [super init]) ) {
        photo_ = [thePhoto copy];
        fileName_ = [aFileName copy];
        theIndexPath_ = [indexPath copy];
    }
    return self;
}

- (void)dealloc {
    [photo_ release];
    photo_ = nil;
    [fileName_ release];
    fileName_ = nil;
    [theIndexPath_ release];
    theIndexPath_ = nil;
    [super dealloc];
}

#pragma mark -
#pragma mark Start & Utility Methods

- (void)done {
    // Alert anyone that we are finished
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    executing_ = NO;
    finished_  = YES;
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)canceled {
	// Code for being cancelled
    [self done];
}

- (void)start {
    // Ensure that this operation starts on the main thread
    if (![NSThread isMainThread])
    {
        [self performSelectorOnMainThread:@selector(start)
                               withObject:nil waitUntilDone:NO];
        return;
    }
    
    // Ensure that the operation should exute
    if ( finished_ || [self isCancelled] ) { 
        [self done]; 
        return; 
    }
    
    // From this point on, the operation is officially executing--remember, isExecuting
    // needs to be KVO compliant!
    [self willChangeValueForKey:@"isExecuting"];
    executing_ = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    // process here...
    //
    // write Image to File
    [self writeImageToFile:photo_ withFileName:fileName_];
    
    // get Doc Path
    NSString *docPath = [self getDocumentPath:fileName_];
    
    // upload file
    [[self restClient] uploadFile:fileName_ toPath:@"/Photos/" withParentRev:nil fromPath:docPath];
}

- (void)onProgress {
    [self willChangeValueForKey:@"isExecuting"];
    executing_ = YES;
    [self didChangeValueForKey:@"isExecuting"];
}

#pragma mark -
#pragma mark Overrides

- (BOOL)isConcurrent {
    return YES;
}

- (BOOL)isExecuting {
    return executing_;
}

- (BOOL)isFinished {
    return finished_;
}

#pragma mark - Upload Callback

- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath
              from:(NSString*)srcPath metadata:(DBMetadata*)metadata {
    //NSLog(@"File upload from camera success");
    NSString *aFileName = [destPath lastPathComponent];
    [self deleteFile:aFileName];
    
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
        success_ = YES;
		[self done];
	}
}

- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error {
    //NSLog(@"File upload from camera failed with error - %@", error);
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
        success_ = NO;
		[self done];
	}
}

- (void)restClient:(DBRestClient*)client uploadProgress:(CGFloat)progress 
           forFile:(NSString*)destPath from:(NSString*)srcPath {
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
        theProgress_ = progress;
		[self onProgress];
	}
}

#pragma mark - Private Method

- (void)writeImageToFile:(UIImage*)image withFileName:(NSString*)aFileName {
    // Build the path, and create if needed.
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //NSString* fileName = aFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:aFileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    // The main act.
    NSData *imgData = UIImageJPEGRepresentation(image, 1);
    [imgData writeToFile:fileAtPath atomically:NO];
}

- (NSString *)getDocumentPath:(NSString*)aFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:aFileName];
    return path;
}

- (void)deleteFile:(NSString*)aFileName {
    // Build the path, and create if needed.
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //NSString* fileName = aFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:aFileName];
    
    NSError *error;
    
    if ([[NSFileManager defaultManager] removeItemAtPath:fileAtPath error:&error] != YES)
        NSLog(@"Unable to delete file: %@", [error localizedDescription]);
//    else
//        NSLog(@"Delete File Success");
}

@end
