//
//  ABExportPhotosViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/28/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface ABExportPhotosViewController : BasicViewController {
    NSArray *exportPhotoData;
    NSArray *progressData;
    
    NSOperationQueue *operationQueue;
}

@property (nonatomic, retain) NSArray *exportPhotoData;
@property (nonatomic, retain) NSArray *progressData;

@end
