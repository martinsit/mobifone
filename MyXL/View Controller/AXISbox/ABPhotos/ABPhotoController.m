//
//  ABPhotoController.m
//  Aconnect
//
//  Created by linkit on 4/26/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import "ABPhotoController.h"
#import <DropboxSDK/DropboxSDK.h>
#import <stdlib.h>
#import "ABFileModel.h"
#import "DataManager.h"

#import "AXISnetCommon.h"

#import "HumanReadableDataSizeHelper.h"

@interface ABPhotoController () <DBRestClientDelegate>

// export photos
- (NSString*)getFilename:(BOOL)fromCamera withIndex:(int)index;
- (void)writeImageToFile:(UIImage*)image withFileName:(NSString*)aFileName;
- (NSString *)getDocumentPath:(NSString*)aFileName;
- (void)uploadFile:(NSString*)aFileName to:(NSString*)toPath from:(NSString*)fromPath;
- (void)deleteFile:(NSString*)aFileName;

// import photos
- (NSString*)photoPath;
- (NSString*)createTempPhotoPath:(NSString*)fileName;

@property (nonatomic, readonly) DBRestClient* restClient;

@end

@implementation ABPhotoController

@synthesize delegate;
@synthesize callback;
@synthesize errorCallback;

@synthesize abFileData;
@synthesize indexPathInTableView;
@synthesize delegatePhoto;

- (void)dealloc {
    [restClient release];
    [photosHash release];
    [photoPaths release];
    [currentPhotoPath release];
    
    [abFileData release];
    [indexPathInTableView release];
	[super dealloc];
}

- (DBRestClient*)restClient {
    if (restClient == nil) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

#pragma mark - Import Photos

- (void)getListFile:(id)theDelegate 
       withSelector:(SEL)theSelector {
    
    //[self setWorking:YES];
    self.delegate = theDelegate;
    self.callback = theSelector;
    
    isGetList = YES;
    
    NSString *photosRoot = nil;
    if ([DBSession sharedSession].root == kDBRootDropbox) {
        photosRoot = @"/Photos";
        //NSLog(@"/Photos");
    } else {
        photosRoot = @"/";
        //NSLog(@"/");
    }
    
    [self.restClient loadMetadata:photosRoot withHash:photosHash];
}

- (void)loadPhoto:(NSUInteger)index
     withDelegate:(id)theDelegate 
     withSelector:(SEL)theSelector {
    
    self.delegate = theDelegate;
    self.callback = theSelector;
    
    isDownloadPhotoForList = NO;
    
    DataManager *sharedData = [DataManager sharedInstance];
    ABFileModel *aFileData = [sharedData.dropboxFileData objectAtIndex:index];
    
    [self.restClient loadThumbnail:aFileData.path ofSize:@"iphone_bestfit" intoPath:[self photoPath]];
}

- (NSString*)photoPath {
    return [NSTemporaryDirectory() stringByAppendingPathComponent:@"photo.jpg"];
}

- (void)downloadPhoto:(NSString*)size {
    isDownloadPhotoForList = YES;
    DataManager *sharedData = [DataManager sharedInstance];
    ABFileModel *aFileData = [sharedData.dropboxFileData objectAtIndex:indexPathInTableView.row];
    
    NSString *aFileName = [aFileData.path lastPathComponent];
    
    //[self.restClient loadThumbnail:aFileData.path ofSize:@"iphone_bestfit" intoPath:[self createTempPhotoPath:aFileName]];
    [self.restClient loadThumbnail:aFileData.path ofSize:size intoPath:[self createTempPhotoPath:aFileName]];
}

- (NSString*)createTempPhotoPath:(NSString*)fileName {
    return [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
}

#pragma mark - Export Photos

- (void)exportPhotos:(NSArray*)photos
        isFromCamera:(BOOL)fromCamera
        withDelegate:(id)theDelegate
        withSelector:(SEL)theSelector {
    self.delegate = theDelegate;
    self.callback = theSelector;
    
    totalPhoto = [photos count];
    counter = 0;
    
    for (int i=0; i<[photos count]; i++) {
        //NSLog(@"[photos objectAtIndex:i] = %@",[photos objectAtIndex:i]);
        // get file name here
        NSString *aFileName;
        UIImage *photo;
        if (fromCamera) {
            aFileName = [NSString stringWithFormat:@"%@.jpg",[self getFilename:YES withIndex:i]];
            photo = [photos objectAtIndex:i];
        }
        else {
            NSDictionary *dict = [photos objectAtIndex:i];
            aFileName = [dict objectForKey:@"originalFileName"];
            photo = [dict objectForKey:UIImagePickerControllerOriginalImage];
        }
        
        // write Image to File
        [self writeImageToFile:photo withFileName:aFileName];
        
        // get Doc Path
        NSString *docPath = [self getDocumentPath:aFileName];
        
        //NSLog(@"upload file ke-%i namanya = %@",i,aFileName);
        
        // upload file
        [self uploadFile:aFileName to:@"/Photos/" from:docPath];
    }
}

- (NSString*)getFilename:(BOOL)fromCamera withIndex:(int)index {
    NSString *fileName = @"";
    if (fromCamera) {
        fileName = [NSString stringWithFormat:@"Camera_%@",generateUniqueString()];
    }
    else {
        
    }
    //NSLog(@"fileName = %@",fileName);
    return fileName;
}

- (void)writeImageToFile:(UIImage*)image withFileName:(NSString*)aFileName {
    // Build the path, and create if needed.
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = aFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    // The main act.
    NSData *imgData = UIImageJPEGRepresentation(image, 1);
    [imgData writeToFile:fileAtPath atomically:NO];
    
    //tambahan
    /*
    NSDictionary * attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:fileAtPath error:nil];
    
    // file size
    NSNumber *theFileSize = [attributes objectForKey:NSFileSize];
    NSLog(@"theFileSize = %f",[theFileSize floatValue]);
    NSString *strImageSize = [HumanReadableDataSizeHelper humanReadableSizeFromBytes:theFileSize
                                                                   useSiPrefixes:YES 
                                                                 useSiMultiplier:NO];
    NSLog(@"strImageSize = %@",strImageSize);*/
}

- (NSString *)getDocumentPath:(NSString*)aFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:aFileName];
    return path;
}

- (void)uploadFile:(NSString*)aFileName to:(NSString*)toPath from:(NSString*)fromPath {
    [[self restClient] uploadFile:aFileName toPath:toPath withParentRev:nil fromPath:fromPath];
}

- (void)deleteFile:(NSString*)aFileName {
    // Build the path, and create if needed.
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = aFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    NSError *error;
    
    if ([[NSFileManager defaultManager] removeItemAtPath:fileAtPath error:&error] != YES)
        NSLog(@"Unable to delete file: %@", [error localizedDescription]);
//    else
//        NSLog(@"Delete File Success");
}

#pragma mark - DBRestClientDelegate methods

- (void)restClient:(DBRestClient*)client loadedMetadata:(DBMetadata*)metadata {
    //NSLog(@"Load Metadata Photo");
    [photosHash release];
    photosHash = [metadata.hash retain];
    
    NSArray* validExtensions = [NSArray arrayWithObjects:@"jpg", @"jpeg", @"png", nil];
    NSMutableArray* newPhotoPaths = [NSMutableArray new];
    
    NSMutableArray *fileDataTmp = [[NSMutableArray alloc] init];
    
    for (DBMetadata* child in metadata.contents) {
        NSString* extension = [[child.path pathExtension] lowercaseString];
        if (!child.isDirectory && [validExtensions indexOfObject:extension] != NSNotFound) {
            [newPhotoPaths addObject:child.path];
            
            ABFileModel *aFileData = [[ABFileModel alloc] init];
            aFileData.filename          = child.filename;
            aFileData.thumbnailExists   = child.thumbnailExists;
            aFileData.totalBytes        = child.totalBytes;
            aFileData.lastModifiedDate  = child.lastModifiedDate;
            aFileData.humanReadableSize = child.humanReadableSize;
            aFileData.icon              = child.icon;
            aFileData.path              = child.path;
            aFileData.rev               = child.rev;
            
            [fileDataTmp addObject:aFileData];
            
            [aFileData release];
            aFileData = nil;
        }
    }
    [photoPaths release];
    photoPaths = newPhotoPaths;
    
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.dropboxFileData = [NSArray arrayWithArray:fileDataTmp];
    [fileDataTmp release];
    
    if (isGetList) {
        if (delegate && callback) {
            if ([delegate respondsToSelector:self.callback]) {
                BOOL respond = YES;
                NSNumber *passedValue = [NSNumber numberWithBool:respond];
                [delegate performSelector:self.callback withObject:passedValue];
            }
            else {
                //NSLog(@"No response from delegate");
            }
        }
    }
    //[self loadRandomPhoto];
}

- (void)restClient:(DBRestClient*)client metadataUnchangedAtPath:(NSString*)path {
    //NSLog(@"Load Metadata Photo metadataUnchangedAtPath");
    //[self loadRandomPhoto];
}

- (void)restClient:(DBRestClient*)client loadMetadataFailedWithError:(NSError*)error {
    //NSLog(@"restClient:loadMetadataFailedWithError: %@", [error localizedDescription]);
    if (isGetList) {
        if (delegate && callback) {
            if ([delegate respondsToSelector:self.callback]) {
                BOOL respond = NO;
                NSNumber *passedValue = [NSNumber numberWithBool:respond];
                [delegate performSelector:self.callback withObject:passedValue];
            }
            else {
                //NSLog(@"No response from delegate");
            }
        }
    }
}

#pragma mark - Upload Callback

- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath
              from:(NSString*)srcPath metadata:(DBMetadata*)metadata {
    NSString *aFileName = [destPath lastPathComponent];
    [self deleteFile:aFileName];
    
    counter++;
    
    /*
    if (counter == totalPhoto) {
        if (delegate && callback) {
            if ([delegate respondsToSelector:self.callback]) {
                NSString *msg = [NSString stringWithFormat:@"File uploaded successfully to path: %@",metadata.path];
                int respond = 1;
                NSNumber *status = [NSNumber numberWithInt:respond];
                
                NSMutableDictionary *infoDictionary = [[NSMutableDictionary alloc] init];
                [infoDictionary setObject:msg forKey:@"message"];
                [infoDictionary setObject:[NSNull null] forKey:@"progress"];
                [infoDictionary setObject:aFileName forKey:@"filename"];
                
                NSNumber *counterObj = [NSNumber numberWithInt:counter];
                [infoDictionary setObject:counterObj forKey:@"counter"];
                
                NSNumber *totalObj = [NSNumber numberWithInt:totalPhoto];
                [infoDictionary setObject:totalObj forKey:@"total"];
                
                [delegate performSelector:self.callback withObject:status withObject:infoDictionary];
                
                [infoDictionary release];
            }
            else {
                //NSLog(@"No response from delegate");
            }
        }
    }*/
    
    if (delegate && callback) {
        if ([delegate respondsToSelector:self.callback]) {
            NSString *msg = [NSString stringWithFormat:@"File uploaded successfully to path: %@",metadata.path];
            int respond = 1;
            NSNumber *status = [NSNumber numberWithInt:respond];
            
            NSMutableDictionary *infoDictionary = [[NSMutableDictionary alloc] init];
            [infoDictionary setObject:msg forKey:@"message"];
            [infoDictionary setObject:[NSNull null] forKey:@"progress"];
            [infoDictionary setObject:aFileName forKey:@"filename"];
            
            NSNumber *counterObj = [NSNumber numberWithInt:counter];
            [infoDictionary setObject:counterObj forKey:@"counter"];
            
            NSNumber *totalObj = [NSNumber numberWithInt:totalPhoto];
            [infoDictionary setObject:totalObj forKey:@"total"];
            
            [delegate performSelector:self.callback withObject:status withObject:infoDictionary];
            
            [infoDictionary release];
        }
        else {
            //NSLog(@"No response from delegate");
        }
    }
}

- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error {
    //[self setWorking:NO];
    //NSLog(@"File upload failed with error - %@", error);
    NSString *msg = [NSString stringWithFormat:@"File upload failed with error - %@",error];
    
    if (delegate && callback) {
        if ([delegate respondsToSelector:self.callback]) {
            int respond = -1;
            NSNumber *status = [NSNumber numberWithInt:respond];
            
            NSMutableDictionary *infoDictionary = [[NSMutableDictionary alloc] init];
            [infoDictionary setObject:msg forKey:@"message"];
            [infoDictionary setObject:[NSNull null] forKey:@"progress"];
            [infoDictionary setObject:[NSNull null] forKey:@"filename"];
            
            [delegate performSelector:self.callback withObject:status withObject:infoDictionary];
            
            [infoDictionary release];
        }
        else {
            //NSLog(@"No response from delegate");
        }
    }
}

- (void)restClient:(DBRestClient*)client uploadProgress:(CGFloat)progress 
           forFile:(NSString*)destPath from:(NSString*)srcPath {
    /*
    if (progress > 1) { //Work-around: This means the progress is not a 
        progress = progress/((CGFloat)(metadataOfTheFile.totalBytes));
    }*/
    //NSLog(@"progress = %f",progress);
    //NSLog(@"destPath = %@",destPath);
    //NSLog(@"srcPath = %@",srcPath);
    
    if (delegate && callback) {
        if ([delegate respondsToSelector:self.callback]) {
            int respond = 0;
            NSNumber *status = [NSNumber numberWithInt:respond];
            NSString *filename = [destPath lastPathComponent];
            NSNumber *progressNum = [NSNumber numberWithFloat:progress];
            
            NSMutableDictionary *infoDictionary = [[NSMutableDictionary alloc] init];
            [infoDictionary setObject:[NSNull null] forKey:@"message"];
            [infoDictionary setObject:progressNum forKey:@"progress"];
            [infoDictionary setObject:filename forKey:@"filename"];
            
            [delegate performSelector:self.callback withObject:status withObject:infoDictionary];
            
            [infoDictionary release];
        }
        else {
            //NSLog(@"No response from delegate");
        }
    }
}

#pragma mark - Download Callback

- (void)restClient:(DBRestClient*)client loadedThumbnail:(NSString*)destPath {
    //[self setWorking:NO];
    //imageView.image = [UIImage imageWithContentsOfFile:destPath];
    
    if (isDownloadPhotoForList) {
        //isDownloadPhotoForList = NO;
        self.abFileData.photo = [UIImage imageWithContentsOfFile:destPath];
        [delegatePhoto photoDidLoad:self.indexPathInTableView];
    }
    else {
        if (delegate && callback) {
            if ([delegate respondsToSelector:self.callback]) {
                [delegate performSelector:self.callback withObject:destPath];
            }
            else {
                //NSLog(@"No response from delegate");
            }
        }
    }
}

- (void)restClient:(DBRestClient*)client loadThumbnailFailedWithError:(NSError*)error {
    //[self setWorking:NO];
    //[self displayError];
}

@end
