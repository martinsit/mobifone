//
//  ABImportPhotosCell.h
//  Aconnect
//
//  Created by linkit on 4/30/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABImportPhotosCell : UITableViewCell {
    UILabel *filenameLabel;
    UILabel *sizeLabel;
    UIImageView *imageView;
}

@property (nonatomic, retain) UILabel *filenameLabel;
@property (nonatomic, retain) UILabel *sizeLabel;
@property (nonatomic, retain) UIImageView *imageView;

@end
