//
//  ABExportPhotosViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/28/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ABExportPhotosViewController.h"

#import "ABExportPhotosCell.h"
#import "AXISnetCellBackground.h"
#import "SectionHeaderView.h"
#import "SectionFooterView.h"
#import "Constant.h"

#import "ABFileModel.h"
#import "HumanReadableDataSizeHelper.h"
#import "ABUploadPhotoOperation.h"

@interface ABExportPhotosViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@end

@implementation ABExportPhotosViewController

@synthesize exportPhotoData;
@synthesize progressData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [exportPhotoData release];
    [progressData release];
    [super dealloc];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.exportPhotoData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    ABExportPhotosCell *cell = (ABExportPhotosCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
        cell = [[ABExportPhotosCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.backgroundView = [[[AXISnetCellBackground alloc] initWithFrame:cell.frame
                                                           withDefaultColor:kDefaultLightPinkColor
                                                          withSelectedColor:kDefaultDarkPinkColor] autorelease];
        
        cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] initWithFrame:cell.frame
                                                                   withDefaultColor:kDefaultLightPinkColor
                                                                  withSelectedColor:kDefaultDarkPinkColor] autorelease];
        
        ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
	}
    
    NSDictionary *dict = [self.exportPhotoData objectAtIndex:indexPath.row];
    cell.filenameLabel.text = [dict objectForKey:@"originalFileName"];
    
    UIImage *theImage = [dict objectForKey:UIImagePickerControllerOriginalImage];
    cell.imageView.image = theImage;
    
    NSData *imageData = UIImageJPEGRepresentation(theImage, 1.0);
    NSUInteger imageSize = [imageData length];
    NSNumber *size = [NSNumber numberWithUnsignedInteger:imageSize];
    NSString *strSize = [HumanReadableDataSizeHelper humanReadableSizeFromBytes:size
                                                                  useSiPrefixes:YES
                                                                useSiMultiplier:NO];
    cell.sizeLabel.text = strSize;
    
    ABFileModel *aFileData = [progressData objectAtIndex:indexPath.row];
    if (!aFileData.isExecuting && !aFileData.isFinished) {
        aFileData.isExecuting = YES;
        [self startExportPhotos:theImage
                   withFileName:[dict objectForKey:@"originalFileName"]
                   forIndexPath:indexPath];
    }
    
    if (aFileData.isFinished) {
        cell.progressView.hidden = YES;
        cell.filenameLabel.hidden = NO;
        cell.checkmark.hidden = NO;
        if (aFileData.isSuccess) {
            cell.checkmark.image = [UIImage imageNamed:@"check_green.png"];
        }
        else {
            cell.checkmark.image = [UIImage imageNamed:@"cross_red.png"];
        }
    }
    else {
        cell.progressView.hidden = NO;
        cell.filenameLabel.hidden = YES;
        cell.checkmark.hidden = YES;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"EXPORT PHOTO";
    //return self.headerTitleMaster;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //return kDefaultCellHeight;
    return 60.0;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:self.headerIconMaster
                                                isFirstSection:NO] autorelease];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

#pragma mark - Selector

- (void)startExportPhotos:(UIImage*)photo withFileName:(NSString*)aFileName forIndexPath:(NSIndexPath*)indexPath {
    if (!operationQueue) {
        // Create operation queue
        operationQueue = [NSOperationQueue new];
        // set maximum operations possible
        [operationQueue setMaxConcurrentOperationCount:5];
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    }
    
    //for (int i=0; i < [exportPhotoData count]; i++) {
    ABUploadPhotoOperation *uploadOperation = [[ABUploadPhotoOperation alloc] initWithPhoto:photo
                                                                               withFileName:aFileName
                                                                               forIndexPath:indexPath];
    [uploadOperation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    // percobaan
    [uploadOperation addObserver:self forKeyPath:@"isExecuting" options:NSKeyValueObservingOptionNew context:NULL];
    //--
    [operationQueue addOperation:uploadOperation]; // operation starts as soon as its added
    [uploadOperation release];
    //}
}

#pragma mark - KVO Observing

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)operation
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if ([operation isKindOfClass:[ABUploadPhotoOperation class]]) {
        ABUploadPhotoOperation *uploadOperation = (ABUploadPhotoOperation *)operation;
        if ([keyPath isEqualToString:@"isExecuting"]) {
            //ABUploadPhotoOperation *uploadOperation = (ABUploadPhotoOperation *)operation;
            CGFloat currentProgress = [uploadOperation theProgress];
            //NSLog(@"currentProgress = %f",currentProgress);
            NSIndexPath *currentIndexPath = [uploadOperation theIndexPath];
            ABExportPhotosCell *cell = (ABExportPhotosCell*)[_theTableView cellForRowAtIndexPath:currentIndexPath];
            cell.progressView.progress = currentProgress;
        }
        else {
            [UIApplication sharedApplication].idleTimerDisabled = NO;
            BOOL success = [uploadOperation success];
            NSIndexPath *currentIndexPath = [uploadOperation theIndexPath];
            ABExportPhotosCell *cell = (ABExportPhotosCell*)[_theTableView cellForRowAtIndexPath:currentIndexPath];
            ABFileModel *aFileData = [progressData objectAtIndex:currentIndexPath.row];
            aFileData.isExecuting = NO;
            aFileData.isFinished = YES;
            if (success) {
                aFileData.isSuccess = YES;
                cell.progressView.progress = 1.0;
                cell.progressView.hidden = YES;
                cell.filenameLabel.hidden = NO;
                cell.checkmark.image = [UIImage imageNamed:@"check_green.png"];
                cell.checkmark.hidden = NO;
            }
            else {
                aFileData.isSuccess = NO;
                cell.progressView.progress = 1.0;
                cell.progressView.hidden = YES;
                cell.filenameLabel.hidden = NO;
                cell.checkmark.image = [UIImage imageNamed:@"cross_red.png"];
                cell.checkmark.hidden = NO;
            }
        }
    }
}

@end
