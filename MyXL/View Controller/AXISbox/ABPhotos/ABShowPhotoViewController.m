//
//  ABShowPhotoViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/28/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ABShowPhotoViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

#import "ABPhotoController.h"

#import "DataManager.h"
#import "ABFileModel.h"
#import "ABDownloadPhotoOperation.h"

@interface ABShowPhotoViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UIImageView *photoView;
@property (strong, nonatomic) UIButton *saveButton;

@property (nonatomic, retain) ABPhotoController *photoController;
@property (nonatomic, retain) NSOperationQueue *operationQueue;

- (void)performSavePhoto;
- (void)saveToPhotoAlbum;

@end

@implementation ABShowPhotoViewController

@synthesize photoIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    
    //--------------//
    // Setup Button //
    //--------------//
    
    // Save Button
    UIFont *titleFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    
    UIButton *button = createButtonWithFrame(CGRectMake(_photoView.frame.origin.x,
                                                        _photoView.frame.origin.y + _photoView.frame.size.height + kDefaultComponentPadding,
                                                        110,
                                                        kDefaultButtonHeight),
                                             
                                             [Language get:@"save_photo" alter:nil],
                                             titleFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
    
    [button addTarget:self action:@selector(performSavePhoto) forControlEvents:UIControlEventTouchUpInside];
    
    button.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    
    _saveButton = button;
    [_contentView addSubview:_saveButton];
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _saveButton.frame.origin.y + _saveButton.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40)
                                                                   withLabel:[[Language get:@"import_photos" alter:nil] uppercaseString]];
    
    [_contentView addSubview:headerView];
    [_contentView sendSubviewToBack:headerView];
    [headerView release];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height + 20.0)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2) + 20.0;
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _greyBackgroundView.frame.origin.y + _greyBackgroundView.frame.size.height + 20.0);
    
    //--
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    ABPhotoController *aPhotoController = [[ABPhotoController alloc] init];
    self.photoController = aPhotoController;
    [self.photoController loadPhoto:photoIndex withDelegate:self withSelector:@selector(loadPhotoCallback:)];
    [aPhotoController release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
//    [photoView release];
//    [bgView release];
//    [saveBtn release];
//    [photoController release];
    [super dealloc];
}

#pragma mark - Callback

- (void)loadPhotoCallback:(NSString*)path {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    _photoView.image = [UIImage imageWithContentsOfFile:path];
    
    CGRect frame = _saveButton.frame;
    frame.origin.y = _photoView.frame.origin.y + _photoView.frame.size.height + kDefaultComponentPadding;
    _saveButton.frame = frame;
    
    //-----------------------//
    // Relayout Content View //
    //-----------------------//
    frame = [_contentView frame];
    frame.size.height = _saveButton.frame.origin.y + _saveButton.frame.size.height + kDefaultPadding;
    [_contentView setFrame:frame];
    
    NSArray *viewsToRemove = [_contentView subviews];
    for (UIView *v in viewsToRemove) {
        if ([v isKindOfClass:[ContentView class]]) {
            [v removeFromSuperview];
        }
    }
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height + 20)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //-------------------------------//
    // Relayout Grey Background View //
    //-------------------------------//
    CGFloat x = _contentView.frame.origin.x - kDefaultLeftPadding;
    CGFloat y = _contentView.frame.origin.y - kDefaultPadding;
    CGFloat width = _contentView.frame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = _contentView.frame.size.height + (kDefaultPadding*2) + 20;
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
}

#pragma mark - Selector

- (void)performSavePhoto {
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:[Language get:@"import_photos_confirmation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"cancel" alter:nil]
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:[Language get:@"save_photo" alter:nil], nil];
    
    // Show the sheet
    [sheet showInView:self.view];
    [sheet release];
}

- (void)saveToPhotoAlbum {
    
    self.hud.mode = MBProgressHUDModeDeterminate;
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    // Create operation queue
    _operationQueue = [NSOperationQueue new];
    // set maximum operations possible
    //[operationQueue setMaxConcurrentOperationCount:10];
    
    DataManager *sharedData = [DataManager sharedInstance];
    ABFileModel *aFileData = [sharedData.dropboxFileData objectAtIndex:photoIndex];
    
    // download operation goes here
    ABDownloadPhotoOperation *downloadOperation = [[ABDownloadPhotoOperation alloc] initWithPath:aFileData.path];
    [downloadOperation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperation addObserver:self forKeyPath:@"isExecuting" options:NSKeyValueObservingOptionNew context:NULL];
    [_operationQueue addOperation:downloadOperation]; // operation starts as soon as its added
    [downloadOperation release];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self saveToPhotoAlbum];
    }
}

#pragma mark - KVO Observing

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)operation
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if ([operation isKindOfClass:[ABDownloadPhotoOperation class]]) {
        ABDownloadPhotoOperation *downloadOperation = (ABDownloadPhotoOperation *)operation;
        if ([keyPath isEqualToString:@"isExecuting"]) {
            CGFloat currentProgress = [downloadOperation theProgress];
            self.hud.progress = currentProgress;
        }
        else {
            [UIApplication sharedApplication].idleTimerDisabled = NO;
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            BOOL success = [downloadOperation success];
            if (success) {
                UIImage* imageToSave = [downloadOperation aImage];
                // Save it to the camera roll / saved photo album
                UIImageWriteToSavedPhotosAlbum(imageToSave, nil, nil, nil);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"import_photos" alter:nil]
                                                                message:[Language get:@"success" alter:nil]
                                                               delegate:self
                                                      cancelButtonTitle:[Language get:@"ok" alter:nil]
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"import_photos" alter:nil]
                                                                message:[Language get:@"failed" alter:nil]
                                                               delegate:self
                                                      cancelButtonTitle:[Language get:@"ok" alter:nil]
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

@end
