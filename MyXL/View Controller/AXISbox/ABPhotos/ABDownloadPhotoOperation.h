//
//  ABDownloadPhotoOperation.h
//  Aconnect
//
//  Created by linkit on 5/22/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DBRestClient;

@interface ABDownloadPhotoOperation : NSOperation {
    // In concurrent operations, we have to manage the operation's state
    BOOL executing_;
    BOOL finished_;
    
    DBRestClient* restClient;
    NSString *contactPath_;
    CGFloat theProgress_;
    BOOL success_;
    UIImage *aImage_;
}

@property (nonatomic, readonly) NSString *contactPath; 
@property (nonatomic, readonly) CGFloat theProgress;
@property (nonatomic, readonly) BOOL success;
@property (nonatomic, readonly) UIImage *aImage;

- (id)initWithPath:(NSString*)path;

@end
