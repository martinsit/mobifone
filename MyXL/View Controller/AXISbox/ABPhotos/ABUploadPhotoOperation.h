//
//  ABUploadPhotoOperation.h
//  Aconnect
//
//  Created by linkit on 5/18/12.
//  Copyright (c) 2012 Link IT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DBRestClient;

@interface ABUploadPhotoOperation : NSOperation {
    // In concurrent operations, we have to manage the operation's state
    BOOL executing_;
    BOOL finished_;
    
    DBRestClient* restClient;
    UIImage *photo_;
    NSString *fileName_;
    
    CGFloat theProgress_;
    
    NSIndexPath *theIndexPath_;
    BOOL success_;
}

@property (nonatomic, readonly) UIImage *photo;
@property (nonatomic, readonly) NSString *fileName;
@property (nonatomic, readonly) CGFloat theProgress;
@property (nonatomic, readonly) NSIndexPath *theIndexPath;
@property (nonatomic, readonly) BOOL success;

- (id)initWithPhoto:(UIImage*)thePhoto withFileName:(NSString*)aFileName;
- (id)initWithPhoto:(UIImage*)thePhoto withFileName:(NSString*)aFileName forIndexPath:(NSIndexPath*)indexPath;

@end
