//
//  Asset.h
//
//  Created by Matt Tuzzolo on 2/15/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
//#import "LanguageUtil.h"


@interface ELCAsset : UIView {
	ALAsset *asset;
	UIImageView *overlayView;
	BOOL selected;
	id parent;
    //LanguageUtil *lu;
}

@property (nonatomic, retain) ALAsset *asset;
@property (nonatomic, assign) id parent;
//@property (nonatomic, retain) LanguageUtil *lu;

-(id)initWithAsset:(ALAsset*)_asset;
-(BOOL)selected;

@end