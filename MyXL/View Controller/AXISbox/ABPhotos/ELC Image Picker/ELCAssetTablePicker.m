//
//  AssetTablePicker.m
//
//  Created by Matt Tuzzolo on 2/15/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

#import "ELCAssetTablePicker.h"
#import "ELCAssetCell.h"
#import "ELCAsset.h"
#import "ELCAlbumPickerController.h"

#import "Language.h"

@implementation ELCAssetTablePicker

@synthesize parent;
@synthesize selectedAssetsLabel;
@synthesize assetGroup, elcAssets;
@synthesize exportPhotosAlert;
//@synthesize lu;

-(void)viewDidLoad {
    /*
    // init lang start
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *lang = [prefs stringForKey:@"language"];
    
    LanguageUtil *langUtil = [[LanguageUtil alloc] init];
    
    if([lang isEqualToString:@"en"]) langUtil.lang = @"en";
    else if([lang isEqualToString:@"id"]) langUtil.lang = @"id";
    else langUtil.lang = @"id";
    
    self.lu = langUtil;
    [self.lu initWords];
    [langUtil release];
    // init lang end
     */
        
	[self.tableView setSeparatorColor:[UIColor clearColor]];
	[self.tableView setAllowsSelection:NO];

    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    self.elcAssets = tempArray;
    [tempArray release];
	
	UIBarButtonItem *doneButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction:)] autorelease];
	[self.navigationItem setRightBarButtonItem:doneButtonItem];
	[self.navigationItem setTitle:[Language get:@"loading" alter:nil]];

	[self performSelectorInBackground:@selector(preparePhotos) withObject:nil];
    
    // Show partial while full list loads
	[self.tableView performSelector:@selector(reloadData) withObject:nil afterDelay:.5];
}

-(void)preparePhotos {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

	
    //NSLog(@"enumerating photos");
    [self.assetGroup enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) 
     {         
         if(result == nil) 
         {
             return;
         }
         
         ELCAsset *elcAsset = [[[ELCAsset alloc] initWithAsset:result] autorelease];
         [elcAsset setParent:self];
         [self.elcAssets addObject:elcAsset];
     }];    
    //NSLog(@"done enumerating photos");
	
	[self.tableView reloadData];
	[self.navigationItem setTitle:[Language get:@"pick_photo" alter:nil]];
    
    [pool release];

}

- (void)doneAction:(id)sender {
    BOOL isPhotoSelected = NO;
    for (ELCAsset *elcAsset in self.elcAssets)
    {
		if ([elcAsset selected]) {
            isPhotoSelected = YES;
		}
	}
    
    if (isPhotoSelected) {
        NSString *message = [NSString stringWithFormat:@"%@\n%@",[Language get:@"export_photos_confirmation" alter:nil],[Language get:@"ask_continue" alter:nil]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"export_photos" alter:nil]
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:[[Language get:@"yes" alter:nil]uppercaseString]
                                              otherButtonTitles:[[Language get:@"no" alter:nil]uppercaseString],nil];
        self.exportPhotosAlert = alert;
        [self.exportPhotosAlert show];
        [alert release];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"export_photos" alter:nil]
                                                        message:[Language get:@"export_photos_suggestion" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"ok" alter:nil]
                                              otherButtonTitles:nil,nil];
        [alert show];
        [alert release];
    }
}

- (void)didPressYes {
    NSMutableArray *selectedAssetsImages = [[[NSMutableArray alloc] init] autorelease];
    
	for(ELCAsset *elcAsset in self.elcAssets) 
    {		
		if([elcAsset selected]) {
			
			[selectedAssetsImages addObject:[elcAsset asset]];
		}
	}
    
    [(ELCAlbumPickerController*)self.parent selectedAssets:selectedAssetsImages];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView == self.exportPhotosAlert) {
        if (buttonIndex == 0) {
            [self didPressYes];
        }
    }
}

#pragma mark UITableViewDataSource Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        return ceil([self.assetGroup numberOfAssets] / 13.0);
    }
    else {
        return ceil([self.assetGroup numberOfAssets] / 4.0);
    }
}

- (NSArray*)assetsForIndexPath:(NSIndexPath*)_indexPath {
    
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        int index = (_indexPath.row*13);
        int maxIndex = (_indexPath.row*13+12);
        
        // NSLog(@"Getting assets for %d to %d with array count %d", index, maxIndex, [assets count]);
        
        if(maxIndex < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    [self.elcAssets objectAtIndex:index+3],
                    [self.elcAssets objectAtIndex:index+4],
                    [self.elcAssets objectAtIndex:index+5],
                    [self.elcAssets objectAtIndex:index+6],
                    [self.elcAssets objectAtIndex:index+7],
                    [self.elcAssets objectAtIndex:index+8],
                    [self.elcAssets objectAtIndex:index+9],
                    [self.elcAssets objectAtIndex:index+10],
                    [self.elcAssets objectAtIndex:index+11],
                    [self.elcAssets objectAtIndex:index+12],
                    nil];
        }
        
        else if(maxIndex-1 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    [self.elcAssets objectAtIndex:index+3],
                    [self.elcAssets objectAtIndex:index+4],
                    [self.elcAssets objectAtIndex:index+5],
                    [self.elcAssets objectAtIndex:index+6],
                    [self.elcAssets objectAtIndex:index+7],
                    [self.elcAssets objectAtIndex:index+8],
                    [self.elcAssets objectAtIndex:index+9],
                    [self.elcAssets objectAtIndex:index+10],
                    [self.elcAssets objectAtIndex:index+11],
                    nil];
        }
        
        else if(maxIndex-2 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    [self.elcAssets objectAtIndex:index+3],
                    [self.elcAssets objectAtIndex:index+4],
                    [self.elcAssets objectAtIndex:index+5],
                    [self.elcAssets objectAtIndex:index+6],
                    [self.elcAssets objectAtIndex:index+7],
                    [self.elcAssets objectAtIndex:index+8],
                    [self.elcAssets objectAtIndex:index+9],
                    [self.elcAssets objectAtIndex:index+10],
                    nil];
        }
        
        else if(maxIndex-3 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    [self.elcAssets objectAtIndex:index+3],
                    [self.elcAssets objectAtIndex:index+4],
                    [self.elcAssets objectAtIndex:index+5],
                    [self.elcAssets objectAtIndex:index+6],
                    [self.elcAssets objectAtIndex:index+7],
                    [self.elcAssets objectAtIndex:index+8],
                    [self.elcAssets objectAtIndex:index+9],
                    nil];
        }
        
        else if(maxIndex-4 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    [self.elcAssets objectAtIndex:index+3],
                    [self.elcAssets objectAtIndex:index+4],
                    [self.elcAssets objectAtIndex:index+5],
                    [self.elcAssets objectAtIndex:index+6],
                    [self.elcAssets objectAtIndex:index+7],
                    [self.elcAssets objectAtIndex:index+8],
                    nil];
        }
        
        else if(maxIndex-5 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    [self.elcAssets objectAtIndex:index+3],
                    [self.elcAssets objectAtIndex:index+4],
                    [self.elcAssets objectAtIndex:index+5],
                    [self.elcAssets objectAtIndex:index+6],
                    [self.elcAssets objectAtIndex:index+7],
                    nil];
        }
        
        else if(maxIndex-6 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    [self.elcAssets objectAtIndex:index+3],
                    [self.elcAssets objectAtIndex:index+4],
                    [self.elcAssets objectAtIndex:index+5],
                    [self.elcAssets objectAtIndex:index+6],
                    nil];
        }
        
        else if(maxIndex-7 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    [self.elcAssets objectAtIndex:index+3],
                    [self.elcAssets objectAtIndex:index+4],
                    [self.elcAssets objectAtIndex:index+5],
                    nil];
        }
        
        else if(maxIndex-8 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    [self.elcAssets objectAtIndex:index+3],
                    [self.elcAssets objectAtIndex:index+4],
                    nil];
        }
        
        else if(maxIndex-9 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    [self.elcAssets objectAtIndex:index+3],
                    nil];
        }
        
        else if(maxIndex-10 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    nil];
        }
        
        else if(maxIndex-11 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    nil];
        }
        
        else if(maxIndex-12 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObject:[self.elcAssets objectAtIndex:index]];
        }
        
        return nil;
    }
    else {
        int index = (_indexPath.row*4);
        int maxIndex = (_indexPath.row*4+3);
        
        // NSLog(@"Getting assets for %d to %d with array count %d", index, maxIndex, [assets count]);
        
        if(maxIndex < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    [self.elcAssets objectAtIndex:index+3],
                    nil];
        }
        
        else if(maxIndex-1 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    [self.elcAssets objectAtIndex:index+2],
                    nil];
        }
        
        else if(maxIndex-2 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                    [self.elcAssets objectAtIndex:index+1],
                    nil];
        }
        
        else if(maxIndex-3 < [self.elcAssets count]) {
            
            return [NSArray arrayWithObject:[self.elcAssets objectAtIndex:index]];
        }
        
        return nil;
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
        
    ELCAssetCell *cell = (ELCAssetCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) 
    {		        
        cell = [[[ELCAssetCell alloc] initWithAssets:[self assetsForIndexPath:indexPath] reuseIdentifier:CellIdentifier] autorelease];
    }	
	else 
    {		
		[cell setAssets:[self assetsForIndexPath:indexPath]];
	}
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	return 79;
}

- (int)totalSelectedAssets {
    
    int count = 0;
    
    for(ELCAsset *asset in self.elcAssets) 
    {
		if([asset selected]) 
        {            
            count++;	
		}
	}
    
    return count;
}

- (void)dealloc 
{
    [elcAssets release];
    [selectedAssetsLabel release];
    [exportPhotosAlert release];
    [super dealloc];    
}

@end
