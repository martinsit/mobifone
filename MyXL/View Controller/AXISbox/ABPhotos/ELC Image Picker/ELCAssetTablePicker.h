//
//  AssetTablePicker.h
//
//  Created by Matt Tuzzolo on 2/15/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
//#import "LanguageUtil.h"

@interface ELCAssetTablePicker : UITableViewController <UIAlertViewDelegate>
{
	ALAssetsGroup *assetGroup;
	
	NSMutableArray *elcAssets;
	int selectedAssets;
	
	id parent;
	
	NSOperationQueue *queue;
    
    UIAlertView *exportPhotosAlert;
    //LanguageUtil *lu;
}

@property (nonatomic, assign) id parent;
@property (nonatomic, assign) ALAssetsGroup *assetGroup;
@property (nonatomic, retain) NSMutableArray *elcAssets;
@property (nonatomic, retain) IBOutlet UILabel *selectedAssetsLabel;
@property (nonatomic, retain) UIAlertView *exportPhotosAlert;
//@property (nonatomic, retain) LanguageUtil *lu;

-(int)totalSelectedAssets;
-(void)preparePhotos;

-(void)doneAction:(id)sender;
-(void)didPressYes;

@end