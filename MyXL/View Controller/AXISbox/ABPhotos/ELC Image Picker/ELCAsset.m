//
//  Asset.m
//
//  Created by Matt Tuzzolo on 2/15/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

#import "ELCAsset.h"
#import "ELCAssetTablePicker.h"
#import "Language.h"

@implementation ELCAsset

@synthesize asset;
@synthesize parent;
//@synthesize lu;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

-(id)initWithAsset:(ALAsset*)_asset {
    /*
    // init lang start
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *lang = [prefs stringForKey:@"language"];
    
    LanguageUtil *langUtil = [[LanguageUtil alloc] init];
    
    if([lang isEqualToString:@"en"]) langUtil.lang = @"en";
    else if([lang isEqualToString:@"id"]) langUtil.lang = @"id";
    else langUtil.lang = @"id";
    
    self.lu = langUtil;
    [self.lu initWords];
    [langUtil release];
    // init lang end
     */
	
	if (self = [super initWithFrame:CGRectMake(0, 0, 0, 0)]) {
		
		self.asset = _asset;
		
		CGRect viewFrames = CGRectMake(0, 0, 75, 75);
		
		UIImageView *assetImageView = [[UIImageView alloc] initWithFrame:viewFrames];
		[assetImageView setContentMode:UIViewContentModeScaleToFill];
		[assetImageView setImage:[UIImage imageWithCGImage:[self.asset thumbnail]]];
		[self addSubview:assetImageView];
		[assetImageView release];
		
		overlayView = [[UIImageView alloc] initWithFrame:viewFrames];
		[overlayView setImage:[UIImage imageNamed:@"Overlay.png"]];
		[overlayView setHidden:YES];
		[self addSubview:overlayView];
    }
    
	return self;	
}

-(void)toggleSelection {
    
	overlayView.hidden = !overlayView.hidden;
    
    if([(ELCAssetTablePicker*)self.parent totalSelectedAssets] > 5) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"max_5_photos" alter:nil]
                                                       delegate:self 
                                              cancelButtonTitle:nil 
                                              otherButtonTitles:@"OK", nil];
		[alert show];
		[alert release];	
        
        //[(ELCAssetTablePicker*)self.parent doneAction:nil];
        
        overlayView.hidden = !overlayView.hidden;
    }
}

-(BOOL)selected {
	
	return !overlayView.hidden;
}

-(void)setSelected:(BOOL)_selected {
    
	[overlayView setHidden:!_selected];
}

- (void)dealloc 
{    
    self.asset = nil;
	[overlayView release];
    //[lu release];
    [super dealloc];
}

@end

