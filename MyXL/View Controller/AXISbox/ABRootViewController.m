//
//  ABRootViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/25/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ABRootViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import "UnderLineLabel.h"
#import <QuartzCore/QuartzCore.h>

#import "DataManager.h"
#import "GroupingAndSorting.h"
#import "MTPopupWindow.h"

#import "AppDelegate.h"
#import <DropboxSDK/DropboxSDK.h>

@interface ABRootViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *topInfoLabel;

@property (strong, nonatomic) UIButton *loginButton;

@property (strong, nonatomic) IBOutlet UnderLineLabel *termsLabel;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

@property (strong, nonatomic) IBOutlet UIView *blockBgView;
@property (strong, nonatomic) IBOutlet UIView *popUpView;
@property (strong, nonatomic) IBOutlet UIWebView *theWebView;

- (void)performTerms;
- (IBAction)closePopUp:(id)sender;
- (void)performLogin;

@end

@implementation ABRootViewController

@synthesize parentId = _parentId;

@synthesize sortedMenu = _sortedMenu;
@synthesize theSourceMenu = _theSourceMenu;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    
    DataManager *dataManager = [DataManager sharedInstance];
    NSArray *contentTmp = [dataManager.menuData valueForKey:_parentId];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    self.sortedMenu = [sortingResult allKeys];
    self.sortedMenu = [GroupingAndSorting doSortingByGroupId:self.sortedMenu];
    //NSLog(@"self.sortedMenu = %@",self.sortedMenu);
    
    self.theSourceMenu = sortingResult;
    //NSLog(@"self.theSourceMenu = %@",self.theSourceMenu);
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:0];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu lastObject];
    //NSLog(@"menuModel.menuName = %@",menuModel.menuName);
    
    
    //-------------//
    // Setup Label //
    //-------------//
    _topInfoLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _topInfoLabel.textColor = kDefaultPurpleColor;
    _topInfoLabel.numberOfLines = 0;
    _topInfoLabel.lineBreakMode = UILineBreakModeWordWrap;
    _topInfoLabel.text = [Language get:@"axisbox_login" alter:nil];
    [_topInfoLabel sizeToFit];
    
    //--------------//
    // Setup Button //
    //--------------//
    
    // Login Button
    UIFont *titleFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    
    UIButton *button = createButtonWithFrame(CGRectMake(_topInfoLabel.frame.origin.x,
                                                        _topInfoLabel.frame.origin.y + _topInfoLabel.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             
                                             [Language get:@"login" alter:nil],
                                             titleFont,
                                             kDefaultBaseColor,
                                             kDefaultPinkColor,
                                             kDefaultPurpleColor);
    
    [button addTarget:self action:@selector(performLogin) forControlEvents:UIControlEventTouchUpInside];
    
    _loginButton = button;
    [_contentView addSubview:_loginButton];
    
    
    _termsLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontValueSize];
    _termsLabel.textColor = kDefaultPurpleColor;
    _termsLabel.text = menuModel.menuName;
    [_termsLabel setShouldUnderline:YES];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(performTerms)];
    [_termsLabel setUserInteractionEnabled:YES];
    [_termsLabel addGestureRecognizer:tapGesture];
    [tapGesture release];
    
    CGRect labelFrame = _termsLabel.frame;
    labelFrame.origin.y = _loginButton.frame.origin.y + _loginButton.frame.size.height + kDefaultComponentPadding;
    _termsLabel.frame = labelFrame;
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultButtonGrayColor;
    
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _termsLabel.frame.origin.y + _termsLabel.frame.size.height + kDefaultPadding;
    _contentView.frame = newFrame;
    
    SectionHeaderView *headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,40)
                                                                   withLabel:menuModel.groupName];
    _sectionHeaderView = headerView;
    _sectionHeaderView.icon = self.headerIconMaster;
    
    [_contentView addSubview:_sectionHeaderView];
    [_contentView sendSubviewToBack:_sectionHeaderView];
    [headerView release];
    
    ContentView *contentViewTmp = [[ContentView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
    [_contentView addSubview:contentViewTmp];
    [_contentView sendSubviewToBack:contentViewTmp];
    [contentViewTmp release];
    
    //----------------------//
    // Grey Background View //
    //----------------------//
    CGFloat x = newFrame.origin.x - kDefaultLeftPadding;
    CGFloat y = newFrame.origin.y - kDefaultPadding;
    CGFloat width = newFrame.size.width + (kDefaultLeftPadding*2);
    CGFloat height = newFrame.size.height + (kDefaultPadding*2);
    _greyBackgroundView.frame = CGRectMake(x, y, width, height);
    _greyBackgroundView.backgroundColor = kDefaultButtonGrayColor;
    _greyBackgroundView.layer.cornerRadius = kDefaultCornerRadius;
    _greyBackgroundView.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Selector

- (void)performTerms {
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:0];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu lastObject];
    
    /*
    NSString *body = [NSString stringWithFormat:@"<html><head><style> body, label,table, td {font-family:Helvetica; font-size:12px} p {padding:0;margin:0} label {font-weight:bold} p.Q {font-size:16px; color:#B5005B;font-weight:bold} .left-pad {margin-top:0;padding-left:7px;} .left-pad li {margin-left:10px !important;overflow:visible !important;} .tbl-faq {text-align:center;font-size:12px;color:#333333;border:1px solid #666666;border-collapse: collapse;} .tbl-faq.head {background-color: #dedede;} .tbl-faq.body {background-color:#ffffff; font-weight:normal;} </style> </head> <body> <p class=\"Q\">%@</p><p class=\"A\"><br/>%@</p> </body> </html>",menuModel.menuName,menuModel.desc];*/
    
    NSString *body = [NSString stringWithFormat:@"<htm><head><style> body,p,li {font-family:Open Sans; font-size:11px; color:#7a1878} h2 {font-size:20px; color:#ee2b74}</style></head><body><h2>%@</h2>\n%@</body>",menuModel.menuName,menuModel.desc];
    
    NSString *reqSysVer = @"6.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending) {
        [MTPopupWindow showWindowWithHTMLFile:body insideView:self.view];
    }
    else {
        _blockBgView.hidden = NO;
        _popUpView.hidden = NO;
        [_theWebView loadHTMLString:body baseURL:nil];
    }
}

- (IBAction)closePopUp:(id)sender {
    _blockBgView.hidden = YES;
    _popUpView.hidden = YES;
}

- (void)performLogin {
    /*
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    app.viewId = self.viewId;
    if (![[DBSession sharedSession] isLinked]) {
		[[DBSession sharedSession] link];
    }*/
    
    DataManager *sharedData = [DataManager sharedInstance];
    sharedData.parentId = _parentId;
    sharedData.navController = self.navigationController;
    
    if (![[DBSession sharedSession] isLinked]) {
        [[DBSession sharedSession] linkFromController:self];
    }
}

@end
