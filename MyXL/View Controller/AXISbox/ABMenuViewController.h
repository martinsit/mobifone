//
//  ABMenuViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/25/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@class DBRestClient;

@interface ABMenuViewController : BasicViewController {
    NSString *_parentId;
    DBRestClient* restClient;
    NSString *_accountValue;
    NSString *_quotaValue;
    NSString *_usageValue;
}

@property (nonatomic, retain) NSString *parentId;
@property (nonatomic, retain) NSString *accountValue;
@property (nonatomic, retain) NSString *quotaValue;
@property (nonatomic, retain) NSString *usageValue;

@end
