//
//  ABSubMenuViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/26/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "ELCImagePickerController.h"

@interface ABSubMenuViewController : BasicViewController <
UIActionSheetDelegate,
ELCImagePickerControllerDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
CLLocationManagerDelegate> {
    NSString *_parentId;
}

@property (nonatomic, retain) NSString *parentId;

@end
