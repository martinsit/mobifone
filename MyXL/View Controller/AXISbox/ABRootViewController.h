//
//  ABRootViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/25/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface ABRootViewController : BasicViewController {
    NSString *_parentId;
    
    NSArray *_sortedMenu;
    NSDictionary *_theSourceMenu;
}

@property (nonatomic, retain) NSString *parentId;

@property (nonatomic, retain) NSArray *sortedMenu;
@property (nonatomic, retain) NSDictionary *theSourceMenu;

@end
