//
//  ABMenuViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/25/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ABMenuViewController.h"
#import "Constant.h"
#import "SectionFooterView.h"
#import "SectionHeaderView.h"
#import "AXISboxAccountCell.h"
//#import "MyPackageCell.h"
#import "CommonCell.h"
#import "BorderCellBackground.h"
#import "AXISnetCellBackground.h"
#import "AXISnetCommon.h"

#import "MenuModel.h"
#import "DataManager.h"
#import "GroupingAndSorting.h"

#import <DropboxSDK/DropboxSDK.h>
#import "HumanReadableDataSizeHelper.h"
#import "MTPopupWindow.h"
#import "ABSubMenuViewController.h"

@interface ABMenuViewController () <DBRestClientDelegate>

@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (nonatomic, retain) NSArray *sortedMenu;
@property (nonatomic, retain) NSDictionary *theSourceMenu;

@property (nonatomic, readonly) DBRestClient* restClient;

@property (strong, nonatomic) IBOutlet UIView *blockBgView;
@property (strong, nonatomic) IBOutlet UIView *popUpView;
@property (strong, nonatomic) IBOutlet UIWebView *theWebView;

- (IBAction)logoutButtonPressed:(id)sender;
- (void)unlink;
- (void)performTerms;
- (IBAction)closePopUp:(id)sender;

@end

@implementation ABMenuViewController

@synthesize parentId = _parentId;
@synthesize accountValue = _accountValue;
@synthesize quotaValue = _quotaValue;
@synthesize usageValue = _usageValue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self.restClient loadAccountInfo];
}

- (void)viewWillAppear:(BOOL)animated {
    DataManager *dataManager = [DataManager sharedInstance];
    NSArray *contentTmp = [dataManager.menuData valueForKey:_parentId];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    self.sortedMenu = [sortingResult allKeys];
    self.sortedMenu = [GroupingAndSorting doSortingByGroupId:self.sortedMenu];
    self.theSourceMenu = sortingResult;
    
    [_theTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.parentId = nil;
    self.accountValue = nil;
    self.quotaValue = nil;
    self.usageValue = nil;
}

- (void)dealloc {
    [_parentId release];
    _parentId = nil;
    
    [restClient release];
    
    [_accountValue release];
    _accountValue = nil;
    
    [_quotaValue release];
    _quotaValue = nil;
    
    [_usageValue release];
    _usageValue = nil;
    
    [super dealloc];
}

- (DBRestClient*)restClient {
    if (restClient == nil) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sortedMenu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section]] count] + 1;
    }
    else {
        return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section]] count];
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifierAccount = @"Account";
    static NSString *cellIdentifierCommon = @"Common";
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        AXISboxAccountCell *cell = (AXISboxAccountCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierAccount];
        
        if (cell == nil) {
            cell = [[AXISboxAccountCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifierAccount];
            
            cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
            cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
            
            [cell.changeButton setTitle:[Language get:@"change_account" alter:nil] forState:UIControlStateNormal];
            
            [cell.changeButton addTarget:self
                                  action:@selector(logoutButtonPressed:)
                        forControlEvents:UIControlEventTouchUpInside];
        }
        
        cell.accountTitleLabel.text = [[Language get:@"dropbox_account" alter:nil] uppercaseString];
        cell.accountValueLabel.text = self.accountValue;
        
        cell.usageTitleLabel.text = [[Language get:@"usage" alter:nil] uppercaseString];
        cell.usageValueLabel.text = self.usageValue;
        
        cell.quotaTitleLabel.text = [[Language get:@"quota" alter:nil] uppercaseString];
        cell.quotaValueLabel.text = self.quotaValue;
        
        [cell.changeButton setTag:indexPath.row];
        
        return cell;
    }
    else {
        CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierCommon];
        
        if (cell == nil) {
            cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifierCommon];
            
            [cell.infoButton addTarget:self
                                action:@selector(performTerms)
                      forControlEvents:UIControlEventTouchUpInside];
            
            cell.backgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
            cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
            ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
        }
        
        NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
        NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
        
        MenuModel *menuModel;
        if (indexPath.section == 0) {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
        }
        else {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row];
        }
        
        cell.iconLabel.text = icon(menuModel.menuId);
        if ([cell.iconLabel.text length] <= 0) {
            cell.iconLabel.text = icon(@"default_icon");
        }
        
        cell.titleLabel.text = [menuModel.menuName uppercaseString];
        if ([menuModel.desc length] > 0) {
            cell.arrowLabel.hidden = YES;
            cell.infoButton.hidden = NO;
        }
        else {
            cell.arrowLabel.hidden = NO;
            cell.infoButton.hidden = YES;
        }
        return cell;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    MenuModel *menuModel = [objectsForMenu objectAtIndex:0];
    return menuModel.groupName;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 90.0;
    }
    else {
        return kDefaultCellHeight;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger index = indexPath.row;
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    NSUInteger lastIndex = [objectsForMenu count] + 1 - 1;
    
    if (index == lastIndex) {
        [self performTerms];
    }
    else if (indexPath.row == 0) {
        //NSLog(@"Do nothing");
    }
    else {
        MenuModel *menuModel;
        if (indexPath.section == 0) {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
        }
        else {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row];
        }
        
        ABSubMenuViewController *detailViewController = [[ABSubMenuViewController alloc] initWithNibName:@"ABSubMenuViewController" bundle:nil];
        detailViewController.parentId = menuModel.menuId;
        
        [self.navigationController pushViewController:detailViewController animated:YES];
        detailViewController = nil;
        [detailViewController release];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu objectAtIndex:0];
    
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:icon([NSString stringWithFormat:@"gid_%@",menuModel.groupId])
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:icon([NSString stringWithFormat:@"gid_%@",menuModel.groupId])
                                                isFirstSection:NO] autorelease];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

#pragma mark - Dropbox Account Callback

- (void)restClient:(DBRestClient*)client loadedAccountInfo:(DBAccountInfo*)info {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    //NSLog(@"UserID: %@ %@", [info displayName], [info userId]);
    DBQuota *quota = info.quota;
    long long total = quota.totalBytes;
    long long consumed = quota.totalConsumedBytes;
    
    NSString *quotaVal = [NSString stringWithFormat:@"%qi",total];
    unsigned long long ullvalue = strtoull([quotaVal UTF8String], NULL, 0);
    NSNumber *quotaNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
    NSString *strQuota = [HumanReadableDataSizeHelper humanReadableSizeFromBytes:quotaNumber
                                                                   useSiPrefixes:YES
                                                                 useSiMultiplier:NO];
    
    NSString *usageVal = [NSString stringWithFormat:@"%qi",consumed];
    ullvalue = strtoull([usageVal UTF8String], NULL, 0);
    NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
    NSString *strUsage = [HumanReadableDataSizeHelper humanReadableSizeFromBytes:usageNumber
                                                                   useSiPrefixes:YES
                                                                 useSiMultiplier:NO];
    
    self.accountValue = info.displayName;
    self.quotaValue = strQuota;
    self.usageValue = strUsage;
    
    [_theTableView reloadData];
}

- (void)restClient:(DBRestClient*)client loadAccountInfoFailedWithError:(NSError*)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - Selector

- (IBAction)logoutButtonPressed:(id)sender {
    [self unlink];
}

- (void)unlink {
    if (![[DBSession sharedSession] isLinked]) {
        [[DBSession sharedSession] linkFromController:self];
    }
    else {
        [[DBSession sharedSession] unlinkAll];
        [[[[UIAlertView alloc] initWithTitle:@""
                                     message:[Language get:@"unlink_message" alter:nil]
                                    delegate:nil
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil] autorelease] show];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Selector

- (void)performTerms {
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:0];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu lastObject];
    /*
    NSString *body = [NSString stringWithFormat:@"<html><head><style> body, label,table, td {font-family:Helvetica; font-size:12px} p {padding:0;margin:0} label {font-weight:bold} p.Q {font-size:16px; color:#B5005B;font-weight:bold} .left-pad {margin-top:0;padding-left:7px;} .left-pad li {margin-left:10px !important;overflow:visible !important;} .tbl-faq {text-align:center;font-size:12px;color:#333333;border:1px solid #666666;border-collapse: collapse;} .tbl-faq.head {background-color: #dedede;} .tbl-faq.body {background-color:#ffffff; font-weight:normal;} </style> </head> <body> <p class=\"Q\">%@</p><p class=\"A\"><br/>%@</p> </body> </html>",menuModel.menuName,menuModel.desc];*/
    
    NSString *body = [NSString stringWithFormat:@"<htm><head><style> body,p,li {font-family:Open Sans; font-size:11px; color:#7a1878} h2 {font-size:20px; color:#ee2b74}</style></head><body><h2>%@</h2>\n%@</body>",menuModel.menuName,menuModel.desc];
    
    NSString *reqSysVer = @"6.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending) {
        [MTPopupWindow showWindowWithHTMLFile:body insideView:self.view];
    }
    else {
        _blockBgView.hidden = NO;
        _popUpView.hidden = NO;
        [_theWebView loadHTMLString:body baseURL:nil];
    }
}

- (IBAction)closePopUp:(id)sender {
    _blockBgView.hidden = YES;
    _popUpView.hidden = YES;
}

@end
