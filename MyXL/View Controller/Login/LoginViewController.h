//
//  LoginViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/10/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AXISnetRequest.h"

@class LoginViewController;

@protocol LoginViewControllerDelegate <NSObject>
@optional
- (void)viewControllerDidLoginSuccess:(LoginViewController*)viewController;
@end

@interface LoginViewController : BasicViewController <UITextFieldDelegate, AXISnetRequestDelegate> {
    id <LoginViewControllerDelegate> _delegate;
    NSArray *content;
}

@property (assign) id <LoginViewControllerDelegate> delegate;
@property (nonatomic, retain) NSArray *content;

@end
