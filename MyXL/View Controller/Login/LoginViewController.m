//
//  LoginViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/10/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "LoginViewController.h"

#import "Constant.h"
//#import "EncryptDecrypt.h"
//#import "JSON.h"
//#import "ASIFormDataRequest.h"
#import "AppDelegate.h"

#import "AXISnetRequest.h"

//#import "AXISnetNavigationBar.h"
//#import "ContentView.h"
#import "AXISnetCommon.h"
#import <QuartzCore/QuartzCore.h>

#import "RegisterViewController.h"
#import "ForgotPasswordViewController.h"
#import "AccountActivationViewController.h"

#import "DataManager.h"
#import "AXISnetNavigationBar.h"
#import "EncryptDecrypt.h"

#import "MenuModel.h"
#import "DTCustomColoredAccessory.h"
#import "CommonCell.h"
#import "UIDeviceHardware.h"

#import "LanguageCell.h"

#import "QuotaMeterViewController.h"

#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "LogoModel.h"
#import "AppDelegate.h"
#import "RequestPasswordResultViewController.h"
#import "RequestPasswordViewController.h"

@interface LoginViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *greyBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIView *authView;
//@property (strong, nonatomic) IBOutlet UIView *separatorAuthView;
@property (strong, nonatomic) IBOutlet UIImageView *bgView;

@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (retain, nonatomic) IBOutlet UIImageView *smallLogoImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (strong, nonatomic) IBOutlet UIView *bgTableView;
@property (strong, nonatomic) UIButton *loginButton;
@property (strong, nonatomic) UIButton *registerButton;
@property (strong, nonatomic) UIButton *forgotPasswordButton;
@property (strong, nonatomic) UIButton *accountActivationButton;

@property (retain, nonatomic) IBOutlet UILabel *lblRememberPassword;
@property (retain, nonatomic) UIButton *btnRememberPassword;
@property (retain, nonatomic) UIButton *btnRequestPassword;
@property (retain, nonatomic) IBOutlet UILabel *lblMsisdn;
@property (retain, nonatomic) IBOutlet UILabel *lblPassword;
@property (retain, nonatomic) IBOutlet UILabel *lblErrorPassword;
@property (retain, nonatomic) IBOutlet UILabel *lblErrorMsisdn;
@property (retain, nonatomic) IBOutlet UILabel *lblErrorMsisdn2;
@property (nonatomic,copy) NSString *errorMsisdnKey;
@property (nonatomic,copy) NSString *errorPasswordKey;
@property (retain, nonatomic) IBOutlet UIView *languageView;
@property (nonatomic, retain) UIButton *indonesiaButton;
@property (retain, nonatomic) IBOutlet UILabel *lblDisclaimer;
@property (nonatomic, retain) UIButton *englishButton;
@property (nonatomic, retain) UILabel *lblForgotPassword;
@property (nonatomic, retain) UIView *rememberPasswordView;
@property (nonatomic, assign) BOOL checkedRememberPassword;
//@property (retain, nonatomic) IBOutlet UIButton *indonesiaButton;
//@property (retain, nonatomic) IBOutlet UIButton *englishButton;

- (void)performLogin:(id)sender;
- (void)performRegister:(id)sender;
- (void)performForgotPassword:(id)sender;
- (void)performAccountActivation:(id)sender;
- (void)login;
- (void)setupButton;

@end

@implementation LoginViewController

@synthesize delegate = _delegate;
@synthesize content;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    float currentVersion = 7.0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= currentVersion) {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    
    //----------------//
    // Navigation Bar //
    //----------------//
    self.navigationItem.leftBarButtonItem = nil;
    
    _logoImageView.image = [UIImage imageNamed:@"mloyalty_logo"];
    _smallLogoImageView.image = [UIImage imageNamed:@"mobifone_logo"];
    _logoImageView.contentMode = UIViewContentModeCenter;
    _smallLogoImageView.contentMode = UIViewContentModeCenter;
    
    //-------------//
    // Setup Label //
    //-------------//
//    _titleLabel.font = [UIFont fontWithName:kDefaultFontBold size:24.0];
//    _titleLabel.textColor = kDefaultWhiteColor;
//    _titleLabel.text = @"LOG IN";
    
    //_usernameLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
//    _usernameLabel.textColor = kDefaultWhiteColor;
    
    // TODO : Hygiene
//    _lblMsisdn.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
//    _lblMsisdn.textColor = kDefaultWhiteColor;
    
//    _lblPassword.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
//    _lblPassword.textColor = kDefaultWhiteColor;
    
    _lblErrorMsisdn.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _lblErrorMsisdn2.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _lblErrorPassword.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];

    _usernameTextField.font = [UIFont fontWithName:kDefaultFontLight size:13.0];
    _usernameTextField.textColor = kDefaultBlackColor;
    
    //_passwordLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    //_passwordLabel.textColor = kDefaultWhiteColor;
    
    _passwordTextField.font = [UIFont fontWithName:kDefaultFontLight size:13.0];
    _passwordTextField.textColor = kDefaultBlackColor;
    
    /*
     * authView rounded corner
     */
    _authView.layer.cornerRadius = kDefaultCornerRadius;
    _authView.layer.masksToBounds = YES;
    
    /*
     * authView separator color
     */
    //_separatorAuthView.backgroundColor = kDefaultNavyBlueColor;
    
    /*
     * Position
     */
    /*
    NSString *platform = [UIDeviceHardware platformString];
    
    if ([platform isEqualToString:@"iPhone 5"] || [platform isEqualToString:@"iPhone 5 (GSM+CDMA)"] ||
        [platform isEqualToString:@"iPhone 5c"] || [platform isEqualToString:@"iPhone 5s"]) {
        _bgView.image = [UIImage imageNamed:@"old-xl-splash-iPhone5-640x1136.jpg"];
    }
    else {
        if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            _bgView.image = [UIImage imageNamed:@"old-xl-splash-iPad-1024x768.jpg"];
        }
        else {
            _bgView.image = [UIImage imageNamed:@"old-xl-splash-iPhone4-640x960.jpg"];
        }
    }*/
    _bgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture.png"] ];     //imageFromColor(kDefaultHygieneBgColor);
    
    if (IS_IPAD) {
        CGRect frame = _logoImageView.frame;
        frame.origin.y = 150;
        _logoImageView.frame = frame;
        
        frame = _usernameLabel.frame;
        frame.origin.y = _logoImageView.frame.origin.y + _logoImageView.frame.size.height + 50;
        _usernameLabel.frame = frame;
        
        frame = _authView.frame;
        frame.origin.y = _usernameLabel.frame.origin.y + _usernameLabel.frame.size.height + 10;
        _authView.frame = frame;
    }
    else {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        CGRect frame = _logoImageView.frame;
        if (screenSize.height > 480.0f) {
            // iPhone 5
            frame.origin.y = 44;
        }
        else {
            // iPhone 4
            frame.origin.y = 20;
        }
        
        _logoImageView.frame = frame;
        
        frame = _usernameLabel.frame;
        frame.origin.y = _logoImageView.frame.origin.y + _logoImageView.frame.size.height + kDefaultComponentPadding;
        _usernameLabel.frame = frame;
        
        frame = _authView.frame;
        frame.origin.y = _usernameLabel.frame.origin.y + _usernameLabel.frame.size.height;
        _authView.frame = frame;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
            frame = _usernameTextField.frame;
            frame.origin.y += 2.0;
            _usernameTextField.frame = frame;
            
            frame = _passwordTextField.frame;
            frame.origin.y += 2.0;
            _passwordTextField.frame = frame;
        }
    }
    
    // TODO : Hygiene
    UITapGestureRecognizer *dismissKeyboardTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboardOnTap)];
    dismissKeyboardTap.numberOfTapsRequired = 1;
    dismissKeyboardTap.numberOfTouchesRequired = 1;
    [_authView addGestureRecognizer:dismissKeyboardTap];
}

- (void)viewWillAppear:(BOOL)animated {
    
    // Google Analytics
    [self activePage:@"Login Page"];
    
    NSArray *viewsToRemove = [_contentView subviews];
    for (UIView *v in viewsToRemove) {
        if ([v isKindOfClass:[UIButton class]]) {
            [v removeFromSuperview];
        }
        else if(v.tag == 999)
        {
            [v removeFromSuperview];
        }
    }
    
    if(_lblForgotPassword != nil)
    {
        [_lblForgotPassword removeFromSuperview];
        [_btnRequestPassword removeFromSuperview];
    }
    
    [self setupButton];
   
    
    //_usernameLabel.text = [Language get:@"welcome_to_myxl" alter:nil];
    //_usernameLabel.text = [Language get:@"select_payment_x" alter:nil];
    //_lblMsisdn.text = [Language get:@"your_xl_phone_number" alter:nil];
    //_lblPassword.text = [Language get:@"password" alter:nil];
    _lblRememberPassword.text = [Language get:@"remember_password" alter:nil];
    
    UIColor *color = [UIColor blackColor];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 6.0) {
//        _usernameTextField.placeholder = [Language get:@"your_xl_phone_number" alter:nil];
        _passwordTextField.placeholder = [Language get:@"password" alter:nil];
    }
    else {
//        _usernameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[Language get:@"your_xl_phone_number" alter:nil]
//                                                                                   attributes:@{NSForegroundColorAttributeName: color}];
    
        
        
        _passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[Language get:@"password" alter:nil]
                                                                                   attributes:@{NSForegroundColorAttributeName: color}];
    }
    
    // TODO : V1.9.5
    NSDictionary *profileDict = getDataFromPreferenceWithKey(KEEP_SIGNIN_CACHE_KEY);
    NSString *password = [profileDict objectForKey:PROFILE_PASSWORD_CACHE_KEY];
    NSString *msisdn = [profileDict objectForKey:PROFILE_MSISDN_CACHE_KEY];
    
    // TODO : Hygiene
    //NSString *password = getDataFromPreferenceWithKey(@"key");
    //NSString *msisdn = getDataFromPreferenceWithKey(@"value");
    if([password length] > 0 && [msisdn length] > 0)
    {
        NSString *saltKeyAPI = SALT_KEY;
        msisdn = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCDecrypt withKey:saltKeyAPI];
        password = [EncryptDecrypt doCipherForiOS7:password action:kCCDecrypt withKey:saltKeyAPI];
        _passwordTextField.text = password;
        _usernameTextField.text = msisdn;
        _checkedRememberPassword = YES;
    }
    else
    {
        _passwordTextField.text = @"";
        _usernameTextField.text = @"";
    }
    
    /*
    _passwordTextField.backgroundColor = [UIColor whiteColor];
    _passwordTextField.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:0.7].CGColor;
    _passwordTextField.layer.borderWidth = 1.0f;
    UIView* leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 41, 20)];
    _passwordTextField.leftViewMode = UITextFieldViewModeAlways;
    _passwordTextField.leftView = leftView;*/
    
    //---------------------//
    // Hide Navigation Bar //
    //---------------------//
    AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[self.navigationController navigationBar];
    navBar.hidden = YES;
    
    /*
     * Setup UITableView content
     */
//    [self setupTableViewContent];
    
    // TODO : Hygiene
    [self createLanguageButtons];
    [self setupBottomView];
    
    // TODO : Hygiene
   // _lblDisclaimer.textColor = _lblMsisdn.textColor;
    
    if (IS_IPAD) {
//        _lblDisclaimer.font = [UIFont fontWithName:kDefaultFontLight size:14.0];
    }
    else {
//        _lblDisclaimer.font = _lblMsisdn.font;
    }
//    _lblDisclaimer.frame = CGRectMake(CGRectGetMinX(_lblDisclaimer.frame), CGRectGetMaxY(_usernameLabel.frame) + 8, CGRectGetWidth(_lblDisclaimer.frame), CGRectGetHeight(_lblDisclaimer.frame));
    [_theScrollView setContentSize:CGSizeMake(CGRectGetWidth(_contentView.frame),CGRectGetMaxY(_contentView.frame) + 20)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
	[content release];
    [_lblRememberPassword release];
    [_btnRememberPassword release];
    [_btnRequestPassword release];
    [_lblForgotPassword release];
    [_rememberPasswordView release];
    [_lblMsisdn release];
    [_lblPassword release];
    [_lblErrorPassword release];
    [_lblErrorMsisdn release];
    [_indonesiaButton release];
    [_englishButton release];
    [_languageView release];
    [_lblDisclaimer release];
    [_lblErrorMsisdn2 release];
    [_smallLogoImageView release];
    [super dealloc];
}

#pragma mark - Selector

// TODO : Hygiene
-(void)requestPassword:(id)sender
{
    /*
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    if(appDelegate.isOffNet)
    {
        if(IS_IPAD)
        {
            RequestPasswordResultViewController *reqPasswordViewController = [[RequestPasswordResultViewController alloc] initWithNibName:@"RequestPasswordResultViewController" bundle:nil];
            reqPasswordViewController.isOffNet = YES;
            [self.navigationController pushViewController:reqPasswordViewController animated:YES];
            reqPasswordViewController = nil;
            [reqPasswordViewController release];
        }
        else
        {
            RequestPasswordViewController *reqPasswordViewController = [[RequestPasswordViewController alloc] initWithNibName:@"RequestPasswordViewController" bundle:nil];
            [self.navigationController pushViewController:reqPasswordViewController animated:YES];
            reqPasswordViewController = nil;
            [reqPasswordViewController release];
        }
    }
    else
    {
        RequestPasswordViewController *reqPasswordViewController = [[RequestPasswordViewController alloc] initWithNibName:@"RequestPasswordViewController" bundle:nil];
        [self.navigationController pushViewController:reqPasswordViewController animated:YES];
        reqPasswordViewController = nil;
        [reqPasswordViewController release];
    }*/
    
    RequestPasswordViewController *reqPasswordViewController = [[RequestPasswordViewController alloc] initWithNibName:@"RequestPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:reqPasswordViewController animated:YES];
    reqPasswordViewController = nil;
    [reqPasswordViewController release];
}

// TODO : Hygiene
-(void)rememberPasswordChecked:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger tag = button.tag;
    if(tag == 0)
    {
        [button setBackgroundColor:[UIColor clearColor]];
        [button setImage:[UIImage imageNamed:@"check-on"] forState:UIControlStateNormal];
        button.tag = 1;
    }
    else
    {
        [button setBackgroundColor:[UIColor whiteColor]];
        [button setAlpha:0.5];
        [button setTitle:@"" forState:UIControlStateNormal];
        [button setImage:nil forState:UIControlStateNormal];
//        [button setImage:[UIImage imageNamed:@"check-off"] forState:UIControlStateNormal];
        button.tag = 0;
    }
}

- (void)performLogin:(id)sender
{
    // TODO : V1.9
    // Loading Time for GAI
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.dateToday = [[NSDate alloc] init];
    NSLog(@"Login start %@", appDelegate.dateToday);
    
    // TODO : Hygiene
    [_usernameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    
//    BOOL isValidMsisdn = validateMsisdn(_usernameTextField.text);
    
    if([_usernameTextField.text isEqualToString:@""])
    {
//        _errorMsisdnKey = @"error_msisdn_empty";
//        _lblErrorMsisdn2.text = [Language get:_errorMsisdnKey alter:nil];
//        _lblErrorMsisdn2.hidden = NO;
        _lblDisclaimer.hidden = NO;
    }
    /*
    else if(!isValidMsisdn)
    {
        _errorMsisdnKey = @"error_msisdn";
        _lblErrorMsisdn2.text = [Language get:_errorMsisdnKey alter:nil];
        _lblErrorMsisdn2.hidden = NO;
    }*/
    else if([_usernameTextField.text length] < MIN_MSISDN_LENGTH)
    {
//        _errorMsisdnKey = @"error_msisdn";
//        _lblErrorMsisdn2.text = [Language get:_errorMsisdnKey alter:nil];
//        _lblErrorMsisdn2.hidden = NO;
        _lblDisclaimer.hidden = NO;
    }
    
    if([_passwordTextField.text isEqualToString:@""])
    {
//        _errorPasswordKey = @"error_password_empty";
//        _lblErrorPassword.text = [Language get:_errorPasswordKey alter:nil];
//        _lblErrorPassword.hidden = NO;
        _lblDisclaimer.hidden = NO;
    }
    // TODO : V1.9
    // Update the minimum character length for password to be 6
//    else if([_passwordTextField.text length] < 5)
//    else if([_passwordTextField.text length] < MIN_PASSWORD_LENGTH)
//    {
//        _errorPasswordKey = @"error_password";
//        _lblErrorPassword.text = [Language get:_errorPasswordKey alter:nil];
//        _lblErrorPassword.hidden = NO;
//    }

//    if(![_passwordTextField.text isEqualToString:@""] && [_passwordTextField.text length] >= MIN_PASSWORD_LENGTH && [_usernameTextField.text length] >= MIN_MSISDN_LENGTH && ![_usernameTextField.text isEqualToString:@""] && isValidMsisdn)
    
    //if(![_passwordTextField.text isEqualToString:@""] && [_usernameTextField.text length] >= MIN_MSISDN_LENGTH && ![_usernameTextField.text isEqualToString:@""] && isValidMsisdn)
    
    if(![_passwordTextField.text isEqualToString:@""] && [_usernameTextField.text length] >= MIN_MSISDN_LENGTH && ![_usernameTextField.text isEqualToString:@""])
    {
        _lblErrorMsisdn.hidden = YES;
        _lblErrorPassword.hidden = YES;
        _lblErrorMsisdn2.hidden = YES;
        _lblDisclaimer.hidden = YES;
        [self login];
    }
}

- (int)validateInput {
    if ([_usernameTextField.text length] <= 0 ||
        [_passwordTextField.text length] <= 0 ) {
        return 2;
    }
    else {
        return 0;
    }
}

- (void)performRegister:(id)sender {
    /*
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *documentsDirectory = applicationDocumentsDirectory();
    NSString *logoName = @"";
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else if ([sharedData.profileData.telcoOperator isEqualToString:@"AXIS"]) {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    else {
        logoName = XL_LOGO_RETINA_NAME;
    }
    NSString *mediaPath = [documentsDirectory stringByAppendingPathComponent:logoName];
    
    // 8. Set Logo
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.customNavBar updateLogo:mediaPath];*/
    
    RegisterViewController *registerViewController = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:registerViewController animated:YES];
    registerViewController = nil;
    [registerViewController release];
}

- (void)performForgotPassword:(id)sender {
    /*
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *documentsDirectory = applicationDocumentsDirectory();
    NSString *logoName = @"";
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    NSString *mediaPath = [documentsDirectory stringByAppendingPathComponent:logoName];
    
    // 8. Set Logo
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.customNavBar updateLogo:mediaPath];*/
    
    ForgotPasswordViewController *forgotPasswordViewController = [[ForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:forgotPasswordViewController animated:YES];
    forgotPasswordViewController = nil;
    [forgotPasswordViewController release];
}

- (void)performAccountActivation:(id)sender {
    /*
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *documentsDirectory = applicationDocumentsDirectory();
    NSString *logoName = @"";
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    NSString *mediaPath = [documentsDirectory stringByAppendingPathComponent:logoName];
    
    // 8. Set Logo
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.customNavBar updateLogo:mediaPath];*/
    
    AccountActivationViewController *accountActivationViewController = [[AccountActivationViewController alloc] initWithNibName:@"AccountActivationViewController" bundle:nil];
    [self.navigationController pushViewController:accountActivationViewController animated:YES];
    accountActivationViewController = nil;
    [accountActivationViewController release];
}

// TODO : Hygiene
-(void)createLanguageButtons
{
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    
    _indonesiaButton = createButtonWithFrame(CGRectMake(0, 0, kDefaultButtonHeight, kDefaultButtonHeight),
                                            @"VN",
                                            buttonFont,
                                            kDefaultWhiteColor,
                                            kDefaultGrayColor,
                                            kDefaultAquaMarineColor);
    
    _englishButton = createButtonWithFrame(CGRectMake(CGRectGetMaxX(_indonesiaButton.frame), 0, kDefaultButtonHeight, kDefaultButtonHeight),
                                          @"EN",
                                          buttonFont,
                                           kDefaultWhiteColor,
                                           kDefaultGrayColor,
                                           kDefaultAquaMarineColor);
    int currentLang = [Language getCurrentLanguage];
    if(currentLang == 1)
    {
        // ID
        _indonesiaButton.tag = 1;
        _englishButton.tag = 0;
//        [self toggleLanguageButton];
    }
    else
    {
        // ENG
        _indonesiaButton.tag = 0;
        _englishButton.tag = 1;
    }
    
    [self toggleLanguageButton];
    
//    [_indonesiaButton setBackgroundImage:imageFromColor(kDefaultYellowColor)
//                                  forState:UIControlStateNormal];
//    [_indonesiaButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
//                                  forState:UIControlStateHighlighted];
//    _indonesiaButton.layer.borderWidth = 0.0;
//    _indonesiaButton.layer.borderColor = [UIColor clearColor].CGColor;
//    UILabel *enBtnTitle = (UILabel*)[_indonesiaButton viewWithTag:99];
//    enBtnTitle.textColor = kDefaultNavyBlueColor;
//    
//    [_englishButton setBackgroundImage:imageFromColor(kDefaultNavyBlueColor)
//                                    forState:UIControlStateNormal];
//    [_englishButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
//                                    forState:UIControlStateHighlighted];
//    _englishButton.layer.borderWidth = 1.0;
//    _englishButton.layer.borderColor = kDefaultWhiteColor.CGColor;
//    
//    UILabel *idBtnTitle = (UILabel*)[_englishButton viewWithTag:99];
//    idBtnTitle.textColor = kDefaultWhiteColor;
    
    [_englishButton addTarget:self action:@selector(performLangEN) forControlEvents:UIControlEventTouchUpInside];
    [_indonesiaButton addTarget:self action:@selector(performLangID) forControlEvents:UIControlEventTouchUpInside];
    
    [_languageView addSubview:_englishButton];
    [_languageView addSubview:_indonesiaButton];
}

-(void)toggleLanguageButton
{
    if(_indonesiaButton.tag == 1)
    {
        [_indonesiaButton setBackgroundImage:imageFromColor(kDefaultYellowColor)
                                  forState:UIControlStateNormal];
        [_indonesiaButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                  forState:UIControlStateHighlighted];
        _indonesiaButton.layer.borderWidth = 0.0;
        _indonesiaButton.layer.borderColor = [UIColor clearColor].CGColor;
        UILabel *enBtnTitle = (UILabel*)[_indonesiaButton viewWithTag:99];
        enBtnTitle.textColor = kDefaultNavyBlueColor;
        
        [_englishButton setBackgroundImage:imageFromColor(kDefaultNavyBlueColor)
                                    forState:UIControlStateNormal];
        [_englishButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                    forState:UIControlStateHighlighted];
        _englishButton.layer.borderWidth = 1.0;
        _englishButton.layer.borderColor = kDefaultWhiteColor.CGColor;
        
        UILabel *idBtnTitle = (UILabel*)[_englishButton viewWithTag:99];
        idBtnTitle.textColor = kDefaultWhiteColor;
    }
    else
    {
        [_englishButton setBackgroundImage:imageFromColor(kDefaultYellowColor)
                                  forState:UIControlStateNormal];
        [_englishButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                  forState:UIControlStateHighlighted];
        _englishButton.layer.borderWidth = 0.0;
        _englishButton.layer.borderColor = [UIColor clearColor].CGColor;
        UILabel *enBtnTitle = (UILabel*)[_englishButton viewWithTag:99];
        enBtnTitle.textColor = kDefaultNavyBlueColor;
        
        [_indonesiaButton setBackgroundImage:imageFromColor(kDefaultNavyBlueColor)
                                    forState:UIControlStateNormal];
        [_indonesiaButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                    forState:UIControlStateHighlighted];
        _indonesiaButton.layer.borderWidth = 1.0;
        _indonesiaButton.layer.borderColor = kDefaultWhiteColor.CGColor;
        
        UILabel *idBtnTitle = (UILabel*)[_indonesiaButton viewWithTag:99];
        idBtnTitle.textColor = kDefaultWhiteColor;
    }
}

- (void)login {
    
    // TODO : Hygiene
    if(_btnRememberPassword.tag == 1)
    {
        NSString *msisdn = _usernameTextField.text;
        NSString *pwd = _passwordTextField.text;
        NSString *saltKeyAPI = SALT_KEY;
        msisdn = [EncryptDecrypt doCipherForiOS7:msisdn action:kCCEncrypt withKey:saltKeyAPI];
        pwd = [EncryptDecrypt doCipherForiOS7:pwd action:kCCEncrypt withKey:saltKeyAPI];
        
        //saveDataToPreferenceWithKeyAndValue(@"value", msisdn);
        //saveDataToPreferenceWithKeyAndValue(@"key", pwd);
        
        // TODO : V1.9.5
        saveDataToPreferenceWithKeyAndValue(PROFILE_MSISDN_CACHE_KEY, msisdn);
        saveDataToPreferenceWithKeyAndValue(PROFILE_PASSWORD_CACHE_KEY, pwd);
        saveDataToPreferenceWithKeyAndValue(PROFILE_FLAG_KMSI_CACHE_KEY, @"1");\
        
        NSString *token = getDataFromPreferenceWithKey(PROFILE_TOKEN_CACHE_KEY);
        if ([token length] > 0) {
            saveDataToPreferenceWithKeyAndValue(PROFILE_TOKEN_CACHE_KEY, token);
        }
        else {
            saveDataToPreferenceWithKeyAndValue(PROFILE_TOKEN_CACHE_KEY, @"");
        }
    }
    else
    {
        //saveDataToPreferenceWithKeyAndValue(@"value", @"");
        //saveDataToPreferenceWithKeyAndValue(@"key", @"");
        
        // TODO : V1.9.5
        saveDataToPreferenceWithKeyAndValue(PROFILE_MSISDN_CACHE_KEY, @"");
        saveDataToPreferenceWithKeyAndValue(PROFILE_PASSWORD_CACHE_KEY, @"");
        saveDataToPreferenceWithKeyAndValue(PROFILE_FLAG_KMSI_CACHE_KEY, @"0");
        saveDataToPreferenceWithKeyAndValue(PROFILE_TOKEN_CACHE_KEY, @"");
    }
    
    [_usernameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    int currentLang = [Language getCurrentLanguage];
    NSString *lang = @"";
    if (currentLang == 0) {
        lang = @"en";
    }
    else {
        lang = @"id";
    }
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = SIGN_IN_REQ;
    
    // TODO : HYGIENE
    [request signInWithUsername:_usernameTextField.text withPassword:_passwordTextField.text withLanguage:lang withDeviceToken:@""];
}

// TODO : Hygiene
-(void)setupBottomView
{
    _rememberPasswordView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_loginButton.frame) + kDefaultComponentPadding, CGRectGetMinY(_loginButton.frame), CGRectGetWidth(self.view.frame) - CGRectGetMaxX(_loginButton.frame) - 20, CGRectGetHeight(_loginButton.frame))];
    _rememberPasswordView.tag = 999;
    _rememberPasswordView.backgroundColor = [UIColor clearColor];
    [_contentView addSubview:_rememberPasswordView];
    
    _btnRememberPassword = [[UIButton alloc] init];
    _btnRememberPassword.frame = CGRectMake(4, 4, 21, 21);
    [_btnRememberPassword setBackgroundColor:[UIColor blackColor]];
    [_btnRememberPassword setAlpha:0.5];
    [_btnRememberPassword setTitle:@"" forState:UIControlStateNormal];
    [_btnRememberPassword addTarget:self action:@selector(rememberPasswordChecked:) forControlEvents:UIControlEventTouchUpInside];
    _btnRememberPassword.layer.cornerRadius = 0;
    
    if(_checkedRememberPassword)
    {
        [_btnRememberPassword setImage:[UIImage imageNamed:@"check-on"] forState:UIControlStateNormal];
        _btnRememberPassword.tag = 1;
    }
    
    [_rememberPasswordView addSubview:_btnRememberPassword];
    
    CGFloat lblWidth = CGRectGetWidth(_rememberPasswordView.frame) - 21 - kDefaultComponentPadding;
    _lblRememberPassword = [[UILabel alloc] init];
    _lblRememberPassword.frame = CGRectMake(CGRectGetMaxX(_btnRememberPassword.frame) + kDefaultComponentPadding, _btnRememberPassword.frame.origin.y - 5.0, lblWidth, _btnRememberPassword.frame.size.height + 10.0);
    _lblRememberPassword.text = [Language get:@"remember_password" alter:nil];
    _lblRememberPassword.numberOfLines = 0;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0) {
        _lblRememberPassword.lineBreakMode = NSLineBreakByWordWrapping;
    }
    else {
        _lblRememberPassword.lineBreakMode = UILineBreakModeWordWrap;
    }
    
    _lblRememberPassword.textColor = [UIColor blackColor];
    _lblRememberPassword.textAlignment = NSTextAlignmentLeft;
    
    if(IS_IPAD)
    {
        _lblRememberPassword.font = [UIFont fontWithName:kDefaultFontLight size:14];
    }
    else
    {
        _lblRememberPassword.font = [UIFont fontWithName:kDefaultFontLight size:12];
    }
    
    _lblRememberPassword.textColor = kDefaultBlackColor;
    [_rememberPasswordView addSubview:_lblRememberPassword];
    
    [self createRequestPasswordButton];
}

-(void)createRequestPasswordButton
{
    _lblForgotPassword = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_loginButton.frame), CGRectGetMaxY(_loginButton.frame) + kDefaultComponentPadding *10, CGRectGetWidth(_usernameLabel.frame), CGRectGetHeight(_usernameLabel.frame))];
    
    _lblForgotPassword.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _lblForgotPassword.textColor = [UIColor blackColor];
    _lblForgotPassword.text = [Language get:@"forgot_password" alter:nil];
    [_contentView addSubview:_lblForgotPassword];
    
    CGFloat btnWidth = 180;
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize-2];
    UIColor *btnColor = [UIColor colorWithRed:255.0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
    
    _btnRequestPassword = createButtonWithFrame(CGRectMake(CGRectGetMinX(_loginButton.frame),
                                                           CGRectGetMaxY(_lblForgotPassword.frame) + kDefaultComponentPadding,
                                                           btnWidth,
                                                           kDefaultButtonHeight/1.5),
                                                [[Language get:@"request_password" alter:nil] uppercaseString],
                                                buttonFont,
                                                kDefaultWhiteColor,
                                                btnColor,
                                                kDefaultBlueColor);
    
    _btnRequestPassword.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    //NSLog(@"forgot password %@", NSStringFromCGRect(_lblForgotPassword.frame));
    //NSLog(@"button request password %@", NSStringFromCGRect(_btnRequestPassword.frame));
    [_btnRequestPassword addTarget:self action:@selector(requestPassword:) forControlEvents:UIControlEventTouchUpInside];
    
    [_contentView addSubview:_btnRequestPassword];
    
    _contentView.frame = CGRectMake(CGRectGetMinX(_contentView.frame), CGRectGetMinY(_contentView.frame), CGRectGetWidth(_contentView.frame), CGRectGetMaxY(_btnRequestPassword.frame) + 20);
//    [self showOffnetDisclaimer];
    [self setupLabels];
}

-(void)createLoginButton
{
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    
    UIButton *button = createButtonWithFrame(CGRectMake(CGRectGetMinX(_usernameTextField.frame),
                                                        CGRectGetMaxY(_authView.frame) + kDefaultComponentPadding,
                                                        CGRectGetWidth(_usernameTextField.frame)/2,
                                                        kDefaultButtonHeight),
                                             [[Language get:@"login" alter:nil] uppercaseString],
                                             buttonFont,
                                             kDefaultWhiteColor,
                                             kDefaultAquaMarineColor,
                                             kDefaultBlueColor);
   
    [button addTarget:self action:@selector(performLogin:) forControlEvents:UIControlEventTouchUpInside];
    
    _loginButton = button;
    [_contentView addSubview:_loginButton];

}
- (void)setupButton {
    //--------------//
    // Setup Button //
    //--------------//
    
    // Login Button
    /*
    UIFont *iconFont = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize];
    UIFont *titleFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    UIButton *button = createButtonWithIcon(CGRectMake(_passwordTextField.frame.origin.x,
                                                       _passwordTextField.frame.origin.y + _passwordTextField.frame.size.height + kDefaultComponentPadding,
                                                       110,
                                                       kDefaultButtonHeight),
                                            @"F",
                                            iconFont,
                                            kDefaultWhiteColor,
                                            [[Language get:@"login" alter:nil] uppercaseString],
                                            titleFont,
                                            kDefaultWhiteColor,
                                            kDefaultBlueColor,
                                            kDefaultBlueColor);*/
    
    // TODO : NEPTUNE
    [self createLoginButton];
//    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
//    
//    UIButton *button = createButtonWithFrame(CGRectMake(CGRectGetMinX(_authView.frame) + CGRectGetMinX(_usernameTextField.frame)-2,
//                                                        CGRectGetMaxY(_authView.frame) + kDefaultComponentPadding,
//                                                        100,
//                                                        kDefaultButtonHeight),
//                                             [[Language get:@"login" alter:nil] uppercaseString],
//                                             buttonFont,
//                                             kDefaultWhiteColor,
//                                             kDefaultAquaMarineColor,
//                                             kDefaultBlueColor);
//    
//    [button addTarget:self action:@selector(performLogin:) forControlEvents:UIControlEventTouchUpInside];
//    
//    _loginButton = button;
//    [_contentView addSubview:_loginButton];
    
//    _btnRememberPassword = [[UIButton alloc] init];
//    _btnRememberPassword.frame = CGRectMake(CGRectGetMinX(_lblRememberPassword.frame) - kDefaultComponentPadding - 21, CGRectGetMinY(_lblRememberPassword.frame), 21, CGRectGetHeight(_btnRememberPassword.frame));
//    [_btnRememberPassword setImage:[UIImage imageNamed:@"check-off"] forState:UIControlStateNormal];
    
//    [_contentView addSubview:_btnRememberPassword];
    
    /*
    // Register Button
    CGFloat padding = 10.0;
    CGFloat contentWidth = _contentView.frame.size.width - (padding*2);
    
    button = createButtonWithIconAndArrow(CGRectMake(padding, _loginButton.frame.origin.y + _loginButton.frame.size.height + 20.0, contentWidth, kDefaultButtonHeight),
                                          @"A",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultNavyBlueColor,
                                          [[Language get:@"registration" alter:nil] uppercaseString],
                                          [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize],
                                          kDefaultNavyBlueColor,
                                          @"d",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultNavyBlueColor,
                                          kDefaultBlueColor,
                                          kDefaultPinkColor);
    
    [button addTarget:self
               action:@selector(performRegister:)
     forControlEvents:UIControlEventTouchDown];
    
    _registerButton = button;
    [_contentView addSubview:_registerButton];
    
    // Forgot Password
    button = createButtonWithIconAndArrow(CGRectMake(padding, _registerButton.frame.origin.y + _registerButton.frame.size.height + 4.0, contentWidth, kDefaultButtonHeight),
                                          @"G",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultNavyBlueColor,
                                          [[Language get:@"forget_password" alter:nil] uppercaseString],
                                          [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize],
                                          kDefaultNavyBlueColor,
                                          @"d",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultNavyBlueColor,
                                          kDefaultBlueColor,
                                          kDefaultPinkColor);
    
    [button addTarget:self
               action:@selector(performForgotPassword:)
     forControlEvents:UIControlEventTouchDown];
    
    _forgotPasswordButton = button;
    [_contentView addSubview:_forgotPasswordButton];
    
    // Account Activation
    button = createButtonWithIconAndArrow(CGRectMake(padding, _forgotPasswordButton.frame.origin.y + _forgotPasswordButton.frame.size.height + 4.0, contentWidth, kDefaultButtonHeight),
                                          @"C",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultNavyBlueColor,
                                          [[Language get:@"account_activation" alter:nil] uppercaseString],
                                          [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize],
                                          kDefaultNavyBlueColor,
                                          @"d",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultNavyBlueColor,
                                          kDefaultBlueColor,
                                          kDefaultPinkColor);
    
    [button addTarget:self
               action:@selector(performAccountActivation:)
     forControlEvents:UIControlEventTouchDown];
    
    _accountActivationButton = button;
    [_contentView addSubview:_accountActivationButton];*/
    
    // TODO : NEPTUNE
//    CGRect frame = _bgTableView.frame;
//    if (IS_IPAD) {
//        //NSLog(@"self.view.bounds.size.height = %f",self.view.bounds.size.height);
//        //NSLog(@"self.view.bounds.size.width = %f",self.view.bounds.size.width);
//        frame.origin.y = self.view.bounds.size.height - _bgTableView.frame.size.height + 5; //+44
//        _bgTableView.frame = frame;
//        
//        frame = _theTableView.frame;
//        frame.origin.y = _bgTableView.frame.origin.y + 10;
//        _theTableView.frame = frame;
//    }
//    else {
//        frame.origin.y = self.view.bounds.size.height - _bgTableView.frame.size.height + 5;
//        _bgTableView.frame = frame;
//        
//        frame = _theTableView.frame;
//        frame.origin.y = _bgTableView.frame.origin.y + 10;
//        _theTableView.frame = frame;
//    }
    
    //--------------//
    // Content View //
    //--------------//
    //_contentView.backgroundColor = kDefaultButtonGrayColor;
    _contentView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _contentView.frame;
    newFrame.size.height = _theTableView.frame.origin.y + _theTableView.frame.size.height;
    _contentView.frame = newFrame;
    
}

- (void)setupTableViewContent {
    NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
    MenuModel *model = [[MenuModel alloc] init];
    model.menuName = [[Language get:@"registration" alter:nil] uppercaseString];
    model.href = @"#registration";
    [contentTmp addObject:model];
    [model release];
    model = nil;
    
    model = [[MenuModel alloc] init];
    model.menuName = [[Language get:@"forget_password" alter:nil] uppercaseString];
    model.href = @"#forget_password";
    [contentTmp addObject:model];
    [model release];
    model = nil;
    
    model = [[MenuModel alloc] init];
    model.menuName = [[Language get:@"account_activation" alter:nil] uppercaseString];
    model.href = @"#account_activation";
    [contentTmp addObject:model];
    [model release];
    model = nil;
    
    model = [[MenuModel alloc] init];
    model.menuName = [[Language get:@"language" alter:nil] uppercaseString];
    model.href = @"#language";
    [contentTmp addObject:model];
    [model release];
    model = nil;
    
    self.content = [NSArray arrayWithArray:contentTmp];
    [contentTmp release];
    
    [_theTableView reloadData];
}

- (void)performLangEN {
    
    int currentLang = [Language getCurrentLanguage];
    
    // EN
    if (currentLang == 0) {
        NSLog(@"Do Nothing");
    }
    else {
        NSString *newLang = @"en";
        [Language setLanguage:newLang];
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.profileData.language = newLang;
        _indonesiaButton.tag = 0;
        _englishButton.tag = 1;
        
        [self refreshView];
    }
}

- (void)performLangID {
    
    int currentLang = [Language getCurrentLanguage];
    
    // ID
    if (currentLang == 1) {
        NSLog(@"Do Nothing");
    }
    else {
        NSString *newLang = @"id";
        [Language setLanguage:newLang];
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.profileData.language = newLang;
        
        _indonesiaButton.tag = 1;
        _englishButton.tag = 0;
        
        [self refreshView];
    }
}

- (void)refreshView
{
    // TODO : Hygiene
//    if(!_lblErrorMsisdn.hidden)
//        _lblErrorMsisdn.text = [Language get:_errorMsisdnKey alter:nil];
    if(!_lblErrorMsisdn2.hidden)
        _lblErrorMsisdn2.text = [Language get:_errorMsisdnKey alter:nil];
    if(!_lblErrorPassword.hidden)
        _lblErrorPassword.text = [Language get:_errorPasswordKey alter:nil];
    
    _lblRememberPassword.text = [Language get:@"remember_password" alter:nil];
    _lblMsisdn.text = [Language get:@"your_xl_phone_number" alter:nil];
    _lblPassword.text = [Language get:@"password" alter:nil];
    _usernameLabel.text = [Language get:@"welcome_to_myxl" alter:nil];
   // _lblDisclaimer.text = [Language get:@"offnet_disclaimer" alter:nil];
    [_btnRequestPassword removeFromSuperview];
    [_lblForgotPassword removeFromSuperview];
    _btnRequestPassword = nil;
    [self createRequestPasswordButton];

//    _usernameLabel.text = [Language get:@"already_have_account" alter:nil];
    //_usernameTextField.placeholder = [Language get:@"your_xl_phone_number" alter:nil];
    //_passwordTextField.placeholder = [Language get:@"password" alter:nil];
    
    UIColor *color = [UIColor blackColor];
//    _usernameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[Language get:@"your_xl_phone_number" alter:nil]
//                                                                               attributes:@{NSForegroundColorAttributeName: color}];
    
    
    _passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[Language get:@"password" alter:nil]
                                                                               attributes:@{NSForegroundColorAttributeName: color}];
    // TODO : Hygiene
    [self setupLabels];
    // SETUP LANG BUTTONS
    [self toggleLanguageButton];
    
//    [self setupTableViewContent];
}

// TODO : HYGIENE
// Setup wording on all the labels and buttons according to the response from get profile
-(void)setupLabels
{
    DataManager *sharedData = [DataManager sharedInstance];
    if(sharedData.loginWordingData)
    {
        int currentLang = [Language getCurrentLanguage];
        if(currentLang == 1)
        {
            //ID
//            _lblDisclaimer.text = sharedData.loginWordingData.wordingID.info;
            _lblMsisdn.text = sharedData.loginWordingData.wordingID.msisdn;
            _lblPassword.text = sharedData.loginWordingData.wordingID.password;
            _lblRememberPassword.text = sharedData.loginWordingData.wordingID.remember;
//            _usernameTextField.placeholder = sharedData.loginWordingData.wordingID.msisdn;
            _passwordTextField.placeholder = sharedData.loginWordingData.wordingID.password;
            _lblForgotPassword.text = sharedData.loginWordingData.wordingID.guide;
            
            UILabel *lblLogin = (UILabel *)[_loginButton viewWithTag:99];
            lblLogin.text = [sharedData.loginWordingData.wordingID.btnLogin uppercaseString];
            _usernameLabel.text = sharedData.loginWordingData.wordingID.welcome;
        }
        else
        {
            // EN
//            _lblDisclaimer.text = sharedData.loginWordingData.wordingEN.info;
            _lblMsisdn.text = sharedData.loginWordingData.wordingEN.msisdn;
            _lblPassword.text = sharedData.loginWordingData.wordingEN.password;
            _lblRememberPassword.text = sharedData.loginWordingData.wordingEN.remember;
//            _usernameTextField.placeholder = sharedData.loginWordingData.wordingEN.msisdn;
            _passwordTextField.placeholder = sharedData.loginWordingData.wordingEN.password;
            _lblForgotPassword.text = sharedData.loginWordingData.wordingEN.guide;
            UILabel *lblLogin = (UILabel *)[_loginButton viewWithTag:99];
            lblLogin.text = [sharedData.loginWordingData.wordingEN.btnLogin uppercaseString];
            _usernameLabel.text = sharedData.loginWordingData.wordingEN.welcome;
        }
    }
}

// TODO : Hygiene
-(void)dismissKeyboardOnTap
{
    [_usernameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
}

// TODO : Hygiene
-(void)showOffnetDisclaimer
{
    if(_lblDisclaimer.hidden)
    {
//        _lblDisclaimer.text = [Language get:@"offnet_disclaimer" alter:nil];
//        _lblDisclaimer.hidden = NO;
        _loginButton.frame = CGRectOffset(_loginButton.frame, 0, CGRectGetHeight(_lblDisclaimer.frame) + 8);
        _authView.frame = CGRectOffset(_authView.frame, 0, CGRectGetHeight(_lblDisclaimer.frame) + 8);
        _rememberPasswordView.frame = CGRectOffset(_rememberPasswordView.frame, 0, CGRectGetHeight(_lblDisclaimer.frame) + 8);
        _btnRequestPassword.frame = CGRectOffset(_btnRequestPassword.frame, 0, CGRectGetHeight(_lblDisclaimer.frame) + 8);
        _lblForgotPassword.frame = CGRectOffset(_lblForgotPassword.frame, 0, CGRectGetHeight(_lblDisclaimer.frame) + 8);
        _contentView.frame = CGRectMake(CGRectGetMinX(_contentView.frame), CGRectGetMinY(_contentView.frame), CGRectGetWidth(_contentView.frame), CGRectGetMaxY(_btnRequestPassword.frame) + 20);
        [_theScrollView setContentSize:CGSizeMake(CGRectGetWidth(self.view.frame), CGRectGetMaxY(_contentView.frame) + 40)];
    }
}

-(void)hideOffnetDisclaimer
{
    if(!_lblDisclaimer.hidden)
    {
        _lblDisclaimer.hidden = YES;
        _loginButton.frame = CGRectOffset(_loginButton.frame, 0, -(CGRectGetHeight(_lblDisclaimer.frame) +8));
        _authView.frame = CGRectOffset(_authView.frame, 0, -(CGRectGetHeight(_lblDisclaimer.frame) +8));
        _rememberPasswordView.frame = CGRectOffset(_rememberPasswordView.frame, 0, -(CGRectGetHeight(_lblDisclaimer.frame) +8));
        _btnRequestPassword.frame = CGRectOffset(_btnRequestPassword.frame, 0, -(CGRectGetHeight(_lblDisclaimer.frame) +8));
        _lblForgotPassword.frame = CGRectOffset(_lblForgotPassword.frame, 0, -(CGRectGetHeight(_lblDisclaimer.frame) +8));
        _contentView.frame = CGRectMake(CGRectGetMinX(_contentView.frame), CGRectGetMinY(_contentView.frame), CGRectGetWidth(_contentView.frame), CGRectGetMaxY(_btnRequestPassword.frame) + 20);
        [_theScrollView setContentSize:CGSizeMake(CGRectGetWidth(self.view.frame), CGRectGetMaxY(_contentView.frame) + 40)];
    }
}

- (void)requestMenu
{
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = MENU_REQ;
    [request menu];
}

//- (NSString *)applicationDocumentsDirectory
//{
//    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
//}

/*
- (void)saveImage:(UIImage*)image withName:(NSString*)imageName
{
    NSData *imageData = UIImagePNGRepresentation(image); //convert image into .png format.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imageName]]; //add our image to the path
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
    
    NSLog(@"image saved");
}*/

-(void)updateVersionCheckWithNegativeSelector:(SEL)selector
{
    // TODO : V1.9
    // NEED TO PUT VERSION CHECK BEFORE THIS LINE
    DataManager *sharedData = [DataManager sharedInstance];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(sharedData.updateVersionModel)
    {
        if([sharedData.updateVersionModel.code integerValue] == kMajorNotificationUpdate)
        {
            [appDelegate applicationHasMajorUpdateWithMessage:sharedData.updateVersionModel.message andPosBtnTitle:sharedData.updateVersionModel.posBtnTitle];
        }
        else if([sharedData.updateVersionModel.code integerValue] == kMinorNotificationUpdate)
        {
            [appDelegate applicationHasMinorUpdateWithMessage:sharedData.updateVersionModel.message andPosBtnTitle:sharedData.updateVersionModel.posBtnTitle andNegativeBtnTitle:sharedData.updateVersionModel.cancelBtnTitle];
        }
        else
        {
            // Execute next step if the code attribute is missing from the response
            if([self respondsToSelector:selector])
                [self performSelector:selector withObject:nil afterDelay:0];
        }
    }
    else
    {
        // Go straight to getmenu if there's no update
        // Req Menu
        if([self respondsToSelector:selector])
            [self performSelector:selector withObject:nil afterDelay:0];
    }
}

#pragma mark - Logo Delegate

- (void)requestRetinaLogoCompleted:(ASIHTTPRequest *)request {
    //NSLog(@"imageFetchComplete");
    UIImage *imageValue = [UIImage imageWithData:[request responseData]];
    NSString *logoName = @"";
    
    DataManager *sharedData = [DataManager sharedInstance];
    LogoModel *logoModel1 = [sharedData.profileData.logo objectAtIndex:1];
    
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    
    saveImage(imageValue, logoName);
    
    // 7. Set Preferences
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *key = @"";
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        key = XL_LOGO_RETINA;
    }
    else {
        key = AXIS_LOGO_RETINA;
    }
    [prefs setObject:logoModel1.version
              forKey:key];
    [prefs synchronize];
    
    // 8. Set Logo
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *documentsDirectory = applicationDocumentsDirectory();
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",logoName]];
    [appDelegate.customNavBar updateLogo:fullPath];
    
    // TODO : V1.9
    [self updateVersionCheckWithNegativeSelector:@selector(requestMenu)];
    
//    [self requestMenu];
}

- (void)requestLogoFailed:(ASIHTTPRequest *)request {
    //NSError *error = [request error];
    //NSLog(@"error = %@",error);
    
    UIImage *imageValue = nil;
    NSString *logoName = @"";
    
    DataManager *sharedData = [DataManager sharedInstance];
    LogoModel *logoModel1 = [sharedData.profileData.logo objectAtIndex:1];
    
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    
    saveImage(imageValue, logoName);
    
    // 7. Set Preferences
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *key = @"";
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        key = XL_LOGO_RETINA;
    }
    else {
        key = AXIS_LOGO_RETINA;
    }
    [prefs setObject:logoModel1.version
              forKey:key];
    [prefs synchronize];
    
    // 8. Set Logo
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *documentsDirectory = applicationDocumentsDirectory();
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",logoName]];
    [appDelegate.customNavBar updateLogo:fullPath];
    
    // TODO : V1.9
    [self updateVersionCheckWithNegativeSelector:@selector(requestMenu)];
    // Req Menu
//    [self requestMenu];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    
    NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
    int type = [typeNumber intValue];
    
    BOOL success = [reqResult boolValue];
    
    if (success) {
        if (type == SIGN_IN_REQ) {
            
            //----------------//
            // CR Change Logo //
            //----------------//
            // 1. Check Existing Version for Standard Logo
            DataManager *sharedData = [DataManager sharedInstance];
            
            /*
            LogoModel *logoModel = [sharedData.profileData.logo objectAtIndex:0];
            BOOL stdLogoSame = isStandardLogoVersionSame(logoModel.version,sharedData.profileData.telcoOperator);
            
            if (!stdLogoSame) {
                // 2. Download Logo
                ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:logoModel.logoURL]];
                NSString *documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
                NSString *logoName = @"";
                if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
                    logoName = XL_LOGO_STD_NAME;
                }
                else {
                    logoName = AXIS_LOGO_STD_NAME;
                }
                NSString *mediaPath = [documentDirectory stringByAppendingPathComponent:logoName];
                [request setDownloadDestinationPath:mediaPath];
                [request startSynchronous];
                
                //---------- Test Keberadaan File ----------
                
                NSLog(@"Got the file!");
                NSURL *theURL = [NSURL URLWithString:mediaPath];
                
                NSLog(@"Time to Play File!");
                NSLog(@"Filename is %@", [theURL absoluteString]);
                
                BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[theURL absoluteString]];
                
                if (fileExists) {
                    NSLog(@"THE FILE EXISTS ZOMG");
                }
                
                // Create file manager
                NSError *error;
                NSFileManager *fileMgr = [NSFileManager defaultManager];
                
                // Point to Document directory
                NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
                
                // Write out the contents of home directory to console
                NSLog(@"Documents directory: %@", [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error]);
                //----------------------------------------
                
                // 3. Set Preferences
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *key = @"";
                if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
                    key = XL_LOGO_STD;
                }
                else {
                    key = AXIS_LOGO_STD;
                }
                [prefs setObject:logoModel.version
                          forKey:key];
                [prefs synchronize];
                
                // 4. Set Logo
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate.customNavBar updateLogo:mediaPath];
            }
            else {
                NSString *documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
                NSString *logoName = @"";
                if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
                    logoName = XL_LOGO_STD_NAME;
                }
                else {
                    logoName = AXIS_LOGO_STD_NAME;
                }
                NSString *mediaPath = [documentDirectory stringByAppendingPathComponent:logoName];
                
                // 4. Set Logo
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate.customNavBar updateLogo:mediaPath];
            }*/
            
            // 5. Check Existing Version for Retina Logo
            LogoModel *logoModel1 = [sharedData.profileData.logo objectAtIndex:1];
            BOOL retinaLogoSame = isRetinaLogoVersionSame(logoModel1.version,sharedData.profileData.telcoOperator);
            if (!retinaLogoSame) {
                // 6. Download Logo
//                ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:logoModel1.logoURL]];
//                [request setDidFinishSelector:@selector(requestRetinaLogoCompleted:)];
//                [request setDidFailSelector:@selector(requestLogoFailed:)];
//                [request setDelegate:self];
//                [request startAsynchronous];
                
                ASINetworkQueue *networkQueue = [[ASINetworkQueue alloc] init];
                [networkQueue reset];
                //[networkQueue setDownloadProgressDelegate:progressIndicator];
                [networkQueue setRequestDidFinishSelector:@selector(requestRetinaLogoCompleted:)];
                [networkQueue setRequestDidFailSelector:@selector(requestLogoFailed:)];
                //[networkQueue setShowAccurateProgress:[accurateProgress isOn]];
                [networkQueue setDelegate:self];
                
                ASIHTTPRequest *request;
                //NSLog(@"--> url logo = %@",logoModel1.logoURL);
                request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:logoModel1.logoURL]];
                [request setUserInfo:[NSDictionary dictionaryWithObject:@"1" forKey:@"tag"]];
                [networkQueue addOperation:request];
                
                [networkQueue go];
                
                
                /*
                ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:logoModel1.logoURL]];
                NSString *documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
                NSString *logoName = @"";
                if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
                    logoName = XL_LOGO_RETINA_NAME;
                }
                else {
                    logoName = AXIS_LOGO_RETINA_NAME;
                }
                NSString *mediaPath = [documentDirectory stringByAppendingPathComponent:logoName];
                [request setDownloadDestinationPath:mediaPath];
                [request startSynchronous];*/
                
                
            }
            else {
                NSString *documentsDirectory = applicationDocumentsDirectory();
                NSString *logoName = @"";
                if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
                    logoName = XL_LOGO_RETINA_NAME;
                }
                else {
                    logoName = AXIS_LOGO_RETINA_NAME;
                }
                NSString *mediaPath = [documentsDirectory stringByAppendingPathComponent:logoName];
                
                // 8. Set Logo
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate.customNavBar updateLogo:mediaPath];
                
                // TODO : V1.9
                [self updateVersionCheckWithNegativeSelector:@selector(requestMenu)];
                
                // Req Menu
//                [self requestMenu];
            }
            //----------------//
            
            //[self requestMenu];
        }
        if (type == MENU_REQ) {
            if ([_delegate respondsToSelector:@selector(viewControllerDidLoginSuccess:)])
                [_delegate viewControllerDidLoginSuccess:self];
        }
    }
    else {
        // TODO : Hygiene
//        NSString *errorCode = [result valueForKey:ERROR_CODE_KEY];
        NSString *reason = [result valueForKey:REASON_KEY];
        if (type == SIGN_IN_REQ) {
            // TODO : V1.9
            _lblErrorMsisdn.text = reason;
            _lblErrorMsisdn.hidden = NO;
            
//            // Msisdn is not registered/invalid
//            if([errorCode isEqualToString:@"239"])
//            {
//                _errorMsisdnKey = @"error_msisdn";
//                _lblErrorMsisdn.text = reason;
//                _lblErrorMsisdn.hidden = NO;
//            }
//            else if([errorCode isEqualToString:@"212"])
//            {
//                _errorPasswordKey = @"error_password";
//                _lblErrorPassword.text = reason;
//                _lblErrorPassword.hidden = NO;
//            }
//            else
//            {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
//                                                                message:reason
//                                                               delegate:self
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil];
//                [alert show];
//                [alert release];
//            }
        }
    }
}

#pragma mark - UITextField delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _usernameTextField)
    {
        NSInteger insertDelta = string.length - range.length;
        
        if (textField.text.length + insertDelta > MAX_MSISDN_LENGTH)
        {
            return NO;
        }
        else {
            return YES;
        }
    }
    else
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        // TODO : V1.9
        // Update the maximum character length for password to be 64
//        return !([newString length] > 8);
        return !([newString length] > MAX_PASSWORD_LENGTH);
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField == _usernameTextField)
    {
        _lblErrorMsisdn.hidden = YES;
        _lblErrorMsisdn2.hidden = YES;
    }
    else
    {
        _lblErrorPassword.hidden = YES;
    }
    
    CGFloat targetY = textField.frame.origin.y + 40;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    if (textField == _usernameTextField) {
//        return !([newString length] > 13);
//    }
//    else {
//        return !([newString length] > 8);
//    }
//}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // TODO : Hygiene
    return 1;
//    return [self.content count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO : Hygiene
    MenuModel *menuModel = [self.content objectAtIndex:indexPath.row + 3];
//    MenuModel *menuModel = [self.content objectAtIndex:indexPath.row];
    
    if ([menuModel.href isEqualToString:@"#language"]) {
        static NSString *CellIdentifier = @"LangCell";
        
        LanguageCell *cell = (LanguageCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[LanguageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            [cell.englishButton addTarget:self
                                   action:@selector(performLangEN)
                         forControlEvents:UIControlEventTouchUpInside];
            
            [cell.indonesiaButton addTarget:self
                                     action:@selector(performLangID)
                           forControlEvents:UIControlEventTouchUpInside];
        }
        
        cell.titleLabel.text = [menuModel.menuName uppercaseString];
        cell.iconLabel.text = @"I";
        
        cell.iconLabel.backgroundColor = kDefaultYellowColor;
        cell.iconLabel.textColor = kDefaultNavyBlueColor;
        cell.titleLabel.textColor = kDefaultWhiteColor;
        cell.lineView.backgroundColor = [UIColor clearColor];
        cell.lineView.hidden = YES;
        
        int currentLang = [Language getCurrentLanguage];
        // EN
        if (currentLang == 0) {
            [cell.englishButton setBackgroundImage:imageFromColor(kDefaultYellowColor)
                                          forState:UIControlStateNormal];
            [cell.englishButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                          forState:UIControlStateHighlighted];
            cell.englishButton.layer.borderWidth = 0.0;
            cell.englishButton.layer.borderColor = [UIColor clearColor].CGColor;
            UILabel *enBtnTitle = (UILabel*)[cell.englishButton viewWithTag:99];
            enBtnTitle.textColor = kDefaultNavyBlueColor;
            
            [cell.indonesiaButton setBackgroundImage:imageFromColor(kDefaultNavyBlueColor)
                                            forState:UIControlStateNormal];
            [cell.indonesiaButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                            forState:UIControlStateHighlighted];
            cell.indonesiaButton.layer.borderWidth = 1.0;
            cell.indonesiaButton.layer.borderColor = kDefaultWhiteColor.CGColor;
            
            UILabel *idBtnTitle = (UILabel*)[cell.indonesiaButton viewWithTag:99];
            idBtnTitle.textColor = kDefaultWhiteColor;
        }
        else {
            [cell.englishButton setBackgroundImage:imageFromColor(kDefaultNavyBlueColor)
                                          forState:UIControlStateNormal];
            [cell.englishButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                          forState:UIControlStateHighlighted];
            cell.englishButton.layer.borderWidth = 1.0;
            cell.englishButton.layer.borderColor = kDefaultWhiteColor.CGColor;
            
            UILabel *enBtnTitle = (UILabel*)[cell.englishButton viewWithTag:99];
            enBtnTitle.textColor = kDefaultWhiteColor;
            
            [cell.indonesiaButton setBackgroundImage:imageFromColor(kDefaultYellowColor)
                                            forState:UIControlStateNormal];
            [cell.indonesiaButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                            forState:UIControlStateHighlighted];
            cell.indonesiaButton.layer.borderWidth = 0.0;
            cell.indonesiaButton.layer.borderColor = [UIColor clearColor].CGColor;
            
            UILabel *idBtnTitle = (UILabel*)[cell.indonesiaButton viewWithTag:99];
            idBtnTitle.textColor = kDefaultNavyBlueColor;
        }
        
        return cell;
    }
    else {
        static NSString *CellIdentifier = @"Cell";
        
        CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        if ([menuModel.href isEqualToString:@"#registration"]) {
            cell.iconLabel.text = @"A";
        }
        else if ([menuModel.href isEqualToString:@"#forget_password"]) {
            cell.iconLabel.text = @"U";
        }
        else {
            cell.iconLabel.text = @"~";
        }
        
        cell.titleLabel.text = [menuModel.menuName uppercaseString];
        
        cell.infoButton.hidden = YES;
        cell.descLabel.hidden = YES;
        cell.refreshButton.hidden = YES;
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kDefaultCellHeight;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // TODO : Hygiene
    // disable these buttons as they are not needed anymore
    MenuModel *menuModel = [self.content objectAtIndex:indexPath.row];
    if ([menuModel.href isEqualToString:@"#registration"]) {
//        [self performRegister:nil];
    }
    else if ([menuModel.href isEqualToString:@"#forget_password"]) {
//        [self performForgotPassword:nil];
    }
    else if ([menuModel.href isEqualToString:@"#account_activation"]) {
//        [self performAccountActivation:nil];
    }
    else {
    }
}

- (void)viewDidUnload {
    [self setLblRememberPassword:nil];
    [self setBtnRememberPassword:nil];
    [self setBtnRequestPassword:nil];
    [self setLblMsisdn:nil];
    [self setLblPassword:nil];
    [self setLblErrorPassword:nil];
    [self setLblErrorMsisdn:nil];
    [self setIndonesiaButton:nil];
    [self setEnglishButton:nil];
    [self setLanguageView:nil];
    [self setLblDisclaimer:nil];
    [self setLblErrorMsisdn2:nil];
    [self setSmallLogoImageView:nil];
    [super viewDidUnload];
}

@end
