//
//  RequestPasswordViewController.m
//  MyXL
//
//  Created by tyegah on 2/10/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "RequestPasswordViewController.h"
#import "UnderLineLabel.h"
#import "RequestPasswordResultViewController.h"
#import "AppDelegate.h"

@interface RequestPasswordViewController ()<UITextFieldDelegate>
@property (retain, nonatomic) IBOutlet UILabel *lblNomor;
@property (retain, nonatomic) IBOutlet UITextField *txtMsisdn;
@property (retain, nonatomic) IBOutlet UILabel *lblEmail;
@property (retain, nonatomic) IBOutlet UITextField *txtEmailAddress;
@property (retain, nonatomic) IBOutlet UILabel *lblCaptchaCode;
@property (retain, nonatomic) IBOutlet UIWebView *webViewCaptcha;
@property (retain, nonatomic) IBOutlet UILabel *lblRefreshCaptcha;
@property (retain, nonatomic) IBOutlet UnderLineLabel *lblRecaptcha;
@property (retain, nonatomic) IBOutlet UITextField *txtCaptcha;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UIView *fieldView;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) UIButton *submitButton;
@property (retain, nonatomic) IBOutlet UILabel *lblErrorMsisdn;
@property (retain, nonatomic) IBOutlet UILabel *lblErrorEmail;
@property (retain, nonatomic) IBOutlet UILabel *lblErrorCaptcha;
@property (retain, nonatomic) IBOutlet UILabel *lblAsterisk;
@property (nonatomic, copy) NSString *errorMsisdnKey;
@property (nonatomic, retain) NSTimer *captchaTimer;
@property (nonatomic, retain) UITextField *activeTextField;

@property (nonatomic) float animatedDis;

@end

@implementation RequestPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _txtMsisdn.delegate = self;
    _txtEmailAddress.delegate = self;
    _txtCaptcha.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupNavigationBar];
    [self setupViewContent];
    // Captcha
    [self refreshCaptcha];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_captchaTimer invalidate];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    keyboardFrame = [self.view convertRect:keyboardFrame fromView:nil];
    
    CGFloat heightOfKeyboard = keyboardFrame.size.height;
    
    //NSLog(@"The keyboard is now %d pixels high.", (int)heightOfKeyboard);
    if(_activeTextField != nil)
        [self animateTextField:_activeTextField up:YES withDistance:heightOfKeyboard];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    keyboardFrame = [self.view convertRect:keyboardFrame fromView:nil];
    
    CGFloat heightOfKeyboard = keyboardFrame.size.height;
    
    //NSLog(@"The keyboard is now %d pixels high.", (int)heightOfKeyboard);
    [self animateTextField:_activeTextField up:NO withDistance:heightOfKeyboard];
    _activeTextField = nil;
}

- (void)refreshCaptcha
{
    if(_captchaTimer != nil)
        [_captchaTimer invalidate];
    [self scheduleTimerForCaptcha];
    
    NSString *captchaURL = generateCaptchaURL();
    NSLog(@"captcha URL %@", captchaURL);
    NSURL *captcha = [NSURL URLWithString:captchaURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:captcha];
    [_webViewCaptcha loadRequest:request];
    [request release];
}

-(void)setupNavigationBar
{
    //---------------------//
    // Show Navigation Bar //
    //---------------------//
    AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[self.navigationController navigationBar];
    navBar.hidden = NO;
    navBar.showDeviceLabel = NO;
    navBar.showMsisdnLabel = NO;
    
    // Change NavBar BgColor
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    changeNavbarBg(navBar, appDelegate.window.bounds);
    //----------------------
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *documentsDirectory = applicationDocumentsDirectory();
    NSString *logoName = @"mobifone_logo.png";
    /*
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else if ([sharedData.profileData.telcoOperator isEqualToString:@"AXIS"]) {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    else {
        logoName = XL_LOGO_RETINA_NAME;
    }*/
    
    NSString *mediaPath = [documentsDirectory stringByAppendingPathComponent:logoName];
    [navBar updateLogo:mediaPath];
}

-(void)setupViewContent
{
    //    //--------------//
    //    // Setup Header //
    //    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:[[Language get:@"request_password" alter:nil] uppercaseString]
                                                               isFirstSection:YES];
    headerView1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture.png"]];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:[[Language get:@"please_insert_no_and_email" alter:nil] uppercaseString]
                                                               isFirstSection:NO];
    headerView2.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture.png"]];
    [_contentView addSubview:headerView2];
    
    _txtMsisdn.font = [UIFont fontWithName:kDefaultFontLight size:13.0];
    _txtMsisdn.textColor = kDefaultBlackColor;
    _txtEmailAddress.font = [UIFont fontWithName:kDefaultFontLight size:13.0];
    _txtEmailAddress.textColor = kDefaultBlackColor;
    _txtCaptcha.font = [UIFont fontWithName:kDefaultFontLight size:13.0];
    _txtCaptcha.textColor = kDefaultBlackColor;
    
    // LABELS
    _lblNomor.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _lblNomor.textColor = kDefaultTitleFontGrayColor;
    NSString *text = [NSString stringWithFormat:@"%@*",[Language get:@"xl_number" alter:nil]];
    _txtMsisdn.placeholder = [Language get:@"xl_number" alter:nil];
    _lblNomor.text = text;
    _lblNomor.hidden = true;
    
    _lblCaptchaCode.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _lblCaptchaCode.textColor = kDefaultTitleFontGrayColor;
    text = [NSString stringWithFormat:@"%@*",[Language get:@"captcha_code" alter:nil]];
    _txtCaptcha.placeholder = [Language get:@"captcha_code" alter:nil];
    _lblCaptchaCode.text = text;
    _lblCaptchaCode.hidden = true;
    
    _lblEmail.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _lblEmail.textColor = kDefaultTitleFontGrayColor;
    text = [NSString stringWithFormat:@"%@",[Language get:@"email" alter:nil]];
    _txtEmailAddress.placeholder = text;
    _lblEmail.text = text;
    _lblEmail.hidden = true;
    
    _lblAsterisk.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSmallSize];
    _lblAsterisk.textColor = kDefaultRedColor;
    _lblAsterisk.text = [Language get:@"asterisk" alter:nil];
    
    // Refresh Label
    _lblRefreshCaptcha.font = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize];
    _lblRefreshCaptcha.textColor = kDefaultGreenColor;
    
    _lblRecaptcha.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    _lblRecaptcha.textColor = kDefaultGreenColor;
    _lblRecaptcha.text = [Language get:@"recaptcha" alter:nil];
    [_lblRecaptcha setShouldUnderline:NO];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(refreshCaptcha)];
    [_lblRecaptcha setUserInteractionEnabled:YES];
    [_lblRecaptcha addGestureRecognizer:tapGesture];
    [tapGesture release];
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Texture.png"]];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Setup Button //
    //--------------//
    
    // Activation Button
    UIFont *titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    
    NSLog(@"_txtCaptcha.frame.origin.y = %f",_txtCaptcha.frame.origin.y);
    NSLog(@"_txtCaptcha.frame.size.height = %f",_txtCaptcha.frame.size.height);
    NSLog(@"kDefaultComponentPadding = %f",kDefaultComponentPadding);
    NSLog(@"%f",_txtCaptcha.frame.origin.y + _txtCaptcha.frame.size.height + kDefaultComponentPadding);
    
    
    UIButton *button = createButtonWithFrame(CGRectMake(184,
                                                        400,
                                                        100,
                                                        kDefaultButtonHeight),
                                             [[Language get:@"submit" alter:nil] uppercaseString],
                                             titleFont,
                                             kDefaultWhiteColor,
                                             kDefaultAquaMarineColor,
                                             kDefaultBlueColor);
    
    [button addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    _submitButton = button;
    [_contentView addSubview:_submitButton];
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Texture.png"]];
    newFrame = _contentView.frame;
    newFrame.size.height = CGRectGetMaxY(_submitButton.frame) + kDefaultComponentPadding + 44 + 10;
    _contentView.frame = newFrame;
    
    _scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 200);
    
    [headerView1 release];
    [headerView2 release];
    headerView1 = nil;
    headerView2 = nil;
    
    // TODO : Hygiene
    UITapGestureRecognizer *dismissGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboardOnViewTouch:)];
    dismissGesture.numberOfTapsRequired = 1;
    dismissGesture.numberOfTouchesRequired = 1;
    [_fieldView addGestureRecognizer:dismissGesture];
    [dismissGesture release];
    dismissGesture = nil;
    
    // ERROR LABELS
    /*
    UIFont *errorFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontTitleSize];
    _lblErrorMsisdn.numberOfLines = 0;
    _lblErrorMsisdn.lineBreakMode = UILineBreakModeWordWrap;
    _lblErrorMsisdn.font = errorFont;
    
    _lblErrorEmail.numberOfLines = 0;
    _lblErrorEmail.lineBreakMode = UILineBreakModeWordWrap;
    _lblErrorEmail.font = errorFont;
    
    _lblErrorCaptcha.numberOfLines = 0;
    _lblErrorCaptcha.lineBreakMode = UILineBreakModeWordWrap;
    _lblErrorCaptcha.font = errorFont;
     */
}

// Timer for captcha refresh
-(void)scheduleTimerForCaptcha
{
    _captchaTimer = nil;
    _captchaTimer = [NSTimer scheduledTimerWithTimeInterval:CAPTCHA_REFRESH_TIME
                                                  target: self
                                                selector:@selector(refreshCaptcha)
                                                userInfo: nil repeats:YES];
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer:_captchaTimer forMode: NSDefaultRunLoopMode];
//    NSDate *time = [NSDate dateWithTimeIntervalSinceNow:CAPTCHA_REFRESH_TIME];
//    _captchaTimer = [[NSTimer alloc] initWithFireDate:time
//                                          interval:CAPTCHA_REFRESH_TIME
//                                            target:self
//                                          selector:@selector(refreshCaptcha)
//                                          userInfo:nil repeats:YES];
    
//    [t release];
}

-(void)performSubmit:(id)sender
{
    [self.view endEditing:YES];

    NSString *msisdn = _txtMsisdn.text;
    NSString *email = _txtEmailAddress.text;
    NSString *captcha = _txtCaptcha.text;
    
    BOOL validMsisdn = NO;
    if([msisdn isEqualToString:@""])
    {
        _errorMsisdnKey = @"error_general";
        _lblErrorMsisdn.hidden = NO;
//        _lblErrorMsisdn.text = [Language get:@"error_general" alter:nil];
        [self adjustViewOnErrorTextField:@"msisdn"];
    }
    else
    {
        validMsisdn = validateMsisdn(msisdn);
        if(!validMsisdn)
        {
            _errorMsisdnKey = @"error_msisdn";
                    _lblErrorMsisdn.hidden = NO;
//            _lblErrorMsisdn.text = [Language get:@"error_msisdn" alter:nil];
            [self adjustViewOnErrorTextField:@"msisdn"];
        }
        else if ([_txtMsisdn.text length] < 8)
        {
            _errorMsisdnKey = @"error_msisdn";
                    _lblErrorMsisdn.hidden = NO;
//            _lblErrorMsisdn.text = [Language get:@"error_msisdn" alter:nil];
            [self adjustViewOnErrorTextField:@"msisdn"];
        }
    }
    
    if([captcha isEqualToString:@""])
    {
//        _lblErrorCaptcha.text = [Language get:@"error_general" alter:nil];
        _lblErrorCaptcha.hidden = NO;
        [self adjustViewOnErrorTextField:@"captcha"];
    }
    
    if(!isValidEmail(email) && ![email isEqualToString:@""])
    {
        _lblErrorEmail.numberOfLines = 2;
        _lblErrorEmail.lineBreakMode = NSLineBreakByWordWrapping;
        _lblErrorEmail.text = [Language get:@"error_email" alter:nil];
        //NSLog(@"email error %@", [Language get:@"error_email" alter:nil]);
        [self adjustViewOnErrorTextField:@"email"];
    }
    
    if((validMsisdn && ![_txtCaptcha.text isEqualToString:@""] && [_txtMsisdn.text length] >= 8))
    {
        if((![email isEqualToString:@""] && isValidEmail(email)) || [email isEqualToString:@""])
        {
            [self requestPasswordWithMsisdn:msisdn andCaptcha:captcha andEmail:email];
        }
    }
}

-(void)dismissKeyboardOnViewTouch:(UIGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
}

-(void)adjustViewOnErrorTextField:(NSString *)errorField
{
    if ([errorField isEqualToString:@"msisdn"])
    {
        if(_lblErrorMsisdn.hidden)
        {
            CGFloat additionalHeight = 0;
            if ([_errorMsisdnKey isEqualToString:@"error_general"]) {
                _lblErrorMsisdn.frame = CGRectMake(CGRectGetMinX(_lblErrorMsisdn.frame), CGRectGetMaxY(_txtMsisdn.frame) + 8, CGRectGetWidth(_lblErrorMsisdn.frame), 21);
            }
            else
            {
                _lblErrorMsisdn.frame = CGRectMake(CGRectGetMinX(_lblErrorMsisdn.frame), CGRectGetMaxY(_txtMsisdn.frame) + 8, CGRectGetWidth(_lblErrorMsisdn.frame), 32);
                additionalHeight = 11;
            }
            
            _lblErrorMsisdn.hidden = NO;
            
            _txtMsisdn.layer.borderWidth = 3;
            _txtMsisdn.layer.borderColor = kDefaultRedColor.CGColor;
            _txtMsisdn.layer.cornerRadius = 5;
        
            if(additionalHeight == 0)
                _lblEmail.frame = CGRectOffset(_lblErrorMsisdn.frame, 0, CGRectGetHeight(_lblErrorCaptcha.frame));
            else
                _lblEmail.frame = CGRectOffset(_lblErrorMsisdn.frame, 0, CGRectGetHeight(_lblErrorCaptcha.frame) + kDefaultComponentPadding);
            _txtEmailAddress.frame = CGRectOffset(_txtEmailAddress.frame, 0, CGRectGetHeight(_lblErrorCaptcha.frame) + additionalHeight);
            _lblCaptchaCode.frame = CGRectOffset(_lblCaptchaCode.frame, 0, CGRectGetHeight(_lblErrorCaptcha.frame) + additionalHeight);
            _webViewCaptcha.frame = CGRectOffset(_webViewCaptcha.frame, 0, CGRectGetHeight(_lblErrorCaptcha.frame) + additionalHeight);
            _lblRefreshCaptcha.frame = CGRectOffset(_lblRefreshCaptcha.frame, 0, CGRectGetHeight(_lblErrorCaptcha.frame) + additionalHeight);
            _lblRecaptcha.frame = CGRectOffset(_lblRecaptcha.frame, 0, CGRectGetHeight(_lblErrorCaptcha.frame) + additionalHeight);
            _txtCaptcha.frame = CGRectOffset(_txtCaptcha.frame, 0, CGRectGetHeight(_lblErrorCaptcha.frame) + additionalHeight);
            if(!_lblErrorCaptcha.hidden)
                _lblErrorCaptcha.frame = CGRectOffset(_lblErrorCaptcha.frame, 0, CGRectGetHeight(_lblErrorCaptcha.frame) + additionalHeight);
            _submitButton.frame = CGRectOffset(_submitButton.frame, 0, CGRectGetHeight(_lblErrorCaptcha.frame) + additionalHeight);
        }
    }
    else if([errorField isEqualToString:@"email"])
    {
        if(_lblErrorEmail.hidden)
        {
            _lblErrorEmail.hidden = NO;
            
            _txtEmailAddress.layer.borderWidth = 3;
            _txtEmailAddress.layer.borderColor = kDefaultRedColor.CGColor;
            _txtEmailAddress.layer.cornerRadius = 5;
            
            _lblErrorEmail.frame = CGRectMake(CGRectGetMinX(_lblErrorEmail.frame), CGRectGetMaxY(_txtEmailAddress.frame) + 8, CGRectGetWidth(_lblErrorEmail.frame), CGRectGetHeight(_lblErrorEmail.frame));
            _lblCaptchaCode.frame = CGRectOffset(_lblCaptchaCode.frame, 0, CGRectGetHeight(_lblErrorEmail.frame) +kDefaultComponentPadding);
            _webViewCaptcha.frame = CGRectOffset(_webViewCaptcha.frame, 0, CGRectGetHeight(_lblErrorEmail.frame));
            _lblRefreshCaptcha.frame = CGRectOffset(_lblRefreshCaptcha.frame, 0, CGRectGetHeight(_lblErrorEmail.frame));
            _lblRecaptcha.frame = CGRectOffset(_lblRecaptcha.frame, 0, CGRectGetHeight(_lblErrorEmail.frame));
            _txtCaptcha.frame = CGRectOffset(_txtCaptcha.frame, 0, CGRectGetHeight(_lblErrorEmail.frame));
            _lblErrorCaptcha.frame = CGRectOffset(_lblErrorCaptcha.frame, 0, CGRectGetHeight(_lblErrorEmail.frame));
            _submitButton.frame = CGRectOffset(_submitButton.frame, 0, CGRectGetHeight(_lblErrorEmail.frame));
        }
    }
    else if([errorField isEqualToString:@"captcha"])
    {
        if(_lblErrorCaptcha.hidden)
        {
            _lblErrorCaptcha.hidden = NO;
            
            _txtCaptcha.layer.borderWidth = 3;
            _txtCaptcha.layer.borderColor = kDefaultRedColor.CGColor;
            _txtCaptcha.layer.cornerRadius = 5;
            
            _lblErrorCaptcha.frame = CGRectMake(CGRectGetMinX(_lblErrorCaptcha.frame), CGRectGetMaxY(_txtCaptcha.frame) + 8, CGRectGetWidth(_lblErrorCaptcha.frame), CGRectGetHeight(_lblErrorCaptcha.frame));
            _submitButton.frame = CGRectOffset(_submitButton.frame, 0, CGRectGetHeight(_lblErrorCaptcha.frame));
        }
    }
    
    _contentView.frame = CGRectMake(CGRectGetMinX(_contentView.frame), CGRectGetMinY(_contentView.frame), CGRectGetWidth(_contentView.frame), CGRectGetMaxY(_submitButton.frame) + 20);
    
    _scrollView.contentSize = CGSizeMake(CGRectGetWidth(_contentView.frame), CGRectGetMaxY(_contentView.frame) + 200);
}

-(void)adjustViewBackOnEdit:(NSString *)editedField
{
    if([editedField isEqualToString:@"msisdn"])
    {
        _txtMsisdn.layer.borderWidth = 0;
        _txtMsisdn.layer.borderColor = [[UIColor blackColor] CGColor];
        _txtMsisdn.layer.cornerRadius = 0;
        
        _lblErrorMsisdn.hidden = YES;

        _txtEmailAddress.frame = CGRectOffset(_txtEmailAddress.frame, 0, -(CGRectGetHeight(_lblErrorMsisdn.frame)));
        _lblCaptchaCode.frame = CGRectOffset(_lblCaptchaCode.frame, 0, -(CGRectGetHeight(_lblErrorMsisdn.frame)));
        _webViewCaptcha.frame = CGRectOffset(_webViewCaptcha.frame, 0, -(CGRectGetHeight(_lblErrorMsisdn.frame)));
        _lblRefreshCaptcha.frame = CGRectOffset(_lblRefreshCaptcha.frame, 0, -(CGRectGetHeight(_lblErrorMsisdn.frame)));
        _lblRecaptcha.frame = CGRectOffset(_lblRecaptcha.frame, 0, -(CGRectGetHeight(_lblErrorMsisdn.frame)));
        _txtCaptcha.frame = CGRectOffset(_txtCaptcha.frame, 0, -(CGRectGetHeight(_lblErrorMsisdn.frame)));
        _submitButton.frame = CGRectOffset(_submitButton.frame, 0, -(CGRectGetHeight(_lblErrorMsisdn.frame)));
        if(!_lblErrorCaptcha.hidden)
            _lblErrorCaptcha.frame = CGRectOffset(_lblErrorCaptcha.frame, 0, -(CGRectGetHeight(_lblErrorMsisdn.frame)));
        if(!_lblErrorEmail.hidden)
            _lblErrorEmail.frame = CGRectOffset(_lblErrorEmail.frame, 0, -(CGRectGetHeight(_lblErrorMsisdn.frame)));
        
        CGRect errorMsisdnFrame = _lblErrorMsisdn.frame;
        if(CGRectGetHeight(errorMsisdnFrame) > 21)
            errorMsisdnFrame.size.height = 21;
        
        _lblEmail.frame = errorMsisdnFrame;
    }
    else if([editedField isEqualToString:@"email"])
    {
        _txtEmailAddress.layer.borderWidth = 0;
        _txtEmailAddress.layer.borderColor = [[UIColor blackColor] CGColor];
        _txtEmailAddress.layer.cornerRadius = 0;
        
        _lblErrorEmail.hidden = YES;
        _lblCaptchaCode.frame = CGRectOffset(_lblCaptchaCode.frame, 0, -(CGRectGetHeight(_lblErrorEmail.frame) +kDefaultComponentPadding));
        CGRect errorEmailFrame = _lblCaptchaCode.frame;
        errorEmailFrame.size.height = 35;
        _lblErrorEmail.frame = errorEmailFrame;
        _webViewCaptcha.frame = CGRectOffset(_webViewCaptcha.frame, 0, -(CGRectGetHeight(_lblErrorEmail.frame)));
        _lblRefreshCaptcha.frame = CGRectOffset(_lblRefreshCaptcha.frame, 0, -(CGRectGetHeight(_lblErrorEmail.frame)));
        _lblRecaptcha.frame = CGRectOffset(_lblRecaptcha.frame, 0, -(CGRectGetHeight(_lblErrorEmail.frame)));
        _txtCaptcha.frame = CGRectOffset(_txtCaptcha.frame, 0, -(CGRectGetHeight(_lblErrorEmail.frame)));
        _submitButton.frame = CGRectOffset(_submitButton.frame, 0, -(CGRectGetHeight(_lblErrorEmail.frame)));
        if(!_lblErrorCaptcha.hidden)
            _lblErrorCaptcha.frame = CGRectOffset(_lblErrorCaptcha.frame, 0, -(CGRectGetHeight(_lblErrorEmail.frame)));
    }
    else if([editedField isEqualToString:@"captcha"])
    {
        _txtCaptcha.layer.borderWidth = 0;
        _txtCaptcha.layer.borderColor = [[UIColor blackColor] CGColor];
        _txtCaptcha.layer.cornerRadius = 0;
        
        _lblErrorCaptcha.hidden = YES;
        _submitButton.frame = CGRectOffset(_submitButton.frame, 0, -(CGRectGetHeight(_lblErrorCaptcha.frame)));
    }
    
    _contentView.frame = CGRectMake(CGRectGetMinX(_contentView.frame), CGRectGetMinY(_contentView.frame), CGRectGetWidth(_contentView.frame), CGRectGetMaxY(_submitButton.frame) + 20);
    
    _scrollView.contentSize = CGSizeMake(CGRectGetWidth(_contentView.frame), CGRectGetMaxY(_contentView.frame) + 200);
}

- (void)requestPasswordWithMsisdn:(NSString *)msisdn
                       andCaptcha:(NSString *)captcha
                         andEmail:(NSString *)email
{
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = RESET_PASSWORD_REQ;
    
    int currentLang = [Language getCurrentLanguage];
    NSString *lang = @"";
    if (currentLang == 0) {
        lang = @"en";
    }
    else {
        lang = @"id";
    }
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    if([sharedData.deviceToken length] > 0)
    {
        [request requestPassword:msisdn
                     withCaptcha:captcha
                       withEmail:email
                         withCid:sharedData.cid
                       withToken:@""
                    withLanguage:lang
                 withDeviceToken:sharedData.deviceToken];
    }
    else
    {
        [request requestPassword:msisdn
                     withCaptcha:captcha
                       withEmail:email
                         withCid:sharedData.cid
                       withToken:@""
                    withLanguage:lang
                 withDeviceToken:@""];
    }
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up withDistance:(CGFloat)distance
{
    CGPoint temp = [textField.superview convertPoint:textField.frame.origin toView:nil];
    if(up)
    {
        int moveUpValue = temp.y+textField.frame.size.height;
        //NSLog(@"moveup value %d", moveUpValue);
        _animatedDis = distance -(self.view.frame.size.height - moveUpValue);
        //NSLog(@"frame height %f, animatedDis %f", self.view.frame.size.height, _animatedDis);
    }
    
    if(_animatedDis>0)
    {
        const int movementDistance = _animatedDis;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        
        // animate the view up / down
        [UIView animateWithDuration:movementDuration animations:^ {
            self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        }];
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
    int type = [typeNumber intValue];
    BOOL success = [reqResult boolValue];
    if (success)
    {
        NSString *successCode = [result valueForKey:ERROR_CODE_KEY];
        if (type == RESET_PASSWORD_REQ)
        {
            _txtCaptcha.text = @"";
            _txtEmailAddress.text = @"";
            _txtMsisdn.text = @"";
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.tempMsisdn = _txtMsisdn.text;
            
            if([successCode isEqualToString:@"200"])
            {
                // show splash pop up message
                NSString *message = [result valueForKey:REASON_KEY];
                [[[[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:[Language get:@"close" alter:nil] otherButtonTitles:nil, nil] autorelease] show];
            }
            else if([successCode isEqualToString:@"201"])
            {
                // redirect to disclaimer page
                
                if (IS_IPAD) {
                    RequestPasswordResultViewController *viewController = [[RequestPasswordResultViewController alloc] initWithNibName:@"RequestPasswordResultViewController" bundle:nil];
                    viewController.isOffNet = YES;
                    [self.navigationController pushViewController:viewController animated:YES];
                    [viewController release];
                    viewController = nil;
                }
                else {
                    RequestPasswordResultViewController *viewController = [[RequestPasswordResultViewController alloc] initWithNibName:@"RequestPasswordResultViewController" bundle:nil];
                    viewController.isOffNet = NO;
                    [self.navigationController pushViewController:viewController animated:YES];
                    [viewController release];
                    viewController = nil;
                }
            }
        }
    }
    else
    {
        NSString *reason = [result valueForKey:REASON_KEY];
        NSString *errorCode = [result valueForKey:ERROR_CODE_KEY];
        if([errorCode isEqualToString:@"214"])
        {
            _errorMsisdnKey = @"error_msisdn";
            _lblErrorMsisdn.text = reason;
            [self adjustViewOnErrorTextField:@"msisdn"];
        }
        else if([errorCode isEqualToString:@"219"])
        {
            _lblErrorCaptcha.text = [Language get:@"error_captcha" alter:nil];
            [self adjustViewOnErrorTextField:@"captcha"];
        }
        else if([errorCode isEqualToString:@"9999"])
        {
            [[[[UIAlertView alloc] initWithTitle:@""
                                         message:reason
                                        delegate:nil
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil, nil] autorelease] show];
        }
        [self refreshCaptcha];
        _txtCaptcha.text = @"";
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _txtMsisdn)
    {
        NSInteger insertDelta = string.length - range.length;
        
        if (textField.text.length + insertDelta > MAX_MSISDN_LENGTH)
        {
            return NO;
        }
        else {
            return YES;
        }
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    _activeTextField = textField;

    if(!_lblErrorMsisdn.hidden && textField == _txtMsisdn)
    {
        [self adjustViewBackOnEdit:@"msisdn"];
    }
    else if(!_lblErrorEmail.hidden && textField == _txtEmailAddress)
    {
        [self adjustViewBackOnEdit:@"email"];
    }
    else if(!_lblErrorCaptcha.hidden && textField == _txtCaptcha)
    {
        [self adjustViewBackOnEdit:@"captcha"];
    }
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    _activeTextField = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    //NSLog(@"MEMORY WARNING ON REQ PASSWORD");
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_captchaTimer release];
    [_lblNomor release];
    [_txtMsisdn release];
    [_lblEmail release];
    [_txtEmailAddress release];
    [_lblCaptchaCode release];
    [_webViewCaptcha release];
    [_lblRefreshCaptcha release];
    [_lblRecaptcha release];
    [_txtCaptcha release];
    [_contentView release];
    [_fieldView release];
    [_scrollView release];
    [_lblErrorMsisdn release];
    [_lblErrorEmail release];
    [_lblErrorCaptcha release];
    [_lblAsterisk release];
    [_submitButton release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setLblNomor:nil];
    [self setTxtMsisdn:nil];
    [self setLblEmail:nil];
    [self setTxtEmailAddress:nil];
    [self setLblCaptchaCode:nil];
    [self setWebViewCaptcha:nil];
    [self setLblRefreshCaptcha:nil];
    [self setLblRecaptcha:nil];
    [self setTxtCaptcha:nil];
    [self setContentView:nil];
    [self setFieldView:nil];
    [self setScrollView:nil];
    [self setLblErrorMsisdn:nil];
    [self setLblErrorEmail:nil];
    [self setLblErrorCaptcha:nil];
    [self setLblAsterisk:nil];
    [self setSubmitButton:nil];
    [self setCaptchaTimer:nil];
    
    [super viewDidUnload];
}
@end
