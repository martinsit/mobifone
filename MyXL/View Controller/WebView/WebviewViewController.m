//
//  WebviewViewController.m
//  MyXL
//
//  Created by tyegah on 8/10/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "WebviewViewController.h"
#import "SubMenuViewController.h"
#import "ShopLocatorViewController.h"
#import "PaymentChoiceViewController.h"

@interface WebviewViewController ()<UIWebViewDelegate>

@end

@implementation WebviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [self setupLayout];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupLayout
{
    self.view.backgroundColor = kDefaultWhiteSmokeColor;
    NSURL *url = [NSURL URLWithString:_strURL];
    NSLog(@"URL = %@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:60.0];
    NSLog(@"request = %@",request);
    _webView.delegate = self;
    [_webView loadRequest:request];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
}

#pragma mark - UIWebView Delegate
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"did fail load error %@",[error localizedDescription]);
    [self.hud hide:YES];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"did start load");
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"did finish load");
    [self.hud hide:YES];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestDesc = [[request URL] absoluteString];
    NSLog(@"request DESC %@", requestDesc);
    if(![requestDesc isEqualToString:_strURL])
    {
        if([requestDesc rangeOfString:@"#"].location != NSNotFound)
        {
            NSString *hrefString = [requestDesc substringFromIndex:[requestDesc rangeOfString:@"#"].location];
            [self redirectToPageWithHref:hrefString];
        }
        else
            [self showModalWebViewForURL:[request URL]];
        return NO;
    }
    return YES;
}

-(void)redirectToPageWithHref:(NSString *)href
{
    DataManager *sharedData = [DataManager sharedInstance];
    if([href rangeOfString:@"paket"].location != NSNotFound)
    {
        NSString *parentID = [href substringFromIndex:[href rangeOfString:@"."].location+1];
        if([parentID length] > 0)
        {
            NSString *groupName = @"";
            // get the group name of the package
            if([parentID rangeOfString:@"_"].location != NSNotFound)
            {
                NSString *groupID = [parentID substringToIndex:[parentID rangeOfString:@"_"].location];
                NSArray *arr = [sharedData.menuData objectForKey:groupID];
                if([arr count] > 0)
                {
                    MenuModel *model = [arr objectAtIndex:0];
                    groupName = model.groupName;
                }
            }
            
            NSArray *menuArray = [sharedData.menuData objectForKey:parentID];
            if([menuArray count] > 0)
            {
                [self redirectToBuyPackageWithParentID:parentID andGroupName:groupName];
            }
        }
    }
    else if([href isEqualToString:@"#lokasi"])
    {
        [self redirectToStoreLocator];
    }
    else if([href isEqualToString:@"#reload_pg"])
    {
        [self redirectToReloadBalance];
    }
}

-(void)redirectToBuyPackageWithParentID:(NSString *)parentID andGroupName:(NSString *)groupName
{
    SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
    subMenuVC.parentId = parentID;
    if([groupName length] == 0)
        subMenuVC.grayHeaderTitle = [Language get:@"package_recommendation" alter:nil];
    else
        subMenuVC.grayHeaderTitle = groupName;
    subMenuVC.isHotOffer = YES;
    [self.navigationController pushViewController:subMenuVC animated:YES];
    [subMenuVC release];
    subMenuVC = nil;
}

-(void)redirectToStoreLocator
{
    ShopLocatorViewController *shopLocatorViewController = [[ShopLocatorViewController alloc] initWithNibName:@"ShopLocatorViewController" bundle:nil];
    [self.navigationController pushViewController:shopLocatorViewController animated:YES];
    [shopLocatorViewController release];
    shopLocatorViewController = nil;
}

-(void)redirectToReloadBalance
{
    DataManager *sharedData = [DataManager sharedInstance];
    NSArray *contentTmp = nil;
    NSDictionary *dict = [GroupingAndSorting doGrouping:sharedData.homeMenu];
    NSArray *objectsForMenu = [dict objectForKey:@"20"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"href==%@",@"#reload_pg"];
    NSArray *filteredArray = [objectsForMenu filteredArrayUsingPredicate:predicate];
    if([filteredArray count] > 0)
    {
        MenuModel *menuModel = [filteredArray objectAtIndex:0];
        contentTmp = [sharedData.menuData valueForKey:menuModel.menuId]; //-> 5 is menuID for reloadBalance
        // Grouping
        NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
        
        // Sorting
        NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
        
        NSArray *paymentMenu = [sortingResult allKeys];
        paymentMenu = [GroupingAndSorting doSortingByGroupId:paymentMenu];
        
        NSMutableArray *paymentMenuTmp = [[NSMutableArray alloc] init];
        for (int i=0; i<[paymentMenu count]; i++) {
            NSString *groupIdOfMenu = [paymentMenu objectAtIndex:i];
            NSArray *objectsForMenu = [sortingResult objectForKey:groupIdOfMenu];
            for (MenuModel *menu in objectsForMenu) {
                [paymentMenuTmp addObject:menu];
            }
        }
        
        PaymentChoiceViewController *subMenuVC = [[PaymentChoiceViewController alloc] initWithNibName:@"PaymentChoiceViewController" bundle:nil];
        subMenuVC.paymentMenu = paymentMenuTmp;
        subMenuVC.grayHeaderTitle = [Language get:@"balance_management" alter:nil];
        subMenuVC.blueHeaderTitle = menuModel.menuName;
        [subMenuVC createBarButtonItem:BACK];
        [self.navigationController pushViewController:subMenuVC animated:YES];
        [subMenuVC release];
        subMenuVC = nil;
    }
}

-(void)showModalWebViewForURL:(NSURL *)url
{
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
    webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:webViewController animated:YES completion:NULL];
}

- (void)dealloc {
    [_webView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setWebView:nil];
    [super viewDidUnload];
}
@end
