//
//  WebviewViewController.h
//  MyXL
//
//  Created by tyegah on 8/10/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface WebviewViewController : BasicViewController
@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, copy) NSString *strURL;

@end
