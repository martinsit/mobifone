//
//  BalanceViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/11/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BalanceViewController.h"
#import "CommonCell.h"
#import "BalanceCell.h"
#import "MenuModel.h"
#import "FooterView.h"
#import "GroupingAndSorting.h"
#import "DataManager.h"
#import "SectionHeaderView.h"

#import "AXISnetCellBackground.h"
#import "SectionFooterView.h"

#import "SubMenuViewController.h"

#import "BorderCellBackground.h"

#import "CheckBalanceViewController.h"
#import "CheckBonusViewController.h"
#import "ReloadBalanceViewController.h"
#import "BalanceTransferViewController.h"
#import "BuyVoucherStepOneViewController.h"
//#import "ABMenuViewController.h"

//#import <DropboxSDK/DropboxSDK.h>
//#import "ABRootViewController.h"

#import "Constant.h"

#import "TransactionHistoryViewController.h"

#import "EncryptDecrypt.h"
#import "AXISnetCommon.h"

#import "AppDelegate.h"

#import "NotificationViewController.h"

#import "AdsModel.h"

@interface BalanceViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (strong, nonatomic) IBOutlet KASlideShow *slideshow;
@property (strong, nonatomic) MenuModel *selectedMenu;

@property (nonatomic, retain) NSIndexPath *checkedIndexPath;
@property (readwrite) CGFloat extraHeight;

- (void)requestTransactionHistory;
- (void)requestBalanceItems;
- (void)requestPaymentParam;
- (void)performInfo:(id)sender;

@end

@implementation BalanceViewController

@synthesize sortedMenu = _sortedMenu;
@synthesize theSourceMenu = _theSourceMenu;

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
    FooterView *footer = [[[FooterView alloc] initWithFrame:CGRectMake(0.0, 0.0, _theTableView.bounds.size.width, 50.0)] autorelease];
    _theTableView.tableFooterView = footer;*/
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
    [_theTableView setBackgroundColor:kDefaultButtonGrayColor];
    
    //[self performAds:BALANCE_PAGE withSize:ADS_SIZE_OTHER_IPHONE];
}

- (void)viewWillAppear:(BOOL)animated {
    [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    
    DataManager *dataManager = [DataManager sharedInstance];
    NSArray *contentTmp = [dataManager.menuData valueForKey:dataManager.parentId];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    self.sortedMenu = [sortingResult allKeys];
    self.sortedMenu = [GroupingAndSorting doSortingByGroupId:self.sortedMenu];
    self.theSourceMenu = sortingResult;
    
    // Set Parent
    //self.currentSortedMenu = self.sortedMenu;
    //self.currentSourceMenu = self.theSourceMenu;
    //
    
    for (int i=0; i<[self.sortedMenu count]; i++) {
        NSString *groupIdOfMenu = [self.sortedMenu objectAtIndex:i];
        NSArray *objectsForMenu = [self.theSourceMenu objectForKey:groupIdOfMenu];
        for (MenuModel *menuModel in objectsForMenu) {
            menuModel.open = NO;
        }
    }
    self.checkedIndexPath = nil;
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.sortedMenu = nil;
    self.theSourceMenu = nil;
}

- (void)dealloc {
    [_sortedMenu release];
    _sortedMenu = nil;
    [_theSourceMenu release];
    _theSourceMenu = nil;
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sortedMenu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section]] count] + 1;
    }
    else {
        return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section]] count];
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierBalance = @"Balance";
    static NSString *CellIdentifierCommon = @"Common";
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        BalanceCell *cell = (BalanceCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierBalance];
        
        if (cell == nil) {
            cell = [[BalanceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierBalance];
            
            cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
            cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
            
            cell.userInteractionEnabled = NO;
        }
        
        DataManager *sharedData = [DataManager sharedInstance];
        
        cell.msisdnTitleLabel.text = [[Language get:@"your_axis_number" alter:nil] uppercaseString];
        
        NSString *saltKeyAPI = SALT_KEY;
        NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.checkBalanceData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
        cell.msisdnValueLabel.text = msisdn;
        
        cell.statusTitleLabel.text = [[Language get:@"status" alter:nil] uppercaseString];
        if ([sharedData.checkBalanceData.accountStatus isEqualToString:@"1"]) {
            cell.statusValueLabel.text = [Language get:@"active" alter:nil];
            cell.validityValueLabel.text = changeDateFormat(sharedData.checkBalanceData.activeStopDate);
        }
        else {
            cell.statusValueLabel.text = [Language get:@"grace_period" alter:nil];
            cell.validityValueLabel.text = changeDateFormat(sharedData.checkBalanceData.suspendStopDate);
        }
        
        cell.balanceTitleLabel.text = [[Language get:@"your_balance" alter:nil] uppercaseString];
        NSString *balance = addThousandsSeparator(sharedData.checkBalanceData.balance, sharedData.profileData.language);
        cell.balanceValueLabel.text = balance;
        
        cell.validityTitleLabel.text = [[Language get:@"validity" alter:nil] uppercaseString];
        
        // Type
        cell.typeTitleLabel.text = [[Language get:@"type" alter:nil] uppercaseString];
        cell.typeValueLabel.text = sharedData.checkBalanceData.accountType;
        
        return cell;
    }
    else {
        
        //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierCommon];
        
        if (cell == nil) {
            cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierCommon];
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            [cell.infoButton addTarget:self
                                action:@selector(performInfo:)
                      forControlEvents:UIControlEventTouchUpInside];
        }
        
        cell.backgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
        cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
        ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
        
        NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
        NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
        
        MenuModel *menuModel;
        if (indexPath.section == 0) {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
        }
        else {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row];
        }
        
        cell.iconLabel.text = icon(menuModel.menuId);
        if ([cell.iconLabel.text length] <= 0) {
            cell.iconLabel.text = icon(@"default_icon");
        }
        
        cell.titleLabel.text = [menuModel.menuName uppercaseString];
        cell.titleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        //cell.needResize = YES;
        
        if ([menuModel.desc length] > 0) {
            cell.arrowLabel.hidden = YES;
            cell.infoButton.hidden = NO;
        }
        else {
            cell.arrowLabel.hidden = NO;
            cell.infoButton.hidden = YES;
        }
        
        // Accordion
        if (menuModel.open) {
            cell.descLabel.hidden = NO;
            
            CGRect newFrame = cell.descLabel.frame;
            newFrame.size.height = _extraHeight;
            cell.descLabel.frame = newFrame;
        }
        else {
            cell.descLabel.hidden = YES;
        }
        cell.descLabel.text = menuModel.desc;
        
        return cell;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *groupIdOfMenu = [self.sortedMenu objectAtIndex:section];
    NSArray *objectsForMenu = [self.theSourceMenu objectForKey:groupIdOfMenu];
    MenuModel *menuModel = [objectsForMenu objectAtIndex:0];
    return menuModel.groupName;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    
    //return [[[SectionFooterView alloc] init] autorelease];
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 135.0; //90
    }
    else {
        //return kDefaultCellHeight;
        NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
        NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
        
        MenuModel *menuModel;
        if (indexPath.section == 0) {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
        }
        else {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row];
        }
        
        CGFloat labelHeightDefault = 28.0;
        
        NSString *text = [menuModel.menuName uppercaseString];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 240.0, 28.0)];
        label.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        CGSize size = calculateExpectedSize(label, text);
        
        CGFloat extraHeightForTitle = 0.0;
        if (size.height > labelHeightDefault) {
            extraHeightForTitle = size.height - labelHeightDefault;
        }
        
        if (menuModel.open) {
            return kDefaultCellHeight + _extraHeight + extraHeightForTitle;
        }
        else {
            return kDefaultCellHeight + extraHeightForTitle;
        }
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    MenuModel *menuModel = nil;
    
    if (indexPath.section == 0) {
        if (indexPath.row != 0) {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
        }
    }
    else {
        menuModel = [objectsForMenu objectAtIndex:indexPath.row];
    }
    
    _selectedMenu = menuModel;
    
    if ([menuModel.href isEqualToString:@"#check_balance"]) {
        CheckBalanceViewController *checkBalanceVC = [[CheckBalanceViewController alloc] initWithNibName:@"CheckBalanceViewController" bundle:nil];
        checkBalanceVC.headerTitle = menuModel.menuName;
        checkBalanceVC.headerIcon = icon(menuModel.menuId);
        [self.navigationController pushViewController:checkBalanceVC animated:YES];
        checkBalanceVC = nil;
        [checkBalanceVC release];
    }
    else if ([menuModel.href isEqualToString:@"#topup_hvrn"]) {
        ReloadBalanceViewController *reloadBalanceVC = [[ReloadBalanceViewController alloc] initWithNibName:@"ReloadBalanceViewController" bundle:nil];
        reloadBalanceVC.headerTitle = menuModel.menuName;
        reloadBalanceVC.headerIcon = icon(menuModel.menuId);
        [self.navigationController pushViewController:reloadBalanceVC animated:YES];
        reloadBalanceVC = nil;
        [reloadBalanceVC release];
    }
    else if ([menuModel.href isEqualToString:@"#check_bonus"]) {
        CheckBonusViewController *checkBonusVC = [[CheckBonusViewController alloc] initWithNibName:@"CheckBonusViewController" bundle:nil];
        checkBonusVC.headerTitle = menuModel.menuName;
        checkBonusVC.headerIcon = icon(menuModel.menuId);
        [self.navigationController pushViewController:checkBonusVC animated:YES];
        checkBonusVC = nil;
        [checkBonusVC release];
    }
    else if ([menuModel.href isEqualToString:@"#balance_transfer"]) {
        [self requestBalanceItems];
    }
    else if ([menuModel.href isEqualToString:@"#history"]) {
        [self requestTransactionHistory];
    }
    else if ([menuModel.href isEqualToString:@"#buy_voucher"]) {
        [self requestPaymentParam];
    }
    else if ([menuModel.href isEqualToString:@"#axis_box"]) {
//        if (![[DBSession sharedSession] isLinked]) {
//            ABRootViewController *abRootViewController = [[ABRootViewController alloc] initWithNibName:@"ABRootViewController" bundle:nil];
//            
//            abRootViewController.parentId = menuModel.menuId;
//            abRootViewController.headerTitleMaster = menuModel.menuName;
//            abRootViewController.headerIconMaster = icon(menuModel.menuId);
//            
//            [self.navigationController pushViewController:abRootViewController animated:YES];
//            abRootViewController = nil;
//            [abRootViewController release];
//        }
//        else {
//            ABMenuViewController *abMenuViewController = [[ABMenuViewController alloc] initWithNibName:@"ABMenuViewController" bundle:nil];
//            
//            abMenuViewController.parentId = menuModel.menuId;
//            abMenuViewController.headerTitleMaster = menuModel.menuName;
//            abMenuViewController.headerIconMaster = icon(menuModel.menuId);
//            
//            [self.navigationController pushViewController:abMenuViewController animated:YES];
//            abMenuViewController = nil;
//            [abMenuViewController release];
//        }
    }
    else {
        SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
        
        //        NSString *groupIdOfMenu = [self.sortedMenu objectAtIndex:indexPath.section];
        //        NSArray *objectsForMenu = [self.theSourceMenu objectForKey:groupIdOfMenu];
        //        MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
        //        NSLog(@"menuModel.parentId = %@",menuModel.menuId);
        
        subMenuVC.parentId = menuModel.menuId;
        subMenuVC.groupDesc = menuModel.desc;
        //NSLog(@"subMenuVC.groupDesc = %@",subMenuVC.groupDesc);
        
        subMenuVC.headerIconMaster = icon(_selectedMenu.menuId);
        if ([subMenuVC.headerIconMaster length] <= 0) {
            subMenuVC.headerIconMaster = icon(@"default_icon");
        }
        
        subMenuVC.headerTitleMaster = _selectedMenu.menuName;
        
        [self.navigationController pushViewController:subMenuVC animated:YES];
        subMenuVC = nil;
        [subMenuVC release];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    /*
    SectionHeaderView *headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40.0)] autorelease];
    return headerView;*/
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu objectAtIndex:0];
    
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:icon([NSString stringWithFormat:@"gid_%@",menuModel.groupId])
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:icon([NSString stringWithFormat:@"gid_%@",menuModel.groupId])
                                                isFirstSection:NO] autorelease];
    }
    
    return headerView;
}

/*
- (NSString*)sectionHeaderTitleForSection:(NSInteger)section
{
    return nil;
}*/

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

#pragma mark - Selector

- (void)requestTransactionHistory {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = TRX_HISTORY_REQ;
    [request lastTransaction:@""];
}

- (void)requestBalanceItems {
    
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = BALANCE_ITEMS_REQ;
    [request balanceItems];
}

- (void)requestPaymentParam {
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = PAYMENT_PARAM_REQ;
    [request paymentParam];
}

- (void)performInfo:(id)sender {
    UIButton *button = (UIButton *)sender;
    CommonCell *cell = (CommonCell *)[[button superview] superview];
    int section = [_theTableView indexPathForCell:cell].section;
    int row = [_theTableView indexPathForCell:cell].row;
    
    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:selectedIndexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel;
    if (selectedIndexPath.section == 0) {
        menuModel = [objectsForMenu objectAtIndex:selectedIndexPath.row - 1];
    }
    else {
        menuModel = [objectsForMenu objectAtIndex:selectedIndexPath.row];
    }
    
    // Uncheck the previous checked row
    if (self.checkedIndexPath)
    {
        NSString *groupIdOfMenuLast = [_sortedMenu objectAtIndex:self.checkedIndexPath.section];
        NSArray *objectsForMenuLast = [_theSourceMenu objectForKey:groupIdOfMenuLast];
        
        MenuModel *uncheckSection;
        if (self.checkedIndexPath.section == 0) {
            uncheckSection = [objectsForMenuLast objectAtIndex:self.checkedIndexPath.row - 1];
        }
        else {
            uncheckSection = [objectsForMenuLast objectAtIndex:self.checkedIndexPath.row];
        }
        uncheckSection.open = NO;
    }
    
    if ([self.checkedIndexPath isEqual:selectedIndexPath])
    {
        self.checkedIndexPath = nil;
    }
    else
    {
        menuModel.open = YES;
        self.checkedIndexPath = selectedIndexPath;
        
        CommonCell *selectedCell = (CommonCell*)[_theTableView cellForRowAtIndexPath:selectedIndexPath];
        
        CGSize size = calculateExpectedSize(selectedCell.descLabel, menuModel.desc);
        
        _extraHeight = size.height;
    }
    
    [_theTableView reloadData];
}

- (void)setupAds {
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        CGRect frame = _slideshow.frame;
        frame.size.width = self.view.bounds.size.width;
        _slideshow.frame = frame;
        
        frame = _slideshow.topImageView.frame;
        frame.size.width = self.view.bounds.size.width;
        _slideshow.topImageView.frame = frame;
        
        frame = _slideshow.bottomImageView.frame;
        frame.size.width = self.view.bounds.size.width;
        _slideshow.bottomImageView.frame = frame;
    }
    
    _slideshow.delegate = self;
    [_slideshow setDelay:3]; // Delay between transitions
    [_slideshow setTransitionDuration:1]; // Transition duration
    [_slideshow setTransitionType:KASlideShowTransitionSlide]; // Choose a transition type (fade or slide)
    [_slideshow setImagesContentMode:UIViewContentModeScaleAspectFit]; // Choose a content mode for images to display
    
    for (int i=0; i<[self.adsContent count]; i++) {
        AdsModel *adsModel = [self.adsContent objectAtIndex:i];
        // TODO : BUG FIX
        if(adsModel.image)
            [_slideshow addImage:adsModel.image];
    }
    
    _slideshow.hidden = NO;
    [_slideshow start];
    
    CGRect frame = _theTableView.frame;
    frame.origin.y = _theTableView.frame.origin.y + _slideshow.frame.size.height;
    frame.size.height = _theTableView.frame.size.height - _slideshow.frame.size.height;
    _theTableView.frame = frame;
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == TRX_HISTORY_REQ) {
            DataManager *dataManager = [DataManager sharedInstance];
            
            TransactionHistoryViewController *transactionHistoryViewController = [[TransactionHistoryViewController alloc] initWithNibName:@"TransactionHistoryViewController" bundle:nil];
            
            transactionHistoryViewController.content = dataManager.lastTransactionData;
            
            transactionHistoryViewController.headerTitleMaster = _selectedMenu.menuName;
            transactionHistoryViewController.headerIconMaster = icon(_selectedMenu.menuId);
            
            [self.navigationController pushViewController:transactionHistoryViewController animated:YES];
            transactionHistoryViewController = nil;
            [transactionHistoryViewController release];
        }
        
        if (type == BALANCE_ITEMS_REQ) {
            BalanceTransferViewController *balanceTransferVC = [[BalanceTransferViewController alloc] initWithNibName:@"BalanceTransferViewController" bundle:nil];
            balanceTransferVC.headerTitle = _selectedMenu.menuName;
            balanceTransferVC.headerIcon = icon(_selectedMenu.menuId);
            [self.navigationController pushViewController:balanceTransferVC animated:YES];
            balanceTransferVC = nil;
            [balanceTransferVC release];
        }
        
        if (type == PAYMENT_PARAM_REQ) {
            BuyVoucherStepOneViewController *buyVoucherStepOneViewController = [[BuyVoucherStepOneViewController alloc] initWithNibName:@"BuyVoucherStepOneViewController" bundle:nil];
            buyVoucherStepOneViewController.headerTitleMaster = _selectedMenu.menuName;
            buyVoucherStepOneViewController.headerIconMaster = icon(_selectedMenu.menuId);
            [self.navigationController pushViewController:buyVoucherStepOneViewController animated:YES];
            buyVoucherStepOneViewController = nil;
            [buyVoucherStepOneViewController release];
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            [notificationViewController createBarButtonItem:BACK];
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
        
        if (type == SIGN_OUT_REQ) {
            AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            appDelegate.loggedIn = NO;
            
            // TODO : V1.9
            // set update device token to be YES again.
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.shouldUpdateDeviceToken = YES;
            // TODO : Hygiene
//            appDelegate.isAutoLogin = NO;
            [appDelegate showLoginView];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
