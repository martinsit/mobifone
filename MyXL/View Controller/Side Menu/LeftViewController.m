//
//  LeftViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/27/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "LeftViewController.h"

//#import "CommonCell.h"
#import "LeftCell.h"
#import "LeftCellBackground.h"

#import "DataManager.h"
#import "MenuModel.h"
#import "IIViewDeckController.h"

#import "AXISnetCommon.h"

//#import "BalanceViewController.h"
//#import "PackageViewController.h"
//#import "PromoViewController.h"
//#import "ASMenuViewController.h"
#import "SubMenuViewController.h"

#import "Constant.h"
#import "EncryptDecrypt.h"

#import <QuartzCore/QuartzCore.h>

//#import "CheckQuotaViewController.h"
#import "SectionHeaderView.h"
#import "GroupingAndSorting.h"

#import "CommonCell.h"
#import "AppDelegate.h"
#import "NotificationViewController.h"

#import "ChangePasswordViewController.h"

#import "ContactUsModel.h"
#import "ContactUsViewController.h"

#import "LanguageCell.h"

#import "PaymentChoiceViewController.h"

#import "QuotaCalculatorViewController.h"

#import "UpdateProfileViewController.h"

#import "BalanceSubmenuViewController.h"

#import "CustomBadge.h"

#import "InfoAndPromoViewController.h"

#import "WebviewViewController.h"

@interface LeftViewController ()


@property (strong, nonatomic) MenuModel *selectedMenu;
@property (strong, nonatomic) IBOutlet UILabel *msisdnLabel;
@property (strong, nonatomic) UIButton *indonesiaButton;
@property (strong, nonatomic) UIButton *englishButton;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong, nonatomic) IBOutlet UIView *bgView;
// TODO : V1.9
//@property (nonatomic, retain) NSIndexPath *inboxIndexpath;
@property (nonatomic) NSInteger inboxRow;
@property (nonatomic) NSInteger inboxSection;
@property (nonatomic) NSInteger unreadMessageCount;
@property (nonatomic, retain) CustomBadge *customBadgeView;

@end

@implementation LeftViewController

@synthesize staticMenu;
@synthesize content;
@synthesize hud;
@synthesize sortedMenu = _sortedMenu;
@synthesize theSourceMenu = _theSourceMenu;
@synthesize versionMenu;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.backgroundColor = kDefaultWhiteColor;
        /*
         * Restructure & Reskinning
         */
        DataManager *sharedData = [DataManager sharedInstance];
        _bgView.backgroundColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _inboxSection = -1;
    _inboxRow = -1;
    float currentVersion = 7.0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= currentVersion) {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    if ([msisdn length] > 0) {
        msisdn = [msisdn stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"0"];
    }
    
    _msisdnLabel.text = msisdn;
    _msisdnLabel.font = [UIFont fontWithName:kDefaultFontLight size:20.0];
    
    _versionLabel.text = [Language get:@"app_version" alter:nil];
    _versionLabel.font = [UIFont fontWithName:kDefaultFontLight size:11.0];
    _versionLabel.textColor = kDefaultWhiteColor;
    _versionLabel.backgroundColor = [UIColor clearColor];
    _footerView.backgroundColor = [UIColor clearColor];
    
    /*
     * Add ID and EN Button
     */
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    
    _englishButton = createButtonWithFrame(CGRectMake(_msisdnLabel.frame.size.width - kDefaultButtonHeight - 5.0, (_headerView.frame.size.height - kDefaultButtonHeight)/2.0, kDefaultButtonHeight, kDefaultButtonHeight),
                                           @"EN",
                                           buttonFont,
                                           kDefaultNavyBlueColor,
                                           kDefaultYellowColor,
                                           kDefaultAquaMarineColor);
    [_englishButton addTarget:self
                       action:@selector(performLangEN)
             forControlEvents:UIControlEventTouchUpInside];
    
    _indonesiaButton = createButtonWithFrame(CGRectMake(_englishButton.frame.origin.x - kDefaultButtonHeight - 5.0, (_headerView.frame.size.height - kDefaultButtonHeight)/2.0, kDefaultButtonHeight, kDefaultButtonHeight),
                                             @"ID",
                                             buttonFont,
                                             kDefaultNavyBlueColor,
                                             kDefaultYellowColor,
                                             kDefaultAquaMarineColor);
    [_indonesiaButton addTarget:self
                         action:@selector(performLangID)
               forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_indonesiaButton];
    [_headerView addSubview:_englishButton];
    //---------------------
    
    //NSLog(@"------$$$$$$$$$$$$$-------");
    //DataManager *sharedData = [DataManager sharedInstance];
    self.content = sharedData.leftMenuData;
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:self.content];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    self.sortedMenu = [sortingResult allKeys];
    self.sortedMenu = [GroupingAndSorting doSortingByGroupId:self.sortedMenu];
    self.theSourceMenu = sortingResult;
    
    [self setupTableViewContent];
    
    [_theTableView reloadData];
    //NSLog(@"------#############-------");
}

// For iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    DataManager *sharedData = [DataManager sharedInstance];
    int currentLang = [Language getCurrentLanguage];
    // EN
    if (currentLang == 0) {
        [_englishButton setBackgroundImage:imageFromColor(kDefaultYellowColor)
                                  forState:UIControlStateNormal];
        [_englishButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                  forState:UIControlStateHighlighted];
        _englishButton.layer.borderWidth = 0.0;
        _englishButton.layer.borderColor = [UIColor clearColor].CGColor;
        UILabel *enBtnTitle = (UILabel*)[_englishButton viewWithTag:99];
        enBtnTitle.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
        
        [_indonesiaButton setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuColor))
                                    forState:UIControlStateNormal];
        [_indonesiaButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                    forState:UIControlStateHighlighted];
        _indonesiaButton.layer.borderWidth = 1.0;
        _indonesiaButton.layer.borderColor = kDefaultWhiteColor.CGColor;
        UILabel *idBtnTitle = (UILabel*)[_indonesiaButton viewWithTag:99];
        idBtnTitle.textColor = kDefaultWhiteColor;
    }
    else {
        [_englishButton setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuColor))
                                  forState:UIControlStateNormal];
        [_englishButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                  forState:UIControlStateHighlighted];
        _englishButton.layer.borderWidth = 1.0;
        _englishButton.layer.borderColor = kDefaultWhiteColor.CGColor;
        
        UILabel *enBtnTitle = (UILabel*)[_englishButton viewWithTag:99];
        enBtnTitle.textColor = kDefaultWhiteColor;
        
        [_indonesiaButton setBackgroundImage:imageFromColor(kDefaultYellowColor)
                                    forState:UIControlStateNormal];
        [_indonesiaButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                    forState:UIControlStateHighlighted];
        _indonesiaButton.layer.borderWidth = 0.0;
        _indonesiaButton.layer.borderColor = [UIColor clearColor].CGColor;
        UILabel *idBtnTitle = (UILabel*)[_indonesiaButton viewWithTag:99];
        idBtnTitle.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
    }
    
    [self requestUnreadNotification];
}

// TODO : V1.9
-(void)viewDidDisappear:(BOOL)animated
{
    // Cancel the unread notif request
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    [request cancelRequestWithRequestType:UNREAD_NOTIFICATION_REQ];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [staticMenu release];
    [content release];
    [hud release];
    hud = nil;
    [super dealloc];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return 2;
    return [_sortedMenu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    /*
    if (section == 0) {
        return [[self.theSourceMenu objectForKey:@"20"] count] + 2;
    }
    else {
        return [[self.theSourceMenu objectForKey:@"21"] count] + 1;
    }*/
    
    return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataManager *sharedData = [DataManager sharedInstance];
    
    static NSString *CellIdentifier = @"Cell";
    
    CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    //----------
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    MenuModel *menuModel;
    menuModel = [objectsForMenu objectAtIndex:indexPath.row];
    //-----------
    
    cell.titleLabel.text = [menuModel.menuName uppercaseString];
    // TODO : V1.9
    if([menuModel.href isEqualToString:@"#inbox"])
    {
        _inboxRow = indexPath.row;
        _inboxSection = indexPath.section;
    }
    
    NSString *icon = menuModel.icon;
    if ([icon length] <= 0) {
        icon = menuModel.hex;
    }
    cell.iconLabel.text = icon;
    
    //------------------//
    // Reskinning       //
    //------------------//
    if ([sharedData.profileData.themesModel.headerGradientStart isEqualToString:@""] &&
        [sharedData.profileData.themesModel.headerGradientEnd isEqualToString:@""]) {
        // AXIS
        cell.iconLabel.backgroundColor = kDefaultWhiteColor;
        cell.iconLabel.textColor = colorWithHexString(sharedData.profileData.themesModel.menuColor);
    }
    else {
        // XL
        cell.iconLabel.backgroundColor = kDefaultGrayColor;
        cell.iconLabel.textColor = kDefaultNavyBlueColor;
    }
    
    cell.titleLabel.textColor = kDefaultWhiteColor;
    cell.arrowLabel.textColor = kDefaultSeparatorColor;
    cell.lineView.backgroundColor = kDefaultSeparatorColor;
    //        CGRect lineFrame = cell.lineView.frame;
    //        lineFrame.size.height = 0.5;
    //        cell.lineView.frame = lineFrame;
    
    cell.infoButton.hidden = YES;
    cell.descLabel.hidden = YES;
    
    if (indexPath.section == 0) {
        if (indexPath.row - 2 == objectsForMenu.count -1) {
            cell.lineView.hidden = YES;
        }
        else {
            cell.lineView.hidden = NO;
        }
    }
    else {
        if (indexPath.row == objectsForMenu.count -1) {
            cell.lineView.hidden = YES;
        }
        else {
            cell.lineView.hidden = NO;
        }
    }
    
    cell.refreshButton.hidden = YES;
    
    return cell;
    
    /*
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"Cell";
     
        CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     
        if (cell == nil) {
            cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.backgroundColor = [UIColor clearColor];
        }
        
        //----------
        if (indexPath.row == 0) {
            objectsForMenu = staticMenu;
            menuModel = [objectsForMenu objectAtIndex:0];
        }
        else if (indexPath.row == 1) {
            objectsForMenu = staticMenu;
            menuModel = [objectsForMenu objectAtIndex:1];
        }
        else {
            objectsForMenu = [_theSourceMenu objectForKey:@"20"];
            menuModel = [objectsForMenu objectAtIndex:indexPath.row - 2];
        }
        //-----------
        
        cell.titleLabel.text = [menuModel.menuName uppercaseString];
        
        NSString *icon = menuModel.icon;
        if ([icon length] <= 0) {
            icon = menuModel.hex;
        }
        cell.iconLabel.text = icon;
        
        cell.iconLabel.backgroundColor = kDefaultGrayColor;
        cell.iconLabel.textColor = kDefaultNavyBlueColor;
        cell.titleLabel.textColor = kDefaultWhiteColor;
        cell.arrowLabel.textColor = kDefaultSeparatorColor;
        cell.lineView.backgroundColor = kDefaultSeparatorColor;
//        CGRect lineFrame = cell.lineView.frame;
//        lineFrame.size.height = 0.5;
//        cell.lineView.frame = lineFrame;
        
        cell.infoButton.hidden = YES;
        cell.descLabel.hidden = YES;
        
        if (indexPath.section == 0) {
            if (indexPath.row - 2 == objectsForMenu.count -1) {
                cell.lineView.hidden = YES;
            }
            else {
                cell.lineView.hidden = NO;
            }
        }
        else {
            if (indexPath.row == objectsForMenu.count -1) {
                cell.lineView.hidden = YES;
            }
            else {
                cell.lineView.hidden = NO;
            }
        }
        
        cell.refreshButton.hidden = YES;
        
        return cell;
    }
    else {
        
        if (indexPath.row == [[self.theSourceMenu objectForKey:@"21"] count]) {
            // Version
            objectsForMenu = versionMenu;
            menuModel = [objectsForMenu objectAtIndex:0];
            
            static NSString *CellIdentifier = @"VersionCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (nil == cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                              reuseIdentifier:CellIdentifier];
                cell.backgroundColor = [UIColor clearColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 3.0, cell.frame.size.width, 0.5)];
                lineView.backgroundColor = kDefaultSeparatorColor;
                [cell.contentView addSubview:lineView];
                [lineView release];
            }
            
            cell.textLabel.text = menuModel.menuName;
            cell.textLabel.textAlignment = UITextAlignmentCenter;
            cell.textLabel.font = [UIFont fontWithName:kDefaultFontLight size:11.0];
            cell.textLabel.textColor = kDefaultWhiteColor;
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.textLabel.numberOfLines = 0;
            cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
            
            return cell;
        }
        else {
            objectsForMenu = [_theSourceMenu objectForKey:@"21"];
            menuModel = [objectsForMenu objectAtIndex:indexPath.row];
            
            if ([menuModel.href isEqualToString:@"#change_password"] ||
                [menuModel.href isEqualToString:@"#about"] ||
                [menuModel.href isEqualToString:@"#logout"]) {
                
                static NSString *CellIdentifier = @"Cell";
                
                CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                if (cell == nil) {
                    cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    cell.backgroundColor = [UIColor clearColor];
                }
                
                cell.titleLabel.text = [menuModel.menuName uppercaseString];
                
                NSString *icon = menuModel.icon;
                if ([icon length] <= 0) {
                    icon = menuModel.hex;
                }
                cell.iconLabel.text = icon;
                
                cell.iconLabel.backgroundColor = kDefaultGrayColor;
                cell.iconLabel.textColor = kDefaultNavyBlueColor;
                cell.titleLabel.textColor = kDefaultWhiteColor;
                cell.arrowLabel.textColor = kDefaultSeparatorColor;
                cell.lineView.backgroundColor = kDefaultSeparatorColor;
                
                cell.infoButton.hidden = YES;
                cell.descLabel.hidden = YES;
                
                if (indexPath.section == 0) {
                    if (indexPath.row - 2 == objectsForMenu.count -1) {
                        cell.lineView.hidden = YES;
                    }
                    else {
                        cell.lineView.hidden = NO;
                    }
                }
                else {
                    if (indexPath.row == objectsForMenu.count -1) {
                        cell.lineView.hidden = YES;
                    }
                    else {
                        cell.lineView.hidden = NO;
                    }
                }
                
                cell.refreshButton.hidden = YES;
                
                return cell;
            }
            else {
                static NSString *CellIdentifier = @"Cell";
                
                LanguageCell *cell = (LanguageCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                if (cell == nil) {
                    cell = [[LanguageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    cell.backgroundColor = [UIColor clearColor];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    
                    [cell.englishButton addTarget:self
                                           action:@selector(performLangEN)
                                 forControlEvents:UIControlEventTouchUpInside];
                    
                    [cell.indonesiaButton addTarget:self
                                             action:@selector(performLangID)
                                   forControlEvents:UIControlEventTouchUpInside];
                }
                
                cell.titleLabel.text = [menuModel.menuName uppercaseString];
                
                NSString *icon = menuModel.icon;
                if ([icon length] <= 0) {
                    icon = menuModel.hex;
                }
                cell.iconLabel.text = icon;
                //NSLog(@"--> icon = %@",icon);
                
                cell.iconLabel.backgroundColor = kDefaultGrayColor;
                cell.iconLabel.textColor = kDefaultNavyBlueColor;
                cell.titleLabel.textColor = kDefaultWhiteColor;
                cell.lineView.backgroundColor = kDefaultSeparatorColor;
                
                int currentLang = [Language getCurrentLanguage];
                // EN
                if (currentLang == 0) {
                    [cell.englishButton setBackgroundImage:imageFromColor(kDefaultYellowColor)
                                                  forState:UIControlStateNormal];
                    [cell.englishButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                                  forState:UIControlStateHighlighted];
                    cell.englishButton.layer.borderWidth = 0.0;
                    cell.englishButton.layer.borderColor = [UIColor clearColor].CGColor;
                    UILabel *enBtnTitle = (UILabel*)[cell.englishButton viewWithTag:99];
                    enBtnTitle.textColor = kDefaultNavyBlueColor;
                    //[cell.englishButton setTitleColor:kDefaultNavyBlueColor forState:UIControlStateNormal];
                    //[cell.englishButton setTitleColor:kDefaultNavyBlueColor forState:UIControlStateHighlighted];
                    
                    [cell.indonesiaButton setBackgroundImage:imageFromColor(kDefaultNavyBlueColor)
                                                    forState:UIControlStateNormal];
                    [cell.indonesiaButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                                    forState:UIControlStateHighlighted];
                    cell.indonesiaButton.layer.borderWidth = 1.0;
                    cell.indonesiaButton.layer.borderColor = kDefaultWhiteColor.CGColor;
                    //[cell.indonesiaButton setTitleColor:kDefaultWhiteColor forState:UIControlStateNormal];
                    //[cell.indonesiaButton setTitleColor:kDefaultWhiteColor forState:UIControlStateHighlighted];
                    UILabel *idBtnTitle = (UILabel*)[cell.indonesiaButton viewWithTag:99];
                    idBtnTitle.textColor = kDefaultWhiteColor;
                }
                else {
                    [cell.englishButton setBackgroundImage:imageFromColor(kDefaultNavyBlueColor)
                                                  forState:UIControlStateNormal];
                    [cell.englishButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                                  forState:UIControlStateHighlighted];
                    cell.englishButton.layer.borderWidth = 1.0;
                    cell.englishButton.layer.borderColor = kDefaultWhiteColor.CGColor;
                    
                    UILabel *enBtnTitle = (UILabel*)[cell.englishButton viewWithTag:99];
                    enBtnTitle.textColor = kDefaultWhiteColor;
                    //[cell.englishButton setTitleColor:kDefaultWhiteColor forState:UIControlStateNormal];
                    //[cell.englishButton setTitleColor:kDefaultWhiteColor forState:UIControlStateHighlighted];
                    
                    [cell.indonesiaButton setBackgroundImage:imageFromColor(kDefaultYellowColor)
                                                    forState:UIControlStateNormal];
                    [cell.indonesiaButton setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                                                    forState:UIControlStateHighlighted];
                    cell.indonesiaButton.layer.borderWidth = 0.0;
                    cell.indonesiaButton.layer.borderColor = [UIColor clearColor].CGColor;
                    //[cell.indonesiaButton setTitleColor:kDefaultNavyBlueColor forState:UIControlStateNormal];
                    //[cell.indonesiaButton setTitleColor:kDefaultNavyBlueColor forState:UIControlStateHighlighted];
                    UILabel *idBtnTitle = (UILabel*)[cell.indonesiaButton viewWithTag:99];
                    idBtnTitle.textColor = kDefaultNavyBlueColor;
                }
                
                if (indexPath.section == 0) {
                    if (indexPath.row - 2 == objectsForMenu.count -1) {
                        cell.lineView.hidden = YES;
                    }
                    else {
                        cell.lineView.hidden = NO;
                    }
                }
                else {
                    if (indexPath.row == objectsForMenu.count -1) {
                        cell.lineView.hidden = YES;
                    }
                    else {
                        cell.lineView.hidden = NO;
                    }
                }
                
                return cell;
            }
        }
        
        
    }*/
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    /*
    if (section == 0) {
        return [Language get:@"main_menu" alter:nil];
    }
    else {
        return [Language get:@"apps_info_setting" alter:nil];
    }*/
    
    NSString *groupIdOfMenu = [self.sortedMenu objectAtIndex:section];
    NSArray *objectsForMenu = [self.theSourceMenu objectForKey:groupIdOfMenu];
    MenuModel *menuModel = [objectsForMenu objectAtIndex:0];
    return menuModel.groupName;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, defaultSectionHeaderHeight)
                                                               withLabelForXL:sectionTitle
                                                               isFirstSection:NO] autorelease];
    headerView.titleLabel.text = [sectionTitle capitalizedString];
    
    // Add Action Home
    if (section == 0) {
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(performHome)];
        [headerView addGestureRecognizer:singleFingerTap];
        [singleFingerTap release];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return defaultSectionHeaderHeight;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 50.0;
//}

/*
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width - (15*2), defaultSectionHeaderHeight)];
    if (section == [tableView numberOfSections] -1) {
        footer.backgroundColor = [UIColor clearColor];
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15,0,footer.frame.size.width,footer.frame.size.height)];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.text = [Language get:@"app_version" alter:nil];
        lbl.textAlignment = NSTextAlignmentLeft;
        [lbl setFont:[UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize]];
        [lbl setTextColor:kDefaultWhiteColor];
        [footer addSubview:lbl];
        self.theTableView.tableFooterView = footer;
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == [tableView numberOfSections] - 1) {
        return defaultSectionHeaderHeight;
    }
    else {
        return 0.0;
    }
}*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    _selectedMenu = [objectsForMenu objectAtIndex:indexPath.row];
    
    /*
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            _selectedMenu = [self.staticMenu objectAtIndex:indexPath.row];
        }
        else if (indexPath.row == 1) {
            _selectedMenu = [self.staticMenu objectAtIndex:indexPath.row];
        }
        else {
            NSArray *objectsForMenu = [_theSourceMenu objectForKey:@"20"];
            _selectedMenu = [objectsForMenu objectAtIndex:indexPath.row - 2];
        }
    }
    else {
        if (indexPath.row == [[self.theSourceMenu objectForKey:@"21"] count]) {
            return;
        }
        else {
            NSArray *objectsForMenu = [_theSourceMenu objectForKey:@"21"];
            _selectedMenu = [objectsForMenu objectAtIndex:indexPath.row];
        }
    }*/
    
    if ([_selectedMenu.href isEqualToString:@"#home"]) {
        [self performHome];
    }
    else if ([_selectedMenu.href isEqualToString:@"#inbox"]) {
        [self performNotification];
    }
    else if ([_selectedMenu.href isEqualToString:@"#account"]) {
        //[self performInfo:_selectedMenu withLevelTwoType:COMMON];
        [self performUpdateProfile];
    }
    else if ([_selectedMenu.href isEqualToString:@"#package"]) {
        [self performInfo:_selectedMenu withLevelTwoType:COMMON];
    }
    else if ([_selectedMenu.href isEqualToString:@"#xltunai"] || [_selectedMenu.hrefweb isEqualToString:@"#xltunai"] || [_selectedMenu.hrefweb isEqualToString:@"#xltunai/id/7"]) {
        [self performXLTunaiBalance];
    }
    else if ([_selectedMenu.href isEqualToString:@"#language"]) {
    }
    else if ([_selectedMenu.href isEqualToString:@"#change_password"]) {
        [self performChangePassword:nil];
    }
    else if ([_selectedMenu.href isEqualToString:@"#about"]) {
        [self performAbout];
    }
    else if ([_selectedMenu.href isEqualToString:@"#logout"]) {
        [self performLogout:nil];
    }
    else if ([_selectedMenu.href isEqualToString:@"#version"]) {
        // Do nothing
    }
    else if ([_selectedMenu.href isEqualToString:@"#reload_pg"]) {
        [self performPaymentChoice:_selectedMenu];
    }
    else if ([_selectedMenu.href isEqualToString:@"#quotacalculator"]) {
        [self requestQuotaCalculator];
    }
    else if ([_selectedMenu.href isEqualToString:@"#promo"]) {
        [self performPromo];
    }
    else if ([_selectedMenu.href isEqualToString:@"#balance"]) {
        [self performBalanceSubMenu:nil];
    }
    // 4G Readiness Check
    else if([_selectedMenu.href isEqualToString:@"#openwebview"])
    {
        /*
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        // Screen tracking
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"MyXLiOS"     // Event category (required)
                                                              action:@"Cek"  // Event action (required)
                                                               label:@"4G Ready"          // Event label
                                                               value:nil] build]];
        [[GAI sharedInstance] dispatch];*/
        
        [self openWebViewWithURLString:_selectedMenu.desc];
    }
    else {
        [self performInfo:_selectedMenu withLevelTwoType:COMMON];
    }
    
    DataManager *dataManager = [DataManager sharedInstance];
    dataManager.parentId = _selectedMenu.menuId;
    dataManager.menuModel = _selectedMenu;
}

#pragma mark - Selector

- (void)setupTableViewContent {
    /*
    NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
    MenuModel *model = [[MenuModel alloc] init];
    model.menuName = [[Language get:@"home" alter:nil] uppercaseString];
    model.href = @"#home";
    model.icon = @"s";
    [contentTmp addObject:model];
    [model release];
    model = nil;
    
    model = [[MenuModel alloc] init];
    model.menuName = [[Language get:@"inbox" alter:nil] uppercaseString];
    model.href = @"#inbox";
    model.icon = @"P";
    [contentTmp addObject:model];
    [model release];
    model = nil;
    
    self.staticMenu = [NSArray arrayWithArray:contentTmp];
    [contentTmp release];
    
    // For Version
    NSMutableArray *contentTmp2 = [[NSMutableArray alloc] init];
    MenuModel *model2 = [[MenuModel alloc] init];
    model2.menuName = [Language get:@"app_version" alter:nil];
    model2.href = @"#version";
    model2.icon = @"";
    [contentTmp2 addObject:model2];
    [model2 release];
    model2 = nil;
    self.versionMenu = [NSArray arrayWithArray:contentTmp2];
    [contentTmp2 release];*/
}

- (void)performHome {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.navigationController popToRootViewControllerAnimated:YES];
}

- (void)performNotification {
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = NOTIFICATION_REQ;
    [request notification];
}

- (void)performChangePassword:(id)sender {
    ChangePasswordViewController *changePasswordViewController = [[ChangePasswordViewController alloc] initWithNibName:@"ChangePasswordViewController" bundle:nil];
    
    [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
        
        [self.navigationController popToRootViewControllerAnimated:NO];
        self.viewDeckController.centerController = changePasswordViewController;
        
        ChangePasswordViewController *changePasswordVC = (ChangePasswordViewController*)self.viewDeckController.centerController;
        [changePasswordVC createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    }];
}

- (void)performAbout {
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
    ContactUsModel *model = [[ContactUsModel alloc] init];
    
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        model.label = [[Language get:@"about_title_1" alter:nil] uppercaseString];
        model.value = [Language get:@"about_desc_1" alter:nil];
        [contentTmp addObject:model];
        [model release];
        model = nil;
        
        model = [[ContactUsModel alloc] init];
        model.label = [[Language get:@"about_title_2" alter:nil] uppercaseString];
        model.value = [Language get:@"about_desc_2" alter:nil];
        [contentTmp addObject:model];
        [model release];
        model = nil;
    }
    else {
        model.label = [[Language get:@"about_title_1_axis" alter:nil] uppercaseString];
        model.value = [Language get:@"about_desc_1_axis" alter:nil];
        [contentTmp addObject:model];
        [model release];
        model = nil;
    }
    
    NSArray *contentAbout = [NSArray arrayWithArray:contentTmp];
    [contentTmp release];
    
    ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
    contactUsViewController.content = contentAbout;
    contactUsViewController.accordionType = ABOUT_ACC;
    contactUsViewController.menuModel = _selectedMenu;
    
    [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        self.viewDeckController.centerController = contactUsViewController;
        
        ContactUsViewController *contactUsVC = (ContactUsViewController*)self.viewDeckController.centerController;
        [contactUsVC createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
        
    }];
}

- (void)performLogout:(id)sender {
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = SIGN_OUT_REQ;
    [request signOut];
}

- (void)performInfo:(MenuModel*)menuModel withLevelTwoType:(LevelTwoType)type {
    SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
//    subMenuVC.parentId = menuModel.menuId;
//    subMenuVC.groupDesc = menuModel.desc;
    
    subMenuVC.parentMenuModel = menuModel;
    
    subMenuVC.isLevel2 = YES;
    subMenuVC.levelTwoType = type;
    subMenuVC.grayHeaderTitle = menuModel.menuName;
    
    [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        self.viewDeckController.centerController = subMenuVC;
    }];
}

- (void)performLangEN {
    
    int currentLang = [Language getCurrentLanguage];
    
    // EN
    if (currentLang == 0) {
        NSLog(@"Do Nothing");
    }
    else {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = CHANGE_LANGUAGE_REQ;
        [request changeLanguage:@"en"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.dropdownMenuCreated = NO;
    }
}

- (void)performLangID {
    
    int currentLang = [Language getCurrentLanguage];
    
    // ID
    if (currentLang == 1) {
        NSLog(@"Do Nothing");
    }
    else {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = CHANGE_LANGUAGE_REQ;
        [request changeLanguage:@"id"];
        
        DataManager *sharedData = [DataManager sharedInstance];
        sharedData.dropdownMenuCreated = NO;
    }
}

- (void)requestMenu {
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = MENU_REQ;
    [request menu];
}

- (void)updateView {
    DataManager *sharedData = [DataManager sharedInstance];
    self.content = [NSArray arrayWithArray:[sharedData.menuData valueForKey:@"0"]];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:self.content];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    self.sortedMenu = [sortingResult allKeys];
    self.sortedMenu = [GroupingAndSorting doSortingByGroupId:self.sortedMenu];
    self.theSourceMenu = sortingResult;
    
    [self setupTableViewContent];
    
    [_theTableView reloadData];
}

- (void)performXLTunaiBalance {
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = XL_TUNAI_BALANCE_REQ;
    [request xlTunaiBalance];
}

- (void)performPaymentChoice:(MenuModel*)menuModel {
    PaymentChoiceViewController *subMenuVC = [[PaymentChoiceViewController alloc] initWithNibName:@"PaymentChoiceViewController" bundle:nil];
    
    //-- Setting Payment Menu --//
    DataManager *dataManager = [DataManager sharedInstance];
    
    NSArray *contentTmp = nil;
    contentTmp = [dataManager.menuData valueForKey:menuModel.menuId];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    NSArray *paymentMenu = [sortingResult allKeys];
    paymentMenu = [GroupingAndSorting doSortingByGroupId:paymentMenu];
    
    NSMutableArray *paymentMenuTmp = [[NSMutableArray alloc] init];
    for (int i=0; i<[paymentMenu count]; i++) {
        NSString *groupIdOfMenu = [paymentMenu objectAtIndex:i];
        NSArray *objectsForMenu = [sortingResult objectForKey:groupIdOfMenu];
        for (MenuModel *menu in objectsForMenu) {
            [paymentMenuTmp addObject:menu];
        }
    }
    
    subMenuVC.paymentMenu = paymentMenuTmp;
    subMenuVC.grayHeaderTitle = [Language get:@"balance_management" alter:nil];
    subMenuVC.blueHeaderTitle = menuModel.menuName;
    subMenuVC.isLevel2 = YES;
    
    [paymentMenuTmp release];
    
    //---------------------------//
    
    [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        self.viewDeckController.centerController = subMenuVC;
        
        PaymentChoiceViewController *subMenuVCTmp = (PaymentChoiceViewController*)self.viewDeckController.centerController;
        [subMenuVCTmp viewWillAppear:YES];
        
    }];
    
    
}

- (void)requestQuotaCalculator {
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = QUOTA_CALCULATOR_REQ;
    [request quotaCalculator];
}

- (void)performQuotaCalculator {
    
    QuotaCalculatorViewController *quotaCalculatorViewController = [[QuotaCalculatorViewController alloc] initWithNibName:@"QuotaCalculatorViewController" bundle:nil];
    
    [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        self.viewDeckController.centerController = quotaCalculatorViewController;
        
        QuotaCalculatorViewController *quotaCalcVC = (QuotaCalculatorViewController*)self.viewDeckController.centerController;
        [quotaCalcVC createBarButtonItem:MENU_BACK_LEVEL2];
        [quotaCalcVC viewWillAppear:YES];
    }];
}

- (void)performPromo {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDate *promoCacheDate = [prefs objectForKey:PROMO_CACHE_KEY];
    NSDate *currentDate = [NSDate date];
    NSTimeInterval secondsBetween = [currentDate timeIntervalSinceDate:promoCacheDate];
    float minutes = secondsBetween / 60;
    
    if (promoCacheDate == nil || minutes > kDefaultCache) {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = PROMO_REQ;
        [request promo];
    }
    else {
        DataManager *sharedData = [DataManager sharedInstance];
        
        // TODO : V1.9
        // DROP 2
        InfoAndPromoViewController *infoAndPromoVC = [[InfoAndPromoViewController alloc] initWithNibName:@"InfoAndPromoViewController" bundle:nil];
        infoAndPromoVC.contentData = sharedData.promoData;
        infoAndPromoVC.menuModel = _selectedMenu;
        infoAndPromoVC.isParentPromo = YES;
        infoAndPromoVC.pageTitle = _selectedMenu.menuName;
        
        [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
            [self.navigationController popToRootViewControllerAnimated:NO];
            self.viewDeckController.centerController = infoAndPromoVC;
        }];
    }
}

- (void)performUpdateProfile {
    
    //[self requestProfileInfo];
    
    
    DataManager *sharedData = [DataManager sharedInstance];
    int value = [sharedData.profileData.status intValue];
    if (value == 2) {
        [self requestProfileInfo];
    }
    else {
        UpdateProfileViewController *updateProfileViewController = [[UpdateProfileViewController alloc] initWithNibName:@"UpdateProfileViewController" bundle:nil];
        updateProfileViewController.menuModel = _selectedMenu;
        
        [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
            [self.navigationController popToRootViewControllerAnimated:NO];
            self.viewDeckController.centerController = updateProfileViewController;
            
            UpdateProfileViewController *updateProfileVC = (UpdateProfileViewController*)self.viewDeckController.centerController;
            [updateProfileVC createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
        }];
    }
}

- (void)requestProfileInfo {
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = [Language get:@"loading" alter:nil];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = PROFILE_INFO_REQ;
    [request profileInfo];
}

- (void)performBalanceSubMenu:(id)sender {
    
    BalanceSubmenuViewController *subMenuVC = [[BalanceSubmenuViewController alloc] initWithNibName:@"BalanceSubmenuViewController" bundle:nil];
    
    [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        self.viewDeckController.centerController = subMenuVC;
        
        BalanceSubmenuViewController *subMenu = (BalanceSubmenuViewController*)self.viewDeckController.centerController;
        [subMenu createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    }];
    
}

- (void)openWebViewWithURLString:(NSString *)strURL
{
    WebviewViewController *webviewVC = [[WebviewViewController alloc] initWithNibName:@"WebviewViewController" bundle:nil];
    webviewVC.strURL = strURL;
    
    [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        self.viewDeckController.centerController = webviewVC;
        
        WebviewViewController *subMenu = (WebviewViewController*)self.viewDeckController.centerController;
        [subMenu createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    }];

}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        NSNumber *repeatResult = [result valueForKey:REPEAT_KEY];
        BOOL repeat = [repeatResult boolValue];
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
                [self.navigationController popToRootViewControllerAnimated:NO];
                self.viewDeckController.centerController = notificationViewController;
                
                NotificationViewController *notificationVC = (NotificationViewController*)self.viewDeckController.centerController;
                [notificationVC createBarButtonItem:MENU_BACK_LEVEL2];
                
            }];
            
            [notificationViewController.theTableView reloadData];
        }
        
        if (type == SIGN_OUT_REQ) {
            
            resetAllData();
            
            AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            appDelegate.loggedIn = NO;
            
            // TODO : V1.9
            // set update device token to be YES again.
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.shouldUpdateDeviceToken = YES;
            
            // TODO : Hygiene
            // Reset this autologin variable on the login view instead
//            appDelegate.isAutoLogin = NO;
            [appDelegate showLoginView];
            
            [self performHome];
            
        }
        
        if (type == CHANGE_LANGUAGE_REQ) {
            [self requestMenu];
        }
        
        if (type == MENU_REQ) {
            [self updateView];
            [self performHome];
        }
        
        if (type == XL_TUNAI_BALANCE_REQ) {
            if (repeat) {
                [self performXLTunaiBalance];
            }
            else {
                [self performInfo:_selectedMenu withLevelTwoType:XL_TUNAI];
            }
        }
        
        if (type == QUOTA_CALCULATOR_REQ) {
            [self performQuotaCalculator];
        }
        
        if (type == PROMO_REQ) {
            
            // Changes by iNot at 24 May 2013
            // Set Current Datetime to Preference for cache
            NSDate *current = [NSDate date];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setObject:current
                      forKey:PROMO_CACHE_KEY];
            [prefs synchronize];
            //
            
            DataManager *sharedData = [DataManager sharedInstance];
            
            // DROP 2
            InfoAndPromoViewController *infoAndPromoVC = [[InfoAndPromoViewController alloc] initWithNibName:@"InfoAndPromoViewController" bundle:nil];
            infoAndPromoVC.contentData = sharedData.promoData;
            infoAndPromoVC.menuModel = _selectedMenu;
            infoAndPromoVC.isParentPromo = YES;
            infoAndPromoVC.pageTitle = _selectedMenu.menuName;
            
            [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
                [self.navigationController popToRootViewControllerAnimated:NO];
                self.viewDeckController.centerController = infoAndPromoVC;
            }];
            
        }
        
        if (type == PROFILE_INFO_REQ) {
            [self performUpdateProfile];
            
            /*
            UpdateProfileViewController *updateProfileViewController = [[UpdateProfileViewController alloc] initWithNibName:@"UpdateProfileViewController" bundle:nil];
            updateProfileViewController.menuModel = _selectedMenu;
            
            [self.viewDeckController closeLeftViewBouncing:^(IIViewDeckController *controller) {
                [self.navigationController popToRootViewControllerAnimated:NO];
                self.viewDeckController.centerController = updateProfileViewController;
                
                UpdateProfileViewController *updateProfileVC = (UpdateProfileViewController*)self.viewDeckController.centerController;
                [updateProfileVC createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
            }];*/
        }
    }
    else {
        
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

// TODO : V1.9
// Adding badge count for inbox message icon
#pragma mark - Badge Count For Inbox Icon
-(void)createBadgeOnMessageIcon
{
    @try
    {
        DataManager *sharedData = [DataManager sharedInstance];
        if(_unreadMessageCount != sharedData.unreadMsg)
        {
            _unreadMessageCount = sharedData.unreadMsg;
            if(_inboxSection >= 0)
            {
                CommonCell *cell = (CommonCell *)[self.theTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_inboxRow inSection:_inboxSection]];
                if(cell)
                {
                    CGRect iconFrame = cell.iconLabel.frame;
                    
                    if(_unreadMessageCount > 0)
                    {
                        CustomBadge *customBadge = (CustomBadge *)[cell.contentView viewWithTag:BADGE_ICON_TAG];
                        if(customBadge)
                        {
                            customBadge.hidden = NO;
                            [customBadge setBadgeText:[NSString stringWithFormat:@"%ld",(long)_unreadMessageCount]];
                        }
                        else
                        {
                            _customBadgeView = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%ld",(long)_unreadMessageCount]];
                            _customBadgeView.tag = BADGE_ICON_TAG;
                            _customBadgeView.badgeScaleFactor = 0.7;
                            [_customBadgeView setFrame:CGRectMake(CGRectGetMaxX(iconFrame)- (CGRectGetWidth(iconFrame)/2), 2, 20, 20)];
                            [cell.contentView addSubview:_customBadgeView];
                        }
                    }
                    else
                    {
                        CustomBadge *customBadge = (CustomBadge *)[cell.contentView viewWithTag:BADGE_ICON_TAG];
                        if(customBadge)
                        {
                            customBadge.hidden = YES;
                        }
                    }
                }
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

-(void)requestUnreadNotification
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    {
        if(appDelegate.loggedIn)
        {
            AXISnetRequest *request = [AXISnetRequest sharedInstance];
            request.delegate = self;
//            request.requestType = UNREAD_NOTIFICATION_REQ;
            [request getUnreadMessageNotificationWithDelegate:self andCallBack:@selector(requestUnreadNotificationDone)];
        }
    }
}

-(void)requestUnreadNotificationDone
{
    [self createBadgeOnMessageIcon];
}

@end
