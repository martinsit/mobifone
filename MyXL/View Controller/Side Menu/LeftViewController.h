//
//  LeftViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/27/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "AXISnetRequest.h"

@interface LeftViewController : UIViewController <AXISnetRequestDelegate> {
    MBProgressHUD *hud;
    NSArray *_sortedMenu;
    NSDictionary *_theSourceMenu;
}

@property (nonatomic, retain) NSArray *staticMenu;
@property (nonatomic, retain) NSArray *content;
@property (nonatomic, retain) NSArray *sortedMenu;
@property (nonatomic, retain) NSDictionary *theSourceMenu;
@property (strong, nonatomic) MBProgressHUD *hud;
@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@property (nonatomic, retain) NSArray *versionMenu;

- (void)setupTableViewContent;

@end
