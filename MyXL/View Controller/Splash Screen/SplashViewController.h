//
//  SplashViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/9/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLActivityIndicatorView.h"

@interface SplashViewController : UIViewController {
    YLActivityIndicatorView *_loadingView;
}

@property (strong, nonatomic) YLActivityIndicatorView *loadingView;

@end
