//
//  SplashViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 4/9/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "SplashViewController.h"
#import "SplashView.h"
#import "Constant.h"

#import "UIDeviceHardware.h"

#define DegreesToRadians(x) ((x) * M_PI / 180.0)

@interface SplashViewController ()

@property (strong, nonatomic) SplashView *splashView;


@property (strong, nonatomic) IBOutlet UIImageView *bgView;
@property (strong, nonatomic) IBOutlet UIImageView *logoView;

@end

@implementation SplashViewController

@synthesize loadingView = _loadingView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
    float currentVersion = 7.0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= currentVersion) {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString *platform = [UIDeviceHardware platformString];
    
    if ([platform isEqualToString:@"iPhone 5"] || [platform isEqualToString:@"iPhone 5 (GSM+CDMA)"] ||
        [platform isEqualToString:@"iPhone 5C"] || [platform isEqualToString:@"iPhone 5S"]) {
        UIImage *backGround = [UIImage imageNamed:@"texture.png"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:backGround];
    }
    else {
        if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            UIImage *backGround = [UIImage imageNamed:@"texture.png"];
            self.view.backgroundColor = [UIColor colorWithPatternImage:backGround];
        }
        else {
            UIImage *backGround = [UIImage imageNamed:@"texture.png"];
            self.view.backgroundColor = [UIColor colorWithPatternImage:backGround];
        }
    }
    
    if (IS_IPAD) {
        CGRect frame = _logoView.frame;
        frame.origin.y = 300;
        _logoView.frame = frame;
    }
    
    YLActivityIndicatorView* v1 = [[YLActivityIndicatorView alloc] init];
    CGRect screenFrame = [[UIScreen mainScreen] bounds];
    
    if (IS_IPAD) {
        v1.center = CGPointMake(screenFrame.size.width/2, (screenFrame.size.height/3)*2.5);
    }
    else {
        v1.center = CGPointMake(screenFrame.size.width/2, (screenFrame.size.height/3)*2);
    }
    
    v1.dotCount = 6;
    v1.hidesWhenStopped = YES;
    _loadingView = v1;
    [self.view addSubview:_loadingView];
    [_loadingView startAnimating];
    [v1 release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Autorotation (iOS <= 5.x)
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
                interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    }
    else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}

// Autorotation (iOS >= 6.0)
- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
    }
    else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

// For iOS 7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
