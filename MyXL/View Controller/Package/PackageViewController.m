//
//  PackageViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/11/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PackageViewController.h"
#import "BalanceCell.h"
#import "PPUCell.h"
#import "QuotaCell.h"
#import "QuotaiPadCell.h"
#import "UnlimitedCell.h"
#import "FUPCell.h"
#import "FUPiPadCell.h"
#import "CommonCell.h"
#import "MenuModel.h"
#import "FooterView.h"
#import "GroupingAndSorting.h"
#import "DataManager.h"
#import "SectionHeaderView.h"

#import "BorderCellBackground.h"
#import "AXISnetCellBackground.h"
#import "Constant.h"
#import "SectionFooterView.h"

#import "CheckUsageModel.h"
#import "AXISnetCommon.h"
#import "HumanReadableDataSizeHelper.h"

#import "MyPackageViewController.h"
#import "CheckUsageViewController.h"
#import "SubMenuViewController.h"
#import "SurveyStepOneViewController.h"
#import "NotificationViewController.h"

#import "AppDelegate.h"

#import "ProgressBar.h"

@interface PackageViewController ()

@property (strong, nonatomic) IBOutlet UITableView *theTableView;
@property (strong, nonatomic) MenuModel *selectedMenu;
@property (readwrite) CGFloat checkUsageCellHeight;

@property (nonatomic, retain) NSIndexPath *checkedIndexPath;
@property (readwrite) CGFloat extraHeight;

- (void)recommendedPackage;
- (CGFloat)calculateCellHeight;
- (void)performInfo:(id)sender;

@end

@implementation PackageViewController

@synthesize sortedMenu = _sortedMenu;
@synthesize theSourceMenu = _theSourceMenu;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
    FooterView *footer = [[[FooterView alloc] initWithFrame:CGRectMake(0.0, 0.0, _theTableView.bounds.size.width, 50.0)] autorelease];
    _theTableView.tableFooterView = footer;*/
    
    DataManager *dataManager = [DataManager sharedInstance];
    NSArray *contentTmp = [dataManager.menuData valueForKey:dataManager.parentId];
    
    // Grouping
    NSDictionary *groupingResult = [GroupingAndSorting doGrouping:contentTmp];
    
    // Sorting
    NSDictionary *sortingResult = [GroupingAndSorting doSortingByMenuId:groupingResult];
    
    self.sortedMenu = [sortingResult allKeys];
    self.sortedMenu = [GroupingAndSorting doSortingByGroupId:self.sortedMenu];
    self.theSourceMenu = sortingResult;
    
    _checkUsageCellHeight = 0;
    
    [_theTableView setBackgroundView:nil];
    [_theTableView setBackgroundView:[[[UIView alloc] init] autorelease]];
}

- (void)viewWillAppear:(BOOL)animated {
    [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    
    for (int i=0; i<[self.sortedMenu count]; i++) {
        NSString *groupIdOfMenu = [self.sortedMenu objectAtIndex:i];
        NSArray *objectsForMenu = [self.theSourceMenu objectForKey:groupIdOfMenu];
        for (MenuModel *menuModel in objectsForMenu) {
            menuModel.open = NO;
        }
    }
    self.checkedIndexPath = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.sortedMenu = nil;;
    self.theSourceMenu = nil;
}

- (void)dealloc {
    [_sortedMenu release];
    _sortedMenu = nil;
    [_theSourceMenu release];
    _theSourceMenu = nil;
    [super dealloc];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sortedMenu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section]] count] + 1;
    }
    else {
        return [[_theSourceMenu objectForKey:[_sortedMenu objectAtIndex:section]] count];
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString *CellIdentifierBalance = @"Balance";
    static NSString *CellIdentifierPPU = @"PPU";
    static NSString *CellIdentifierQuota = @"Quota";
    static NSString *CellIdentifierUnlimited = @"Unlimited";
    static NSString *CellIdentifierFUP = @"FUP";
    static NSString *CellIdentifierCommon = @"Common";
    
    DataManager *sharedData = [DataManager sharedInstance];
    CheckUsageModel *checkUsageModel = [sharedData.checkUsageData objectAtIndex:0];
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        //-----//
        // PPU //
        //-----//
        if ([checkUsageModel.packageType isEqualToString:@"0"]) {
            PPUCell *cell = (PPUCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPPU];
            
            if (cell == nil) {
                cell = [[PPUCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierPPU];
                
                cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                
                cell.userInteractionEnabled = NO;
            }
            
            cell.packageNameTitleLabel.text = [[Language get:@"active_package" alter:nil] uppercaseString];
            cell.packageNameValueLabel.text = checkUsageModel.packageName;
            
            cell.packageTypeTitleLabel.text = [[Language get:@"type" alter:nil] uppercaseString];
            NSString *type = convertPackageType(checkUsageModel.packageType);
            cell.packageTypeValueLabel.text = type;
            
            cell.usageTitleLabel.text = [[Language get:@"usage" alter:nil] uppercaseString];
            unsigned long long ullvalue = strtoull([checkUsageModel.usage UTF8String], NULL, 0);
            NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
            NSString *strUsage = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:usageNumber];
            cell.usageValueLabel.text = strUsage;
            
            if ([checkUsageModel.info length] > 0) {
                cell.infoTitleLabel.hidden = NO;
                cell.infoTitleLabel.text = [[Language get:@"info_package" alter:nil] uppercaseString];
                cell.infoValueLabel.hidden = NO;
                cell.infoValueLabel.text = checkUsageModel.info;
                
                /*
                CGSize infoValueSize = calculateExpectedSize(cell.infoValueLabel, checkUsageModel.info);
                
                CGFloat leftPadding = 20.0;
                //CGFloat topPadding = 5.0;
                CGFloat width = 150.0;
                CGFloat height = 20.0;
                CGFloat deduction = 5.0;
                
                CGRect frame = CGRectMake(leftPadding,
                                          cell.infoTitleLabel.frame.origin.y + cell.infoTitleLabel.frame.size.height - deduction,
                                          width*2 - (leftPadding*2),
                                          height);
                UILabel *infoLabel = [[UILabel alloc] initWithFrame:frame];
                infoLabel.text = checkUsageModel.info;
                infoLabel.backgroundColor = [UIColor redColor];
                
                frame = infoLabel.frame;
                frame.size.height = infoValueSize.height;
                infoLabel.frame = frame;
                
                cell.infoValueLabel = infoLabel;
                //[infoLabel release];*/
            }
            else {
                cell.infoTitleLabel.hidden = YES;
                cell.infoValueLabel.hidden = YES;
            }
            
            return cell;
        }
        //-------//
        // Quota //
        //-------//
        else if ([checkUsageModel.packageType isEqualToString:@"1"]) {
            // iPad
            if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
                QuotaiPadCell *cell = (QuotaiPadCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierQuota];
                
                if (cell == nil) {
                    cell = [[QuotaiPadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierQuota];
                    
                    cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    
                    cell.userInteractionEnabled = NO;
                }
                
                cell.packageNameTitleLabel.text = [[Language get:@"active_package" alter:nil] uppercaseString];
                cell.packageNameValueLabel.text = checkUsageModel.packageName;
                
                cell.packageTypeTitleLabel.text = [[Language get:@"type" alter:nil] uppercaseString];
                NSString *type = convertPackageType(checkUsageModel.packageType);
                cell.packageTypeValueLabel.text = type;
                
                cell.usageTitleLabel.text = [[Language get:@"usage" alter:nil] uppercaseString];
                //unsigned long long ullvalue = strtoull([checkUsageModel.usage UTF8String], NULL, 0);
                //NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
                //NSString *strUsage = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:usageNumber];
                //cell.usageValueLabel.text = strUsage;
                cell.usageValueLabel.text = [NSString stringWithFormat:@"%@ (%@)",checkUsageModel.usage,checkUsageModel.usagePercent];
                
                cell.quotaTitleLabel.text = [[Language get:@"quota" alter:nil] uppercaseString];
                cell.quotaValueLabel.text = checkUsageModel.quota;
                
                // -- Usage Bar --
                for (UIView *view in [cell.barView subviews])
                {
                    [view removeFromSuperview];
                }
                
                CGRect parentViewFrame = cell.barView.frame;
                
                //CGFloat tableWidth = 0.0;
                CGFloat leftPadding = 20.0;
                CGFloat height = 15.0;
                
                CGFloat width = (self.view.frame.size.width - (45*2) - (leftPadding*2))/3;
                //tableWidth = self.view.frame.size.width - (45*2);
                parentViewFrame = CGRectMake(0,
                                             0,
                                             width*2,
                                             height);
                
                NSArray *usageFromString = [checkUsageModel.usagePercent componentsSeparatedByString:@"%"];
                NSString *usageString = [usageFromString objectAtIndex:0];
                
                float usage = [usageString floatValue] / 100;
                ProgressBar *progressBar;
                if (usage <= 0.5 ) {
                    progressBar = [[ProgressBar alloc] initWithFrame:parentViewFrame
                                                 andProgressBarColor:ProgressBarGreen
                                                     withPercentView:YES];
                }
                else if (usage <= 0.8) {
                    progressBar = [[ProgressBar alloc] initWithFrame:parentViewFrame
                                                 andProgressBarColor:ProgressBarYellow
                                                     withPercentView:YES];
                }
                else {
                    progressBar = [[ProgressBar alloc] initWithFrame:parentViewFrame
                                                 andProgressBarColor:ProgressBarRed
                                                     withPercentView:YES];
                }
                [progressBar setProgress:usage];
                
                [progressBar addRightText:checkUsageModel.quota];
                
                [cell.barView addSubview:progressBar];
                //
                
                cell.validityTitleLabel.text = [[Language get:@"validity" alter:nil] uppercaseString];
                cell.validityValueLabel.text = checkUsageModel.endDate;
                
                // -- Remaining Bar --
                for (UIView *view in [cell.remainingBarView subviews])
                {
                    [view removeFromSuperview];
                }
                
                //parentViewFrame = cell.remainingBarView.frame;
                
                parentViewFrame = CGRectMake(0,0,width*2,height);
                
                //NSArray *usageFromString = [checkUsageModel.usagePercent componentsSeparatedByString:@"%"];
                //NSString *usageString = checkUsageModel.remainingPercentage;
                //float remaining = (float)checkUsageModel.remainingPercentage;
                
                usage = (float)checkUsageModel.remainingPercentage;
                usage = usage / 100;
                //NSLog(@"usage = %f",usage);
                progressBar = [[ProgressBar alloc] initWithFrame:parentViewFrame
                                             andProgressBarColor:ProgressBarBlack
                                                 withPercentView:NO];
                [progressBar setProgress:usage];
                
                [progressBar addRightText:checkUsageModel.remainingLabel];
                
                [cell.remainingBarView addSubview:progressBar];
                //
                
                if ([checkUsageModel.info length] > 0) {
                    cell.infoTitleLabel.hidden = NO;
                    cell.infoTitleLabel.text = [[Language get:@"info_package" alter:nil] uppercaseString];
                    cell.infoValueLabel.hidden = NO;
                    cell.infoValueLabel.text = checkUsageModel.info;
                }
                else {
                    cell.infoTitleLabel.hidden = YES;
                    cell.infoValueLabel.hidden = YES;
                }
                
                return cell;
            }
            // iPhone
            else {
                QuotaCell *cell = (QuotaCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierQuota];
                
                if (cell == nil) {
                    cell = [[QuotaCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierQuota];
                    
                    cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    
                    cell.userInteractionEnabled = NO;
                }
                
                cell.packageNameTitleLabel.text = [[Language get:@"active_package" alter:nil] uppercaseString];
                cell.packageNameValueLabel.text = checkUsageModel.packageName;
                
                cell.packageTypeTitleLabel.text = [[Language get:@"type" alter:nil] uppercaseString];
                NSString *type = convertPackageType(checkUsageModel.packageType);
                cell.packageTypeValueLabel.text = type;
                
                cell.usageTitleLabel.text = [[Language get:@"usage" alter:nil] uppercaseString];
                //unsigned long long ullvalue = strtoull([checkUsageModel.usage UTF8String], NULL, 0);
                //NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
                //NSString *strUsage = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:usageNumber];
                //cell.usageValueLabel.text = strUsage;
                cell.usageValueLabel.text = [NSString stringWithFormat:@"%@ (%@)",checkUsageModel.usage,checkUsageModel.usagePercent];
                
                cell.quotaTitleLabel.text = [[Language get:@"quota" alter:nil] uppercaseString];
                cell.quotaValueLabel.text = checkUsageModel.quota;
                
                // -- Usage Bar --
                for (UIView *view in [cell.barView subviews])
                {
                    [view removeFromSuperview];
                }
                
                CGRect parentViewFrame = cell.barView.frame;
                
                CGFloat tableWidth = 0.0;
                CGFloat leftPadding = 20.0;
                CGFloat height = 15.0;
                if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
                    tableWidth = self.view.frame.size.width - (45*2);
                    parentViewFrame = CGRectMake(0,
                                                 0,
                                                 tableWidth - (leftPadding*2),
                                                 height);
                }
                else {
                    tableWidth = self.view.frame.size.width - (10*2);
                    parentViewFrame = CGRectMake(0,
                                                 0,
                                                 tableWidth - (leftPadding*2),
                                                 height);
                }
                
                NSArray *usageFromString = [checkUsageModel.usagePercent componentsSeparatedByString:@"%"];
                NSString *usageString = [usageFromString objectAtIndex:0];
                
                float usage = [usageString floatValue] / 100;
                ProgressBar *progressBar;
                if (usage <= 0.5 ) {
                    progressBar = [[ProgressBar alloc] initWithFrame:parentViewFrame
                                                 andProgressBarColor:ProgressBarGreen
                                                     withPercentView:YES];
                }
                else if (usage <= 0.8) {
                    progressBar = [[ProgressBar alloc] initWithFrame:parentViewFrame
                                                 andProgressBarColor:ProgressBarYellow
                                                     withPercentView:YES];
                }
                else {
                    progressBar = [[ProgressBar alloc] initWithFrame:parentViewFrame
                                                 andProgressBarColor:ProgressBarRed
                                                     withPercentView:YES];
                }
                [progressBar setProgress:usage];
                
                [progressBar addRightText:checkUsageModel.quota];
                
                [cell.barView addSubview:progressBar];
                //
                
                cell.validityTitleLabel.text = [[Language get:@"validity" alter:nil] uppercaseString];
                cell.validityValueLabel.text = checkUsageModel.endDate;
                
                // -- Remaining Bar --
                for (UIView *view in [cell.remainingBarView subviews])
                {
                    [view removeFromSuperview];
                }
                
                parentViewFrame = cell.remainingBarView.frame;
                
                //CGFloat tableWidth = 0.0;
                //CGFloat leftPadding = 20.0;
                //CGFloat height = 15.0;
                if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
                    tableWidth = self.view.frame.size.width - (45*2);
                    parentViewFrame = CGRectMake(0,
                                                 0,
                                                 tableWidth - (leftPadding*2),
                                                 height);
                }
                else {
                    tableWidth = self.view.frame.size.width - (10*2);
                    parentViewFrame = CGRectMake(0,
                                                 0,
                                                 tableWidth - (leftPadding*2),
                                                 height);
                }
                
                //NSArray *usageFromString = [checkUsageModel.usagePercent componentsSeparatedByString:@"%"];
                //NSString *usageString = checkUsageModel.remainingPercentage;
                //float remaining = (float)checkUsageModel.remainingPercentage;
                
                usage = (float)checkUsageModel.remainingPercentage;
                usage = usage / 100;
                //NSLog(@"usage = %f",usage);
                progressBar = [[ProgressBar alloc] initWithFrame:parentViewFrame
                                             andProgressBarColor:ProgressBarBlack
                                                 withPercentView:NO];
                [progressBar setProgress:usage];
                
                [progressBar addRightText:checkUsageModel.remainingLabel];
                
                [cell.remainingBarView addSubview:progressBar];
                //
                
                if ([checkUsageModel.info length] > 0) {
                    cell.infoTitleLabel.hidden = NO;
                    cell.infoTitleLabel.text = [[Language get:@"info_package" alter:nil] uppercaseString];
                    cell.infoValueLabel.hidden = NO;
                    cell.infoValueLabel.text = checkUsageModel.info;
                }
                else {
                    cell.infoTitleLabel.hidden = YES;
                    cell.infoValueLabel.hidden = YES;
                }
                
                return cell;
            }
        }
        //-----------//
        // Unlimited //
        //-----------//
        else if ([checkUsageModel.packageType isEqualToString:@"2"]) {
            UnlimitedCell *cell = (UnlimitedCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierUnlimited];
            
            if (cell == nil) {
                cell = [[UnlimitedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierUnlimited];
                
                cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                
                cell.userInteractionEnabled = NO;
            }
            
            cell.packageNameTitleLabel.text = [[Language get:@"active_package" alter:nil] uppercaseString];
            cell.packageNameValueLabel.text = checkUsageModel.packageName;
            
            cell.packageTypeTitleLabel.text = [[Language get:@"type" alter:nil] uppercaseString];
            NSString *type = convertPackageType(checkUsageModel.packageType);
            cell.packageTypeValueLabel.text = type;
            
            cell.usageTitleLabel.text = [[Language get:@"usage" alter:nil] uppercaseString];
            unsigned long long ullvalue = strtoull([checkUsageModel.usage UTF8String], NULL, 0);
            NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
            NSString *strUsage = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:usageNumber];
            cell.usageValueLabel.text = strUsage;
            
            cell.validityTitleLabel.text = [[Language get:@"validity" alter:nil] uppercaseString];
            cell.validityValueLabel.text = checkUsageModel.endDate;
            
            if ([checkUsageModel.info length] > 0) {
                cell.infoTitleLabel.hidden = NO;
                cell.infoTitleLabel.text = [[Language get:@"info_package" alter:nil] uppercaseString];
                cell.infoValueLabel.hidden = NO;
                cell.infoValueLabel.text = checkUsageModel.info;
            }
            else {
                cell.infoTitleLabel.hidden = YES;
                cell.infoValueLabel.hidden = YES;
            }
            
            return cell;
        }
        //-----//
        // FUP //
        //-----//
        else {
            // iPad
            if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
                FUPiPadCell *cell = (FUPiPadCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierFUP];
                
                if (cell == nil) {
                    cell = [[FUPiPadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierFUP];
                    
                    cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    
                    cell.userInteractionEnabled = NO;
                }
                
                cell.packageNameTitleLabel.text = [[Language get:@"active_package" alter:nil] uppercaseString];
                cell.packageNameValueLabel.text = checkUsageModel.packageName;
                
                cell.packageTypeTitleLabel.text = [[Language get:@"type" alter:nil] uppercaseString];
                NSString *type = convertPackageType(checkUsageModel.packageType);
                cell.packageTypeValueLabel.text = type;
                
                cell.usageTitleLabel.text = [[Language get:@"usage" alter:nil] uppercaseString];
                unsigned long long ullvalue = strtoull([checkUsageModel.usage UTF8String], NULL, 0);
                NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
                NSString *strUsage = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:usageNumber];
                cell.usageValueLabel.text = strUsage;
                //cell.usageValueLabel.text = [NSString stringWithFormat:@"%@",checkUsageModel.usage];
                
                // -- Usage Bar --
                for (UIView *view in [cell.barView subviews])
                {
                    [view removeFromSuperview];
                }
                
                CGRect parentViewFrame = cell.barView.frame;
                
                //CGFloat tableWidth = 0.0;
                CGFloat leftPadding = 20.0;
                CGFloat height = 15.0;
                //tableWidth = self.view.frame.size.width - (45*2);
                
                CGFloat width = (self.view.frame.size.width - (45*2) - (leftPadding*2))/3;
                parentViewFrame = CGRectMake(0,0,width*2,height);
                
                float usageValue = [checkUsageModel.usage floatValue];
                float quotaValue = [checkUsageModel.quota floatValue];
                float progress = (usageValue/quotaValue);
                
                ProgressBar *progressBar = [[ProgressBar alloc] initWithFrame:parentViewFrame
                                                          andProgressBarColor:ProgressBarGreen
                                                              withPercentView:YES];
                [progressBar setProgress:progress];
                
                // Threshold
                //
                NSMutableArray *thresholdTmp = [[NSMutableArray alloc] init];
                if ([checkUsageModel.t1 length] > 0) {
                    [thresholdTmp addObject:checkUsageModel.t1];
                }
                if ([checkUsageModel.t2 length] > 0) {
                    [thresholdTmp addObject:checkUsageModel.t2];
                }
                if ([checkUsageModel.t3 length] > 0) {
                    [thresholdTmp addObject:checkUsageModel.t3];
                }
                if ([checkUsageModel.t4 length] > 0) {
                    [thresholdTmp addObject:checkUsageModel.t4];
                }
                if ([checkUsageModel.t5 length] > 0) {
                    [thresholdTmp addObject:checkUsageModel.t5];
                }
                [thresholdTmp addObject:@"~"];
                
                //
                for (int i=0; i<[thresholdTmp count]; i++) {
                    if ([[thresholdTmp objectAtIndex:i] isEqualToString:@"~"]) {
                        [progressBar addVerticalLine:1.0 withText:@"~"];
                    }
                    else {
                        NSString *thresholdString = [thresholdTmp objectAtIndex:i];
                        float thresholdValue = [thresholdString floatValue];
                        thresholdValue = (thresholdValue/quotaValue);
                        
                        unsigned long long ullvalue = strtoull([thresholdString UTF8String], NULL, 0);
                        NSNumber *thresholdNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
                        NSString *formattedString = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:thresholdNumber];
                        
                        [progressBar addVerticalLine:thresholdValue withText:formattedString];
                    }
                }
                
                [cell.barView addSubview:progressBar];
                
                [thresholdTmp release];
                thresholdTmp = nil;
                //
                
                cell.validityTitleLabel.text = [[Language get:@"validity" alter:nil] uppercaseString];
                cell.validityValueLabel.text = checkUsageModel.endDate;
                
                // -- Remaining Bar --
                for (UIView *view in [cell.remainingBarView subviews])
                {
                    [view removeFromSuperview];
                }
                
                //parentViewFrame = cell.remainingBarView.frame;
                parentViewFrame = CGRectMake(0,0,width*2,height);
                
                float usage = (float)checkUsageModel.remainingPercentage;
                usage = usage / 100;
                progressBar = [[ProgressBar alloc] initWithFrame:parentViewFrame
                                             andProgressBarColor:ProgressBarBlack
                                                 withPercentView:NO];
                [progressBar setProgress:usage];
                
                [progressBar addRightText:checkUsageModel.remainingLabel];
                
                [cell.remainingBarView addSubview:progressBar];
                //
                
                if ([checkUsageModel.info length] > 0) {
                    cell.infoTitleLabel.hidden = NO;
                    cell.infoTitleLabel.text = [[Language get:@"info_package" alter:nil] uppercaseString];
                    cell.infoValueLabel.hidden = NO;
                    cell.infoValueLabel.text = checkUsageModel.info;
                }
                else {
                    cell.infoTitleLabel.hidden = YES;
                    cell.infoValueLabel.hidden = YES;
                }
                
                return cell;
            }
            // iPhone
            else {
                FUPCell *cell = (FUPCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierFUP];
                
                if (cell == nil) {
                    cell = [[FUPCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierFUP];
                    
                    cell.backgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    cell.selectedBackgroundView = [[[BorderCellBackground alloc] init] autorelease];
                    
                    cell.userInteractionEnabled = NO;
                }
                
                cell.packageNameTitleLabel.text = [[Language get:@"active_package" alter:nil] uppercaseString];
                cell.packageNameValueLabel.text = checkUsageModel.packageName;
                
                cell.packageTypeTitleLabel.text = [[Language get:@"type" alter:nil] uppercaseString];
                NSString *type = convertPackageType(checkUsageModel.packageType);
                cell.packageTypeValueLabel.text = type;
                
                cell.usageTitleLabel.text = [[Language get:@"usage" alter:nil] uppercaseString];
                unsigned long long ullvalue = strtoull([checkUsageModel.usage UTF8String], NULL, 0);
                NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
                NSString *strUsage = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:usageNumber];
                cell.usageValueLabel.text = strUsage;
                //cell.usageValueLabel.text = [NSString stringWithFormat:@"%@",checkUsageModel.usage];
                
                // -- Usage Bar --
                for (UIView *view in [cell.barView subviews])
                {
                    [view removeFromSuperview];
                }
                
                CGRect parentViewFrame = cell.barView.frame;
                
                CGFloat tableWidth = 0.0;
                CGFloat leftPadding = 20.0;
                CGFloat height = 15.0;
                if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
                    tableWidth = self.view.frame.size.width - (45*2);
                    parentViewFrame = CGRectMake(0,
                                                 0,
                                                 tableWidth - (leftPadding*2),
                                                 height);
                }
                else {
                    tableWidth = self.view.frame.size.width - (10*2);
                    parentViewFrame = CGRectMake(0,
                                                 0,
                                                 tableWidth - (leftPadding*2),
                                                 height);
                }
                
                //NSArray *usageFromString = [checkUsageModel.usagePercent componentsSeparatedByString:@"%"];
                //NSString *usageString = [usageFromString objectAtIndex:0];
                
                //float usage = [usageString floatValue] / 100;
                float usageValue = [checkUsageModel.usage floatValue];
                float quotaValue = [checkUsageModel.quota floatValue];
                float progress = (usageValue/quotaValue);
                
                ProgressBar *progressBar = [[ProgressBar alloc] initWithFrame:parentViewFrame
                                                          andProgressBarColor:ProgressBarGreen
                                                              withPercentView:YES];
                [progressBar setProgress:progress];
                
                // Threshold
                //
                NSMutableArray *thresholdTmp = [[NSMutableArray alloc] init];
                if ([checkUsageModel.t1 length] > 0) {
                    [thresholdTmp addObject:checkUsageModel.t1];
                }
                if ([checkUsageModel.t2 length] > 0) {
                    [thresholdTmp addObject:checkUsageModel.t2];
                }
                if ([checkUsageModel.t3 length] > 0) {
                    [thresholdTmp addObject:checkUsageModel.t3];
                }
                if ([checkUsageModel.t4 length] > 0) {
                    [thresholdTmp addObject:checkUsageModel.t4];
                }
                if ([checkUsageModel.t5 length] > 0) {
                    [thresholdTmp addObject:checkUsageModel.t5];
                }
                [thresholdTmp addObject:@"~"];
                
                //
                for (int i=0; i<[thresholdTmp count]; i++) {
                    if ([[thresholdTmp objectAtIndex:i] isEqualToString:@"~"]) {
                        [progressBar addVerticalLine:1.0 withText:@"~"];
                    }
                    else {
                        NSString *thresholdString = [thresholdTmp objectAtIndex:i];
                        float thresholdValue = [thresholdString floatValue];
                        thresholdValue = (thresholdValue/quotaValue);
                        
                        unsigned long long ullvalue = strtoull([thresholdString UTF8String], NULL, 0);
                        NSNumber *thresholdNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
                        NSString *formattedString = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:thresholdNumber];
                        
                        [progressBar addVerticalLine:thresholdValue withText:formattedString];
                    }
                }
                
                [cell.barView addSubview:progressBar];
                
                [thresholdTmp release];
                thresholdTmp = nil;
                //
                
                cell.validityTitleLabel.text = [[Language get:@"validity" alter:nil] uppercaseString];
                cell.validityValueLabel.text = checkUsageModel.endDate;
                
                // -- Remaining Bar --
                for (UIView *view in [cell.remainingBarView subviews])
                {
                    [view removeFromSuperview];
                }
                
                parentViewFrame = cell.remainingBarView.frame;
                
                if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
                    tableWidth = self.view.frame.size.width - (45*2);
                    parentViewFrame = CGRectMake(0,
                                                 0,
                                                 tableWidth - (leftPadding*2),
                                                 height);
                }
                else {
                    tableWidth = self.view.frame.size.width - (10*2);
                    parentViewFrame = CGRectMake(0,
                                                 0,
                                                 tableWidth - (leftPadding*2),
                                                 height);
                }
                
                float usage = (float)checkUsageModel.remainingPercentage;
                usage = usage / 100;
                progressBar = [[ProgressBar alloc] initWithFrame:parentViewFrame
                                             andProgressBarColor:ProgressBarBlack
                                                 withPercentView:NO];
                [progressBar setProgress:usage];
                
                [progressBar addRightText:checkUsageModel.remainingLabel];
                
                [cell.remainingBarView addSubview:progressBar];
                //
                
                if ([checkUsageModel.info length] > 0) {
                    cell.infoTitleLabel.hidden = NO;
                    cell.infoTitleLabel.text = [[Language get:@"info_package" alter:nil] uppercaseString];
                    cell.infoValueLabel.hidden = NO;
                    cell.infoValueLabel.text = checkUsageModel.info;
                }
                else {
                    cell.infoTitleLabel.hidden = YES;
                    cell.infoValueLabel.hidden = YES;
                }
                
                return cell;
            }
        }
    }
    else {
        
        //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        CommonCell *cell = (CommonCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifierCommon];
        
        if (cell == nil) {
            cell = [[CommonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierCommon];
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            [cell.infoButton addTarget:self
                                action:@selector(performInfo:)
                      forControlEvents:UIControlEventTouchUpInside];
        }
        
        cell.backgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
        cell.selectedBackgroundView = [[[AXISnetCellBackground alloc] init] autorelease];
        ((AXISnetCellBackground *)cell.selectedBackgroundView).selected = YES;
        
        NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
        NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
        
        MenuModel *menuModel;
        if (indexPath.section == 0) {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
        }
        else {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row];
        }
        
        cell.iconLabel.text = icon(menuModel.menuId);
        if ([cell.iconLabel.text length] <= 0) {
            cell.iconLabel.text = icon(@"default_icon");
        }
        cell.titleLabel.text = [menuModel.menuName uppercaseString];
        cell.titleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        
        if ([menuModel.desc length] > 0) {
            cell.arrowLabel.hidden = YES;
            cell.infoButton.hidden = NO;
        }
        else {
            cell.arrowLabel.hidden = NO;
            cell.infoButton.hidden = YES;
        }
        
        // Accordion
        if (menuModel.open) {
            cell.descLabel.hidden = NO;
            
            CGRect newFrame = cell.descLabel.frame;
            newFrame.size.height = _extraHeight;
            cell.descLabel.frame = newFrame;
        }
        else {
            cell.descLabel.hidden = YES;
        }
        cell.descLabel.text = menuModel.desc;
        
        return cell;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *groupIdOfMenu = [self.sortedMenu objectAtIndex:section];
    NSArray *objectsForMenu = [self.theSourceMenu objectForKey:groupIdOfMenu];
    MenuModel *menuModel = [objectsForMenu objectAtIndex:0];
    return menuModel.groupName;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 24.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionFooterView *footer = nil;
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:YES] autorelease];
    }
    else {
        footer = [[[SectionFooterView alloc] initWithFrame:self.view.frame lastSection:NO] autorelease];
    }
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        //return 90.0;
        if (_checkUsageCellHeight == 0) {
            return [self calculateCellHeight];
        }
        else {
            return _checkUsageCellHeight;
        }
    }
    else {
        //return kDefaultCellHeight;
        
        NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
        NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
        
        MenuModel *menuModel;
        if (indexPath.section == 0) {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
        }
        else {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row];
        }
        
        CGFloat labelHeightDefault = 28.0;
        
        NSString *text = [menuModel.menuName uppercaseString];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 240.0, 28.0)];
        label.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        CGSize size = calculateExpectedSize(label, text);
        
        CGFloat extraHeightForTitle = 0.0;
        if (size.height > labelHeightDefault) {
            extraHeightForTitle = size.height - labelHeightDefault;
        }
        
        if (menuModel.open) {
            return kDefaultCellHeight + _extraHeight + extraHeightForTitle;
        }
        else {
            return kDefaultCellHeight + extraHeightForTitle;
        }
    }
}

- (CGFloat)calculateCellHeight {
    DataManager *sharedData = [DataManager sharedInstance];
    CheckUsageModel *checkUsageModel = [sharedData.checkUsageData objectAtIndex:0];
    // PPU
    if ([checkUsageModel.packageType isEqualToString:@"0"] ||
        [checkUsageModel.packageType isEqualToString:@"2"]) {
        
        CGFloat leftPadding = 20.0;
        CGFloat topPadding = 5.0;
        CGFloat width = (self.view.frame.size.width - (leftPadding*4)) / 2; //150
        CGFloat height = 20.0;
        CGFloat deduction = 5.0;
        
        CGRect frame = CGRectMake(leftPadding,
                                  topPadding,
                                  width,
                                  height);
        
        UILabel *leftTitleLabel = [[UILabel alloc] initWithFrame:frame];
//        leftTitleLabel.textAlignment = UITextAlignmentLeft;
//        leftTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
//        leftTitleLabel.numberOfLines = 0;
//        leftTitleLabel.lineBreakMode = UILineBreakModeWordWrap;
        
//        NSString *packageNameTitle = [[Language get:@"active_package" alter:nil] uppercaseString];
//        CGSize packageNameTitleSize = calculateExpectedSize(leftTitleLabel,packageNameTitle);
        
        frame = CGRectMake(leftPadding,
                           leftTitleLabel.frame.origin.y + leftTitleLabel.frame.size.height - deduction,
                           width,
                           height);
        UILabel *leftValueLabel = [[UILabel alloc] initWithFrame:frame];
        leftValueLabel.textAlignment = UITextAlignmentLeft;
        leftValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
        leftValueLabel.numberOfLines = 0;
        leftValueLabel.lineBreakMode = UILineBreakModeWordWrap;
        NSString *packageNameValue = checkUsageModel.packageName;
        CGSize packageNameValueSize = calculateExpectedSize(leftValueLabel,packageNameValue);
        frame = leftValueLabel.frame;
        frame.size.height = packageNameValueSize.height;
        leftValueLabel.frame = frame;
        
        frame = CGRectMake(leftPadding,
                           leftValueLabel.frame.origin.y + leftValueLabel.frame.size.height + topPadding,
                           width,
                           height);
        leftTitleLabel.frame = frame;
//        NSString *usageTitle = [[Language get:@"usage" alter:nil] uppercaseString];
//        CGSize usageTitleSize = [usageTitle sizeWithFont:[UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize]
//                                                   constrainedToSize:CGSizeMake(width, 1000.0f)];
        
        
        frame = CGRectMake(leftPadding,
                           leftTitleLabel.frame.origin.y + leftTitleLabel.frame.size.height - deduction,
                           width,
                           height);
        leftValueLabel.frame = frame;
        
        unsigned long long ullvalue = strtoull([checkUsageModel.usage UTF8String], NULL, 0);
        NSNumber *usageNumber = [NSNumber numberWithUnsignedLongLong:ullvalue];
        NSString *strUsage = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:usageNumber];
        NSString *usageValue = strUsage;
        CGSize usageValueSize = calculateExpectedSize(leftValueLabel, usageValue);
        frame = leftValueLabel.frame;
        frame.size.height = usageValueSize.height;
        leftValueLabel.frame = frame;
        
        if ([checkUsageModel.info length] > 0) {
            frame = CGRectMake(leftPadding,
                               leftValueLabel.frame.origin.y + leftValueLabel.frame.size.height + topPadding,
                               width,
                               height);
            leftTitleLabel.frame = frame;
            
            frame = CGRectMake(leftPadding,
                               leftTitleLabel.frame.origin.y + leftTitleLabel.frame.size.height - deduction,
                               (width*2) - (leftPadding*2),
                               height);
            leftValueLabel.frame = frame;
            NSString *infoValue = checkUsageModel.info;
            CGSize infoValueSize = calculateExpectedSize(leftValueLabel, infoValue);
            frame = leftValueLabel.frame;
            frame.size.height = infoValueSize.height;
            leftValueLabel.frame = frame;
        }
        
        _checkUsageCellHeight = leftValueLabel.frame.origin.y + leftValueLabel.frame.size.height + (topPadding*2);
        return _checkUsageCellHeight;
    }
    else if ([checkUsageModel.packageType isEqualToString:@"1"]) {
        if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            return 131;
        }
        else {
            return 180;
        }
    }
    else if ([checkUsageModel.packageType isEqualToString:@"3"]) {
        if ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
            return 131;
        }
        else {
            return 190; //180
        }
    }
    else {
        return 90; // 70
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:indexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    MenuModel *menuModel = nil;
    
    if (indexPath.section == 0) {
        if (indexPath.row != 0) {
            menuModel = [objectsForMenu objectAtIndex:indexPath.row - 1];
        }
    }
    else {
        menuModel = [objectsForMenu objectAtIndex:indexPath.row];
    }
    
    _selectedMenu = menuModel;
    
    if ([menuModel.href isEqualToString:@"#my_package"]) {
        MyPackageViewController *myPackageViewController = [[MyPackageViewController alloc] initWithNibName:@"MyPackageViewController" bundle:nil];
        
        //myPackageViewController.headerTitle = menuModel.menuName;
        
        myPackageViewController.headerTitleMaster = _selectedMenu.menuName;
        myPackageViewController.headerIconMaster = icon(_selectedMenu.menuId);
        
        [self.navigationController pushViewController:myPackageViewController animated:YES];
        myPackageViewController = nil;
        [myPackageViewController release];
    }
    
    else if ([menuModel.href isEqualToString:@"#check_usage"]) {
        DataManager *sharedData = [DataManager sharedInstance];
        
        CheckUsageViewController *checkUsageViewController = [[CheckUsageViewController alloc] initWithNibName:@"CheckUsageViewController" bundle:nil];
        
        checkUsageViewController.checkUsageModel = [sharedData.checkUsageData objectAtIndex:0];
        checkUsageViewController.headerIconMaster = icon(_selectedMenu.menuId);
        checkUsageViewController.headerTitleMaster = _selectedMenu.menuName;
        
        [self.navigationController pushViewController:checkUsageViewController animated:YES];
        checkUsageViewController = nil;
        [checkUsageViewController release];
    }
    
    else if ([menuModel.href isEqualToString:@"#recommended_package"]) {
        [self recommendedPackage];
    }
    
    else if ([menuModel.href isEqualToString:@"#survey"]) {
        SurveyStepOneViewController *surveyStepOneViewController = [[SurveyStepOneViewController alloc] initWithNibName:@"SurveyStepOneViewController" bundle:nil];
        surveyStepOneViewController.menuModel = _selectedMenu;
        [self.navigationController pushViewController:surveyStepOneViewController animated:YES];
        surveyStepOneViewController = nil;
        [surveyStepOneViewController release];
    }
    
    else {
        SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
        
        //        NSString *groupIdOfMenu = [self.sortedMenu objectAtIndex:indexPath.section];
        //        NSArray *objectsForMenu = [self.theSourceMenu objectForKey:groupIdOfMenu];
        //        MenuModel *menuModel = [objectsForMenu objectAtIndex:indexPath.row];
        //        NSLog(@"menuModel.parentId = %@",menuModel.menuId);
        
        subMenuVC.parentId = menuModel.menuId;
        subMenuVC.groupDesc = menuModel.desc;
        //(@"subMenuVC.groupDesc = %@",subMenuVC.groupDesc);
        
        subMenuVC.headerIconMaster = icon(_selectedMenu.menuId);
        if ([subMenuVC.headerIconMaster length] <= 0) {
            subMenuVC.headerIconMaster = icon(@"default_icon");
        }
        
        subMenuVC.headerTitleMaster = _selectedMenu.menuName;
        
        [self.navigationController pushViewController:subMenuVC animated:YES];
        subMenuVC = nil;
        [subMenuVC release];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    /*
     SectionHeaderView *headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40.0)] autorelease];
     return headerView;*/
    
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    
    MenuModel *menuModel = [objectsForMenu objectAtIndex:0];
    
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    SectionHeaderView *headerView;
    if (section == 0) {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, firstSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:icon([NSString stringWithFormat:@"gid_%@",menuModel.groupId])
                                                isFirstSection:YES] autorelease];
    }
    else {
        headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, otherSectionHeaderHeight)
                                                     withLabel:sectionTitle
                                                      withIcon:icon([NSString stringWithFormat:@"gid_%@",menuModel.groupId])
                                                isFirstSection:NO] autorelease];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return firstSectionHeaderHeight;
    }
    else {
        return otherSectionHeaderHeight;
    }
}

#pragma mark - Selector

- (void)recommendedPackage {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = RECOMMENDED_PACKAGE_REQ;
    [request recommendedPackage];
}

- (void)performInfo:(id)sender {
    //--
    UIButton *button = (UIButton *)sender;
    CommonCell *cell = (CommonCell *)[[button superview] superview];
    int section = [_theTableView indexPathForCell:cell].section;
    //NSLog(@"section = %i",section);
    int row = [_theTableView indexPathForCell:cell].row;
    //NSLog(@"row = %i",row);
    
    //CommonCell *cell = (CommonCell*)[_theTableView cellForRowAtIndexPath:indexPath];
    
    //CGRect testFrame = cell.descLabel.frame;
    //NSLog(@"frame = %f",testFrame.size.height);
    
    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
    NSString *groupIdOfMenu = [_sortedMenu objectAtIndex:selectedIndexPath.section];
    NSArray *objectsForMenu = [_theSourceMenu objectForKey:groupIdOfMenu];
    //--
    
    MenuModel *menuModel;
    if (selectedIndexPath.section == 0) {
        menuModel = [objectsForMenu objectAtIndex:selectedIndexPath.row - 1];
    }
    else {
        menuModel = [objectsForMenu objectAtIndex:selectedIndexPath.row];
    }
    //NSLog(@"menuModel.menuName = %@",menuModel.menuName);
    
    // Uncheck the previous checked row
    if (self.checkedIndexPath)
    {
        NSString *groupIdOfMenuLast = [_sortedMenu objectAtIndex:self.checkedIndexPath.section];
        NSArray *objectsForMenuLast = [_theSourceMenu objectForKey:groupIdOfMenuLast];
        
        MenuModel *uncheckSection;
        if (self.checkedIndexPath.section == 0) {
            uncheckSection = [objectsForMenuLast objectAtIndex:self.checkedIndexPath.row - 1];
        }
        else {
            uncheckSection = [objectsForMenuLast objectAtIndex:self.checkedIndexPath.row];
        }
        uncheckSection.open = NO;
    }
    
    if ([self.checkedIndexPath isEqual:selectedIndexPath])
    {
        self.checkedIndexPath = nil;
    }
    else
    {
        menuModel.open = YES;
        self.checkedIndexPath = selectedIndexPath;
        
        CommonCell *selectedCell = (CommonCell*)[_theTableView cellForRowAtIndexPath:selectedIndexPath];
        
        //NSLog(@"menuModel.desc = %@",menuModel.desc);
        //(@"label width = %f",selectedCell.descLabel.frame.size.width);
        
        CGSize size = calculateExpectedSize(selectedCell.descLabel, menuModel.desc);
        
        _extraHeight = size.height;
        //NSLog(@"_extraHeight = %f",_extraHeight);
    }
    
    [_theTableView reloadData];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == RECOMMENDED_PACKAGE_REQ) {
            
            DataManager *dataManager = [DataManager sharedInstance];
            
            for (int i=0; i<[dataManager.recommendedData count]; i++) {
                MenuModel *recommendedPackageModel = [dataManager.recommendedData objectAtIndex:i];
                recommendedPackageModel.groupId        = _selectedMenu.menuId;
                recommendedPackageModel.groupName      = _selectedMenu.menuName;
            }
            
            SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
            subMenuVC.parentId = _selectedMenu.menuId;
            subMenuVC.groupDesc = _selectedMenu.desc;
            //NSLog(@"subMenuVC.groupDesc = %@",subMenuVC.groupDesc);
            subMenuVC.recommendedPackageModel = _selectedMenu;
            
//            subMenuVC.headerIconMaster = icon(_selectedMenu.menuId);
//            subMenuVC.headerTitleMaster = _selectedMenu.menuName;
            
            [self.navigationController pushViewController:subMenuVC animated:YES];
            subMenuVC = nil;
            [subMenuVC release];
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            [notificationViewController createBarButtonItem:BACK];
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
        
        if (type == SIGN_OUT_REQ) {
            // TODO : Hygiene
            resetAllData();
            
            AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            appDelegate.loggedIn = NO;
            
            // TODO : V1.9
            // set update device token to be YES again.
            DataManager *sharedData = [DataManager sharedInstance];
            sharedData.shouldUpdateDeviceToken = YES;
            
            // TODO : Hygiene
            // Reset this autologin variable on the login view instead
//            appDelegate.isAutoLogin = NO;
            [appDelegate showLoginView];
        }
    }
    else {
        
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
