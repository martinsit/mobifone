//
//  PackageViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/11/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AXISnetRequest.h"

@interface PackageViewController : BasicViewController <AXISnetRequestDelegate> {
    NSArray *_sortedMenu;
    NSDictionary *_theSourceMenu;
}

@property (nonatomic, retain) NSArray *sortedMenu;
@property (nonatomic, retain) NSDictionary *theSourceMenu;

@end
