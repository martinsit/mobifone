//
//  QuotaCalculatorViewController.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 6/19/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "QuotaCalculatorViewController.h"
#import "SectionHeaderView.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "DataManager.h"
#import "InternetUsageModel.h"

#import "SVSegmentedControl.h"

#import "QuotaCalculatorBar.h"

#import "SubMenuViewController.h"

#import "HumanReadableDataSizeHelper.h"

@interface QuotaCalculatorViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, retain) IBOutlet UIView *barContainer;
@property (nonatomic, retain) QuotaCalculatorBar *barViewObject;

@property (nonatomic, retain) IBOutlet UILabel *dataQuotaSimulatorLabel;
@property (nonatomic, retain) IBOutlet UILabel *dailyAverageLabel;

@property (strong, nonatomic) IBOutlet UIView *sliderContainerView;

@property (strong, nonatomic) UIButton *submitButton;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (strong, nonatomic) NSString *planType;

@property (strong, nonatomic) IBOutlet UILabel *totalLabel;

@property (readwrite) double planQuota;

- (IBAction)indexChanged:(UISegmentedControl *)sender;

@end

@implementation QuotaCalculatorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.planType = @"1";
}

- (void)viewWillAppear:(BOOL)animated {
    [self checkAndRemoveSubviews];
    [self setupUILayout];
    [self setupLabelValue];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Selector

- (void)checkAndRemoveSubviews {
    for (UIView *subView in [_contentView subviews])
    {
        if ([subView isKindOfClass:[SectionHeaderView class]])
            [subView removeFromSuperview];
        
        if ([subView isKindOfClass:[UIButton class]])
            [subView removeFromSuperview];
    }
    
    for (UIView *subView in [_sliderContainerView subviews])
    {
        if ([subView isKindOfClass:[QuotaSliderView class]])
            [subView removeFromSuperview];
    }
    
    for (UIView *subView in [_barContainer subviews])
    {
        if ([subView isKindOfClass:[QuotaCalculatorBar class]])
            [subView removeFromSuperview];
    }
}

-(void)setupLayout
{
    
}

- (void)setupUILayout {
    DataManager *sharedData = [DataManager sharedInstance];
    
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1;
    if (IS_IPAD) {
        headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_HEIGHT,kDefaultCellHeight*1.5)
                                                withLabelForXL:sharedData.quotaCalculatorData.title
                                                isFirstSection:YES];
    }
    else {
        headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,kDefaultCellHeight*1.5)
                                                withLabelForXL:sharedData.quotaCalculatorData.title
                                                isFirstSection:YES];
    }
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2;
    if (IS_IPAD) {
        headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, SCREEN_HEIGHT,kDefaultCellHeight)
                                                withLabelForXL:sharedData.quotaCalculatorData.subtitle
                                                isFirstSection:NO];
    }
    else {
        headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, SCREEN_WIDTH,kDefaultCellHeight)
                                                withLabelForXL:sharedData.quotaCalculatorData.subtitle
                                                isFirstSection:NO];
    }
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    _dataQuotaSimulatorLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _dataQuotaSimulatorLabel.textColor = kDefaultTitleFontGrayColor;
    
    _dailyAverageLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _dailyAverageLabel.textColor = kDefaultTitleFontGrayColor;
    
    _totalLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _totalLabel.textColor = kDefaultTitleFontGrayColor;
    
    //-------------------//
    // Segmented Control //
    //-------------------//
    /*
	SVSegmentedControl *redSC = [[SVSegmentedControl alloc] initWithSectionTitles:[NSArray arrayWithObjects:@"Daily", @"Weekly", @"Monthly", nil]];
    [redSC addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
	redSC.crossFadeLabelsOnDrag = YES;
	redSC.thumb.tintColor = kDefaultOrangeColor;
    [redSC setSelectedSegmentIndex:1 animated:NO];
    
    CGRect frame = redSC.frame;
    frame.origin.y = _dailyAverageLabel.frame.origin.y + _dailyAverageLabel.frame.size.height;
    redSC.frame = frame;
	
	[_fieldView addSubview:redSC];
	
	redSC.center = CGPointMake(160, redSC.frame.origin.y);*/
    
    //-------------//
    // Slider View //
    //-------------//
    
    CGFloat startY = 0.0;
    CGFloat sliderHeight = 100.0;
    NSMutableArray *quotaBarDataTmp = [[NSMutableArray alloc] init];
    for (int i = 0; i < [sharedData.quotaCalculatorData.listUsage count]; i++) {
        InternetUsageModel *model = [sharedData.quotaCalculatorData.listUsage objectAtIndex:i];
        
        [quotaBarDataTmp addObject:model];
        
        CGRect sliderFrame = CGRectMake(0, startY, _sliderContainerView.frame.size.width, sliderHeight);
        // TODO : V1.9
        // Adding isQuotaCalculatorView to the init method
        QuotaSliderView *sliderView = [[QuotaSliderView alloc] initWithFrame:sliderFrame withData:model isQuotaCalculatorView:YES];
        sliderView.delegate = self;
        [_sliderContainerView addSubview:sliderView];
        startY += sliderHeight;
    }
    CGRect sliderContainerFrame = _sliderContainerView.frame;
    sliderContainerFrame.size.height = sliderHeight * [sharedData.quotaCalculatorData.listUsage count];
    _sliderContainerView.frame = sliderContainerFrame;
    
    //---------------------------//
    // Quota Calculator Bar View //
    //---------------------------//
    NSArray *quotaBarData = [NSArray arrayWithArray:quotaBarDataTmp];
    CGRect barFrame = CGRectMake(0, 0, _barContainer.frame.size.width, _barContainer.frame.size.height);
    QuotaCalculatorBar *barView = [[QuotaCalculatorBar alloc] initWithFrame:barFrame withData:quotaBarData];
    _barViewObject = barView;
    [_barContainer addSubview:_barViewObject];
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    newFrame.size.height = _sliderContainerView.frame.origin.y + _sliderContainerView.frame.size.height;
    _fieldView.frame = newFrame;
    
    //TODO : V1.9
    UILabel *lblPackageRecommendation = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(_theScrollView.frame)/2 - 100, CGRectGetMaxY(_fieldView.frame) + kDefaultComponentPadding, 200, 21)];
    lblPackageRecommendation.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    lblPackageRecommendation.textColor = kDefaultTitleFontGrayColor;
    lblPackageRecommendation.text = [Language get:@"package_recommendation" alter:nil];
    lblPackageRecommendation.textAlignment = NSTextAlignmentCenter;
    
    [_contentView addSubview:lblPackageRecommendation];
    
    //--------------//
    // Setup Button //
    //--------------//
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    NSString *buttonText = sharedData.quotaCalculatorData.submitText;
    
    // TODO : V1.9
    // Bugs fixing
    UIButton *submitBtn = createButtonWithFrame(CGRectMake(CGRectGetWidth(_theScrollView.frame)/2 - 50,
                                                           CGRectGetMaxY(lblPackageRecommendation.frame) + kDefaultComponentPadding,
                                                           100,
                                                           kDefaultButtonHeight),
                                                [buttonText uppercaseString],
                                                buttonFont,
                                                kDefaultWhiteColor,
                                                kDefaultAquaMarineColor,
                                                kDefaultBlueColor);
    
//    UIButton *submitBtn = createButtonWithFrame(CGRectMake(_dailyAverageLabel.frame.origin.x,
//                                                           _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
//                                                           100,
//                                                           kDefaultButtonHeight),
//                                                [buttonText uppercaseString],
//                                                buttonFont,
//                                                kDefaultWhiteColor,
//                                                kDefaultAquaMarineColor,
//                                                kDefaultBlueColor);
    [submitBtn addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    _submitButton = submitBtn;
    [_contentView addSubview:_submitButton];
    
    //--------------//
    // Content View //
    //--------------//
    newFrame = _contentView.frame;
    newFrame.size.height = _submitButton.frame.origin.y + _submitButton.frame.size.height + kDefaultComponentPadding;
    _contentView.frame = newFrame;
    _contentView.backgroundColor = kDefaultWhiteColor;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 200.0);
    
    [headerView1 release];
    [headerView2 release];
    
}

- (void)setupLabelValue {
    DataManager *sharedData = [DataManager sharedInstance];
    
    _dataQuotaSimulatorLabel.text = sharedData.quotaCalculatorData.calculateText;
    _dailyAverageLabel.text = sharedData.quotaCalculatorData.dailyUsage;
    
    //-------------------------------------//
    // Set the label of UISegmentedControl //
    //-------------------------------------//
    [_segmentedControl setTitle:sharedData.quotaCalculatorData.daily forSegmentAtIndex:0];
    [_segmentedControl setTitle:sharedData.quotaCalculatorData.weekly forSegmentAtIndex:1];
    [_segmentedControl setTitle:sharedData.quotaCalculatorData.monthly forSegmentAtIndex:2];
    //-------------------------------------//
    
    NSString *currentSet = @"";
    double totalValue = 0.0;
    for (int i = 0; i < [sharedData.quotaCalculatorData.listUsage count]; i++) {
        InternetUsageModel *model = [sharedData.quotaCalculatorData.listUsage objectAtIndex:i];
        totalValue = totalValue + (model.sliderValue/model.maxScalePerdayValue*model.maxVolumeValue);
    }
    
    NSString *maxQuota = @"";
    if ([self.planType isEqualToString:@"1"]) {
        maxQuota = sharedData.quotaCalculatorData.totalVolumeDailyString;
        _planQuota = totalValue; // --> in kB
    }
    else if ([self.planType isEqualToString:@"2"]) {
        maxQuota = sharedData.quotaCalculatorData.totalVolumeWeeklyString;
        _planQuota = totalValue*7; // --> in kB
    }
    else {
        maxQuota = sharedData.quotaCalculatorData.totalVolumeMonthlyString;
        _planQuota = totalValue*30; // --> in kB
    }
    
    // convert to bytes
    totalValue = _planQuota*1024;
    NSNumber *totalNumber = [NSNumber numberWithDouble:totalValue];
    currentSet = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:totalNumber];
    
    
    _totalLabel.text = [NSString stringWithFormat:@"Total %@/%@ per day",currentSet,maxQuota];
}

- (void)segmentedControlChangedValue:(SVSegmentedControl*)segmentedControl
{
	//NSLog(@"segmentedControl %i did select index %i (via UIControl method)", segmentedControl.tag, segmentedControl.selectedSegmentIndex);
}

- (void)setupTotalLabelValue {
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *currentSet = @"";
    double totalValue = 0.0;
    for (int i = 0; i < [sharedData.quotaCalculatorData.listUsage count]; i++) {
        InternetUsageModel *model = [sharedData.quotaCalculatorData.listUsage objectAtIndex:i];
        totalValue = totalValue + (model.sliderValue/model.maxScalePerdayValue*model.maxVolumeValue);
    }
    
    NSString *maxQuota = @"";

    if ([self.planType isEqualToString:@"1"]) {
        maxQuota = sharedData.quotaCalculatorData.totalVolumeDailyString;
        _planQuota = totalValue; // --> in kB daily
    }
    else if ([self.planType isEqualToString:@"2"]) {
        maxQuota = sharedData.quotaCalculatorData.totalVolumeWeeklyString;
        _planQuota = totalValue*7; // --> in kB weekly
    }
    else {
        maxQuota = sharedData.quotaCalculatorData.totalVolumeMonthlyString;
        _planQuota = totalValue*30; // --> in kB monthly
    }
    
    // convert to bytes
    totalValue = _planQuota*1024;
    NSNumber *totalNumber = [NSNumber numberWithDouble:totalValue];
    currentSet = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:totalNumber];
    
    _totalLabel.text = [NSString stringWithFormat:@"Total %@/%@",currentSet,maxQuota];
}

- (IBAction)indexChanged:(UISegmentedControl *)sender {
    DataManager *sharedData = [DataManager sharedInstance];
    switch (self.segmentedControl.selectedSegmentIndex)
    {
        case 0:
            self.planType = @"1";
            _dailyAverageLabel.text = sharedData.quotaCalculatorData.dailyUsage;
            break;
        case 1:
            self.planType = @"2";
            _dailyAverageLabel.text = sharedData.quotaCalculatorData.weeklyUsage;
            break;
        case 2:
            self.planType = @"3";
            _dailyAverageLabel.text = sharedData.quotaCalculatorData.monthlyUsage;
            break;
        default:
            self.planType = @"1";
            _dailyAverageLabel.text = sharedData.quotaCalculatorData.dailyUsage;
            break; 
    }
    
    [self setupTotalLabelValue];
}

- (void)performSubmit:(id)sender
{
    /*
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = QUOTA_CALCULATOR_CALCULATE_REQ;
    // TODO: V1.9
    // Harcoded the planType and multiply planQuota with 30
    [request quotaCalculatorCalculate:[NSString stringWithFormat:@"%f",_planQuota*30] forPlanType:@"3"];*/
    
    // TODO: V1.9.5
    // Click Submit Redirect to Buy Package Instead Of Result Quota Calculator
    SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
    
    DataManager *dataManager = [DataManager sharedInstance];
    subMenuVC.parentMenuModel = dataManager.packageMenu;
    subMenuVC.isLevel2 = NO;
    subMenuVC.levelTwoType = COMMON;
    subMenuVC.grayHeaderTitle = dataManager.packageMenu.menuName;
    
    [self.navigationController pushViewController:subMenuVC animated:YES];
    subMenuVC = nil;
    [subMenuVC release];
}

#pragma mark - QuotaSliderViewDelegate

- (void)quotaSliderValueChanged:(ASValueTrackingSlider*)slider {
//    NSLog(@"slider %i value = %f", slider.tag, slider.value);
    [_barViewObject updateValueForBarIndex:slider.tag withValue:slider.value];
    [self setupLabelValue];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == QUOTA_CALCULATOR_CALCULATE_REQ) {
            SubMenuViewController *subMenuVC = [[SubMenuViewController alloc] initWithNibName:@"SubMenuViewController" bundle:nil];
            subMenuVC.isLevel2 = NO;
            subMenuVC.subMenuContentType = RECOMMENDED_XL_PACKAGE;
            // TODO : V1.9
            // Bugs fixing
            subMenuVC.grayHeaderTitle = [Language get:@"package_recommendation" alter:nil];
            
            [self.navigationController pushViewController:subMenuVC animated:YES];
            subMenuVC = nil;
            [subMenuVC release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
