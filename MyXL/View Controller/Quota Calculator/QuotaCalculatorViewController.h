//
//  QuotaCalculatorViewController.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 6/19/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BasicViewController.h"
#import "QuotaSliderView.h"

@interface QuotaCalculatorViewController : BasicViewController <QuotaSliderViewDelegate>

@end
