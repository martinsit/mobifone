//
//  InfoAndPromoViewController.h
//  MyXL
//
//  Created by Tity Septiani on 5/21/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AccordionView.h"
#import "MenuModel.h"
#import "DYRateView.h"

@interface InfoAndPromoViewController : BasicViewController
@property (nonatomic, retain) AccordionView *accordionView;
@property (nonatomic, retain) NSArray *contentData;
@property (nonatomic, retain) MenuModel *menuModel;
@property (nonatomic) BOOL isParentPromo;
@property (nonatomic, copy) NSString *pageTitle;
@end
