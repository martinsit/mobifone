//
//  DetailInfoAndPromoViewController.m
//  MyXL
//
//  Created by Tity Septiani on 6/5/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "DetailInfoAndPromoViewController.h"
#import "SVModalWebViewController.h"
#import "SVWebViewController.h"
#import "AsyncImageView.h"

@interface DetailInfoAndPromoViewController ()<UIAlertViewDelegate>
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UIView *logoHeaderView;
@property (retain, nonatomic) IBOutlet AsyncImageView *imgLogo;
@property (retain, nonatomic) IBOutlet UIView *subtitleView;
@property (retain, nonatomic) IBOutlet UILabel *lblSubtitle;
@property (retain, nonatomic) IBOutlet UIView *descriptionView;
@property (retain, nonatomic) IBOutlet UIView *voucherView;
@property (retain, nonatomic) IBOutlet UILabel *lblVoucherDesc;
@property (retain, nonatomic) IBOutlet UIView *termsAndConditionView;
@property (retain, nonatomic) IBOutlet UILabel *lblTermsAndCondition;
@property (retain, nonatomic) IBOutlet UIView *webLinkView;
@property (retain, nonatomic) IBOutlet UILabel *lblWebLink;
@property (retain, nonatomic) IBOutlet UIWebView *webBody;
@property (retain, nonatomic) IBOutlet UITextView *txtViewBody;
@property (retain, nonatomic) IBOutlet UIView *termsAndConditionsDetailView;
@property (retain, nonatomic) IBOutlet UIWebView *webTermsAndConditions;
@property (nonatomic) BOOL isNoRefresh;
@property (nonatomic, retain) UIButton *btnVoucherCode;

@end

@implementation DetailInfoAndPromoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self setupViewLayout];
}

-(void)viewWillAppear:(BOOL)animated
{
    if(!_isNoRefresh)
        [self setupViewLayout];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - View Setup
-(void)setupViewLayout
{
    /*
     * Clear View
     */
    for (UIView *oldViews in self.contentView.subviews)
    {
        if(oldViews.tag == 345 || oldViews.tag == 767)
            [oldViews removeFromSuperview];
    }
    
    UIFont *fontBold = [UIFont fontWithName:kDefaultFont size:12];
    UIFont *fontBoldTitle = [UIFont fontWithName:kDefaultFont size:14];
    _lblSubtitle.font = fontBoldTitle;
    _lblTermsAndCondition.font = fontBold;
    _lblVoucherDesc.font = fontBold;
    _lblWebLink.font = fontBold;
    _txtViewBody.font = fontBold;
    _lblTermsAndCondition.textColor = [UIColor whiteColor];
    _lblWebLink.textColor = [UIColor whiteColor];
    _lblSubtitle.textColor = [UIColor whiteColor];
    
    DataManager *sharedData = [DataManager sharedInstance];
    _voucherView.backgroundColor = [UIColor clearColor];
    _subtitleView.backgroundColor = kDefaultNavyBlueColor;
    _termsAndConditionView.backgroundColor = colorWithHexString(sharedData.profileData.themesModel.accordionBg);
    _webLinkView.backgroundColor = colorWithHexString(sharedData.profileData.themesModel.accordionBg);
    
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:_pageTitle
                                                               isFirstSection:YES];
    headerView1.tag = 345;
    [_contentView addSubview:headerView1];
    [headerView1 release];
    headerView1 = nil;
    
    if([_promoModel.promoImage length] > 0)
    {
        NSURL *imgLogoURL = [NSURL URLWithString:[_promoModel.promoImage stringByReplacingOccurrencesOfString:@"\\" withString:@""]];
        if(imgLogoURL)
            _imgLogo.imageURL = imgLogoURL;
    }
    else
    {
        _logoHeaderView.hidden = YES;
        [self repositionViewsWithOffset:-CGRectGetHeight(_logoHeaderView.frame) andStartTag:_logoHeaderView.tag+1];
    }
    
//    NSString *strSubtitle = @"dasfvashef ajhewje jwaejwqe wegjrksahfw wkje  ajegjwhe wejgw wjegrw wejqrwqjerwqefw wejfwjqef qwehjfwqjeh wewqewqef wewqewq wqjehwqjeh fasmdfaflqwefgwqgefgefgqfeqg fwqefhjqbwfegj";
    [self adjustHeightForLabel:_lblSubtitle andView:_subtitleView andText:[_promoModel.promoTitle uppercaseString]];
    _lblSubtitle.text = [_promoModel.promoTitle uppercaseString];
//    _lblSubtitle.text = [strSubtitle uppercaseString];
    
//    _txtViewBody.text = _promoModel.promoBody;
    [_webBody loadHTMLString:_promoModel.promoBody baseURL:nil];
    
    if([_promoModel.promoVoucherDesc length] > 0)
    {
        [self adjustHeightForLabel:_lblVoucherDesc andView:_voucherView andText:_promoModel.promoVoucherDesc];
        NSLog(@"Voucher view frame %@", NSStringFromCGRect(_voucherView.frame));
        _lblVoucherDesc.text = _promoModel.promoVoucherDesc;
        
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
        _btnVoucherCode = createButtonWithFrame(CGRectMake(CGRectGetWidth(_voucherView.frame) - 125,
                                                                    CGRectGetHeight(_voucherView.frame)/2 - 15,
                                                                    120,
                                                                    30),
                                                         _promoModel.promoBtnVoucher,
                                                         buttonFont,
                                                         kDefaultWhiteColor,
                                                         kDefaultAquaMarineColor,
                                                         kDefaultBlueColor);
        [_btnVoucherCode addTarget:self action:@selector(getVoucherCode:) forControlEvents:UIControlEventTouchUpInside];
        
        _btnVoucherCode.tag = 767;
        
        [_voucherView addSubview:_btnVoucherCode];
    }
    else
    {
        _voucherView.hidden = YES;
        [self repositionViewsWithOffset:-CGRectGetHeight(_voucherView.frame) andStartTag:_voucherView.tag+1];
    }
    
    [self adjustHeightForLabel:_lblTermsAndCondition andView:_termsAndConditionView andText:_promoModel.promoBtnTerm];
    _lblTermsAndCondition.text = _promoModel.promoBtnTerm;
    
    [self adjustHeightForLabel:_lblWebLink andView:_webLinkView andText:_promoModel.promoRelatedInfo];
    _lblWebLink.text = _promoModel.promoRelatedInfo;
    
    if([_promoModel.promoTermDesc length] > 0)
    {
        UITapGestureRecognizer *termsAndConditionTapped = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(termsAndConditionTapped:)] autorelease];
        termsAndConditionTapped.numberOfTapsRequired = 1;
        termsAndConditionTapped.numberOfTouchesRequired = 1;
        [_termsAndConditionView addGestureRecognizer:termsAndConditionTapped];
//        [termsAndConditionTapped release];
//        termsAndConditionTapped = nil;
    }
    else
    {
        _termsAndConditionView.hidden = YES;
        [self repositionViewsWithOffset:-CGRectGetHeight(_termsAndConditionView.frame) andStartTag:_termsAndConditionView.tag+1];
    }
    
    if([[[_promoModel.promoRelatedInfoURL stringByReplacingOccurrencesOfString:@"\\" withString:@""] stringByReplacingOccurrencesOfString:@"http://" withString:@""] length] > 0)
    {
        UITapGestureRecognizer *webLinkTapped = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openWebViewWithURL)] autorelease];
        webLinkTapped.numberOfTapsRequired = 1;
        webLinkTapped.numberOfTouchesRequired = 1;
        [_webLinkView addGestureRecognizer:webLinkTapped];
//        [webLinkTapped release];
//        webLinkTapped = nil;
    }
    else
    {
        _webLinkView.hidden = YES;
    }
    
    [self adjustScrollViewContentSize];
}

-(void)adjustScrollViewContentSize
{
    CGRect contentViewFrame = _contentView.frame;
    contentViewFrame.size.height = CGRectGetMaxY(_webLinkView.frame) + 20;
    _contentView.frame = contentViewFrame;
    _scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.frame), CGRectGetMaxY(_webLinkView.frame) + 20);
}

-(void)getVoucherCode:(id)sender
{
    [self showPopupAlertWithMessage:_promoModel.promoVoucher andPositiveButtonTitle:@"OK" andCancelBtnTitle:nil];
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.requestType = VOUCHER_PROMO_REQ;
    [request getVoucherPromoWithVoucherInfo:_promoModel.promoVoucher];
}

-(void)requestDoneWithInfo:(NSDictionary *)result
{
    
}

-(CGFloat)adjustHeightForLabel:(UILabel *)label andView:(UIView *)v andText:(NSString *)text
{
    @try
    {
        CGSize lblSize = calculateLabelSize(CGRectGetWidth(label.frame), text, label.font);
        CGFloat posYOffset = 0;
        if(lblSize.height > CGRectGetHeight(v.frame))
        {
            CGRect frame = label.frame;
            frame.size.height = lblSize.height;
            label.frame = CGRectIntegral(frame);
            
            posYOffset = lblSize.height - CGRectGetHeight(v.frame);
            CGRect vFrame = v.frame;
            vFrame.size.height = lblSize.height + 16;
            v.frame = CGRectIntegral(vFrame);
            if(posYOffset > 0 && v.tag > 1000 && v.tag < 1006)
                [self repositionViewsWithOffset:posYOffset+16 andStartTag:v.tag+1];
        }
        else if(lblSize.height > CGRectGetHeight(label.frame))
        {
            CGRect frame = label.frame;
            frame.size.height = lblSize.height;
            label.frame = CGRectIntegral(frame);
        }
    }
    @catch (NSException *exception) {
        
    }
    
    return 0;
}

-(void)repositionViewsWithOffset:(CGFloat)yOffset
                     andStartTag:(NSInteger)startTag
{
    for (int i = startTag; i < 1007; i++) {
        UIView *v = [self.view viewWithTag:i];
        if(v)
        {
            v.frame = CGRectOffset(v.frame, 0, yOffset);
        }
    }
}

-(void)openWebViewWithURL
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[_promoModel.promoRelatedInfoURL stringByReplacingOccurrencesOfString:@"\\" withString:@""]]];
    if(url)
    {
        _isNoRefresh = YES; // prevent the view from being reloaded after the webview is closed
        SVModalWebViewController *webView = [[SVModalWebViewController alloc] initWithURL:url];
        webView.modalPresentationStyle = UIModalPresentationPageSheet;
        [self presentViewController:webView animated:YES completion:nil];
        [webView release];
        webView = nil;
    }
}

-(void)termsAndConditionTapped:(UIGestureRecognizer *)recognizer
{
    CGRect imgHolderRect = _logoHeaderView.frame;
    if(_termsAndConditionsDetailView.hidden)
    {
        [_webTermsAndConditions loadHTMLString:_promoModel.promoTermDesc baseURL:nil];
        _termsAndConditionsDetailView.hidden = NO;
        CGRect detailRect = _termsAndConditionsDetailView.frame;
        detailRect.origin.y = CGRectGetMaxY(_termsAndConditionView.frame) + 1;
        _termsAndConditionsDetailView.frame = detailRect;
        
        CGRect weblinkRect = _webLinkView.frame;
        weblinkRect.origin.y = CGRectGetMaxY(_termsAndConditionsDetailView.frame);
        _webLinkView.frame = weblinkRect;
    }
    else
    {
        _termsAndConditionsDetailView.hidden = YES;
        CGRect weblinkRect = _webLinkView.frame;
        weblinkRect.origin.y = CGRectGetMaxY(_termsAndConditionView.frame) + 1;
        _webLinkView.frame = weblinkRect;
    }
    
    [self adjustScrollViewContentSize];
    _logoHeaderView.frame = imgHolderRect;
}

-(void)showPopupAlertWithMessage:(NSString *)message
          andPositiveButtonTitle:(NSString *)posBtnTitle
               andCancelBtnTitle:(NSString *)cancelBtnTitle
{
    UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:cancelBtnTitle otherButtonTitles:posBtnTitle, nil] autorelease];
    alertView.tag = 1;
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
        if(buttonIndex == 0)
        {
            
        }
        else
        {
            
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_scrollView release];
    [_contentView release];
    [_subtitleView release];
    [_lblSubtitle release];
    [_descriptionView release];
    [_voucherView release];
    [_lblVoucherDesc release];
    [_termsAndConditionView release];
    [_lblTermsAndCondition release];
    [_webLinkView release];
    [_lblWebLink release];
    [_txtViewBody release];
    [_termsAndConditionsDetailView release];
    [_webBody release];
    [_webTermsAndConditions release];
    [_btnVoucherCode release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setScrollView:nil];
    [self setContentView:nil];
    [self setSubtitleView:nil];
    [self setLblSubtitle:nil];
    [self setDescriptionView:nil];
    [self setVoucherView:nil];
    [self setLblVoucherDesc:nil];
    [self setTermsAndConditionView:nil];
    [self setLblTermsAndCondition:nil];
    [self setWebLinkView:nil];
    [self setLblWebLink:nil];
    [self setTxtViewBody:nil];
    [self setTermsAndConditionsDetailView:nil];
    [self setWebBody:nil];
    [self setWebTermsAndConditions:nil];
    [self setBtnVoucherCode:nil];
    [super viewDidUnload];
}
@end
