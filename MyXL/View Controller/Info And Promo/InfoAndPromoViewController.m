//
//  InfoAndPromoViewController.m
//  MyXL
//
//  Created by Tity Septiani on 5/21/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "InfoAndPromoViewController.h"
#import "SectionFooterView.h"
#import "SectionHeaderView.h"
#import "FlatButton.h"
#import "PromoModel.h"
#import "AXISnetCommon.h"
#import "DetailPromoViewController.h"
#import "DetailInfoAndPromoViewController.h"
#import "SVModalWebViewController.h"

@interface InfoAndPromoViewController ()

@end

@implementation InfoAndPromoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        self.screenName = @"InfoAndPromo";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupLayout];
    if(_isParentPromo)
    {
        [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    }
    else
    {
        [self createBarButtonItem:BACK];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    self.screenName = @"InfoAndPromoiOS";
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"InfoAndPromoiOS"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [[GAI sharedInstance] dispatch];
    [super viewDidAppear:animated];
}

-(void)setupLayout
{
    /*
     * Clear View
     */
    
    for (UIView *oldViews in self.view.subviews)
    {
        [oldViews removeFromSuperview];
    }
    
    //***********
    DataManager *sharedData = [DataManager sharedInstance];
    self.view.backgroundColor = kDefaultWhiteSmokeColor;
    
    SectionHeaderView *titleView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0.0, 31.0,  CGRectGetWidth([UIScreen mainScreen].bounds), kDefaultCellHeight*1.5)
                                                             withLabelForXL:_pageTitle
                                                             isFirstSection:YES];
    [self.view addSubview:titleView];

    
    _accordionView = [[AccordionView alloc] initWithFrame:CGRectMake(0, titleView.frame.origin.y + titleView.frame.size.height, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight(self.view.bounds))];
    [self.view addSubview:_accordionView];
    _accordionView.startsClosed = YES;
//    if(_isParentPromo)
//        _accordionView.showDetail = YES;
    
    UIFont *titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontHeaderSize];
    
            CGFloat bluecol = 211.0;
    for (int i = 0; i < [_contentData count]; i++) {
        PromoModel *promoModel = [_contentData objectAtIndex:i];
        FlatButton *button;
        bluecol = bluecol - 10.0;
        NSLog(@"%f",bluecol);
        button = [[FlatButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kDefaultCellHeight + 10.0)
                                             title:promoModel.promoTitle
                                         titleFont:titleFont
                                        titleColor:kDefaultWhiteColor
                                           bgColor:[UIColor colorWithRed:0.0/255.0 green:89.0/255.0 blue:bluecol/255.0 alpha:1.0]
                  //colorWithHexString(sharedData.profileData.themesModel.accordionBg)
                                     selectedColor:colorWithHexString(sharedData.profileData.themesModel.accordionBg)];
        
        button.tag = i;
        [button addTarget:self action:@selector(listItemClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        // Adjust Title Label height
        CGSize labelSize = calculateExpectedSize(button.titleLbl, promoModel.promoTitle);
        CGRect labelFrame = button.titleLbl.frame;
        labelFrame.size.height = labelSize.height;
        labelFrame.origin.y = (button.frame.size.height - labelFrame.size.height)/2;
        button.titleLbl.frame = labelFrame;
        /*
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, button.frame.size.height - 1, self.view.bounds.size.width, 1)];
        lineView.backgroundColor = kDefaultWhiteColor;
        [button addSubview:lineView];
         */

        UIView *contentView = [[UIView alloc] initWithFrame:CGRectZero];
        [_accordionView addHeader:button withView:contentView];
    }
}

-(BOOL)checkPromoChildsInArray:(NSArray *)promoChilds
{
    for (PromoModel *pm in promoChilds) {
        if(pm.promoTotalChild > 0)
        {
            return YES;
        }
    }
    
    return NO;
}

-(void)listItemClicked:(UIButton *)sender
{
    NSInteger *tag = sender.tag;
    PromoModel *promoModel = [_contentData objectAtIndex:tag];
    if([promoModel.promoBtnAction isEqualToString:@"popup"])
    {
//        [self showPopupAlertWithMessage:@"THIS IS A POPUP" andPositiveButtonTitle:@"OK" andCancelBtnTitle:nil];
    }
    else if([promoModel.promoBtnAction isEqualToString:@"open_url"])
    {
        [self openWebViewWithURL:promoModel.promoUrl];
    }
    else if(promoModel.promoTotalChild > 0 || [promoModel.promoBtnAction isEqualToString:@"drilldown"])
    {
        NSArray *promoChilds = promoModel.promoChild;
        InfoAndPromoViewController *infoAndPromoVC = [[InfoAndPromoViewController alloc] initWithNibName:@"InfoAndPromoViewController" bundle:nil];
        infoAndPromoVC.pageTitle = promoModel.promoTitle;
        infoAndPromoVC.contentData = promoChilds;
        infoAndPromoVC.menuModel = _menuModel;
        [self.navigationController pushViewController:infoAndPromoVC animated:YES];
    }
    // if promo has no child but with open_content action
    else if([promoModel.promoBtnAction isEqualToString:@"open_content"])
    {
        DetailInfoAndPromoViewController *detailPromoVC = [[DetailInfoAndPromoViewController alloc] initWithNibName:@"DetailInfoAndPromoViewController" bundle:nil];
        detailPromoVC.pageTitle = promoModel.promoTitle;
        detailPromoVC.promoModel = promoModel;
        [self.navigationController pushViewController:detailPromoVC animated:YES];
    }
}

-(void)openWebViewWithURL:(NSString *)strURL
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[strURL stringByReplacingOccurrencesOfString:@"\\" withString:@""]]];
    if(url)
    {
        SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
        webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
        [self presentViewController:webViewController animated:YES completion:NULL];
    }
}

-(void)showPopupAlertWithMessage:(NSString *)message
          andPositiveButtonTitle:(NSString *)posBtnTitle
               andCancelBtnTitle:(NSString *)cancelBtnTitle
{
    UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:cancelBtnTitle otherButtonTitles:posBtnTitle, nil] autorelease];
    alertView.tag = 1;
    [alertView show];
}

#pragma mark - UIAlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
        if(buttonIndex == 0)
        {
            
        }
        else
        {
            
        }
    }
}

-(void)dealloc
{
    [super dealloc];
//    [_menuModel release];
//    _menuModel = nil;
//    [_contentData release];
//    _contentData = nil;
    [_accordionView release];
    _accordionView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
