//
//  DetailInfoAndPromoViewController.h
//  MyXL
//
//  Created by Tity Septiani on 6/5/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "PromoModel.h"

@interface DetailInfoAndPromoViewController : BasicViewController
@property (nonatomic, copy) NSString *pageTitle;
@property (nonatomic, retain) PromoModel *promoModel;
@end
