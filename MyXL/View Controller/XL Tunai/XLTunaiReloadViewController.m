//
//  XLTunaiReloadViewController.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 12/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XLTunaiReloadViewController.h"
#import "Constant.h"
#import <QuartzCore/QuartzCore.h>
#import "ContentView.h"
#import "SectionHeaderView.h"

#import "CustomButton.h"
#import "AXISnetCommon.h"
#import "DataManager.h"
#import "EncryptDecrypt.h"

#import "DetailReloadBalanceViewController.h"

#import "UnderLineLabel.h"

#import "ActionSheetStringPicker.h"

#import "ThankYouViewController.h"

#import "NotificationViewController.h"

@interface XLTunaiReloadViewController ()<UIWebViewDelegate>

- (IBAction)performChangeToMyAXIS:(id)sender;
- (IBAction)performChangeToOtherAXIS:(id)sender;
- (IBAction)performShowNominal:(id)sender;
- (void)changeSentTo:(BOOL)isMyAXISSelected;
- (void)didTapMyAXISWithGesture:(UITapGestureRecognizer *)tapGesture;
- (void)didTapOtherAXISWithGesture:(UITapGestureRecognizer *)tapGesture;

- (void)performSubmit:(id)sender;
- (void)submit;

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *asteriskLabel;

@property (strong, nonatomic) IBOutlet UILabel *sentToLabel;
@property (strong, nonatomic) IBOutlet UILabel *yourAXISLabel;
@property (strong, nonatomic) IBOutlet UILabel *myMSISDNLabel;
@property (strong, nonatomic) IBOutlet UILabel *otherAXISLabel;
@property (strong, nonatomic) IBOutlet UITextField *otherMSISDNTF;

@property (strong, nonatomic) IBOutlet UILabel *nominalLabel;
@property (strong, nonatomic) IBOutlet UIButton *nominalButton;
@property (strong, nonatomic) NSArray *nominalAmount;
@property (strong, nonatomic) NSArray *nominalAmountLabel;
@property (readwrite) int rowIndex;

@property (strong, nonatomic) IBOutlet UILabel *suggestionLabel;
@property (strong, nonatomic) IBOutlet UITextField *voucherTF;
@property (strong, nonatomic) UIButton *submitButton;

@property (strong, nonatomic) IBOutlet UIButton *myAXISButton;
@property (strong, nonatomic) IBOutlet UIButton *otherAXISButton;

@property (strong, nonatomic) IBOutlet UILabel *captchaLabel;
@property (strong, nonatomic) IBOutlet UIWebView *captchaWebView;
@property (strong, nonatomic) IBOutlet UILabel *refreshLabel;
@property (strong, nonatomic) IBOutlet UnderLineLabel *recaptchaLabel;
@property (strong, nonatomic) IBOutlet UITextField *captchaTF;

@property (readwrite) BOOL isMyAXIS;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

// TODO : Hygiene
@property (nonatomic, retain) NSTimer *captchaTimer;

@end

@implementation XLTunaiReloadViewController

@synthesize headerTitle = _headerTitle;
@synthesize headerIcon = _headerIcon;
@synthesize menuModel = _menuModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    /*
     CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
     UIView *customView = [[UIView alloc] initWithFrame:applicationFrame];
     customView.backgroundColor = kDefaultBackgroundGrayColor;
     customView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
     [self.view addSubview:customView];*/
    
    self.nominalAmountLabel = [NSArray arrayWithObjects:@"Rp 25.000", @"Rp 50.000", @"Rp 100.000", @"Rp 200.000", nil];
    self.nominalAmount = [NSArray arrayWithObjects:@"25000", @"50000", @"100000", @"200000", nil];
}

/**
 *  Get XL Tunai Balance from the API
 */
- (void)performXLTunaiBalance
{
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = XL_TUNAI_BALANCE_REQ;
    [request xlTunaiBalance];
}

- (void)viewWillAppear:(BOOL)animated {
    self.theScrollView.hidden = YES;
    //--------------//
    // Setup Header //
    //--------------//
    // TODO : V1.9
    // Fixing, double request API XL Tunai Balance
    //[self performXLTunaiBalance];
    self.theScrollView.hidden = NO;
    [self setupLayout];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Screen tracking
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    self.screenName = @"XLTunaiIOS";
    [tracker set:kGAIScreenName value:@"XLTunaiIOS"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [[GAI sharedInstance] dispatch];
}

-(void)setupLayout
{
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *xltunaiBalance = @"";
    if(sharedData.xlTunaiBalanceData)
    {
        xltunaiBalance = [AXISnetCommon formatStringWithCurrency:[sharedData.xlTunaiBalanceData.balance floatValue]];
    }
    
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:_menuModel.menuName
                                                                  andSubtitle:[NSString stringWithFormat:@"Rp. %@,-",xltunaiBalance]
                                                            andIsFirstSection:YES];
    
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:_menuModel.groupName
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
    //                                                               withLabelForXL:_menuModel.groupName
    //                                                               isFirstSection:YES];
    //    [_contentView addSubview:headerView1];
    //
    //    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
    //                                                               withLabelForXL:_menuModel.menuName
    //                                                               isFirstSection:NO];
    //    [_contentView addSubview:headerView2];
    
    //----------------//
    // Setup POSTPAID //
    //----------------//
    //    DataManager *sharedData = [DataManager sharedInstance];
    if (sharedData.profileData.msisdnType == POSTPAID) {
        _yourAXISLabel.hidden = YES;
        _myMSISDNLabel.hidden = YES;
        _myAXISButton.hidden = YES;
        
        // Set Default to Other XL Number
        [self performChangeToOtherAXIS:nil];
        
        [self setupPostpaidLayout];
    }
    else {
        _yourAXISLabel.hidden = NO;
        _myMSISDNLabel.hidden = NO;
        _myAXISButton.hidden = NO;
        
        // Set Default to My XL Number
        [self performChangeToMyAXIS:nil];
    }
    
    //-------------//
    // Setup Label //
    //-------------//
    
    /*
     * Asterisk
     */
    _asteriskLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _asteriskLabel.textColor = kDefaultNavyBlueColor;
    _asteriskLabel.text = [Language get:@"asterisk" alter:nil];
    
    _sentToLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _sentToLabel.textColor = kDefaultTitleFontGrayColor;
    _sentToLabel.backgroundColor = [UIColor clearColor];
    _sentToLabel.text = [Language get:@"send_to" alter:nil];
    
    _yourAXISLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _yourAXISLabel.textColor = kDefaultNavyBlueColor;
    _yourAXISLabel.backgroundColor = [UIColor clearColor];
    _yourAXISLabel.text = [Language get:@"your_axis_number" alter:nil];
    
    _myMSISDNLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _myMSISDNLabel.textColor = kDefaultNavyBlueColor;
    _myMSISDNLabel.backgroundColor = [UIColor clearColor];
    
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    if ([msisdn length] > 0) {
        msisdn = [msisdn stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"0"];
    }
    _myMSISDNLabel.text = msisdn;
    
    _otherAXISLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _otherAXISLabel.textColor = kDefaultNavyBlueColor;
    _otherAXISLabel.backgroundColor = [UIColor clearColor];
    _otherAXISLabel.text = [Language get:@"other_axis_number" alter:nil];
    
    _otherMSISDNTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _otherMSISDNTF.textColor = kDefaultNavyBlueColor;
    
    _suggestionLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _suggestionLabel.textColor = kDefaultTitleFontGrayColor;
    _suggestionLabel.backgroundColor = [UIColor clearColor];
    _suggestionLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"pin_xl_tunai" alter:nil]];
    [_suggestionLabel sizeToFit];
    
    _nominalLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _nominalLabel.textColor = kDefaultTitleFontGrayColor;
    _nominalLabel.backgroundColor = [UIColor clearColor];
    _nominalLabel.text = [Language get:@"nominal_balance" alter:nil];
    
    _rowIndex = 0;
    [_nominalButton setTitle:[self.nominalAmountLabel objectAtIndex:_rowIndex] forState:UIControlStateNormal];
    [_nominalButton setTitleColor:kDefaultNavyBlueColor forState:UIControlStateNormal];
    _nominalButton.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _nominalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _nominalButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    UIImage *backgroundImage = [[UIImage imageNamed:@"combo_bg_1.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0];
    [_nominalButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
    _voucherTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _voucherTF.textColor = kDefaultNavyBlueColor;
    _voucherTF.placeholder = [Language get:@"6_digits" alter:nil];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(didTapMyAXISWithGesture:)];
    [_yourAXISLabel setUserInteractionEnabled:YES];
    [_yourAXISLabel addGestureRecognizer:tapGesture];
    [tapGesture release];
    
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(didTapOtherAXISWithGesture:)];
    [_otherAXISLabel setUserInteractionEnabled:YES];
    [_otherAXISLabel addGestureRecognizer:tapGesture2];
    [tapGesture2 release];
    
    // Add by iNot 22 July 2013
    // Captcha
    _captchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _captchaLabel.textColor = kDefaultTitleFontGrayColor;
    NSString *text = [NSString stringWithFormat:@"%@*",[Language get:@"captcha_code" alter:nil]];
    _captchaLabel.text = text;
    
    // TODO : V1.9
    // Bugs fixing captcha not showing because the webview frame is wrong
    _captchaWebView.frame = CGRectMake(CGRectGetMinX(_captchaWebView.frame), CGRectGetMaxY(_captchaWebView.frame) + 8, CGRectGetWidth(_captchaWebView.frame), CGRectGetHeight(_captchaWebView.frame));
    
    _refreshLabel.frame = CGRectMake(CGRectGetMinX(_refreshLabel.frame), CGRectGetMaxY(_captchaWebView.frame) + 8, CGRectGetWidth(_refreshLabel.frame), CGRectGetHeight(_refreshLabel.frame));
    
    _recaptchaLabel.frame = CGRectMake(CGRectGetMinX(_recaptchaLabel.frame), CGRectGetMaxY(_captchaWebView.frame) + 8, CGRectGetWidth(_recaptchaLabel.frame), CGRectGetHeight(_recaptchaLabel.frame));
    
    _captchaTF.frame = CGRectMake(CGRectGetMinX(_captchaTF.frame), CGRectGetMaxY(_recaptchaLabel.frame) + 8, CGRectGetWidth(_captchaTF.frame), CGRectGetHeight(_captchaTF.frame));
    //*********************************************************//
    
    // Refresh Label
    _refreshLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontTitleSize];
    _refreshLabel.textColor = kDefaultNavyBlueColor;
    
    _recaptchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _recaptchaLabel.textColor = kDefaultNavyBlueColor;
    _recaptchaLabel.text = [Language get:@"recaptcha" alter:nil];
    [_recaptchaLabel setShouldUnderline:NO];
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(refreshCaptcha)];
    [_recaptchaLabel setUserInteractionEnabled:YES];
    [_recaptchaLabel addGestureRecognizer:tapGesture3];
    [tapGesture3 release];
    
    _captchaTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _captchaTF.textColor = kDefaultNavyBlueColor;
    
    //****** Request on 5 September 2013, Hide Captcha ******
    //_captchaLabel.hidden = YES;
    //_recaptchaLabel.hidden = YES;
    //_captchaWebView.hidden = YES;
    //_captchaTF.hidden = YES;
    //*******************************************************
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    newFrame.size.height = _captchaTF.frame.origin.y + _captchaTF.frame.size.height;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Setup Button //
    //--------------//
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    UIButton *button = createButtonWithFrame(CGRectMake(_captchaTF.frame.origin.x,
                                                        _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             [[Language get:@"submit" alter:nil] uppercaseString],
                                             buttonFont,
                                             kDefaultWhiteColor,
                                             kDefaultAquaMarineColor,
                                             kDefaultBlueColor);
    [button addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    _submitButton = button;
    [_contentView addSubview:_submitButton];
    
    //--------------//
    // Content View //
    //--------------//
    newFrame = _contentView.frame;
    newFrame.size.height = _submitButton.frame.origin.y + _submitButton.frame.size.height + kDefaultComponentPadding;
    _contentView.frame = newFrame;
    _contentView.backgroundColor = kDefaultWhiteColor;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 200.0);
    
    [headerView1 release];
    [headerView2 release];
    
    // Request Captcha
    [self refreshCaptcha];
}

// TODO : Hygiene
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_captchaTimer invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.headerTitle = nil;
    self.headerIcon = nil;
}

- (void)dealloc {
    [_headerTitle release];
    _headerTitle = nil;
    [_headerIcon release];
    _headerIcon = nil;
    [super dealloc];
}

#pragma mark - Selector

- (IBAction)performShowNominal:(id)sender {
    [_otherMSISDNTF resignFirstResponder];
    [_voucherTF resignFirstResponder];
    [_captchaTF resignFirstResponder];
    
    [ActionSheetStringPicker showPickerWithTitle:@""
                                            rows:self.nominalAmountLabel
                                initialSelection:_rowIndex
                                          target:self
                                   successAction:@selector(actionPickerSelected:element:)
                                    cancelAction:@selector(actionPickerCanceled:)
                                          origin:sender];
}

- (IBAction)performChangeToMyAXIS:(id)sender {
    _isMyAXIS = YES;
    [self changeSentTo:_isMyAXIS];
}

- (void)didTapMyAXISWithGesture:(UITapGestureRecognizer *)tapGesture {
    _isMyAXIS = YES;
    [self changeSentTo:_isMyAXIS];
}

- (IBAction)performChangeToOtherAXIS:(id)sender {
    _isMyAXIS = NO;
    [self changeSentTo:_isMyAXIS];
}

- (void)didTapOtherAXISWithGesture:(UITapGestureRecognizer *)tapGesture {
    _isMyAXIS = NO;
    [self changeSentTo:_isMyAXIS];
}

- (void)changeSentTo:(BOOL)isMyAXISSelected {
    if (isMyAXISSelected) {
        [_myAXISButton setSelected:YES];
        [_otherAXISButton setSelected:NO];
        
        _myMSISDNLabel.hidden = NO;
        _otherMSISDNTF.hidden = YES;
    }
    else {
        [_myAXISButton setSelected:NO];
        [_otherAXISButton setSelected:YES];
        
        _myMSISDNLabel.hidden = YES;
        _otherMSISDNTF.hidden = NO;
    }
}

- (void)performSubmit:(id)sender {
    NSLog(@"PerfSubmit");
    [_otherMSISDNTF resignFirstResponder];
    [_voucherTF resignFirstResponder];
    [_captchaTF resignFirstResponder];
    //[_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    
    int valid = [self validateInput];
    if (valid == 2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"empty_validation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 99;
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
    else {
        //DataManager *sharedData = [DataManager sharedInstance];
        //NSString *price = [NSString stringWithFormat:@"%@",addThousandsSeparator(sharedData.xlTunaiBalanceData.balance, sharedData.profileData.language)];
        NSString *msisdnTo = @"";
        if (_isMyAXIS) {
            msisdnTo = _myMSISDNLabel.text;
        }
        else {
            msisdnTo = _otherMSISDNTF.text;
        }
        
        NSString *confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@. \n %@",
                                         [Language get:@"xl_tunai_reload_confirm_1" alter:nil],
                                         [self.nominalAmountLabel objectAtIndex:_rowIndex],
                                         [Language get:@"xl_tunai_reload_confirm_2" alter:nil],
                                         [self.nominalAmountLabel objectAtIndex:_rowIndex],
                                         [Language get:@"xl_tunai_reload_confirm_3" alter:nil],
                                         msisdnTo,
                                         [Language get:@"ask_continue" alter:nil]];
        
        UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                  message:confirmationMessage
                                                                 delegate:self
                                                        cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                        otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
        buyPackageAlert.tag = 2;
        [buyPackageAlert show];
    }
}

- (void)submit
{
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = XL_TUNAI_RELOAD_REQ;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    if (_isMyAXIS) {
        [request xlTunaiReload:_voucherTF.text
                      msisdnTo:sharedData.profileData.msisdn
                    withAmount:[self.nominalAmount objectAtIndex:_rowIndex]
                   withCaptcha:_captchaTF.text
                       withCid:sharedData.cid];
    }
    else {
        NSString *saltKeyAPI = SALT_KEY;
        NSString *msisdnToEncrypted = [EncryptDecrypt doCipherForiOS7:_otherMSISDNTF.text action:kCCEncrypt withKey:saltKeyAPI];
        //NSLog(@"msisdnToEncrypted = %@",msisdnToEncrypted);
        
        [request xlTunaiReload:_voucherTF.text
                      msisdnTo:msisdnToEncrypted
                    withAmount:[self.nominalAmount objectAtIndex:_rowIndex]
                   withCaptcha:_captchaTF.text
                       withCid:sharedData.cid];
    }
}


-(void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"webview did start load");
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"webview did finish load");
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"webview did fail to load with error %@", [error description]);
}

- (void)refreshCaptcha {
    // TODO : Hygiene
    if(_captchaTimer != nil)
        [_captchaTimer invalidate];
    [self scheduleTimerForCaptcha];
    
    NSString *captchaURL = generateCaptchaURL();
    NSURL *captcha = [NSURL URLWithString:captchaURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:captcha];
    _captchaWebView.delegate = self;
    [_captchaWebView loadRequest:request];
    NSLog(@"webview frame %@", NSStringFromCGRect(_captchaWebView.frame));
    [request release];
}

// TODO : Hygiene
// Timer for captcha refresh
-(void)scheduleTimerForCaptcha
{
    NSDate *time = [NSDate dateWithTimeIntervalSinceNow:CAPTCHA_REFRESH_TIME];
    NSTimer *t = [[NSTimer alloc] initWithFireDate:time
                                          interval:CAPTCHA_REFRESH_TIME
                                            target:self
                                          selector:@selector(refreshCaptcha)
                                          userInfo:nil repeats:YES];
    
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer:t forMode: NSDefaultRunLoopMode];
    [t release];
}

- (int)validateInput {
    /*
     * Validate empty field
     */
    if (_isMyAXIS) {
        if ([_voucherTF.text length] <= 0 ||
            [_captchaTF.text length] <= 0) {
            return 2;
        }
        else {
            return 0;
        }
    }
    else {
        if ([_voucherTF.text length] <= 0 ||
            [_captchaTF.text length] <= 0 ||
            [_otherMSISDNTF.text length] <= 0) {
            return 2;
        }
        else {
            return 0;
        }
    }
}

- (void)setupPostpaidLayout {
    CGRect frame = _otherAXISButton.frame;
    frame.origin.y = _myAXISButton.frame.origin.y;
    _otherAXISButton.frame = frame;
    
    frame = _otherAXISLabel.frame;
    frame.origin.y = _yourAXISLabel.frame.origin.y;
    _otherAXISLabel.frame = frame;
    
    frame = _otherMSISDNTF.frame;
    frame.origin.y = _myMSISDNLabel.frame.origin.y;
    _otherMSISDNTF.frame = frame;
    
    frame = _nominalLabel.frame;
    frame.origin.y = _otherMSISDNTF.frame.origin.y + _otherMSISDNTF.frame.size.height + kDefaultComponentPadding;
    _nominalLabel.frame = frame;
    
    frame = _nominalButton.frame;
    frame.origin.y = _nominalLabel.frame.origin.y + _nominalLabel.frame.size.height + kDefaultComponentPadding;
    _nominalButton.frame = frame;
    
    frame = _suggestionLabel.frame;
    frame.origin.y = _nominalButton.frame.origin.y + _nominalButton.frame.size.height + (kDefaultComponentPadding*2);
    _suggestionLabel.frame = frame;
    
    frame = _voucherTF.frame;
    frame.origin.y = _suggestionLabel.frame.origin.y + _suggestionLabel.frame.size.height + kDefaultComponentPadding;
    _voucherTF.frame = frame;
    
    frame = _captchaLabel.frame;
    frame.origin.y = _voucherTF.frame.origin.y + _voucherTF.frame.size.height + kDefaultComponentPadding;
    _captchaLabel.frame = frame;
    
    frame = _captchaWebView.frame;
    frame.origin.y = _captchaLabel.frame.origin.y + _captchaLabel.frame.size.height + kDefaultComponentPadding;
    _captchaWebView.frame = frame;
    
    frame = _recaptchaLabel.frame;
    frame.origin.y = _captchaWebView.frame.origin.y + _captchaWebView.frame.size.height + kDefaultComponentPadding;
    _recaptchaLabel.frame = frame;
    
    frame = _captchaTF.frame;
    frame.origin.y = _captchaWebView.frame.origin.y + _captchaWebView.frame.size.height + kDefaultComponentPadding;
    _captchaTF.frame = frame;
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == XL_TUNAI_RELOAD_REQ) {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            
            //thankYouViewController.info = [Language get:@"xl_tunai_reload_thank_you" alter:nil];
            NSString *reason = [result valueForKey:REASON_KEY];
            thankYouViewController.info = reason;
            thankYouViewController.menuModel = _menuModel;
            thankYouViewController.displayFBButton = NO;
            
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            thankYouViewController = nil;
            [thankYouViewController release];
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
        
        if(type == XL_TUNAI_BALANCE_REQ)
        {
            self.theScrollView.hidden = NO;
            [self setupLayout];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _captchaTF) {
        [textField resignFirstResponder];
        [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat targetY = -10 + textField.frame.origin.y;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _voucherTF) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return !([newString length] > 6);
    }
    else {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return !([newString length] > 16);
    }
}

#pragma mark - Implementation ActionSheetPicker.h

- (void)actionPickerSelected:(NSNumber *)selectedIndex element:(id)element {
    _rowIndex = [selectedIndex intValue];
    [_nominalButton setTitle:[self.nominalAmountLabel objectAtIndex:_rowIndex] forState:UIControlStateNormal];
}

- (void)actionPickerCanceled:(id)sender {
    //NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // tag == 2 XL Tunai Reload
    if (alertView.tag == 2) {
        if (buttonIndex == 0) {
            //NSLog(@"YES");
            [self submit];
        }
        else {
            //NSLog(@"NO");
        }
    }
}

@end
