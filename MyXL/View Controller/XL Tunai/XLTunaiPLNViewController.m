//
//  XLTunaiPLNViewController.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 12/22/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "XLTunaiPLNViewController.h"
#import "Constant.h"
#import <QuartzCore/QuartzCore.h>
#import "ContentView.h"
#import "SectionHeaderView.h"

#import "CustomButton.h"
#import "AXISnetCommon.h"
#import "DataManager.h"
#import "EncryptDecrypt.h"

#import "DetailReloadBalanceViewController.h"

#import "UnderLineLabel.h"

#import "ActionSheetStringPicker.h"

#import "ThankYouViewController.h"

#import "NotificationViewController.h"

@interface XLTunaiPLNViewController ()

//- (IBAction)performChangeToMyAXIS:(id)sender;
//- (IBAction)performChangeToOtherAXIS:(id)sender;
- (IBAction)performShowNominal:(id)sender;
//- (void)changeSentTo:(BOOL)isMyAXISSelected;
//- (void)didTapMyAXISWithGesture:(UITapGestureRecognizer *)tapGesture;
//- (void)didTapOtherAXISWithGesture:(UITapGestureRecognizer *)tapGesture;

- (void)performSubmit:(id)sender;
- (void)submit;

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *asteriskLabel;

@property (strong, nonatomic) IBOutlet UILabel *meterLabel;
@property (strong, nonatomic) IBOutlet UITextField *meterTF;

@property (strong, nonatomic) IBOutlet UILabel *nominalLabel;
@property (strong, nonatomic) IBOutlet UIButton *nominalButton;
@property (strong, nonatomic) NSArray *nominalAmount;
@property (strong, nonatomic) NSArray *nominalAmountLabel;
@property (readwrite) int rowIndex;

@property (strong, nonatomic) IBOutlet UILabel *suggestionLabel;
@property (strong, nonatomic) IBOutlet UITextField *voucherTF;
@property (strong, nonatomic) UIButton *submitButton;

@property (strong, nonatomic) IBOutlet UILabel *captchaLabel;
@property (strong, nonatomic) IBOutlet UIWebView *captchaWebView;
@property (strong, nonatomic) IBOutlet UILabel *refreshLabel;
@property (strong, nonatomic) IBOutlet UnderLineLabel *recaptchaLabel;
@property (strong, nonatomic) IBOutlet UITextField *captchaTF;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

// TODO : Hygiene
@property (nonatomic, retain) NSTimer *captchaTimer;

@end

@implementation XLTunaiPLNViewController

@synthesize headerTitle = _headerTitle;
@synthesize headerIcon = _headerIcon;
@synthesize menuModel = _menuModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    /*
     CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
     UIView *customView = [[UIView alloc] initWithFrame:applicationFrame];
     customView.backgroundColor = kDefaultBackgroundGrayColor;
     customView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
     [self.view addSubview:customView];*/
    
    self.nominalAmountLabel = [NSArray arrayWithObjects:@"Rp 20.000", @"Rp 25.000", @"Rp 50.000", @"Rp 100.000", @"Rp. 200.000", @"Rp 500.000", nil];
    self.nominalAmount = [NSArray arrayWithObjects:@"20000", @"25000", @"50000", @"100000", @"200000", @"500000", nil];
}

- (void)viewWillAppear:(BOOL)animated {
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:_menuModel.groupName
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:_menuModel.menuName
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    
    /*
     * Asterisk
     */
    _asteriskLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _asteriskLabel.textColor = kDefaultNavyBlueColor;
    _asteriskLabel.text = [Language get:@"asterisk" alter:nil];
    
    _meterLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _meterLabel.textColor = kDefaultTitleFontGrayColor;
    _meterLabel.backgroundColor = [UIColor clearColor];
    _meterLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"meter_id" alter:nil]];
    
    _meterTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _meterTF.textColor = kDefaultNavyBlueColor;
    
    _suggestionLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _suggestionLabel.textColor = kDefaultTitleFontGrayColor;
    _suggestionLabel.backgroundColor = [UIColor clearColor];
    _suggestionLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"pin_xl_tunai" alter:nil]];
    [_suggestionLabel sizeToFit];
    
    _nominalLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _nominalLabel.textColor = kDefaultTitleFontGrayColor;
    _nominalLabel.backgroundColor = [UIColor clearColor];
    _nominalLabel.text = [Language get:@"nominal" alter:nil];
    
    _rowIndex = 0;
    [_nominalButton setTitle:[self.nominalAmountLabel objectAtIndex:_rowIndex] forState:UIControlStateNormal];
    [_nominalButton setTitleColor:kDefaultNavyBlueColor forState:UIControlStateNormal];
    _nominalButton.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _nominalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _nominalButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    UIImage *backgroundImage = [[UIImage imageNamed:@"combo_bg_1.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0];
    [_nominalButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
    _voucherTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _voucherTF.textColor = kDefaultNavyBlueColor;
    _voucherTF.placeholder = [Language get:@"6_digits" alter:nil];
    
    // Add by iNot 22 July 2013
    // Captcha
    _captchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _captchaLabel.textColor = kDefaultTitleFontGrayColor;
    NSString *text = [NSString stringWithFormat:@"%@*",[Language get:@"captcha_code" alter:nil]];
    _captchaLabel.text = text;
    
    // Refresh Label
    _refreshLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontTitleSize];
    _refreshLabel.textColor = kDefaultNavyBlueColor;
    
    _recaptchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _recaptchaLabel.textColor = kDefaultNavyBlueColor;
    _recaptchaLabel.text = [Language get:@"recaptcha" alter:nil];
    [_recaptchaLabel setShouldUnderline:NO];
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(refreshCaptcha)];
    [_recaptchaLabel setUserInteractionEnabled:YES];
    [_recaptchaLabel addGestureRecognizer:tapGesture3];
    [tapGesture3 release];
    
    _captchaTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _captchaTF.textColor = kDefaultNavyBlueColor;
    
    //****** Request on 5 September 2013, Hide Captcha ******
    //_captchaLabel.hidden = YES;
    //_recaptchaLabel.hidden = YES;
    //_captchaWebView.hidden = YES;
    //_captchaTF.hidden = YES;
    //*******************************************************
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    newFrame.size.height = _captchaTF.frame.origin.y + _captchaTF.frame.size.height;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Setup Button //
    //--------------//
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    UIButton *button = createButtonWithFrame(CGRectMake(_captchaTF.frame.origin.x,
                                                        _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             [[Language get:@"submit" alter:nil] uppercaseString],
                                             buttonFont,
                                             kDefaultWhiteColor,
                                             kDefaultAquaMarineColor,
                                             kDefaultBlueColor);
    
    [button addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    _submitButton = button;
    [_contentView addSubview:_submitButton];
    
    //--------------//
    // Content View //
    //--------------//
    newFrame = _contentView.frame;
    newFrame.size.height = _submitButton.frame.origin.y + _submitButton.frame.size.height + kDefaultComponentPadding;
    _contentView.frame = newFrame;
    _contentView.backgroundColor = kDefaultWhiteColor;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 200.0);
    
    [headerView1 release];
    [headerView2 release];
    
    // Request Captcha
    [self refreshCaptcha];
}

// TODO : Hygiene
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_captchaTimer invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.headerTitle = nil;
    self.headerIcon = nil;
}

- (void)dealloc {
    [_headerTitle release];
    _headerTitle = nil;
    [_headerIcon release];
    _headerIcon = nil;
    [super dealloc];
}

#pragma mark - Selector

- (IBAction)performShowNominal:(id)sender {
    [_meterTF resignFirstResponder];
    [_voucherTF resignFirstResponder];
    [_captchaTF resignFirstResponder];
    
    [ActionSheetStringPicker showPickerWithTitle:@""
                                            rows:self.nominalAmountLabel
                                initialSelection:_rowIndex
                                          target:self
                                   successAction:@selector(actionPickerSelected:element:)
                                    cancelAction:@selector(actionPickerCanceled:)
                                          origin:sender];
}

- (void)performSubmit:(id)sender {
    [_meterTF resignFirstResponder];
    [_voucherTF resignFirstResponder];
    [_captchaTF resignFirstResponder];
    //[_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    
    int valid = [self validateInput];
    if (valid == 2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"empty_validation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 99;
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
    else {
        //DataManager *sharedData = [DataManager sharedInstance];
        //NSString *price = [NSString stringWithFormat:@"%@",addThousandsSeparator(sharedData.xlTunaiBalanceData.balance, sharedData.profileData.language)];
        
        NSString *confirmationMessage = [NSString stringWithFormat:@"%@ %@ %@ %@. \n %@",
                                         [Language get:@"xl_tunai_reload_confirm_1" alter:nil],
                                         [self.nominalAmountLabel objectAtIndex:_rowIndex],
                                         [Language get:@"xl_tunai_pln_confirm_2" alter:nil],
                                         _meterTF.text,
                                         [Language get:@"ask_continue" alter:nil]];
        
        UIAlertView *buyPackageAlert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                                  message:confirmationMessage
                                                                 delegate:self
                                                        cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                                        otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString],nil];
        buyPackageAlert.tag = 2;
        [buyPackageAlert show];
    }
}

- (void)submit
{
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = XL_TUNAI_PLN_REQ;
    
    DataManager *sharedData = [DataManager sharedInstance];
    [request xlTunaiPLN:_voucherTF.text
            withMeterId:_meterTF.text
             withAmount:[self.nominalAmount objectAtIndex:_rowIndex]
            withCaptcha:_captchaTF.text
                withCid:sharedData.cid];
}

- (void)refreshCaptcha {
    // TODO : Hygiene
    if(_captchaTimer != nil)
        [_captchaTimer invalidate];
    [self scheduleTimerForCaptcha];
    
    NSString *captchaURL = generateCaptchaURL();
    NSURL *captcha = [NSURL URLWithString:captchaURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:captcha];
    [_captchaWebView loadRequest:request];
    [request release];
}

// TODO : Hygiene
// Timer for captcha refresh
-(void)scheduleTimerForCaptcha
{
    NSDate *time = [NSDate dateWithTimeIntervalSinceNow:CAPTCHA_REFRESH_TIME];
    NSTimer *t = [[NSTimer alloc] initWithFireDate:time
                                          interval:CAPTCHA_REFRESH_TIME
                                            target:self
                                          selector:@selector(refreshCaptcha)
                                          userInfo:nil repeats:YES];
    
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer:t forMode: NSDefaultRunLoopMode];
    [t release];
}

- (int)validateInput {
    /*
     * Validate empty field
     */
    if ([_voucherTF.text length] <= 0 ||
        [_captchaTF.text length] <= 0 ||
        [_meterTF.text length] <= 0) {
        return 2;
    }
    else {
        return 0;
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == XL_TUNAI_PLN_REQ) {
            ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
            
            //thankYouViewController.info = [Language get:@"xl_tunai_pln_thank_you" alter:nil];
            NSString *reason = [result valueForKey:REASON_KEY];
            thankYouViewController.info = reason;
            thankYouViewController.menuModel = _menuModel;
            thankYouViewController.displayFBButton = NO;
            
            [self.navigationController pushViewController:thankYouViewController animated:YES];
            thankYouViewController = nil;
            [thankYouViewController release];
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _captchaTF) {
        [textField resignFirstResponder];
        [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat targetY = -10 + textField.frame.origin.y;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _voucherTF) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return !([newString length] > 6);
    }
    else {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return !([newString length] > 16);
    }
}

#pragma mark - Implementation ActionSheetPicker.h

- (void)actionPickerSelected:(NSNumber *)selectedIndex element:(id)element {
    _rowIndex = [selectedIndex intValue];
    [_nominalButton setTitle:[self.nominalAmountLabel objectAtIndex:_rowIndex] forState:UIControlStateNormal];
}

- (void)actionPickerCanceled:(id)sender {
    //NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // tag == 2 XL Tunai Reload
    if (alertView.tag == 2) {
        if (buttonIndex == 0) {
            //NSLog(@"YES");
            [self submit];
        }
        else {
            //NSLog(@"NO");
        }
    }
}

@end
