//
//  PaketSukaRenameViewController.m
//  MyXL
//
//  Created by tyegah on 3/13/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PaketSukaRenameViewController.h"
#import "SectionHeaderView.h"
#import "RenamePackageModel.h"
#import "PaketSukaRenamePackageViewController.h"

@interface PaketSukaRenameViewController ()
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;
@property (retain, nonatomic) IBOutlet UIView *packagesView;

@end

@implementation PaketSukaRenameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    for (UIView *subView in [_contentView subviews])
    {
        if ([subView isKindOfClass:[SectionHeaderView class]])
            [subView removeFromSuperview];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    DataManager *sharedData = [DataManager sharedInstance];
    if(sharedData.renameSukaSukaData)
    {
        [self setupView];
    }
}

-(void)setupView
{
    DataManager *sharedData = [DataManager sharedInstance];
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:sharedData.renameSukaSukaData.pageTitle
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    [headerView1 release];
    headerView1 = nil;
    
    _lblTitle.text = sharedData.renameSukaSukaData.pageDesc;
    _lblTitle.hidden = NO;
    
    CGFloat positionY = 0;
    NSInteger viewTag = 1;
    if(sharedData.renameSukaSukaData.packages && [sharedData.renameSukaSukaData.packages count] > 0)
    {
        for (RenamePackageModel *pkgModel in sharedData.renameSukaSukaData.packages) {
            CGFloat height = [self createListItemWithTitle:pkgModel.packageName andIcon:@"Q" andPositionY:positionY andViewTag:viewTag];
            viewTag++;
            positionY += height;
        }
    }
}

-(CGFloat)createListItemWithTitle:(NSString *)title andIcon:(NSString *)icon andPositionY:(CGFloat)positionY andViewTag:(NSInteger)viewTag
{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, positionY, CGRectGetWidth(_packagesView.frame), 30)];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    view.backgroundColor = [UIColor colorWithRed:5/255.0 green:28/255.0 blue:91/255.0 alpha:1.0];
    view.tag = viewTag + 10;
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(packageClicked:)];
//    [view addGestureRecognizer:tapGesture];
//    [tapGesture release];
//    tapGesture = nil;
    CGFloat leftMargin = 10;
    CGFloat iconSize = 25;

    UIFont *lblFont = [UIFont fontWithName:kDefaultFontBold size:12];
    CGSize lblSize = calculateLabelSize(CGRectGetWidth(view.frame) - iconSize - (leftMargin*3), title, lblFont);
    
    CGRect viewFrame = view.frame;
    viewFrame.size.height = lblSize.height + 16;
    view.frame = viewFrame;
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 0, CGRectGetWidth(view.frame) - iconSize - (leftMargin*3), CGRectGetHeight(view.frame))];
    lblTitle.font = [UIFont fontWithName:kDefaultFontBold size:12];
    lblTitle.textColor = [UIColor colorWithRed:94/255.0 green:210/255.0 blue:240/255.0 alpha:1.0];
    lblTitle.text = title;
    lblTitle.numberOfLines = 0;
    
    UIButton *btnIcon = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblTitle.frame) + leftMargin, (CGRectGetHeight(viewFrame) - iconSize)/2, iconSize, iconSize)];
    btnIcon.titleLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:17];
    btnIcon.backgroundColor = [UIColor colorWithRed:253/255.0 green:250/255.0 blue:41/255.0 alpha:1.0];
    [btnIcon setTitle:icon forState:UIControlStateNormal];
    [btnIcon setTitleColor:kDefaultBlackColor forState:UIControlStateNormal];
    btnIcon.tag = viewTag + 30;
    btnIcon.layer.cornerRadius = 5;
    [btnIcon addTarget:self action:@selector(btnIconClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:lblTitle];
    [view addSubview:btnIcon];
    
    [btnIcon release];
    [lblTitle release];
    btnIcon = nil;
    lblTitle = nil;
    
    [_packagesView addSubview:view];
    
    CGRect pkgFrame = _packagesView.frame;
    pkgFrame.size.height = CGRectGetMaxY(view.frame) + 20;
    _packagesView.frame = pkgFrame;
    CGFloat viewHeight = CGRectGetHeight(view.frame);
    [view release];
    view = nil;
    
    return viewHeight + 8;
}

-(void)btnIconClicked:(id)sender
{
    @try
    {
        NSInteger tag = (UIView *)[sender tag];
        NSInteger index = tag - 31;
        DataManager *sharedData = [DataManager sharedInstance];
        if(index < [sharedData.renameSukaSukaData.packages count])
        {
            RenamePackageModel *pkgModel = [sharedData.renameSukaSukaData.packages objectAtIndex:index];
            if(pkgModel)
            {
                [self mappingRenameSukaSukaWithServiceID:pkgModel.serviceID];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

-(void)packageClicked:(UIGestureRecognizer *)recognizer
{
    
}

-(void)mappingRenameSukaSukaWithServiceID:(NSString *)serviceID
{
    @try
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = MAPPING_RENAME_SUKASUKA_REQ;
        [request mappingRenameSukaSukaWithServiceID:serviceID];
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

-(void)getQuotaSukaSuka
{
    @try
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = QUOTA_SUKASUKA_REQ;
        [request checkQuotaSukasuka];
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

#pragma mark - AXISnetRequest Delegate
-(void)requestDoneWithInfo:(NSDictionary *)result
{
    [self.hud hide:YES];
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
    int type = [typeNumber intValue];
    BOOL success = [reqResult boolValue];
    DataManager *sharedData = [DataManager sharedInstance];
    if (success)
    {
        if(type == QUOTA_SUKASUKA_REQ)
        {
            if(sharedData.renameSukaSukaData)
            {
                [self setupView];
            }
        }
        else if(type == MAPPING_RENAME_SUKASUKA_REQ)
        {
            [self gotoRenameMappingPage];
        }
    }
    else
    {
        if(type == QUOTA_SUKASUKA_REQ)
        {
            if(sharedData.renameSukaSukaData)
            {
                [self setupView];
            }
            else
            {
                NSString *reason = [result valueForKey:REASON_KEY];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                                message:reason
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
        }
        else
        {
            NSString *reason = [result valueForKey:REASON_KEY];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                            message:reason
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
}

-(void)gotoRenameMappingPage
{
    @try
    {
        PaketSukaRenamePackageViewController *renamePkgVC = [[PaketSukaRenamePackageViewController alloc] initWithNibName:@"PaketSukaRenamePackageViewController" bundle:nil];
        [self.navigationController pushViewController:renamePkgVC animated:YES];
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_scrollView release];
    [_contentView release];
    [_lblTitle release];
    [_packagesView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setScrollView:nil];
    [self setContentView:nil];
    [self setLblTitle:nil];
    [self setPackagesView:nil];
    [super viewDidUnload];
}
@end
