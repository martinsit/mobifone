//
//  SukaMenuCell.m
//  MyXL
//
//  Created by tyegah on 3/13/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "SukaMenuCell.h"

@implementation SukaMenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)init
{
    self = [[[[NSBundle mainBundle] loadNibNamed:@"SukaMenuCell" owner:nil options:nil]
             lastObject]
            retain];
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_lblTitle release];
    [_btnIcon release];
    [_cellContentView release];
    [super dealloc];
}
@end
