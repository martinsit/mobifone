//
//  PaketSukaNameViewController.m
//  MyXL
//
//  Created by tyegah on 1/28/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PaketSukaNameViewController.h"
#import "ThankYouViewController.h"
#import "SectionHeaderView.h"
#import "DataManager.h"
#import "IIViewDeckController.h"
#import "LeftViewController.h"
#import "IIWrapController.h"
#import "MappingSukaSukaModel.h"

@interface PaketSukaNameViewController ()
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UIView *headerView;
@property (retain, nonatomic) IBOutlet UIView *footerView;
@property (retain, nonatomic) IBOutlet UILabel *lblFooterTitle;
@property (retain, nonatomic) IBOutlet UITextField *txtPackageName;
@property (retain, nonatomic) IBOutlet UIButton *btnCheckName;
@property (retain, nonatomic) IBOutlet UILabel *lblErrorPackageName;
@property (retain, nonatomic) IBOutlet UILabel *lblPaketSukaTitle;
@property (retain, nonatomic) UIButton *submitButton;
@property (nonatomic, assign) float animatedDis;
@property (retain, nonatomic) IBOutlet UILabel *lblNelpon;
@property (retain, nonatomic) IBOutlet UILabel *lblPaketNelponDesc;
@property (retain, nonatomic) IBOutlet UILabel *lblSMS;
@property (retain, nonatomic) IBOutlet UILabel *lblPaketSMSDesc;
@property (retain, nonatomic) IBOutlet UILabel *lblInternet;
@property (retain, nonatomic) IBOutlet UILabel *lblPaketInternetDesc;
@property (retain, nonatomic) IBOutlet UILabel *lblPriceTitle;
@property (retain, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) LeftViewController *sideMenuViewController;
@property (retain, nonatomic) IBOutlet UILabel *lblActivePeriodTitle;
@property (retain, nonatomic) IBOutlet UILabel *lblActivePeriod;
@property (nonatomic, retain) UITextField *activeTextField;


@end

@implementation PaketSukaNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    _txtPackageName.delegate = self;
    DataManager *sharedData = [DataManager sharedInstance];
    if(sharedData.mappingSukaSukaData)
    {
        [self setupViewLayout];
        [self setupLabelValue];
    }
    
    // TODO : Neptune
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

// TODO : Neptune
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
//    if(IS_IPAD)
//    {
        NSDictionary *info = [notification userInfo];
        NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect keyboardFrame = [kbFrame CGRectValue];
        keyboardFrame = [self.view convertRect:keyboardFrame fromView:nil];
        
        CGFloat heightOfKeyboard = keyboardFrame.size.height;
        if(!IS_IPAD)
            heightOfKeyboard = heightOfKeyboard - 20;
        //NSLog(@"The keyboard is now %d pixels high.", (int)heightOfKeyboard);
        if(_activeTextField != nil)
            [self animateTextField:_activeTextField up:YES withDistance:heightOfKeyboard];
//    }
}

- (void)keyboardWillHide:(NSNotification *)notification
{
//    if (IS_IPAD) {
        NSDictionary *info = [notification userInfo];
        NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect keyboardFrame = [kbFrame CGRectValue];
        keyboardFrame = [self.view convertRect:keyboardFrame fromView:nil];
        
        CGFloat heightOfKeyboard = keyboardFrame.size.height;
        if(!IS_IPAD)
            heightOfKeyboard = heightOfKeyboard - 20;
        //NSLog(@"The keyboard is now %d pixels high.", (int)heightOfKeyboard);
        [self animateTextField:_activeTextField up:NO withDistance:heightOfKeyboard];
        _activeTextField = nil;
//    }
}

-(void)setupViewLayout
{
    UIFont *fontBold = [UIFont fontWithName:kDefaultFont size:15];
    
    UIFont *fontBoldBigger = [UIFont fontWithName:kDefaultFont size:18];
    
    UIFont *fontLight = [UIFont fontWithName:kDefaultFontLight size:15];
    UIColor *orangeColor = [UIColor colorWithRed:242/255.0 green:154/255.0 blue:39/255.0 alpha:1];
    
    _lblPaketSukaTitle.font = fontBold;
    _lblNelpon.font = fontBold;
    _lblSMS.font = fontBold;
    _lblInternet.font = fontBold;
    _lblPrice.font = fontBoldBigger;
    _lblPriceTitle.font = fontBold;
    _lblPrice.textColor = orangeColor;
    
    _lblPaketNelponDesc.font = fontLight;
    _lblPaketSMSDesc.font = fontLight;
    _lblPaketInternetDesc.font = fontLight;
    _lblPaketInternetDesc.numberOfLines = 0;
    _lblPaketInternetDesc.lineBreakMode = UILineBreakModeWordWrap;
    
    _lblErrorPackageName.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    _lblFooterTitle.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    _lblActivePeriod.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    _lblActivePeriod.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    _lblActivePeriodTitle.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    _lblActivePeriodTitle.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    
    _txtPackageName.layer.borderWidth = 3;
    _txtPackageName.layer.borderColor = orangeColor.CGColor;
    _txtPackageName.layer.cornerRadius = 5;
    _txtPackageName.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    _txtPackageName.placeholder = [Language get:@"package_name_placeholder" alter:nil];
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *packageTitle = sharedData.matrixSukaSukaData.title_pkg;
    if([packageTitle length] == 0)
        packageTitle = [Language get:@"package_sesukamu" alter:nil];
    int lang = [Language getCurrentLanguage];
    NSString *lblPackageSummaryTitle = [Language get:@"package_summary" alter:nil];
    if(lang == 1)
    {
        lblPackageSummaryTitle = [NSString stringWithFormat:@"%@ %@", packageTitle, lblPackageSummaryTitle];
    }
    
    _lblPaketSukaTitle.text = lblPackageSummaryTitle;
    _lblFooterTitle.text = [Language get:@"name_your_package" alter:nil];
    if(sharedData.mappingSukaSukaData.custom_pkg != nil && ![sharedData.mappingSukaSukaData.custom_pkg isEqual:[NSNull null]])
    {
        _lblFooterTitle.text = sharedData.mappingSukaSukaData.custom_pkg;
    }
    _lblNelpon.text = [Language get:@"voice_package" alter:nil];
    _lblPriceTitle.text = [Language get:@"price_idr" alter:nil];
    _lblActivePeriodTitle.text = [Language get:@"package_active_period" alter:nil];
    
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:packageTitle
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:[Language get:@"create_your_own_package" alter:nil]
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    _headerView.frame = CGRectOffset(_headerView.frame, 0, CGRectGetHeight(headerView2.frame) + CGRectGetHeight(headerView1.frame) - CGRectGetMinY(_headerView.frame));
    
    _footerView.frame = CGRectMake(CGRectGetMinX(_footerView.frame), CGRectGetMaxY(_headerView.frame), CGRectGetWidth(_footerView.frame), CGRectGetHeight(_footerView.frame));
    
    //--------------//
    // Setup Button //
    //--------------//
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    NSString *buttonText = sharedData.matrixSukaSukaData.label_submit;
    
    UIButton *submitBtn = createButtonWithFrame(CGRectMake(CGRectGetWidth(self.view.frame) - 120,
                                                           CGRectGetMaxY(_footerView.frame) + kDefaultComponentPadding,
                                                           100,
                                                           kDefaultButtonHeight),
                                                [buttonText uppercaseString],
                                                buttonFont,
                                                kDefaultWhiteColor,
                                                kDefaultAquaMarineColor,
                                                kDefaultBlueColor);
    submitBtn.frame = _btnCheckName.frame;
    
    [_btnCheckName setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                             forState:UIControlStateNormal];
    [_btnCheckName setBackgroundImage:imageFromColor(kDefaultAquaMarineColor)
                             forState:UIControlStateHighlighted];
    
    _btnCheckName.layer.cornerRadius = kDefaultCornerRadius;
    _btnCheckName.layer.masksToBounds = YES;
    
    _btnCheckName.titleLabel.font = buttonFont;
    _btnCheckName.titleLabel.textColor = kDefaultWhiteColor;
    
    [submitBtn addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    _submitButton = submitBtn;
    [_footerView addSubview:_submitButton];
    
    _contentView.frame = CGRectMake(CGRectGetMinX(_contentView.frame), CGRectGetMinY(_contentView.frame), CGRectGetWidth(_contentView.frame), CGRectGetHeight(_contentView.frame) + kDefaultComponentPadding);
    
    _scrollView.contentSize = CGSizeMake(0, CGRectGetMaxY(_contentView.frame));
}

-(void)setupLabelValue
{
    DataManager *sharedData = [DataManager sharedInstance];
    MappingSukaSukaModel *mappingModel = sharedData.mappingSukaSukaData;
    _lblPaketSMSDesc.text = mappingModel.smsDetail;
    _lblPaketNelponDesc.text = mappingModel.voiceDetail;
    _lblPaketInternetDesc.text = mappingModel.internetDetail;
    _lblPrice.text = [NSString stringWithFormat:@"%@",mappingModel.price];
    _lblActivePeriod.text = mappingModel.duration;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up withDistance:(CGFloat)distance
{
    CGPoint temp = [textField.superview convertPoint:textField.frame.origin toView:nil];
    if(up)
    {
        int moveUpValue = temp.y+textField.frame.size.height;
        //NSLog(@"moveup value %d", moveUpValue);
        _animatedDis = distance -(self.view.frame.size.height - moveUpValue);
        //NSLog(@"frame height %f, animatedDis %f", self.view.frame.size.height, _animatedDis);
    }
    
    if(_animatedDis>0)
    {
        const int movementDistance = _animatedDis;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        
        // animate the view up / down
        [UIView animateWithDuration:movementDuration animations:^ {
            self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        }];
    }
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    CGPoint temp = [textField.superview convertPoint:textField.frame.origin toView:nil];
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait){
        
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            //NSLog(@"moveup value %d", moveUpValue);
            _animatedDis = 220 -(self.view.frame.size.height - moveUpValue);
            //NSLog(@"frame height %f, animatedDis %f", self.view.frame.size.height, _animatedDis);
        }
    }
    
    if(_animatedDis>0)
    {
        const int movementDistance = _animatedDis;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        
        // animate the view up / down
        [UIView animateWithDuration:movementDuration animations:^ {
            if (orientation == UIInterfaceOrientationPortrait){
                self.view.frame = CGRectOffset(self.view.frame, 0, movement);
            }
        }];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 15);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if(!IS_IPAD)
//        [self animateTextField:textField up:YES];
//    else
        _activeTextField = textField;
    self.lblErrorPackageName.hidden = YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    if(!IS_IPAD)
//        [self animateTextField:textField up:NO];
//    else
        _activeTextField = nil;
}

- (IBAction)btnCheckNameClicked:(id)sender {
    [_txtPackageName resignFirstResponder];
    if([self.txtPackageName.text isEqualToString:@""])
    {
        
    }
    else
    {
        if([self.txtPackageName.text isEqualToString:@"test"])
        {
            _lblErrorPackageName.hidden = NO;
        }
        else
        {
            
        }
    }
}

-(void)performSubmit:(id)sender
{
    [_txtPackageName resignFirstResponder];
    
    [self buyPaketSukaSuka];
    _submitButton.enabled = NO;
}

-(void)gotoThankyouPageWithInfo:(NSString *)info
{
    DataManager *dataManager = [DataManager sharedInstance];
    
    LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    leftViewController.content = dataManager.leftMenuData;;
    _sideMenuViewController = leftViewController;
    
    ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
    thankYouViewController.info = info;
    thankYouViewController.displayFBButton = NO;
    thankYouViewController.displaySocialMediaButtons = YES;
    thankYouViewController.hideInboxBtn = NO;
    thankYouViewController.packageName = _txtPackageName.text;
    
    MenuModel *menuModel = [[MenuModel alloc] init];
    menuModel.groupName = [Language get:@"thank_you" alter:nil];
    thankYouViewController.menuModel = menuModel;
    [menuModel release];
    
    UIViewController *infoViewController = thankYouViewController;
    
    IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:infoViewController leftViewController:_sideMenuViewController];
    deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
    deckController.leftSize = self.view.frame.size.width - 260; //270
    infoViewController = deckController;
    
    UINavigationController *navController = customizedNavigationController(self.view.bounds,NO);
    [navController setViewControllers:[NSArray arrayWithObject:infoViewController]];
    
    infoViewController = [[IIWrapController alloc] initWithViewController:navController];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController pushViewController:infoViewController animated:YES];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self buyPaketSukaSuka];
    }
    else {
        _submitButton.enabled = YES;
    }
}

-(void)buyPaketSukaSuka
{
    @try
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        DataManager *dataManager = [DataManager sharedInstance];
        MappingSukaSukaModel *dataModel = dataManager.mappingSukaSukaData;
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = MAPPING_PAKET_SUKASUKA_REQ;
        [request buyPackageSukaSukaWithName:_txtPackageName.text andAmount:dataModel.price andPackageID:dataModel.serviceID];
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

#pragma mark - AXISnetRequest Delegate
-(void)requestDoneWithInfo:(NSDictionary *)result
{
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
     NSString *reason = [result valueForKey:REASON_KEY];
    NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
    int type = [typeNumber intValue];
    
    if(type == BUY_PAKET_SUKASUKA_REQ)
        [self.hud hide:YES];
    
    BOOL success = [reqResult boolValue];
    if (success)
    {
        _submitButton.enabled = YES;
        [self gotoThankyouPageWithInfo:reason];
    }
    else
    {
        _submitButton.enabled = YES;
        [[[[UIAlertView alloc] initWithTitle:[Language get:@"error" alter:nil]
                                     message:reason
                                    delegate:nil
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil] autorelease] show];
    }
}

- (void)dealloc {
    [_scrollView release];
    [_contentView release];
    [_headerView release];
    [_footerView release];
//    [_lblPkgTitle release];
//    [_lblPkgName release];
//    [_lblPkgDescription release];
//    [_lblPkgName2 release];
//    [_lblPkgDescription2 release];
//    [_lblOnlyWith release];
    [_lblPrice release];
//    [_lblPeriod release];
    [_lblFooterTitle release];
    [_txtPackageName release];
    [_btnCheckName release];
    [_lblErrorPackageName release];
    [_lblPaketSukaTitle release];
    [_lblNelpon release];
    [_lblPaketNelponDesc release];
    [_lblSMS release];
    [_lblPaketSMSDesc release];
    [_lblInternet release];
    [_lblPaketInternetDesc release];
    [_lblPriceTitle release];
    [_lblPrice release];
    [_lblActivePeriodTitle release];
    [_lblActivePeriod release];
    [_activeTextField release];
    [_submitButton release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setScrollView:nil];
    [self setContentView:nil];
    [self setHeaderView:nil];
    [self setFooterView:nil];
//    [self setLblPkgTitle:nil];
//    [self setLblPkgName:nil];
//    [self setLblPkgDescription:nil];
//    [self setLblPkgName2:nil];
//    [self setLblPkgDescription2:nil];
//    [self setLblOnlyWith:nil];
    [self setLblPrice:nil];
//    [self setLblPeriod:nil];
    [self setLblFooterTitle:nil];
    [self setTxtPackageName:nil];
    [self setBtnCheckName:nil];
    [self setLblErrorPackageName:nil];
    [self setLblPaketSukaTitle:nil];
    [self setLblNelpon:nil];
    [self setLblPaketNelponDesc:nil];
    [self setLblSMS:nil];
    [self setLblPaketSMSDesc:nil];
    [self setLblInternet:nil];
    [self setLblPaketInternetDesc:nil];
    [self setLblPriceTitle:nil];
    [self setLblPrice:nil];
    [self setLblActivePeriodTitle:nil];
    [self setLblActivePeriod:nil];
    [super viewDidUnload];
}
@end
