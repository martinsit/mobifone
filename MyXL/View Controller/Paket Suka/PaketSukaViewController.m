//
//  PaketSukaViewController.m
//  MyXL
//
//  Created by tyegah on 1/27/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PaketSukaViewController.h"
#import "SectionHeaderView.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "DataManager.h"
#import "InternetUsageModel.h"

#import "QuotaCalculatorBar.h"

#import "SubMenuViewController.h"

#import "HumanReadableDataSizeHelper.h"
#import "PaketSukaNameViewController.h"
#import "CheckUsageXLModel.h"
#import "PackageSukaMatrixModel.h"
#import "NotificationViewController.h"

@interface PaketSukaViewController ()
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UIView *fieldView;
@property (retain, nonatomic) IBOutlet UILabel *lblInternetUsageTitle;
@property (retain, nonatomic) IBOutlet UILabel *lblActivePeriod;
@property (retain, nonatomic) IBOutlet UILabel *lblInternetPackageUsage;
@property (retain, nonatomic) IBOutlet UIView *sliderContainerView;
@property (strong, nonatomic) UIButton *submitButton;
@property (readwrite) double planQuota;
@property (strong, nonatomic) NSString *planType;
@property (retain, nonatomic) IBOutlet UIView *footerView;
@property (retain, nonatomic) IBOutlet UILabel *lblEstimationTitle;
@property (retain, nonatomic) IBOutlet UILabel *lblEstimationQuota;
@property (retain, nonatomic) IBOutlet UIView *radioContainerView;
@property (retain, nonatomic) IBOutlet UIView *barContainer;
@property (assign, nonatomic) NSInteger *internetOpt;
@property (assign, nonatomic) NSInteger *smsOpt;
@property (assign, nonatomic) NSInteger *voiceOpt;

@property (nonatomic, retain) QuotaCalculatorBar *barViewObject;

@property (readwrite) BOOL isCheckingPackage;

@end

#define OPTION_NORMAL_COLOR [UIColor colorWithRed:217/255.0 green:217/255.0 blue:217/255.0 alpha:1];
#define OPTION_SELECTED_COLOR_YELLOW [UIColor colorWithRed:250/255.0 green:225/255.0 blue:0/255.0 alpha:1];
#define OPTION_SELECTED_COLOR_BLUE [UIColor colorWithRed:0/255.0 green:159/255.0 blue:223/255.0 alpha:1];

#define OPTION_NORMAL_COLOR [UIColor colorWithRed:217/255.0 green:217/255.0 blue:217/255.0 alpha:1];
#define OPTION_SELECTED_COLOR [UIColor colorWithRed:243/255.0 green:233/255.0 blue:39/255.0 alpha:1];

@implementation PaketSukaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //    [self setupUILayout];
    _internetOpt = 0;
    _smsOpt = 0;
    _voiceOpt = 0;
    _fieldView.hidden = YES;
    _isCheckingPackage = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self checkAndRemoveSubviews];
    
    [self setupUILayoutWithMatrix:YES];
    [self setupLabelValue];
    [self setupTotalLabelValue];
    _fieldView.hidden = NO;
    [self getMyPackage];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    [request cancelAllRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkAndRemoveSubviews {
    for (UIView *subView in [_contentView subviews])
    {
        if ([subView isKindOfClass:[SectionHeaderView class]])
            [subView removeFromSuperview];
        
        if ([subView isKindOfClass:[UIButton class]])
            [subView removeFromSuperview];
    }
    
    for (UIView *subView in [_sliderContainerView subviews])
    {
        if ([subView isKindOfClass:[QuotaSliderView class]])
            [subView removeFromSuperview];
    }
    
    for (UIView *subView in [_radioContainerView subviews])
    {
        if ([subView isKindOfClass:[UIView class]])
            [subView removeFromSuperview];
    }
}

- (void)setupUILayoutWithMatrix:(BOOL)showMatrix {
    DataManager *sharedData = [DataManager sharedInstance];
    if(sharedData.matrixSukaSukaData)
    {
        //--------------//
        // Setup Header //
        //--------------//
        NSString *packageTitle = [sharedData.matrixSukaSukaData.title_pkg length] > 0 ? sharedData.matrixSukaSukaData.title_pkg : @"";
        if([packageTitle length] == 0)
            packageTitle = [Language get:@"package_sesukamu" alter:nil];
        SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                                   withLabelForXL:packageTitle
                                                                   isFirstSection:YES];
        [_contentView addSubview:headerView1];
        
        SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                                   withLabelForXL:[Language get:@"create_your_own_package" alter:nil]
                                                                   isFirstSection:NO];
        [_contentView addSubview:headerView2];
        
        //-------------//
        // Setup Label //
        //-------------//
        _lblInternetUsageTitle.font = [UIFont fontWithName:kDefaultFont size:15];
        
        _lblInternetUsageTitle.text = [Language get:@"your_previous_package" alter:nil];
        if (sharedData.matrixSukaSukaData.prev_pkg != nil && ![sharedData.matrixSukaSukaData.prev_pkg isEqual:[NSNull null]]) {
            _lblInternetUsageTitle.text = sharedData.matrixSukaSukaData.prev_pkg;
        }
        //    _lblInternetUsageTitle.textColor = kDefaultTitleFontGrayColor;
        
        _lblEstimationTitle.font = [UIFont fontWithName:kDefaultFont size:15];
        
        //_lblQuotaMeterTitle.font = [UIFont fontWithName:kDefaultFont size:15];
        
        //_lblTotal.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
        _lblEstimationQuota.font = [UIFont fontWithName:kDefaultFontBold size:15.0];
        
        _lblActivePeriod.font = [UIFont fontWithName:kDefaultFont size:15];
        _lblInternetPackageUsage.font = [UIFont fontWithName:kDefaultFontLight size:16];
        _lblActivePeriod.text = [Language get:@"create_package" alter:nil];
        
        if(showMatrix)
        {
            //********************************* NEW PAKET VIEW ************************//
            CGFloat viewWidth = [UIScreen mainScreen].bounds.size.width;
            CGFloat defaultItemPadding = 4;
            CGFloat leftRightPadding = 8;
            CGFloat lblHeaderHeight = 25;
            CGFloat frameWidth = (viewWidth - (leftRightPadding*2) - (defaultItemPadding*2))/3;
            CGFloat frameHeight = 80;
            CGFloat imgIconSize = 15;
            
            UIFont *headerFont = [UIFont fontWithName:kDefaultFontBold size:15];
            
            UIFont *optionFont = [UIFont fontWithName:kDefaultFontBold size:10];
            if(IS_IPAD)
                optionFont = [UIFont fontWithName:kDefaultFontBold size:12];
            
            //**************** HEADER ****************//
            UIView *leftHeaderView = [[[UIView alloc] init] autorelease];
            leftHeaderView.frame = CGRectMake(leftRightPadding, 20, frameWidth, frameHeight);
            leftHeaderView.backgroundColor = [UIColor clearColor];
            
            UILabel *lblLeftHeader = [[[UILabel alloc] init]autorelease];
            lblLeftHeader.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblHeaderHeight);
            lblLeftHeader.textAlignment = NSTextAlignmentLeft;
            lblLeftHeader.font = headerFont;
            lblLeftHeader.textColor = kDefaultWhiteColor;
            lblLeftHeader.text = [Language get:@"voice_package" alter:nil];
            
            [leftHeaderView addSubview:lblLeftHeader];
            
            UIView *centerHeaderView = [[[UIView alloc] init] autorelease];
            centerHeaderView.frame = CGRectMake(CGRectGetMaxX(leftHeaderView.frame) + defaultItemPadding, 20, frameWidth, frameHeight);
            centerHeaderView.backgroundColor = [UIColor clearColor];
            
            UILabel *lblCenterHeader = [[[UILabel alloc] init]autorelease];
            lblCenterHeader.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblHeaderHeight);
            lblCenterHeader.textAlignment = NSTextAlignmentLeft;
            lblCenterHeader.font = headerFont;
            lblCenterHeader.textColor = kDefaultWhiteColor;
            lblCenterHeader.text = @"SMS";
            
            [centerHeaderView addSubview:lblCenterHeader];
            
            UIView *rightHeaderView = [[[UIView alloc] init] autorelease];
            rightHeaderView.frame = CGRectMake(CGRectGetMaxX(centerHeaderView.frame) + defaultItemPadding, 20, frameWidth, frameHeight);
            rightHeaderView.backgroundColor = [UIColor clearColor];
            
            UILabel *lblRightHeader = [[[UILabel alloc] init]autorelease];
            lblRightHeader.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblHeaderHeight);
            lblRightHeader.textAlignment = NSTextAlignmentLeft;
            lblRightHeader.font = headerFont;
            lblRightHeader.textColor = kDefaultWhiteColor;
            lblRightHeader.text = @"INTERNET";
            
            [rightHeaderView addSubview:lblRightHeader];
            
            UIImageView *imgLeftHeader = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg-nelpon-orange.png"]] autorelease];
            imgLeftHeader.contentMode = UIViewContentModeScaleToFill;
            imgLeftHeader.frame = leftHeaderView.frame;
            UIImageView *imgCenterHeader = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg-sms-orange.png"]] autorelease];
            imgCenterHeader.frame = centerHeaderView.frame;
            UIImageView *imgRightHeader = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg-internet-orange.png"]] autorelease];
            imgRightHeader.frame = rightHeaderView.frame;
            
            
            [_radioContainerView addSubview:imgLeftHeader];
            [_radioContainerView addSubview:imgCenterHeader];
            [_radioContainerView addSubview:imgRightHeader];
            
            [_radioContainerView addSubview:leftHeaderView];
            [_radioContainerView addSubview:centerHeaderView];
            [_radioContainerView addSubview:rightHeaderView];
            
            //************************* OPTION 1 **************************//
            PackageSukaMatrixModel *matrixModel = sharedData.matrixSukaSukaData;
            if(matrixModel)
            {
                if([matrixModel.sms count] == 4 && [matrixModel.data count] == 4 && [matrixModel.voice count] == 4)
                {
                    CGFloat lblOptionHeight = frameHeight - (defaultItemPadding*3) - imgIconSize;
                    UIImage *imgOption = [UIImage imageNamed:@"icon-check-green.png"];
                    UIImage *imgRadioOn = [UIImage imageNamed:@"radio-on"];
                    UIImage *imgRadioOff = [UIImage imageNamed:@"radio-off"];
                    
                    // TODO : VOICE 1
                    
                    UIView *leftOption1View = [[[UIView alloc] init] autorelease];
                    leftOption1View.frame = CGRectMake(leftRightPadding, CGRectGetMaxY(leftHeaderView.frame) + defaultItemPadding, frameWidth, frameHeight);
                    leftOption1View.backgroundColor = OPTION_SELECTED_COLOR_BLUE;
                    leftOption1View.tag = 11;
                    
                    UILabel *lblLeftOption1 = [[[UILabel alloc] init]autorelease];
                    lblLeftOption1.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblOptionHeight);
                    lblLeftOption1.textAlignment = NSTextAlignmentCenter;
                    lblLeftOption1.font = optionFont;
                    lblLeftOption1.textColor = [UIColor blackColor];
                    lblLeftOption1.numberOfLines = 0;
                    
                    NSString *strOpt = @"";
                    NSDictionary *optDict = (NSDictionary *)[matrixModel.voice objectAtIndex:0];
                    NSString *onnet = [optDict objectForKey:@"onnet"];
                    NSString *offnet = [optDict objectForKey:@"offnet"];
                    
                    if([offnet length] > 0 && [onnet length] > 0)
                        strOpt = [NSString stringWithFormat:@"%@\n%@", [optDict objectForKey:@"onnet"], [optDict objectForKey:@"offnet"]];
                    else
                    {
                        if([offnet length] == 0)
                        {
                            strOpt = onnet;
                        }
                        else
                        {
                            strOpt = offnet;
                        }
                    }
                    
                    lblLeftOption1.text = strOpt;
                    
                    [leftOption1View addSubview:lblLeftOption1];
                    
                    UIImageView *imgLeftOption1 = [[[UIImageView alloc] init] autorelease];
                    imgLeftOption1.frame = CGRectMake(CGRectGetWidth(leftOption1View.frame) - imgIconSize - defaultItemPadding, CGRectGetHeight(leftOption1View.frame) - imgIconSize - defaultItemPadding, imgIconSize, imgIconSize);
                    imgLeftOption1.image = imgOption;
                    imgLeftOption1.tag = 100;
                    imgLeftOption1.hidden = NO;
                    
                    [leftOption1View addSubview:imgLeftOption1];
                    
                    UIImageView *imgRadioButtonLeftView1 = [[[UIImageView alloc] init] autorelease];
                    imgRadioButtonLeftView1.frame = CGRectMake(defaultItemPadding, defaultItemPadding, imgIconSize, imgIconSize);
                    imgRadioButtonLeftView1.image = imgRadioOn;
                    imgRadioButtonLeftView1.tag = 200;
                    [leftOption1View addSubview:imgRadioButtonLeftView1];
                    
                    UITapGestureRecognizer *tapGestureLeftOpt1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(optionTapped:)];
                    [leftOption1View addGestureRecognizer:tapGestureLeftOpt1];
                    [tapGestureLeftOpt1 release];
                    tapGestureLeftOpt1 = nil;
                    
                    // TODO : SMS 1
                    
                    UIView *centerOption1View = [[[UIView alloc] init] autorelease];
                    centerOption1View.frame = CGRectMake(CGRectGetMaxX(leftHeaderView.frame) + defaultItemPadding, CGRectGetMaxY(leftHeaderView.frame) + defaultItemPadding, frameWidth, frameHeight);
                    centerOption1View.backgroundColor = OPTION_SELECTED_COLOR_BLUE;
                    centerOption1View.tag = 21;
                    
                    UILabel *lblCenterOption1 = [[[UILabel alloc] init]autorelease];
                    lblCenterOption1.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblOptionHeight);
                    lblCenterOption1.textAlignment = NSTextAlignmentCenter;
                    lblCenterOption1.font = optionFont;
                    lblCenterOption1.textColor = [UIColor blackColor];
                    
                    optDict = (NSDictionary *)[matrixModel.sms objectAtIndex:0];
                    onnet = [optDict objectForKey:@"onnet"];
                    offnet = [optDict objectForKey:@"offnet"];
                    
                    if([offnet length] > 0 && [onnet length] > 0)
                        strOpt = [NSString stringWithFormat:@"%@\n%@", [optDict objectForKey:@"onnet"], [optDict objectForKey:@"offnet"]];
                    else
                    {
                        if([offnet length] == 0)
                        {
                            strOpt = onnet;
                        }
                        else
                        {
                            strOpt = offnet;
                        }
                    }
                    
                    lblCenterOption1.text = strOpt;
                    
                    [centerOption1View addSubview:lblCenterOption1];
                    
                    UIImageView *imgCenterOption1 = [[[UIImageView alloc] init] autorelease];
                    imgCenterOption1.frame = CGRectMake(CGRectGetWidth(leftOption1View.frame) - imgIconSize - defaultItemPadding, CGRectGetHeight(leftOption1View.frame) - imgIconSize - defaultItemPadding, imgIconSize, imgIconSize);
                    imgCenterOption1.image = imgOption;
                    imgCenterOption1.tag = 100;
                    imgCenterOption1.hidden = NO;
                    
                    [centerOption1View addSubview:imgCenterOption1];
                    
                    UIImageView *imgRadioButtonCenterView1 = [[[UIImageView alloc] init] autorelease];
                    imgRadioButtonCenterView1.frame = CGRectMake(defaultItemPadding, defaultItemPadding, imgIconSize, imgIconSize);
                    imgRadioButtonCenterView1.image = imgRadioOn;
                    imgRadioButtonCenterView1.tag = 200;
                    [centerOption1View addSubview:imgRadioButtonCenterView1];
                    
                    UITapGestureRecognizer *tapGestureCenterOpt1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(optionTapped:)];
                    [centerOption1View addGestureRecognizer:tapGestureCenterOpt1];
                    [tapGestureCenterOpt1 release];
                    tapGestureCenterOpt1 = nil;
                    
                    // TODO : DATA 1
                    
                    UIView *rightOption1View = [[[UIView alloc] init] autorelease];
                    rightOption1View.frame = CGRectMake(CGRectGetMaxX(centerHeaderView.frame) + defaultItemPadding, CGRectGetMaxY(leftHeaderView.frame) + defaultItemPadding, frameWidth, frameHeight);
                    rightOption1View.backgroundColor = OPTION_SELECTED_COLOR_YELLOW;
                    rightOption1View.tag = 31;
                    
                    UILabel *lblRightOption1 = [[[UILabel alloc] init]autorelease];
                    lblRightOption1.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblOptionHeight);
                    lblRightOption1.textAlignment = NSTextAlignmentCenter;
                    lblRightOption1.font = optionFont;
                    lblRightOption1.textColor = [UIColor blackColor];
                    
                    optDict = (NSDictionary *)[matrixModel.data objectAtIndex:0];
                    onnet = [optDict objectForKey:@"onnet"];
                    offnet = [optDict objectForKey:@"offnet"];
                    
                    if([offnet length] > 0 && [onnet length] > 0)
                        strOpt = [NSString stringWithFormat:@"%@\n%@", [optDict objectForKey:@"onnet"], [optDict objectForKey:@"offnet"]];
                    else
                    {
                        if([offnet length] == 0)
                        {
                            strOpt = onnet;
                        }
                        else
                        {
                            strOpt = offnet;
                        }
                    }
                    
                    lblRightOption1.text = strOpt;
                    
                    [rightOption1View addSubview:lblRightOption1];
                    
                    UIImageView *imgRightOption1 = [[[UIImageView alloc] init] autorelease];
                    imgRightOption1.frame = CGRectMake(CGRectGetWidth(leftOption1View.frame) - imgIconSize - defaultItemPadding, CGRectGetHeight(leftOption1View.frame) - imgIconSize - defaultItemPadding, imgIconSize, imgIconSize);
                    imgRightOption1.image = imgOption;
                    imgRightOption1.tag = 100;
                    imgRightOption1.hidden = NO;
                    
                    [rightOption1View addSubview:imgRightOption1];
                    
                    UIImageView *imgRadioButtonRightView1 = [[[UIImageView alloc] init] autorelease];
                    imgRadioButtonRightView1.frame = CGRectMake(defaultItemPadding, defaultItemPadding, imgIconSize, imgIconSize);
                    imgRadioButtonRightView1.image = imgRadioOn;
                    imgRadioButtonRightView1.tag = 200;
                    [rightOption1View addSubview:imgRadioButtonRightView1];
                    
                    UITapGestureRecognizer *tapGestureRightOpt1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(optionTapped:)];
                    [rightOption1View addGestureRecognizer:tapGestureRightOpt1];
                    [tapGestureRightOpt1 release];
                    tapGestureRightOpt1 = nil;
                    
                    [_radioContainerView addSubview:leftOption1View];
                    [_radioContainerView addSubview:centerOption1View];
                    [_radioContainerView addSubview:rightOption1View];
                    
                    //************************* OPTION 2 **************************//
                    
                    // TODO : VOICE 2
                    
                    UIView *leftOption2View = [[[UIView alloc] init] autorelease];
                    leftOption2View.frame = CGRectMake(leftRightPadding, CGRectGetMaxY(leftOption1View.frame) + defaultItemPadding, frameWidth, frameHeight);
                    leftOption2View.backgroundColor = OPTION_NORMAL_COLOR;
                    leftOption2View.tag = 12;
                    
                    UILabel *lblLeftOption2 = [[[UILabel alloc] init]autorelease];
                    lblLeftOption2.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblOptionHeight);
                    lblLeftOption2.textAlignment = NSTextAlignmentCenter;
                    lblLeftOption2.font = optionFont;
                    lblLeftOption2.textColor = [UIColor blackColor];
                    lblLeftOption2.numberOfLines = 0;
                    
                    optDict = (NSDictionary *)[matrixModel.voice objectAtIndex:1];
                    onnet = [optDict objectForKey:@"onnet"];
                    offnet = [optDict objectForKey:@"offnet"];
                    
                    if([offnet length] > 0 && [onnet length] > 0)
                        strOpt = [NSString stringWithFormat:@"%@\n%@", [optDict objectForKey:@"onnet"], [optDict objectForKey:@"offnet"]];
                    else
                    {
                        if([offnet length] == 0)
                        {
                            strOpt = onnet;
                        }
                        else
                        {
                            strOpt = offnet;
                        }
                    }
                    
                    lblLeftOption2.text = strOpt;
                    
                    [leftOption2View addSubview:lblLeftOption2];
                    
                    UIImageView *imgLeftOption2 = [[[UIImageView alloc] init] autorelease];
                    imgLeftOption2.frame = CGRectMake(CGRectGetWidth(leftOption1View.frame) - imgIconSize - defaultItemPadding, CGRectGetHeight(leftOption1View.frame) - imgIconSize - defaultItemPadding, imgIconSize, imgIconSize);
                    imgLeftOption2.image = imgOption;
                    imgLeftOption2.tag = 100;
                    imgLeftOption2.hidden = YES;
                    
                    [leftOption2View addSubview:imgLeftOption2];
                    
                    UIImageView *imgRadioButtonLeftView2 = [[[UIImageView alloc] init] autorelease];
                    imgRadioButtonLeftView2.frame = CGRectMake(defaultItemPadding, defaultItemPadding, imgIconSize, imgIconSize);
                    imgRadioButtonLeftView2.image = imgRadioOff;
                    imgRadioButtonLeftView2.tag = 200;
                    [leftOption2View addSubview:imgRadioButtonLeftView2];
                    
                    UITapGestureRecognizer *tapGestureLeftOpt2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(optionTapped:)];
                    [leftOption2View addGestureRecognizer:tapGestureLeftOpt2];
                    [tapGestureLeftOpt2 release];
                    tapGestureLeftOpt2 = nil;
                    
                    
                    // TODO : SMS 2
                    UIView *centerOption2View = [[[UIView alloc] init] autorelease];
                    centerOption2View.frame = CGRectMake(CGRectGetMaxX(leftOption1View.frame) + defaultItemPadding, CGRectGetMaxY(leftOption1View.frame) + defaultItemPadding, frameWidth, frameHeight);
                    centerOption2View.backgroundColor = OPTION_NORMAL_COLOR;
                    centerOption2View.tag = 22;
                    
                    UILabel *lblCenterOption2 = [[[UILabel alloc] init]autorelease];
                    lblCenterOption2.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblOptionHeight);
                    lblCenterOption2.textAlignment = NSTextAlignmentCenter;
                    lblCenterOption2.font = optionFont;
                    lblCenterOption2.numberOfLines = 0;
                    lblCenterOption2.textColor = [UIColor blackColor];
                    
                    optDict = (NSDictionary *)[matrixModel.sms objectAtIndex:1];
                    onnet = [optDict objectForKey:@"onnet"];
                    offnet = [optDict objectForKey:@"offnet"];
                    
                    if([offnet length] > 0 && [onnet length] > 0)
                        strOpt = [NSString stringWithFormat:@"%@\n%@", [optDict objectForKey:@"onnet"], [optDict objectForKey:@"offnet"]];
                    else
                    {
                        if([offnet length] == 0)
                        {
                            strOpt = onnet;
                        }
                        else
                        {
                            strOpt = offnet;
                        }
                    }
                    
                    lblCenterOption2.text = strOpt;
                    
                    [centerOption2View addSubview:lblCenterOption2];
                    
                    UIImageView *imgCenterOption2 = [[[UIImageView alloc] init] autorelease];
                    imgCenterOption2.frame = CGRectMake(CGRectGetWidth(leftOption1View.frame) - imgIconSize - defaultItemPadding, CGRectGetHeight(leftOption1View.frame) - imgIconSize - defaultItemPadding, imgIconSize, imgIconSize);
                    imgCenterOption2.image = imgOption;
                    imgCenterOption2.tag = 100;
                    imgCenterOption2.hidden = YES;
                    
                    [centerOption2View addSubview:imgCenterOption2];
                    
                    UIImageView *imgRadioButtonCenterView2 = [[[UIImageView alloc] init] autorelease];
                    imgRadioButtonCenterView2.frame = CGRectMake(defaultItemPadding, defaultItemPadding, imgIconSize, imgIconSize);
                    imgRadioButtonCenterView2.image = imgRadioOff;
                    imgRadioButtonCenterView2.tag = 200;
                    [centerOption2View addSubview:imgRadioButtonCenterView2];
                    
                    UITapGestureRecognizer *tapGestureCenterOpt2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(optionTapped:)];
                    [centerOption2View addGestureRecognizer:tapGestureCenterOpt2];
                    [tapGestureCenterOpt2 release];
                    tapGestureCenterOpt2 = nil;
                    
                    
                    // TODO : DATA 2
                    UIView *rightOption2View = [[[UIView alloc] init] autorelease];
                    rightOption2View.frame = CGRectMake(CGRectGetMaxX(centerOption2View.frame) + defaultItemPadding, CGRectGetMaxY(leftOption1View.frame) + defaultItemPadding, frameWidth, frameHeight);
                    rightOption2View.backgroundColor = OPTION_NORMAL_COLOR;
                    rightOption2View.tag = 32;
                    
                    UILabel *lblRightOption2 = [[[UILabel alloc] init]autorelease];
                    lblRightOption2.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblOptionHeight);
                    lblRightOption2.textAlignment = NSTextAlignmentCenter;
                    lblRightOption2.font = optionFont;
                    lblRightOption2.textColor = [UIColor blackColor];
                    lblRightOption2.numberOfLines = 0;
                    
                    optDict = (NSDictionary *)[matrixModel.data objectAtIndex:1];
                    onnet = [optDict objectForKey:@"onnet"];
                    offnet = [optDict objectForKey:@"offnet"];
                    
                    if([offnet length] > 0 && [onnet length] > 0)
                        strOpt = [NSString stringWithFormat:@"%@\n%@", [optDict objectForKey:@"onnet"], [optDict objectForKey:@"offnet"]];
                    else
                    {
                        if([offnet length] == 0)
                        {
                            strOpt = onnet;
                        }
                        else
                        {
                            strOpt = offnet;
                        }
                    }
                    
                    lblRightOption2.text = strOpt;
                    
                    [rightOption2View addSubview:lblRightOption2];
                    
                    UIImageView *imgRightOption2 = [[[UIImageView alloc] init] autorelease];
                    imgRightOption2.frame = CGRectMake(CGRectGetWidth(leftOption1View.frame) - imgIconSize - defaultItemPadding, CGRectGetHeight(leftOption1View.frame) - imgIconSize - defaultItemPadding, imgIconSize, imgIconSize);
                    imgRightOption2.image = imgOption;
                    imgRightOption2.tag = 100;
                    imgRightOption2.hidden = YES;
                    
                    [rightOption2View addSubview:imgRightOption2];
                    
                    UIImageView *imgRadioButtonRightView2 = [[[UIImageView alloc] init] autorelease];
                    imgRadioButtonRightView2.frame = CGRectMake(defaultItemPadding, defaultItemPadding, imgIconSize, imgIconSize);
                    imgRadioButtonRightView2.image = imgRadioOff;
                    imgRadioButtonRightView2.tag = 200;
                    [rightOption2View addSubview:imgRadioButtonRightView2];
                    
                    UITapGestureRecognizer *tapGestureRightOpt2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(optionTapped:)];
                    [rightOption2View addGestureRecognizer:tapGestureRightOpt2];
                    [tapGestureRightOpt2 release];
                    tapGestureRightOpt2 = nil;
                    
                    [_radioContainerView addSubview:leftOption2View];
                    [_radioContainerView addSubview:centerOption2View];
                    [_radioContainerView addSubview:rightOption2View];
                    
                    //************************* OPTION 3 **************************//
                    
                    // TODO : VOICE 3
                    UIView *leftOption3View = [[[UIView alloc] init] autorelease];
                    leftOption3View.frame = CGRectMake(leftRightPadding, CGRectGetMaxY(leftOption2View.frame) + defaultItemPadding, frameWidth, frameHeight);
                    leftOption3View.backgroundColor = OPTION_NORMAL_COLOR;
                    leftOption3View.tag = 13;
                    
                    UILabel *lblLeftOption3 = [[[UILabel alloc] init]autorelease];
                    lblLeftOption3.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblOptionHeight);
                    lblLeftOption3.textAlignment = NSTextAlignmentCenter;
                    lblLeftOption3.font = optionFont;
                    lblLeftOption3.textColor = [UIColor blackColor];
                    lblLeftOption3.numberOfLines = 0;
                    
                    optDict = (NSDictionary *)[matrixModel.voice objectAtIndex:2];
                    onnet = [optDict objectForKey:@"onnet"];
                    offnet = [optDict objectForKey:@"offnet"];
                    
                    if([offnet length] > 0 && [onnet length] > 0)
                        strOpt = [NSString stringWithFormat:@"%@\n%@", [optDict objectForKey:@"onnet"], [optDict objectForKey:@"offnet"]];
                    else
                    {
                        if([offnet length] == 0)
                        {
                            strOpt = onnet;
                        }
                        else
                        {
                            strOpt = offnet;
                        }
                    }
                    
                    lblLeftOption3.text = strOpt;
                    
                    [leftOption3View addSubview:lblLeftOption3];
                    
                    UITapGestureRecognizer *tapGestureLeftOpt3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(optionTapped:)];
                    [leftOption3View addGestureRecognizer:tapGestureLeftOpt3];
                    [tapGestureLeftOpt3 release];
                    tapGestureLeftOpt3 = nil;
                    
                    UIImageView *imgLeftOption3 = [[[UIImageView alloc] init] autorelease];
                    imgLeftOption3.frame = CGRectMake(CGRectGetWidth(leftOption1View.frame) - imgIconSize - defaultItemPadding, CGRectGetHeight(leftOption1View.frame) - imgIconSize - defaultItemPadding, imgIconSize, imgIconSize);
                    imgLeftOption3.image = imgOption;
                    imgLeftOption3.tag = 100;
                    imgLeftOption3.hidden = YES;
                    
                    [leftOption3View addSubview:imgLeftOption3];
                    
                    UIImageView *imgRadioButtonLeftView3 = [[[UIImageView alloc] init] autorelease];
                    imgRadioButtonLeftView3.frame = CGRectMake(defaultItemPadding, defaultItemPadding, imgIconSize, imgIconSize);
                    imgRadioButtonLeftView3.image = imgRadioOff;
                    imgRadioButtonLeftView3.tag = 200;
                    [leftOption3View addSubview:imgRadioButtonLeftView3];
                    
                    // TODO : SMS 3
                    UIView *centerOption3View = [[[UIView alloc] init] autorelease];
                    centerOption3View.frame = CGRectMake(CGRectGetMaxX(leftOption1View.frame) + defaultItemPadding, CGRectGetMaxY(leftOption2View.frame) + defaultItemPadding, frameWidth, frameHeight);
                    centerOption3View.backgroundColor = OPTION_NORMAL_COLOR;
                    centerOption3View.tag = 23;
                    
                    UILabel *lblCenterOption3 = [[[UILabel alloc] init]autorelease];
                    lblCenterOption3.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblOptionHeight);
                    lblCenterOption3.textAlignment = NSTextAlignmentCenter;
                    lblCenterOption3.font = optionFont;
                    lblCenterOption3.numberOfLines = 0;
                    lblCenterOption3.textColor = [UIColor blackColor];
                    
                    optDict = (NSDictionary *)[matrixModel.sms objectAtIndex:2];
                    onnet = [optDict objectForKey:@"onnet"];
                    offnet = [optDict objectForKey:@"offnet"];
                    
                    if([offnet length] > 0 && [onnet length] > 0)
                        strOpt = [NSString stringWithFormat:@"%@\n%@", [optDict objectForKey:@"onnet"], [optDict objectForKey:@"offnet"]];
                    else
                    {
                        if([offnet length] == 0)
                        {
                            strOpt = onnet;
                        }
                        else
                        {
                            strOpt = offnet;
                        }
                    }
                    
                    lblCenterOption3.text = strOpt;
                    
                    [centerOption3View addSubview:lblCenterOption3];
                    
                    UIImageView *imgCenterOption3 = [[[UIImageView alloc] init] autorelease];
                    imgCenterOption3.frame = CGRectMake(CGRectGetWidth(leftOption1View.frame) - imgIconSize - defaultItemPadding, CGRectGetHeight(leftOption1View.frame) - imgIconSize - defaultItemPadding, imgIconSize, imgIconSize);
                    imgCenterOption3.image = imgOption;
                    imgCenterOption3.tag = 100;
                    imgCenterOption3.hidden = YES;
                    
                    [centerOption3View addSubview:imgCenterOption3];
                    
                    UIImageView *imgRadioButtonCenterView3 = [[[UIImageView alloc] init] autorelease];
                    imgRadioButtonCenterView3.frame = CGRectMake(defaultItemPadding, defaultItemPadding, imgIconSize, imgIconSize);
                    imgRadioButtonCenterView3.image = imgRadioOff;
                    imgRadioButtonCenterView3.tag = 200;
                    [centerOption3View addSubview:imgRadioButtonCenterView3];
                    
                    UITapGestureRecognizer *tapGestureCenterOpt3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(optionTapped:)];
                    [centerOption3View addGestureRecognizer:tapGestureCenterOpt3];
                    [tapGestureCenterOpt3 release];
                    tapGestureCenterOpt3 = nil;
                    
                    // TODO : DATA 3
                    UIView *rightOption3View = [[[UIView alloc] init] autorelease];
                    rightOption3View.frame = CGRectMake(CGRectGetMaxX(centerOption2View.frame) + defaultItemPadding, CGRectGetMaxY(leftOption2View.frame) + defaultItemPadding, frameWidth, frameHeight);
                    rightOption3View.backgroundColor = OPTION_NORMAL_COLOR;
                    rightOption3View.tag = 33;
                    
                    UILabel *lblRightOption3 = [[[UILabel alloc] init]autorelease];
                    lblRightOption3.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblOptionHeight);
                    lblRightOption3.textAlignment = NSTextAlignmentCenter;
                    lblRightOption3.font = optionFont;
                    lblRightOption3.textColor = [UIColor blackColor];
                    lblRightOption3.numberOfLines = 0;
                    
                    optDict = (NSDictionary *)[matrixModel.data objectAtIndex:2];
                    onnet = [optDict objectForKey:@"onnet"];
                    offnet = [optDict objectForKey:@"offnet"];
                    
                    if([offnet length] > 0 && [onnet length] > 0)
                        strOpt = [NSString stringWithFormat:@"%@\n%@", [optDict objectForKey:@"onnet"], [optDict objectForKey:@"offnet"]];
                    else
                    {
                        if([offnet length] == 0)
                        {
                            strOpt = onnet;
                        }
                        else
                        {
                            strOpt = offnet;
                        }
                    }
                    
                    lblRightOption3.text = strOpt;
                    
                    [rightOption3View addSubview:lblRightOption3];
                    
                    UIImageView *imgRightOption3 = [[[UIImageView alloc] init] autorelease];
                    imgRightOption3.frame = CGRectMake(CGRectGetWidth(leftOption1View.frame) - imgIconSize - defaultItemPadding, CGRectGetHeight(leftOption1View.frame) - imgIconSize - defaultItemPadding, imgIconSize, imgIconSize);
                    imgRightOption3.image = imgOption;
                    imgRightOption3.tag = 100;
                    imgRightOption3.hidden = YES;
                    
                    [rightOption3View addSubview:imgRightOption3];
                    
                    UIImageView *imgRadioButtonRightView3 = [[[UIImageView alloc] init] autorelease];
                    imgRadioButtonRightView3.frame = CGRectMake(defaultItemPadding, defaultItemPadding, imgIconSize, imgIconSize);
                    imgRadioButtonRightView3.image = imgRadioOff;
                    imgRadioButtonRightView3.tag = 200;
                    [rightOption3View addSubview:imgRadioButtonRightView3];
                    
                    UITapGestureRecognizer *tapGestureRightOpt3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(optionTapped:)];
                    [rightOption3View addGestureRecognizer:tapGestureRightOpt3];
                    [tapGestureRightOpt3 release];
                    tapGestureRightOpt3 = nil;
                    
                    [_radioContainerView addSubview:leftOption3View];
                    [_radioContainerView addSubview:centerOption3View];
                    [_radioContainerView addSubview:rightOption3View];
                    
                    //************************* OPTION 4 **************************//
                    
                    // TODO : VOICE 4
                    UIView *leftOption4View = [[[UIView alloc] init] autorelease];
                    leftOption4View.frame = CGRectMake(leftRightPadding, CGRectGetMaxY(leftOption3View.frame) + defaultItemPadding, frameWidth, frameHeight);
                    leftOption4View.backgroundColor = OPTION_NORMAL_COLOR;
                    leftOption4View.tag = 14;
                    
                    UILabel *lblLeftOption4 = [[[UILabel alloc] init]autorelease];
                    lblLeftOption4.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblOptionHeight);
                    lblLeftOption4.textAlignment = NSTextAlignmentCenter;
                    lblLeftOption4.font = optionFont;
                    lblLeftOption4.textColor = [UIColor blackColor];
                    lblLeftOption4.numberOfLines = 0;
                    
                    optDict = (NSDictionary *)[matrixModel.voice objectAtIndex:3];
                    onnet = [optDict objectForKey:@"onnet"];
                    offnet = [optDict objectForKey:@"offnet"];
                    
                    if([offnet length] > 0 && [onnet length] > 0)
                        strOpt = [NSString stringWithFormat:@"%@\n%@", [optDict objectForKey:@"onnet"], [optDict objectForKey:@"offnet"]];
                    else
                    {
                        if([offnet length] == 0)
                        {
                            strOpt = onnet;
                        }
                        else
                        {
                            strOpt = offnet;
                        }
                    }
                    
                    lblLeftOption4.text = strOpt;
                    
                    [leftOption4View addSubview:lblLeftOption4];
                    
                    UIImageView *imgLeftOption4 = [[[UIImageView alloc] init] autorelease];
                    imgLeftOption4.frame = CGRectMake(CGRectGetWidth(leftOption1View.frame) - imgIconSize - defaultItemPadding, CGRectGetHeight(leftOption1View.frame) - imgIconSize - defaultItemPadding, imgIconSize, imgIconSize);
                    imgLeftOption4.image = imgOption;
                    imgLeftOption4.tag = 100;
                    imgLeftOption4.hidden = YES;
                    
                    [leftOption4View addSubview:imgLeftOption4];
                    
                    UIImageView *imgRadioButtonLeftView4 = [[[UIImageView alloc] init] autorelease];
                    imgRadioButtonLeftView4.frame = CGRectMake(defaultItemPadding, defaultItemPadding, imgIconSize, imgIconSize);
                    imgRadioButtonLeftView4.image = imgRadioOff;
                    imgRadioButtonLeftView4.tag = 200;
                    [leftOption4View addSubview:imgRadioButtonLeftView4];
                    
                    UITapGestureRecognizer *tapGestureLeftOpt4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(optionTapped:)];
                    [leftOption4View addGestureRecognizer:tapGestureLeftOpt4];
                    [tapGestureLeftOpt4 release];
                    tapGestureLeftOpt4 = nil;
                    
                    // TODO : SMS 4
                    UIView *centerOption4View = [[[UIView alloc] init] autorelease];
                    centerOption4View.frame = CGRectMake(CGRectGetMaxX(leftOption1View.frame) + defaultItemPadding, CGRectGetMaxY(leftOption3View.frame) + defaultItemPadding, frameWidth, frameHeight);
                    centerOption4View.backgroundColor = OPTION_NORMAL_COLOR;
                    centerOption4View.tag = 24;
                    
                    UILabel *lblCenterOption4 = [[[UILabel alloc] init]autorelease];
                    lblCenterOption4.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblOptionHeight);
                    lblCenterOption4.textAlignment = NSTextAlignmentCenter;
                    lblCenterOption4.font = optionFont;
                    lblCenterOption4.numberOfLines = 0;
                    lblCenterOption4.textColor = [UIColor blackColor];
                    
                    optDict = (NSDictionary *)[matrixModel.sms objectAtIndex:3];
                    onnet = [optDict objectForKey:@"onnet"];
                    offnet = [optDict objectForKey:@"offnet"];
                    
                    if([offnet length] > 0 && [onnet length] > 0)
                        strOpt = [NSString stringWithFormat:@"%@\n%@", [optDict objectForKey:@"onnet"], [optDict objectForKey:@"offnet"]];
                    else
                    {
                        if([offnet length] == 0)
                        {
                            strOpt = onnet;
                        }
                        else
                        {
                            strOpt = offnet;
                        }
                    }
                    
                    lblCenterOption4.text = strOpt;
                    
                    [centerOption4View addSubview:lblCenterOption4];
                    
                    UIImageView *imgCenterOption4 = [[[UIImageView alloc] init] autorelease];
                    imgCenterOption4.frame = CGRectMake(CGRectGetWidth(leftOption1View.frame) - imgIconSize - defaultItemPadding, CGRectGetHeight(leftOption1View.frame) - imgIconSize - defaultItemPadding, imgIconSize, imgIconSize);
                    imgCenterOption4.image = imgOption;
                    imgCenterOption4.tag = 100;
                    imgCenterOption4.hidden = YES;
                    
                    [centerOption4View addSubview:imgCenterOption4];
                    
                    UIImageView *imgRadioButtonCenterView4 = [[[UIImageView alloc] init] autorelease];
                    imgRadioButtonCenterView4.frame = CGRectMake(defaultItemPadding, defaultItemPadding, imgIconSize, imgIconSize);
                    imgRadioButtonCenterView4.image = imgRadioOff;
                    imgRadioButtonCenterView4.tag = 200;
                    [centerOption4View addSubview:imgRadioButtonCenterView4];
                    
                    UITapGestureRecognizer *tapGestureCenterOpt4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(optionTapped:)];
                    [centerOption4View addGestureRecognizer:tapGestureCenterOpt4];
                    [tapGestureCenterOpt4 release];
                    tapGestureCenterOpt4 = nil;
                    
                    //TODO : DATA 4
                    UIView *rightOption4View = [[[UIView alloc] init] autorelease];
                    rightOption4View.frame = CGRectMake(CGRectGetMaxX(centerOption2View.frame) + defaultItemPadding, CGRectGetMaxY(leftOption3View.frame) + defaultItemPadding, frameWidth, frameHeight);
                    rightOption4View.backgroundColor = OPTION_NORMAL_COLOR;
                    rightOption4View.tag = 34;
                    
                    UILabel *lblRightOption4 = [[[UILabel alloc] init]autorelease];
                    lblRightOption4.frame = CGRectMake(leftRightPadding, leftRightPadding, CGRectGetWidth(leftHeaderView.frame) - (leftRightPadding*2), lblOptionHeight);
                    lblRightOption4.textAlignment = NSTextAlignmentCenter;
                    lblRightOption4.font = optionFont;
                    lblRightOption4.textColor = [UIColor blackColor];
                    lblRightOption4.numberOfLines = 0;
                    
                    optDict = (NSDictionary *)[matrixModel.data objectAtIndex:3];
                    onnet = [optDict objectForKey:@"onnet"];
                    offnet = [optDict objectForKey:@"offnet"];
                    
                    if([offnet length] > 0 && [onnet length] > 0)
                        strOpt = [NSString stringWithFormat:@"%@\n%@", [optDict objectForKey:@"onnet"], [optDict objectForKey:@"offnet"]];
                    else
                    {
                        if([offnet length] == 0)
                        {
                            strOpt = onnet;
                        }
                        else
                        {
                            strOpt = offnet;
                        }
                    }
                    
                    lblRightOption4.text = strOpt;
                    
                    [rightOption4View addSubview:lblRightOption4];
                    
                    UIImageView *imgRightOption4 = [[[UIImageView alloc] init] autorelease];
                    imgRightOption4.frame = CGRectMake(CGRectGetWidth(leftOption1View.frame) - imgIconSize - defaultItemPadding, CGRectGetHeight(leftOption1View.frame) - imgIconSize - defaultItemPadding, imgIconSize, imgIconSize);
                    imgRightOption4.image = imgOption;
                    imgRightOption4.tag = 100;
                    imgRightOption4.hidden = YES;
                    
                    [rightOption4View addSubview:imgRightOption4];
                    
                    UIImageView *imgRadioButtonRightView4 = [[[UIImageView alloc] init] autorelease];
                    imgRadioButtonRightView4.frame = CGRectMake(defaultItemPadding, defaultItemPadding, imgIconSize, imgIconSize);
                    imgRadioButtonRightView4.image = imgRadioOff;
                    imgRadioButtonRightView4.tag = 200;
                    [rightOption4View addSubview:imgRadioButtonRightView4];
                    
                    UITapGestureRecognizer *tapGestureRightOpt4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(optionTapped:)];
                    [rightOption4View addGestureRecognizer:tapGestureRightOpt4];
                    [tapGestureRightOpt4 release];
                    tapGestureRightOpt4 = nil;
                    
                    [_radioContainerView addSubview:leftOption4View];
                    [_radioContainerView addSubview:centerOption4View];
                    [_radioContainerView addSubview:rightOption4View];
                    
                    _radioContainerView.frame = CGRectMake(CGRectGetMinX(_radioContainerView.frame), CGRectGetMinY(_radioContainerView.frame), CGRectGetWidth(_radioContainerView.frame),CGRectGetMaxY(leftOption4View.frame) + 20);
                    
                    _barContainer.frame = CGRectMake(CGRectGetMinX(_barContainer.frame), CGRectGetMaxY(_radioContainerView.frame) + 20, CGRectGetWidth(_barContainer.frame), CGRectGetHeight(_barContainer.frame));
                    
                    // Slide View
                    CGFloat startY = 0.0;
                    CGFloat sliderHeight = 100.0;
                    NSMutableArray *quotaBarDataTmp = [[NSMutableArray alloc] init];
                    for (int i = 0; i < [sharedData.quotaCalculatorData.listUsage count]; i++)
                    {
                        InternetUsageModel *model = [sharedData.quotaCalculatorData.listUsage objectAtIndex:i];
                        [quotaBarDataTmp addObject:model];
                        
                        CGRect sliderFrame = CGRectMake(0, startY, _sliderContainerView.frame.size.width, sliderHeight);
                        QuotaSliderView *sliderView = [[QuotaSliderView alloc] initWithFrame:sliderFrame withData:model isQuotaCalculatorView:NO];
                        sliderView.delegate = self;
                        [_sliderContainerView addSubview:sliderView];
                        startY += sliderHeight;
                        //        }
                    }
                    
                    CGRect sliderContainerFrame = _sliderContainerView.frame;
                    sliderContainerFrame.size.height = sliderHeight * [sharedData.quotaCalculatorData.listUsage count];
                    sliderContainerFrame.origin.y = CGRectGetMaxY(_barContainer.frame) + 20.0;
                    _sliderContainerView.frame = sliderContainerFrame;
                    
                    //        ---------------------------//
                    //         Quota Calculator Bar View //
                    //        ---------------------------//
                    NSArray *quotaBarData = [NSArray arrayWithArray:quotaBarDataTmp];
                    CGRect barFrame = CGRectMake(0, 0, _barContainer.frame.size.width, _barContainer.frame.size.height);
                    QuotaCalculatorBar *barView = [[QuotaCalculatorBar alloc] initWithFrame:barFrame withData:quotaBarData];
                    _barViewObject = barView;
                    [_barContainer addSubview:_barViewObject];
                    
                    //------------------//
                    // Setup Field View //
                    //------------------//
                    _fieldView.backgroundColor = [UIColor clearColor];
                    CGRect newFrame = _fieldView.frame;
                    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
                    
                    // TODO : Calculator View
                    _footerView.frame = CGRectMake(CGRectGetMinX(_footerView.frame), CGRectGetMaxY(_sliderContainerView.frame), CGRectGetWidth(_footerView.frame), CGRectGetHeight(_footerView.frame));
                    
                    newFrame.size.height = CGRectGetMaxY(_footerView.frame) + 20.0;
                    _fieldView.frame = newFrame;
                    
                    //--------------//
                    // Setup Button //
                    //--------------//
                    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
                    //    NSString *buttonText = sharedData.quotaCalculatorData.submitText;
                    NSString *buttonText = sharedData.matrixSukaSukaData.label_submit;
                    UIButton *submitBtn = createButtonWithFrame(CGRectMake(CGRectGetWidth(self.view.frame) - 100 - 20,
                                                                           CGRectGetMaxY(_fieldView.frame) + kDefaultComponentPadding,
                                                                           100,
                                                                           kDefaultButtonHeight),
                                                                [buttonText uppercaseString],
                                                                buttonFont,
                                                                kDefaultWhiteColor,
                                                                kDefaultAquaMarineColor,
                                                                kDefaultBlueColor);
                    [submitBtn addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
                    
                    _submitButton = submitBtn;
                    [_contentView addSubview:_submitButton];
                    
                    //--------------//
                    // Content View //
                    //--------------//
                    
                    // TODO : Calculator View
                    newFrame = CGRectMake(CGRectGetMinX(newFrame), CGRectGetMinY(newFrame), CGRectGetWidth(newFrame), CGRectGetHeight(newFrame) + CGRectGetHeight(_footerView.frame));
                    
                    newFrame = _contentView.frame;
                    
                    // TODO : Before Calculator View
                    newFrame.size.height = CGRectGetMaxY(_submitButton.frame) + 20;
                    _contentView.frame = newFrame;
                }
            }
        }
        _contentView.backgroundColor = kDefaultWhiteColor;
        
        _scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height);
        
        [headerView1 release];
        [headerView2 release];
        headerView1 = nil;
        headerView2 = nil;
    }
}

- (void)setupTotalLabelValue
{
    @try
    {
        DataManager *sharedData = [DataManager sharedInstance];
        if(sharedData.quotaCalculatorData)
        {
            NSString *currentSet = @"";
            double totalValue = 0.0;
            for (int i = 0; i < [sharedData.quotaCalculatorData.listUsage count]; i++) {
                InternetUsageModel *model = [sharedData.quotaCalculatorData.listUsage objectAtIndex:i];
                totalValue = totalValue + (model.sliderValue/model.maxScalePerdayValue*model.maxVolumeValue);
            }
            
            NSString *maxQuota = @"";
            maxQuota = sharedData.quotaCalculatorData.totalVolumeMonthlyString;
            _planQuota = totalValue*30; // --> in kB monthly
            
            // convert to bytes
            totalValue = _planQuota*1024;
            NSNumber *totalNumber = [NSNumber numberWithDouble:totalValue];
            currentSet = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:totalNumber];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

- (void)setupLabelValue {
    DataManager *sharedData = [DataManager sharedInstance];
    
//    _lblInternetPackageUsage.text = @"HotRod 3G 5GB 30Hr";
    [self setupInternetPackageLabel];
    
    //_lblQuotaMeterTitle.text = @"Rata-rata penggunaan data harian :";
    
    NSString *currentSet = @"";
    double totalValue = 0.0;
    for (int i = 0; i < [sharedData.quotaCalculatorData.listUsage count]; i++) {
        InternetUsageModel *model = [sharedData.quotaCalculatorData.listUsage objectAtIndex:i];
        totalValue = totalValue + (model.sliderValue/model.maxScalePerdayValue*model.maxVolumeValue);
    }
    
    NSString *maxQuota = @"";
    
//    maxQuota = sharedData.quotaCalculatorData.totalVolumeDailyString;
//    _planQuota = totalValue;
    
    maxQuota = sharedData.quotaCalculatorData.totalVolumeMonthlyString;
    _planQuota = totalValue*30; // --> in kB
    
    // convert to bytes
    totalValue = _planQuota*1024;
    NSNumber *totalNumber = [NSNumber numberWithDouble:totalValue];
    currentSet = [HumanReadableDataSizeHelper axisHumanReadableSizeFromBytes:totalNumber];
    //_lblTotal.text = [NSString stringWithFormat:@"Total %@/%@",currentSet,maxQuota];
//    _lblEstimationQuota.text = [NSString stringWithFormat:@"Total %@/%@",currentSet,maxQuota];
    
    _lblEstimationQuota.text = [NSString stringWithFormat:@"Total %@/%@",currentSet,[Language get:@"30days" alter:nil]];
    [self updateInternetDataOptionViewWithValue:totalValue];
//    _lblActivePeriod.text = @"21 Nov 2015 s/d 20 Des 2015";
}

- (void)setupInternetPackageLabel {
    if (_isCheckingPackage) {
        _lblInternetPackageUsage.text = [Language get:@"checking" alter:nil];
    }
    else {
        DataManager *sharedData = [DataManager sharedInstance];
        if([sharedData.checkUsageXLData count] > 0)
        {
            CheckUsageXLModel *usageModel = [sharedData.checkUsageXLData objectAtIndex:0];
            if(usageModel)
                _lblInternetPackageUsage.text = usageModel.name;
            else
                _lblInternetPackageUsage.text = [Language get:@"no_package" alter:nil];
        }
        else {
            _lblInternetPackageUsage.text = [Language get:@"no_package" alter:nil];
        }
    }
}

-(void)performSubmit:(id)sender
{
    [self mappingPaketSukaSuka];
    _submitButton.enabled = NO;
}

#pragma mark - API Request
-(void)getMyPackage
{
    @try
    {
        //        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = CHECK_USAGE_XL_REQ;
        [request checkInternetUsageXL];
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

-(void)getPackageMatrix
{
    @try
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = MATRIX_PAKET_SUKASUKA_REQ;
        [request getPackageSukaMatrix];
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

-(void)getUsageSimulatorData
{
    @try
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = USAGE_SIMULATOR_SUKASUKA_REQ;
        [request usageSimulatorSukaSuka];
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

-(void)mappingPaketSukaSuka
{
    @try
    {
        //        if(_internetOpt > 0 && _smsOpt > 0 && _voiceOpt > 0)
        if(_internetOpt > 0 || (_smsOpt > 0 && _voiceOpt > 0))
        {
            self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            self.hud.labelText = [Language get:@"loading" alter:nil];
            
            AXISnetRequest *request = [AXISnetRequest sharedInstance];
            request.delegate = self;
            request.requestType = MAPPING_PAKET_SUKASUKA_REQ;
            [request mappingSukaSukaWithPaketSMS:[NSString stringWithFormat:@"%ld", (long)_smsOpt] andVoice:[NSString stringWithFormat:@"%ld", (long)_voiceOpt] andInternet:[NSString stringWithFormat:@"%ld", (long)_internetOpt]];
        }
        else
        {
            _submitButton.enabled = YES;
            DataManager *sharedData = [DataManager sharedInstance];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:sharedData.matrixSukaSukaData.notif delegate:self cancelButtonTitle:[Language get:@"ok" alter:nil] otherButtonTitles:nil, nil];
            [alertView show];
            [alertView release];
            alertView = nil;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

#pragma mark - AXISnetRequest Delegate
-(void)requestDoneWithInfo:(NSDictionary *)result
{
    [self.hud hide:YES];
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    
    NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
    int type = [typeNumber intValue];
    
    BOOL success = [reqResult boolValue];
    if (success)
    {
        if(type == MATRIX_PAKET_SUKASUKA_REQ)
        {
//            _fieldView.hidden = NO;
//            [self setupUILayoutWithMatrix:YES];
//            [self getMyPackage];
            // TODO : Usage Simulator
            [self getUsageSimulatorData];
        }
        else if(type == CHECK_USAGE_XL_REQ)
        {
            /*
            DataManager *sharedData = [DataManager sharedInstance];
            if([sharedData.checkUsageXLData count] > 0)
            {
                CheckUsageXLModel *usageModel = [sharedData.checkUsageXLData objectAtIndex:0];
                if(usageModel)
                    _lblInternetPackageUsage.text = usageModel.name;
                else
                    _lblInternetPackageUsage.text = [Language get:@"no_package" alter:nil];
            }*/
            
            _isCheckingPackage = NO;
            [self setupInternetPackageLabel];
        }
        else if(type == MAPPING_PAKET_SUKASUKA_REQ)
        {
            _submitButton.enabled = YES;
            PaketSukaNameViewController *packageViewController = [[PaketSukaNameViewController alloc] initWithNibName:@"PaketSukaNameViewController" bundle:nil];
            
            [self.navigationController pushViewController:packageViewController animated:YES];
            packageViewController = nil;
            [packageViewController release];
        }
        else if(type == USAGE_SIMULATOR_SUKASUKA_REQ)
        {
            [self setupUILayoutWithMatrix:YES];
            [self setupLabelValue];
            [self setupTotalLabelValue];
            _fieldView.hidden = NO;
            [self getMyPackage];
        }
        else if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else
    {
        
        if(type == CHECK_USAGE_XL_REQ)
        {
            //NSLog(@"update internet label");
            _isCheckingPackage = NO;
            [self setupInternetPackageLabel];
        }
        else if (type == MATRIX_PAKET_SUKASUKA_REQ)
        {
           
            [self setupUILayoutWithMatrix:NO];
        }
        else if (type == MAPPING_PAKET_SUKASUKA_REQ)
        {
            _submitButton.enabled = YES;
        }
    }
}

#pragma mark - Gesture Recognizer
-(void)optionTapped:(UIGestureRecognizer *)recognizer
{
    [self markSelectedOptionView:recognizer.view];
}

-(void)updateInternetDataOptionViewWithValue:(double)value
{
    @try
    {
        double realValue = value/1000;
        DataManager *sharedData = [DataManager sharedInstance];
        if(sharedData.matrixSukaSukaData.data)
        {
            for (int i = 0; i < [sharedData.matrixSukaSukaData.data count]; i++) {
                if(i < [sharedData.matrixSukaSukaData.data count] -1)
                {
                    NSDictionary *dataDict1 = [sharedData.matrixSukaSukaData.data objectAtIndex:i];
                    NSDictionary *dataDict2 = [sharedData.matrixSukaSukaData.data objectAtIndex:i+1];
                    double onnetValue1 = [[dataDict1 valueForKey:@"onnet_val"] doubleValue];
                    double onnetValue2 = [[dataDict2 valueForKey:@"onnet_val"] doubleValue];
                    
                    if(realValue > onnetValue1 && realValue < onnetValue2)
                    {
                        NSInteger viewTag = (i+1) + 31;
                        UIView *view = [_radioContainerView viewWithTag:viewTag];
                        if(view)
                        {
                            [self markSelectedOptionView:view];
                        }
                    }
                    else if(realValue == 0)
                    {
                        NSInteger viewTag = 31;
                        UIView *view = [_radioContainerView viewWithTag:viewTag];
                        if(view)
                        {
                            [self markSelectedOptionView:view];
                        }
                    }
                }
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}

-(void)markSelectedOptionView:(UIView *)view
{
    @try
    {
        UIImage *radioButtonOn = [UIImage imageNamed:@"radio-on"];
        UIImageView *imgView = (UIImageView *)[view viewWithTag:100];
        UIImageView *radioView = (UIImageView *)[view viewWithTag:200];
        BOOL check = NO;
        NSInteger viewTag = view.tag;
        if(imgView && radioView)
        {
            if(imgView.hidden)
            {
                check = YES;
                imgView.hidden = NO;
                if(viewTag < 30)
                {
                    view.backgroundColor = OPTION_SELECTED_COLOR_BLUE;
                }
                else
                    view.backgroundColor = OPTION_SELECTED_COLOR_YELLOW;
                radioView.image = radioButtonOn;
            }
        }
        
        if(viewTag < 30)
        {
            NSInteger pairingViewTag = 0;
            if(viewTag < 20)
            {
                pairingViewTag = viewTag + 10;
                _voiceOpt = viewTag - 11;
                _smsOpt = pairingViewTag - 21;
            }
            else
            {
                pairingViewTag = viewTag - 10;
                _smsOpt = viewTag - 21;
                _voiceOpt = pairingViewTag - 11;
            }
            UIView *pairingView = [_radioContainerView viewWithTag:pairingViewTag];
            
            UIImageView *pairingImgView = (UIImageView *)[pairingView viewWithTag:100];
            UIImageView *pairingRadioView = (UIImageView *)[pairingView viewWithTag:200];
            if(pairingView)
            {
                if(check && [pairingRadioView isKindOfClass:[UIImageView class]] && [pairingImgView isKindOfClass:[UIImageView class]])
                {
                    pairingImgView.hidden = NO;
                    pairingView.backgroundColor = OPTION_SELECTED_COLOR_BLUE;
                    pairingRadioView.image = radioButtonOn;
                }
            }
        }
        else
        {
            _internetOpt = viewTag - 31;
        }
        [self unselectOtherViewsExceptViewWithTag:viewTag];
        
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

-(void)unselectOtherViewsExceptViewWithTag:(NSInteger)viewTag
{
    @try
    {
        UIImage *radioButtonOff = [UIImage imageNamed:@"radio-off"];
        NSMutableArray *internetTags = [[NSMutableArray alloc] initWithObjects:@"31",@"32",@"33",@"34", nil];
        NSMutableArray *smsNelponTags = [[NSMutableArray alloc] initWithObjects:@"11",@"21",@"12",@"22",@"13",@"23",@"14",@"24", nil];
        if(viewTag > 30)
        {
            [internetTags removeObject:[NSString stringWithFormat:@"%ld", (long)viewTag]];
            for (NSString *tag in internetTags) {
                UIView *view = [_radioContainerView viewWithTag:[tag integerValue]];
                if(view)
                {
                    UIImageView *imgView = (UIImageView *)[view viewWithTag:100];
                    UIImageView *radioView = (UIImageView *)[view viewWithTag:200];
                    imgView.hidden = YES;
                    radioView.image = radioButtonOff;
                    view.backgroundColor = OPTION_NORMAL_COLOR;
                    
                }
            }
        }
        else
        {
            NSInteger pairingTag = 0;
            if(viewTag < 20)
                pairingTag = viewTag + 10;
            else
                pairingTag = viewTag - 10;
            
            [smsNelponTags removeObject:[NSString stringWithFormat:@"%ld", (long)viewTag]];
            [smsNelponTags removeObject:[NSString stringWithFormat:@"%ld", (long)pairingTag]];
            
            for (NSString *tag in smsNelponTags) {
                UIView *view = [_radioContainerView viewWithTag:[tag integerValue]];
                if(view)
                {
                    UIImageView *imgView = (UIImageView *)[view viewWithTag:100];
                    UIImageView *radioView = (UIImageView *)[view viewWithTag:200];
                    radioView.image = radioButtonOff;
                    imgView.hidden = YES;
                    view.backgroundColor = OPTION_NORMAL_COLOR;
                }
            }
        }
        
        [internetTags release];
        [smsNelponTags release];
        internetTags = nil;
        smsNelponTags = nil;
    }
    @catch (NSException *exception) {
        
    }
}

#pragma mark - QuotaSliderViewDelegate
- (void)quotaSliderValueChanged:(ASValueTrackingSlider*)slider {
//    NSLog(@"slider %i value = %f", slider.tag, slider.value);
    [_barViewObject updateValueForBarIndex:slider.tag withValue:slider.value];
    [self setupLabelValue];
}


- (void)dealloc {
    [_scrollView release];
    [_contentView release];
    [_fieldView release];
    [_lblInternetUsageTitle release];
    [_lblActivePeriod release];
    [_lblInternetPackageUsage release];
    //[_lblQuotaMeterTitle release];
    [_sliderContainerView release];
    [_footerView release];
    [_lblEstimationTitle release];
    [_lblEstimationQuota release];
    [_radioContainerView release];
    [_barContainer release];
    //[_lblTotal release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setScrollView:nil];
    [self setContentView:nil];
    [self setFieldView:nil];
    [self setLblInternetUsageTitle:nil];
    [self setLblActivePeriod:nil];
    [self setLblInternetPackageUsage:nil];
    //[self setLblQuotaMeterTitle:nil];
    [self setSliderContainerView:nil];
    [self setFooterView:nil];
    [self setLblEstimationTitle:nil];
    [self setLblEstimationQuota:nil];
    [self setRadioContainerView:nil];
    [self setBarContainer:nil];
    //[self setLblTotal:nil];
    [super viewDidUnload];
}
@end
