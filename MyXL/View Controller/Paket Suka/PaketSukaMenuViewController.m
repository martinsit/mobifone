//
//  PaketSukaMenuViewController.m
//  MyXL
//
//  Created by tyegah on 3/13/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PaketSukaMenuViewController.h"
#import "SectionHeaderView.h"
#import "SukaMenuCell.h"
#import "PaketSukaViewController.h"
#import "PaketSukaRenameViewController.h"
#import "NotificationViewController.h"

@interface PaketSukaMenuViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UITableView *tblView;
@end

@implementation PaketSukaMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    for (UIView *subView in [_contentView subviews])
    {
        if ([subView isKindOfClass:[SectionHeaderView class]])
            [subView removeFromSuperview];
    }
    
    _tblView.hidden = YES;
    _tblView.dataSource = self;
    _tblView.delegate = self;
    
//    [self getPageSukaSuka];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    DataManager *sharedData = [DataManager sharedInstance];
    if (sharedData.pageSukaSukaData)
    {
        [self setupHeaderWithTitle:sharedData.pageSukaSukaData.pageTitle];
        _tblView.hidden = NO;
        [_tblView reloadData];
    }
}

-(void)setupHeaderWithTitle:(NSString *)title
{
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:title
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    [headerView1 release];
    headerView1 = nil;
}

-(void)getPageSukaSuka
{
    @try
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = PAGE_SUKASUKA_REQ;
        [request pageSukasuka];
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

#pragma mark - AXISnetRequest Delegate
-(void)requestDoneWithInfo:(NSDictionary *)result
{
    [self.hud hide:YES];
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
    int type = [typeNumber intValue];
    
    BOOL success = [reqResult boolValue];
    if (success)
    {
        if(type == PAGE_SUKASUKA_REQ)
        {
            DataManager *sharedData = [DataManager sharedInstance];
            if (sharedData.pageSukaSukaData) {
                [self setupHeaderWithTitle:sharedData.pageSukaSukaData.pageTitle];
                _tblView.hidden = NO;
                [_tblView reloadData];
            }
        }
        
        if(type == MATRIX_PAKET_SUKASUKA_REQ)
        {
            //            _fieldView.hidden = NO;
            //            [self setupUILayoutWithMatrix:YES];
            //            [self getMyPackage];
            // TODO : Usage Simulator
            [self getUsageSimulatorData];
        }
        else if(type == USAGE_SIMULATOR_SUKASUKA_REQ)
        {
            PaketSukaViewController *packageVC = [[PaketSukaViewController alloc] initWithNibName:@"PaketSukaViewController" bundle:nil];
            
            [self.navigationController pushViewController:packageVC animated:YES];
            [packageVC release];
            packageVC = nil;
            
//            [self setupUILayoutWithMatrix:YES];
//            [self setupLabelValue];
//            [self setupTotalLabelValue];
//            _fieldView.hidden = NO;
//            [self getMyPackage];
        }
        else if(type == QUOTA_SUKASUKA_REQ)
        {
            PaketSukaRenameViewController *packageVC = [[PaketSukaRenameViewController alloc] initWithNibName:@"PaketSukaRenameViewController" bundle:nil];
            
            [self.navigationController pushViewController:packageVC animated:YES];
            [packageVC release];
            packageVC = nil;
        }
        else if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else
    {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        if(type == PAGE_SUKASUKA_REQ)
        {
            _tblView.hidden = NO;
            [_tblView reloadData];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellid = @"SukaMenuCell";
    SukaMenuCell *cell = (SukaMenuCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    if(!cell)
    {
       cell = (SukaMenuCell *)[[[NSBundle mainBundle] loadNibNamed:cellid owner:nil options: nil] objectAtIndex:0];
    }
    DataManager *sharedData = [DataManager sharedInstance];
//    cell.backgroundColor = colorWithHexString(sharedData.profileData.themesModel.menuFeature);
    cell.lblTitle.font = [UIFont fontWithName:kDefaultFont size:12];
    cell.lblTitle.textColor = colorWithHexString(sharedData.profileData.themesModel.menuFeatureTxt);
    cell.btnIcon.layer.cornerRadius = 5;
    if(indexPath.row == 0)
    {
        [cell.btnIcon setTitle:@"C" forState:UIControlStateNormal];
        cell.lblTitle.text = @"BUAT PAKET SESUKAMU";
        if(sharedData.pageSukaSukaData)
        {
            cell.lblTitle.text = [sharedData.pageSukaSukaData.lblBeliSesukamu uppercaseString];
        }
    }
    else
    {
        [cell.btnIcon setTitle:@"Q" forState:UIControlStateNormal];
        cell.lblTitle.text = @"GANTI NAMA PAKET";
        if(sharedData.pageSukaSukaData)
        {
            cell.lblTitle.text = [sharedData.pageSukaSukaData.lblUbahNama uppercaseString];
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        [self getPackageMatrix];
//        PaketSukaViewController *packageVC = [[PaketSukaViewController alloc] initWithNibName:@"PaketSukaViewController" bundle:nil];
//        
//        [self.navigationController pushViewController:packageVC animated:YES];
//        [packageVC release];
//        packageVC = nil;
    }
    else
    {
        [self getQuotaSukaSuka];
//        PaketSukaRenameViewController *packageVC = [[PaketSukaRenameViewController alloc] initWithNibName:@"PaketSukaRenameViewController" bundle:nil];
//        
//        [self.navigationController pushViewController:packageVC animated:YES];
//        [packageVC release];
//        packageVC = nil;
    }
}


-(void)getQuotaSukaSuka
{
    @try
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = QUOTA_SUKASUKA_REQ;
        [request checkQuotaSukasuka];
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

-(void)getPackageMatrix
{
    @try
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = MATRIX_PAKET_SUKASUKA_REQ;
        [request getPackageSukaMatrix];
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

-(void)getUsageSimulatorData
{
    @try
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = USAGE_SIMULATOR_SUKASUKA_REQ;
        [request usageSimulatorSukaSuka];
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

- (void)dealloc {
    [_tblView release];
    [_contentView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTblView:nil];
    [self setContentView:nil];
    [super viewDidUnload];
}
@end
