//
//  PaketSukaRenamePackageViewController.m
//  MyXL
//
//  Created by tyegah on 3/16/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PaketSukaRenamePackageViewController.h"
#import "LeftViewController.h"
#import "Language.h"
#import "SectionHeaderView.h"
#import "IIViewDeckController.h"
#import "ThankYouViewController.h"
#import "IIWrapController.h"

@interface PaketSukaRenamePackageViewController ()<UITextFieldDelegate>
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UIView *headerView;
@property (retain, nonatomic) IBOutlet UIView *footerView;
@property (retain, nonatomic) IBOutlet UILabel *lblFooterTitle;
@property (retain, nonatomic) IBOutlet UITextField *txtPackageName;
@property (retain, nonatomic) IBOutlet UILabel *lblErrorPackageName;
@property (retain, nonatomic) UIButton *submitButton;
@property (nonatomic, assign) float animatedDis;
@property (retain, nonatomic) IBOutlet UILabel *lblNelpon;
@property (retain, nonatomic) IBOutlet UILabel *lblPaketNelponDesc;
@property (retain, nonatomic) IBOutlet UILabel *lblSMS;
@property (retain, nonatomic) IBOutlet UILabel *lblPaketSMSDesc;
@property (retain, nonatomic) IBOutlet UILabel *lblInternet;
@property (retain, nonatomic) IBOutlet UILabel *lblPaketInternetDesc;
@property (strong, nonatomic) LeftViewController *sideMenuViewController;
@property (nonatomic, retain) UITextField *activeTextField;
@property (retain, nonatomic) IBOutlet UIButton *btnRename;
@end

@implementation PaketSukaRenamePackageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupViewLayout];
    [self setupLabelValue];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    _txtPackageName.delegate = self;
    
    // TODO : Neptune
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

// TODO : Neptune
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

#pragma mark - UIKeyboard Notification Delegate
- (void)keyboardWillShow:(NSNotification *)notification
{
    //    if(IS_IPAD)
    //    {
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    keyboardFrame = [self.view convertRect:keyboardFrame fromView:nil];
    
    CGFloat heightOfKeyboard = keyboardFrame.size.height;
    if(!IS_IPAD)
        heightOfKeyboard = heightOfKeyboard - 20;
    //NSLog(@"The keyboard is now %d pixels high.", (int)heightOfKeyboard);
    if(_activeTextField != nil)
        [self animateTextField:_activeTextField up:YES withDistance:heightOfKeyboard];
    //    }
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    //    if (IS_IPAD) {
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    keyboardFrame = [self.view convertRect:keyboardFrame fromView:nil];
    
    CGFloat heightOfKeyboard = keyboardFrame.size.height;
    if(!IS_IPAD)
        heightOfKeyboard = heightOfKeyboard - 20;
    //NSLog(@"The keyboard is now %d pixels high.", (int)heightOfKeyboard);
    [self animateTextField:_activeTextField up:NO withDistance:heightOfKeyboard];
    _activeTextField = nil;
    //    }
}

#pragma mark - UITextfield Delegate
-(void)animateTextField:(UITextField*)textField up:(BOOL)up withDistance:(CGFloat)distance
{
    CGPoint temp = [textField.superview convertPoint:textField.frame.origin toView:nil];
    if(up)
    {
        int moveUpValue = temp.y+textField.frame.size.height;
        //NSLog(@"moveup value %d", moveUpValue);
        _animatedDis = distance -(self.view.frame.size.height - moveUpValue);
        //NSLog(@"frame height %f, animatedDis %f", self.view.frame.size.height, _animatedDis);
    }
    
    if(_animatedDis>0)
    {
        const int movementDistance = _animatedDis;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        
        // animate the view up / down
        [UIView animateWithDuration:movementDuration animations:^ {
            self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        }];
    }
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    CGPoint temp = [textField.superview convertPoint:textField.frame.origin toView:nil];
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait){
        
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
//            NSLog(@"moveup value %d", moveUpValue);
            _animatedDis = 220 -(self.view.frame.size.height - moveUpValue);
//            NSLog(@"frame height %f, animatedDis %f", self.view.frame.size.height, _animatedDis);
        }
    }
    
    if(_animatedDis>0)
    {
        const int movementDistance = _animatedDis;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        
        // animate the view up / down
        [UIView animateWithDuration:movementDuration animations:^ {
            if (orientation == UIInterfaceOrientationPortrait){
                self.view.frame = CGRectOffset(self.view.frame, 0, movement);
            }
        }];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 15);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    _activeTextField = textField;
    self.lblErrorPackageName.hidden = YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    _activeTextField = nil;
}

#pragma mark - IBActions
-(void)performSubmit:(id)sender
{
    NSLog(@"Submit ed");
    [_txtPackageName resignFirstResponder];
    
    [self renamePaketSukaSuka];
}

#pragma mark - METHODS

-(void)setupViewLayout
{
    DataManager *sharedData = [DataManager sharedInstance];
    UIFont *fontBold = [UIFont fontWithName:kDefaultFont size:15];
    
    UIFont *fontLight = [UIFont fontWithName:kDefaultFontLight size:15];
    UIColor *orangeColor = [UIColor colorWithRed:242/255.0 green:154/255.0 blue:39/255.0 alpha:1];
    
    _lblNelpon.font = fontBold;
    _lblSMS.font = fontBold;
    _lblInternet.font = fontBold;
    
    _lblPaketNelponDesc.font = fontLight;
    _lblPaketSMSDesc.font = fontLight;
    _lblPaketInternetDesc.font = fontLight;
    _lblPaketInternetDesc.numberOfLines = 0;
    _lblPaketInternetDesc.lineBreakMode = UILineBreakModeWordWrap;
    
    _lblErrorPackageName.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
     _lblFooterTitle.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    if(IS_IPAD)
        _lblFooterTitle.font = [UIFont fontWithName:kDefaultFontLight size:17.0];
    _txtPackageName.layer.borderWidth = 3;
    _txtPackageName.layer.borderColor = orangeColor.CGColor;
    _txtPackageName.layer.cornerRadius = 5;
    _txtPackageName.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    _txtPackageName.placeholder = [Language get:@"package_name_placeholder" alter:nil];
    
    NSString *packageTitle = sharedData.matrixSukaSukaData.title_pkg;
    if([packageTitle length] == 0)
        packageTitle = [Language get:@"package_sesukamu" alter:nil];
    int lang = [Language getCurrentLanguage];
    NSString *lblPackageSummaryTitle = [Language get:@"package_summary" alter:nil];
    if(lang == 1)
    {
        lblPackageSummaryTitle = [NSString stringWithFormat:@"%@ %@", packageTitle, lblPackageSummaryTitle];
    }
    
    _lblFooterTitle.text = [Language get:@"name_your_package" alter:nil];
    if(sharedData.mappingSukaSukaData.custom_pkg != nil && ![sharedData.mappingSukaSukaData.custom_pkg isEqual:[NSNull null]])
    {
        _lblFooterTitle.text = sharedData.mappingSukaSukaData.custom_pkg;
    }
    _lblNelpon.text = [Language get:@"voice_package" alter:nil];
    
    //--------------//
    // Setup Header //
    //--------------//
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:sharedData.mappingSukaSukaData.last_pkg
                                                               isFirstSection:NO];
    headerView2.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [_contentView addSubview:headerView2];
    
    _headerView.frame = CGRectOffset(_headerView.frame, 0, CGRectGetHeight(headerView2.frame) - CGRectGetMinY(_headerView.frame));
    
    _footerView.frame = CGRectMake(CGRectGetMinX(_footerView.frame), CGRectGetMaxY(_headerView.frame), CGRectGetWidth(_footerView.frame), CGRectGetHeight(_footerView.frame));
    
    //--------------//
    // Setup Button //
    //--------------//
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    NSString *buttonText = sharedData.matrixSukaSukaData.label_submit;
    if([buttonText length] == 0)
    {
        buttonText = [Language get:@"rename" alter:nil];
    }
    
    UIButton *submitBtn = createButtonWithFrame(CGRectMake(CGRectGetWidth(self.view.frame) - 120,
                                                           CGRectGetMaxY(_footerView.frame) + kDefaultComponentPadding,
                                                           100,
                                                           kDefaultButtonHeight),
                                                [buttonText uppercaseString],
                                                buttonFont,
                                                kDefaultWhiteColor,
                                                kDefaultAquaMarineColor,
                                                kDefaultBlueColor);
    
    [submitBtn addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    submitBtn.frame = _btnRename.frame;
    _submitButton = submitBtn;
    [_footerView addSubview:_submitButton];
    
    _contentView.frame = CGRectMake(CGRectGetMinX(_contentView.frame), CGRectGetMinY(_contentView.frame), CGRectGetWidth(_contentView.frame), CGRectGetHeight(_contentView.frame) + kDefaultComponentPadding);
    
    _scrollView.contentSize = CGSizeMake(0, CGRectGetMaxY(_contentView.frame));
}

-(void)setupLabelValue
{
    DataManager *sharedData = [DataManager sharedInstance];
    MappingSukaSukaModel *mappingModel = sharedData.mappingSukaSukaData;
    _lblPaketSMSDesc.text = mappingModel.smsDetail;
    _lblPaketNelponDesc.text = mappingModel.voiceDetail;
    _lblPaketInternetDesc.text = mappingModel.internetDetail;
}

-(void)gotoThankyouPageWithInfo:(NSString *)info
{
    DataManager *dataManager = [DataManager sharedInstance];
    
    LeftViewController *leftViewController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    leftViewController.content = dataManager.leftMenuData;;
    _sideMenuViewController = leftViewController;
    
    ThankYouViewController *thankYouViewController = [[ThankYouViewController alloc] initWithNibName:@"ThankYouViewController" bundle:nil];
    thankYouViewController.info = info;
    thankYouViewController.displayFBButton = NO;
    thankYouViewController.displaySocialMediaButtons = YES;
    thankYouViewController.hideInboxBtn = NO;
    thankYouViewController.packageName = _txtPackageName.text;
    
    UIViewController *infoViewController = thankYouViewController;
    
    IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:infoViewController leftViewController:_sideMenuViewController];
    deckController.navigationControllerBehavior = IIViewDeckNavigationControllerIntegrated;
    deckController.leftSize = self.view.frame.size.width - 260; //270
    infoViewController = deckController;
    
    UINavigationController *navController = customizedNavigationController(self.view.bounds,NO);
    [navController setViewControllers:[NSArray arrayWithObject:infoViewController]];
    
    infoViewController = [[IIWrapController alloc] initWithViewController:navController];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController pushViewController:infoViewController animated:YES];
}

-(void)renamePaketSukaSuka
{
    @try
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.labelText = [Language get:@"loading" alter:nil];
        
        DataManager *dataManager = [DataManager sharedInstance];
        MappingSukaSukaModel *dataModel = dataManager.mappingSukaSukaData;
        AXISnetRequest *request = [AXISnetRequest sharedInstance];
        request.delegate = self;
        request.requestType = RENAME_SUKASUKA_REQ;
        [request renamePackageSukaSukaWithName:_txtPackageName.text andServiceID:dataModel.serviceID];
        
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self renamePaketSukaSuka];
    }
    else {
        
    }
}

#pragma mark - AXISnetRequest Delegate
-(void)requestDoneWithInfo:(NSDictionary *)result
{
     [self.hud hide:YES];
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    NSString *reason = [result valueForKey:REASON_KEY];
    NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
    int type = [typeNumber intValue];
    BOOL success = [reqResult boolValue];
    if (success)
    {
        if(type == RENAME_SUKASUKA_REQ)
            [self gotoThankyouPageWithInfo:reason];
    }
    else
    {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)dealloc {
    [_scrollView release];
    [_contentView release];
    [_headerView release];
    [_footerView release];
    [_lblFooterTitle release];
    [_txtPackageName release];
    [_lblErrorPackageName release];
    [_lblNelpon release];
    [_lblPaketNelponDesc release];
    [_lblSMS release];
    [_lblPaketSMSDesc release];
    [_lblInternet release];
    [_lblPaketInternetDesc release];
    [_activeTextField release];
    [_submitButton release];
    [_btnRename release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setScrollView:nil];
    [self setContentView:nil];
    [self setHeaderView:nil];
    [self setFooterView:nil];
    [self setLblFooterTitle:nil];
    [self setTxtPackageName:nil];
    [self setLblErrorPackageName:nil];
    [self setLblNelpon:nil];
    [self setLblPaketNelponDesc:nil];
    [self setLblSMS:nil];
    [self setLblPaketSMSDesc:nil];
    [self setLblInternet:nil];
    [self setLblPaketInternetDesc:nil];
    [self setBtnRename:nil];
    [super viewDidUnload];
}

@end
