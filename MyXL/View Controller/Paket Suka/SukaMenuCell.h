//
//  SukaMenuCell.h
//  MyXL
//
//  Created by tyegah on 3/13/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SukaMenuCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;
@property (retain, nonatomic) IBOutlet UIButton *btnIcon;
@property (retain, nonatomic) IBOutlet UIView *cellContentView;

@end
