//
//  PaketSukaNameViewController.h
//  MyXL
//
//  Created by tyegah on 1/28/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface PaketSukaNameViewController : BasicViewController<UITextFieldDelegate>

@end
