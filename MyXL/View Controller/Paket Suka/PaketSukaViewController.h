//
//  PaketSukaViewController.h
//  MyXL
//
//  Created by tyegah on 1/27/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "QuotaSliderView.h"

@interface PaketSukaViewController : BasicViewController <QuotaSliderViewDelegate>

@end
