//
//  PaymentChoiceViewController.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 5/28/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "PaymentChoiceViewController.h"
#import "SectionHeaderView.h"
#import "Language.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "DataManager.h"
#import "DummyBuyVoucherViewController.h"

#import "ReloadBalanceViewController.h"
#import "XLTunaiReloadViewController.h"
#import "TOWebViewController.h"
#import "NotificationViewController.h"

#import "EncryptDecrypt.h"

#import "SVWebViewController.h"

@interface PaymentChoiceViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, retain) IBOutlet UILabel *methodLabel;

@property (strong, nonatomic) UIButton *submitButton;

@property (readwrite) int counter;

@end

@implementation PaymentChoiceViewController

//@synthesize headerTitle = _headerTitle;
//@synthesize headerIcon = _headerIcon;
@synthesize menuModel = _menuModel;
@synthesize paymentMenu = _paymentMenu;
@synthesize selectedPayment;
@synthesize grayHeaderTitle = _grayHeaderTitle;
@synthesize blueHeaderTitle = _blueHeaderTitle;
@synthesize isLevel2 = _isLevel2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    _counter = 0;
    [self setupUILayout];
    [self setupValue];
    
    if (_isLevel2) {
        [self createBarButtonItem:MENU_PROFILE_NOTIF_BACK_LEVEL2];
    }
    else {
        [self createBarButtonItem:BACK_NOTIF];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Selector

- (void)setupUILayout {
    
    
    
    //-------------------------//
    // Setup Radio Button View //
    //-------------------------//
    [self createRadioView];
    
    
}

- (void)setupValue {
    _methodLabel.text = [Language get:@"select_payment" alter:nil];
}

- (void)performSubmit:(id)sender {
    BOOL valid = [self validateRadioInput:_counter];
    
    if (valid) {
        MenuModel *paymentModel = [self.paymentMenu objectAtIndex:self.selectedPayment];
        if ([paymentModel.href isEqualToString:@"#xltunai_reload"]) {/*
            DataManager *sharedData = [DataManager sharedInstance];
            if ([sharedData.xlTunaiBalanceData.code isEqualToString:@""] || sharedData.xlTunaiBalanceData == nil) {
                [self performXLTunaiBalance];
            }
            else if ([sharedData.xlTunaiBalanceData.code isEqualToString:@"01"]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                                message:[Language get:@"xl_tunai_not_eligible" alter:nil]
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
            else {*/
                DummyBuyVoucherViewController *reloadBalanceVC = [[DummyBuyVoucherViewController alloc] initWithNibName:@"DummyBuyVoucherViewController" bundle:nil];
                
                MenuModel *menu = [[MenuModel alloc] init];
                menu.groupName = _blueHeaderTitle;
                menu.menuName = paymentModel.menuName;
//                reloadBalanceVC.menuModel = menu;
                
                //reloadBalanceVC.menuModel = _menuModel;
                //reloadBalanceVC.headerTitle = _menuModel.menuName;
                //reloadBalanceVC.headerIcon = icon(_menuModel.menuId);
                
//                [reloadBalanceVC createBarButtonItem:BACK_NOTIF];
                
                [self.navigationController pushViewController:reloadBalanceVC animated:YES];
                reloadBalanceVC = nil;
                [reloadBalanceVC release];
                
                [menu release];
            }
//        }
        else if ([paymentModel.href isEqualToString:@"#topup_hvrn"]) {
            ReloadBalanceViewController *reloadBalanceVC = [[ReloadBalanceViewController alloc] initWithNibName:@"ReloadBalanceViewController" bundle:nil];
            
            MenuModel *menu = [[MenuModel alloc] init];
            menu.groupName = _blueHeaderTitle;
            menu.menuName = paymentModel.menuName;
            reloadBalanceVC.menuModel = menu;
            
            //reloadBalanceVC.menuModel = _menuModel;
            //reloadBalanceVC.headerTitle = _menuModel.menuName;
            //reloadBalanceVC.headerIcon = icon(_menuModel.menuId);
            
            [reloadBalanceVC createBarButtonItem:BACK_NOTIF];
            
            [self.navigationController pushViewController:reloadBalanceVC animated:YES];
            reloadBalanceVC = nil;
            [reloadBalanceVC release];
            
            [menu release];
        }
        else if ([paymentModel.href isEqualToString:@"#auto_reload"]) {
            NSURL *url = nil;
            url = [NSURL URLWithString:@"https://isipulsa.xl.co.id/login"];
            
            SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
            webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
            [self presentViewController:webViewController animated:YES completion:NULL];
        }
        else {
            NSURL *url = nil;
            //url = [NSURL URLWithString:@"https://isipulsa.xl.co.id/myxl"];
            
            DataManager *sharedData = [DataManager sharedInstance];
            
            NSString *saltKeyAPI = SALT_KEY;
            NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
            NSString *paymentId = @"1"; //hardcoded
            NSString *lang = sharedData.profileData.language;
            
            NSString *baseUrlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API,M_KARTU];
            NSString *paramString = [NSString stringWithFormat:@"msisdn=%@&paymentid=%@&lang=%@",msisdn,paymentId,lang];
            //NSLog(@"paramString = %@",paramString);
            paramString = [EncryptDecrypt doCipherForiOS7:paramString action:kCCEncrypt withKey:saltKeyAPI];
            //NSLog(@"Encrypt paramString = %@",paramString);
            
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlString,paramString]];
            //NSLog(@"URL M-Kartu = %@",url);
            
            SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
            webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
            [self presentViewController:webViewController animated:YES completion:NULL];
            
            // Screen tracking
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            self.screenName = @"VisaAndMasterCardIOS";
            [tracker set:kGAIScreenName value:@"VisaAndMasterCardIOS"];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
            [[GAI sharedInstance] dispatch];
        }
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"select_one" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"ok" alter:nil]
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)createRadioView {
    
    //--------------//
    // Setup Header //
    //--------------//
   /* SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:_grayHeaderTitle
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    */
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,10)
                                                               withLabelForXL:@"Choose Payment method below"
                                                               isFirstSection:YES];
    headerView2.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSmallSize] ;
    
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
//    _methodLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
//    _methodLabel.textColor = kDefaultTitleFontGrayColor;
    
    //[self setupPaymentMethodContent];
    
    NSMutableArray *buttons = [[NSMutableArray alloc] init];
    CGFloat startY = 0.0;
    CGFloat lastY = 0.0;
    
    for (int i = 0; i < [_paymentMenu count]; i++) {
        
        NSLog(@"i is %d and Paymennt menu count is %lu",i,(unsigned long)_paymentMenu.count);
        
        NSLog(@"Payment menu is %@",[_paymentMenu objectAtIndex:i]);
        
        MenuModel *labelValue = [_paymentMenu objectAtIndex:i];
            // Button
        UIButton* aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [aButton setTag:i];
        CGRect frame = CGRectMake(0.0, startY, kDefaultButtonHeight, kDefaultButtonHeight);
        aButton.frame = frame;
        if (i == 0) {
            aButton.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:208.0/255.0 alpha:1.0];
        } else if ( i == 1){
            aButton.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:93.0/255.0 blue:167.0/255.0 alpha:1.0];
        }

        
        
        [aButton setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
        [aButton setImage:[UIImage imageNamed:@"radio-on.png"] forState:UIControlStateSelected];
        
        [_fieldView addSubview:aButton];
        [buttons addObject:aButton];
        
        // Label
        CGFloat xLabel = aButton.frame.origin.x + aButton.frame.size.width ;//+ kDefaultComponentPadding;
        CGFloat width = _contentView.frame.size.width /*- (kDefaultPadding*2)*/ - aButton.frame.size.width;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xLabel, startY, width, kDefaultButtonHeight)];
        
        if (i == 0) {
            label.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:208.0/255.0 alpha:1.0];
        } else if ( i == 1){
            label.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:93.0/255.0 blue:167.0/255.0 alpha:1.0];
        }

        label.numberOfLines = 0;
        label.lineBreakMode = UILineBreakModeWordWrap;
        label.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontValueSize];
        label.textColor = kDefaultWhiteColor;
        label.text = labelValue.menuName;
        
        CGSize size = calculateExpectedSize(label, labelValue.menuName);
        if (size.height < kDefaultButtonHeight) {
            frame = label.frame;
            frame.size.height = kDefaultButtonHeight;
        }
        else {
            frame = label.frame;
            frame.size.height = size.height;
        }
        
        label.frame = frame;
        
        [_fieldView addSubview:label];
        
        startY = label.frame.origin.y + label.frame.size.height; //+ kDefaultComponentPadding;
        lastY = label.frame.origin.y + label.frame.size.height;
    }
    // Instantiate your GSRadioButtonSetController object
    self.radioButtonSetController = [[GSRadioButtonSetController alloc] init];
    
    // Set its delegate to your view controller
    self.radioButtonSetController.delegate = self;
    
    // Set its buttons property to an array of buttons that you've
    // created previously.
    NSArray *buttonArray = [buttons copy];
    self.radioButtonSetController.buttons = buttonArray;
    
    [buttons release];
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    newFrame.size.height = startY;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Setup Button //
    //--------------//
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    UIButton *button = createButtonWithFrame(CGRectMake(_fieldView.frame.origin.x + _fieldView.frame.size.width - 140 ,                                          _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
                                                        120,
                                                        kDefaultButtonHeight),
                                             [[Language get:@"next" alter:nil] uppercaseString],
                                             buttonFont,
                                             kDefaultWhiteColor,
                                             [UIColor colorWithRed:0.0/255.0 green:93.0/255.0 blue:167.0/255.0 alpha:1.0],
                                             kDefaultBlueColor);
    
    [button addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    _submitButton = button;
    [_contentView addSubview:_submitButton];
    
    //--------------//
    // Content View //
    //--------------//
    newFrame = _contentView.frame;
    newFrame.size.height = _submitButton.frame.origin.y + _submitButton.frame.size.height + kDefaultComponentPadding;
    _contentView.frame = newFrame;
    _contentView.backgroundColor = [UIColor clearColor];
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 200.0);
    
//    [headerView1 release];
    [headerView2 release];
}

- (void)setupPaymentMethodContent
{
    
    NSMutableArray *contentTmp = [[NSMutableArray alloc] init];
    
    MenuModel *model = [[MenuModel alloc] init];
    model.menuName = @"XL Tunai";
    model.href = @"#xltunai";
    [contentTmp addObject:model];
    model = nil;
    
    model = [[MenuModel alloc] init];
    model.menuName = @"Voucher";
    model.href = @"#topup_hvrn";
    [contentTmp addObject:model];
    model = nil;
    
    model = [[MenuModel alloc] init];
    model.menuName = [Language get:@"credit_card" alter:nil];
    model.href = @"#mkartu";
    [contentTmp addObject:model];
    model = nil;
    
    self.paymentMenu = [NSArray arrayWithArray:contentTmp];
    [contentTmp release];
    
}

- (BOOL)validateRadioInput:(int)input {
    
    BOOL result = NO;
    int min = 1;
    int max = 1;
    if (input >= min && input <= max) {
        result = YES;
    }
    return  result;
}

- (void)performXLTunaiBalance {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = XL_TUNAI_BALANCE_REQ;
    [request xlTunaiBalance];
}

#pragma mark - GSRadioButtonSetController delegate methods

- (void)radioButtonSetController:(GSRadioButtonSetController *)controller
          didSelectButtonAtIndex:(NSUInteger)selectedIndex
{
    if (_counter == 0) {
        _counter++;
    }
    
    // Handle button selection here
    self.selectedPayment = selectedIndex;
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        NSNumber *repeatResult = [result valueForKey:REPEAT_KEY];
        BOOL repeat = [repeatResult boolValue];
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == XL_TUNAI_BALANCE_REQ) {
            if (repeat) {
                [self performXLTunaiBalance];
            }
            else {
                MenuModel *paymentModel = [self.paymentMenu objectAtIndex:self.selectedPayment];
                DataManager *sharedData = [DataManager sharedInstance];
                
                if ([sharedData.xlTunaiBalanceData.code isEqualToString:@"01"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                                    message:[Language get:@"xl_tunai_not_eligible" alter:nil]
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    [alert release];
                }
                else {
                    XLTunaiReloadViewController *reloadBalanceVC = [[XLTunaiReloadViewController alloc] initWithNibName:@"XLTunaiReloadViewController" bundle:nil];
                    
                    MenuModel *menu = [[MenuModel alloc] init];
                    menu.groupName = _blueHeaderTitle;
                    menu.menuName = paymentModel.menuName;
                    reloadBalanceVC.menuModel = menu;
                    
                    [reloadBalanceVC createBarButtonItem:BACK_NOTIF];
                    
                    [self.navigationController pushViewController:reloadBalanceVC animated:YES];
                    reloadBalanceVC = nil;
                    [reloadBalanceVC release];
                    
                    [menu release];
                }
            }
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
