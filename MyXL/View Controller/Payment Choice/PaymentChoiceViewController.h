//
//  PaymentChoiceViewController.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 5/28/14.
//  Copyright (c) 2014 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "BasicViewController.h"
#import "MenuModel.h"
#import "GSRadioButtonSetController.h"
#import "AXISnetRequest.h"

@interface PaymentChoiceViewController : BasicViewController <GSRadioButtonSetControllerDelegate, AXISnetRequestDelegate> {
    //NSString *_headerTitle;
    //NSString *_headerIcon;
    MenuModel *_menuModel;
    NSArray *paymentMenu;
    int selectedPayment;
    NSString *_grayHeaderTitle;
    NSString *_blueHeaderTitle;
    BOOL _isLevel2;
}

//@property (nonatomic, retain) NSString *headerTitle;
//@property (nonatomic, retain) NSString *headerIcon;
@property (nonatomic, retain) MenuModel *menuModel;
@property (nonatomic, strong) GSRadioButtonSetController *radioButtonSetController;
@property (nonatomic, retain) NSArray *paymentMenu;
@property (readwrite) int selectedPayment;
@property (nonatomic, retain) NSString *grayHeaderTitle;
@property (nonatomic, retain) NSString *blueHeaderTitle;
@property (readwrite) BOOL isLevel2;

@end
