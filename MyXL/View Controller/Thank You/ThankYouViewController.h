//
//  ThankYouViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/10/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AXISnetRequest.h"
#import "MenuModel.h"

@interface ThankYouViewController : BasicViewController <AXISnetRequestDelegate> {
    NSString *_info;
    BOOL _displayFBButton;
    NSString *_trxId;
    MenuModel *_menuModel;
    
    BOOL _isXLStar;
    
    BOOL _hideInboxBtn;
}

@property (nonatomic, retain) NSString *info;
@property (readwrite) BOOL displayFBButton;
@property (nonatomic, retain) NSString *trxId;
@property (nonatomic, retain) MenuModel *menuModel;
@property (readwrite) BOOL isXLStar;
@property (readwrite) BOOL hideInboxBtn;

//TODO : NEPTUNE
@property (readwrite) BOOL displaySocialMediaButtons;
@property (nonatomic, retain) NSString *packageName;

@end
