//
//  ThankYouViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 3/10/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ThankYouViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>
#import <Social/Social.h>
#import "AppDelegate.h"
//#import <FacebookSDK/FacebookSDK.h>
#import "AXISnetRequest.h"
#import "DataManager.h"
#import "STTwitter.h"

#import "NotificationViewController.h"
#import "RNBlurModalView.h"

#define SOSMED_URL      @"url"
#define SOSMED_TITLE_FB @"title_fb"
#define SOSMED_URL_LOGO @"url_logo"
#define SOSMED_DESC_FB  @"desc_app_fb"
#define SOSMED_DESC_TW  @"desc_app_twit"
#define SOSMED_SHARE_FB @"share_fb"
#define SOSMED_SHARE_TW @"share_twit"
#define TWITTER_CONSUMER_KEY @"QQwK6tq2jeEUnppeo8JjZ4cXr"
#define TWITTER_SECRET_KEY @"8MASrNWRAIDP9aPdhAhnAObdrIMRA84x7D2WHDeYMFdM0iK6Gn"

@interface ThankYouViewController () <FBLoginViewDelegate, UITextViewDelegate, UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *topInfoLabel;
@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

@property (strong, nonatomic) UIButton *inboxButton;
// TODO : Neptune
@property (nonatomic, retain) STTwitterAPI *twitter;
@property (nonatomic, retain) RNBlurModalView *twitterModalView;
@property (strong, nonatomic) UIButton *shareFBButton;
@property (strong, nonatomic) UIButton *twitterButton;
@property (nonatomic, retain) NSDictionary *twitterAuthDict;
@property (nonatomic, retain) UITextView *twitterTextView;
@property (nonatomic, retain) UILabel *errorTwitLabel;

- (void)performShare;
- (void)requestShareToFacebook;

@end

@implementation ThankYouViewController

@synthesize info = _info;
@synthesize displayFBButton = _displayFBButton;
@synthesize trxId = _trxId;
@synthesize menuModel = _menuModel;
@synthesize isXLStar = _isXLStar;
@synthesize hideInboxBtn = _hideInboxBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // TODO : NEPTUNE
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(twitterAuthenticated:) name:@"TWITTER_AUTH" object:nil];
    _packageName = @"";
}

- (void)viewWillAppear:(BOOL)animated {
    // TODO : NEPTUNE
    if(_displaySocialMediaButtons)
        [self createBarButtonItem:MENU_NOTIF];
    else
        [self createBarButtonItem:BACK_NOTIF];
    
    //--------------//
    // Setup Header //
    //--------------//
    CGFloat screenWidth = 0.0;
    if (IS_IPAD) {
        screenWidth = 1024.0;
    }
    else {
        screenWidth = 320.0;
    }
    
    SectionHeaderView *headerView1;
    if (_isXLStar) {
        headerView1 = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, screenWidth,kDefaultCellHeight*1.5)
                                                withLabelForXL:_menuModel.menuName
                                                isFirstSection:YES] autorelease];
    }
    else {
        // TODO : NEPTUNE
        NSString *headerTitle = @"";
        if(_displaySocialMediaButtons)
        {
            headerTitle = [[Language get:@"notification" alter:nil] uppercaseString];
        }
        else
        {
            headerTitle = [[Language get:@"thank_you" alter:nil] uppercaseString];
            
        }
        headerView1 = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, screenWidth,kDefaultCellHeight*1.5)
                                                withLabelForXL:headerTitle
                                                isFirstSection:YES] autorelease];
    }
    
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, screenWidth,kDefaultCellHeight)
                                                               withLabelForXL:_menuModel.groupName
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    _topInfoLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontSize];
    _topInfoLabel.textColor = kDefaultBlackColor;
    _topInfoLabel.backgroundColor = [UIColor clearColor];
    _topInfoLabel.numberOfLines = 0;
    _topInfoLabel.lineBreakMode = UILineBreakModeWordWrap;
    _topInfoLabel.text = _info;
    
    [_topInfoLabel sizeToFit];
    
    CGFloat componentHeight = _topInfoLabel.frame.origin.y + _topInfoLabel.frame.size.height + kDefaultPadding;
    
    if (_displayFBButton) {
        /*
        FBLoginView *loginview = [[FBLoginView alloc] init];
        [self resizeLoginView:loginview];
        loginview.frame = CGRectOffset(loginview.frame, _topInfoLabel.frame.origin.x, _topInfoLabel.frame.origin.y + _topInfoLabel.frame.size.height + kDefaultPadding);
        
        loginview.delegate = self;
        [_contentView addSubview:loginview];
        [loginview sizeToFit];*/
        
        UIImage *image = [UIImage imageNamed:@"facebook_share"];
        //UIImage *imagePressed = [UIImage imageNamed:@"login-button-small-pressed"];
        
        UIButton *buttonTmp = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonTmp.frame = CGRectMake(_topInfoLabel.frame.origin.x, componentHeight, 61.0, 21.0);
        [buttonTmp setBackgroundImage:image forState:UIControlStateNormal];
        //[buttonTmp setBackgroundImage:imagePressed forState:UIControlStateHighlighted];
        //[buttonTmp setTitle:@"Share" forState:UIControlStateNormal];
        
        [buttonTmp addTarget:self action:@selector(buttonClickHandler:) forControlEvents:UIControlEventTouchUpInside];
        
        _shareFBButton = buttonTmp;
        [_contentView addSubview:_shareFBButton];
        
        componentHeight = componentHeight + _shareFBButton.frame.size.height + kDefaultPadding;
    }
    
    // TODO : NEPTUNE
    if(_displaySocialMediaButtons)
    {
        CGSize lblInfoSize = calculateExpectedSize(_topInfoLabel, _info);
        CGFloat btnWidth = 120.0;
        CGFloat btnHeight = 35.0;
        
        UIButton *twitterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        twitterBtn.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 + 8, CGRectGetMinY(_topInfoLabel.frame) + lblInfoSize.height + 20, btnWidth, btnHeight);
        [twitterBtn setImage:[UIImage imageNamed:@"share-twit-button"] forState:UIControlStateNormal];
        [twitterBtn setTitle:@"Tw" forState:UIControlEventTouchUpInside];
        [twitterBtn addTarget:self action:@selector(shareTwitterClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        _twitterButton = twitterBtn;
        [_fieldView addSubview:_twitterButton];
        
        UIButton *fbButton = [UIButton buttonWithType:UIButtonTypeCustom];
        fbButton.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 8 - btnWidth, CGRectGetMinY(_twitterButton.frame), btnWidth, btnHeight);
        [fbButton setImage:[UIImage imageNamed:@"share-fb-button"] forState:UIControlStateNormal];
        [fbButton setTitle:@"Fb" forState:UIControlEventTouchUpInside];
        [fbButton addTarget:self action:@selector(shareFbClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        _shareFBButton = fbButton;
        [_fieldView addSubview:_shareFBButton];
        
        componentHeight = CGRectGetMaxY(twitterBtn.frame) + kDefaultPadding;
    }
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    
    // TODO: NEPTUNE
    newFrame.size.height = componentHeight;
    
    _fieldView.frame = newFrame;
    
    if (_hideInboxBtn) {
        //--------------//
        // Content View //
        //--------------//
        _contentView.backgroundColor = kDefaultWhiteColor;
        newFrame = _contentView.frame;
        newFrame.size.height = _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding + 30;
        _contentView.frame = newFrame;
    }
    else {
        //--------------//
        // Setup Button //
        //--------------//
        DataManager *dataManager = [DataManager sharedInstance];
        
        UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
        UIButton *button = createButtonWithFrame(CGRectMake(_topInfoLabel.frame.origin.x,
                                                            CGRectGetMaxY(_fieldView.frame) + kDefaultComponentPadding,
                                                            120,
                                                            kDefaultButtonHeight),
                                                 [[Language get:@"inbox" alter:nil] uppercaseString],
                                                 buttonFont,
                                                 kDefaultWhiteColor,
                                                 colorWithHexString(dataManager.profileData.themesModel.menuFeature),
                                                 colorWithHexString(dataManager.profileData.themesModel.menuColor));
        [button addTarget:self action:@selector(performInbox) forControlEvents:UIControlEventTouchUpInside];
        _inboxButton = button;
        //NSLog(@"button frame %@", NSStringFromCGRect(button.frame));
        //NSLog(@"field view frame %@", NSStringFromCGRect(_fieldView.frame));
        [_contentView addSubview:_inboxButton];
        
        //--------------//
        // Content View //
        //--------------//
        _contentView.backgroundColor = kDefaultWhiteColor;
        newFrame = _contentView.frame;
        newFrame.size.height = _inboxButton.frame.origin.y + _inboxButton.frame.size.height + kDefaultComponentPadding + 50;
        _contentView.frame = newFrame;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    if(_displaySocialMediaButtons)
    {
        // TODO : V1.9
        // Loading Time for Buy Package
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if(appDelegate.dateToday)
        {
            NSTimeInterval interval = -[appDelegate.dateToday timeIntervalSinceNow];
            NSLog(@"buy package interval %f", interval);
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"MyXL IPA" interval:@((int)(interval * 1000)) name:@"Buy Package Time" label:nil] build]];
        }
    }
    
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.info = nil;
}

- (void)dealloc
{
    [_info release];
    _info = nil;
    [_twitter release];
    _twitter = nil;
    [_twitterModalView release];
    _twitterModalView = nil;
    [_twitterButton release];
    _twitterButton = nil;
    [_errorTwitLabel release];
    _errorTwitLabel = nil;
    [_twitterAuthDict release];
    _twitterAuthDict = nil;
    [_twitterTextView release];
    _twitterTextView = nil;
    [_packageName release];
    _packageName = nil;
    [super dealloc];
}

- (void)resizeLoginView:(FBLoginView*)loginview
{
    for(UIView * view in loginview.subviews)
    {
        if([view isKindOfClass:[UILabel class]])
        {
            view.frame = CGRectMake( view.frame.origin.x-23, view.frame.origin.y, view.frame.size.width/2, view.frame.size.height /2);
        }
        else
        {
            view.frame = CGRectMake( view.frame.origin.x, view.frame.origin.y, view.frame.size.width/2, view.frame.size.height /2);
        }
    }
    for(UILabel * label in loginview.subviews)
    {
        label.font = [UIFont systemFontOfSize:12];
    }
}

- (void)buttonClickHandler:(id)sender {
    
    //NSLog(@"button click");
    
    // get the app delegate so that we can access the session property
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    // this button's job is to flip-flop the session from open to closed
    if (appDelegate.session.isOpen) {
        // if a user logs out explicitly, we delete any cached token information, and next
        // time they run the applicaiton they will be presented with log in UX again; most
        // users will simply close the app or switch away, without logging out; this will
        // cause the implicit cached-token login to occur on next launch of the application
        //[appDelegate.session closeAndClearTokenInformation];
        
        //NSLog(@"Session Open");
        [self requestShareToFacebook];
    } else {
        //NSLog(@"Session Close");
        //NSLog(@"appDelegate.session.state = %d",appDelegate.session.state);
//        if (appDelegate.session.state != FBSessionStateCreated) {
            // Create a new, logged out session.
            //NSLog(@"Create Session");
//            appDelegate.session = [[FBSession alloc] init];
//        }
        
        // if the session isn't open, let's open it now and present the login UX to the user
        /*
        [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                         FBSessionState status,
                                                         NSError *error) {
            // and here we make sure to update our UX according to the new session state
            NSLog(@"perform share");
            [self performShare];
        }];*/
        
        [appDelegate.session openWithCompletionHandler:nil];
        [self performShare];
    }
}

-(void)shareFbClicked:(id)sender
{
    DataManager *sharedData = [DataManager sharedInstance];
    @try
    {
        if(sharedData.mappingSukaSukaData.sosmed)
        {
            // Put together the dialog parameters
            NSDictionary *sosmedDict = sharedData.mappingSukaSukaData.sosmed;
//            NSString *fbDesc = [[sosmedDict objectForKey:SOSMED_DESC_FB] stringByReplacingOccurrencesOfString:@"'(pkg_name)'" withString:_packageName];
//            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                           [sosmedDict objectForKey:SOSMED_TITLE_FB], @"name",
//                                           @" ", @"caption",
//                                           fbDesc, @"description",
//                                           [sosmedDict objectForKey:SOSMED_URL], @"link",
//                                           [sosmedDict objectForKey:SOSMED_URL_LOGO], @"picture",
//                                           @"1714111455481952",@"app_id",
//                                           nil];
            
            if(!IS_IPAD)
            {
                // Show the feed dialog
//                [FBWebDialogs presentFeedDialogModallyWithSession:nil
//                                                       parameters:params
//                                                          handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
//                                                              if (error) {
//                                                                  // An error occurred, we need to handle the error
//                                                                  // See: https://developers.facebook.com/docs/ios/errors
//                                                                  NSLog(@"Error publishing story: %@", error.description);
//                                                              } else {
//                                                                  if (result == FBWebDialogResultDialogNotCompleted) {
//                                                                      // User cancelled.
//                                                                      NSLog(@"User cancelled.");
//                                                                  } else {
//                                                                      // Handle the publish feed callback
//                                                                      NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
//                                                                      
//                                                                      if (![urlParams valueForKey:@"post_id"]) {
//                                                                          // User cancelled.
//                                                                          NSLog(@"User cancelled.");
//                                                                          
//                                                                      } else {
//                                                                          // User clicked the Share button
//                                                                          NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
//                                                                          NSLog(@"result %@", result);
//                                                                      }
//                                                                  }
//                                                              }
//                                                          }];
            }
            else
            {
                if(NSClassFromString(@"SLComposeViewController") && [SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
                {
                    NSDictionary *sosmedDict = sharedData.mappingSukaSukaData.sosmed;
                    NSString *shareContent = [sosmedDict objectForKey:SOSMED_DESC_FB];
                    shareContent = [shareContent stringByReplacingOccurrencesOfString:@"'(pkg_name)'" withString:_packageName];
                    
                    SLComposeViewController *facebookPostSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                    [facebookPostSheet setInitialText:shareContent];
                    [facebookPostSheet addURL:[NSURL URLWithString:[sosmedDict objectForKey:SOSMED_URL]]];
                    NSData *dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:[sosmedDict objectForKey:SOSMED_URL_LOGO]]];
                    UIImage *image = [UIImage imageWithData:dataImage];
                    [facebookPostSheet addImage:image];
                    [self presentViewController:facebookPostSheet animated:YES completion:nil];
                }
                else
                {
                    int lang = [Language getCurrentLanguage];
                    NSString *title = @"";
                    NSString *message = @"";
                    if(lang == 1)
                    {
                        title = @"Maaf";
                        message = @"Anda tidak dapat mengirimkan post ke Facebook. Pastikan perangkat anda memiliki akun Facebook yang terkonfigurasi";
                    }
                    else
                    {
                        title = @"Sorry";
                        message = @"You can't send a post right now, make sure your device has at least one Facebook account setup.";
                    }
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:[Language get:@"ok" alter:nil] otherButtonTitles:nil, nil];
                    [alertView show];
                    [alertView release];
                    alertView = nil;
                }
            }
        }
        else
        {
            int lang = [Language getCurrentLanguage];
            NSString *title = @"";
            NSString *message = @"";
            if(lang == 1)
            {
                title = @"Maaf";
                message = @"Anda tidak dapat mengirimkan post ke Facebook. Pastikan perangkat anda memiliki akun Facebook yang terkonfigurasi";
            }
            else
            {
                title = @"Sorry";
                message = @"You can't send a post right now, make sure your device has at least one Facebook account setup.";
            }
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:[Language get:@"ok" alter:nil] otherButtonTitles:nil, nil];
            [alertView show];
            [alertView release];
            alertView = nil;
        }

    }
    @catch (NSException *exception) {
        NSLog(@"exception %@", [exception reason]);
    }
    
}

-(NSDictionary *)parseURLParams:(NSString *)query
{
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

-(void)shareTwitterClicked:(id)sender
{
    @try {
        if(NSClassFromString(@"SLComposeViewController") && [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            DataManager *sharedData = [DataManager sharedInstance];
            NSDictionary *sosmedDict = sharedData.mappingSukaSukaData.sosmed;
            
            /*
             * Using Tiny URL
             */
            NSString *apiEndpoint = [NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@",[sosmedDict objectForKey:SOSMED_URL]];
            NSString *shortURL = [NSString stringWithContentsOfURL:[NSURL URLWithString:apiEndpoint]
                                                          encoding:NSASCIIStringEncoding
                                                             error:nil];
            NSString *shareContent = [[sosmedDict objectForKey:SOSMED_DESC_TW] stringByReplacingOccurrencesOfString:[sosmedDict objectForKey:SOSMED_URL] withString:shortURL];
            shareContent = [shareContent stringByReplacingOccurrencesOfString:@"'(pkg_name)'" withString:_packageName];
            //----------------
            
            //NSString *shareContent = [sosmedDict objectForKey:SOSMED_DESC_TW];
            //shareContent = [shareContent stringByReplacingOccurrencesOfString:@"'(pkg_name)'" withString:_packageName];
            
            SLComposeViewController *twitterPostSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [twitterPostSheet setInitialText:shareContent];
            
            [self presentViewController:twitterPostSheet animated:YES completion:nil];
        }
        else
        {
            int lang = [Language getCurrentLanguage];
            NSString *title = @"";
            NSString *message = @"";
            if(lang == 1)
            {
                title = @"Maaf";
                message = @"Anda tidak dapat mengirimkan post ke Twitter. Pastikan perangkat anda memiliki akun Twitter yang terkonfigurasi";
            }
            else
            {
                title = @"Sorry";
                message = @"You can't send a post right now, make sure your device has at least one Twitter account setup.";
            }
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:[Language get:@"ok" alter:nil] otherButtonTitles:nil, nil];
            [alertView show];
            [alertView release];
            alertView = nil;
            
            //        _twitter = [STTwitterAPI twitterAPIWithOAuthConsumerKey:TWITTER_CONSUMER_KEY
            //                                                 consumerSecret:TWITTER_SECRET_KEY];
            //
            //        [_twitter postTokenRequest:^(NSURL *url, NSString *oauthToken) {
            //            NSLog(@"-- url: %@", url);
            //            NSLog(@"-- oauthToken: %@", oauthToken);
            //            [self setupTwitterLoginViewWithURL:url];
            //        } authenticateInsteadOfAuthorize:NO
            //                        forceLogin:@(YES)
            //                        screenName:nil
            //                     oauthCallback:@"MyXL://twitter_access_tokens/"
            //                        errorBlock:^(NSError *error) {
            //                            NSLog(@"-- error: %@", error);
            //                        }];
            //        
            //        [_twitter retain];
        }

    }
    @catch (NSException *exception) {
        
    }
   
}

-(void)setupTwitterLoginViewWithURL:(NSURL *)url
{
    if(_twitterModalView != nil)
    {
        _twitterModalView = nil;
    }
    
    UIView *twitterView = [[[NSBundle mainBundle] loadNibNamed:@"TwitterWebview" owner:self options:nil] objectAtIndex:0];
    UIWebView *webView = (UIWebView *)[twitterView viewWithTag:1];
    webView.delegate = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    
    _twitterModalView = [[RNBlurModalView alloc] initWithView:twitterView];
    [_twitterModalView hideCloseButton:NO];
    _twitterModalView.dismissOnOutsideTouch = NO;
    [_twitterModalView show];
}

-(void)twitterAuthenticated:(NSNotification *)notification
{
    [_twitterModalView hide];
    _twitterModalView = nil;
    _twitterAuthDict = [[NSDictionary alloc] init];
    _twitterAuthDict = [notification.userInfo copy];
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSDictionary *sosmedDict = sharedData.mappingSukaSukaData.sosmed;
    //NSLog(@"sosmed dict %@", sosmedDict);

//    NSString *apiEndpoint = [NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@",[sosmedDict objectForKey:SOSMED_URL]];
//    NSString *shortURL = [NSString stringWithContentsOfURL:[NSURL URLWithString:apiEndpoint]
//                                                  encoding:NSASCIIStringEncoding
//                                                     error:nil];
    //    NSString *shareContent = [[sosmedDict objectForKey:SOSMED_DESC_TW] stringByReplacingOccurrencesOfString:[sosmedDict objectForKey:SOSMED_URL] withString:shortURL];
    
    if(sosmedDict)
    {
        NSString *shareContent = [sosmedDict objectForKey:SOSMED_DESC_TW];
        shareContent = [shareContent stringByReplacingOccurrencesOfString:@"'(pkg_name)'" withString:_packageName];
        
        [self createTwitterPostViewWithContent:shareContent];
    }
}

-(void)postToTwitter
{
    if([_twitterTextView.text length] <= 140)
    {
        if([_twitter isKindOfClass:[STTwitterAPI class]])
        {
            if([_twitter respondsToSelector:@selector(postStatusUpdate:inReplyToStatusID:latitude:longitude:placeID:displayCoordinates:trimUser:successBlock:errorBlock:)])
            {
                [_twitter postAccessTokenRequestWithPIN:[_twitterAuthDict objectForKey:@"verifier"] successBlock:^(NSString *oauthToken, NSString *oauthTokenSecret, NSString *userID, NSString *screenName) {
                    [_twitter postStatusUpdate:_twitterTextView.text
                             inReplyToStatusID:nil
                                      latitude:nil
                                     longitude:nil
                                       placeID:nil
                            displayCoordinates:nil
                                      trimUser:nil
                                  successBlock:^(NSDictionary *status) {
                                      NSLog(@"-- statuses: %@", status);
                                  } errorBlock:^(NSError *error) {
                                      NSLog(@"error %@", [error description]);
                                  }];
                    
                } errorBlock:^(NSError *error) {
                    NSLog(@"-- %@", [error localizedDescription]);
                }];
                
                [_twitterModalView hide];
                _twitterModalView = nil;
            }
        }
    }
    else
    {
        _errorTwitLabel.hidden = NO;
    }
}

-(void)createTwitterPostViewWithContent:(NSString *)shareContent
{
    UIColor *twitButtonColor = [UIColor colorWithRed:77/255.0 green:155/255.0 blue:215/255.0 alpha:1];
    if(_twitterModalView != nil)
    {
        _twitterModalView = nil;
    }
    
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    UIView *twitterPostView = [[[NSBundle mainBundle] loadNibNamed:@"TwitterPostView" owner:self options:nil] objectAtIndex:0];
    UITextView *textView = (UITextView *)[twitterPostView viewWithTag:1];
    textView.text = shareContent;
    textView.font = buttonFont;
    textView.layer.borderWidth = 1;
    textView.layer.borderColor = kDefaultBlackColor.CGColor;
    textView.delegate = self;
    _twitterTextView = textView;
    UIButton *btn = (UIButton *)[twitterPostView viewWithTag:2];
    UIButton *btn2 = (UIButton *)[twitterPostView viewWithTag:3];
    
    UIButton *cancelButton = createButtonWithFrame(CGRectMake(CGRectGetMinX(btn.frame),
                                                              CGRectGetMinY(btn.frame),
                                                              CGRectGetWidth(btn.frame),
                                                              kDefaultButtonHeight),
                                                   [[Language get:@"cancel" alter:nil] uppercaseString],
                                                   buttonFont,
                                                   kDefaultWhiteColor,
                                                   twitButtonColor,
                                                   twitButtonColor);
    [cancelButton addTarget:self action:@selector(dismissTwitterPostView:) forControlEvents:UIControlEventTouchUpInside];
    [twitterPostView addSubview:cancelButton];
    [cancelButton release];
    cancelButton = nil;
    
    UIButton *postButton = createButtonWithFrame(CGRectMake(CGRectGetMinX(btn2.frame),
                                                    CGRectGetMinY(btn2.frame),
                                                    CGRectGetWidth(btn2.frame),
                                                    kDefaultButtonHeight),
                                         [[Language get:@"Tweet" alter:nil] uppercaseString],
                                         buttonFont,
                                         kDefaultWhiteColor,
                                         twitButtonColor,
                                         twitButtonColor);
    [postButton addTarget:self action:@selector(postToTwitter) forControlEvents:UIControlEventTouchUpInside];
    [twitterPostView addSubview:postButton];
    [postButton release];
    postButton = nil;
    
    UILabel *errorLabel = (UILabel *)[twitterPostView viewWithTag:4];
    errorLabel.font = buttonFont;
    _errorTwitLabel = errorLabel;
    
    _twitterModalView = [[RNBlurModalView alloc] initWithView:twitterPostView];
    [twitterPostView release];
    twitterPostView = nil;
    
    [_twitterModalView hideCloseButton:NO];
    _twitterModalView.dismissOnOutsideTouch = NO;
    [_twitterModalView show];
}

-(void)dismissTwitterPostView:(id)sender
{
    [_twitterModalView hide];
    _twitterModalView = nil;
}

- (void)setOAuthToken:(NSString *)token oauthVerifier:(NSString *)verifier {
    
    // in case the user has just authenticated through WebViewVC
    [_twitterModalView hide];
    _twitterModalView = nil;
    
    DataManager *sharedData = [DataManager sharedInstance];
    NSDictionary *sosmedDict = sharedData.mappingSukaSukaData.sosmed;
    
    [_twitter postStatusUpdate:[sosmedDict objectForKey:SOSMED_DESC_TW]
             inReplyToStatusID:nil
                      latitude:nil
                     longitude:nil
                       placeID:nil
            displayCoordinates:nil
                      trimUser:nil
                  successBlock:^(NSDictionary *status) {
                      NSLog(@"-- statuses: %@", status);
                  } errorBlock:^(NSError *error) {
                      NSLog(@"error %@", [error description]);
                  }];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if(_errorTwitLabel != nil)
        _errorTwitLabel.hidden = YES;
}

-(void)sessionOpener
{
//    NSLog(@"%d", FBSession.activeSession.isOpen);
//    NSLog(@"%@", FBSession.activeSession.description );
}

- (void)performShare {
    //NSLog(@"Perform Share");
    /*
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    if (appDelegate.session.isOpen) {
        [self requestShareToFacebook];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:@"Can't share to Facebook"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
     */
    
    [self requestShareToFacebook];
}

- (void)requestShareToFacebook {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = SHARE_FB_REQ;
    
    DataManager *sharedData = [DataManager sharedInstance];
    [request shareToFacebook:sharedData.profileData.token
                  withMsisdn:sharedData.profileData.msisdn
                   withTrxId:self.trxId];
}

- (void)performInbox {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = NOTIFICATION_REQ;
    [request notification];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == SHARE_FB_REQ) {
            //NSLog(@"Share FB Berhasil");
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            [notificationViewController createBarButtonItem:BACK];
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end
