//
//  AccountActivationViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/23/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "AccountActivationViewController.h"

#import "Constant.h"
#import "AXISnetCommon.h"
#import <QuartzCore/QuartzCore.h>

#import "RegisterViewController.h"
#import "ResendPinViewController.h"
#import "AXISnetRequest.h"

#import "AXISnetNavigationBar.h"
#import "SectionHeaderView.h"

#import "DataManager.h"

@interface AccountActivationViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *asteriskLabel;

@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UILabel *pinLabel;
@property (strong, nonatomic) IBOutlet UITextField *pinTextField;
@property (strong, nonatomic) UIButton *activationButton;
@property (strong, nonatomic) UIButton *loginButton;
@property (strong, nonatomic) UIButton *registerButton;
@property (strong, nonatomic) UIButton *resendPinButton;

- (void)performAccountActivation:(id)sender;
- (void)performLogin:(id)sender;
- (void)performRegister:(id)sender;
- (void)performResendPin:(id)sender;
- (void)activateAccount;

@end

@implementation AccountActivationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    //---------------------//
    // Show Navigation Bar //
    //---------------------//
    AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[self.navigationController navigationBar];
    navBar.hidden = NO;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *documentsDirectory = applicationDocumentsDirectory();
    NSString *logoName = @"";
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else if ([sharedData.profileData.telcoOperator isEqualToString:@"AXIS"]) {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    else {
        logoName = XL_LOGO_RETINA_NAME;
    }
    NSString *mediaPath = [documentsDirectory stringByAppendingPathComponent:logoName];
    [navBar updateLogo:mediaPath];
    //--
    
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:[[Language get:@"my_account" alter:nil] uppercaseString]
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:[[Language get:@"account_activation" alter:nil] uppercaseString]
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    
    _asteriskLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _asteriskLabel.textColor = kDefaultNavyBlueColor;
    _asteriskLabel.text = [Language get:@"asterisk" alter:nil];
    
    _usernameLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _usernameLabel.textColor = kDefaultTitleFontGrayColor;
    _usernameLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"your_axis_number" alter:nil]];
    
    _pinLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _pinLabel.textColor = kDefaultTitleFontGrayColor;
    _pinLabel.text = [NSString stringWithFormat:@"%@*",[[Language get:@"pin" alter:nil] uppercaseString]];
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Setup Button //
    //--------------//
    // Activation Button
    UIFont *titleFont = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize];
    UIButton *button = createButtonWithFrame(CGRectMake(_pinTextField.frame.origin.x,
                                                        _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             [[Language get:@"activate_account" alter:nil] uppercaseString],
                                             titleFont,
                                             kDefaultWhiteColor,
                                             kDefaultAquaMarineColor,
                                             kDefaultBlueColor);
    [button addTarget:self action:@selector(performAccountActivation:) forControlEvents:UIControlEventTouchUpInside];
    _activationButton = button;
    [_contentView addSubview:_activationButton];
    
    // Login Button
    CGFloat padding = 10.0;
    CGFloat contentWidth = _contentView.frame.size.width - (padding*2);
    
    button = createButtonWithIconAndArrow(CGRectMake(padding, _activationButton.frame.origin.y + _activationButton.frame.size.height + 20.0, contentWidth, kDefaultButtonHeight),
                                          @"F",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          [[Language get:@"login" alter:nil] uppercaseString],
                                          [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          @"d",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          kDefaultAquaMarineColor,
                                          kDefaultBlueColor);
    
    [button addTarget:self
               action:@selector(performLogin:)
     forControlEvents:UIControlEventTouchDown];
    
    _loginButton = button;
    [_contentView addSubview:_loginButton];
    
    // Register Button
    button = createButtonWithIconAndArrow(CGRectMake(padding, _loginButton.frame.origin.y + _loginButton.frame.size.height + 4.0, contentWidth, kDefaultButtonHeight),
                                          @"A",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          [[Language get:@"registration" alter:nil] uppercaseString],
                                          [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          @"d",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          kDefaultAquaMarineColor,
                                          kDefaultBlueColor);
    
    [button addTarget:self
               action:@selector(performRegister:)
     forControlEvents:UIControlEventTouchDown];
    
    _registerButton = button;
    [_contentView addSubview:_registerButton];
    
    // Resend PIN
    /*
    button = createButtonWithIconAndArrow(CGRectMake(padding, _registerButton.frame.origin.y + _registerButton.frame.size.height + 4.0, contentWidth, kDefaultButtonHeight),
                                          @"U",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          [[Language get:@"ask_lost_pin" alter:nil] uppercaseString],
                                          [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          @"d",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          kDefaultAquaMarineColor,
                                          kDefaultBlueColor);
    
    [button addTarget:self
               action:@selector(performResendPin:)
     forControlEvents:UIControlEventTouchDown];
    
    _resendPinButton = button;
    [_contentView addSubview:_resendPinButton];*/
    
    
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultWhiteColor;
    newFrame = _contentView.frame;
    newFrame.size.height = _registerButton.frame.origin.y + _registerButton.frame.size.height + kDefaultComponentPadding + 44 + 10;
    _contentView.frame = newFrame;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 300.0);
    
    [headerView1 release];
    [headerView2 release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Selector

- (void)performAccountActivation:(id)sender {
    int valid = [self validateInput];
    if (valid == 2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"empty_validation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }
    else {
        [self activateAccount];
    }
}

- (int)validateInput {
    if ([_usernameTextField.text length] <= 0 ||
        [_pinTextField.text length] <= 0) {
        return 2;
    }
    else {
        return 0;
    }
}

- (void)performLogin:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)performRegister:(id)sender {
    RegisterViewController *registerViewController = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:registerViewController animated:YES];
    registerViewController = nil;
    [registerViewController release];
}

- (void)performResendPin:(id)sender {
    ResendPinViewController *resendPinViewController = [[ResendPinViewController alloc] initWithNibName:@"ResendPinViewController" bundle:nil];
    [self.navigationController pushViewController:resendPinViewController animated:YES];
    resendPinViewController = nil;
    [resendPinViewController release];
}

- (void)activateAccount {
    [_usernameTextField resignFirstResponder];
    [_pinTextField resignFirstResponder];
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    int currentLang = [Language getCurrentLanguage];
    NSString *lang = @"";
    if (currentLang == 0) {
        lang = @"en";
    }
    else {
        lang = @"id";
    }
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = ACTIVATION_REQ;
    [request activateMsisdn:_usernameTextField.text withPin:_pinTextField.text withLanguage:lang];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    
    NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
    int type = [typeNumber intValue];
    
    BOOL success = [reqResult boolValue];
    
    if (success) {
        if (type == ACTIVATION_REQ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[Language get:@"account_activation_success" alter:nil]
                                                           delegate:self
                                                  cancelButtonTitle:[[Language get:@"ok" alter:nil] uppercaseString]
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat targetY = -42 + textField.frame.origin.y;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 30);
}

@end
