//
//  DetailReloadBalanceViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "DetailReloadBalanceViewController.h"
#import "Constant.h"
#import <QuartzCore/QuartzCore.h>
#import "ContentView.h"
#import "SectionHeaderView.h"
#import "DataManager.h"
#import "EncryptDecrypt.h"
#import "AXISnetCommon.h"

@interface DetailReloadBalanceViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *msisdnTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *msisdnValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *nominalTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *nominalValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *balanceTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *balanceValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *activeDateTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *activeDateValueLabel;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;

@end

@implementation DetailReloadBalanceViewController

@synthesize headerTitle = _headerTitle;
@synthesize headerIcon = _headerIcon;

@synthesize msisdn = _msisdn;
@synthesize nominal = _nominal;
@synthesize balanceNew = _balanceNew;
@synthesize activeDateNew = _activeDateNew;

@synthesize isBalanceTransfer = _isBalanceTransfer;
@synthesize menuModel = _menuModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:_menuModel.groupName
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:_menuModel.menuName
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    _msisdnTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _msisdnTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _msisdnTitleLabel.text = [[Language get:@"xl_number" alter:nil] uppercaseString];
    
    _msisdnValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _msisdnValueLabel.textColor = kDefaultPurpleColor;
    
    _nominalTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _nominalTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _nominalTitleLabel.text = [[Language get:@"nominal" alter:nil] uppercaseString];
    
    _nominalValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _nominalValueLabel.textColor = kDefaultPurpleColor;
    
    _balanceTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _balanceTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _balanceTitleLabel.text = [[Language get:@"new_balance" alter:nil] uppercaseString];
    
    _balanceValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _balanceValueLabel.textColor = kDefaultPurpleColor;
    
    _activeDateTitleLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _activeDateTitleLabel.textColor = kDefaultTitleFontGrayColor;
    _activeDateTitleLabel.text = [[Language get:@"new_active_date" alter:nil] uppercaseString];
    
    _activeDateValueLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _activeDateValueLabel.textColor = kDefaultPurpleColor;
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    newFrame.size.height = _activeDateValueLabel.frame.origin.y + _activeDateValueLabel.frame.size.height;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultWhiteColor;
    newFrame = _contentView.frame;
    newFrame.size.height = _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding + 10;
    _contentView.frame = newFrame;
    
    if (_isBalanceTransfer) {
        NSString *saltKeyAPI = SALT_KEY;
        _msisdnTitleLabel.text = [[Language get:@"transfer_from" alter:nil] uppercaseString];
        _msisdnValueLabel.text = [EncryptDecrypt doCipherForiOS7:_msisdn action:kCCDecrypt withKey:saltKeyAPI];
        
        DataManager *sharedData = [DataManager sharedInstance];
        
        _nominalTitleLabel.text = [[Language get:@"balance_transfer_to" alter:nil] uppercaseString];
        _nominalValueLabel.text = [EncryptDecrypt doCipherForiOS7:_nominal action:kCCDecrypt withKey:saltKeyAPI];
        
        _balanceTitleLabel.text = [[Language get:@"transfer_amount" alter:nil] uppercaseString];
        _balanceValueLabel.text = addThousandsSeparator(_balanceNew, sharedData.profileData.language);
        
        _activeDateTitleLabel.text = [[Language get:@"transfer_charged" alter:nil] uppercaseString];
        _activeDateValueLabel.text = addThousandsSeparator(_activeDateNew, sharedData.profileData.language);
    }
    else {
        //NSString *saltKeyAPI = SALT_KEY;
        //_msisdnValueLabel.text = [EncryptDecrypt doCipherForiOS7:_msisdn action:kCCDecrypt withKey:saltKeyAPI];
        _msisdnValueLabel.text = _msisdn;
        
        
        DataManager *sharedData = [DataManager sharedInstance];
        _nominalValueLabel.text = addThousandsSeparator(_nominal, sharedData.profileData.language);
        
        _balanceValueLabel.text = addThousandsSeparator(_balanceNew, sharedData.profileData.language);
        
        _activeDateValueLabel.text = changeDateFormat(_activeDateNew);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.headerTitle = nil;
    self.headerIcon = nil;
    self.msisdn = nil;
    self.nominal = nil;
    self.balanceNew = nil;
    self.activeDateNew = nil;
}

- (void)dealloc {
    [_headerTitle release];
    _headerTitle = nil;
    [_headerIcon release];
    _headerIcon = nil;
    [_msisdn release];
    _msisdn = nil;
    [_nominal release];
    _nominal = nil;
    [_balanceNew release];
    _balanceNew = nil;
    [_activeDateNew release];
    _activeDateNew = nil;
    [super dealloc];
}

@end
