//
//  ReloadBalanceViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ReloadBalanceViewController.h"
#import "Constant.h"
#import <QuartzCore/QuartzCore.h>
#import "ContentView.h"
#import "SectionHeaderView.h"

#import "CustomButton.h"
#import "AXISnetCommon.h"
#import "DataManager.h"
#import "EncryptDecrypt.h"

#import "DetailReloadBalanceViewController.h"

#import "UnderLineLabel.h"

#import "NotificationViewController.h"

@interface ReloadBalanceViewController ()

- (IBAction)performChangeToMyAXIS:(id)sender;
- (IBAction)performChangeToOtherAXIS:(id)sender;
- (void)changeSentTo:(BOOL)isMyAXISSelected;
- (void)didTapMyAXISWithGesture:(UITapGestureRecognizer *)tapGesture;
- (void)didTapOtherAXISWithGesture:(UITapGestureRecognizer *)tapGesture;

- (void)performSubmit:(id)sender;
- (void)submit;

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *asteriskLabel;

@property (strong, nonatomic) IBOutlet UILabel *sentToLabel;
@property (strong, nonatomic) IBOutlet UILabel *yourAXISLabel;
@property (strong, nonatomic) IBOutlet UILabel *myMSISDNLabel;
@property (strong, nonatomic) IBOutlet UILabel *otherAXISLabel;
@property (strong, nonatomic) IBOutlet UITextField *otherMSISDNTF;
@property (strong, nonatomic) IBOutlet UILabel *suggestionLabel;
@property (strong, nonatomic) IBOutlet UITextField *voucherTF;
@property (strong, nonatomic) UIButton *submitButton;

@property (strong, nonatomic) IBOutlet UIButton *myAXISButton;
@property (strong, nonatomic) IBOutlet UIButton *otherAXISButton;

@property (strong, nonatomic) IBOutlet UILabel *captchaLabel;
@property (strong, nonatomic) IBOutlet UIWebView *captchaWebView;
@property (strong, nonatomic) IBOutlet UILabel *refreshLabel;
@property (strong, nonatomic) IBOutlet UnderLineLabel *recaptchaLabel;
@property (strong, nonatomic) IBOutlet UITextField *captchaTF;

@property (readwrite) BOOL isMyAXIS;

@property (strong, nonatomic) SectionHeaderView *sectionHeaderView;
// TODO : Hygiene
@property (nonatomic, retain) NSTimer *captchaTimer;

@end

@implementation ReloadBalanceViewController

@synthesize headerTitle = _headerTitle;
@synthesize headerIcon = _headerIcon;
@synthesize menuModel = _menuModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    /*
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    UIView *customView = [[UIView alloc] initWithFrame:applicationFrame];
    customView.backgroundColor = kDefaultBackgroundGrayColor;
    customView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:customView];*/
}

- (void)viewWillAppear:(BOOL)animated {
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:_menuModel.groupName
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:_menuModel.menuName
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    
    /*
     * Asterisk
     */
    _asteriskLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _asteriskLabel.textColor = kDefaultNavyBlueColor;
    _asteriskLabel.text = [Language get:@"asterisk" alter:nil];
    
    _sentToLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _sentToLabel.textColor = kDefaultTitleFontGrayColor;
    _sentToLabel.backgroundColor = [UIColor clearColor];
    _sentToLabel.text = [Language get:@"send_to" alter:nil];
    
    _yourAXISLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _yourAXISLabel.textColor = kDefaultNavyBlueColor;
    _yourAXISLabel.backgroundColor = [UIColor clearColor];
    _yourAXISLabel.text = [Language get:@"your_axis_number" alter:nil];
    
    _myMSISDNLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _myMSISDNLabel.textColor = kDefaultNavyBlueColor;
    _myMSISDNLabel.backgroundColor = [UIColor clearColor];
    DataManager *sharedData = [DataManager sharedInstance];
    NSString *saltKeyAPI = SALT_KEY;
    NSString *msisdn = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    if ([msisdn length] > 0) {
        msisdn = [msisdn stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"0"];
    }
    _myMSISDNLabel.text = msisdn;
    
    _otherAXISLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _otherAXISLabel.textColor = kDefaultNavyBlueColor;
    _otherAXISLabel.backgroundColor = [UIColor clearColor];
    _otherAXISLabel.text = [Language get:@"other_axis_number" alter:nil];
    
    _otherMSISDNTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _otherMSISDNTF.textColor = kDefaultNavyBlueColor;
    
    _suggestionLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _suggestionLabel.textColor = kDefaultTitleFontGrayColor;
    _suggestionLabel.backgroundColor = [UIColor clearColor];
    _suggestionLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"insert_voucher" alter:nil]];
    [_suggestionLabel sizeToFit];
    
    _voucherTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _voucherTF.textColor = kDefaultNavyBlueColor;
    _voucherTF.placeholder = [Language get:@"16_digits" alter:nil];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(didTapMyAXISWithGesture:)];
    [_yourAXISLabel setUserInteractionEnabled:YES];
    [_yourAXISLabel addGestureRecognizer:tapGesture];
    [tapGesture release];
    
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(didTapOtherAXISWithGesture:)];
    [_otherAXISLabel setUserInteractionEnabled:YES];
    [_otherAXISLabel addGestureRecognizer:tapGesture2];
    [tapGesture2 release];
    
    // Add by iNot 22 July 2013
    // Captcha
    _captchaLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _captchaLabel.textColor = kDefaultTitleFontGrayColor;
    NSString *text = [NSString stringWithFormat:@"%@*",[Language get:@"captcha_code" alter:nil]];
    _captchaLabel.text = text;
    
    // Refresh Label
    _refreshLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize];
    _refreshLabel.textColor = kDefaultNavyBlueColor;
    
    // Recaptcha Label
    _recaptchaLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _recaptchaLabel.textColor = kDefaultNavyBlueColor;
    _recaptchaLabel.text = [Language get:@"recaptcha" alter:nil];
    [_recaptchaLabel setShouldUnderline:NO];
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(refreshCaptcha)];
    [_recaptchaLabel setUserInteractionEnabled:YES];
    [_recaptchaLabel addGestureRecognizer:tapGesture3];
    [tapGesture3 release];
    
    _captchaTF.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _captchaTF.textColor = kDefaultNavyBlueColor;
    //_captchaTF.placeholder = [Language get:@"6_digits" alter:nil];
    
    //****** Request on 5 September 2013, Hide Captcha ******
    //_captchaLabel.hidden = YES;
    //_recaptchaLabel.hidden = YES;
    //_captchaWebView.hidden = YES;
    //_captchaTF.hidden = YES;
    //*******************************************************
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    newFrame.size.height = _captchaTF.frame.origin.y + _captchaTF.frame.size.height;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Setup Button //
    //--------------//
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    UIButton *button = createButtonWithFrame(CGRectMake(_captchaTF.frame.origin.x,
                                                        _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             [[Language get:@"submit" alter:nil] uppercaseString],
                                             buttonFont,
                                             kDefaultWhiteColor,
                                             kDefaultAquaMarineColor,
                                             kDefaultBlueColor);
    
    [button addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    _submitButton = button;
    [_contentView addSubview:_submitButton];
    
    //--------------//
    // Content View //
    //--------------//
    newFrame = _contentView.frame;
    newFrame.size.height = _submitButton.frame.origin.y + _submitButton.frame.size.height + kDefaultComponentPadding;
    _contentView.frame = newFrame;
    _contentView.backgroundColor = kDefaultWhiteColor;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 200.0);
    
    [headerView1 release];
    [headerView2 release];
    
    // Set Default to My AXIS
    [self performChangeToMyAXIS:nil];
    
    // Request Captcha
    [self refreshCaptcha];
    
    /*
    UITapGestureRecognizer *tapGesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(dismissKeyboard)];
    [_contentView setUserInteractionEnabled:YES];
    [_contentView addGestureRecognizer:tapGesture4];
    [tapGesture4 release];*/
}

// TODO : Hygiene
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_captchaTimer invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self.headerTitle = nil;
    self.headerIcon = nil;
}

- (void)dealloc {
    [_headerTitle release];
    _headerTitle = nil;
    [_headerIcon release];
    _headerIcon = nil;
    [super dealloc];
}

#pragma mark - Selector

- (IBAction)performChangeToMyAXIS:(id)sender {
    _isMyAXIS = YES;
    [self changeSentTo:_isMyAXIS];
}

- (void)didTapMyAXISWithGesture:(UITapGestureRecognizer *)tapGesture {
    _isMyAXIS = YES;
    [self changeSentTo:_isMyAXIS];
}

- (IBAction)performChangeToOtherAXIS:(id)sender {
    _isMyAXIS = NO;
    [self changeSentTo:_isMyAXIS];
}

- (void)didTapOtherAXISWithGesture:(UITapGestureRecognizer *)tapGesture {
    _isMyAXIS = NO;
    [self changeSentTo:_isMyAXIS];
}

- (void)changeSentTo:(BOOL)isMyAXISSelected {
    if (isMyAXISSelected) {
        [_myAXISButton setSelected:YES];
        [_otherAXISButton setSelected:NO];
        
        _myMSISDNLabel.hidden = NO;
        _otherMSISDNTF.hidden = YES;
    }
    else {
        [_myAXISButton setSelected:NO];
        [_otherAXISButton setSelected:YES];
        
        _myMSISDNLabel.hidden = YES;
        _otherMSISDNTF.hidden = NO;
    }
}

- (void)performSubmit:(id)sender {
    int valid = [self validateInput];
    if (valid == 2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"empty_validation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 99;
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
    else if (valid == 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"voucher_digit_validation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 99;
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
    else {
        NSString *msisdn = @"";
        
        if (_isMyAXIS) {
            msisdn = _myMSISDNLabel.text;
        }
        else {
            msisdn = _otherMSISDNTF.text;
        }
        
        NSString *message = [NSString stringWithFormat:@"%@ %@ %@ %@?",[Language get:@"reload_voucher_confirm_1" alter:nil], msisdn, [Language get:@"reload_voucher_confirm_2" alter:nil], _voucherTF.text];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                              otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString], nil];
        alert.tag = 200;
        [alert show];
        [alert release];
    }
}

- (int)validateInput {
    /*
     * Validate empty field
     */
    if (_isMyAXIS) {
        if ([_voucherTF.text length] <= 0 ||
            [_captchaTF.text length] <= 0) {
            return 2;
        }
        else if ([_voucherTF.text length] < 16) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        if ([_voucherTF.text length] <= 0 ||
            [_captchaTF.text length] <= 0 ||
            [_otherMSISDNTF.text length] <= 0) {
            return 2;
        }
        else if ([_voucherTF.text length] < 16) {
            return 1;
        }
        else {
            return 0;
        }
    }
}

- (void)submit {
    [_otherMSISDNTF resignFirstResponder];
    [_voucherTF resignFirstResponder];
    [_captchaTF resignFirstResponder];
    [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = TOPUP_VOUCHER_REQ;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    if (_isMyAXIS) {
        
        [request topUpVoucher:_voucherTF.text
                     msisdnTo:sharedData.profileData.msisdn
                  withCaptcha:_captchaTF.text
                      withCid:sharedData.cid];
        /*
        [request topUpVoucher:_voucherTF.text
                     msisdnTo:sharedData.profileData.msisdn
                  withCaptcha:@""
                      withCid:@""];*/
    }
    else {
        NSString *saltKeyAPI = SALT_KEY;
        NSString *msisdnToEncrypted = [EncryptDecrypt doCipherForiOS7:_otherMSISDNTF.text action:kCCEncrypt withKey:saltKeyAPI];
        //NSLog(@"msisdnToEncrypted = %@",msisdnToEncrypted);
        
        [request topUpVoucher:_voucherTF.text
                     msisdnTo:msisdnToEncrypted
                  withCaptcha:_captchaTF.text
                      withCid:sharedData.cid];
        /*
        [request topUpVoucher:_voucherTF.text
                     msisdnTo:msisdnToEncrypted
                  withCaptcha:@""
                      withCid:@""];*/
    }
}

- (void)refreshCaptcha {
    // TODO : Hygiene
    if(_captchaTimer != nil)
        [_captchaTimer invalidate];
    [self scheduleTimerForCaptcha];
    
    NSString *captchaURL = generateCaptchaURL();
    NSURL *captcha = [NSURL URLWithString:captchaURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:captcha];
    [_captchaWebView loadRequest:request];
    [request release];
}

// TODO : Hygiene
// Timer for captcha refresh
-(void)scheduleTimerForCaptcha
{
    NSDate *time = [NSDate dateWithTimeIntervalSinceNow:CAPTCHA_REFRESH_TIME];
    NSTimer *t = [[NSTimer alloc] initWithFireDate:time
                                          interval:CAPTCHA_REFRESH_TIME
                                            target:self
                                          selector:@selector(refreshCaptcha)
                                          userInfo:nil repeats:YES];
    
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer:t forMode: NSDefaultRunLoopMode];
    [t release];
}

/*
- (void)dismissKeyboard
{
    UITextField *activeTextField = nil;
    if ([_otherMSISDNTF isEditing]) activeTextField = _otherMSISDNTF;
    else if ([_voucherTF isEditing]) activeTextField = _voucherTF;
    else if ([_captchaTF isEditing]) activeTextField = _captchaTF;
    if (activeTextField) [activeTextField resignFirstResponder];
}*/

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == TOPUP_VOUCHER_REQ) {
            DetailReloadBalanceViewController *detailReloadBalanceViewController = [[DetailReloadBalanceViewController alloc] initWithNibName:@"DetailReloadBalanceViewController" bundle:nil];
            detailReloadBalanceViewController.headerTitle = _headerTitle;
            detailReloadBalanceViewController.headerIcon = _headerIcon;
            
            detailReloadBalanceViewController.menuModel = _menuModel;
            
            DataManager *sharedData = [DataManager sharedInstance];
            if (_isMyAXIS) {
                detailReloadBalanceViewController.msisdn = _myMSISDNLabel.text;
                //NSLog(@"MSISDN = %@",detailReloadBalanceViewController.msisdn);
            }
            else {
                detailReloadBalanceViewController.msisdn = _otherMSISDNTF.text;
                //NSLog(@"MSISDN = %@",detailReloadBalanceViewController.msisdn);
            }
            
            detailReloadBalanceViewController.nominal = sharedData.topUpVoucherModel.voucherValue;
            detailReloadBalanceViewController.balanceNew = sharedData.topUpVoucherModel.balanceNew;
            detailReloadBalanceViewController.activeDateNew = sharedData.topUpVoucherModel.activeStopNew;
            
            [detailReloadBalanceViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:detailReloadBalanceViewController animated:YES];
            detailReloadBalanceViewController = nil;
            [detailReloadBalanceViewController release];
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _captchaTF) {
        [textField resignFirstResponder];
        [_theScrollView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat targetY = -10 + textField.frame.origin.y;
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 16);
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // tag == 200 Submit Reload Voucher
    if (alertView.tag == 200) {
        if (buttonIndex == 0) {
            //NSLog(@"YES");
            [self submit];
        }
        else {
            //NSLog(@"NO");
        }
    }
}

@end
