//
//  ReloadBalanceViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/14/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AXISnetRequest.h"
#import "MenuModel.h"

@interface ReloadBalanceViewController : BasicViewController <UITextFieldDelegate, AXISnetRequestDelegate, UIAlertViewDelegate> {
    NSString *_headerTitle;
    NSString *_headerIcon;
    MenuModel *_menuModel;
}

@property (nonatomic, retain) NSString *headerTitle;
@property (nonatomic, retain) NSString *headerIcon;
@property (nonatomic, retain) MenuModel *menuModel;

@end
