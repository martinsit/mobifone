//
//  DetailReloadBalanceViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/31/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "MenuModel.h"

@interface DetailReloadBalanceViewController : BasicViewController {
    NSString *_headerTitle;
    NSString *_headerIcon;
    
    NSString *_msisdn;
    NSString *_nominal;
    NSString *_balanceNew;
    NSString *_activeDateNew;
    
    BOOL _isBalanceTransfer;
    MenuModel *_menuModel;
}

@property (nonatomic, retain) NSString *headerTitle;
@property (nonatomic, retain) NSString *headerIcon;

@property (nonatomic, retain) NSString *msisdn;
@property (nonatomic, retain) NSString *nominal;
@property (nonatomic, retain) NSString *balanceNew;
@property (nonatomic, retain) NSString *activeDateNew;

@property (readwrite) BOOL isBalanceTransfer;
@property (nonatomic, retain) MenuModel *menuModel;

@end
