//
//  CustArrowTableViewCell.h
//  MyXL
//
//  Created by Martin Partahi Sitorus on 1/8/16.
//  Copyright (c) 2016 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustArrowTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIImageView *itemImageView;
@property (retain, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (retain, nonatomic) IBOutlet UIImageView *arrowImageLabel;

@end
