//
//  DetailTitleTableViewCell.m
//  MyXL
//
//  Created by Martin Partahi Sitorus on 1/8/16.
//  Copyright (c) 2016 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "DetailTitleTableViewCell.h"

@implementation DetailTitleTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_lblTitle release];
    [_imageTitle release];
    [super dealloc];
}
@end
