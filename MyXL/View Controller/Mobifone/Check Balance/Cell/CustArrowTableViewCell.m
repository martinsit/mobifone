//
//  CustArrowTableViewCell.m
//  MyXL
//
//  Created by Martin Partahi Sitorus on 1/8/16.
//  Copyright (c) 2016 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CustArrowTableViewCell.h"

@implementation CustArrowTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    _itemImageView.image = [_itemImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_itemImageView setTintColor:[UIColor whiteColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_itemImageView release];
    [_itemNameLabel release];
    [_arrowImageLabel release];
    [super dealloc];
}
@end
