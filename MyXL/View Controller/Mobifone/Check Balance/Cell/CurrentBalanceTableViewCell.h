//
//  CurrentBalanceTableViewCell.h
//  MyXL
//
//  Created by Martin Partahi Sitorus on 1/8/16.
//  Copyright (c) 2016 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrentBalanceTableViewCell : UITableViewCell

@end
