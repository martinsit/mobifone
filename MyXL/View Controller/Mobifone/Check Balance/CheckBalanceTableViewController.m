//
//  CheckBalanceTableViewController.m
//  MyXL
//
//  Created by Martin Partahi Sitorus on 1/8/16.
//  Copyright (c) 2016 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "CheckBalanceTableViewController.h"

#import "CurrentBalanceTableViewCell.h"
#import "CustArrowTableViewCell.h"
#import "DetailBalanceViewController.h"

@interface CheckBalanceTableViewController ()
@property (retain, nonatomic) IBOutlet UITableView *theTable;
@property (retain, nonatomic) NSArray *menuArray;
@property (retain, nonatomic) NSArray *menuDictionary;

@end

@implementation CheckBalanceTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_theTable registerNib:[UINib nibWithNibName:@"CurrentBalanceTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"Balance"];
    [_theTable registerNib:[UINib nibWithNibName:@"CustArrowTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CustomArrow"];
    _theTable.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture.png"]];
    
    _menuArray = [[NSArray alloc]initWithObjects:@"Buy Package",@"Reload Voucher", nil];
    _menuDictionary = [[NSArray alloc]initWithObjects:@"Utilization", @"Purchase Package", @"Reload Voucher", nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return section;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@" Section : %ld",(long)indexPath.section);
    NSLog(@"Row : %ld",(long)indexPath.row);
    
    if (indexPath.section == 1 && indexPath.row == 0) {
        static NSString *cellIdentifier = @"Balance";
        CurrentBalanceTableViewCell *cell = (CurrentBalanceTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        return cell;
    } else if (indexPath.section == 2){
    
        static NSString *cellIdentifier = @"CustomArrow";
        CustArrowTableViewCell *cell = (CustArrowTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        NSString *itemName = [_menuArray objectAtIndex:indexPath.row];
        
        if (indexPath.row == 0) {
            cell.backgroundColor = [UIColor colorWithRed:218.0/255.0 green:37.0/255.0 blue:29.0/255.0 alpha:1.0];
            cell.itemImageView.image = [UIImage imageNamed:@"buy_package_down_ico.png"];
        } else if (indexPath.row == 1)
        {
            cell.backgroundColor = [UIColor colorWithRed:193.0/255.0 green:33.0/255.0 blue:26.0/255.0 alpha:1.0];
            cell.itemImageView.image = [UIImage imageNamed:@"reload_balance_ico.png"];
        }

        cell.itemNameLabel.text = itemName;
        
    return cell;
    } else if (indexPath.section == 3)
    {
        static NSString *cellIdentifier = @"CustomArrow";
        CustArrowTableViewCell *cell = (CustArrowTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        NSString *itemName = [_menuDictionary objectAtIndex:indexPath.row];
        
        cell.itemNameLabel.text = itemName;
        
        if (indexPath.row == 0) {
            cell.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:208.0/255.0 alpha:1.0];
                        cell.itemImageView.image = [UIImage imageNamed:@"utilization_down_ico.png"];
        } else if (indexPath.row == 1)
        {
            cell.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:105.0/255.0 blue:189.0/255.0 alpha:1.0];
                        cell.itemImageView.image = [UIImage imageNamed:@"purchase_package_down_ico.png"];
        } else if (indexPath.row == 2)
        {
            cell.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:93.0/255.0 blue:167.0/255.0 alpha:1.0];
                        cell.itemImageView.image = [UIImage imageNamed:@"reload_balance_ico.png"];
        }
        
        return cell;
    }
    
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0) {
        return 60.0;
    }else {
        return 44.0;
    }
    return 0.0;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        NSString *sectTitle = @"Setting";
        return sectTitle;
    } else if (section == 3)
    {
        NSString *sectTitle = @"History";
        return sectTitle;
    }
    return @"";
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3) {
        DetailBalanceViewController *detailViewController = [[DetailBalanceViewController alloc] initWithNibName:@"DetailBalanceViewController" bundle:nil];
        
        if (indexPath.row == 0) {
            detailViewController.dummyTitle = @"Utilization";
            detailViewController.dummyImage = [UIImage imageNamed:@"utilization_down_ico.png"];
        } else if (indexPath.row == 1)
        {
            detailViewController.dummyTitle = @"Purchase Package";
            detailViewController.dummyImage = [UIImage imageNamed:@"purchase_package_down_ico.png"];
        } else if (indexPath.row == 2)
        {
            detailViewController.dummyTitle = @"Purchase Package";
            detailViewController.dummyImage = [UIImage imageNamed:@"reload_balance_ico.png"];
        }
        // Pass the selected object to the new view controller.
        
        // Push the view controller.
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(20, 8, 320, 15);
    myLabel.font = [UIFont boldSystemFontOfSize:15];
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    UIView *headerView = [[UIView alloc] init];
    headerView.frame = CGRectMake(20, 8, 320, 15);
    [headerView addSubview:myLabel];
    
    return headerView;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_theTable release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTheTable:nil];
    [super viewDidUnload];
}
@end
