//
//  DetailBalanceViewController.h
//  MyXL
//
//  Created by Martin Partahi Sitorus on 1/8/16.
//  Copyright (c) 2016 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailBalanceViewController : UIViewController

@property (retain,nonatomic) NSString *dummyTitle;
@property (retain,nonatomic) UIImage *dummyImage;

@end
