//
//  DetailBalanceViewController.m
//  MyXL
//
//  Created by Martin Partahi Sitorus on 1/8/16.
//  Copyright (c) 2016 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "DetailBalanceViewController.h"
#import "CurrentBalanceTableViewCell.h"
#import "DetailTitleTableViewCell.h"


@interface DetailBalanceViewController ()
@property (retain, nonatomic) IBOutlet UITableView *theTable;

@end

@implementation DetailBalanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_theTable registerNib:[UINib nibWithNibName:@"CurrentBalanceTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"Balance"];
    [_theTable registerNib:[UINib nibWithNibName:@"DetailTitleTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"Title"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 20;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSLog(@"Row : %ld",(long)indexPath.row);
    
    if (indexPath.row == 0 ) {
        static NSString *cellIdentifier = @"Balance";
        CurrentBalanceTableViewCell *cell = (CurrentBalanceTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        return cell;
    } else if (indexPath.row == 1)
    {
        static NSString *cellIdentifier = @"Title";
        DetailTitleTableViewCell *cell = (DetailTitleTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        cell.lblTitle.text = _dummyTitle;
        cell.imageTitle.image = _dummyImage;
        return cell;
    }
    else
    {
        static NSString *simpleTextName = @"Simple_Text_Cell_Style";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTextName];
        
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTextName];
        }
        
        NSString *colorName = @"GoallIIIeeee";
        NSString *title = [colorName capitalizedString];
        cell.textLabel.text = title;
        return cell;
    }
    
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 60.0;
    }else {
        return 44.0;
    }
    return 0.0;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_theTable release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTheTable:nil];
    [super viewDidUnload];
}
@end
