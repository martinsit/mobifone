//
//  UpdateProfileViewController.h
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/21/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AXISnetRequest.h"
#import "MenuModel.h"

typedef enum {
    LANG = 0,
    GENDER,
    BIRTHDAY
}PickerType;

@interface UpdateProfileViewController : BasicViewController <UITextFieldDelegate, AXISnetRequestDelegate, UIAlertViewDelegate> {
    PickerType pickerType;
    MenuModel *_menuModel;
}

@property (nonatomic, retain) MenuModel *menuModel;

@end
