//
//  UpdateProfileViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/21/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "UpdateProfileViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

#import "DataManager.h"
#import "EncryptDecrypt.h"

#import "ActionSheetStringPicker.h"

#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"

#import "NotificationViewController.h"

#import "ActionSheetDatePicker.h"

@interface UpdateProfileViewController () <FBLoginViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *asteriskLabel;

@property (strong, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (strong, nonatomic) IBOutlet UITextField *fullNameTextField;

@property (strong, nonatomic) IBOutlet UILabel *genderLabel;
@property (strong, nonatomic) IBOutlet UIButton *genderButton;

@property (strong, nonatomic) IBOutlet UILabel *placeOfBirthLabel;
@property (strong, nonatomic) IBOutlet UITextField *placeOfBirthTextField;

@property (strong, nonatomic) IBOutlet UILabel *birthdayLabel;
@property (strong, nonatomic) IBOutlet UIButton *birthdayButton;

@property (strong, nonatomic) IBOutlet UILabel *ktpLabel;
@property (strong, nonatomic) IBOutlet UITextField *ktpTextField;

@property (strong, nonatomic) IBOutlet UILabel *noKtpLabel;
@property (strong, nonatomic) IBOutlet UITextField *noKtpTextField;

@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UITextField *address1TextField;
@property (strong, nonatomic) IBOutlet UITextField *address2TextField;
@property (strong, nonatomic) IBOutlet UITextField *address3TextField;

@property (strong, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;

@property (strong, nonatomic) IBOutlet UILabel *otherNumberLabel;
@property (strong, nonatomic) IBOutlet UITextField *otherNumberTextField;

//-------------
@property (strong, nonatomic) UIButton *submitButton;

//mobifone
@property (strong,nonatomic) UIButton *editedProfileButton;

//@property (strong, nonatomic) UIActionSheet *langActionSheet;
//@property (strong, nonatomic) UIPickerView *langPickerView;
//@property (strong, nonatomic) NSArray *lang;
//@property (readwrite) int rowLang;
//@property (strong, nonatomic) NSString *selectedLang;

//@property (strong, nonatomic) UIActionSheet *genderActionSheet;
//@property (strong, nonatomic) UIPickerView *genderPickerView;
@property (strong, nonatomic) NSArray *genders;
@property (readwrite) int rowGender;
@property (strong, nonatomic) NSString *selectedGender;

//@property (strong, nonatomic) UIActionSheet *birthdayActionSheet;
//@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) NSDate *dateValue;

//@property (strong, nonatomic) id<FBGraphUser> loggedInUser;

- (void)showGenderPicker:(id)sender;
- (void)genderWasSelected:(NSNumber *)selectedIndex element:(id)element;
- (void)showDatePicker:(id)sender;
- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element;
- (void)performSubmit:(id)sender;
- (void)submit;
- (void)requestMenu;

@end

@implementation UpdateProfileViewController

//@synthesize loggedInUser = _loggedInUser;
//
//@synthesize langActionSheet;
//@synthesize langPickerView;
//@synthesize lang;
//@synthesize rowLang;
//@synthesize selectedLang;
//
//@synthesize genderActionSheet;
//@synthesize genderPickerView;
@synthesize genders;
@synthesize rowGender;
@synthesize selectedGender;

//@synthesize birthdayActionSheet;
//@synthesize datePicker;
@synthesize dateValue;

@synthesize menuModel = _menuModel;

#pragma mark - APP Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload {
//    self.loggedInUser = nil;
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated {
    
    //--------------//
    // Setup Header //
    //--------------//
    /*
    SectionHeaderView *headerView1;
    if (IS_IPAD) {
        headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_HEIGHT,kDefaultCellHeight*1.5)
                                                withLabelForXL:_menuModel.groupName
                                                isFirstSection:YES];
    }
    else {
        headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,kDefaultCellHeight*1.5)
                                                withLabelForXL:_menuModel.groupName
                                                isFirstSection:YES];
    }
    headerView1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"texture.png"]];
    [_contentView addSubview:headerView1];
    */
    //
    // Handle PostPaid and Prepaid
    //
    SectionHeaderView *headerView2;
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    if (sharedData.profileData.msisdnType == 2) {
        // PostPaid
        if (IS_IPAD) {
            headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_HEIGHT,kDefaultCellHeight)
                                                    withLabelForXL:_menuModel.menuName
                                                    isFirstSection:NO];
        }
        else {
            headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,kDefaultCellHeight)
                                                    withLabelForXL:_menuModel.menuName
                                                    isFirstSection:NO];
        }
        
    }
    else {
        // PrePaid
        if (IS_IPAD) {
            headerView2 = [[SectionHeaderView alloc] initForProfileWithFrame:CGRectMake(0, 0, SCREEN_HEIGHT,kDefaultCellHeight)
                                                                   withTitle:_menuModel.menuName
                                                              withEditButton:@"Q"];
        }
        else {
            headerView2 = [[SectionHeaderView alloc] initForProfileWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,kDefaultCellHeight)
                                                                   withTitle:_menuModel.menuName
                                                              withEditButton:@"Q"];
        }
        
        [headerView2.editProfileButton addTarget:self action:@selector(performEditProfile) forControlEvents:UIControlEventTouchUpInside];
        _editedProfileButton = headerView2.editProfileButton;
        
    }
    headerView2.backgroundColor = [UIColor whiteColor];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    
    /*
     * Asterisk
     */
    _asteriskLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _asteriskLabel.textColor = kDefaultNavyBlueColor;
    _asteriskLabel.text = [Language get:@"asterisk" alter:nil];
    
    /*
     * Name
     */
    _fullNameLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _fullNameLabel.textColor = kDefaultTitleFontGrayColor;
    _fullNameLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"full_name" alter:nil]];
    
    _fullNameTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _fullNameTextField.textColor = kDefaultNavyBlueColor;
    _fullNameTextField.text = sharedData.profileData.fullName;
    
    /*
     * Gender
     */
    _genderLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _genderLabel.textColor = kDefaultTitleFontGrayColor;
    _genderLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"gender" alter:nil]];
    
    /*
     * Gender Button
     */
    self.genders = [NSArray arrayWithObjects:[Language get:@"male" alter:nil],
                    [Language get:@"female" alter:nil],
                    nil];
    if ([sharedData.profileData.gender isEqualToString:@"F"]) {
        rowGender = 1;
        self.selectedGender = @"F";
    }
    else {
        rowGender = 0;
        self.selectedGender = @"M";
    }
    
    UIImage *backgroundImage = [[UIImage imageNamed:@"combo_bg_1.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0];
    [_genderButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    [_genderButton setTitle:[self.genders objectAtIndex:rowGender] forState:UIControlStateNormal];
    [_genderButton setTitleColor:kDefaultNavyBlueColor forState:UIControlStateNormal];
    _genderButton.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _genderButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _genderButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [_genderButton addTarget:self action:@selector(showGenderPicker:) forControlEvents:UIControlEventTouchUpInside];
    
    /*
     * Place of Birth
     */
    _placeOfBirthLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _placeOfBirthLabel.textColor = kDefaultTitleFontGrayColor;
    _placeOfBirthLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"place_of_birth" alter:nil]];
    
    _placeOfBirthTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _placeOfBirthTextField.textColor = kDefaultNavyBlueColor;
    _placeOfBirthTextField.text = sharedData.profileData.placeBirth;
    
    /*
     * Birthday
     */
    _birthdayLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _birthdayLabel.textColor = kDefaultTitleFontGrayColor;
    _birthdayLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"birthday" alter:nil]];
    
    /*
     * Birthday Button
     */
    NSString *date = sharedData.profileData.birth;
    if ([date length] > 0) {
        NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
        [inputFormatter setDateFormat:@"dd-MM-yyyy"];
        self.dateValue = [inputFormatter dateFromString:date];
    }
    else {
        self.dateValue = [NSDate date];
    }
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd MMM yyyy"];
    [_birthdayButton setTitle:[NSString stringWithFormat:@"%@",[df stringFromDate:self.dateValue]]
                     forState:UIControlStateNormal];
    [_birthdayButton setTitleColor:kDefaultNavyBlueColor forState:UIControlStateNormal];
    _birthdayButton.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _birthdayButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _birthdayButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [_birthdayButton addTarget:self action:@selector(showDatePicker:) forControlEvents:UIControlEventTouchUpInside];
    [_birthdayButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
    /*
     * KTP
     */
    _ktpLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _ktpLabel.textColor = kDefaultTitleFontGrayColor;
    _ktpLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"ktp" alter:nil]];
    
    _ktpTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _ktpTextField.textColor = kDefaultNavyBlueColor;
    _ktpTextField.text = sharedData.profileData.idCardType;
    
    /*
     * KTP Number
     */
    _noKtpLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _noKtpLabel.textColor = kDefaultTitleFontGrayColor;
    _noKtpLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"ktp_number" alter:nil]];
    
    _noKtpTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _noKtpTextField.textColor = kDefaultNavyBlueColor;
    _noKtpTextField.text = sharedData.profileData.idCardNumber;
    
    /*
     * Address
     */
    _addressLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _addressLabel.textColor = kDefaultTitleFontGrayColor;
    _addressLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"address" alter:nil]];
    
    _address1TextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _address1TextField.textColor = kDefaultNavyBlueColor;
    _address1TextField.text = sharedData.profileData.address;
    
    _address2TextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _address2TextField.textColor = kDefaultNavyBlueColor;
    _address2TextField.text = sharedData.profileData.address2;
    
    _address3TextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _address3TextField.textColor = kDefaultNavyBlueColor;
    _address3TextField.text = sharedData.profileData.address3;
    
    /*
     * Email
     */
    _emailLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _emailLabel.textColor = kDefaultTitleFontGrayColor;
    _emailLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"email" alter:nil]];
    
    _emailTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _emailTextField.textColor = kDefaultNavyBlueColor;
    _emailTextField.text = sharedData.profileData.email;
    
    /*
     * Other Number
     */
    _otherNumberLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _otherNumberLabel.textColor = kDefaultTitleFontGrayColor;
    _otherNumberLabel.text = [NSString stringWithFormat:@"%@",[Language get:@"other_number" alter:nil]];
    
    _otherNumberTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _otherNumberTextField.textColor = kDefaultNavyBlueColor;
    _otherNumberTextField.text = sharedData.profileData.otherPhone;
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Texture.png"]];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    //newFrame.size.height = _languageButton.frame.origin.y + _languageButton.frame.size.height;
    newFrame.size.height = _emailTextField.frame.origin.y + _emailTextField.frame.size.height;
    _fieldView.frame = newFrame;
    
    /*
     * Submit Button
     */
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    UIButton *button = createButtonWithFrame(CGRectMake(15.0,
                                                        _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             
                                             [[Language get:@"submit" alter:nil] uppercaseString],
                                             buttonFont,
                                             kDefaultWhiteColor,
                                             colorWithHexString(sharedData.profileData.themesModel.menuFeature),
                                             colorWithHexString(sharedData.profileData.themesModel.menuColor));
    [button addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    button.hidden = YES;
    
    _submitButton = button;
    [_contentView addSubview:_submitButton];
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Texture.png"]];
    newFrame = _contentView.frame;
    newFrame.size.height = _submitButton.frame.origin.y + _submitButton.frame.size.height + kDefaultComponentPadding + (44.0*3) + (10.0*3);
    _contentView.frame = newFrame;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 50.0);
    
//    [headerView1 release];
    [headerView2 release];
    
    // Disable Input
    [self activateInput:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Implementation ActionSheetPicker.h

- (void)showGenderPicker:(id)sender {
    [ActionSheetStringPicker showPickerWithTitle:@""
                                            rows:self.genders
                                initialSelection:rowGender
                                          target:self
                                   successAction:@selector(genderWasSelected:element:)
                                    cancelAction:@selector(actionPickerCancelled:)
                                          origin:sender];
}

- (void)genderWasSelected:(NSNumber *)selectedIndex element:(id)element {
    rowGender = [selectedIndex intValue];
    if (rowGender == 0) {
        self.selectedGender = @"M";
    }
    else {
        self.selectedGender = @"F";
    }
    
    [_genderButton setTitle:[self.genders objectAtIndex:rowGender] forState:UIControlStateNormal];
    _genderButton.titleLabel.textColor = kDefaultNavyBlueColor;
}

- (void)showDatePicker:(id)sender {
    [ActionSheetDatePicker showPickerWithTitle:@""
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:self.dateValue
                                        target:self
                                        action:@selector(dateWasSelected:element:)
                                        origin:sender];
}

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element {
    self.dateValue = selectedDate;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd MMM yyyy"];
    
    [_birthdayButton setTitle:[NSString stringWithFormat:@"%@",[df stringFromDate:self.dateValue]]
                     forState:UIControlStateNormal];
    _birthdayButton.titleLabel.textColor = kDefaultNavyBlueColor;
}

- (void)actionPickerCancelled:(id)sender {
    //NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - Selector

- (int)validateInput {
    if ([_fullNameTextField.text length] <= 0 ||
        [_placeOfBirthTextField.text length] <= 0 ||
        [_ktpTextField.text length] <= 0 ||
        [_noKtpTextField.text length] <= 0 ||
        [_address1TextField.text length] <= 0 ||
        [_emailTextField.text length] <= 0) {
        return 2;
    }
    else if (isValidEmail(_emailTextField.text) == NO) {
        return 3;
    }
    else {
        return 0;
    }
}

- (void)performSubmit:(id)sender {
    int valid = [self validateInput];
    if (valid == 2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"empty_validation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    else if (valid == 3) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:[Language get:@"email_validation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"confirmation" alter:nil]
                                                        message:[Language get:@"update_profile_confirmation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:[[Language get:@"yes" alter:nil] uppercaseString]
                                              otherButtonTitles:[[Language get:@"no" alter:nil] uppercaseString], nil];
        alert.tag = 10;
        [alert show];
        [alert release];
    }
}

- (void)submit {
    [_fullNameTextField resignFirstResponder];
    [_placeOfBirthTextField resignFirstResponder];
    [_ktpTextField resignFirstResponder];
    [_noKtpTextField resignFirstResponder];
    [_address1TextField resignFirstResponder];
    [_address2TextField resignFirstResponder];
    [_address3TextField resignFirstResponder];
    [_emailTextField resignFirstResponder];
    [_otherNumberTextField resignFirstResponder];
    
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = UPDATE_PROFILE_REQ;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MM-yyyy"];
    NSString *birthday = [NSString stringWithFormat:@"%@",[df stringFromDate:self.dateValue]];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    [request updateProfile:@""
              withFullname:_fullNameTextField.text
                 withEmail:_emailTextField.text
              withLanguage:sharedData.profileData.language
                withGender:self.selectedGender
              withBirthday:birthday
            withEmailNotif:@""
             withPushNotif:@""
              withAddress1:_address1TextField.text
              withAddress2:_address2TextField.text
              withAddress3:_address3TextField.text
                  withCity:@""
               withZipcode:@""
             withAutologin:@""
            withIdCardType:_ktpTextField.text
          withIdCardNumber:_noKtpTextField.text
          withPlaceOfBirth:_placeOfBirthTextField.text
           withOtherNumber:_otherNumberTextField.text
                withStatus:sharedData.profileData.status];
}

- (void)requestMenu {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = MENU_REQ;
    [request menu];
}

- (void)performEditProfile {
    [self activateInput:YES];
}

- (void)activateInput:(BOOL)active {
    if (active) {
        
        _fullNameTextField.borderStyle = UITextBorderStyleLine;
                _placeOfBirthTextField.borderStyle = UITextBorderStyleLine;
                _ktpTextField.borderStyle = UITextBorderStyleLine;
                _noKtpTextField.borderStyle = UITextBorderStyleLine;
                _address1TextField.borderStyle = UITextBorderStyleLine;
                _emailTextField.borderStyle = UITextBorderStyleLine;
                _otherNumberTextField.borderStyle = UITextBorderStyleLine;
        _submitButton.hidden = NO;
        _editedProfileButton.hidden = YES;
        _fullNameTextField.enabled = YES;
        _genderButton.enabled = YES;
        _placeOfBirthTextField.enabled = YES;
        _birthdayButton.enabled = YES;
        _ktpTextField.enabled = YES;
        _noKtpTextField.enabled = YES;
        _address1TextField.enabled = YES;
        _address2TextField.enabled = YES;
        _address3TextField.enabled = YES;
        _emailTextField.enabled = YES;
        _otherNumberTextField.enabled = YES;
        _submitButton.enabled = YES;
        
        [_fullNameTextField becomeFirstResponder];
    }
    else {
        _fullNameTextField.enabled = NO;
        _genderButton.enabled = NO;
        _placeOfBirthTextField.enabled = NO;
        _birthdayButton.enabled = NO;
        _ktpTextField.enabled = NO;
        _noKtpTextField.enabled = NO;
        _address1TextField.enabled = NO;
        _address2TextField.enabled = NO;
        _address3TextField.enabled = NO;
        _emailTextField.enabled = NO;
        _otherNumberTextField.enabled = NO;
        _submitButton.enabled = NO;
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
        int type = [typeNumber intValue];
        
        if (type == UPDATE_PROFILE_REQ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[Language get:@"update_profile_notification" alter:nil]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
            
            // Get Menu if Language changed
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *currentLanguage = [prefs stringForKey:@"language"];
            
            DataManager *sharedData = [DataManager sharedInstance];
            NSString *selectedLanguage = sharedData.profileData.language;
            
            if (![currentLanguage isEqualToString:selectedLanguage]) {
                // Set Language
                [Language setLanguage:selectedLanguage];
                
                [self requestMenu];
            }
            else {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        
        if (type == MENU_REQ) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        if (type == REG_FB_TOKEN_REQ) {
            //NSLog(@"Register FB Berhasil");
        }
        
        if (type == UNLINK_FB_REQ) {
            //NSLog(@"Unlink FB Berhasil");
        }
        
        if (type == NOTIFICATION_REQ) {
            NotificationViewController *notificationViewController = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            
            DataManager *sharedData = [DataManager sharedInstance];
            notificationViewController.content = sharedData.notificationData;
            
            [notificationViewController createBarButtonItem:BACK];
            
            [self.navigationController pushViewController:notificationViewController animated:YES];
            notificationViewController = nil;
            [notificationViewController release];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGFloat targetY = -42 + textField.frame.origin.y;
    
    if (textField == _otherNumberTextField) {
        targetY = textField.frame.origin.y + 80.0;
    }
    if (textField == _emailTextField) {
        targetY = textField.frame.origin.y + 20.0;
    }
    
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 30);
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10) {
        if (buttonIndex == 0) {
            [self submit];
        }
    }
}

/*
#pragma mark -
#pragma mark UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView1 didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView1 == langPickerView) {
        rowLang = row;
    }
	else {
        rowGender = row;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView1 titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView1 == langPickerView) {
        return [lang objectAtIndex:row];
    }
    else {
        return [genders objectAtIndex:row];
    }
}

#pragma mark -
#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return 2;
}*/

@end
