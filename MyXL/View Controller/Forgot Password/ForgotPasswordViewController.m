//
//  ForgotPasswordViewController.m
//  AXISnet
//
//  Created by Tony Hadisiswanto on 1/23/13.
//  Copyright (c) 2013 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "ForgotPasswordViewController.h"

#import "Constant.h"
#import "AXISnetCommon.h"
#import <QuartzCore/QuartzCore.h>
#import "UnderLineLabel.h"

#import "RegisterViewController.h"
#import "AXISnetRequest.h"
#import "DataManager.h"

#import "AXISnetNavigationBar.h"
#import "SectionHeaderView.h"

@interface ForgotPasswordViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *asteriskLabel;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;

@property (strong, nonatomic) IBOutlet UILabel *refreshLabel;
@property (strong, nonatomic) IBOutlet UILabel *captchaLabel;
@property (strong, nonatomic) IBOutlet UIWebView *captchaWebView;

@property (strong, nonatomic) IBOutlet UnderLineLabel *recaptchaLabel;
@property (strong, nonatomic) IBOutlet UITextField *captchaTextField;

@property (strong, nonatomic) UIButton *submitButton;
@property (strong, nonatomic) UIButton *loginButton;
@property (strong, nonatomic) UIButton *registerButton;

- (void)refreshCaptcha;
- (void)performLogin:(id)sender;
- (void)performRegister:(id)sender;
- (void)performSubmit:(id)sender;
- (void)submit;

@end

@implementation ForgotPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    //---------------------//
    // Show Navigation Bar //
    //---------------------//
    AXISnetNavigationBar *navBar = (AXISnetNavigationBar *)[self.navigationController navigationBar];
    navBar.hidden = NO;
    DataManager *sharedData = [DataManager sharedInstance];
    
    NSString *documentsDirectory = applicationDocumentsDirectory();
    NSString *logoName = @"";
    if ([sharedData.profileData.telcoOperator isEqualToString:@"XL"]) {
        logoName = XL_LOGO_RETINA_NAME;
    }
    else if ([sharedData.profileData.telcoOperator isEqualToString:@"AXIS"]) {
        logoName = AXIS_LOGO_RETINA_NAME;
    }
    else {
        logoName = XL_LOGO_RETINA_NAME;
    }
    NSString *mediaPath = [documentsDirectory stringByAppendingPathComponent:logoName];
    [navBar updateLogo:mediaPath];
    //--
    
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:[[Language get:@"my_account" alter:nil] uppercaseString]
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:[[Language get:@"forget_password" alter:nil] uppercaseString]
                                                               isFirstSection:NO];
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
//    _titleLabel.font = [UIFont fontWithName:kDefaultFontBold size:24.0];
//    _titleLabel.textColor = kDefaultFontLightGrayColor;
//    _titleLabel.text = [[Language get:@"forget_password" alter:nil] uppercaseString];
    
    _asteriskLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _asteriskLabel.textColor = kDefaultNavyBlueColor;
    _asteriskLabel.text = [Language get:@"asterisk" alter:nil];
    
    _usernameLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _usernameLabel.textColor = kDefaultTitleFontGrayColor;
    NSString *text = [NSString stringWithFormat:@"%@*",[Language get:@"your_axis_number" alter:nil]];
    _usernameLabel.text = text;
    
    _captchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _captchaLabel.textColor = kDefaultTitleFontGrayColor;
    text = [NSString stringWithFormat:@"%@*",[Language get:@"captcha_code" alter:nil]];
    _captchaLabel.text = text;
    
    // Refresh Label
    _refreshLabel.font = [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize];
    _refreshLabel.textColor = kDefaultNavyBlueColor;
    
    _recaptchaLabel.font = [UIFont fontWithName:kDefaultFontBold size:kDefaultFontTitleSize];
    _recaptchaLabel.textColor = kDefaultPurpleColor;
    _recaptchaLabel.text = [Language get:@"recaptcha" alter:nil];
    [_recaptchaLabel setShouldUnderline:NO];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(refreshCaptcha)];
    [_recaptchaLabel setUserInteractionEnabled:YES];
    [_recaptchaLabel addGestureRecognizer:tapGesture];
    [tapGesture release];
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    _fieldView.frame = newFrame;
    
    //--------------//
    // Setup Button //
    //--------------//
    
    // Activation Button
    UIFont *titleFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    
    UIButton *button = createButtonWithFrame(CGRectMake(_captchaTextField.frame.origin.x,
                                                        _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
                                                        100,
                                                        kDefaultButtonHeight),
                                             [[Language get:@"submit" alter:nil] uppercaseString],
                                             titleFont,
                                             kDefaultWhiteColor,
                                             kDefaultAquaMarineColor,
                                             kDefaultBlueColor);
    
    [button addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    _submitButton = button;
    [_contentView addSubview:_submitButton];
    
    // Login Button
    CGFloat padding = 10.0;
    CGFloat contentWidth = _contentView.frame.size.width - (padding*2);
    
    button = createButtonWithIconAndArrow(CGRectMake(padding, _submitButton.frame.origin.y + _submitButton.frame.size.height + 20.0, contentWidth, kDefaultButtonHeight),
                                          @"F",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          [[Language get:@"login" alter:nil] uppercaseString],
                                          [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          @"d",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          kDefaultAquaMarineColor,
                                          kDefaultBlueColor);
    
    [button addTarget:self
               action:@selector(performLogin:)
     forControlEvents:UIControlEventTouchDown];
    
    _loginButton = button;
    [_contentView addSubview:_loginButton];
    
    // Register Button
    button = createButtonWithIconAndArrow(CGRectMake(padding, _loginButton.frame.origin.y + _loginButton.frame.size.height + 4.0, contentWidth, kDefaultButtonHeight),
                                          @"A",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          [[Language get:@"registration" alter:nil] uppercaseString],
                                          [UIFont fontWithName:kDefaultFontBold size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          @"d",
                                          [UIFont fontWithName:kDefaultFontKSAN size:kDefaultFontSize],
                                          kDefaultWhiteColor,
                                          kDefaultAquaMarineColor,
                                          kDefaultBlueColor);
    
    [button addTarget:self
               action:@selector(performRegister:)
     forControlEvents:UIControlEventTouchDown];
    
    _registerButton = button;
    [_contentView addSubview:_registerButton];
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultWhiteColor;
    newFrame = _contentView.frame;
    newFrame.size.height = _registerButton.frame.origin.y + _registerButton.frame.size.height + kDefaultComponentPadding + 44 + 10;
    _contentView.frame = newFrame;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 300.0);
    
    [headerView1 release];
    [headerView2 release];
    
    // Captcha
    [self refreshCaptcha];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Selector

- (void)refreshCaptcha {
    NSString *captchaURL = generateCaptchaURL();
    NSURL *captcha = [NSURL URLWithString:captchaURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:captcha];
    [_captchaWebView loadRequest:request];
    [request release];
}

- (void)performSubmit:(id)sender {
    [self submit];
}

- (void)performLogin:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)performRegister:(id)sender {
    RegisterViewController *registerViewController = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:registerViewController animated:YES];
    registerViewController = nil;
    [registerViewController release];
}

- (void)submit {
    [_usernameTextField resignFirstResponder];
    [_captchaTextField resignFirstResponder];
    
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = RESET_PASSWORD_REQ;
    
    int currentLang = [Language getCurrentLanguage];
    NSString *lang = @"";
    if (currentLang == 0) {
        lang = @"en";
    }
    else {
        lang = @"id";
    }
    
    NSString *msisdn = @"";
    if ([_usernameTextField.text length] > 0) {
        msisdn = _usernameTextField.text;
    }
    
    NSString *captcha = @"";
    if ([_captchaTextField.text length] > 0) {
        captcha = _captchaTextField.text;
    }
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    [request resetPassword:msisdn
               withCaptcha:captcha
                   withCid:sharedData.cid
                 withToken:@""
              withLanguage:lang];
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    
    NSNumber *typeNumber = [result valueForKey:REQUEST_KEY];
    int type = [typeNumber intValue];
    
    BOOL success = [reqResult boolValue];
    
    if (success) {
        if (type == RESET_PASSWORD_REQ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[Language get:@"forget_password_success" alter:nil]
                                                           delegate:self
                                                  cancelButtonTitle:[[Language get:@"ok" alter:nil] uppercaseString]
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language get:@"notice" alter:nil]
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        [self refreshCaptcha];
    }
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    CGFloat targetY = -42 + textField.frame.origin.y;
    
    if (textField == _captchaTextField) {
        targetY = -60 + textField.frame.origin.y;
    }
    
    //[_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 30);
}

@end
