//
//  Replace4GUsimViewController.m
//  MyXL
//
//  Created by Tony Hadisiswanto on 4/9/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import "Replace4GUsimViewController.h"
#import "Constant.h"
#import "AXISnetCommon.h"
#import "SectionHeaderView.h"
#import "ContentView.h"
#import <QuartzCore/QuartzCore.h>

#import "DataManager.h"
#import "EncryptDecrypt.h"

#import "ActionSheetStringPicker.h"

#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"

#import "NotificationViewController.h"

#import "ActionSheetDatePicker.h"

@interface Replace4GUsimViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *theScrollView;
@property (strong, nonatomic) IBOutlet UIView *fieldView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *asteriskLabel;

@property (strong, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (strong, nonatomic) IBOutlet UITextField *fullNameTextField;
@property (strong, nonatomic) IBOutlet UILabel *fullNameRedLabel;

@property (strong, nonatomic) IBOutlet UILabel *genderLabel;
@property (strong, nonatomic) IBOutlet UIButton *genderButton;

@property (strong, nonatomic) IBOutlet UILabel *birthdayLabel;
@property (strong, nonatomic) IBOutlet UIButton *birthdayButton;

@property (strong, nonatomic) IBOutlet UILabel *noKtpLabel;
@property (strong, nonatomic) IBOutlet UITextField *noKtpTextField;
@property (strong, nonatomic) IBOutlet UILabel *noKtpRedLabel;

@property (strong, nonatomic) IBOutlet UILabel *otherNumberLabel;
@property (strong, nonatomic) IBOutlet UITextField *otherNumberTextField;
@property (strong, nonatomic) IBOutlet UILabel *otherNumberRedLabel;

@property (strong, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UILabel *emailRedLabel;

@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UITextField *addressTextField;
@property (strong, nonatomic) IBOutlet UILabel *addressRedLabel;

@property (strong, nonatomic) IBOutlet UILabel *cityLabel;
@property (strong, nonatomic) IBOutlet UITextField *cityTextField;
@property (strong, nonatomic) IBOutlet UILabel *cityRedLabel;

@property (strong, nonatomic) IBOutlet UILabel *zipLabel;
@property (strong, nonatomic) IBOutlet UITextField *zipTextField;
@property (strong, nonatomic) IBOutlet UILabel *zipRedLabel;

//-------------
@property (strong, nonatomic) UIButton *submitButton;

//@property (strong, nonatomic) UIActionSheet *langActionSheet;
//@property (strong, nonatomic) UIPickerView *langPickerView;
//@property (strong, nonatomic) NSArray *lang;
//@property (readwrite) int rowLang;
//@property (strong, nonatomic) NSString *selectedLang;

//@property (strong, nonatomic) UIActionSheet *genderActionSheet;
//@property (strong, nonatomic) UIPickerView *genderPickerView;
@property (strong, nonatomic) NSArray *genders;
@property (readwrite) int rowGender;
@property (strong, nonatomic) NSString *selectedGender;

//@property (strong, nonatomic) UIActionSheet *birthdayActionSheet;
//@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) NSDate *dateValue;

//@property (strong, nonatomic) id<FBGraphUser> loggedInUser;

- (void)showGenderPicker:(id)sender;
- (void)genderWasSelected:(NSNumber *)selectedIndex element:(id)element;
- (void)showDatePicker:(id)sender;
- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element;
- (void)performSubmit:(id)sender;
- (void)submit;
- (void)requestMenu;

@end

@implementation Replace4GUsimViewController

@synthesize genders;
@synthesize rowGender;
@synthesize selectedGender;

//@synthesize birthdayActionSheet;
//@synthesize datePicker;
@synthesize dateValue;

@synthesize menuModel = _menuModel;

#pragma mark - APP Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    //--------------//
    // Setup Header //
    //--------------//
    SectionHeaderView *headerView1 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width,kDefaultCellHeight*1.5)
                                                               withLabelForXL:_menuModel.groupName
                                                               isFirstSection:YES];
    [_contentView addSubview:headerView1];
    
    //
    // Handle PostPaid and Prepaid
    //
    SectionHeaderView *headerView2 = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0, headerView1.frame.origin.y + headerView1.frame.size.height, _contentView.frame.size.width,kDefaultCellHeight)
                                                               withLabelForXL:_menuModel.menuName
                                                               isFirstSection:NO];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    [_contentView addSubview:headerView2];
    
    //-------------//
    // Setup Label //
    //-------------//
    
    /*
     * Asterisk
     */
    _asteriskLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _asteriskLabel.textColor = kDefaultNavyBlueColor;
    _asteriskLabel.text = [Language get:@"asterisk" alter:nil];
    
    /*
     * Name
     */
    _fullNameLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _fullNameLabel.textColor = kDefaultTitleFontGrayColor;
    _fullNameLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"complete_full_name" alter:nil]];
    
    _fullNameTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _fullNameTextField.textColor = kDefaultNavyBlueColor;
    //_fullNameTextField.text = sharedData.profileData.fullName;
    
    _fullNameRedLabel.font = [UIFont fontWithName:kDefaultFontLight size:12.0];
    _fullNameRedLabel.text = @"";
    
    /*
     * Gender
     */
    _genderLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _genderLabel.textColor = kDefaultTitleFontGrayColor;
    _genderLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"gender" alter:nil]];
    
    /*
     * Gender Button
     */
    self.genders = [NSArray arrayWithObjects:[Language get:@"male" alter:nil],
                    [Language get:@"female" alter:nil],
                    nil];
    if ([sharedData.profileData.gender isEqualToString:@"F"]) {
        rowGender = 1;
        self.selectedGender = @"F";
    }
    else {
        rowGender = 0;
        self.selectedGender = @"M";
    }
    
    UIImage *backgroundImage = [[UIImage imageNamed:@"combo_bg_1.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0];
    [_genderButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    [_genderButton setTitle:[self.genders objectAtIndex:rowGender] forState:UIControlStateNormal];
    [_genderButton setTitleColor:kDefaultNavyBlueColor forState:UIControlStateNormal];
    _genderButton.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _genderButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _genderButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [_genderButton addTarget:self action:@selector(showGenderPicker:) forControlEvents:UIControlEventTouchUpInside];
    
    /*
     * Birthday
     */
    _birthdayLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _birthdayLabel.textColor = kDefaultTitleFontGrayColor;
    _birthdayLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"birthday_4gusim" alter:nil]];
    
    /*
     * Birthday Button
     */
    NSString *date = sharedData.profileData.birth;
    if ([date length] > 0) {
        NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
        [inputFormatter setDateFormat:@"dd-MM-yyyy"];
        self.dateValue = [inputFormatter dateFromString:date];
    }
    else {
        self.dateValue = [NSDate date];
    }
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd MMM yyyy"];
    [_birthdayButton setTitle:[NSString stringWithFormat:@"%@",[df stringFromDate:self.dateValue]]
                     forState:UIControlStateNormal];
    [_birthdayButton setTitleColor:kDefaultNavyBlueColor forState:UIControlStateNormal];
    _birthdayButton.titleLabel.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _birthdayButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _birthdayButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [_birthdayButton addTarget:self action:@selector(showDatePicker:) forControlEvents:UIControlEventTouchUpInside];
    [_birthdayButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
    /*
     * KTP Number
     */
    _noKtpLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _noKtpLabel.textColor = kDefaultTitleFontGrayColor;
    _noKtpLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"complete_ktp_number" alter:nil]];
    
    _noKtpTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _noKtpTextField.textColor = kDefaultNavyBlueColor;
    //_noKtpTextField.text = sharedData.profileData.idCardNumber;
    
    _noKtpRedLabel.font = [UIFont fontWithName:kDefaultFontLight size:12.0];
    _noKtpRedLabel.text = @"";
    
    /*
     * Phone Number
     */
    _otherNumberLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _otherNumberLabel.textColor = kDefaultTitleFontGrayColor;
    _otherNumberLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"phone_number" alter:nil]];
    
    _otherNumberTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _otherNumberTextField.textColor = kDefaultNavyBlueColor;
    //NSString *saltKeyAPI = SALT_KEY;
    //_otherNumberTextField.text = [EncryptDecrypt doCipherForiOS7:sharedData.profileData.msisdn action:kCCDecrypt withKey:saltKeyAPI];
    
    _otherNumberRedLabel.font = [UIFont fontWithName:kDefaultFontLight size:12.0];
    _otherNumberRedLabel.text = @"";
    
    /*
     * Email
     */
    _emailLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _emailLabel.textColor = kDefaultTitleFontGrayColor;
    _emailLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"email_4gusim" alter:nil]];
    
    _emailTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _emailTextField.textColor = kDefaultNavyBlueColor;
    //_emailTextField.text = sharedData.profileData.email;
    
    _emailRedLabel.font = [UIFont fontWithName:kDefaultFontLight size:12.0];
    _emailRedLabel.text = @"";
    
    /*
     * Address
     */
    _addressLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _addressLabel.textColor = kDefaultTitleFontGrayColor;
    _addressLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"address" alter:nil]];
    
    _addressTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _addressTextField.textColor = kDefaultNavyBlueColor;
    //_addressTextField.text = sharedData.profileData.address;
    
    _addressRedLabel.font = [UIFont fontWithName:kDefaultFontLight size:12.0];
    _addressRedLabel.text = @"";
    
    /*
     * City
     */
    _cityLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _cityLabel.textColor = kDefaultTitleFontGrayColor;
    _cityLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"city" alter:nil]];
    
    _cityTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _cityTextField.textColor = kDefaultNavyBlueColor;
    //_cityTextField.text = sharedData.profileData.city;
    
    _cityRedLabel.font = [UIFont fontWithName:kDefaultFontLight size:12.0];
    _cityRedLabel.text = @"";
    
    /*
     * ZIP Code
     */
    _zipLabel.font = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    _zipLabel.textColor = kDefaultTitleFontGrayColor;
    _zipLabel.text = [NSString stringWithFormat:@"%@*",[Language get:@"zip_code" alter:nil]];
    
    _zipTextField.font = [UIFont fontWithName:kDefaultFontLight size:kDefaultFontValueSize];
    _zipTextField.textColor = kDefaultNavyBlueColor;
    //_zipTextField.text = sharedData.profileData.postcode;
    
    _zipRedLabel.font = [UIFont fontWithName:kDefaultFontLight size:12.0];
    _zipRedLabel.text = @"";
    
    //------------------//
    // Setup Field View //
    //------------------//
    _fieldView.backgroundColor = [UIColor clearColor];
    CGRect newFrame = _fieldView.frame;
    newFrame.origin.y = headerView2.frame.origin.y + headerView2.frame.size.height + kDefaultComponentPadding;
    newFrame.size.height = _zipRedLabel.frame.origin.y + _zipRedLabel.frame.size.height;
    _fieldView.frame = newFrame;
    
    /*
     * Submit Button
     */
    UIFont *buttonFont = [UIFont fontWithName:kDefaultFont size:kDefaultFontSize];
    UIButton *button = createButtonWithFrame(CGRectMake(15.0,
                                                        _fieldView.frame.origin.y + _fieldView.frame.size.height + kDefaultComponentPadding,
                                                        150,
                                                        kDefaultButtonHeight),
                                             
                                             [[Language get:@"replace_4g_usim" alter:nil] uppercaseString],
                                             buttonFont,
                                             kDefaultWhiteColor,
                                             [UIColor grayColor],
                                             [UIColor grayColor]);
    [button addTarget:self action:@selector(performSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    _submitButton = button;
    [_contentView addSubview:_submitButton];
    
    //colorWithHexString(sharedData.profileData.themesModel.menuFeature)
    //colorWithHexString(sharedData.profileData.themesModel.menuColor)
    
    //--------------//
    // Content View //
    //--------------//
    _contentView.backgroundColor = kDefaultWhiteColor;
    newFrame = _contentView.frame;
    newFrame.size.height = _submitButton.frame.origin.y + _submitButton.frame.size.height + kDefaultComponentPadding + (44.0*3) + (10.0*3);
    _contentView.frame = newFrame;
    
    _theScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, _contentView.frame.origin.y + _contentView.frame.size.height + 50.0);
    
    [headerView1 release];
    [headerView2 release];
    
    // Disable Input
    [self activateInput:YES];
}

#pragma mark - Implementation ActionSheetPicker.h

- (void)showGenderPicker:(id)sender {
    [ActionSheetStringPicker showPickerWithTitle:@""
                                            rows:self.genders
                                initialSelection:rowGender
                                          target:self
                                   successAction:@selector(genderWasSelected:element:)
                                    cancelAction:@selector(actionPickerCancelled:)
                                          origin:sender];
}

- (void)genderWasSelected:(NSNumber *)selectedIndex element:(id)element {
    rowGender = [selectedIndex intValue];
    if (rowGender == 0) {
        self.selectedGender = @"M";
    }
    else {
        self.selectedGender = @"F";
    }
    
    [_genderButton setTitle:[self.genders objectAtIndex:rowGender] forState:UIControlStateNormal];
    _genderButton.titleLabel.textColor = kDefaultNavyBlueColor;
}

- (void)showDatePicker:(id)sender {
    [ActionSheetDatePicker showPickerWithTitle:@""
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:self.dateValue
                                        target:self
                                        action:@selector(dateWasSelected:element:)
                                        origin:sender];
}

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element {
    self.dateValue = selectedDate;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd MMM yyyy"];
    
    [_birthdayButton setTitle:[NSString stringWithFormat:@"%@",[df stringFromDate:self.dateValue]]
                     forState:UIControlStateNormal];
    _birthdayButton.titleLabel.textColor = kDefaultNavyBlueColor;
}

- (void)actionPickerCancelled:(id)sender {
    //NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - Selector

- (int)validateInput {
    if ([_fullNameTextField.text length] <= 0 ||
        [_noKtpTextField.text length] <= 0 ||
        [_otherNumberTextField.text length] <= 0 ||
        [_emailTextField.text length] <= 0 ||
        [_addressTextField.text length] <= 0 ||
        [_cityTextField.text length] <= 0 ||
        [_zipTextField.text length] <= 0) {
        return 2;
    }
    else if (isValidCharacter(_fullNameTextField.text) == NO) {
        return 3; // fullname char
    }
    else if ([_noKtpTextField.text length] < 6) {
        return 4; // no ktp length
    }
    else if ([_otherNumberTextField.text length] < 8) {
        return 5; // phone number length
    }
    else if (isValidEmail(_emailTextField.text) == NO) {
        return 6; // email
    }
    else if (isValidCharacter(_cityTextField.text) == NO) {
        return 7; // city char
    }
    else if ([_zipTextField.text length] < 4) {
        return 8; // city char
    }
    else {
        return 0;
    }
}

- (void)performSubmit:(id)sender {
    int valid = [self validateInput];
    if (valid == 2) {
        _fullNameRedLabel.text      = [Language get:@"field_is_required" alter:nil];
        _noKtpRedLabel.text         = [Language get:@"field_is_required" alter:nil];
        _otherNumberRedLabel.text   = [Language get:@"field_is_required" alter:nil];
        _emailRedLabel.text         = [Language get:@"field_is_required" alter:nil];
        _addressRedLabel.text       = [Language get:@"field_is_required" alter:nil];
        _cityRedLabel.text          = [Language get:@"field_is_required" alter:nil];
        _zipRedLabel.text           = [Language get:@"field_is_required" alter:nil];
    }
    else if (valid == 3) {
        _fullNameRedLabel.text      = [Language get:@"invalid_format" alter:nil];
        _noKtpRedLabel.text         = @"";
        _otherNumberRedLabel.text   = @"";
        _emailRedLabel.text         = @"";
        _addressRedLabel.text       = @"";
        _cityRedLabel.text          = @"";
        _zipRedLabel.text           = @"";
    }
    else if (valid == 4) {
        _fullNameRedLabel.text      = @"";
        _noKtpRedLabel.text         = [Language get:@"invalid_format" alter:nil];
        _otherNumberRedLabel.text   = @"";
        _emailRedLabel.text         = @"";
        _addressRedLabel.text       = @"";
        _cityRedLabel.text          = @"";
        _zipRedLabel.text           = @"";
    }
    else if (valid == 5) {
        _fullNameRedLabel.text      = @"";
        _noKtpRedLabel.text         = @"";
        _otherNumberRedLabel.text   = [Language get:@"invalid_format" alter:nil];
        _emailRedLabel.text         = @"";
        _addressRedLabel.text       = @"";
        _cityRedLabel.text          = @"";
        _zipRedLabel.text           = @"";
    }
    else if (valid == 6) {
        _fullNameRedLabel.text      = @"";
        _noKtpRedLabel.text         = @"";
        _otherNumberRedLabel.text   = @"";
        _emailRedLabel.text         = [Language get:@"invalid_format" alter:nil];
        _addressRedLabel.text       = @"";
        _cityRedLabel.text          = @"";
        _zipRedLabel.text           = @"";
    }
    else if (valid == 7) {
        _fullNameRedLabel.text      = @"";
        _noKtpRedLabel.text         = @"";
        _otherNumberRedLabel.text   = @"";
        _emailRedLabel.text         = @"";
        _addressRedLabel.text       = @"";
        _cityRedLabel.text          = [Language get:@"invalid_format" alter:nil];
        _zipRedLabel.text           = @"";
    }
    else if (valid == 8) {
        _fullNameRedLabel.text      = @"";
        _noKtpRedLabel.text         = @"";
        _otherNumberRedLabel.text   = @"";
        _emailRedLabel.text         = @"";
        _addressRedLabel.text       = @"";
        _cityRedLabel.text          = @"";
        _zipRedLabel.text           = [Language get:@"invalid_format" alter:nil];
    }
    else {
        _fullNameRedLabel.text      = @"";
        _noKtpRedLabel.text         = @"";
        _otherNumberRedLabel.text   = @"";
        _emailRedLabel.text         = @"";
        _addressRedLabel.text       = @"";
        _cityRedLabel.text          = @"";
        _zipRedLabel.text           = @"";
        [self submit];
    }
}

- (void)submit {
    [_fullNameTextField resignFirstResponder];
    [_noKtpTextField resignFirstResponder];
    [_otherNumberTextField resignFirstResponder];
    [_emailTextField resignFirstResponder];
    [_addressTextField resignFirstResponder];
    [_cityTextField resignFirstResponder];
    [_zipTextField resignFirstResponder];
    
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = REPLACE_4G_USIM_REQ;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MM-yyyy"];
    NSString *birthday = [NSString stringWithFormat:@"%@",[df stringFromDate:self.dateValue]];
    
    DataManager *sharedData = [DataManager sharedInstance];
    
    [request replace4gUsim:@""
              withFullname:_fullNameTextField.text
                 withEmail:_emailTextField.text
              withLanguage:sharedData.profileData.language
                withGender:self.selectedGender
              withBirthday:birthday
            withEmailNotif:@""
             withPushNotif:@""
              withAddress1:_addressTextField.text
              withAddress2:@""
              withAddress3:@""
                  withCity:_cityTextField.text
               withZipcode:_zipTextField.text
             withAutologin:@""
            withIdCardType:@""
          withIdCardNumber:_noKtpTextField.text
          withPlaceOfBirth:@""
           withOtherNumber:_otherNumberTextField.text
                withStatus:sharedData.profileData.status];
}

- (void)requestMenu {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.labelText = [Language get:@"loading" alter:nil];
    [self showHudToView:self.view withText:[Language get:@"loading" alter:nil]];
    
    AXISnetRequest *request = [AXISnetRequest sharedInstance];
    request.delegate = self;
    request.requestType = MENU_REQ;
    [request menu];
}

- (void)performEditProfile {
    [self activateInput:YES];
}

- (void)activateInput:(BOOL)active {
    if (active) {
        _fullNameTextField.enabled = YES;
        _genderButton.enabled = YES;
        _cityTextField.enabled = YES;
        _birthdayButton.enabled = YES;
        _zipTextField.enabled = YES;
        _noKtpTextField.enabled = YES;
        _addressTextField.enabled = YES;
        _emailTextField.enabled = YES;
        _otherNumberTextField.enabled = YES;
        _submitButton.enabled = NO;
        [_submitButton setBackgroundImage:imageFromColor([UIColor grayColor])
                                 forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:imageFromColor([UIColor grayColor])
                                 forState:UIControlStateHighlighted];
        
        [_fullNameTextField becomeFirstResponder];
    }
    else {
        _fullNameTextField.enabled = NO;
        _genderButton.enabled = NO;
        _cityTextField.enabled = NO;
        _birthdayButton.enabled = NO;
        _zipTextField.enabled = NO;
        _noKtpTextField.enabled = NO;
        _addressTextField.enabled = NO;
        _emailTextField.enabled = NO;
        _otherNumberTextField.enabled = NO;
        _submitButton.enabled = NO;
        [_submitButton setBackgroundImage:imageFromColor([UIColor grayColor])
                                 forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:imageFromColor([UIColor grayColor])
                                 forState:UIControlStateHighlighted];
    }
}

- (void)resetInputField {
    _fullNameTextField.text     = @"";
    _noKtpTextField.text        = @"";
    _otherNumberTextField.text  = @"";
    _emailTextField.text        = @"";
    _addressTextField.text      = @"";
    _cityTextField.text         = @"";
    _zipTextField.text          = @"";
    _submitButton.enabled = NO;
    [_submitButton setBackgroundImage:imageFromColor([UIColor grayColor])
                             forState:UIControlStateNormal];
    [_submitButton setBackgroundImage:imageFromColor([UIColor grayColor])
                             forState:UIControlStateHighlighted];
}

- (void)checkEveryField {
    if ([_fullNameTextField.text length] > 0 &&
        [_noKtpTextField.text length] > 0 &&
        [_otherNumberTextField.text length] > 0 &&
        [_emailTextField.text length] > 0 &&
        [_addressTextField.text length] > 0 &&
        [_cityTextField.text length] > 0 &&
        [_zipTextField.text length] > 0) {
        
        DataManager *sharedData = [DataManager sharedInstance];
        [_submitButton setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuFeature))
                                 forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:imageFromColor(colorWithHexString(sharedData.profileData.themesModel.menuColor))
                                 forState:UIControlStateHighlighted];
        _submitButton.enabled = YES;
    }
    else {
        [_submitButton setBackgroundImage:imageFromColor([UIColor grayColor])
                                 forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:imageFromColor([UIColor grayColor])
                                 forState:UIControlStateHighlighted];
        _submitButton.enabled = NO;
    }
}

#pragma mark - AXISnetRequestDelegate

- (void)requestDoneWithInfo:(NSDictionary*)result {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSNumber *reqResult = [result valueForKey:RESULT_KEY];
    BOOL success = [reqResult boolValue];
    
    if (success) {
        NSString *reason = [result valueForKey:REASON_KEY];
        NSLog(@"reason = %@",reason);
        /*
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[Language get:@"replace_4g_usim_confirmation" alter:nil]
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"close" alter:nil]
                                              otherButtonTitles:nil];*/
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"close" alter:nil]
                                              otherButtonTitles:nil];
        
        [alert show];
        [alert release];
    }
    else {
        NSString *reason = [result valueForKey:REASON_KEY];
        NSLog(@"reason = %@",reason);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:reason
                                                       delegate:self
                                              cancelButtonTitle:[Language get:@"close" alter:nil]
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    [self resetInputField];
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self checkEveryField];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self checkEveryField];
    CGFloat targetY = -42 + textField.frame.origin.y;
    
    if (textField == _otherNumberTextField) {
        targetY = textField.frame.origin.y + 80.0;
    }
    if (textField == _emailTextField) {
        targetY = textField.frame.origin.y + 20.0;
    }
    
    [_theScrollView setContentOffset:CGPointMake(0.0, targetY) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self checkEveryField];
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == _fullNameTextField) {
        return !([newString length] > 30);
    }
    else if (textField == _noKtpTextField) {
        return !([newString length] > 20);
    }
    else if (textField == _otherNumberTextField) {
        return !([newString length] > 20);
    }
    else if (textField == _emailTextField) {
        return !([newString length] > 320);
    }
    else if (textField == _addressTextField) {
        return !([newString length] > 70);
    }
    else if (textField == _cityTextField) {
        return !([newString length] > 25);
    }
    else if (textField == _zipTextField) {
        return !([newString length] > 6);
    }
    else {
        return !([newString length] > 30);
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10) {
        if (buttonIndex == 0) {
            [self submit];
        }
    }
}

@end
