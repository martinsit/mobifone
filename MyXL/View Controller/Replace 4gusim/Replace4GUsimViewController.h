//
//  Replace4GUsimViewController.h
//  MyXL
//
//  Created by Tony Hadisiswanto on 4/9/15.
//  Copyright (c) 2015 PT AXIS Telekom Indonesia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "AXISnetRequest.h"
#import "MenuModel.h"
#import "UpdateProfileViewController.h"

@interface Replace4GUsimViewController : BasicViewController <UITextFieldDelegate, AXISnetRequestDelegate, UIAlertViewDelegate> {
    PickerType pickerType;
    MenuModel *_menuModel;
}

@property (nonatomic, retain) MenuModel *menuModel;

@end
